/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.IdentificationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.Assujeti;
import cd.hologram.erecettesvg.models.Atd;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.Commandement;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.Contrainte;
import cd.hologram.erecettesvg.models.DetailsRole;
import cd.hologram.erecettesvg.models.FichePriseCharge;
import cd.hologram.erecettesvg.models.Med;
import cd.hologram.erecettesvg.models.MedPeriodeDeclaration;
import cd.hologram.erecettesvg.models.ParamAmr;
import cd.hologram.erecettesvg.models.ParamContrainte;
import cd.hologram.erecettesvg.models.Penalite;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.RetraitDeclaration;
import cd.hologram.erecettesvg.pojo.DetailPenaliteMock;
import cd.hologram.erecettesvg.properties.PropertiesMessage;
import cd.hologram.erecettesvg.sql.SQLQueryPoursuite;
import cd.hologram.erecettesvg.util.Casting;
import cd.hologram.erecettesvg.util.ConvertDate;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gauthier.muata
 */
public class PoursuiteBusiness {

    private static final Dao myDao = new Dao();

    public static List<Med> getListMedBySimpleSearch(String typeSearch, String valueSearch) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilter = GeneralConst.EMPTY_STRING;

        switch (typeSearch) {
            case GeneralConst.Number.ONE:
                sqlParamFilter = " AND M.PERSONNE = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
            case GeneralConst.Number.TWO:
                sqlParamFilter = " AND M.NUMERO_DOCUMENT = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
        }

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_MED_V1;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilter);

        List<Med> listMed = (List<Med>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Med.class), false);
        return listMed;
    }

    public static List<Med> getListMedISBySimpleSearch(String typeSearch, String valueSearch) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilter = GeneralConst.EMPTY_STRING;

        switch (typeSearch) {
            case GeneralConst.Number.ONE:
                sqlParamFilter = " AND M.PERSONNE = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
            case GeneralConst.Number.TWO:
                sqlParamFilter = " AND M.ID = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
        }

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_MED_INVITATION_SERVICE_PAIEMENT;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilter);

        List<Med> listMed = (List<Med>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Med.class), false);
        return listMed;
    }

    public static List<Med> getListMedBySimpleSearchService(String typeSearch, String valueSearch) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilter = GeneralConst.EMPTY_STRING;

        switch (typeSearch) {
            case GeneralConst.Number.ONE:
                sqlParamFilter = " AND M.PERSONNE = '%s' AND M.TYPE_MED = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch, "INVITATION_SERVICE");
                break;
            case GeneralConst.Number.TWO:
                sqlParamFilter = " AND M.ID = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
        }

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_MED_V1;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilter);

        List<Med> listMed = (List<Med>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Med.class), false);
        return listMed;
    }

    public static List<Med> getListMedByAdvancedSearch(String codeService, String typeMed, String stateMed,
            String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByService = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByTypeMed = " AND M.TYPE_MED = '%s'";
        String sqlParamFilterByStateMed = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByDateMed = GeneralConst.EMPTY_STRING;

        switch (typeMed) {
            case GeneralConst.Number.ONE:

                sqlParamFilterByTypeMed = String.format(sqlParamFilterByTypeMed, DocumentConst.DocumentCode.MED_2);

                if (!codeService.equals(GeneralConst.ALL)) {

                    sqlParamFilterByService = " AND M.ARTICLE_BUDGETAIRE IN (SELECT AB.CODE "
                            + " FROM T_ARTICLE_BUDGETAIRE AB WITH (READPAST) "
                            + " WHERE AB.ARTICLE_GENERIQUE IN (SELECT AG.CODE "
                            + " FROM T_ARTICLE_GENERIQUE AG WITH (READPAST) WHERE AG.SERVICE_ASSIETTE = '%s'))";

                    sqlParamFilterByService = String.format(sqlParamFilterByService, codeService.trim());
                }

                break;
            case GeneralConst.Number.TWO:

                sqlParamFilterByTypeMed = String.format(sqlParamFilterByTypeMed, DocumentConst.DocumentCode.MEP_2);

                if (!codeService.equals(GeneralConst.ALL)) {

                    sqlParamFilterByService = " AND M.NOTE_CALCUL IN (SELECT NC.NUMERO "
                            + " FROM T_NOTE_CALCUL NC WITH (READPAST) WHERE NC.SERVICE = '%s')";

                    sqlParamFilterByService = String.format(sqlParamFilterByService, codeService.trim());
                }

                break;
        }

        switch (stateMed) {
            case GeneralConst.Number.TWO:
                sqlParamFilterByStateMed = " AND CONVERT(DATE,M.DATE_ECHEANCE,103) > CONVERT(DATE,GETDATE(),103)";
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByStateMed = " AND CONVERT(DATE,M.DATE_ECHEANCE,103) < CONVERT(DATE,GETDATE(),103)";
                break;
        }

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterByDateMed = " AND DBO.FDATE(M.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterByDateMed = String.format(sqlParamFilterByDateMed, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_MED_V2;

        sqlQueryMaster = String.format(sqlQueryMaster,
                sqlParamFilterByService, sqlParamFilterByTypeMed, sqlParamFilterByStateMed, sqlParamFilterByDateMed);

        List<Med> listMed = (List<Med>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Med.class), false);
        return listMed;
    }

    public static List<Med> getListInvitationApayerByAdvancedSearch(String codeService, String stateMed,
            String dateDebut, String dateFin, String codeSiteRec) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByService = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByTypeMed = " AND M.TYPE_MED = 'INVITATION_PAIEMENT'";
        String sqlParamFilterByStateMed = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByDateMed = GeneralConst.EMPTY_STRING;

        if (!codeService.equals(GeneralConst.ALL)) {

            sqlParamFilterByService = " AND M.FK_SITE_TAXATION = '%s' AND M.FK_SITE = '%s'";
            sqlParamFilterByService = String.format(sqlParamFilterByService, codeService.trim(), codeSiteRec);
        }

        switch (stateMed) {
            case GeneralConst.Number.TWO:
                sqlParamFilterByStateMed = " AND CONVERT(DATE,M.DATE_ECHEANCE,103) > CONVERT(DATE,GETDATE(),103)";
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByStateMed = " AND CONVERT(DATE,M.DATE_ECHEANCE,103) < CONVERT(DATE,GETDATE(),103)";
                break;
        }

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterByDateMed = " AND DBO.FDATE(M.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterByDateMed = String.format(sqlParamFilterByDateMed, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_MED_V2;

        sqlQueryMaster = String.format(sqlQueryMaster,
                sqlParamFilterByService, sqlParamFilterByTypeMed, sqlParamFilterByStateMed, sqlParamFilterByDateMed);

        List<Med> listMed = (List<Med>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Med.class), false);
        return listMed;
    }

    public static List<Med> getListInvitationServicePaiementByAdvancedSearch(String codeService, String stateMed,
            String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByService = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByTypeMed = " AND M.TYPE_MED = 'INVITATION_SERVICE'";
        String sqlParamFilterByStateMed = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByDateMed = GeneralConst.EMPTY_STRING;

        if (!codeService.equals(GeneralConst.ALL)) {

            sqlParamFilterByService = " AND M.FK_SITE = '%s'";
            sqlParamFilterByService = String.format(sqlParamFilterByService, codeService.trim());
        }

        switch (stateMed) {
            case GeneralConst.Number.TWO:
                sqlParamFilterByStateMed = " AND CONVERT(DATE,M.DATE_ECHEANCE,103) > CONVERT(DATE,GETDATE(),103)";
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByStateMed = " AND CONVERT(DATE,M.DATE_ECHEANCE,103) < CONVERT(DATE,GETDATE(),103)";
                break;
        }

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterByDateMed = " AND DBO.FDATE(M.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterByDateMed = String.format(sqlParamFilterByDateMed, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_MED_V2;

        sqlQueryMaster = String.format(sqlQueryMaster,
                sqlParamFilterByService, sqlParamFilterByTypeMed, sqlParamFilterByStateMed, sqlParamFilterByDateMed);

        List<Med> listMed = (List<Med>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Med.class), false);
        return listMed;
    }

    public static List<Med> getListInvitationApayerByAdvancedSearchService(String codeService, String stateMed,
            String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByService = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByTypeMed = " AND M.TYPE_MED = 'INVITATION_SERVICE'";
        String sqlParamFilterByStateMed = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByDateMed = GeneralConst.EMPTY_STRING;

        /*if (!codeService.equals(GeneralConst.ALL)) {

         sqlParamFilterByService = " AND M.ARTICLE_BUDGETAIRE IN (SELECT AB.CODE "
         + " FROM T_ARTICLE_BUDGETAIRE AB WITH (READPAST) "
         + " WHERE AB.ARTICLE_GENERIQUE IN (SELECT AG.CODE "
         + " FROM T_ARTICLE_GENERIQUE AG WITH (READPAST) WHERE AG.SERVICE_ASSIETTE = '%s'))";

         sqlParamFilterByService = String.format(sqlParamFilterByService, codeService.trim());
         }*/
        if (!codeService.equals(GeneralConst.ALL)) {

            sqlParamFilterByService = " AND M.FK_SITE = '%s'";
            sqlParamFilterByService = String.format(sqlParamFilterByService, codeService.trim());
        }

        switch (stateMed) {
            case GeneralConst.Number.TWO:
                sqlParamFilterByStateMed = " AND CONVERT(DATE,M.DATE_ECHEANCE,103) > CONVERT(DATE,GETDATE(),103)";
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByStateMed = " AND CONVERT(DATE,M.DATE_ECHEANCE,103) < CONVERT(DATE,GETDATE(),103)";
                break;
        }

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterByDateMed = " AND DBO.FDATE(M.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterByDateMed = String.format(sqlParamFilterByDateMed, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_MED_V2;

        sqlQueryMaster = String.format(sqlQueryMaster,
                sqlParamFilterByService, sqlParamFilterByTypeMed, sqlParamFilterByStateMed, sqlParamFilterByDateMed);

        List<Med> listMed = (List<Med>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Med.class), false);
        return listMed;
    }

    public static List<Amr> getListAmrBySimpleSearch(String typeSearch, String valueSearch) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilter = GeneralConst.EMPTY_STRING;

        switch (typeSearch) {
            case GeneralConst.Number.ONE:
                sqlParamFilter = " AND A.NUMERO = '%s' AND A.TYPE_AMR = 'AMR'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
            case GeneralConst.Number.TWO:
                sqlParamFilter = " AND A.FK_MED IN (SELECT M.ID FROM T_MED M WITH (READPAST) WHERE M.PERSONNE = '%s')";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
        }

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_AMR_V1;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilter);

        List<Amr> listAmr = (List<Amr>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Amr.class), false);
        return listAmr;
    }

    public static List<Amr> getListAmrTaxationofficeBySimpleSearch(String typeSearch, String valueSearch) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilter = GeneralConst.EMPTY_STRING;

        switch (typeSearch) {
            case GeneralConst.Number.ONE:
                sqlParamFilter = " AND A.NUMERO_DOCUMENT = '%s' AND A.TYPE_AMR = 'AMRR'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;

            case GeneralConst.Number.TWO:
                sqlParamFilter = " AND A.FK_MED IN (SELECT M.ID FROM T_MED M WITH (READPAST) WHERE M.PERSONNE = '%s')";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
        }

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_AMR_V1;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilter);

        List<Amr> listAmr = (List<Amr>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Amr.class), false);
        return listAmr;
    }

    public static List<Amr> getListAmrByAdvancedSearch(String codeService, String stateAmr,
            String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByService = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByTypeAmr = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByStateAmr = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByDateAmr = GeneralConst.EMPTY_STRING;

        switch (stateAmr) {
            case GeneralConst.Number.TWO:
                sqlParamFilterByStateAmr = " AND CONVERT(DATE,A.DATE_ECHEANCE,103) > CONVERT(DATE,GETDATE(),103)";
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByStateAmr = " AND CONVERT(DATE,A.DATE_ECHEANCE,103) < CONVERT(DATE,GETDATE(),103)";
                break;
        }

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterByDateAmr = " AND DBO.FDATE(A.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterByDateAmr = String.format(sqlParamFilterByDateAmr, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_AMR_INVITATION_A_PAYER;

        sqlParamFilterByService = " AND A.FK_MED IN (SELECT m.ID FROM T_MED m WITH (READPAST) WHERE m.FK_SITE = '%s')";
        sqlParamFilterByService = String.format(sqlParamFilterByService, codeService);

        sqlQueryMaster = String.format(sqlQueryMaster,
                sqlParamFilterByService, sqlParamFilterByTypeAmr, sqlParamFilterByStateAmr, sqlParamFilterByDateAmr);

        List<Amr> listAmr = (List<Amr>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Amr.class), false);
        return listAmr;
    }

    public static List<Amr> getListAmrTaxationOfficeByAdvancedSearch(String codeService, String stateAmr,
            String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByService = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByTypeAmr = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByStateAmr = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByDateAmr = GeneralConst.EMPTY_STRING;

        switch (stateAmr) {
            case GeneralConst.Number.TWO:
                sqlParamFilterByStateAmr = " AND CONVERT(DATE,A.DATE_ECHEANCE,103) > CONVERT(DATE,GETDATE(),103)";
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByStateAmr = " AND CONVERT(DATE,A.DATE_ECHEANCE,103) < CONVERT(DATE,GETDATE(),103)";
                break;
        }

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterByDateAmr = " AND DBO.FDATE(A.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterByDateAmr = String.format(sqlParamFilterByDateAmr, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_AMR_RELANCE;

        sqlQueryMaster = String.format(sqlQueryMaster,
                sqlParamFilterByService, sqlParamFilterByTypeAmr, sqlParamFilterByStateAmr, sqlParamFilterByDateAmr);

        List<Amr> listAmr = (List<Amr>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Amr.class), false);
        return listAmr;
    }

    public static Amr getAmrByNumero(String numero) {

        try {

            String query = "SELECT A.* FROM T_AMR A WITH (READPAST) WHERE A.NUMERO = ?1";

            List<Amr> amrs = (List<Amr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Amr.class), false, numero);

            return amrs.isEmpty() ? null : amrs.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static List<ParamAmr> getistAccountBankToCdfFromParamAmr() {

        try {

            //String query = "SELECT ID,COMPTE_DGRK_CDF FROM T_PARAM_AMR WHERE ETAT = 1";
            String query = "SELECT * FROM T_PARAM_AMR WHERE ETAT = 1";

            List<ParamAmr> paramAmrs = (List<ParamAmr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ParamAmr.class), false);

            return paramAmrs.isEmpty() ? null : paramAmrs;
        } catch (Exception e) {
            throw e;
        }

    }

    public static List<ParamAmr> getistAccountBankToUsdFromParamAmr() {

        try {

            //String query = "SELECT ID,COMPTE_DGRK_USD FROM T_PARAM_AMR WHERE ETAT = 1";
            String query = "SELECT * FROM T_PARAM_AMR WHERE ETAT = 1";

            List<ParamAmr> paramAmrs = (List<ParamAmr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ParamAmr.class), false);

            return paramAmrs.isEmpty() ? null : paramAmrs;
        } catch (Exception e) {
            throw e;
        }

    }

    public static String saveContrainteAndCommandement(String numeroAmr, String userId, String agentHuissier, String parquetName) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "CODE_RETURNS";

        String query = "F_NEW_CONTRAINTE_AND_COMMANDEMENT_V2";

        params.put("NUMERO_AMR", numeroAmr);
        params.put("AGENT", userId);
        params.put("AGNT_COMMANDEMENT", agentHuissier);

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

        if (!result.isEmpty()) {
            returnValue = result;
        }
        return returnValue;
    }

    public static String saveAtd(String contrainteId, String userId, String tierDetenteur) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "ATD_ID";

        String query = "F_NEW_ATD";

        params.put("ID_CONTRAINTE", contrainteId);
        params.put("ID_DENTENTEUR", tierDetenteur);
        params.put("AGENT_CREAT", Integer.valueOf(userId));

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

        if (!result.isEmpty()) {
            returnValue = result;
        }
        return returnValue;
    }

    public static Contrainte getContrainteByCode(String code) {

        try {
            String query = "SELECT C.* FROM T_CONTRAINTE C WITH (READPAST) WHERE C.ID = ?1";
            List<Contrainte> contraintes = (List<Contrainte>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Contrainte.class), false, code);

            return contraintes.isEmpty() ? null : contraintes.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Commandement getCommandementByCode(String code) {

        try {
            String query = "SELECT C.* FROM T_COMMANDEMENT C WITH (READPAST) WHERE C.ID = ?1";
            List<Commandement> commandements = (List<Commandement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commandement.class), false, code);

            return commandements.isEmpty() ? null : commandements.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Commandement getCommandementByContrainte(String codeContrainte) {

        try {
            String query = "SELECT C.* FROM T_COMMANDEMENT C WITH (READPAST) WHERE C.ID_CONTRAINTE = ?1";
            List<Commandement> commandements = (List<Commandement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commandement.class), false, codeContrainte);

            return commandements.isEmpty() ? null : commandements.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static BonAPayer getBonAPayerByCode(String code) {

        try {
            String query = "SELECT B.* FROM T_BON_A_PAYER B WITH (READPAST) WHERE B.CODE = ?1";
            List<BonAPayer> bonAPayers = (List<BonAPayer>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(BonAPayer.class), false, code);

            return bonAPayers.isEmpty() ? null : bonAPayers.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static ParamContrainte getParamContrainte() {

        try {
            String query = "SELECT PC.* FROM T_PARAM_CONTRAINTE PC WITH (READPAST) WHERE PC.ETAT = 1";
            List<ParamContrainte> paramContraintes = (List<ParamContrainte>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ParamContrainte.class), false);

            return paramContraintes.isEmpty() ? null : paramContraintes.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static ParamAmr getCompteBancaireToParamAmr() {

        try {
            String query = "SELECT PA.* FROM T_PARAM_AMR PA WITH (READPAST) WHERE PA.ETAT = 1";
            List<ParamAmr> paramAmrs = (List<ParamAmr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ParamAmr.class), false);

            return paramAmrs.isEmpty() ? null : paramAmrs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Contrainte> getListContrainteBySimpleSearch(String typeSearch, String valueSearch) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilter = GeneralConst.EMPTY_STRING;

        switch (typeSearch) {

            case GeneralConst.Number.ONE:

                sqlParamFilter = " AND C.NUMERO_DOCUMENT = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;

            case GeneralConst.Number.TWO:

                sqlParamFilter = " AND C.ID_AMR IN (SELECT A.NUMERO FROM T_AMR A WITH (READPAST) \n"
                        + "WHERE A.FICHE_PRISE_CHARGE IN (SELECT FPC.CODE FROM T_FICHE_PRISE_CHARGE FPC WITH (READPAST) \n"
                        + "WHERE FPC.PERSONNE = '%s'))";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;

        }

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_CONTRAINTE_V1;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilter);

        List<Contrainte> listContrainte = (List<Contrainte>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Contrainte.class), false);
        return listContrainte;
    }

    public static List<Contrainte> getListContrainteByAdvancedSearch(
            String codeService, String codeTypeContrainte, String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByService = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByTypeContrainte = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByDateContrainte = GeneralConst.EMPTY_STRING;

        switch (codeTypeContrainte) {
            case GeneralConst.Number.TWO:
                sqlParamFilterByTypeContrainte = "AND CONVERT(DATE,C.DATE_ECHEANCE,103) > CONVERT(DATE,GETDATE(),103)";
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByTypeContrainte = " AND CONVERT(DATE,C.DATE_ECHEANCE,103) < CONVERT(DATE,GETDATE(),103)";
                break;
        }

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterByDateContrainte = " AND DBO.FDATE(C.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterByDateContrainte = String.format(sqlParamFilterByDateContrainte, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_CONTRAINTE_V2;

        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilterByTypeContrainte, sqlParamFilterByDateContrainte);

        List<Contrainte> listContrainte = (List<Contrainte>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Contrainte.class), false);
        return listContrainte;
    }

    public static boolean executeQueryBulkUpdate(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }

    public static List<Commandement> getListCommandementBySimpleSearch(String typeSearch, String valueSearch) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilter = GeneralConst.EMPTY_STRING;

        switch (typeSearch) {
            case GeneralConst.Number.ONE:
                sqlParamFilter = " AND CMD.NUMERO_DOCUMENT = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
            case GeneralConst.Number.TWO:
                sqlParamFilter = " AND CMD.ID_CONTRAINTE IN (SELECT C.ID FROM T_CONTRAINTE C WITH (READPAST) WHERE \n"
                        + "C.ID_AMR IN (SELECT A.NUMERO FROM T_AMR A WITH (READPAST) \n"
                        + "WHERE A.FICHE_PRISE_CHARGE IN (SELECT FPC.CODE FROM T_FICHE_PRISE_CHARGE FPC WITH (READPAST)\n"
                        + "WHERE FPC.PERSONNE = '%s')))";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;

        }

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_COMMANDEMENT_V1;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilter);

        List<Commandement> listCommandement = (List<Commandement>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Commandement.class), false);
        return listCommandement;
    }

    public static List<Commandement> getListCommandementByAdvancedSearch(
            String codeService, String codeTypeCommandement, String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByService = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByTypeCommandement = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByDateCommandement = GeneralConst.EMPTY_STRING;

        switch (codeTypeCommandement) {
            case GeneralConst.Number.TWO:
                sqlParamFilterByTypeCommandement = "AND CONVERT(DATE,CMD.DATE_ECHEANCE,103) > CONVERT(DATE,GETDATE(),103)";
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByTypeCommandement = " AND CONVERT(DATE,CMD.DATE_ECHEANCE,103) < CONVERT(DATE,GETDATE(),103)";
                break;
        }

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterByDateCommandement = " AND DBO.FDATE(CMD.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterByDateCommandement = String.format(sqlParamFilterByDateCommandement, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_COMMANDEMENT_V2;

        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilterByTypeCommandement, sqlParamFilterByDateCommandement);

        List<Commandement> listCommandement = (List<Commandement>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Commandement.class), false);
        return listCommandement;
    }

    public static Atd getAtdByCommandementID(String cmdId) {

        try {

            String query = SQLQueryPoursuite.SELECT_ATD_BY_COMMANDEMENT;

            List<Atd> atds = (List<Atd>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Atd.class), false, cmdId);

            return atds.isEmpty() ? null : atds.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static Atd getAtdByID(String Id) {

        try {

            String query = SQLQueryPoursuite.SELECT_ATD_BY_ID;

            List<Atd> atds = (List<Atd>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Atd.class), false, Id);

            return atds.isEmpty() ? null : atds.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static List<Atd> getListAtdBySimpleSearch(String typeSearch, String valueSearch) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilter = GeneralConst.EMPTY_STRING;

        switch (typeSearch) {
            case GeneralConst.Number.ONE:
                sqlParamFilter = " AND A.ID = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
            case GeneralConst.Number.TWO:
                sqlParamFilter = " AND A.ID_COMMANDEMENT IN (SELECT CMD.ID FROM T_COMMANDEMENT CMD WITH (READPAST) WHERE CMD.ID_CONTRAINTE IN (SELECT C.ID FROM T_CONTRAINTE C WITH (READPAST) WHERE \n"
                        + "C.ID_AMR IN (SELECT A.NUMERO FROM T_AMR A WITH (READPAST) \n"
                        + "WHERE A.FICHE_PRISE_CHARGE IN (SELECT FPC.CODE FROM T_FICHE_PRISE_CHARGE FPC WITH (READPAST)\n"
                        + "WHERE FPC.PERSONNE = '%s'))))";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;

        }

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_ATD_V1;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilter);

        List<Atd> listAtd = (List<Atd>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Atd.class), false);
        return listAtd;
    }

    public static List<Atd> getListAtdByAdvancedSearch(String codeService, String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterDate = GeneralConst.EMPTY_STRING;

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterDate = " AND DBO.FDATE(A.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterDate = String.format(sqlParamFilterDate, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_ATD_V1;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilterDate);

        List<Atd> listAtd = (List<Atd>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Atd.class), false);
        return listAtd;
    }

    public static Integer checkRecidivisteDeclaration(String assujettisement, Integer periode) {

        Integer result = 0;

        String masterQuery = String.format(SQLQueryPoursuite.CHECK_RECIDIVISTE_DECLARATION, assujettisement, periode);

        try {
            result = myDao.getDaoImpl().getSingleResultToInteger(masterQuery);
        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static Integer checkRecidivisteRedressement(String assujettisement) {

        Integer result = 0;

        String masterQuery = String.format(SQLQueryPoursuite.CHECK_RECIDIVISTE_REDRESSEMENT, assujettisement);

        try {
            result = myDao.getDaoImpl().getSingleResultToInteger(masterQuery);
        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static List<FichePriseCharge> getListFichePriseChargeBySimpleSearch(String typeSearch, String valueSearch) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilter = GeneralConst.EMPTY_STRING;

        switch (typeSearch) {

            case GeneralConst.Number.ONE:
                sqlParamFilter = " AND FPC.CODE = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
            case GeneralConst.Number.TWO:
                sqlParamFilter = " AND FPC.PERSONNE = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
        }

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_FICHE_PRISE_CHARGE_V1;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilter);

        List<FichePriseCharge> listFichePriseCharge = (List<FichePriseCharge>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(FichePriseCharge.class), false);
        return listFichePriseCharge;
    }

    public static List<FichePriseCharge> getListFichePriseChargeByAdvancedSearch(
            String codeService, String codeTypeContrainte, String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByTypeFpc = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByDateFpc = GeneralConst.EMPTY_STRING;

        sqlParamFilterByTypeFpc = " WHERE  FPC.ETAT IN (0,1,2,3)";

        switch (codeTypeContrainte) {
            case GeneralConst.Number.ONE:
                sqlParamFilterByTypeFpc = " WHERE FPC.ETAT = 3"; // Waitring
                break;
            case GeneralConst.Number.TWO:
                sqlParamFilterByTypeFpc = " WHERE FPC.ETAT = 2"; // Validate
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByTypeFpc = " WHERE FPC.ETAT = 0"; // Reject
                break;
        }

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterByDateFpc = " AND DBO.FDATE(FPC.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterByDateFpc = String.format(sqlParamFilterByDateFpc, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_FICHE_PRISE_CHARGE_V2;

        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilterByTypeFpc, sqlParamFilterByDateFpc);

        List<FichePriseCharge> listFichePriseCharge = (List<FichePriseCharge>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(FichePriseCharge.class), false);
        return listFichePriseCharge;
    }

    public static Penalite getPenaliteByID(String Id) {

        try {

            String query = "SELECT P.* FROM T_PENALITE P WITH (READPAST) WHERE P.CODE = ?1";

            List<Penalite> penalites = (List<Penalite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Penalite.class), false, Id);

            return penalites.isEmpty() ? null : penalites.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static Boolean saveAmrOfInvitationApayer(String med, int userId, String valueMotif, String valueDetailMotif,String noteTaxation) {

        boolean result = false;
        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int count = 0;

        try {

            count++;
            String sqlQuery = ":EXEC F_NEW_AMR_INVITATION_A_PAYER_V2 ?1,?2,?3,?4,?5";
            firstBulk.put(count + sqlQuery, new Object[]{
                med,
                userId,
                valueDetailMotif,
                valueMotif,
                noteTaxation
            });

            result = executeQueryBulkUpdate(firstBulk);

        } catch (Exception e) {
        }

        return result;
    }

    public static Boolean traiterFichePriseCharge(String value, String idFpc, String typeFpc, String userId) {

        boolean result = false;
        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int count = 0;

        String sqlCreateAmr1 = GeneralConst.EMPTY_STRING;
        String sqlCreateAmr2 = GeneralConst.EMPTY_STRING;

        try {

            String sqlUpdateFiche = ":UPDATE T_FICHE_PRISE_CHARGE SET ETAT = %s WHERE CODE = ?1";
            sqlUpdateFiche = String.format(sqlUpdateFiche, value);

            firstBulk.put(count + sqlUpdateFiche, new Object[]{idFpc});

            if (value.equals(GeneralConst.Number.TWO)) {

                FichePriseCharge fpc = getFichePriseChargeByID(idFpc);

                if (fpc != null) {

                    switch (typeFpc) {

                        //case "NP":
                        case "2":

                            count++;
                            sqlCreateAmr2 = ":EXEC F_NEW_AMR22 ?1,?2,?3,?4,?5,?6";
                            firstBulk.put(count + sqlCreateAmr2, new Object[]{
                                fpc.getTotalPenalitedu(),
                                null,
                                userId,
                                fpc.getNumeroReference(),
                                new Date(),
                                fpc.getCode()
                            });

                            break;

                        //case "TO":
                        case "3":

                            count++;

                            sqlCreateAmr2 = ":EXEC F_NEW_AMR12 ?1,?2,?3,?4,?5,?6";
                            firstBulk.put(count + sqlCreateAmr2, new Object[]{
                                fpc.getTotalPrincipaldu(),
                                null,
                                userId,
                                fpc.getNumeroReference(),
                                new Date(),
                                fpc.getCode()
                            });

                            count++;

                            sqlCreateAmr2 = ":EXEC F_NEW_AMR22 ?1,?2,?3,?4,?5,?6";
                            firstBulk.put(count + sqlCreateAmr2, new Object[]{
                                fpc.getTotalPenalitedu(),
                                null,
                                userId,
                                fpc.getNumeroReference(),
                                new Date(),
                                fpc.getCode()
                            });

                            break;

                        case "1":

                            count++;

                            sqlCreateAmr2 = ":EXEC F_NEW_AMR22 ?1,?2,?3,?4,?5,?6";
                            firstBulk.put(count + sqlCreateAmr2, new Object[]{
                                fpc.getTotalPenalitedu(),
                                null,
                                userId,
                                fpc.getNumeroReference(),
                                new Date(),
                                fpc.getCode()
                            });

                            break;

                        default:

                            break;
                    }
                }

            }

            result = executeQueryBulkUpdate(firstBulk);

        } catch (Exception e) {
        }

        return result;
    }

    public static List<CompteBancaire> getListCompteBancaireToParamAmr(String param) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        sqlQueryMaster = "SELECT C.* FROM T_COMPTE_BANCAIRE C WITH (READPAST) WHERE C.CODE IN (%s)";
        sqlQueryMaster = String.format(sqlQueryMaster, param);

        List<CompteBancaire> listCompteBancaire = (List<CompteBancaire>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(CompteBancaire.class), false);
        return listCompteBancaire;
    }

    public static FichePriseCharge getFichePriseChargeByID(String Id) {

        try {

            String query = "SELECT * FROM T_FICHE_PRISE_CHARGE WITH (READPAST) WHERE CODE = ?1";

            List<FichePriseCharge> fichePriseCharges = (List<FichePriseCharge>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(FichePriseCharge.class), false, Id);

            return fichePriseCharges.isEmpty() ? null : fichePriseCharges.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static Amr getAmrTwoByFichePriseCharge(String fpcId) {

        try {

            String query = "SELECT * FROM T_AMR WITH (READPAST) WHERE FICHE_PRISE_CHARGE = ?1 AND TYPE_AMR = 'AMR2'";

            List<Amr> amrs = (List<Amr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Amr.class), false, fpcId);

            return amrs.isEmpty() ? null : amrs.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static Amr getAmrOneByFichePriseCharge(String fpcId) {

        try {

            String query = "SELECT * FROM T_AMR WITH (READPAST) WHERE FICHE_PRISE_CHARGE = ?1 AND TYPE_AMR = 'AMR1'";

            List<Amr> amrs = (List<Amr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Amr.class), false, fpcId);

            return amrs.isEmpty() ? null : amrs.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static List<Amr> getListAmrTwoByFichePriseCharge(String param) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        sqlQueryMaster = "SELECT * FROM T_AMR WITH (READPAST) WHERE FICHE_PRISE_CHARGE = ?1";
        sqlQueryMaster = String.format(sqlQueryMaster, param);

        List<Amr> listAmr = (List<Amr>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Amr.class), false);
        return listAmr;
    }

    public static List<BonAPayer> getListBonAPayerBySimpleSearch(String typeSearch, String valueSearch) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilter = GeneralConst.EMPTY_STRING;

        switch (typeSearch) {

            case GeneralConst.Number.ONE:
                sqlParamFilter = " AND BP.CODE = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
            case GeneralConst.Number.TWO:
                sqlParamFilter = " AND BP.FK_PERSONNE = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilter = " AND BP.FK_AMR = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
        }

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_BON_A_PAYER_V1;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilter);

        List<BonAPayer> listBonAPayer = (List<BonAPayer>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(BonAPayer.class), false);
        return listBonAPayer;
    }

    public static List<BonAPayer> getListBonAPayerByAdvancedSearch(
            String codeService, String codeType, String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByTypeBap = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByDateBap = GeneralConst.EMPTY_STRING;

        sqlParamFilterByTypeBap = " WHERE BP.ETAT IN (0,1,2,3,4,5)";

        switch (codeType) {
            case GeneralConst.Number.ONE:
                sqlParamFilterByTypeBap = " WHERE BP.ETAT = 2"; // Waitring
                break;
            case GeneralConst.Number.TWO:
                sqlParamFilterByTypeBap = " WHERE BP.ETAT = 1"; // Validate
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByTypeBap = " WHERE BP.ETAT = 0"; // Reject
                break;
        }

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterByDateBap = " AND DBO.FDATE(BP.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterByDateBap = String.format(sqlParamFilterByDateBap, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_BON_A_PAYER_V2;

        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilterByTypeBap, sqlParamFilterByDateBap);

        List<BonAPayer> listBonAPayer = (List<BonAPayer>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(BonAPayer.class), false);
        return listBonAPayer;
    }

    public static Integer checkRecidivistePaiement(String codeArticlebudgetaire, String codeAssujetti) {

        Integer result = 0;
        String sqlQuery = "SELECT dbo.F_GET_RECIDIVISTE('%s', '%s')";

        String masterQuery = String.format(sqlQuery, codeArticlebudgetaire, codeAssujetti);

        try {
            result = myDao.getDaoImpl().getSingleResultToInteger(masterQuery);
        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static Integer checkRecidivisteDeclaration(String codeAssujetissement, int periodeCode) {

        Integer result = 0;
        String sqlQuery = "SELECT dbo.F_GET_RECIDIVISTE_DECLARATION('%s', %s)";

        String masterQuery = String.format(sqlQuery, codeAssujetissement, periodeCode);

        try {
            result = myDao.getDaoImpl().getSingleResultToInteger(masterQuery);
        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static FichePriseCharge getFichePriseChargeByMed(String medCode) {

        try {
            String query = "SELECT fpc.code,fpc.total_penalitedu FROM t_fiche_prise_charge fpc WITH (READPAST) WHERE fpc.FK_MED = ?1";
            List<FichePriseCharge> fichePriseCharges = (List<FichePriseCharge>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(FichePriseCharge.class), false, medCode);

            return fichePriseCharges.isEmpty() ? null : fichePriseCharges.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Amr getAmrByMed(String medCode) {

        try {
            String query = "SELECT a.NUMERO,FK_MED FROM T_AMR a WITH (READPAST) WHERE a.FK_MED = ?1";
            List<Amr> amrs = (List<Amr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Amr.class), false, medCode);

            return amrs.isEmpty() ? null : amrs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Amr getAmrByMedAll(String medCode) {

        try {
            String query = "SELECT * FROM T_AMR a WITH (READPAST) WHERE a.FK_MED = ?1";
            List<Amr> amrs = (List<Amr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Amr.class), false, medCode);

            return amrs.isEmpty() ? null : amrs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static DetailsRole getDetailRoleByMed(String medCode) {

        try {
            String query = "select * from T_DETAILS_ROLE WITH (READPAST) WHERE FK_MED = ?1";
            List<DetailsRole> detailsRoles = (List<DetailsRole>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsRole.class), false, medCode);

            return detailsRoles.isEmpty() ? null : detailsRoles.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Amr getAmrByMedV2(String medCode) {

        try {
            String query = "SELECT a.* FROM T_AMR a WITH (READPAST) WHERE a.FK_MED = ?1";
            List<Amr> amrs = (List<Amr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Amr.class), false, medCode);

            return amrs.isEmpty() ? null : amrs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static String createFichePriseCharge(FichePriseCharge fpc,
            List<DetailPenaliteMock> detailPenaliteMocks, String numeroNc) {

        PropertiesMessage propertiesMessage = null;
        String result = GeneralConst.EMPTY_STRING;

        try {
            propertiesMessage = new PropertiesMessage();
        } catch (IOException ex) {
            Logger.getLogger(TaxationBusiness.class.getName()).log(Level.SEVERE, null, ex);
        }

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int counter = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> paramsFpc = new HashMap<>();

        String firstStoredProcedure = "F_NEW_FPC_WEB";
        String firstStoredReturnValueKey = "CODE_FPC";

        paramsFpc.put("TOTAL_PRINCIPAL_DU", fpc.getTotalPrincipaldu());
        paramsFpc.put("TOTAL_PENALITE_DU", fpc.getTotalPenalitedu());
        paramsFpc.put("PERSONNE", fpc.getPersonne().getCode());
        paramsFpc.put("DEVISE", fpc.getDevise());
        paramsFpc.put("NUMERO_REFERENCE", fpc.getNumeroReference());
        paramsFpc.put("CAS_FPC", fpc.getCasFpc());
        paramsFpc.put("ARTICLE_BUDGETAIRE", fpc.getFkArticleBudgetaire());
        paramsFpc.put("MED", fpc.getFkMed());

        for (DetailPenaliteMock dpm : detailPenaliteMocks) {

            /*BigDecimal amountDu = new BigDecimal("0");
             amountDu = amountDu.add(dpm.getAmountPrincipal());
             amountDu = amountDu.add(dpm.getAmountPenalite());*/
            counter++;
            String sqlQuery = ":EXEC F_NEW_DETAIL_FPC_WEB '%s',?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11";

            if (numeroNc.isEmpty()) {

                firstBulk.put(counter + sqlQuery, new Object[]{
                    dpm.getAmountPrincipal(),
                    dpm.getAmountPenalite(),
                    dpm.getTypeTaux(),
                    dpm.getTaux(),
                    null,//amountDu,
                    dpm.getCodePenalite(),
                    dpm.getNumberMonth(),
                    Integer.valueOf(dpm.getReference()),
                    null,
                    dpm.getAmountPrincipal(),
                    dpm.getExercice(),});
            } else {
                firstBulk.put(counter + sqlQuery, new Object[]{
                    dpm.getAmountPrincipal(),
                    dpm.getAmountPenalite(),
                    dpm.getTypeTaux(),
                    dpm.getTaux(),
                    null,//amountDu,
                    dpm.getCodePenalite(),
                    dpm.getNumberMonth(),
                    0,
                    numeroNc,
                    dpm.getAmountPrincipal(),
                    dpm.getExercice()
                });
            }

        }

        result = myDao.getDaoImpl().execBulkQueryv4(
                firstStoredProcedure, firstStoredReturnValueKey, paramsFpc, firstBulk);

        return result;
    }

    public static List<Med> getListMedBySearch(String codeService) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        String dateDebut = ConvertDate.formatDateToString(new Date());
        String dateFin = ConvertDate.formatDateToString(new Date());

        dateDebut = dateDebut.replaceAll("/", "");
        dateFin = dateFin.replaceAll("/", "");

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_MEP;

        List<Med> listMed = (List<Med>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Med.class), false, codeService);
        return listMed;
    }

    public static List<Agent> getistAgentHuissier(String codeFonctionHuissier) {

        try {

            String query = "SELECT * FROM T_AGENT A WITH (READPAST) WHERE A.FONCTION = ?1 AND A.ETAT = 1 ORDER BY A.PRENOMS,A.NOM";

            List<Agent> agents = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, codeFonctionHuissier);

            return agents.isEmpty() ? null : agents;
        } catch (Exception e) {
            throw e;
        }

    }

    public static List<Amr> getListAmrByAdvancedSearch(String codeService, String typeAmr, String stateAmr,
            String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByService = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByTypeAmr = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByStateAmr = GeneralConst.EMPTY_STRING;
        String sqlParamFilterByDateAmr = GeneralConst.EMPTY_STRING;

        switch (typeAmr) {
            case GeneralConst.Number.TWO:
                sqlParamFilterByTypeAmr = " AND A.TYPE_AMR = 'AMR1'";
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByTypeAmr = " AND A.TYPE_AMR = 'AMR2'";
                break;
        }

        switch (stateAmr) {
            case GeneralConst.Number.TWO:
                sqlParamFilterByStateAmr = " AND CONVERT(DATE,A.DATE_ECHEANCE,103) > CONVERT(DATE,GETDATE(),103)";
                break;
            case GeneralConst.Number.THREE:
                sqlParamFilterByStateAmr = " AND CONVERT(DATE,A.DATE_ECHEANCE,103) < CONVERT(DATE,GETDATE(),103)";
                break;
            case GeneralConst.Number.FOUR:
                sqlParamFilterByStateAmr = " AND A.SOLDE = 0";
                break;
            case GeneralConst.Number.FIVE:
                sqlParamFilterByStateAmr = " AND A.EN_CONTENTIEUX = 1";
                break;
            case GeneralConst.Number.SIX:
                sqlParamFilterByStateAmr = " AND A.FRACTIONNER = 1";
                break;
            case GeneralConst.Number.SEVEN:
                sqlParamFilterByStateAmr = "";
                break;
        }

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        sqlParamFilterByDateAmr = " AND DBO.FDATE(A.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamFilterByDateAmr = String.format(sqlParamFilterByDateAmr, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryPoursuite.SELECT_LIST_AMR_V2;

        sqlQueryMaster = String.format(sqlQueryMaster,
                sqlParamFilterByService, sqlParamFilterByTypeAmr, sqlParamFilterByStateAmr, sqlParamFilterByDateAmr);

        List<Amr> listAmr = (List<Amr>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Amr.class), false);
        return listAmr;
    }

    public static String saveContrainte(String numeroAmr, String userId, String numberMonth) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "CODE_RETURNS";

        String query = "F_NEW_CONTRAINTE_AND_COMMANDEMENT";

        params.put("NUMERO_AMR", numeroAmr);
        params.put("AGENT", userId);
        params.put("NOMBRE_MOIS_RETARD", Integer.valueOf(numberMonth));

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

        if (!result.isEmpty()) {
            returnValue = result;
        }
        return returnValue;
    }

    public static Boolean saveNoteTaxationPenaliteOfInvitationAPayer(Med med, RetraitDeclaration retraitDeclarationPrinciapl,
            String userId, String banque, String compteBancaireProvince, String compteBancaireDrhKat) {

        boolean result = false;
        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int count = 0;

        try {
            String sqlQuery = "";

            sqlQuery = ":INSERT INTO T_RETRAIT_DECLARATION (REQUERANT,FK_ASSUJETTI,CODE_DECLARATION,FK_AB,FK_PERIODE,"
                    + " FK_AGENT_CREATE,DATE_CREATE,MONTANT,DEVISE,ETAT,EST_PENALISE,RETRAIT_DECLARATION_MERE,"
                    + " DATE_ECHEANCE_PAIEMENT,FK_BANQUE,FK_COMPTE_BANCAIRE,FK_SITE,NEW_ID) "
                    + " VALUES (?1,?2,?3,?4,?5,?6,GETDATE(),?7,?8,?9,?10,?11,(SELECT DATEADD(DAY,3,GETDATE())),?12,?13,?14,?15)";

            Agent agentRecouvrement = GeneralBusiness.getAgentByCode(userId);

            BigDecimal amount = new BigDecimal(0);
            BigDecimal _deviser = new BigDecimal(2);

            amount = amount.add(med.getMontantPenalite().divide(_deviser));

            firstBulk.put(count + sqlQuery, new Object[]{
                retraitDeclarationPrinciapl.getRequerant(),
                retraitDeclarationPrinciapl.getFkAssujetti(),
                retraitDeclarationPrinciapl.getCodeDeclaration() + "-1",
                retraitDeclarationPrinciapl.getFkAb(),
                retraitDeclarationPrinciapl.getFkPeriode(),
                userId, amount, retraitDeclarationPrinciapl.getDevise(), 4, 0,
                retraitDeclarationPrinciapl.getId(),
                banque, compteBancaireProvince,
                agentRecouvrement.getSite().getCode(),
                retraitDeclarationPrinciapl.getNewId()
            });

            count++;

            firstBulk.put(count + sqlQuery, new Object[]{
                retraitDeclarationPrinciapl.getRequerant(),
                retraitDeclarationPrinciapl.getFkAssujetti(),
                retraitDeclarationPrinciapl.getCodeDeclaration() + "-2",
                retraitDeclarationPrinciapl.getFkAb(),
                retraitDeclarationPrinciapl.getFkPeriode(),
                userId, amount, retraitDeclarationPrinciapl.getDevise(), 4, 0,
                retraitDeclarationPrinciapl.getId(),
                banque, compteBancaireDrhKat,
                agentRecouvrement.getSite().getCode(),
                retraitDeclarationPrinciapl.getNewId()

            });

            count++;

            firstBulk.put(count + ":UPDATE T_RETRAIT_DECLARATION SET EST_PENALISE = 1 WHERE ID = ?1", new Object[]{
                retraitDeclarationPrinciapl.getId()
            });

            result = executeQueryBulkUpdate(firstBulk);

            RetraitDeclaration rd1 = NotePerceptionBusiness.getRetraitDeclarationByCodeDeclaration(
                    retraitDeclarationPrinciapl.getCodeDeclaration() + "-1");

            RetraitDeclaration rd2 = NotePerceptionBusiness.getRetraitDeclarationByCodeDeclaration(
                    retraitDeclarationPrinciapl.getCodeDeclaration() + "-2");

            firstBulk = new HashMap<>();
            count = 0;

            firstBulk.put(count + ":UPDATE T_RETRAIT_DECLARATION SET CODE_DECLARATION = ?1 WHERE ID = ?2", new Object[]{
                rd1.getId(), rd1.getId()
            });

            count++;

            firstBulk.put(count + ":UPDATE T_RETRAIT_DECLARATION SET CODE_DECLARATION = ?1 WHERE ID = ?2", new Object[]{
                rd2.getId(), rd2.getId()
            });

            result = executeQueryBulkUpdate(firstBulk);

        } catch (Exception e) {
        }

        return result;
    }

    public static Boolean saveNoteTaxationPrincipalAndPenaliteOfInvitationService(Med med,
            String userId, String codeBanque, String compteBancaire, String compteBancairePenalite, String devise1, String devise2, String newId) {

        boolean result = false;
        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int count = 0;

        try {

            Agent agentRecouvrement = GeneralBusiness.getAgentByCode(userId);
            Personne personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());

            PeriodeDeclaration periodeDeclaration = RecouvrementBusiness.getPeriodeDeclarationById(med.getPeriodeDeclaration());

            Assujeti assujeti = periodeDeclaration.getAssujetissement();

            String sqlQuery = "";

            sqlQuery = ":INSERT INTO T_RETRAIT_DECLARATION (REQUERANT,FK_ASSUJETTI,CODE_DECLARATION,FK_AB,FK_PERIODE,"
                    + " FK_AGENT_CREATE,DATE_CREATE,MONTANT,DEVISE,ETAT,EST_PENALISE,RETRAIT_DECLARATION_MERE,"
                    + " DATE_ECHEANCE_PAIEMENT,FK_BANQUE,FK_COMPTE_BANCAIRE,FK_SITE,NEW_ID) "
                    + " VALUES (?1,?2,?3,?4,?5,?6,GETDATE(),?7,?8,?9,?10,?11,(SELECT DATEADD(DAY,3,GETDATE())),?12,?13,?14,?15)";

            BigDecimal amountPrincipal = new BigDecimal("0");
            BigDecimal amountPenalite = new BigDecimal("0");

            amountPrincipal = amountPrincipal.add(med.getMontant());
            amountPenalite = amountPenalite.add(med.getMontantPenalite());

            //if(!med.get)
            firstBulk.put(count + sqlQuery, new Object[]{
                personne.toString().toUpperCase(),
                personne.getCode(),
                "",
                med.getArticleBudgetaire(),
                med.getPeriodeDeclaration(),
                userId, amountPrincipal, devise1, 1, 1,
                null,
                codeBanque, compteBancaire,
                agentRecouvrement.getSite().getCode(),
                newId
            });

            count++;

            firstBulk.put(count + sqlQuery, new Object[]{
                personne.toString().toUpperCase(),
                personne.getCode(),
                "",
                med.getArticleBudgetaire(),
                med.getPeriodeDeclaration(),
                userId, amountPenalite, devise1, 4, 0,
                null,
                codeBanque, compteBancairePenalite,
                agentRecouvrement.getSite().getCode(),
                newId
            });

            result = executeQueryBulkUpdate(firstBulk);

            RetraitDeclaration rd1 = NotePerceptionBusiness.getRetraitDeclarationPrincipalByNewid(newId);

            RetraitDeclaration rd2 = NotePerceptionBusiness.getRetraitDeclarationPenaliteByNewid(newId);

            firstBulk = new HashMap<>();
            count = 0;

            firstBulk.put(count + ":UPDATE T_RETRAIT_DECLARATION SET CODE_DECLARATION = ?1 WHERE ID = ?2", new Object[]{
                rd1.getId(), rd1.getId()
            });

            count++;

            firstBulk.put(count + ":UPDATE T_RETRAIT_DECLARATION SET CODE_DECLARATION = ?1,RETRAIT_DECLARATION_MERE = ?2 WHERE ID = ?3", new Object[]{
                rd2.getId(), rd1.getId(), rd2.getId()
            });

            result = executeQueryBulkUpdate(firstBulk);

        } catch (Exception e) {
        }

        return result;
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationPenaliteByPricipal(int pricipalID) {

        String sqlQuery = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE RETRAIT_DECLARATION_MERE = ?1";

        List<RetraitDeclaration> listRetraitDeclaration = (List<RetraitDeclaration>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, pricipalID);
        return listRetraitDeclaration;
    }

    public static Boolean saveMedPeriodeDeclaration(String medID, List<MedPeriodeDeclaration> listMedPeriodeDeclaration) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;
        int counter = 0;

        try {

            for (MedPeriodeDeclaration pd : listMedPeriodeDeclaration) {

                String query = ":INSERT INTO T_MED_PERIODE_DECLARATION (FK_MED,FK_PERIODE_DECLARATION,PRINCIPAL_DU,PENALITE_DU,ETAT) VALUES (?1,?2,?3,?4,1)";

                counter++;
                bulkQuery.put(counter + query, new Object[]{
                    medID,
                    pd.getFkPeriodeDeclaration(),
                    pd.getPrincipalDu(),
                    pd.getPenaliteDu()

                });

            }

            String sqlParamUpdate = ":UPDATE T_MED SET PERIODE_DECLARATION = NULL WHERE ID = ?1";
            counter++;
            bulkQuery.put(counter + sqlParamUpdate, new Object[]{
                medID
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean updateMedPeriodeDeclaration(String medID, List<MedPeriodeDeclaration> listMedPeriodeDeclaration) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;
        int counter = 0;

        try {

            for (MedPeriodeDeclaration pd : listMedPeriodeDeclaration) {

                String query = ":UPDATE T_MED_PERIODE_DECLARATION SET PRINCIPAL_DU = ?1, PENALITE_DU = ?2 WHERE FK_PERIODE_DECLARATION = ?3";

                counter++;
                bulkQuery.put(counter + query, new Object[]{
                    pd.getPrincipalDu(),
                    pd.getPenaliteDu(),
                    pd.getFkPeriodeDeclaration(),});

            }

            String sqlParamUpdate = ":UPDATE T_MED SET PERIODE_DECLARATION = NULL WHERE ID = ?1";
            counter++;
            bulkQuery.put(counter + sqlParamUpdate, new Object[]{
                medID
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }
}
