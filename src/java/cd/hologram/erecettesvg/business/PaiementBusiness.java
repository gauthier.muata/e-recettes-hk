/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.PaiementConst;
import cd.hologram.erecettesvg.sql.SQLQueryPaiement;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.util.Casting;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.CustumException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author bonheur.muntasomo
 */
public class PaiementBusiness {

    private static final Dao myDao = new Dao();

    public static List<Journal> getListPaymentJournal(int typeSearch, String valueSearch, int page, boolean rigthUser, String myListCode) {

        try {
            String sqlSecondQuery = GeneralConst.EMPTY_STRING;
            boolean isLike = false;
            String etat = GeneralConst.EMPTY_STRING;

            switch (page) {
                case 0:
                    etat = " IN (1,2,3,4,5) ";
                    break;
                case 1:
                    etat = " IN (2) ";
                    break;
                case 2:
                    etat = " IN (5) ";
                    break;
                case 3:
                    etat = " IN (1) ";
                    break;
                case 4:
                    etat = " IN (4) ";
                    break;
            }

            switch (typeSearch) {
                case 1:
                    if (rigthUser) {
                        sqlSecondQuery = etat + SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_SECONDE_QUERY_ONE + GeneralConst.SPACE
                                + SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_SECONDE_QUERY_TWO_LIST;
                    } else {
                        sqlSecondQuery = etat + SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_SECONDE_QUERY_ONE;
                    }
                    isLike = true;
                    break;
                case 2:
                    if (rigthUser) {
                        sqlSecondQuery = etat + SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_SECONDE_QUERY_TWO + GeneralConst.SPACE
                                + SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_SECONDE_QUERY_TWO_LIST;
                    } else {
                        sqlSecondQuery = etat + SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_SECONDE_QUERY_TWO;
                    }
                    isLike = false;
                    break;
            }

            String sqlMasterQuery = String.format(SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_MASTER_QUERY, sqlSecondQuery);

            List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(sqlMasterQuery,
                    Casting.getInstance().convertIntoClassType(Journal.class), isLike, valueSearch, myListCode);
            return journals;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Etat getEtatByCode(int code) {
        try {

            String query = SQLQueryPaiement.SELECT_ETAT_BY_CODE;

            List<Etat> etats = (List<Etat>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Etat.class), false, code);
            return etats.isEmpty() ? null : etats.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Agent getAgentByCode(int code) {
        try {

            String query = SQLQueryPaiement.SELECT_AGENT_BY_ID;

            List<Agent> agents = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, code);
            return agents.isEmpty() ? null : agents.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Journal> getListPaymentByAdvencedJournal(String site, String banque,
            String compteBancaire, String periodeDebut, String Periodefin, int page) {

        try {
            String sqlSecondQuery = GeneralConst.EMPTY_STRING;

            boolean isLike = false;

            String etat = GeneralConst.EMPTY_STRING;

            switch (page) {
                case 0:
                    etat = " IN (1,2,3,4,5) ";
                    break;
                case 1:
                    etat = " IN (2) ";
                    break;
                case 2:
                    etat = " IN (5) ";
                    break;
                case 3:
                    etat = " IN (1) ";
                    break;
                case 4:
                    etat = " IN (4) ";
                    break;
            }

            String sqlMasterQuery;

            if (site.equals(GeneralConst.ALL) && banque.equals(GeneralConst.ALL)) {
                sqlSecondQuery = etat + SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY1;

            } else if (!site.equals(GeneralConst.ALL) && banque.equals(GeneralConst.ALL)) {
                sqlSecondQuery = etat + (String.format(SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY1S,
                        SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY2));
            } else if (!compteBancaire.equals(GeneralConst.ALL) && !site.equals(GeneralConst.ALL)) {
                sqlSecondQuery = etat + (String.format(SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY1S,
                        SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY2 + GeneralConst.SPACE + SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY3));
            } else if (!compteBancaire.equals(GeneralConst.ALL) && site.equals(GeneralConst.ALL)) {
                sqlSecondQuery = etat + (String.format(SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY1S,
                        SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY3));
            } else if (compteBancaire.equals(GeneralConst.ALL) && !site.equals(GeneralConst.ALL) && !banque.equals(GeneralConst.ALL)) {
                sqlSecondQuery = etat + (String.format(SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY1 + GeneralConst.SPACE + SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY2S,
                        SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY4));
            } else if (compteBancaire.equals(GeneralConst.ALL) && site.equals(GeneralConst.ALL) && !banque.equals(GeneralConst.ALL)) {
                sqlSecondQuery = etat + (String.format(SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY1S,
                        SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_ADVENCED_QUERY4));
            }

            sqlMasterQuery = String.format(SQLQueryPaiement.LIST_PAIEMENT_JOURNAL_MASTER_QUERY, sqlSecondQuery);
            List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(sqlMasterQuery,
                    Casting.getInstance().convertIntoClassType(Journal.class), isLike, periodeDebut, Periodefin, site, compteBancaire, banque);
            return journals;
        } catch (Exception e) {
            throw e;
        }
    }

    int counter = 0;
    HashMap<String, Object[]> bulkQuery = new HashMap<>();

    public String validationApurementAdmin(String codeJournal, String reference, String agentApurement,
            String montantApure, String numeroReleveBancaire, String dateReleveBancaire) {

        NotePerception notePeception = new NotePerception();
        Amr amr = new Amr();
        BonAPayer bap = new BonAPayer();

        String numeroNC = GeneralConst.EMPTY_STRING;
        String CodePersonne = null;

        String chaineApurement = "Apurement";
        String observationReussie = "Apurement administratif a reussi";
        float debut = 0;
        float surplus = 0;

        try {

            Journal journal = getJournalByCode(codeJournal.trim());

            if (journal != null) {
                counter++;

                bulkQuery.put(counter + SQLQueryPaiement.F_APPUREMENT_JOURNAL, new Object[]{
                    codeJournal.trim(),
                    reference.trim(),
                    agentApurement,
                    montantApure,
                    numeroReleveBancaire.trim(), dateReleveBancaire.trim()
                });

                switch (journal.getTypeDocument().trim()) {

                    case PaiementConst.ParamNameDocument.NP:
                    case PaiementConst.ParamNameDocument.DEC:

                        notePeception = getNotePerceptionByCode(journal.getDocumentApure());

                        if (notePeception != null) {
                            numeroNC = notePeception.getNoteCalcul().getNumero().trim();
                            CodePersonne = notePeception.getNoteCalcul().getPersonne().trim();
                            surplus = (journal.getMontantPercu().floatValue()
                                    - notePeception.getNetAPayer().floatValue());
                        }

                        break;

                    case PaiementConst.ParamNameDocument.AMR_ONE:
                    case PaiementConst.ParamNameDocument.AMR_TWO:
                    case PaiementConst.ParamNameDocument.AMR:

                        amr = NotePerceptionBusiness.getAmrByCode(journal.getDocumentApure());

                        if (amr != null) {
                            numeroNC = amr.getFichePriseCharge().getDetailFichePriseChargeList().get(0).getNc();
                            CodePersonne = amr.getFichePriseCharge().getPersonne().getCode().trim();
                            surplus = (journal.getMontantPercu().floatValue()
                                    - amr.getNetAPayer().floatValue());
                        }

                        break;

                    case PaiementConst.ParamNameDocument.BP:

                        bap = getBonAPayerByCode(journal.getDocumentApure());

                        if (bap != null) {
                            numeroNC = bap.getFkAmr().getFichePriseCharge().getDetailFichePriseChargeList().get(0).getNc();
                            CodePersonne = bap.getFkPersonne().getCode().trim();
                            surplus = (journal.getMontantPercu().floatValue()
                                    - bap.getMontant().floatValue());
                        }
                        break;
                }

                if (surplus > 0) {

                    counter++;

                    bulkQuery.put(counter + SQLQueryPaiement.F_NEW_JDOSSIER_V2, new Object[]{
                        CodePersonne,
                        journal.getDocumentApure().trim(),
                        journal.getTypeDocument(),
                        surplus,
                        journal.getDevise(),
                        agentApurement
                    });
                }

            }

            counter++;

            bulkQuery.put(counter + SQLQueryPaiement.F_NEW_SUIVI_DECLARATION, new Object[]{
                numeroNC,
                journal.getDocumentApure().trim(),
                chaineApurement.trim(), debut,
                journal.getMontantPercu(),
                observationReussie,
                journal.getCompteBancaire().getDevise().getCode().trim()

            });

            boolean result = executeQueryBulkInsert(bulkQuery);

            if (result) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public String validationApurementCompt(String codeJournal, String observationAvisRejete, Integer etatApurement) {

        NotePerception notePeception = new NotePerception();
        String numeroNC = GeneralConst.EMPTY_STRING;

        String chaineApurement = "Apurement comptable";
        String observationReussie = "Apurement Comptable  a reussi";
        float debit = 0;

        Amr amr = new Amr();
        BonAPayer bap = new BonAPayer();

        try {

            Journal journal = getJournalByCode(codeJournal.trim());

            if (journal != null) {
                counter++;

                bulkQuery.put(counter + SQLQueryPaiement.F_APUREMENT_COMPTABLE, new Object[]{
                    etatApurement, observationAvisRejete.trim(), codeJournal
                });

                switch (journal.getTypeDocument().trim()) {

                    case PaiementConst.ParamNameDocument.NP:
                    case PaiementConst.ParamNameDocument.DEC:

                        notePeception = getNotePerceptionByCode(journal.getDocumentApure());

                        if (notePeception != null) {
                            numeroNC = notePeception.getNoteCalcul().getNumero().trim();
                        }

                        break;

                    case PaiementConst.ParamNameDocument.AMR_ONE:
                    case PaiementConst.ParamNameDocument.AMR_TWO:
                    case PaiementConst.ParamNameDocument.AMR:

                        amr = NotePerceptionBusiness.getAmrByCode(journal.getDocumentApure());

                        if (amr != null) {
                            numeroNC = amr.getFichePriseCharge().getDetailFichePriseChargeList().get(0).getNc();
                        }

                        break;

                    case PaiementConst.ParamNameDocument.BP:

                        bap = getBonAPayerByCode(journal.getDocumentApure());

                        if (bap != null) {
                            numeroNC = bap.getFkAmr().getFichePriseCharge().getDetailFichePriseChargeList().get(0).getNc();
                        }
                        break;
                }
            }

            counter++;

            bulkQuery.put(counter + SQLQueryPaiement.F_NEW_SUIVI_DECLARATION, new Object[]{
                numeroNC,
                journal.getDocumentApure().trim(),
                chaineApurement.trim(), 0, 0,
                observationReussie,
                journal.getCompteBancaire().getDevise().getCode().trim()

            });

            boolean result = executeQueryBulkInsert(bulkQuery);

            if (result) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public static Journal getJournalByCode(String code) {
        try {

            String query = SQLQueryPaiement.SELECT_JOURNAL_BY_CODE;

            List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Journal.class), false, code);
            return journals.isEmpty() ? null : journals.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static NotePerception getNotePerceptionByCode(String code) {
        try {

            String query = SQLQueryPaiement.SELECT_NOTE_PERCEPTION_BY_CODE;

            List<NotePerception> notePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false, code);
            return notePerceptions.isEmpty() ? null : notePerceptions.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static BonAPayer getBonAPayerByCode(String codeBonAPayer) {
        String query = SQLQueryPaiement.SELECT_BON_A_PAYE_BY_CODE;
        List<BonAPayer> listBonAPayer = (List<BonAPayer>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(BonAPayer.class), false, codeBonAPayer.trim());
        BonAPayer bonAPayer = !listBonAPayer.isEmpty() ? listBonAPayer.get(0) : null;
        return bonAPayer;
    }

    public static BonAPayer getBonAPayerByAMR(String codeAMR) {
        String query = SQLQueryPaiement.SELECT_BON_A_PAYE_BY_AMR;
        List<BonAPayer> listBonAPayer = (List<BonAPayer>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(BonAPayer.class), false, codeAMR.trim());
        BonAPayer bonAPayer = !listBonAPayer.isEmpty() ? listBonAPayer.get(0) : null;
        return bonAPayer;
    }

    public static boolean saveBordereau(Bordereau bordereau) {
        boolean result;
        String sqlQuery = SQLQueryPaiement.EXEC_F_NEW_BORDEREAU;
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                bordereau.getNumeroBordereau(),
                bordereau.getNomComplet(),
                bordereau.getDatePaiement(),
                bordereau.getTotalMontantPercu(),
                bordereau.getCompteBancaire(),
                bordereau.getAgent(),
                bordereau.getDateCreate(),
                bordereau.getEtat(),
                bordereau.getNumeroDeclaration(),
                bordereau.getNumeroAttestationPaiement(),
                bordereau.getCentre()
        );
        return result;
    }

    public static String saveBordereau2(Bordereau bordereau) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "BORDEREAU";
        try {
            String query = SQLQueryPaiement.EXEC_F_NEW_BORDEREAU_OUTPUT;

            params.put("NUMERO_BORDEREAU", bordereau.getNumeroBordereau());
            params.put("NOM_COMPLET", bordereau.getNomComplet());
            params.put("DATE_PAIEMENT", bordereau.getDatePaiement());
            params.put("TOTAL_MONTANT", bordereau.getTotalMontantPercu());
            params.put("COMPTE_BANCAIRE", bordereau.getCompteBancaire().getCode());
            params.put("AGENT", bordereau.getAgent().getCode());
            params.put("DATE_CREATE", bordereau.getDateCreate());
            params.put("ETAT", bordereau.getEtat().getCode());
            params.put("NUMERO_DECLARATION", bordereau.getNumeroDeclaration());
            params.put("NUMERO_ATTESTATION_PAIEMENT", bordereau.getNumeroAttestationPaiement());
            params.put("centre", bordereau.getCentre());

            String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

            if (!result.isEmpty()) {
                returnValue = result;
            }
        } catch (Exception e) {
            throw (e);
        }
        return returnValue;
    }

    public static String saveDetailsBordereau(DetailBordereau db) {

        String valueReturn = GeneralConst.EMPTY_STRING;

        try {
            int counter = 0;
            boolean result = false;
            HashMap<String, Object[]> bulkQuery = new HashMap<>();

            counter++;
            bulkQuery.put(counter + SQLQueryPaiement.EXEC_F_NEW_DETAIL_BORDEREAU,
                    new Object[]{
                        db.getBordereau().getCode(),
                        db.getArticleBudgetaire().getCode(),
                        db.getMontantPercu(),
                        db.getDevise(),
                        db.getPeriodicite(),
                        db.getStatut()
                    });

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);

            if (result) {
                valueReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                valueReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return valueReturn;
    }

    public static boolean checkReferePaiementExists(String numeroBordereau, String compteBancaire, String typeDocument) {
        boolean exist = true;
        String sqlQuery = GeneralConst.EMPTY_STRING;

        switch (typeDocument) {
            case "DEC":

                sqlQuery = SQLQueryPaiement.SELECT_BORDEREAU_BY_REFERENCE_PAIEMENT;

                List<Bordereau> bordereaus = (List<Bordereau>) myDao.getDaoImpl().find(sqlQuery,
                        Casting.getInstance().convertIntoClassType(Bordereau.class), false,
                        numeroBordereau.trim(), compteBancaire.trim());

                if (bordereaus != null) {
                    if (bordereaus.isEmpty()) {
                        exist = false;
                    }
                } else {
                    exist = false;
                }
                break;
            default:
                sqlQuery = SQLQueryPaiement.SELECT_JOURNAL_BY_REFERENCE_PAIEMENT;

                List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(sqlQuery,
                        Casting.getInstance().convertIntoClassType(Journal.class), false,
                        numeroBordereau.trim(), compteBancaire.trim());

                if (journals != null) {
                    if (journals.isEmpty()) {
                        exist = false;
                    }
                } else {
                    exist = false;
                }
                break;
        }

        return exist;
    }

    public static boolean saveJournal(Journal journal, String typeDocument) {
        boolean result;
        String sqlQuery = SQLQueryPaiement.EXEC_F_JOURNAL;
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                journal.getSite().getCode().trim(),
                journal.getDocumentApure().trim(),
                typeDocument.trim(),
                journal.getMontant(),
                journal.getCompteBancaire().getCode().trim(),
                journal.getBordereau().trim(),
                journal.getDateBordereau(),
                journal.getMontantApurre(),
                journal.getNumeroReleveBancaire(),
                journal.getDateReleveBancaire(),
                journal.getAgentCreat(),
                journal.getAgentApurement(),
                journal.getPersonne().getCode().trim(),
                journal.getTaux(),
                journal.getModePaiement().trim(),
                journal.getMontantPercu(),
                journal.getNumeroAttestationPaiement().trim());

        return result;
    }

    public static boolean traiterPaiementJournal(String id, String observation, String value, String userId) {
        boolean result = false;

        String sqlQuery = ":UPDATE T_JOURNAL SET ETAT = ?1, AGENT_MAJ = ?2, "
                + " DATE_MAJ = CONVERT(DATE,GETDATE(),103), OBSERVATION_BANQUE = ?3 WHERE CODE = ?4";

        int counterNC = GeneralConst.Numeric.ZERO;
        HashMap<String, Object[]> firstBulk = new HashMap<>();

        counterNC++;

        firstBulk.put(counterNC + sqlQuery, new Object[]{
            Integer.valueOf(value),
            Integer.valueOf(userId),
            observation.trim(),
            id.trim()
        });

        return result = executeQueryBulkInsert(firstBulk);

    }

    public static boolean traiterPaiementBordereau(String id, String observation, String value, String userId) {
        boolean result = false;

        String sqlQuery = ":UPDATE T_BORDEREAU SET ETAT = ?1, AGENT_MAJ = ?2, "
                + " DATE_MAJ = CONVERT(DATE,GETDATE(),103), OBSERVATION_BANQUE = ?3 WHERE CODE = ?4";

        int counterNC = GeneralConst.Numeric.ZERO;
        HashMap<String, Object[]> firstBulk = new HashMap<>();

        counterNC++;

        firstBulk.put(counterNC + sqlQuery, new Object[]{
            Integer.valueOf(value),
            Integer.valueOf(userId),
            observation.trim(),
            Integer.valueOf(id)
        });

        return result = executeQueryBulkInsert(firstBulk);

    }

    public static boolean createDeclaration(Bordereau bordereau, List<RetraitDeclaration> listDeclaration,String deviseCode ) {

        String bordereauID = GeneralConst.EMPTY_STRING;
        boolean result = false;

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        HashMap<String, Object> paramsBordereau = new HashMap<>();
        int counterNC = GeneralConst.Numeric.ZERO;

        String firstStoredProcedure = "F_NEW_BORDEREAU";
        String secondStoredReturnValueKey = "BORDEREAU";

        paramsBordereau.put("NUMERO_BORDEREAU", bordereau.getNumeroBordereau());
        paramsBordereau.put("NOM_COMPLET", bordereau.getNomComplet());
        paramsBordereau.put("DATE_PAIEMENT", new Date());
        paramsBordereau.put("TOTAL_MONTANT", bordereau.getTotalMontantPercu());
        paramsBordereau.put("COMPTE_BANCAIRE", bordereau.getCompteBancaire().getCode());
        paramsBordereau.put("AGENT", bordereau.getAgent().getCode());
        paramsBordereau.put("DATE_CREATE", new Date());
        paramsBordereau.put("ETAT", bordereau.getEtat().getCode());
        paramsBordereau.put("NUMERO_DECLARATION", bordereau.getNumeroDeclaration());
        paramsBordereau.put("NUMERO_ATTESTATION_PAIEMENT", GeneralConst.EMPTY_STRING);
        paramsBordereau.put("CENTRE", bordereau.getCentre());

        if (!listDeclaration.isEmpty()) {

            for (RetraitDeclaration retraitDeclaration : listDeclaration) {
                counterNC++;

                BigDecimal amount = new BigDecimal(0);

                if (retraitDeclaration.getEtat() == 4 || retraitDeclaration.getEtat() == 3) {

                    //amount = amount.add(retraitDeclaration.getMontant());
                    RetraitDeclaration rdPere = AssujettissementBusiness.getRetraitDeclarationByID(retraitDeclaration.getRetraitDeclarationMere());
                    retraitDeclaration.setFkPeriode(rdPere.getFkPeriode());
                    amount = amount.add(bordereau.getTotalMontantPercu()); 

                } else {

                    /*if (retraitDeclaration.getNewId() != null) {

                        List<RetraitDeclaration> retraitDeclarations = DeclarationBusiness.getListRetraitDeclarationByNewID_V2(retraitDeclaration.getNewId());

                        if (!retraitDeclarations.isEmpty()) {

                            for (RetraitDeclaration rd : retraitDeclarations) {
                                amount = amount.add(rd.getMontant());

                            }
                        }

                    } else {
                        amount = amount.add(retraitDeclaration.getMontant());

                    }*/
                    
                    amount = amount.add(bordereau.getTotalMontantPercu());

                }

                firstBulk.put(counterNC + SQLQueryPaiement.EXEC_F_NEW_DETAIL_BORDEREAU_V2, new Object[]{
                    retraitDeclaration.getFkAb().trim(),
                    amount,
                    deviseCode,//retraitDeclaration.getDevise(),
                    retraitDeclaration.getFkPeriode()
                });
            }
        }

        try {
            bordereauID = myDao.getDaoImpl().execBulkQueryWithOneLevel(
                    firstStoredProcedure, secondStoredReturnValueKey,
                    paramsBordereau, firstBulk);

            if (!bordereauID.isEmpty()) {
                result = true;
            }

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static Journal getJournalByTitrePerceptionAndDocument(String codeTitrePerception,
            String document, String allSite, String agentParam) {
        String query = SQLQueryPaiement.SELECT_PAIEMENT_BY_RERERENCE_DOCUMENT_V1;

        String paramSQL = GeneralConst.EMPTY_STRING;

        if (!agentParam.isEmpty()) {
            switch (allSite) {
                case GeneralConst.Number.ZERO:
                    paramSQL = " AND J.AGENT_CREAT = %s";
                    paramSQL = String.format(paramSQL, agentParam);

                    query = String.format(query, paramSQL);
                    break;
                case GeneralConst.Number.ONE:
                    paramSQL = " AND J.SITE = '%s'";
                    paramSQL = String.format(paramSQL, agentParam);
                    query = String.format(query, paramSQL);
                    break;
            }
        } else {
            query = String.format(query, paramSQL);
        }

        List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Journal.class), false, codeTitrePerception.trim(), document);
        Journal journal = !journals.isEmpty() ? journals.get(0) : null;
        return journal;
    }

    public static Bordereau getBordereauByTitrePerceptionAndDocument(String codeTitrePerception,
            String allSite, String agentParam) {
        String query = SQLQueryPaiement.SELECT_PAIEMENT_BY_RERERENCE_DOCUMENT_V3;

        String paramSQL = GeneralConst.EMPTY_STRING;

        if (!agentParam.isEmpty()) {
            switch (allSite) {
                case GeneralConst.Number.ZERO:
                    paramSQL = " AND B.AGENT = %s";
                    paramSQL = String.format(paramSQL, agentParam);

                    query = String.format(query, paramSQL);
                    break;
                case GeneralConst.Number.ONE:
                    paramSQL = " AND B.AGENT IN (SELECT A.CODE FROM T_AGENT A WITH (READPAST) WHERE A.SITE = '%s')";
                    paramSQL = String.format(paramSQL, agentParam);
                    query = String.format(query, paramSQL);
                    break;
            }
        } else {
            query = String.format(query, paramSQL);
        }

        List<Bordereau> bordereaus = (List<Bordereau>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Bordereau.class), false, codeTitrePerception.trim());
        Bordereau bordereau = !bordereaus.isEmpty() ? bordereaus.get(0) : null;
        return bordereau;
    }

    public static List<Bordereau> getListBordereauByTitrePerceptionAndDocumentAdvancedSearch(
            String accountBank,
            String allSite, String agentParam,
            String startDate, String endDate, String state) {

        String paramSQL1 = GeneralConst.EMPTY_STRING;
        String paramSQL2 = GeneralConst.EMPTY_STRING;

        if (startDate != null) {
            if (startDate.contains("-")) {
                startDate = ConvertDate.getValidFormatDate(startDate);
            }
        }

        if (endDate != null) {
            if (endDate.contains("-")) {
                endDate = ConvertDate.getValidFormatDate(endDate);
            }
        }

        String paramSQL3 = " AND DBO.FDATE(B.DATE_PAIEMENT) BETWEEN '%s' AND '%s'";
        paramSQL3 = String.format(paramSQL3, startDate, endDate);

        if (!accountBank.equals(GeneralConst.ALL)) {
            paramSQL2 = " AND B.COMPTE_BANCAIRE = '%s'";
            paramSQL2 = String.format(paramSQL2, accountBank);
        }

        String paramSQL4 = GeneralConst.EMPTY_STRING;

        if (!state.equals(GeneralConst.ALL)) {
            paramSQL4 = " AND B.ETAT = %s";
            paramSQL4 = String.format(paramSQL4, state);
        }

        String query = SQLQueryPaiement.SELECT_PAIEMENT_BY_RERERENCE_DOCUMENT_V3_ADVANCED;

        if (!agentParam.isEmpty()) {
            switch (allSite) {
                case GeneralConst.Number.ZERO:
                    paramSQL1 = " AND B.AGENT = %s";
                    paramSQL1 = String.format(paramSQL1, agentParam);

                    query = String.format(query, paramSQL1, paramSQL2, paramSQL3, paramSQL4);
                    break;
                case GeneralConst.Number.ONE:
                    paramSQL1 = " AND B.AGENT IN (SELECT A.CODE FROM T_AGENT A WITH (READPAST) WHERE A.SITE = '%s')";
                    paramSQL1 = String.format(paramSQL1, agentParam);
                    query = String.format(query, paramSQL1, paramSQL2, paramSQL3, paramSQL4);
                    break;
            }
        } else {
            query = String.format(query, paramSQL1, paramSQL2, paramSQL3, paramSQL4);
        }

        List<Bordereau> bordereaus = (List<Bordereau>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Bordereau.class), false);
        return bordereaus;
    }

    public static List<Bordereau> getListBordereauByNameAssujetti(
            String assujettiName,
            String allSite, String agentParam,
            String startDate, String endDate) {

        String paramSQL1 = GeneralConst.EMPTY_STRING;

        String query = SQLQueryPaiement.SELECT_PAIEMENT_BY_ASSUJETTI_NAME;

        if (!agentParam.isEmpty()) {
            switch (allSite) {
                case GeneralConst.Number.ZERO:
                    paramSQL1 = " AND B.AGENT = %s";
                    paramSQL1 = String.format(paramSQL1, agentParam);

                    query = String.format(query, paramSQL1);
                    break;
                case GeneralConst.Number.ONE:
                    paramSQL1 = " AND B.AGENT IN (SELECT A.CODE FROM T_AGENT A WITH (READPAST) WHERE A.SITE = '%s')";
                    paramSQL1 = String.format(paramSQL1, agentParam);
                    query = String.format(query, paramSQL1);
                    break;
            }
        } else {
            query = String.format(query, paramSQL1);
        }

        List<Bordereau> bordereaus = (List<Bordereau>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Bordereau.class), true, assujettiName);
        return bordereaus;
    }

    public static List<Journal> getListJournalByTitrePerceptionAndDocument(String valueSearch,
            String document, String allSite, String agentParam) {
        String query = SQLQueryPaiement.SELECT_PAIEMENT_BY_RERERENCE_DOCUMENT_V2;

        String paramSQL1 = GeneralConst.EMPTY_STRING;
        String paramSQL2 = " AND J.TYPE_DOCUMENT = '%s'";
        paramSQL2 = String.format(paramSQL2, document);

        if (!agentParam.isEmpty()) {
            switch (allSite) {
                case GeneralConst.Number.ZERO:
                    paramSQL1 = " AND J.AGENT_CREAT = %s";
                    paramSQL1 = String.format(paramSQL1, agentParam);

                    query = String.format(query, paramSQL2, paramSQL1);
                    break;
                case GeneralConst.Number.ONE:
                    paramSQL1 = " AND J.SITE = '%s'";
                    paramSQL1 = String.format(paramSQL1, agentParam);
                    query = String.format(query, paramSQL2, paramSQL1);
                    break;
            }
        } else {
            query = String.format(query, paramSQL2, paramSQL1);
        }

        List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Journal.class), true, valueSearch.trim());
        return journals;

    }

    public static List<Journal> getListJournalAdvancedSearch(String statDate,
            String endDate, String allSite, String agentParam, String document, String accountBank, String state) {

        if (statDate != null) {
            if (statDate.contains("-")) {
                statDate = ConvertDate.getValidFormatDate(statDate);
            }
        }

        if (endDate != null) {
            if (endDate.contains("-")) {
                endDate = ConvertDate.getValidFormatDate(endDate);
            }
        }

        String query = SQLQueryPaiement.SELECT_PAIEMENT_BY_RERERENCE_DOCUMENT_V1_ADVANCED;

        String paramSQL1 = GeneralConst.EMPTY_STRING;
        String paramSQL2 = " AND DBO.FDATE(J.DATE_CREAT) BETWEEN '%s' AND '%s'";
        paramSQL2 = String.format(paramSQL2, statDate, endDate);

        String paramSQL3 = GeneralConst.EMPTY_STRING;

        if (!accountBank.equals(GeneralConst.ALL)) {
            paramSQL3 = " AND J.COMPTE_BANCAIRE = '%s'";
            paramSQL3 = String.format(paramSQL3, accountBank);
        }

        String paramSQL4 = GeneralConst.EMPTY_STRING;

        if (!state.equals(GeneralConst.ALL)) {
            paramSQL4 = " AND J.ETAT = %s";
            paramSQL4 = String.format(paramSQL4, state);
        } else {
            paramSQL4 = " OR DBO.FDATE(J.DATE_MAJ) BETWEEN '%s' AND '%s'";
            paramSQL4 = String.format(paramSQL4, statDate, endDate);
        }

        if (!agentParam.isEmpty()) {
            switch (allSite) {
                case GeneralConst.Number.ZERO:
                    paramSQL1 = " AND J.AGENT_CREAT = %s";
                    paramSQL1 = String.format(paramSQL1, agentParam);

                    query = String.format(query, paramSQL1, paramSQL2, paramSQL3, paramSQL4);
                    break;
                case GeneralConst.Number.ONE:
                    paramSQL1 = " AND J.SITE = '%s'";
                    paramSQL1 = String.format(paramSQL1, agentParam);
                    query = String.format(query, paramSQL1, paramSQL2, paramSQL3, paramSQL4);
                    break;
            }
        } else {
            query = String.format(query, paramSQL1, paramSQL2, paramSQL3, paramSQL4);
        }

        List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Journal.class), false, document.trim());
        return journals;

    }

    public static boolean executeQueryBulkInsert(HashMap<String, Object[]> params) {
        try {
            return myDao.getDaoImpl().executeBulkQuery(params);
        } catch (Exception e) {
            throw e;
        }

    }

    public static Journal getJournalByDocument(String codeDocument) {
        try {

            String query = SQLQueryPaiement.SELECT_JOURNAL_BY_DOCUMENT;

            List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Journal.class), false, codeDocument);
            return journals.isEmpty() ? null : journals.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Journal getJournalByDocument(String codeDocument, Boolean isApure) {
        try {

            String query = SQLQueryPaiement.SELECT_JOURNAL_BY_DOCUMENT;

            if (!isApure) {
                query = SQLQueryPaiement.SELECT_JOURNAL_BY_DOCUMENT_NON_APURE;
            }

            List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Journal.class), false, codeDocument);
            return journals.isEmpty() ? null : journals.get(0);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public static Journal getJournalByDocumentForAllState(String codeDocument) {
        try {

            String query = SQLQueryPaiement.SELECT_JOURNAL_BY_DOCUMENT_FOR_ALL_STATE;

            List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Journal.class), false, codeDocument);
            return journals.isEmpty() ? null : journals.get(0);
        } catch (Exception e) {
            throw e;
        }
    }
}
