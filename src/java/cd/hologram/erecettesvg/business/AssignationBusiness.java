/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import cd.hologram.erecettesvg.constants.AssujettissementConst;
import cd.hologram.erecettesvg.sql.SQLQueryAssujettissement;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author emmanuel.tsasa
 */
public class AssignationBusiness {

    private static final Dao myDao = new Dao();

    public static String saveAssignationBudgetaire(AssignationBudgetaire ab) throws Exception {

        String result = "0";

        HashMap<String, Object> params = new HashMap<>();

        try {

            if (ab.getId() == 0) {

                String query = SQLQueryAssujettissement.F_NEW_ASSIGNATION_BUDGETAIRE;

                String returnValue = AssujettissementConst.ParamQUERY.ASSIGNATION_RETURN_ID;

                params.put("FK_EXERCICE", ab.getFkExercice());
                params.put("MONTANT_GLOBAL", ab.getMontantGlobal());
                params.put("FK_DEVISE", ab.getFkDevise());
                params.put("DATE_CREATE", ab.getDateCreate());
                params.put("AGENT_CREATE", ab.getAgentCreate());
                params.put("ETAT", ab.getEtat());

                result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

            } else {

                boolean res = myDao.getDaoImpl().executeStoredProcedure(SQLQueryAssujettissement.UPDATE_ASSIGNATION_BUDGETAIRE, ab.getMontantGlobal(), ab.getId());

                if (res) {
                    result = String.valueOf(ab.getId());
                }
            }

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean saveDetailsAssignationBudgetaire(String id, String idUser, List<DetailsAssignationBudgetaire> dabList) throws Exception {

        int counter = 0;
        boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            for (DetailsAssignationBudgetaire dab : dabList) {
                bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_DETAILS_ASSIGNATION_BUDGETAIRE, new Object[]{
                    dab.getFkAssignation(),
                    dab.getFkArticleBudgetaire(),
                    dab.getMontantAssigne(),
                    dab.getFkDevise().getCode(),
                    1,
                    new Date(),
                    dab.getAgentMaj()
                });
                counter++;
            }

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean updateAmountAssignation(Long amount, int daId) throws Exception {
        boolean result = myDao.getDaoImpl().executeNativeQuery(SQLQueryAssujettissement.UPDATE_MONTANT_ASSIGNE, amount, daId);
        return result;
    }
}
