/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.AssujettissementBusiness.propertiesConfig;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.RecouvrementConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.AgentAutoriteBureau;
import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.BanqueAb;
import cd.hologram.erecettesvg.models.Commandement;
import cd.hologram.erecettesvg.models.Contrainte;
import cd.hologram.erecettesvg.models.DernierAvertissement;
import cd.hologram.erecettesvg.models.DetailsRole;
import cd.hologram.erecettesvg.models.ExtraitDeRole;
import cd.hologram.erecettesvg.models.LoginWeb;
import cd.hologram.erecettesvg.models.Med;
import cd.hologram.erecettesvg.models.MedPeriodeDeclaration;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.RetraitDeclaration;
import cd.hologram.erecettesvg.models.Role;
import cd.hologram.erecettesvg.models.Taux;
import cd.hologram.erecettesvg.pojo.DetailRolePrint;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.pojo.PeriodeDeclarationSelection;
import cd.hologram.erecettesvg.sql.SQLQueryAssujettissement;
import cd.hologram.erecettesvg.sql.SQLQueryRecouvrement;
import cd.hologram.erecettesvg.util.Casting;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.Tools;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;

/**
 *
 * @author bonheur.muntasomo
 */
public class RecouvrementBusiness {

    private static final Dao myDao = new Dao();
    static HashMap<String, Object[]> bulkDernierAvert;

    public static List<PeriodeDeclaration> getListDefaillantDeclarationByAdvancedSearch(String codeAB, String sqlQueryParam) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        switch (codeAB) {
            case "00000000000002282020":
                sqlQueryMaster = SQLQueryRecouvrement.SELECT_MASTER_LIST_MED_V11;
                sqlQueryMaster = String.format(sqlQueryMaster, sqlQueryParam);
                break;
            default:
                sqlQueryMaster = SQLQueryRecouvrement.SELECT_MASTER_LIST_MED_V12;
                sqlQueryMaster = String.format(sqlQueryMaster, sqlQueryParam);
                break;
        }

        List<PeriodeDeclaration> listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeAB.trim());
        return listPeriodeDeclarations;
    }

    public static List<PeriodeDeclaration> getListDefaillantDeclarationBySimpleSearch(String codeAssujetti, String codeAB, String codeSite, String sqlQuery) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        List<PeriodeDeclaration> listPeriodeDeclarations = new ArrayList<>();

        switch (codeAssujetti) {

            case "*":

                if (codeAB.equals("00000000000002282020")) {

                    sqlQueryMaster = SQLQueryRecouvrement.SELECT_MASTER_LIST_MED_V222_FOR_ALL_PERSON_AND_RL;
                    sqlQueryMaster = String.format(sqlQueryMaster, sqlQuery);

                    listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                            Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeSite);

                } else {

                    sqlQueryMaster = SQLQueryRecouvrement.SELECT_MASTER_LIST_MED_V222_FOR_ALL_PERSON;
                    sqlQueryMaster = String.format(sqlQueryMaster, sqlQuery);

                    listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                            Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeAB.trim(), codeSite.trim());
                }

                break;

            default:

                if (codeAB.equals("00000000000002282020")) {

                    sqlQueryMaster = SQLQueryRecouvrement.SELECT_MASTER_LIST_MED_V222_AND_RL;
                    sqlQueryMaster = String.format(sqlQueryMaster, sqlQuery);

                    listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                            Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeAssujetti.trim(), codeSite.trim());

                } else {

                    sqlQueryMaster = SQLQueryRecouvrement.SELECT_MASTER_LIST_MED_V222;
                    sqlQueryMaster = String.format(sqlQueryMaster, sqlQuery);

                    listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                            Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeAssujetti.trim(), codeAB.trim(), codeSite);
                }

                break;
        }
        return listPeriodeDeclarations;
    }

    public static List<PeriodeDeclaration> getListDefaillantDeclarationBySimpleSearch(String codeAB,
            String codeSite, String choiceSearch, String valueSearch, String sqlQuery) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        List<PeriodeDeclaration> listPeriodeDeclarations = new ArrayList<>();

        switch (choiceSearch) {

            case "2": // Avenue

                if (codeAB.equals("00000000000002282020")) {

                    sqlQueryMaster = "SELECT PD.* FROM T_PERIODE_DECLARATION PD WITH (READPAST) WHERE PD.ETAT = 1 \n"
                            + "AND PD.ASSUJETISSEMENT IN (SELECT A.ID FROM T_ASSUJETI A WITH (READPAST) \n"
                            + "WHERE ARTICLE_BUDGETAIRE IN ('00000000000002282020','00000000000002352021','00000000000002362021') \n"
                            + "AND A.GESTIONNAIRE IN (select CODE from T_AGENT WHERE SITE = ?1) AND A.BIEN <> '' \n"
                            + "AND A.BIEN IN (SELECT B.ID FROM T_BIEN B WITH (READPAST) WHERE B.FK_ADRESSE_PERSONNE IN (SELECT AP.CODE from T_ADRESSE_PERSONNE AP WITH (READPAST) \n"
                            + "WHERE AP.ADRESSE IN (SELECT ID FROM T_ADRESSE WITH (READPAST) WHERE AVENUE = ?2)))\n"
                            + "AND NOT A.BIEN IS NULL AND A.UNITE_VALEUR IN ('USD','CDF')) \n"
                            + "AND CAST(PD.ID AS INT) NOT IN (SELECT ISNULL(CAST(FK_PERIODE AS INT),0) \n"
                            + "FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ETAT = 1) AND (PD.DATE_LIMITE < GETDATE()) %s";

                    sqlQueryMaster = String.format(sqlQueryMaster, sqlQuery);

                    listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                            Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeSite, valueSearch);

                } else {

                    sqlQueryMaster = "SELECT PD.* FROM T_PERIODE_DECLARATION PD WITH (READPAST) WHERE PD.ETAT = 1 \n"
                            + "AND PD.ASSUJETISSEMENT IN (SELECT A.ID FROM T_ASSUJETI A WITH (READPAST) \n"
                            + "WHERE ARTICLE_BUDGETAIRE = ?1 \n"
                            + "AND A.GESTIONNAIRE IN (select CODE from T_AGENT WHERE SITE = ?2) AND A.BIEN <> '' \n"
                            + "AND A.BIEN IN (SELECT B.ID FROM T_BIEN B WITH (READPAST) WHERE B.FK_ADRESSE_PERSONNE IN (SELECT AP.CODE from T_ADRESSE_PERSONNE AP WITH (READPAST) \n"
                            + "WHERE AP.ADRESSE IN (SELECT ID FROM T_ADRESSE WITH (READPAST) WHERE AVENUE = ?3)))\n"
                            + "AND NOT A.BIEN IS NULL AND A.UNITE_VALEUR IN ('USD','CDF')) \n"
                            + "AND CAST(PD.ID AS INT) NOT IN (SELECT ISNULL(CAST(FK_PERIODE AS INT),0) \n"
                            + "FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ETAT = 1) AND (PD.DATE_LIMITE < GETDATE()) %s";

                    sqlQueryMaster = String.format(sqlQueryMaster, sqlQuery);

                    listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                            Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeSite, valueSearch, valueSearch);

                }

                break;

            case "3": // Quarier

                if (codeAB.equals("00000000000002282020")) {

                    sqlQueryMaster = "SELECT PD.* FROM T_PERIODE_DECLARATION PD WITH (READPAST) WHERE PD.ETAT = 1 \n"
                            + "AND PD.ASSUJETISSEMENT IN (SELECT A.ID FROM T_ASSUJETI A WITH (READPAST) \n"
                            + "WHERE ARTICLE_BUDGETAIRE IN ('00000000000002282020','00000000000002352021','00000000000002362021') \n"
                            + "AND A.GESTIONNAIRE IN (select CODE from T_AGENT WHERE SITE = ?1) AND A.BIEN <> '' \n"
                            + "AND A.BIEN IN (SELECT B.ID FROM T_BIEN B WITH (READPAST) WHERE B.FK_ADRESSE_PERSONNE IN (SELECT AP.CODE from T_ADRESSE_PERSONNE AP WITH (READPAST) \n"
                            + "WHERE AP.ADRESSE IN (SELECT ID FROM T_ADRESSE WITH (READPAST) WHERE QUARTIER = ?2)))\n"
                            + "AND NOT A.BIEN IS NULL AND A.UNITE_VALEUR IN ('USD','CDF')) \n"
                            + "AND CAST(PD.ID AS INT) NOT IN (SELECT ISNULL(CAST(FK_PERIODE AS INT),0) \n"
                            + "FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ETAT = 1) AND (PD.DATE_LIMITE < GETDATE()) %s";

                    sqlQueryMaster = String.format(sqlQueryMaster, sqlQuery);

                    listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                            Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeSite, valueSearch);

                } else {

                    sqlQueryMaster = "SELECT PD.* FROM T_PERIODE_DECLARATION PD WITH (READPAST) WHERE PD.ETAT = 1 \n"
                            + "AND PD.ASSUJETISSEMENT IN (SELECT A.ID FROM T_ASSUJETI A WITH (READPAST) \n"
                            + "WHERE ARTICLE_BUDGETAIRE = ?1 \n"
                            + "AND A.GESTIONNAIRE IN (select CODE from T_AGENT WHERE SITE = ?2) AND A.BIEN <> '' \n"
                            + "AND A.BIEN IN (SELECT B.ID FROM T_BIEN B WITH (READPAST) WHERE B.FK_ADRESSE_PERSONNE IN (SELECT AP.CODE from T_ADRESSE_PERSONNE AP WITH (READPAST) \n"
                            + "WHERE AP.ADRESSE IN (SELECT ID FROM T_ADRESSE WITH (READPAST) WHERE QUARTIER = ?3)))\n"
                            + "AND NOT A.BIEN IS NULL AND A.UNITE_VALEUR IN ('USD','CDF')) \n"
                            + "AND CAST(PD.ID AS INT) NOT IN (SELECT ISNULL(CAST(FK_PERIODE AS INT),0) \n"
                            + "FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ETAT = 1) AND (PD.DATE_LIMITE < GETDATE()) %s";

                    sqlQueryMaster = String.format(sqlQueryMaster, sqlQuery);

                    listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                            Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeSite, valueSearch, valueSearch);

                }

                break;

            case "4": // Commune

                if (codeAB.equals("00000000000002282020")) {

                    sqlQueryMaster = "SELECT PD.* FROM T_PERIODE_DECLARATION PD WITH (READPAST) WHERE PD.ETAT = 1 \n"
                            + "AND PD.ASSUJETISSEMENT IN (SELECT A.ID FROM T_ASSUJETI A WITH (READPAST) \n"
                            + "WHERE ARTICLE_BUDGETAIRE IN ('00000000000002282020','00000000000002352021','00000000000002362021') \n"
                            + "AND A.GESTIONNAIRE IN (select CODE from T_AGENT WHERE SITE = ?1) AND A.BIEN <> '' \n"
                            + "AND A.BIEN IN (SELECT B.ID FROM T_BIEN B WITH (READPAST) WHERE B.FK_ADRESSE_PERSONNE IN (SELECT AP.CODE from T_ADRESSE_PERSONNE AP WITH (READPAST) \n"
                            + "WHERE AP.ADRESSE IN (SELECT ID FROM T_ADRESSE WITH (READPAST) WHERE COMMUNE = ?2)))\n"
                            + "AND NOT A.BIEN IS NULL AND A.UNITE_VALEUR IN ('USD','CDF')) \n"
                            + "AND CAST(PD.ID AS INT) NOT IN (SELECT ISNULL(CAST(FK_PERIODE AS INT),0) \n"
                            + "FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ETAT = 1) AND (PD.DATE_LIMITE < GETDATE()) %s";

                    sqlQueryMaster = String.format(sqlQueryMaster, sqlQuery);

                    listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                            Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeSite, valueSearch);

                } else {

                    sqlQueryMaster = "SELECT PD.* FROM T_PERIODE_DECLARATION PD WITH (READPAST) WHERE PD.ETAT = 1 \n"
                            + "AND PD.ASSUJETISSEMENT IN (SELECT A.ID FROM T_ASSUJETI A WITH (READPAST) \n"
                            + "WHERE ARTICLE_BUDGETAIRE = ?1 \n"
                            + "AND A.GESTIONNAIRE IN (select CODE from T_AGENT WHERE SITE = ?2) AND A.BIEN <> '' \n"
                            + "AND A.BIEN IN (SELECT B.ID FROM T_BIEN B WITH (READPAST) WHERE B.FK_ADRESSE_PERSONNE IN (SELECT AP.CODE from T_ADRESSE_PERSONNE AP WITH (READPAST) \n"
                            + "WHERE AP.ADRESSE IN (SELECT ID FROM T_ADRESSE WITH (READPAST) WHERE COMMUNE = ?3)))\n"
                            + "AND NOT A.BIEN IS NULL AND A.UNITE_VALEUR IN ('USD','CDF')) \n"
                            + "AND CAST(PD.ID AS INT) NOT IN (SELECT ISNULL(CAST(FK_PERIODE AS INT),0) \n"
                            + "FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ETAT = 1) AND (PD.DATE_LIMITE < GETDATE()) %s";

                    sqlQueryMaster = String.format(sqlQueryMaster, sqlQuery);

                    listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                            Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeSite, valueSearch, valueSearch);

                }

                break;
        }
        return listPeriodeDeclarations;
    }

    public static List<PeriodeDeclaration> getListDefaillantDeclarationBySimpleSearch(String codeAssujetti) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_MASTER_LIST_MED_V22;

        List<PeriodeDeclaration> listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeAssujetti.trim());
        return listPeriodeDeclarations;
    }

    public static Med getMedByPerideDeclaration(int periodeID) {
        String query = SQLQueryRecouvrement.SELECT_MED_BY_PERIODE_DECLARATION;
        List<Med> meds = (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, periodeID);
        return meds.isEmpty() ? null : meds.get(0);
    }

    public static Med getMedByPerideDeclaration(int periodeID, String typMed) {
        String query = SQLQueryRecouvrement.SELECT_MED_BY_PERIODE_DECLARATION_2;
        List<Med> meds = (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, periodeID, typMed);
        return meds.isEmpty() ? null : meds.get(0);
    }

    public static String checkExistArticleRole(String articleRole) {
        String sqlQuery = SQLQueryRecouvrement.F_CHECK_EXIST_ARTICLE_ROLE.concat("('" + articleRole.trim() + "')");
        String code = (String) myDao.getDaoImpl().getSingleResult(sqlQuery);
        return code;
    }

    public static Med getMedByNotePerception(String np) {
        String query = SQLQueryRecouvrement.SELECT_MED_BY_NOTE_PERCEPTION;
        List<Med> meds = (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, np);
        return meds.isEmpty() ? null : meds.get(0);
    }

    public static Med getMedById(String id) {
        String query = SQLQueryRecouvrement.SELECT_MED_BY_ID;
        List<Med> meds = (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, id);
        return meds.isEmpty() ? null : meds.get(0);
    }

    public static Boolean saveMed(
            String codeArticleBudgetaire,
            String assujettiCode,
            String periodeDeclaration,
            String echeanceDeclaration,
            String adresseCode,
            String agentCreat,
            float amountPeriodeDeclaration,
            int periodeId,
            String typeDoc,
            String numeroNp,
            String numeroNc,
            float amountPenalite,
            String dateHeureInvitation,
            int numberPd
    ) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {
            counter++;

            switch (typeDoc) {
                case "DECLARATION":
                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_MED, new Object[]{
                        agentCreat,
                        assujettiCode,
                        periodeId, null, null,
                        typeDoc,
                        codeArticleBudgetaire,
                        adresseCode,
                        periodeDeclaration,
                        amountPeriodeDeclaration,
                        new Date()
                    });
                    break;
                case "INVITATION_PAIEMENT":
                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_MED, new Object[]{
                        agentCreat,
                        assujettiCode,
                        periodeId, null, null,
                        typeDoc,
                        codeArticleBudgetaire,
                        adresseCode,
                        periodeDeclaration,
                        amountPeriodeDeclaration,
                        new Date()
                    });
                    break;
                case "SERVICE":
                case "RELANCE":
                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_MED_SERVICE_V2, new Object[]{
                        agentCreat,
                        assujettiCode,
                        periodeId, numeroNc, null,
                        typeDoc,
                        codeArticleBudgetaire,
                        adresseCode,
                        null,
                        amountPeriodeDeclaration,
                        amountPenalite,
                        new Date(),
                        dateHeureInvitation,
                        numberPd
                    });
                    break;
                case "PAIEMENT":
                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_MED, new Object[]{
                        agentCreat,
                        assujettiCode,
                        periodeId, numeroNc, numeroNp,
                        typeDoc,
                        codeArticleBudgetaire,
                        adresseCode,
                        periodeDeclaration,
                        amountPeriodeDeclaration,
                        new Date()
                    });
                    break;

                case "INVITATION_SERVICE":
                    Double amountPenalite1 = amountPenalite * 0.7;
                    Double amountPenalite2 = amountPenalite * 0.3;

                    String accountBankProvince = propertiesConfig.getProperty("VALEUR_COMPTE_BANCAIRE_PENALITE_PROVINCE");
                    String accountBankDRHKAT = propertiesConfig.getProperty("VALEUR_COMPTE_BANCAIRE_PENALITE_DRHKAT");

                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_MED_SERVICE, new Object[]{
                        agentCreat,
                        assujettiCode,
                        periodeId, numeroNc, numeroNp,
                        typeDoc,
                        codeArticleBudgetaire,
                        adresseCode,
                        periodeDeclaration,
                        amountPeriodeDeclaration,
                        amountPenalite,
                        new Date()
                    });

                    counter++;

                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_NOTE_PERCEPTION_PENALITE, new Object[]{
                        numeroNc,
                        amountPenalite1,
                        0, amountPenalite1,
                        periodeDeclaration,
                        echeanceDeclaration,
                        numeroNp,
                        agentCreat,
                        new Date(),
                        3,
                        accountBankDRHKAT
                    });

                    counter++;

                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_NOTE_PERCEPTION_PENALITE, new Object[]{
                        numeroNc,
                        amountPenalite2,
                        0, amountPenalite2,
                        periodeDeclaration,
                        echeanceDeclaration,
                        numeroNp,
                        agentCreat,
                        new Date(),
                        3,
                        accountBankProvince
                    });
                    break;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static LoginWeb getLoginWebByAssujetti(String assujettiCode) {
        String query = SQLQueryRecouvrement.SELECT_LOGIN_WEB_BY_ASSUJETTI;
        List<LoginWeb> loginWebs = (List<LoginWeb>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(LoginWeb.class), false, assujettiCode);
        return loginWebs.isEmpty() ? null : loginWebs.get(0);
    }

    public static List<NotePerception> getListNotePerceptionNotPaidByAssujetti(String codeAssujetti) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_LIST_NOTE_PERCEPTION_NOT_PAID_V1;

        List<NotePerception> notePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false, codeAssujetti.trim());
        return notePerceptions;
    }

    public static List<NotePerception> getListNotePerceptionNotPaidByAssujettiRole(String codeAssujetti) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_LIST_NOTE_PERCEPTION_NOT_PAID_ROLE;

        List<NotePerception> notePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false, codeAssujetti.trim());
        return notePerceptions;
    }

    public static List<NotePerception> getListNotePerceptionNotPaidByService_SiteAndPeriode(
            String codeService, String codeSite, String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamSite = GeneralConst.EMPTY_STRING;
        String sqlParamService = GeneralConst.EMPTY_STRING;

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }
        if (codeService != null && codeSite != null) {
            if (!codeService.equals("*") && !codeSite.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "' AND NC.SITE = '" + codeSite.trim() + "'";
            } else if (!codeService.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "'";
            } else if (!codeSite.equals("*")) {
                sqlParamService = " WHERE NC.SITE = '" + codeSite.trim() + "'";
            }
        } else if (codeService != null) {
            if (!codeService.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "'";
            }
        } else if (!codeSite.equals("*")) {
            sqlParamService = " WHERE NC.SITE = '" + codeSite.trim() + "'";
        }

        String sqlParamDate = " AND DBO.FDATE(NP.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamDate = String.format(sqlParamDate, dateDebut, dateFin);

        sqlParamDate = "";

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_LIST_NOTE_PERCEPTION_NOT_PAID_V3;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamService, sqlParamDate);

        List<NotePerception> notePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false,
                codeSite.trim(), codeService.trim());
        return notePerceptions;
    }

    public static List<NotePerception> getListNotePerceptionNotPaidByService_SiteAndPeriodeRole(
            String codeService, String codeSite, String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamSite = GeneralConst.EMPTY_STRING;
        String sqlParamService = GeneralConst.EMPTY_STRING;

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }
        if (codeService != null && codeSite != null) {
            if (!codeService.equals("*") && !codeSite.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "' AND NC.SITE = '" + codeSite.trim() + "'";
            } else if (!codeService.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "'";
            } else if (!codeSite.equals("*")) {
                sqlParamService = " WHERE NC.SITE = '" + codeSite.trim() + "'";
            }
        } else if (codeService != null) {
            if (!codeService.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "'";
            }
        } else if (!codeSite.equals("*")) {
            sqlParamService = " WHERE NC.SITE = '" + codeSite.trim() + "'";
        }

        String sqlParamDate = " AND DBO.FDATE(NP.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamDate = String.format(sqlParamDate, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_LIST_NOTE_PERCEPTION_NOT_PAID_ROLE2;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamService, sqlParamDate);

        List<NotePerception> notePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false,
                codeSite.trim(), codeService.trim());
        return notePerceptions;
    }

    public static Boolean saveRole(Role role, LogUser logUser,
            List<DetailRolePrint> listDetailsRole,
            List<String> listAssujettiClean, String userId, String articleRole, String medID) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object> bulkQueryRole = new HashMap<>();
        HashMap<String, Object[]> bulkQueryDetailRole = new HashMap<>();

        try {

            String returnValueRole = RecouvrementConst.ParamQuery.ROLE_ID;
            String queryRole = SQLQueryRecouvrement.Role.F_NEW_ROLE;

            bulkQueryRole.put(RecouvrementConst.ParamQuery.PERIODE_DEBUT, role.getPeriodeDebut());
            bulkQueryRole.put(RecouvrementConst.ParamQuery.PERIODE_FIN, role.getPeriodeFin());
            bulkQueryRole.put(RecouvrementConst.ParamQuery.AGENT_CREAT, role.getAgentCreat().getCode());
            bulkQueryRole.put(RecouvrementConst.ParamQuery.DESCRIPTION, role.getObservationReceveur());
            bulkQueryRole.put(RecouvrementConst.ParamQuery.ARTICLE_ROLE, role.getArticleRole().trim());
            bulkQueryRole.put("FK_SITE", role.getFkSite());

            if (listDetailsRole != null && !listDetailsRole.isEmpty()) {

                for (DetailRolePrint detailsRole : listDetailsRole) {
                    counter++;
                    bulkQueryDetailRole.put(counter + SQLQueryRecouvrement.Role.F_NEW_DETAILS_ROLE.concat(
                            detailsRole.getAssujetti().trim()), new Object[]{
                                detailsRole.getDocumentReference().trim(),
                                detailsRole.getTypeDocument().trim()
                            });

                }
            }

            result = myDao.getDaoImpl().execBulkQueryV2(
                    queryRole, returnValueRole, bulkQueryRole,
                    bulkQueryDetailRole, listAssujettiClean, userId, articleRole);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Taux getTauxByDevise(String devise) {
        String sqlQuery = SQLQueryRecouvrement.SELECT_TAUX_BY_DEVISE;
        List<Taux> tauxs = (List<Taux>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Taux.class), false, devise.trim());
        return tauxs.isEmpty() ? null : tauxs.get(0);
    }

    public static List<Role> loadProjetRoles(
            int typeSearch,
            String articleRoleValue,
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin,
            String codeEntite,
            String codeAgent) throws JSONException {

        if (dateDebut != null) {
            if (dateDebut.contains("-")) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains("-")) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        String sqlQuerySecondary_By_Site_Service = GeneralConst.EMPTY_STRING;
        String sqlQuerySecondary_By_ArticleRole = GeneralConst.EMPTY_STRING;
        String sqlQuerySecondary_By_DateCreate = GeneralConst.EMPTY_STRING;
        String sqlQueryMaster;
        String agentList = GeneralConst.EMPTY_STRING;

        if (typeSearch == 1) {

            sqlQuerySecondary_By_ArticleRole = SQLQueryRecouvrement.Role.SELECT_ROLE_PART_QUERY_1_BY_ARTICLE_ROLE;
            sqlQuerySecondary_By_ArticleRole = String.format(sqlQuerySecondary_By_ArticleRole, articleRoleValue.trim());

        } else if (typeSearch == 2) {

            if (!codeSite.equals("*")) {
                sqlQuerySecondary_By_Site_Service = " AND R.FK_SITE = '%s'";
                sqlQuerySecondary_By_Site_Service = String.format(sqlQuerySecondary_By_Site_Service, codeSite);
            }

        }

        String paramAgent = GeneralConst.EMPTY_STRING;

        sqlQueryMaster = SQLQueryRecouvrement.Role.SELECT_LIST_PROJET_ROLE;

        sqlQueryMaster = String.format(sqlQueryMaster,
                sqlQuerySecondary_By_Site_Service,
                sqlQuerySecondary_By_ArticleRole,
                sqlQuerySecondary_By_DateCreate, paramAgent);

        try {

            List<Role> listRoles = (List<Role>) myDao.getDaoImpl().find(sqlQueryMaster,
                    Casting.getInstance().convertIntoClassType(Role.class), false);
            return listRoles;
        } catch (Exception e) {
            throw e;
        }

    }

    public static ExtraitDeRole getExtraitRoleByRole(String roleId) {
        String sqlQuery = SQLQueryRecouvrement.ExtraitRole.SELECT_EXTRAIT_ROLE_BY_ROLE;
        List<ExtraitDeRole> extraitDeRoles = (List<ExtraitDeRole>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ExtraitDeRole.class), false, roleId.trim());
        return extraitDeRoles.isEmpty() ? null : extraitDeRoles.get(0);
    }

    public static boolean executeQueryBulkInsertRole(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }

    public static List<ExtraitDeRole> loadExtraitRoles(
            int typeSearch,
            String valueSearch,
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin,
            String typeRegister,
            String critereSearch,
            String codeEntite,
            String codeAgent) throws JSONException {

        if (dateDebut != null) {
            if (dateDebut.contains("-")) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains("-")) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        String sqlQuerySecondary_By_Critere = GeneralConst.EMPTY_STRING;
        String sqlQuerySecondary_By_Site_Service = GeneralConst.EMPTY_STRING;
        String sqlQuerySecondary_By_DateCreate = GeneralConst.EMPTY_STRING;
        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        String agentList = GeneralConst.EMPTY_STRING;

        if (typeSearch == 1) {

            agentList = Tools.getJsonListToString(codeAgent, "codeAgent");

            switch (critereSearch.trim()) {
                case GeneralConst.Number.ONE:
                    sqlQuerySecondary_By_Critere = SQLQueryRecouvrement.Role.SELECT_EXTRAIT_ROLE_BY_CODE;
                    sqlQuerySecondary_By_Critere = String.format(sqlQuerySecondary_By_Critere, valueSearch);
                    break;
                case GeneralConst.Number.TWO:
                    sqlQuerySecondary_By_Critere = SQLQueryRecouvrement.Role.SELECT_EXTRAIT_ROLE_BY_ARTICLE_ROLE;
                    break;
            }

            sqlQuerySecondary_By_Site_Service = "";
            String.format(SQLQueryRecouvrement.Role.SELECT_DETAIL_ROLE_BY_SITE_SERVICE,
                    Tools.getJsonListToString(codeSite, GeneralConst.SearchCode.SITE),
                    Tools.getJsonListToString(codeService, GeneralConst.SearchCode.SERVICE),
                    Tools.getJsonListToString(codeSite, GeneralConst.SearchCode.SITE),
                    Tools.getJsonListToString(codeService, GeneralConst.SearchCode.SERVICE));

        } else if (typeSearch == 2) {

            agentList = Tools.getListToString(codeAgent);

            sqlQuerySecondary_By_DateCreate = SQLQueryRecouvrement.Role.SELECT_EXTRAIT_ROLE_PART_QUERY_4_BY_DATE_CREATE;
            sqlQuerySecondary_By_DateCreate = String.format(sqlQuerySecondary_By_DateCreate,
                    dateDebut, dateFin);

            sqlQuerySecondary_By_Site_Service = "";
//                    String.format(SQLQueryRecouvrement.Role.SELECT_DETAIL_ROLE_BY_SITE_SERVICE,
//                    Tools.getListToString(codeSite),
//                    Tools.getListToString(codeService),
//                    Tools.getListToString(codeSite),
//                    Tools.getListToString(codeService));
        }

        String sqlParamAgent = GeneralConst.EMPTY_STRING;
//                " AND EXR.AGENT_CREAT IN (%s)";
//        sqlParamAgent = String.format(sqlParamAgent, agentList);

        switch (typeRegister.trim()) {
            case GeneralConst.Number.ONE:
                sqlQueryMaster = SQLQueryRecouvrement.Role.SELECT_LIST_EXTRAIT_ROLE_A_ORDONNANCER;
                break;
            case GeneralConst.Number.TWO:
                sqlQueryMaster = SQLQueryRecouvrement.Role.SELECT_LIST_EXTRAIT_ROLE_ORDONNANCER;
                break;
        }

        sqlQueryMaster = String.format(sqlQueryMaster,
                sqlQuerySecondary_By_Site_Service,
                sqlQuerySecondary_By_Critere,
                sqlQuerySecondary_By_DateCreate,
                sqlParamAgent);

        try {

            List<ExtraitDeRole> listExtraitRole = (List<ExtraitDeRole>) myDao.getDaoImpl().find(sqlQueryMaster,
                    Casting.getInstance().convertIntoClassType(ExtraitDeRole.class), false, valueSearch.trim());
            return listExtraitRole;
        } catch (Exception e) {
            throw e;
        }

    }

    public static String getNiveauExtraitRole(String extraitRoleId) {
        String sqlQuery = SQLQueryRecouvrement.ExtraitRole.F_GET_EXTRAIT_ROLE_NIVEAU.concat("('" + extraitRoleId.trim() + "')");
        String code = (String) myDao.getDaoImpl().getSingleResult(sqlQuery);
        return code;
    }

    public static List<NotePerception> getListNotePerceptionDependancy(String npMere) {
        String sqlQuery = SQLQueryRecouvrement.SELECT_NOTE_PERCEPTION_DEPENDANCY;
        List<NotePerception> listNotePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false, npMere.trim());
        return listNotePerceptions;
    }

    public static String saveContrainte(
            String agentCreate,
            String assujettiCode,
            String dernierAvertissementId,
            LogUser logUser,
            String idHuissier) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "ID_CONTRAINTE";

        String query = "F_NEW_CONTRAINTE";

        params.put("AGENT_CREAT", agentCreate);
        params.put("PERSONNE", assujettiCode.trim());
        params.put("FK_DERNIER_AVERTISSEMENT", dernierAvertissementId.trim());
        params.put("HUISSIER_ID", idHuissier.trim());
        params.put("MAC_ADRESSE", logUser.getMacAddress().trim());
        params.put("IP_ADRESSE", logUser.getIpAddress().trim());
        params.put("HOST_NAME", logUser.getHostName().trim());
        params.put("CONTEXT_BROWSER", logUser.getBrowserContext().trim());

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

        if (result != null && !result.isEmpty()) {
            returnValue = result;
        }
        return returnValue;
    }

    public static String saveCommandement(
            String agentCreate,
            String assujettiCode,
            double fraisPoursuite,
            String contrainteId,
            LogUser logUser) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "ID_COMMANDEMENT";

        String query = "F_NEW_COMMANDEMENT";

        params.put("AGENT_CREAT", agentCreate);
        params.put("PERSONNE", assujettiCode.trim());
        params.put("FK_CONTRAINTE", contrainteId.trim());
        params.put("FRAIS_POURSUITE", fraisPoursuite);
        params.put("MAC_ADRESSE", logUser.getMacAddress().trim());
        params.put("IP_ADRESSE", logUser.getIpAddress().trim());
        params.put("HOST_NAME", logUser.getHostName().trim());
        params.put("CONTEXT_BROWSER", logUser.getBrowserContext().trim());

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

        if (result != null && !result.isEmpty()) {
            returnValue = result;
        }
        return returnValue;
    }

    public static Contrainte getContrainteByID(String iD) {
        String sqlQuery = SQLQueryRecouvrement.Contrainte.SELECT_CONTRAINTE_BY_ID;
        List<Contrainte> contrainte = (List<Contrainte>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Contrainte.class
                ), false, iD);
        return contrainte.isEmpty()
                ? null : contrainte.get(0);

    }

    public static Commandement getCommandementById(String idCommandement) {
        String sqlQuery = SQLQueryRecouvrement.Commandement.SELECT_COMMANDEMENT_BY_ID;
        List<Commandement> commandements = (List<Commandement>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Commandement.class
                ), false, idCommandement.trim());
        return commandements.isEmpty()
                ? null : commandements.get(0);
    }

    public static List<DetailsRole> getListDetailsRoleByExtrait(String extraitRole) {
        String sqlQuery = SQLQueryRecouvrement.ExtraitRole.SELECT_LIST_DETAILS_ROLE_BY_EXTRAIT;
        List<DetailsRole> listDetailsRoles = (List<DetailsRole>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(DetailsRole.class
                ), false, extraitRole.trim());
        return listDetailsRoles;
    }

    public static DernierAvertissement getDernierAvertissementByExtraitRoleAndAssujetti(
            String extraitRoleId, String assujettiCode) {
        String sqlQuery = SQLQueryRecouvrement.DernierAvertissement.SELECT_DERNIER_AVERTISSEMENT_BY_EXTRAIT_AND_ASSUJETTI;
        List<DernierAvertissement> dernierAv = (List<DernierAvertissement>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(DernierAvertissement.class
                ), false,
                extraitRoleId.trim(), assujettiCode.trim());
        return dernierAv.isEmpty()
                ? null : dernierAv.get(0);

    }

    public static boolean saveDernierAvertissement(
            HttpServletRequest request,
            String fkExtraitRole,
            String fkPersonne,
            List<DetailsRole> listDetailsRole) {

        bulkDernierAvert = new HashMap<>();
        int counter = 0;
        Object[] valueObject = null;
        boolean result = false;

        try {

            String idUser = request.getParameter(GeneralConst.ID_USER);
            String operation = request.getParameter(GeneralConst.OPERATION);
            LogUser logUser = new LogUser(request, operation);

            counter++;
            bulkDernierAvert.put(counter + SQLQueryRecouvrement.DernierAvertissement.F_NEW_DERNIER_AVERTISSEMENT,
                    new Object[]{
                        idUser,
                        fkPersonne,
                        fkExtraitRole,
                        logUser.getMacAddress(),
                        logUser.getIpAddress(),
                        logUser.getHostName(),
                        logUser.getBrowserContext()
                    });

            /*if (!listDetailsRole.isEmpty()) {

             for (DetailsRole detailsRole : listDetailsRole) {

             switch (detailsRole.getFkTypeDocument().getCode().trim()) {
             case DocumentConst.DocumentCode.NP:

             NotePerception np = NotePerceptionBusiness.getNotePerceptionByCode(
             detailsRole.getDocumentRef().trim());

             if (np != null) {
             counter++;
             bulkDernierAvert.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
             np.getNoteCalcul().getNumero().trim(),
             np.getNumero().trim(),
             propertiesMessage.getProperty("TXT_GENERATION_DA_SUIVI_COMPTABLE").concat(np.getNumero()),
             GeneralConst.Numeric.ZERO,
             GeneralConst.Numeric.ZERO,
             propertiesMessage.getProperty("TXT_GENERATION_DA_SUIVI_COMPTABLE").concat(np.getNumero()),
             np.getDevise().getCode().trim()
             });
             }
             break;
             case DocumentConst.DocumentCode.BP:
             BonAPayer bap = PaiementBusiness.getBonAPayerByCode(
             detailsRole.getDocumentRef().trim());

             if (bap != null) {
             counter++;
             bulkDernierAvert.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
             bap.getFkNoteCalcul().getNumero().trim(),
             bap.getCode().trim(),
             propertiesMessage.getProperty("TXT_GENERATION_DA_SUIVI_COMPTABLE"),
             GeneralConst.Numeric.ZERO,
             GeneralConst.Numeric.ZERO,
             propertiesMessage.getProperty("TXT_GENERATION_DA_SUIVI_COMPTABLE"),
             propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")
             });
             }
             break;
             }

             }
             }*/
            result = executeQueryBulkInsert(bulkDernierAvert);

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static boolean executeQueryBulkInsert(HashMap<String, Object[]> params) {
        try {
            return myDao.getDaoImpl().executeBulkQuery(params);
        } catch (Exception e) {
            throw e;
        }

    }

    public static ExtraitDeRole getExtraitRoleById(String idExtraitRole) {
        String sqlQuery = SQLQueryRecouvrement.ExtraitRole.SELECT_EXTRAIT_ROLE_BY_ID;
        List<ExtraitDeRole> extraitDeRoles = (List<ExtraitDeRole>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ExtraitDeRole.class), false, idExtraitRole.trim());
        return extraitDeRoles.isEmpty() ? null : extraitDeRoles.get(0);
    }

    public static Role getRoleById(String idRole) {
        String sqlQuery = SQLQueryRecouvrement.Role.SELECT_ROLE_BY_ID;
        List<Role> roles = (List<Role>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Role.class), false, idRole.trim());
        return roles.isEmpty() ? null : roles.get(0);
    }

    public static Amr getAMRByRetrait(Integer fkRetrait) {
        String query = "SELECT * FROM T_AMR WHERE FK_RETRAIT_DECLARATION = ?1 AND ETAT = 1";
        List<Amr> amrs = (List<Amr>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Amr.class), false, fkRetrait);
        return amrs.isEmpty() ? null : amrs.get(0);
    }

    public static PeriodeDeclaration getPeriodeDeclarationById(int periodeId) {
        String query = "SELECT * FROM T_PERIODE_DECLARATION WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";
        List<PeriodeDeclaration> declarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, periodeId);
        return declarations.isEmpty() ? null : declarations.get(0);
    }

    public static PeriodeDeclaration getPeriodeDeclarationByIdV2(int periodeId) {
        String query = "SELECT ID,DEBUT FROM T_PERIODE_DECLARATION  WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";
        List<PeriodeDeclaration> declarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, periodeId);
        return declarations.isEmpty() ? null : declarations.get(0);
    }

    public static Med getMedRelanceByPerideDeclaration(int periodeID, String typMed) {
        String query = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.PERIODE_DECLARATION = (SELECT TOP(1) FK_PERIODE FROM T_RETRAIT_DECLARATION "
                + " WHERE FK_PERIODE = ?1) AND M.ETAT > 0 AND M.TYPE_MED = ?2";
        List<Med> meds = (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, periodeID, typMed);
        return meds.isEmpty() ? null : meds.get(0);
    }

    public static List<Med> getListMedAvance(String codeAB, String sqlQueryParam, String typMed) {
        String query = "SELECT M.* FROM T_MED M INNER JOIN T_PERIODE_DECLARATION PD ON M.PERIODE_DECLARATION = PD.ID WHERE M.ARTICLE_BUDGETAIRE = ?1 AND M.TYPE_MED = ?2 "
                + "AND M.ETAT = 1 AND PD.NOTE_CALCUL IS NULL AND (PD.DATE_LIMITE < GETDATE() OR PD.FIN < GETDATE()) %s";

        query = String.format(query, sqlQueryParam);

        return (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, codeAB, typMed);
    }

    public static List<Med> getListMedAvanceV2(String codeAB, String sqlQueryParam, String typMed, String codeSite) {

        String query = "SELECT M.* FROM T_MED M WITH (READPAST) INNER JOIN T_PERIODE_DECLARATION PD WITH (READPAST) ON M.PERIODE_DECLARATION = PD.ID WHERE M.ARTICLE_BUDGETAIRE = ?1 AND M.TYPE_MED = ?2 "
                + " AND M.ETAT = 1 AND (PD.DATE_LIMITE < GETDATE()) AND FK_SITE = ?3 %s";

        query = String.format(query, sqlQueryParam);

        return (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, codeAB, typMed, codeSite);
    }

    public static RetraitDeclaration getRetraitDeclarationFilsByPeriode(String periodeId) {
        String query = "SELECT * FROM T_RETRAIT_DECLARATION WHERE FK_PERIODE = ?1 AND FK_RETRAIT_PERE IS NOT NULL"
                + " AND EST_PENALISE = 1 AND EST_TAXATION_OFFICE = 1 AND ETAT = 1";
        List<RetraitDeclaration> retraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, periodeId);
        return retraitDeclarations.isEmpty() ? null : retraitDeclarations.get(0);
    }

    public static Boolean saveTaxationOffice(RetraitDeclaration retraitDeclaration, float penaliteInit, float penaliteCalculate,
            String numeroMed, String observation, String periodeDeclarationName, String impotName,
            String compteBancaireDrhkat, String compteBancaireProvince) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_RETRAIT_DECLARATION_2, new Object[]{
                retraitDeclaration.getRequerant(),
                retraitDeclaration.getFkAssujetti(),
                retraitDeclaration.getFkAb(),
                retraitDeclaration.getFkPeriode(),
                retraitDeclaration.getFkAgentCreate(),
                null,
                retraitDeclaration.getMontant(),
                retraitDeclaration.getDevise(),
                retraitDeclaration.getCodeDeclaration(),
                retraitDeclaration.getEstPenalise(),
                penaliteInit,
                retraitDeclaration.getFkBanque(),
                retraitDeclaration.getFkCompteBancaire(),
                penaliteCalculate,
                retraitDeclaration.getTauxRemise(),
                observation,
                compteBancaireDrhkat,
                compteBancaireProvince
            });

            counter++;

            String motif = "Absence déclaration".concat(GeneralConst.SPACE).concat(impotName).concat(GeneralConst.SPACE).concat(periodeDeclarationName.toUpperCase());

            bulkQuery.put(counter + ":EXEC F_NEW_AMR_RELANCE ?1,?2,?3,?4", new Object[]{
                numeroMed,
                retraitDeclaration.getFkAgentCreate(),
                motif, motif
            });

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean saveTaxationOfficeV2(List<RetraitDeclaration> listRetraitDeclarations, RetraitDeclaration rdPenaliteOne,
            RetraitDeclaration rdPenaliteTwo, String numeroMed, String observation, String impotName) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            for (RetraitDeclaration retraitDeclaration : listRetraitDeclarations) {

                counter++;

                bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_RETRAIT_DECLARATION_X, new Object[]{
                    retraitDeclaration.getRequerant(),
                    retraitDeclaration.getFkAssujetti(),
                    retraitDeclaration.getFkAb(),
                    retraitDeclaration.getFkPeriode(),
                    retraitDeclaration.getFkAgentCreate(),
                    null,
                    retraitDeclaration.getMontant(),
                    retraitDeclaration.getDevise(),
                    retraitDeclaration.getCodeDeclaration(),
                    1,
                    0,
                    retraitDeclaration.getFkBanque(),
                    retraitDeclaration.getFkCompteBancaire(),
                    0,
                    0,
                    retraitDeclaration.getObservationRemise()

                });

            }

            counter++;

            bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_RETRAIT_DECLARATION_PENALITE, new Object[]{
                rdPenaliteOne.getRequerant(),
                rdPenaliteOne.getFkAssujetti(),
                rdPenaliteOne.getFkAb(),
                rdPenaliteOne.getFkPeriode(),
                rdPenaliteOne.getFkAgentCreate(),
                null,
                rdPenaliteOne.getMontant(),
                rdPenaliteOne.getDevise(),
                rdPenaliteOne.getCodeDeclaration(),
                0,
                0,
                rdPenaliteOne.getFkBanque(),
                rdPenaliteOne.getFkCompteBancaire(),
                0,
                0,
                rdPenaliteOne.getObservationRemise()
            });

            counter++;

            bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_RETRAIT_DECLARATION_PENALITE, new Object[]{
                rdPenaliteTwo.getRequerant(),
                rdPenaliteTwo.getFkAssujetti(),
                rdPenaliteTwo.getFkAb(),
                rdPenaliteTwo.getFkPeriode(),
                rdPenaliteTwo.getFkAgentCreate(),
                null,
                rdPenaliteTwo.getMontant(),
                rdPenaliteTwo.getDevise(),
                rdPenaliteTwo.getCodeDeclaration(),
                0,
                0,
                rdPenaliteTwo.getFkBanque(),
                rdPenaliteTwo.getFkCompteBancaire(),
                0,
                0,
                rdPenaliteTwo.getObservationRemise()
            });

            counter++;

            String motif = "Absence déclaration".concat(GeneralConst.SPACE).concat(impotName);

            bulkQuery.put(counter + ":EXEC F_NEW_AMR_RELANCE ?1,?2,?3,?4", new Object[]{
                numeroMed,
                rdPenaliteTwo.getFkAgentCreate(),
                motif, motif
            });

            counter++;

            bulkQuery.put(counter + ":UPDATE T_MED_PERIODE_DECLARATION SET TAXATION_OFFICE = 1, DATE_TAXATION_OFFICE = GETDATE() WHERE FK_MED = ?1", new Object[]{
                numeroMed
            });

            counter++;

            bulkQuery.put(counter + ":UPDATE T_RETRAIT_DECLARATION SET EST_TAXATION_OFFICE = 1 WHERE FK_PERIODE = ?1", new Object[]{
                Integer.valueOf(listRetraitDeclarations.get(0).getFkPeriode())
            });

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<Med> getListMedByPersonne(String assujettiCode, String typeMed) {
        String query = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.PERSONNE = ?1 AND M.TYPE_MED = ?2 AND ETAT = 1";
        return (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, assujettiCode, typeMed);
    }

    public static List<Med> getListMedByPersonneV2(String assujettiCode, String codeAB, String codeSite, String referenceRelance, String typeSearch, int numberPd) {

        String query = "";
        List<Med> listRelance = new ArrayList<>();

        switch (typeSearch) {

            case "1":

                if (numberPd != 13 || numberPd != 2050) {

                    query = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.PERSONNE = ?1 AND M.TYPE_MED = 'RELANCE' AND ETAT IN (1,2) AND M.ARTICLE_BUDGETAIRE = ?2 AND M.FK_SITE = ?3 AND (M.NUMBER_PD = ?4 OR M.NUMBER_PD IS NULL)";
                    listRelance = (List<Med>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Med.class), false, assujettiCode, codeAB, codeSite, numberPd);
                } else {
                    query = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.PERSONNE = ?1 AND M.TYPE_MED = 'RELANCE' AND ETAT IN (1,2) AND M.ARTICLE_BUDGETAIRE = ?2 AND M.FK_SITE = ?3";
                    listRelance = (List<Med>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Med.class), false, assujettiCode, codeAB, codeSite);
                }

                break;
            case "2":

                if (numberPd != 13 || numberPd != 2050) {

                    query = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.NUMERO_DOCUMENT = ?1 AND M.TYPE_MED = 'RELANCE' AND ETAT IN (1,2) AND M.ARTICLE_BUDGETAIRE = ?2 AND M.FK_SITE = ?3 AND (M.NUMBER_PD = ?4 OR M.NUMBER_PD IS NULL)";
                    listRelance = (List<Med>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Med.class), false, referenceRelance, codeAB, codeSite, numberPd);

                } else {
                    query = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.NUMERO_DOCUMENT = ?1 AND M.TYPE_MED = 'RELANCE' AND ETAT IN (1,2) AND M.ARTICLE_BUDGETAIRE = ?2 AND M.FK_SITE = ?3";
                    listRelance = (List<Med>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Med.class), false, referenceRelance, codeAB, codeSite);

                }

                break;
            case "*":

                if (numberPd != 13 || numberPd != 2050) {

                    query = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.TYPE_MED = 'RELANCE' AND ETAT IN (1,2) AND M.ARTICLE_BUDGETAIRE = ?1 AND M.FK_SITE = ?2 AND (M.NUMBER_PD = ?3 OR M.NUMBER_PD IS NULL)";
                    listRelance = (List<Med>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Med.class), false, codeAB, codeSite, numberPd);

                } else {

                    query = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.TYPE_MED = 'RELANCE' AND ETAT IN (1,2) AND M.ARTICLE_BUDGETAIRE = ?1 AND M.FK_SITE = ?2";
                    listRelance = (List<Med>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Med.class), false, codeAB, codeSite);

                }

                break;
        }

        return listRelance;
    }

    public static List<MedPeriodeDeclaration> getListPeriodeDeclarationByMed(String codeMed) {
        String query = "SELECT * FROM T_MED_PERIODE_DECLARATION WITH (READPAST) WHERE FK_MED = ?1 AND ETAT = 1";
        return (List<MedPeriodeDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(MedPeriodeDeclaration.class), false, codeMed);
    }
    
    public static List<MedPeriodeDeclaration> getListPeriodeDeclarationNotDeclaredByMed(String codeMed) {
        String query = "SELECT * FROM T_MED_PERIODE_DECLARATION WITH (READPAST) WHERE FK_MED = ?1 AND ETAT = 1 AND EST_PAYE = 0";
        return (List<MedPeriodeDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(MedPeriodeDeclaration.class), false, codeMed);
    }

    public static BanqueAb getBanqueByImpot(String codeImpot) {
        String query = "SELECT * FROM T_BANQUE_AB WITH (READPAST) WHERE FK_AB = ?1 AND ETAT = 1";
        List<BanqueAb> banqueAbs = (List<BanqueAb>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(BanqueAb.class), false, codeImpot);
        return banqueAbs.isEmpty() ? null : banqueAbs.get(0);
    }

    public static AgentAutoriteBureau getAgentAutoriteBureauBySiteAndAB(String codeSite, String codeAB) {
        String query = "select * from T_AGENT_AUTORITE_BUREAU WITH (READPAST) where FK_SITE = ?1 and FK_AB = ?2 and ETAT = 1";
        List<AgentAutoriteBureau> agentAutoriteBureaus = (List<AgentAutoriteBureau>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(AgentAutoriteBureau.class), false, codeSite, codeAB);
        return agentAutoriteBureaus.isEmpty() ? null : agentAutoriteBureaus.get(0);
    }

    public static AgentAutoriteBureau getAgentAutoriteBureauBySite(String codeSite) {
        String query = "select top 1 * from T_AGENT_AUTORITE_BUREAU WITH (READPAST) where FK_SITE = ?1 and FK_AB IS NULL and ETAT = 1";
        List<AgentAutoriteBureau> agentAutoriteBureaus = (List<AgentAutoriteBureau>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(AgentAutoriteBureau.class), false, codeSite);
        return agentAutoriteBureaus.isEmpty() ? null : agentAutoriteBureaus.get(0);
    }

    public static MedPeriodeDeclaration getMedPeriodeDeclarationByPerideDeclaration(int periodeID) {
        String query = "select * from T_MED_PERIODE_DECLARATION WITH (READPAST) where FK_PERIODE_DECLARATION = ?1 AND ETAT = 1";
        List<MedPeriodeDeclaration> medPeriodeDeclarations = (List<MedPeriodeDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(MedPeriodeDeclaration.class), false, periodeID);
        return medPeriodeDeclarations.isEmpty() ? null : medPeriodeDeclarations.get(0);
    }

    public static MedPeriodeDeclaration getMedPeriodeDeclarationByMedID(String medID) {
        String query = "select * from T_MED_PERIODE_DECLARATION WITH (READPAST) where FK_MED = ?1 AND ETAT = 1";
        List<MedPeriodeDeclaration> medPeriodeDeclarations = (List<MedPeriodeDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(MedPeriodeDeclaration.class), false, medID);
        return medPeriodeDeclarations.isEmpty() ? null : medPeriodeDeclarations.get(0);
    }

    public static MedPeriodeDeclaration getMedPeriodeDeclarationByPerideDeclarationV2(int periodeID, String typeMed) {
        String query = "select * from T_MED_PERIODE_DECLARATION where FK_PERIODE_DECLARATION = ?1 AND ETAT = 1 "
                + " AND FK_MED In (SELECT M.ID FROM T_MED M WITH (READPAST) WHERE M.ETAT > 0 AND M.TYPE_MED IN ('SERVICE','RELANCE'))";
        List<MedPeriodeDeclaration> medPeriodeDeclarations = (List<MedPeriodeDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(MedPeriodeDeclaration.class), false, periodeID, typeMed);
        return medPeriodeDeclarations.isEmpty() ? null : medPeriodeDeclarations.get(0);
    }

    public static Boolean changeDayInvitation(String newDateInvitation, String numeroMed) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":UPDATE T_MED SET JOUR_INVITATION = ?1 WHERE ID = ?2", new Object[]{
                newDateInvitation,
                numeroMed
            });

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean suspendreRelance(int userId, String numeroMed) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":UPDATE T_MED SET ETAT = 2 WHERE ID = ?1", new Object[]{
                numeroMed
            });

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }
    
    public static Boolean reactiverRelance(int userId, String numeroMed) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":UPDATE T_MED SET ETAT = 1 WHERE ID = ?1", new Object[]{
                numeroMed
            });

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }
    
    public static Boolean canceledPenaliteRelance(int userId, Med med) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":UPDATE T_MED SET PENALITE_ANNULEE = 1,MONTANT_PENALITE = 0, MONTANT_PENALITE_ANNULEE = ?1 WHERE ID = ?2", new Object[]{
                med.getMontantPenalite(),
                med.getId()
            });

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }
}
