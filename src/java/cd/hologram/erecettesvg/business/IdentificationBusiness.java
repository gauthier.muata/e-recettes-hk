/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import cd.hologram.erecettesvg.constants.GeneralConst;
import static cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.sql.SQLQueryIdentification;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.sql.SQLQueryMed;
import cd.hologram.erecettesvg.util.*;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author emmanuel.tsasa
 */
public class IdentificationBusiness {

    private static final Dao myDao = new Dao();

    public static List<FormeJuridique> getFormeJuridiques() {
        try {
            String query = SQLQueryIdentification.SELECT_FORMES_JURIDIQUES;
            List<FormeJuridique> formeJuridiques = (List<FormeJuridique>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(FormeJuridique.class), false);
            return formeJuridiques;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static AdressePersonne getDefaultAdressByPersone(String personCode) {
        try {
            String query = SQLQueryIdentification.SELECT_DEFAULT_ADRESSE_BY_PERSON;
            List<AdressePersonne> adresses = (List<AdressePersonne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AdressePersonne.class), false, personCode);
            return adresses.isEmpty() ? null : adresses.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Adresse getAdresseByAvenueAndNumero(String avenue, String numero) {
        try {
            String query = SQLQueryIdentification.SELECT_ADRESSE_BY_AVENUE_AND_NUMERO;
            List<Adresse> adresses = (List<Adresse>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Adresse.class), false, avenue, numero);
            return adresses.isEmpty() ? null : adresses.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<EntiteAdministrative> getEntiteAdministratives(String intitule) {
        try {
            String query = SQLQueryIdentification.SELECT_ENTITES_ADMINISTRATIVE;
            List<EntiteAdministrative> entiteAdministratives = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), true, intitule);
            return entiteAdministratives;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<ComplementForme> getComplementFormeByForme(String codeFormeJuridique) {
        try {
            String query = SQLQueryIdentification.SELECT_COMPLEMENTS_FORME;
            List<ComplementForme> complementFormes = (List<ComplementForme>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementForme.class), false, codeFormeJuridique);
            return complementFormes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<ValeurPredefinie> getValeurPredefinies(String typeComplement) {
        try {
            String query = SQLQueryIdentification.SELECT_VALEURS_PREDEFINIES;
            List<ValeurPredefinie> valeurPredefinies = (List<ValeurPredefinie>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ValeurPredefinie.class), false, typeComplement);
            return valeurPredefinies;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Personne> getPersonnesByName(String libelle) {
        try {
            String query = SQLQueryIdentification.SELECT_PERSONNES_BY_NAME;
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), true, libelle);
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Personne> getPersonnesByNameV2(String libelle) {
        try {
            String query = SQLQueryIdentification.SELECT_PERSONNES_BY_NAME_V2;
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), true, libelle);
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Personne> getListAllPersonnesDemandeTeleProcedure(int state) {
        try {

            String query = "";

            switch (state) {
                case 3:

                    query = "SELECT * FROM T_PERSONNE WITH(READPAST) WHERE ETAT = 1 AND "
                            + " CODE IN (select FK_PERSONNE from T_LOGIN_WEB WITH (READPAST) where ETAT = 3) ORDER BY PRENOMS,NOM";

                    break;
                case 4:

                    query = "SELECT * FROM T_PERSONNE WITH(READPAST) WHERE ETAT = 1 AND "
                            + " CODE IN (select FK_PERSONNE from T_LOGIN_WEB WITH (READPAST) where ETAT = 4 AND SEND_MAIL = 0) ORDER BY PRENOMS,NOM";
                    break;
            }

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, state);
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getPersonneByCodeOrNif(String nif, boolean byCode) {
        try {
            String query = "SELECT * FROM T_PERSONNE WITH(READPAST) WHERE ETAT = 1 AND "
                    + " CODE IN (select FK_PERSONNE from T_LOGIN_WEB WITH (READPAST) where ETAT in (2,3,4) AND USERNAME = ?1)";

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, nif);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getPersonneByCodeOrNifV2(String nif, boolean byCode) {
        try {
            String query = "SELECT * FROM T_PERSONNE WITH(READPAST) WHERE ETAT = 1 AND NIF = ?1";

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, nif);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getPersonneByPhoneNumber(String phoneNumber) {
        try {
            String query = "SELECT * FROM T_PERSONNE WITH(READPAST) WHERE ETAT = 1 AND "
                    + " CODE IN (select FK_PERSONNE from T_LOGIN_WEB WITH (READPAST) WHERE TELEPHONE = ?1 AND ETAT = 1)";

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, phoneNumber);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static EntiteAdministrative getEntiteAdministrativeByCode(String code) {
        try {
            //String query = SQLQueryIdentification.SELECT_ENTITE_ADMINISTRATIVE_BY_CODE;
            String query = SQLQueryIdentification.SELECT_ENTITE_ADMINISTRATIVE_BY_CODE_V2;
            List<EntiteAdministrative> entiteAdministratives = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, code);
            return entiteAdministratives.isEmpty() ? null : entiteAdministratives.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static EntiteAdministrative getEntiteAdministrativeByMere(String code) {
        try {
            String query = SQLQueryIdentification.SELECT_ENTITE_ADMINISTRATIVE_BY_MERE;
            List<EntiteAdministrative> entiteAdministratives = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, code);
            return entiteAdministratives.isEmpty() ? null : entiteAdministratives.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static String createPersonne(Personne personne) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = IdentificationConst.ParamQUERY.PERSONNE;
        try {
            String query = SQLQueryIdentification.F_CREATE_PERSONNE;

            params.put(IdentificationConst.ParamQUERY.NOM, personne.getNom().trim());
            params.put(IdentificationConst.ParamQUERY.POSTNOM, personne.getPostnom().trim());
            params.put(IdentificationConst.ParamQUERY.PRENOM, personne.getPrenoms().trim());
            params.put(IdentificationConst.ParamQUERY.FORME, personne.getFormeJuridique().getCode().trim());
            params.put(IdentificationConst.ParamQUERY.AGENT_CREATE, personne.getAgentCreat());
            params.put(IdentificationConst.ParamQUERY.NIF, personne.getNif().trim());

            String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

            if (!result.isEmpty()) {
                returnValue = result;
            }
        } catch (Exception e) {
            throw (e);
        }
        return returnValue;
    }

    public static String updatePersonne(Personne personne) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = IdentificationConst.ParamQUERY.PERSONNE;
        try {
            String query = SQLQueryIdentification.F_UPDATE_PERSONNE_WEB;
            params.put(IdentificationConst.ParamQUERY.CODE, personne.getCode());
            params.put(IdentificationConst.ParamQUERY.NOM, personne.getNom().trim());
            params.put(IdentificationConst.ParamQUERY.POSTNOM, personne.getPostnom().trim());
            params.put(IdentificationConst.ParamQUERY.PRENOMS, personne.getPrenoms().trim());
            params.put(IdentificationConst.ParamQUERY.FORME, personne.getFormeJuridique().getCode().trim());
            params.put(IdentificationConst.ParamQUERY.AGENT_MAJ, personne.getAgentCreat());
            params.put(IdentificationConst.ParamQUERY.NIF, personne.getNif().trim());
            String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

            if (!result.isEmpty()) {
                returnValue = result;
            }
        } catch (Exception e) {
            throw (e);
        }
        return returnValue;
    }

    public static Boolean savePersonne(
            Personne personne,
            List<Complement> complements,
            List<AdressePersonne> adressePersonnes,
            LoginWeb loginWeb) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {
            if (personne.getCode() == null || personne.getCode().isEmpty()) {
                // NOUVEL ASSUJETTI
                String codePersonne = createPersonne(personne);
                if (codePersonne != null && !codePersonne.isEmpty()) {
                    //INSERT TO COMPLEMENT
                    for (Complement complement : complements) {
                        bulkQuery.put(counter + SQLQueryIdentification.F_NEW_PERSONNE_COMPLEMENT, new Object[]{
                            complement.getFkComplementForme().getCode().trim(),
                            codePersonne,
                            complement.getValeur(),
                            personne.getAgentCreat()
                        });
                        counter++;
                    }
                    //INSERT INTO ADRESSE AND ADRESSE_PERSONNE
                    for (AdressePersonne adressePersonne : adressePersonnes) {
                        bulkQuery.put(counter + SQLQueryIdentification.ADD_ADRESSE_WEB, new Object[]{
                            //adressePersonne.getAdresse().getCommune().getEntiteMere().getEntiteMere().getCode(),
                            //adressePersonne.getAdresse().getProvince().getCode().trim(),
                            //adressePersonne.getAdresse().getVille().getCode().trim(),
                            "00000000000427472016",
                            adressePersonne.getAdresse().getCommune().getEntiteMere().getCode(),
                            adressePersonne.getAdresse().getDistrict().getCode().trim(),
                            adressePersonne.getAdresse().getCommune().getCode().trim(),
                            adressePersonne.getAdresse().getQuartier().getCode().trim(),
                            adressePersonne.getAdresse().getAvenue().getCode().trim(),
                            adressePersonne.getAdresse().getNumero(),
                            personne.getAgentCreat(),
                            codePersonne,
                            adressePersonne.getParDefaut(),
                            0,
                            adressePersonne.getCode(),
                            true,
                            adressePersonne.getAdresse().getChaine()
                        });
                        counter++;
                    }
                    //INSERT INTO LOGIN_WEB
                    if (loginWeb != null) {
                        bulkQuery.put(counter + SQLQueryIdentification.F_CREATE_LOGIN_WEB, new Object[]{
                            codePersonne,
                            loginWeb.getMail(),
                            loginWeb.getPassword(),
                            loginWeb.getTelephone(),
                            loginWeb.getMail(),
                            personne.getAgentCreat()});
                        counter++;
                    }

                    result = executeQueryBulkInsert(bulkQuery);
                }
            } else {
                // EDITION ASSUJETTI
                String codePersonne = updatePersonne(personne);
                if (codePersonne != null && !codePersonne.isEmpty()) {
                    for (Complement complement : complements) {
                        if (complement.getId() == null || complement.getId().equals(EMPTY_STRING)) {
                            bulkQuery.put(counter + SQLQueryIdentification.F_NEW_PERSONNE_COMPLEMENT, new Object[]{
                                complement.getFkComplementForme().getCode().trim(),
                                codePersonne,
                                complement.getValeur(),
                                personne.getAgentCreat()
                            });
                        } else {
                            bulkQuery.put(counter + SQLQueryIdentification.F_UPDATE_PERSONNE_COMPLEMENT, new Object[]{
                                complement.getValeur(),
                                complement.getId()
                            });
                        }
                        counter++;
                    }
                    //ADRESSE PERSONNE
                    for (AdressePersonne adressePersonne : adressePersonnes) {
                        if (adressePersonne.getCode() != null && !adressePersonne.getCode().isEmpty()) {
                            bulkQuery.put(counter + SQLQueryIdentification.ADD_ADRESSE_WEB, new Object[]{
                                //adressePersonne.getAdresse().getCommune().getEntiteMere().getEntiteMere().getCode(),
                                //adressePersonne.getAdresse().getProvince().getCode().trim(),
                                //adressePersonne.getAdresse().getVille().getCode().trim(),
                                "00000000000427472016",
                                adressePersonne.getAdresse().getCommune().getEntiteMere().getCode(),
                                adressePersonne.getAdresse().getDistrict().getCode().trim(),
                                adressePersonne.getAdresse().getCommune().getCode().trim(),
                                adressePersonne.getAdresse().getQuartier().getCode().trim(),
                                adressePersonne.getAdresse().getAvenue().getCode().trim(),
                                adressePersonne.getAdresse().getNumero(),
                                personne.getAgentCreat(),
                                codePersonne,
                                adressePersonne.getParDefaut(),
                                1,
                                adressePersonne.getCode(),
                                adressePersonne.getEtat()
                            });
                        } else {
                            bulkQuery.put(counter + SQLQueryIdentification.ADD_ADRESSE_WEB, new Object[]{
                                "00000000000427472016",
                                adressePersonne.getAdresse().getVille().getCode().trim(),
                                adressePersonne.getAdresse().getDistrict().getCode().trim(),
                                adressePersonne.getAdresse().getCommune().getCode().trim(),
                                adressePersonne.getAdresse().getQuartier().getCode().trim(),
                                adressePersonne.getAdresse().getAvenue().getCode().trim(),
                                adressePersonne.getAdresse().getNumero(),
                                personne.getAgentCreat(),
                                codePersonne,
                                adressePersonne.getParDefaut(),
                                0,
                                EMPTY_STRING,
                                true
                            });
                        }
                        counter++;
                    }
                    // Création ou Update de loginWeb
                    if (loginWeb != null) {
                        bulkQuery.put(counter + SQLQueryIdentification.F_CREATE_LOGIN_WEB, new Object[]{
                            codePersonne,
                            loginWeb.getMail(),
                            loginWeb.getPassword(),
                            loginWeb.getTelephone(),
                            loginWeb.getMail(),
                            personne.getAgentCreat()});
                        counter++;
                    } else {

                        // Désactiver le loginWeb
                        LoginWeb loginW = IdentificationBusiness.getLoginWebByPersonne(personne.getCode().trim());
                        if (loginW != null) {

                            bulkQuery.put(counter + SQLQueryIdentification.DESACTIVER_LOGIN_WEB, new Object[]{
                                loginW.getFkPersonne().trim()});
                            counter++;

                        }
                    }

                    result = executeQueryBulkInsert(bulkQuery);
                }
            }
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean executeQueryBulkInsert(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }

    public static boolean disabledPersonne(String codePersonne) {

        boolean saved = false;
        try {
            String query = SQLQueryIdentification.DISABLED_PERSONNE;
            saved = myDao.getDaoImpl().executeNativeQuery(query, codePersonne);
            return saved;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getPersonneByCode(String codePersonne) {
        try {
            String query = SQLQueryIdentification.SELECT_PERSONNE_BY_CODE;
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, codePersonne);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Adresse getDefaultAdressByPerson(String codePersonne) {
        String query = SQLQueryIdentification.SELECT_DEFAULT_ADRESS_BY_PERSONNE;
        List<Adresse> listAdresses = (List<Adresse>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Adresse.class), false, codePersonne);
        Adresse adresse = !listAdresses.isEmpty() ? listAdresses.get(0) : null;
        return adresse;
    }

    public static List<Personne> getListPersonneBySearchAvance(
            String codeFormeJurique,
            String codeService,
            String assujetti) {

        String secondaryQuery_One = GeneralConst.EMPTY_STRING;
        String secondaryQuery_Two = GeneralConst.EMPTY_STRING;
        String primaryQuery = GeneralConst.EMPTY_STRING;

        switch (codeFormeJurique.trim()) {
            case "*":
            case GeneralConst.Number.ZERO:
                secondaryQuery_One = " AND P.FORME_JURIDIQUE IN ('03','04')";
                break;
            default:
                secondaryQuery_One = " AND P.FORME_JURIDIQUE = '%s'";
                secondaryQuery_One = String.format(secondaryQuery_One, codeFormeJurique.trim());
                break;
        }

        switch (codeService.trim()) {
            case "*":
                secondaryQuery_Two = GeneralConst.EMPTY_STRING;
                break;
            default:
                secondaryQuery_Two = " AND SER.CODE = '%s' ";
                secondaryQuery_Two = String.format(secondaryQuery_Two, codeService.trim());

                break;
        }

        String quertyService = SQLQueryIdentification.SELECT_ASSUJETTI_BY_SERVICE;
        quertyService = String.format(quertyService, secondaryQuery_One, secondaryQuery_Two);

        String queryTaxe = SQLQueryIdentification.SELECT_ASSUJETTI_BY_TAXE;
        queryTaxe = String.format(queryTaxe, secondaryQuery_One, secondaryQuery_Two);

        String queryAssujetti = SQLQueryIdentification.SELECT_ASSUJETTI_BY_ASSUJETIS;
        queryAssujetti = String.format(queryAssujetti, secondaryQuery_One, secondaryQuery_Two);

        primaryQuery = quertyService + " UNION " + queryTaxe + " UNION " + queryAssujetti;

        List<Personne> listPersonne = (List<Personne>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(Personne.class), true, assujetti);
        return listPersonne;

    }

    public static List<AdressePersonne> getAdressesPersonnesByPersonne(String codePersonne) {
        try {
            String query = SQLQueryIdentification.SELECT_ADRESSES_PERSONNE_BY_PERSONNE;
            List<AdressePersonne> adressePersonnes = (List<AdressePersonne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AdressePersonne.class), false, codePersonne);
            return adressePersonnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static AdressePersonne getDefaultAdressePersonneByPersonne(String codePersonne) {
        try {
            String query = SQLQueryIdentification.SELECT_DEFAUT_ADRESSE_PERSONNE_BY_PERSONNE;
            List<AdressePersonne> adressePersonnes = (List<AdressePersonne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AdressePersonne.class), false, codePersonne);
            return adressePersonnes.isEmpty() ? null : adressePersonnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static LoginWeb getLoginWebByPersonne(String codePersonne) {
        try {
            String query = SQLQueryIdentification.SELECT_LOGINWEB_BY_PERSONNE;
            List<LoginWeb> loginWeb = (List<LoginWeb>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(LoginWeb.class), false, codePersonne);
            return loginWeb.isEmpty() ? null : loginWeb.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static LoginWeb getLoginWebByTelephoneOrEmail(String email, String telephone) {
        try {
            String query = SQLQueryIdentification.SELECT_LOGINWEB_BY_TELEPHONE_EMAIL;
            List<LoginWeb> loginWeb = (List<LoginWeb>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(LoginWeb.class), false, email, telephone);
            return loginWeb.isEmpty() ? null : loginWeb.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean saveCarte(String numAutocollant, String numAutorisation,
            String proprietaire, String conducteur, String bien, String photo, String numCPI,
            String commune, String district) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            if (proprietaire != null && !proprietaire.isEmpty()) {

                String codeConducteurVehicule = newConducteurVehicule(proprietaire, conducteur, bien);

                if (!codeConducteurVehicule.isEmpty()) {
                    bulkQuery.put(counter + SQLQueryIdentification.F_NEW_CARTE, new Object[]{
                        codeConducteurVehicule,
                        numAutocollant,
                        numCPI,
                        numAutorisation,
                        photo,
                        district
                    });
                    counter++;
                }

                result = executeQueryBulkInsert(bulkQuery);
            }

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static String newConducteurVehicule(String proprietaire, String conducteur, String bien) {
        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "CONDUCTEUR_VEHICULE_ID";
        try {
            String query = SQLQueryIdentification.F_NEW_CONDUCTEUR_VEHICULE2;
            params.put("PROPRIETAIRE", proprietaire);
            params.put("CONDUCTEUR", conducteur);
            params.put("FK_BIEN", bien);
            String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

            if (!result.isEmpty()) {
                returnValue = result;
            }
        } catch (Exception e) {
            throw (e);
        }
        return returnValue;
    }

    public static List<Personne> getAssujettisValiderByName(String libelle) {
        try {
            String query = SQLQueryIdentification.SELECT_ASSUJETTIS_VALIDER_BY_NAME;
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), true, libelle);
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getAssujettisValiderByCodeOrNif(String nif, boolean byCode) {
        try {
            String query = byCode
                    ? SQLQueryIdentification.SELECT_ASSUJETTIS_VALIDER_BY_CODE
                    : SQLQueryIdentification.SELECT_ASSUJETTIS_VALIDER_BY_NIF;

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, nif);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean validerAssujettis(String codePersonne) {

        boolean saved = false;
        try {
            String query = SQLQueryIdentification.F_VALIDATE_DEMANDE_INSCRIPTION;
            saved = myDao.getDaoImpl().executeNativeQuery(query, codePersonne);
            return saved;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean rejeterAssujettis(String codePersonne) {

        boolean saved = false;
        try {
            String query = SQLQueryIdentification.F_REJECTED_DEMANDE_INSCRIPTION;
            saved = myDao.getDaoImpl().executeNativeQuery(query, codePersonne);
            return saved;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean rejeterAssujettisV2(String codePersonne) {

        boolean saved = false;
        try {
            String query = SQLQueryIdentification.F_REJECTED_ASSUJETTI;
            saved = myDao.getDaoImpl().executeNativeQuery(query, codePersonne);
            return saved;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean updatePassWordLoginWeb(String codePersonne, String pwd) {

        boolean saved = false;
        try {
            String query = SQLQueryIdentification.UPDATE_PASSWORD_LOGIN_WEB;
            saved = myDao.getDaoImpl().executeNativeQuery(query, pwd, codePersonne);
            return saved;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Personne> getAssujettisFrom(int order) {
        try {
            String query = "SELECT * FROM T_PERSONNE ORDER BY CODE OFFSET " + order + " ROWS FETCH NEXT 10 ROWS ONLY";
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false);
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static String getValueCheckNif(String nif) {
        String queryStr = "SELECT DBO.F_CHECK_NIF_ASSUJETTI('" + nif + "')";
        String result = myDao.getDaoImpl().getSingleResultToString(queryStr);
        return result;
    }

    public static String getValueCheckMailOrPhone(String value, int type) {
        String queryStr = "SELECT DBO.F_CHECK_PHONE_OR_MAIL('" + value + "'," + type + ")";
        String result = myDao.getDaoImpl().getSingleResultToString(queryStr);
        return result;
    }

    public static List<Complement> getListComplementByFormeJuridique(String formeCode) {
        try {
            String query = "select c.* from t_complement c with (readpast) \n"
                    + "where c.fk_complement_forme in (select cf.code from t_complement_forme cf  with (readpast) \n"
                    + "where cf.forme_juridique = ?1 and cf.etat = 1) and c.fk_complement_forme in ('CF000000000000022015','CF000000000000212015')";
            List<Complement> complements = (List<Complement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Complement.class), false, formeCode);
            return complements;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<EntiteAdministrative> getEntiteAdministrativesByProvince(String intitule, String CodeProvince) {
        try {
            String query = SQLQueryIdentification.SELECT_ENTITES_ADMINISTRATIVE;
            List<EntiteAdministrative> entiteAdministratives = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), true, intitule);
            return entiteAdministratives;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getPersonneByNtdorTelephone(String critere, String typeSearch) {
        try {
            String query = GeneralConst.EMPTY_STRING;

            if (typeSearch.equals(GeneralConst.Number.TWO)) {
                query = SQLQueryIdentification.SELECT_PERSONNE_BY_NTD;
            } else {
                query = SQLQueryIdentification.SELECT_PERSONNE_BY_TELEPHONE;
            }

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, critere);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static LoginWeb getLoginWebByPersonneV2(String codePersonne) {

        try {

            String SELECT_LOGINWEB_BY_TELEPHONE_EMAIL = "SELECT * FROM T_LOGIN_WEB WITH (READPAST) WHERE FK_PERSONNE = ?1 ";

            List<LoginWeb> loginWeb = (List<LoginWeb>) myDao.getDaoImpl().find(SELECT_LOGINWEB_BY_TELEPHONE_EMAIL,
                    Casting.getInstance().convertIntoClassType(LoginWeb.class), false, codePersonne);

            return loginWeb.isEmpty() ? null : loginWeb.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean updateStatePersonneSendMailNotification(String code) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_LOGIN_WEB SET ETAT = 4 WHERE FK_PERSONNE = ?1";
            bulkQuery.put(0 + query, new Object[]{code});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean updateStateSendMail(String code) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_LOGIN_WEB SET SEND_MAIL = 1 WHERE FK_PERSONNE = ?1";
            bulkQuery.put(0 + query, new Object[]{code});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean changeFormeJuridique(String codePersonne, String codeForme) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_PERSONNE SET FORME_JURIDIQUE = ?1 WHERE CODE = ?2";
            bulkQuery.put(0 + query, new Object[]{codeForme, codePersonne});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean resetPwdNtd(String ntd) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_LOGIN_WEB SET PASSWORD = HASHBYTES('MD5','123-456'),ETAT = 1, SEND_MAIL = 1, DATE_CREATION = GETDATE() WHERE USERNAME = ?1";
            bulkQuery.put(0 + query, new Object[]{ntd});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }
}
