package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.PoursuiteBusiness.executeQueryBulkUpdate;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.PropertiesConst;
import cd.hologram.erecettesvg.constants.TaxationConst;
import cd.hologram.erecettesvg.sql.SQLQueryTaxation;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.pojo.ArchiveData;
import cd.hologram.erecettesvg.pojo.ArticleBudgetaireTaxation;
import cd.hologram.erecettesvg.pojo.BienTaxation;
import cd.hologram.erecettesvg.pojo.DetailPenaliteMock;
import cd.hologram.erecettesvg.pojo.GetTauxNonFiscal;
import cd.hologram.erecettesvg.pojo.NewNoteCalcul;
import cd.hologram.erecettesvg.properties.PropertiesMessage;
import static cd.hologram.erecettesvg.servlets.Taxation.getNexSerie;
import cd.hologram.erecettesvg.sql.SQLQueryFractionnement;
import cd.hologram.erecettesvg.sql.SQLQueryIdentification;
import cd.hologram.erecettesvg.sql.SQLQueryMed;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.sql.SQLQueryOrdonnancement;
import cd.hologram.erecettesvg.util.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author gauthier.muata
 */
public class TaxationBusiness {

    private static final Dao myDao = new Dao();

    public static List<Personne> getListAssujettisByCriterion(String valueSearch) {

        String sqlQuerySecondary = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;

        sqlQuerySecondary = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_LIST_ASSUJETTI_WITH_LIKE;
        String sqlQueryMaster = SQLQueryTaxation.MasterQuery.SELECT_MASTER_LIST_ASSUJETTIS_BY_CRITERION;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlQuerySecondary);

        List<Personne> listAssujettis = (List<Personne>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Personne.class), true, valueSearch.trim());
        return listAssujettis;
    }

    public static List<Personne> getListAssujettisByCriterion(String valueSearch, String formeJuridique) {

        String sqlQuerySecondary = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;

        sqlQuerySecondary = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_LIST_ASSUJETTI_WITH_LIKE;
        String sqlQueryMaster = SQLQueryTaxation.MasterQuery.SELECT_MASTER_LIST_ASSUJETTIS_BY_CRITERION_V2;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlQuerySecondary);

        List<Personne> listAssujettis = (List<Personne>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Personne.class), true, valueSearch.trim());
        return listAssujettis;
    }

    public static List<Personne> getListAssujettisByCriterion(int typeSearch, String valueSearch) {
        boolean isLike = false;
        String sqlQuerySecondary = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        switch (typeSearch) {
            case 1:
                sqlQuerySecondary = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_ONE_FOR_LIST_ASSUJETTI;
                isLike = false;
                break;
            case 2:
                sqlQuerySecondary = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_TWO_FOR_LIST_ASSUJETTI;
                isLike = true;
                break;
        }

        String sqlQueryMaster = SQLQueryTaxation.MasterQuery.SELECT_MASTER_LIST_ASSUJETTIS_BY_CRITERION;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlQuerySecondary);

        List<Personne> listAssujettis = (List<Personne>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Personne.class), isLike, valueSearch.trim());
        return listAssujettis;
    }

    public static List<Palier> getListPalierByArticleBudgetaire(String codeArticle, String formeJuridique, Integer baseCalcul, Integer hasPalier) {

        List<Palier> listPalier;

        if (hasPalier > 0) {

            String sqlQuery = String.format(SQLQueryTaxation.SimpleQuery.SELECT_PALIER_BY_ARTICLE_WITH_PALIER, baseCalcul, baseCalcul);

            listPalier = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle.trim());

        } else {

            String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_PALIER_BY_ARTICLE;

            listPalier = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle.trim());
        }

        return listPalier;
    }

    public static List<Tarif> getListTarifByArticleBudgetaire(String codeArticle) {

        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_TARIF_BY_ARTICLE_BUDGETAIRE;
        List<Tarif> listTarifs = (List<Tarif>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Tarif.class), false, codeArticle.trim());
        return listTarifs;
    }

    public static List<Devise> getListAllDevises() {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_ALL_DEVISES;
        List<Devise> listDevises = (List<Devise>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Devise.class), false);
        return listDevises;
    }

    public static List<Unite> getListAllUnites() {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_ALL_UNITES;
        List<Unite> listUnites = (List<Unite>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Unite.class), false);
        return listUnites;
    }

    public static List<Site> getListSiteAgentAffected(String codeAgent) {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_SITE_AGENT_AFFETED;
        List<Site> listSite = (List<Site>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Site.class), false, codeAgent);
        return listSite;
    }

    public static List<Service> getListAllServices() {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_ALL_SERVICES;
        List<Service> listService = (List<Service>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Service.class), false);
        return listService;
    }

    public static List<NoteCalcul> getListTaxationNonFiscalNotClotured(
            String valueSearch,
            int typeSearch,
            String typeRegister,
            boolean allSite,
            boolean allService,
            String codeSite,
            String codeService,
            String codeAgent) {

        String secondaryQuery_One = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String secondaryQuery_Two = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String secondaryQuery_Three = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String primaryQuery = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;

        boolean isLike = false;

        if (typeSearch == 1) {
            secondaryQuery_One = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_ONE_REGISTER_NOTE_CALCUL;
            isLike = true;
        } else if (typeSearch == 2) {
            secondaryQuery_One = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_TWO_REGISTER_NOTE_CALCUL;
            isLike = false;
        }

        if (!allSite) {
            secondaryQuery_Two = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_THREE_REGISTER_NOTE_CALCUL_V2;
            secondaryQuery_Two = String.format(secondaryQuery_Two, codeSite.trim());
        } else {
            secondaryQuery_Two = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        }

        if (!allService) {
            secondaryQuery_Three = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_FOURT_REGISTER_NOTE_CALCUL;
            secondaryQuery_Three = String.format(secondaryQuery_Three, codeService.trim());
        } else {
            secondaryQuery_Three = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        }

        switch (typeRegister.trim()) {
            case TaxationConst.ConfigCodeRegister.RNC: //Registre notes des calculs
                primaryQuery = SQLQueryTaxation.MasterQuery.SELECT_MASTER_REGISTER_NOTE_CALCUL;
                break;
            case TaxationConst.ConfigCodeRegister.RNCC: //Registre notes des calculs cloturées
                primaryQuery = SQLQueryTaxation.MasterQuery.SELECT_MASTER_REGISTER_NOTE_CALCUL_CLOSED;
                break;
            case TaxationConst.ConfigCodeRegister.RNCO: //Registre notes des calculs à ordonnancées
                primaryQuery = SQLQueryTaxation.MasterQuery.SELECT_MASTER_REGISTER_NOTE_CALCUL_TO_ORDONNANCE;
                break;
            case TaxationConst.ConfigCodeRegister.RNCR: //Registre notes des calculs rejetées à la clôture
                primaryQuery = SQLQueryTaxation.MasterQuery.SELECT_MASTER_REGISTER_NOTE_CALCUL_REJECTED;
                break;
            case TaxationConst.ConfigCodeRegister.RNCRO: //Registre notes des calculs rejetées à l'ordonnancement
                primaryQuery = SQLQueryTaxation.MasterQuery.SELECT_MASTER_REGISTER_NOTE_CALCUL_REJECTED_ORDONNANCEMENT;
                break;
        }

        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two, secondaryQuery_Three);

        List<NoteCalcul> listNoteCalculs = (List<NoteCalcul>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(NoteCalcul.class), isLike, valueSearch.trim());
        return listNoteCalculs;
    }

    public static List<NoteCalcul> getListTaxationNonFiscalNotCloturedBySearchAvanced(
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin,
            String typeRegister) {

        String secondaryQuery_One = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String secondaryQuery_Two = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String sqlFirstParam = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;

        switch (codeSite.trim()) {
            case "*":
                secondaryQuery_One = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_ONE_NOTE_CALCUL_ADVANCED;
                secondaryQuery_One = String.format(secondaryQuery_One, dateDebut.trim(), dateFin.trim());
                break;
            default:

                secondaryQuery_One = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_THREE_NOTE_CALCUL_ADVANCED;
                secondaryQuery_One = String.format(secondaryQuery_One, codeSite.trim(), dateDebut.trim(), dateFin.trim());
                break;
        }

        if (!codeService.equals("*")) {
            secondaryQuery_Two = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_TWO_NOTE_CALCUL_ADVANCED;
            secondaryQuery_Two = String.format(secondaryQuery_Two, codeService.trim());
        } else {
            secondaryQuery_Two = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        }

        switch (typeRegister) {
            case TaxationConst.ConfigCodeRegister.RNC:
                sqlFirstParam = " WHERE NC.ETAT = 2 AND NC.AGENT_CLOTURE IS NULL AND NC.DATE_CLOTURE IS NULL";
                break;
            case TaxationConst.ConfigCodeRegister.RNCC:
                sqlFirstParam = " WHERE NC.ETAT = 1 AND NOT NC.AGENT_CLOTURE IS NULL AND NOT NC.DATE_CLOTURE IS NULL";
                break;
            case TaxationConst.ConfigCodeRegister.RNCR:
                sqlFirstParam = " WHERE NC.ETAT = 0 AND NOT NC.AGENT_CLOTURE IS NULL AND NOT NC.DATE_CLOTURE IS NULL";

                secondaryQuery_One = "AND DBO.FDATE(NC.DATE_CLOTURE) BETWEEN '%s' AND '%s'";
                secondaryQuery_One = String.format(secondaryQuery_One, dateDebut.trim(), dateFin.trim());

                break;

            case TaxationConst.ConfigCodeRegister.RNCRO:

                sqlFirstParam = " WHERE NC.AVIS = 'AV0022015' AND NOT NC.AGENT_VALIDATION IS NULL AND NOT NC.DATE_VALIDATION IS NULL";

                secondaryQuery_One = "AND DBO.FDATE(NC.DATE_VALIDATION) BETWEEN '%s' AND '%s'";
                secondaryQuery_One = String.format(secondaryQuery_One, dateDebut.trim(), dateFin.trim());

                break;
            case TaxationConst.ConfigCodeRegister.RNCO:
                //case "":
                sqlFirstParam = " WHERE NC.ETAT = 1 AND NOT NC.AGENT_CLOTURE IS NULL AND NC.AGENT_VALIDATION IS NULL AND NC.AVIS IS NULL";
                break;
        }

        String primaryQuery = SQLQueryTaxation.MasterQuery.SELECT_MASTER_REGISTER_NOTE_CALCUL_ADVANCED_2;

        primaryQuery = String.format(primaryQuery, sqlFirstParam, secondaryQuery_One, secondaryQuery_Two);

        List<NoteCalcul> listNoteCalculs = (List<NoteCalcul>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(NoteCalcul.class), false);
        return listNoteCalculs;
    }

    public static List<DetailsNc> getListDetailsNoteCalculByNc(String noteCalcul) {
        String sqlQury = SQLQueryTaxation.SimpleQuery.SELECT_LIST_DETAIL_NOTE_CALCUL_BY_NC;
        List<DetailsNc> listDetailsNcs = (List<DetailsNc>) myDao.getDaoImpl().find(sqlQury,
                Casting.getInstance().convertIntoClassType(DetailsNc.class), false, noteCalcul.trim());
        return listDetailsNcs;
    }

    public static List<Avis> getListAllAvis() {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_ALL_AVIS;
        List<Avis> listAvis = (List<Avis>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Avis.class), false);
        return listAvis;
    }

    public static List<Site> getListAllSites() {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_ALL_SITES;
        List<Site> listSite = (List<Site>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Site.class), false);
        return listSite;
    }

    public static List<Site> getListAllSitePeage() {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_ALL_SITES_PEAGE;
        List<Site> listSite = (List<Site>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Site.class), false);
        return listSite;
    }

    public static List<Tarif> getListAllTarifPeage() {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_ALL_TARIF_PEAGE;
        List<Tarif> listTarif = (List<Tarif>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Tarif.class), false);
        return listTarif;
    }

    public static List<Tarif> getListAllTarifSousProvisionPeage() {
        String sqlQuery = "SELECT * FROM T_TARIF WITH (READPAST) WHERE ETAT = 1 AND EST_TARIF_PEAGE = 1 AND EST_TARIF_SOUS_PROVISION = 1 ORDER BY INTITULE";
        List<Tarif> listTarif = (List<Tarif>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Tarif.class), false);
        return listTarif;
    }

    public static List<Tarif> getListAllTarifNotSousProvisionPeage() {
        String sqlQuery = "SELECT * FROM T_TARIF WITH (READPAST) WHERE ETAT = 1 AND EST_TARIF_PEAGE = 1 AND EST_TARIF_SOUS_PROVISION IS NULL ORDER BY INTITULE";
        List<Tarif> listTarif = (List<Tarif>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Tarif.class), false);
        return listTarif;
    }

    public static List<Axe> getListAllAxePeage() {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_ALL_AXE_PEAGE;
        List<Axe> listAxeTarif = (List<Axe>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Axe.class), false);
        return listAxeTarif;
    }

    public static List<ArticleBudgetaire> getListArticleBudgetaires(
            String searchValue,
            String seeEverything,
            String codeService) {

        String sqlQuery = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        List<ArticleBudgetaire> listArticleBudgetaire = new ArrayList<>();

        switch (seeEverything.trim()) {
            case "true":
                sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_ALL_ARTICLE_BUDGETAIRE_WITH_LIKE;
                break;
            case "false":
                sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_ARTICLE_BUDGETAIRE_WITH_LIKE_BY_SERVICE;
                sqlQuery = String.format(sqlQuery, codeService.trim());
                break;
        }

        if (sqlQuery != null && !sqlQuery.isEmpty()) {

            listArticleBudgetaire = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), true,
                    searchValue.trim());
        }

        return listArticleBudgetaire;
    }

    public static List<ArticleBudgetaire> getListImpot(
            String searchValue,
            String codeBancaire) {

        String sqlQuery = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        List<ArticleBudgetaire> listArticleBudgetaire = new ArrayList<>();

        sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_LIST_ALL_IMPOT_WITH_LIKE;
        sqlQuery = String.format(sqlQuery, codeBancaire.trim());

        if (sqlQuery != null && !sqlQuery.isEmpty()) {

            listArticleBudgetaire = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), true,
                    searchValue.trim());
        }

        return listArticleBudgetaire;
    }

    public static Tarif getTarifByCode(String code) {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_TARIF_CODE;
        List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Tarif.class), false, code.trim());
        return tarifs.isEmpty() ? null : tarifs.get(0);
    }

    public static Unite getUnitebyCode(String code) {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_UNITE_CODE;
        List<Unite> tarifs = (List<Unite>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Unite.class), false, code.trim());
        return tarifs.isEmpty() ? null : tarifs.get(0);
    }

    public static Service getServiceByCode(String codeService) {
        String sqlQuery = cd.hologram.erecettesvg.sql.SQLQueryGeneral.SERVICE_BY_CODE_SELECT;
        List<Service> services = (List<Service>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Service.class), false, codeService.trim());
        return services.isEmpty() ? null : services.get(0);
    }

    public static DetailsNc getOneLineDetailsNcByNC(String codeNc) {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_ONE_LINE_DETAIL_NC_BY_NC;
        List<DetailsNc> detailsNcs = (List<DetailsNc>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(DetailsNc.class), false, codeNc);
        return detailsNcs.isEmpty() ? null : detailsNcs.get(0);
    }

    public static Site getSiteByCode(String codeSite) {
        String sqlQuery = cd.hologram.erecettesvg.sql.SQLQueryGeneral.SITE_BY_CODE_SELECT;
        List<Site> sites = (List<Site>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Site.class), false, codeSite.trim());
        return sites.isEmpty() ? null : sites.get(0);
    }

    public static Site getSitePeageByCode(String codeSite) {
        String sqlQuery = cd.hologram.erecettesvg.sql.SQLQueryGeneral.SELECT_SITE_PEAGE;
        List<Site> sites = (List<Site>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Site.class), false, codeSite.trim());
        return sites.isEmpty() ? null : sites.get(0);
    }

    public static Adresse getAdressByCode(String codeAdresse) {
        String sqlQuery = cd.hologram.erecettesvg.sql.SQLQueryGeneral.ADRESS_BY_CODE_SELECT;
        List<Adresse> adresses = (List<Adresse>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Adresse.class), false, codeAdresse.trim());
        return adresses.isEmpty() ? null : adresses.get(0);
    }

    public static AdressePersonne getAdressePersonneByCode(String codeAdressePersonne) {
        String sqlQuery = "SELECT * FROM T_ADRESSE_PERSONNE WITH (READPAST) WHERE CODE = ?1";
        List<AdressePersonne> adressePersonnes = (List<AdressePersonne>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(AdressePersonne.class), false, codeAdressePersonne.trim());
        return adressePersonnes.isEmpty() ? null : adressePersonnes.get(0);
    }

    public static Palier getPalierByAbTarifAndFJuridique(String codeArticle, String codeTarif, String codeFormeJ) {

        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_PALIER_BY_TARIF_AND_FORME_JURIDIQUE;
        List<Palier> listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Palier.class), false,
                codeArticle.trim(), codeTarif.trim(), codeFormeJ.trim());
        return listPaliers.isEmpty() ? null : listPaliers.get(0);
    }

    public static Palier getPalierByAbTarifAndFJuridique(String codeArticle, String codeTarif,
            String codeFormeJ, float baseCalcul) {

        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_PALIER_BY_TARIF_AND_FORME_JURIDIQUE_AND_BASE;
        List<Palier> listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Palier.class), false,
                codeArticle.trim(), codeTarif.trim(), codeFormeJ.trim(), baseCalcul);
        return listPaliers.isEmpty() ? null : listPaliers.get(0);
    }

    public static Palier getPalier(String typeTarif, int palier, String tarif, float base,
            String codeAB, String typePersonne) {
        String sqlQuery = SQLQueryTaxation.ExecuteQuery.EXEC_STORED_PROCEDURE_F_GET_TAUX_NF_WEB;
        List<Palier> listPalier = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Palier.class), false, typeTarif, palier, tarif,
                base, codeAB, typePersonne);
        return listPalier.isEmpty() ? null : listPalier.get(0);
    }

    public static Palier getPalierByCode(String codeArticle, String typePersonne, float base) {

        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_PALIER_BY_CODE;
        List<Palier> listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Palier.class), false,
                codeArticle.trim(), typePersonne, base);
        return listPaliers.isEmpty() ? null : listPaliers.get(0);
    }

    public static Taux getTauxByDevise(String codeDevise, String codeDeviseDestination) {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_TAUX_BY_DEVISE;
        List<Taux> listTaux = (List<Taux>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Taux.class), false,
                codeDevise.trim(), codeDeviseDestination.trim());
        return listTaux.isEmpty() ? null : listTaux.get(0);
    }

    public static Avis getAvisById(String idAvis) {
        String sqlQuery = cd.hologram.erecettesvg.sql.SQLQueryTaxation.SimpleQuery.SELECT_AVIS_BY_ID;
        List<Avis> listAvis = (List<Avis>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Avis.class), false, idAvis.trim());
        return listAvis.isEmpty() ? null : listAvis.get(0);
    }

    public static Adresse getAdresseDefaultByAssujetti(String codeAssujetti) {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_ADRESSE_BY_PERSONNE;
        List<Adresse> listAdresses = (List<Adresse>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Adresse.class),
                false, codeAssujetti.trim());
        Adresse adresse = !listAdresses.isEmpty() ? listAdresses.get(0) : null;
        return adresse;
    }

    public static float getTauxPalierByCode(int code) {
        String paramQuery = "(".concat(code + cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING).concat(")");
        String sqlQuery = SQLQueryTaxation.GetQuery.GET_TAUX_PALIER_BY_CODE;
        sqlQuery = String.format(sqlQuery, paramQuery);
        float result = myDao.getDaoImpl().getSingleResultToBigDecimal(sqlQuery);
        return result;
    }

    public static float getMontantTotalDuDetailNoteCalcul(String noteCalcul) {
        float rep = 0;
        String sqlParam = "('".concat(noteCalcul.trim()).concat("')");
        String sqlQuery = SQLQueryTaxation.GetQuery.GET_MONTANT_TOTAL_DETAIL_NC;
        sqlQuery = String.format(sqlQuery, sqlParam);
        float result = myDao.getDaoImpl().getSingleResultToBigDecimal(sqlQuery);

        if (result > 0) {
            rep = result;
        }
        return rep;
    }

    public static float getTauxByDevise(String codeDevise) {
        String sqlParam = "(".concat(codeDevise.trim()).concat(")");
        String sqlQuery = SQLQueryTaxation.GetQuery.GET_TAUX_BY_DEVISE;
        sqlQuery = String.format(sqlQuery, sqlParam);
        float result = myDao.getDaoImpl().getSingleResultToBigDecimal(sqlQuery);
        return result;
    }

    public static String getCurrentExerciceFiscal() {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_EXERCICE_FISCAL;
        String code = (String) myDao.getDaoImpl().getSingleResult(sqlQuery);
        return code;
    }

    public static Integer getNumberTarifByArticleBudgetaire(String codeArticle) {
        String sqlQuery = SQLQueryTaxation.GetQuery.GET_NUMBER_TARIF_BY_ARTICLE_BUDGETAIRE;
        sqlQuery = String.format(sqlQuery, codeArticle.concat(")"));
        Integer result = myDao.getDaoImpl().getSingleResultToInteger(sqlQuery);
        return result;
    }

    public static Integer getIdPalier(String codeArticle, float base, String typePersonne) {
        String queryStr = "SELECT dbo.GET_ID_PALIER('" + codeArticle.trim() + "'," + base + ",'" + typePersonne + "')";
        Integer result = myDao.getDaoImpl().getSingleResultToInteger(queryStr);
        return result;
    }

    public static String saveNotePerception(
            NotePerception np,
            float remise,
            String npManuel,
            String codeService,
            String codeSite,
            float netAPayer) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "NP";

        String query = "F_NEW_NOTE_PERCEPTION_WEB_VG";

        params.put("NC", np.getNoteCalcul().getNumero().trim());
        params.put("TOTAL", np.getNetAPayer());
        params.put("REMISE", remise);
        params.put("NET_A_PAYER", netAPayer);
        params.put("PAYER", 0);
        params.put("SOLDE", netAPayer);
        params.put("EXERCICE", np.getNoteCalcul().getExercice().trim());
        params.put("SITE", codeSite.trim());
        params.put("SERVICE", codeService.trim());
        params.put("DATE_ECHEANCE_PAIEMENT", np.getDateEcheancePaiement().trim());

        params.put("AGENT_CREAT", np.getAgentCreat().trim());
        params.put("FRACTIONNEE", Integer.valueOf(GeneralConst.Number.ZERO));
        params.put("NP_MERE", GeneralConst.EMPTY_STRING);
        params.put("NUMERO_MANUEL", npManuel);
        params.put("DATE_CREAT", np.getDateCreat().trim());
        params.put("AMR", GeneralConst.EMPTY_STRING);

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

        if (result != null && !result.isEmpty()) {
            returnValue = result;
        }
        return returnValue;
    }

    public static String createNoteCacul(NewNoteCalcul newNoteCalcul) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "NC_RETURN";

        String query = "F_NEW_NOTE_CALCUL4";

        params.put("PERSONNE", newNoteCalcul.getCodePersonne().trim());
        params.put("ADRESSE", newNoteCalcul.getCodeAdresse().trim());
        params.put("EXERCICE", newNoteCalcul.getExerciceFiscal().trim());
        params.put("SITE", newNoteCalcul.getCodeSite().trim());
        params.put("SERVICE", newNoteCalcul.getCodeService().trim());
        params.put("AGENT_CREAT", newNoteCalcul.getAgentCreate());
        params.put("DATE_CREAT", newNoteCalcul.getDateCreate().trim());
        params.put("DEPOT_DECLARATION", newNoteCalcul.getCodeDepot());
        params.put("BIEN", newNoteCalcul.getCodeBien());
        params.put("CPI_FICHE", GeneralConst.EMPTY_STRING);

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

        if (!result.isEmpty()) {
            returnValue = result;
        }
        return returnValue;
    }

    public static boolean closeNoteCalcul(
            String noteCalcul,
            int etatNC,
            String codeAgent,
            String observation) {

        boolean result;

        String sqlQuery = SQLQueryTaxation.ExecuteQuery.EXEC_F_CLOTURE_NC;
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                noteCalcul.trim(),
                etatNC,
                codeAgent.trim(),
                observation);
        return result;
    }

    public static boolean executeQueryBulkInsert(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }

    public static GetTauxNonFiscal getTauxFromBase(String tarifVariable, boolean palier, String tarifAb,
            float base, String ab, String formjuri) {
        String query = SQLQueryTaxation.ExecuteQuery.EXEC_F_GET_TAUX_NF;
        List<GetTauxNonFiscal> listTaux = (List<GetTauxNonFiscal>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(GetTauxNonFiscal.class),
                false, tarifVariable, palier, tarifAb, base, ab, formjuri);
        return listTaux.isEmpty() ? null : listTaux.get(0);
    }

    public static ArticleBudgetaire getArticleBudgetaireByCode(String codeArticleBudgetaire) {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_BUDGET_ARTICLE_BY_CODE;
        List<ArticleBudgetaire> listArticleBudgetaire = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, codeArticleBudgetaire.trim());
        return listArticleBudgetaire.isEmpty() ? null : listArticleBudgetaire.get(0);
    }

    public static ArticleBudgetaire getArticleBudgetaireByNC(String numeroNC) {
        String query = "SELECT TOP 1 AB.* FROM T_ARTICLE_BUDGETAIRE AB WHERE AB.CODE IN (SELECT "
                + " DISTINCT ARTICLE_BUDGETAIRE FROM T_DETAILS_NC WHERE NOTE_CALCUL = ?1 AND ETAT = 1) "
                + " AND AB.ETAT = 1";
        List<ArticleBudgetaire> listArticleBudgetaire = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, numeroNC.trim());
        return listArticleBudgetaire.isEmpty() ? null : listArticleBudgetaire.get(0);
    }

    public static NoteCalcul getNoteCalculByNumero(String numeroNc) {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_NOTE_CALCUL_BY_NUMERO;
        List<NoteCalcul> noteCalculs = (List<NoteCalcul>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NoteCalcul.class), false, numeroNc.trim());
        return noteCalculs.isEmpty() ? null : noteCalculs.get(0);
    }

    public static String getUniteComplement(String code) {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_UNITE_COMPLEMENT2;
        sqlQuery = String.format(sqlQuery, code);
        String unite = (String) myDao.getDaoImpl().getSingleResult(sqlQuery);
        return unite;
    }

    public static BigDecimal getMontantTotalDuDetailNoteCalculv2(String noteCalcul) {

        String sqlParam = "('".concat(noteCalcul.trim()).concat("')");
        String sqlQuery = SQLQueryTaxation.GetQuery.GET_MONTANT_TOTAL_DETAIL_NC;
        sqlQuery = String.format(sqlQuery, sqlParam);
        BigDecimal result = myDao.getDaoImpl().getSingleResultToBigDecimal2(sqlQuery);
        return result;
    }

    public static FichePriseCharge getPriseEnChargeByNP(String numeroNP) {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_FICHE_PRISE_CHARGE;
        List<FichePriseCharge> fpch = (List<FichePriseCharge>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(FichePriseCharge.class), false, numeroNP);
        return fpch.isEmpty() ? null : fpch.get(0);
    }

    public static String createNoteCaculv2(NewNoteCalcul newNoteCalcul,
            List<ArticleBudgetaireTaxation> listArticleBudgetaireTaxation,
            List<BienTaxation> bienList,
            List<ArchiveData> documentList,
            String observationDoc, ComplementInfoTaxe complementInfoTaxe) {

        PropertiesMessage propertiesMessage = null;
        String result = GeneralConst.EMPTY_STRING;

        try {
            propertiesMessage = new PropertiesMessage();
        } catch (IOException ex) {
            Logger.getLogger(TaxationBusiness.class.getName()).log(Level.SEVERE, null, ex);
        }

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int counterNC = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> paramsNC = new HashMap<>();

        String firstStoredProcedure = "F_NEW_NOTE_CALCUL4";
        String firstStoredReturnValueKey = "NC_RETURN";

        String devise = GeneralConst.EMPTY_STRING;

        paramsNC.put("PERSONNE", newNoteCalcul.getCodePersonne().trim());
        paramsNC.put("ADRESSE", newNoteCalcul.getCodeAdresse().trim());
        paramsNC.put("EXERCICE", newNoteCalcul.getExerciceFiscal().trim());
        paramsNC.put("SITE", newNoteCalcul.getCodeSite().trim());
        paramsNC.put("SERVICE", newNoteCalcul.getCodeService().trim());
        paramsNC.put("AGENT_CREAT", newNoteCalcul.getAgentCreate());
        paramsNC.put("DATE_CREAT", newNoteCalcul.getDateCreate().trim());
        paramsNC.put("DEPOT_DECLARATION", newNoteCalcul.getCodeDepot());
        paramsNC.put("BIEN", newNoteCalcul.getCodeBien());
        paramsNC.put("CPI_FICHE", "");

        for (ArticleBudgetaireTaxation abt : listArticleBudgetaireTaxation) {

            devise = abt.getDevise().trim();

            counterNC++;

            firstBulk.put(counterNC + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_DNC, new Object[]{
                abt.getArticleBudgetaire().getCode().trim(),
                abt.getTaux(),
                abt.getBaseCalcul(),
                abt.getMontantDu(),
                abt.getQte(),
                Integer.valueOf(GeneralConst.Number.ZERO),
                abt.getTypeTaux().trim(),
                Integer.valueOf(GeneralConst.Number.ZERO),
                abt.getCodeTarif().trim(),
                abt.getDevise().trim()

            });

        }

        for (BienTaxation bienTaxation : bienList) {

            counterNC++;

            firstBulk.put(counterNC + SQLQueryTaxation.ExecuteQuery.INSERT_BIEN_TATXATION, new Object[]{
                bienTaxation.getBienId(),
                bienTaxation.getMessage()
            });
        }

        if (documentList.size() > 0) {
            int i = 0;
            for (ArchiveData ad : documentList) {

                String docu = ad.getData();
                //String directory = ad.getFile();
                i++;
                if (i > 1) {
                    observationDoc = GeneralConst.EMPTY_STRING;
                }

                counterNC++;
                firstBulk.put(counterNC + ":EXEC F_NEW_ARCHIVE_ACCUSE_RECEPTION '%s',?1,?2,'%s',?3", new Object[]{
                    "DECLARATION TAXATION", docu, observationDoc
                });
            }
        }

        if (complementInfoTaxe != null) {

            counterNC++;

            firstBulk.put(counterNC + ":INSERT INTO T_COMPLEMENT_INFO_TAXE (TRANSPORTEUR,NATURE_PRODUIT,NUMERO_PLAQUE,MODE_PAIEMENT,FK_NOTE_CALCUL,ETAT) VALUES (?1,?2,?3,?4,'%s',1)", new Object[]{
                complementInfoTaxe.getTransporteur(),
                complementInfoTaxe.getNatureProduit(),
                complementInfoTaxe.getNumeroPlaque(),
                complementInfoTaxe.getModePaiement()
            });
        }

        counterNC++;

        //counterNC++;
        firstBulk.put(counterNC + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION_NC_V2, new Object[]{
            "Taxation avec succès",
            GeneralConst.Numeric.ZERO,
            GeneralConst.Numeric.ZERO,
            "Note de taxation conforme",
            devise
        });

        result = myDao.getDaoImpl().execBulkQueryv4(
                firstStoredProcedure, firstStoredReturnValueKey, paramsNC, firstBulk);

        return result;
    }

    public static String createNoteCaculv3(NewNoteCalcul newNoteCalcul,
            List<ArticleBudgetaireTaxation> listArticleBudgetaireTaxation,
            List<ArchiveData> documentList,
            List<DetailPenaliteMock> listPenalite,
            String observationDoc
    ) {

        PropertiesMessage propertiesMessage = null;

        try {
            propertiesMessage = new PropertiesMessage();
        } catch (IOException ex) {
            Logger.getLogger(TaxationBusiness.class.getName()).log(Level.SEVERE, null, ex);
        }

        int periode = GeneralConst.Numeric.ZERO;

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        HashMap<String, Object[]> secondBulk = new HashMap<>();
        int counterNC = GeneralConst.Numeric.ZERO;
        int counterFPC = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> paramsNC = new HashMap<>();
        HashMap<String, Object> paramsFPC = new HashMap<>();

        String firstStoredProcedure = "F_NEW_NOTE_CALCUL4";
        String firstStoredReturnValueKey = "NC_RETURN";
        String secondStoredProcedure = "F_NEW_FPC_WEB";
        String secondStoredReturnValueKey = "CODE_FPC";

        String secondParamskey = "NUMERO_REFERENCE";

        String devise = GeneralConst.EMPTY_STRING;

        paramsNC.put("PERSONNE", newNoteCalcul.getCodePersonne().trim());
        paramsNC.put("ADRESSE", newNoteCalcul.getCodeAdresse().trim());
        paramsNC.put("EXERCICE", newNoteCalcul.getExerciceFiscal().trim());
        paramsNC.put("SITE", newNoteCalcul.getCodeSite().trim());
        paramsNC.put("SERVICE", newNoteCalcul.getCodeService().trim());
        paramsNC.put("AGENT_CREAT", newNoteCalcul.getAgentCreate());
        paramsNC.put("DATE_CREAT", newNoteCalcul.getDateCreate().trim());
        paramsNC.put("DEPOT_DECLARATION", newNoteCalcul.getCodeDepot());
        paramsNC.put("BIEN", newNoteCalcul.getCodeBien());
        paramsNC.put("CPI_FICHE", "");

        BigDecimal totalMontantDu = new BigDecimal(BigInteger.ZERO);

        String codeAB = GeneralConst.EMPTY_STRING;

        for (ArticleBudgetaireTaxation abt : listArticleBudgetaireTaxation) {

            periode = Integer.valueOf(abt.getPeriodeDeclaration());
            devise = abt.getDevise();
            codeAB = abt.getArticleBudgetaire().getCode();

            totalMontantDu = totalMontantDu.add(BigDecimal.valueOf(Double.valueOf(String.valueOf(abt.getMontantDu()))));

            counterNC++;
            firstBulk.put(counterNC + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_DNC, new Object[]{
                abt.getArticleBudgetaire().getCode().trim(),
                abt.getTaux(),
                abt.getBaseCalcul(),
                abt.getMontantDu(),
                abt.getQte(),
                abt.isPenaliser(),
                abt.getTypeTaux().trim(),
                periode,
                abt.getCodeTarif().trim(),
                abt.getDevise().trim()

            });

        }

        ArticleBudgetaireTaxation abt = listArticleBudgetaireTaxation.get(GeneralConst.Numeric.ZERO);
        if (periode > GeneralConst.Numeric.ZERO) {

            counterNC++;
            firstBulk.put(counterNC
                    //+ ":UPDATE T_PERIODE_DECLARATION SET EST_EXONERE = 1 WHERE ID = ?1",
                    + ":UPDATE T_PERIODE_DECLARATION SET NOTE_CALCUL = '%s' WHERE ID = ?1",
                    new Object[]{
                        periode
                    });

        }

        if (!listPenalite.isEmpty()) {

            counterNC++;
            firstBulk.put(counterNC + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION_NC_V2, new Object[]{
                "Déclaration tardive",
                GeneralConst.Numeric.ZERO,
                GeneralConst.Numeric.ZERO,
                "Application des intérêts moratoires",
                devise
            });

            counterFPC++;

            BigDecimal amountPen = new BigDecimal("0");
            BigDecimal amountPricipal = new BigDecimal("0");

            for (DetailPenaliteMock dpm : listPenalite) {

                amountPen = amountPen.add(dpm.getAmountPenalite());
                amountPricipal = amountPricipal.add(dpm.getAmountPrincipal());

                String sqlQuery = ":EXEC F_NEW_DETAIL_FPC_WEB '%s',?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11";

                secondBulk.put(counterFPC + sqlQuery, new Object[]{
                    dpm.getAmountPrincipal(),
                    dpm.getAmountPenalite(),
                    dpm.getTypeTaux(),
                    dpm.getTaux(),
                    null,
                    dpm.getCodePenalite(),
                    dpm.getNumberMonth(),
                    periode,
                    null,
                    dpm.getAmountPrincipal(),
                    dpm.getExercice(),});
            }

            FichePriseCharge fpc = new FichePriseCharge();

            fpc.setTotalPrincipaldu(amountPricipal);
            fpc.setTotalPenalitedu(amountPen);
            fpc.setFkMed(null);
            fpc.setFkArticleBudgetaire(codeAB);
            fpc.setPersonne(new Personne(newNoteCalcul.getCodePersonne()));
            fpc.setCasFpc(String.valueOf(1));
            fpc.setDevise(devise);
            fpc.setNumeroReference(periode + "");

            paramsFPC.put("TOTAL_PRINCIPAL_DU", fpc.getTotalPrincipaldu());
            paramsFPC.put("TOTAL_PENALITE_DU", fpc.getTotalPenalitedu());
            paramsFPC.put("PERSONNE", fpc.getPersonne().getCode());
            paramsFPC.put("DEVISE", fpc.getDevise());
            paramsFPC.put("NUMERO_REFERENCE", periode);
            paramsFPC.put("CAS_FPC", fpc.getCasFpc());
            paramsFPC.put("ARTICLE_BUDGETAIRE", fpc.getFkArticleBudgetaire());
            paramsFPC.put("MED", null);
        }

        if (documentList.size() > 0) {
            int i = 0;
            for (ArchiveData ad : documentList) {

                String docu = ad.getData();
                String directory = ad.getFile();
                i++;
                if (i > 1) {
                    observationDoc = GeneralConst.EMPTY_STRING;
                }

                counterNC++;
                firstBulk.put(counterNC + ":EXEC F_NEW_ARCHIVE_ACCUSE_RECEPTION ?1,?2,?3,'%s',?4", new Object[]{
                    periode, "DECLARATION", docu, observationDoc
                });
            }
        }

        counterNC++;
        firstBulk.put(counterNC
                + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION_NC_V2, new Object[]{
                    "Taxation",
                    GeneralConst.Numeric.ZERO,
                    GeneralConst.Numeric.ZERO,
                    "Taxation avec succès",
                    devise
                }
        );

        String result;

        try {
            if (!listPenalite.isEmpty()) {

                result = myDao.getDaoImpl().execBulkQueryv5(paramsNC,
                        paramsFPC,
                        firstBulk,
                        secondBulk,
                        firstStoredProcedure,
                        secondStoredProcedure,
                        firstStoredReturnValueKey,
                        secondStoredReturnValueKey,
                        secondParamskey);

            } else {

                result = myDao.getDaoImpl().execBulkQueryv4(
                        firstStoredProcedure, firstStoredReturnValueKey, paramsNC, firstBulk);

            }
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    public static String createNoteCaculv4(NewNoteCalcul newNoteCalcul,
            List<ArticleBudgetaireTaxation> listArticleBudgetaireTaxation,
            List<ArchiveData> documentList,
            String observationDoc, ComplementFraisCertification complementFraisCertification, String dateOrdo, NotePerception np, String procCourte) {

        PropertiesMessage propertiesMessage = null;
        String result = GeneralConst.EMPTY_STRING;

        try {
            propertiesMessage = new PropertiesMessage();
        } catch (IOException ex) {
            Logger.getLogger(TaxationBusiness.class.getName()).log(Level.SEVERE, null, ex);
        }

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int counterNC = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> paramsNC = new HashMap<>();

        String firstStoredProcedure = "F_NEW_NOTE_CALCUL4";
        String firstStoredReturnValueKey = "NC_RETURN";

        String devise = GeneralConst.EMPTY_STRING;

        paramsNC.put("PERSONNE", newNoteCalcul.getCodePersonne().trim());
        paramsNC.put("ADRESSE", newNoteCalcul.getCodeAdresse().trim());
        paramsNC.put("EXERCICE", newNoteCalcul.getExerciceFiscal().trim());
        paramsNC.put("SITE", newNoteCalcul.getCodeSite().trim());
        paramsNC.put("SERVICE", newNoteCalcul.getCodeService().trim());
        paramsNC.put("AGENT_CREAT", newNoteCalcul.getAgentCreate());
        paramsNC.put("DATE_CREAT", newNoteCalcul.getDateCreate().trim());
        paramsNC.put("DEPOT_DECLARATION", newNoteCalcul.getCodeDepot());
        paramsNC.put("BIEN", null);
        paramsNC.put("CPI_FICHE", "");

        for (ArticleBudgetaireTaxation abt : listArticleBudgetaireTaxation) {

            devise = abt.getDevise().trim();

            counterNC++;

            firstBulk.put(counterNC + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_DNC, new Object[]{
                abt.getArticleBudgetaire().getCode().trim(),
                abt.getTaux(),
                abt.getBaseCalcul(),
                abt.getMontantDu(),
                abt.getQte(),
                Integer.valueOf(GeneralConst.Number.ZERO),
                abt.getTypeTaux().trim(),
                Integer.valueOf(GeneralConst.Number.ZERO),
                abt.getCodeTarif().trim(),
                abt.getDevise().trim()

            });

        }

        counterNC++;
        firstBulk.put(counterNC + ":EXEC F_CLOTURE_NC '%s',?1,?2,?3", new Object[]{
            1, newNoteCalcul.getAgentCreate(), "Taxation conforme"
        });

        if (procCourte.equals("1")) {

            counterNC++;

            firstBulk.put(counterNC + ":EXEC F_NEW_NOTE_PERCEPTION_WEB_VG_V2 '%s', ?1,?2,?3,?4,?5,?6,?7,?8,?9", new Object[]{
                np.getNetAPayer(),
                0,
                np.getNetAPayer(),
                newNoteCalcul.getExerciceFiscal().trim(),
                newNoteCalcul.getCodeSite().trim(),
                newNoteCalcul.getCodeService().trim(),
                np.getDateEcheancePaiement().trim(),
                np.getAgentCreat().trim(),
                np.getDateCreat().trim()
            });

            counterNC++;
            firstBulk.put(counterNC + ":UPDATE T_NOTE_CALCUL SET AGENT_VALIDATION = ?1,DATE_VALIDATION = CONVERT(DATE,?2,103), "
                    + " AVIS = ?3, ETAT = ?4, OBSERVATION_ORDONNANCEMENT = ?5 WHERE NUMERO = '%s'", new Object[]{
                        np.getAgentCreat().trim(),
                        dateOrdo, "AV0012015",
                        1, "Taxation conforme"
                    });
        }

        if (documentList.size() > 0) {
            int i = 0;
            for (ArchiveData ad : documentList) {

                String docu = ad.getData();
                i++;
                if (i > 1) {
                    observationDoc = GeneralConst.EMPTY_STRING;
                }

                counterNC++;
                firstBulk.put(counterNC + ":EXEC F_NEW_ARCHIVE_ACCUSE_RECEPTION '%s',?1,?2,'%s',?3", new Object[]{
                    "DECLARATION TAXATION", docu, observationDoc
                });
            }
        }

        if (complementFraisCertification != null) {

            counterNC++;

            firstBulk.put(counterNC + ":INSERT INTO T_COMPLEMENT_FRAIS_CERTIFICATION (NOTE_VERSEMENT,AGENCE,PRODUIT,NOMBRE_LOT,COTE_PART,"
                    + " BANQUE,COMPTE_BANCAIRE,DATE_TAXATION,FK_AB,FK_NOTE_CALCUL) VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,'%s')", new Object[]{
                        complementFraisCertification.getNoteVersement(),
                        complementFraisCertification.getAgence(),
                        complementFraisCertification.getProduit(),
                        complementFraisCertification.getNumeroLot(),
                        complementFraisCertification.getCotePart(),
                        complementFraisCertification.getBanque(),
                        complementFraisCertification.getCompteBancaire(),
                        complementFraisCertification.getDateTaxation(),
                        complementFraisCertification.getFkAb()
                    });
        }

        counterNC++;

        //counterNC++;
        firstBulk.put(counterNC + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION_NC_V2, new Object[]{
            "Taxation avec succès",
            GeneralConst.Numeric.ZERO,
            GeneralConst.Numeric.ZERO,
            "Note de taxation conforme",
            devise
        });

        result = myDao.getDaoImpl().execBulkQueryv4(
                firstStoredProcedure, firstStoredReturnValueKey, paramsNC, firstBulk);

        return result;
    }

    //ordonancement classique et pénalisé
    public static String saveNotePerceptionV3(NotePerception np, boolean useGenerate, Serie newSerie, String codeSite,
            String codeService, float net, String dateOrdonnancement, int etatOrdonnancement, String idAvis) {

        HashMap<String, Object> paramsNP = new HashMap<>();
        HashMap<String, Object[]> firstBulk = new HashMap<>();

        String result = GeneralConst.EMPTY_STRING;
        String deviseNc = GeneralConst.EMPTY_STRING;
        String notePerceptionManuel = GeneralConst.EMPTY_STRING;

        int counterNP = GeneralConst.Numeric.ZERO;

        Avis avis = getAvisById(idAvis.trim());

        if (avis != null) {

            switch (avis.getCode().trim()) {

                case TaxationConst.OK:

                    if (useGenerate) {

                        Integer currentNbre = 0;

                        Serie currentSerie = NotePerceptionBusiness.getLastSerieBySite(codeSite);

                        if (currentSerie != null) {

                            if (currentSerie.getNombreNoteImprimee() == null) {
                                currentNbre = 0;
                            } else {
                                currentNbre = currentSerie.getNombreNoteImprimee();
                            }

                            newSerie = getNexSerie(currentSerie);

                            if (!newSerie.getDernierNumeroImprime().equals(GeneralConst.EMPTY_STRING)) {

                                notePerceptionManuel = newSerie.getDernierNumeroImprime();

                                Integer totalPrint = currentNbre + currentSerie.getDebut();

                                counterNP++;
                                firstBulk.put(counterNP + SQLQueryOrdonnancement.UPDATE_SERIE, new Object[]{
                                    totalPrint.equals(currentSerie.getFin()),
                                    newSerie.getDernierNumeroImprime(),
                                    newSerie.getNombreNoteImprimee(),
                                    newSerie.getId()
                                });
                            }
                        } else {
                            return GeneralConst.ResultCode.SERIE_OVER;
                        }
                    }

                    String firstStoredReturnValueKey = "NP";
                    String firstStoredProcedure = "F_NEW_NOTE_PERCEPTION_WEB_VG";

                    paramsNP.put("NC", np.getNoteCalcul().getNumero().trim());
                    paramsNP.put("TOTAL", np.getNetAPayer());
                    paramsNP.put("REMISE", 0);
                    paramsNP.put("NET_A_PAYER", net);
                    paramsNP.put("PAYER", 0);
                    paramsNP.put("SOLDE", net);
                    paramsNP.put("EXERCICE", np.getNoteCalcul().getExercice().trim());
                    paramsNP.put("SITE", codeSite.trim());
                    paramsNP.put("SERVICE", codeService.trim());
                    paramsNP.put("DATE_ECHEANCE_PAIEMENT", np.getDateEcheancePaiement().trim());

                    paramsNP.put("AGENT_CREAT", np.getAgentCreat().trim());
                    paramsNP.put("FRACTIONNEE", Integer.valueOf(GeneralConst.Number.ZERO));
                    paramsNP.put("NP_MERE", GeneralConst.EMPTY_STRING);
                    paramsNP.put("NUMERO_MANUEL", np.getNotePerceptionManuel());
                    paramsNP.put("DATE_CREAT", np.getDateCreat().trim());
                    paramsNP.put("AMR", GeneralConst.EMPTY_STRING);

                    DetailsNc detailsNc = getOneLineDetailsNcByNC(np.getNoteCalcul().getNumero().trim());

                    if (detailsNc != null) {

                        if (detailsNc.getDevise() != null) {
                            deviseNc = detailsNc.getDevise().toUpperCase();
                        } else {
                            deviseNc = GeneralConst.Devise.DEVISE_CDF;
                        }
                    }

                    counterNP++;
                    firstBulk.put(counterNP + SQLQueryOrdonnancement.UPDATE_NOTE_CALCUL, new Object[]{
                        np.getNoteCalcul().getAgentValidation().trim(), dateOrdonnancement,
                        np.getNoteCalcul().getAvis().trim(),
                        1, np.getNoteCalcul().getObservation(),
                        np.getNoteCalcul().getNumero().trim()
                    });

                    counterNP++;
                    firstBulk.put(counterNP + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION_NC_V3, new Object[]{
                        np.getNoteCalcul().getNumero().trim(),
                        "Ordonnancement de la note de taxation",
                        net, 0, "Note de taxation conforme",
                        deviseNc
                    });

                    result = myDao.getDaoImpl().execBulkQueryv4(firstStoredProcedure, firstStoredReturnValueKey, paramsNP, firstBulk);

                    break;

                case TaxationConst.KO:

                    String observationRejet = "Note de taxation rejeté à l'ordonnancement";

                    counterNP++;
                    firstBulk.put(counterNP + SQLQueryOrdonnancement.UPDATE_NOTE_CALCUL, new Object[]{
                        np.getNoteCalcul().getAgentValidation().trim(), dateOrdonnancement,
                        np.getNoteCalcul().getAvis().trim(),
                        0, np.getNoteCalcul().getObservationOrdonnancement(),
                        np.getNoteCalcul().getNumero().trim()
                    });

                    counterNP++;
                    firstBulk.put(counterNP + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
                        np.getNoteCalcul().getNumero().trim(),
                        null, observationRejet, 0, 0,
                        observationRejet, deviseNc
                    });

                    boolean res = myDao.getDaoImpl().executeBulkQuery(firstBulk);

                    result = (res) ? TaxationConst.KO : GeneralConst.EMPTY_STRING;

                    break;
            }
        }

        return result;
    }

    public static List<ArticleBudgetaire> getListArticleBudgetaireConducteurMoto(String paramCode) {

        String sqlQuery = SQLQueryTaxation.MasterQuery.SELECT_LIST_ARTICLE_BUDGETAIRE_CONDUCTEUR_MOTO;
        sqlQuery = String.format(sqlQuery, paramCode);

        List<ArticleBudgetaire> listArticleBudgetaire = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, paramCode);
        return listArticleBudgetaire;
    }

    public static String saveDemandeFractionnement(
            DemandeEchelonnement demandeEchelonnement, JSONArray detailEchelonnement,
            List<String> documentList, String fkDocument, String observationDoc, String codePersonne) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = GeneralConst.Numeric.ZERO;

        HashMap<String, Object> paramsStored = new HashMap<>();

        String storedProcedure = "F_NEW_DEMANDE_ECHELONNEMENT";
        String storedReturnValueKey = "CODE";

        paramsStored.put("REFERENCE_LETTRE", demandeEchelonnement.getReferenceLettre());
        paramsStored.put("OBSERVATION", demandeEchelonnement.getObservation());
        paramsStored.put("PERSONNE", codePersonne);
        paramsStored.put("AGENT_CREAT", demandeEchelonnement.getAgentCreat());
        paramsStored.put("FK_DIVISION", demandeEchelonnement.getFkDivision());
        paramsStored.put("FK_BUREAU", demandeEchelonnement.getFkBureau());
        paramsStored.put("APPLY_TEN_PERCENT", demandeEchelonnement.getApplyTenPercent());

        PropertiesMessage propertiesMessage = null;

        try {
            propertiesMessage = new PropertiesMessage();
        } catch (IOException ex) {
            Logger.getLogger(FractionnementBusiness.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (int i = 0; i < detailEchelonnement.length(); i++) {

            try {

                JSONObject jsonObject = detailEchelonnement.getJSONObject(i);

                String typDoc = jsonObject.getString("TYPE_DOCUMENT");
                String refDoc = jsonObject.getString("referenceDocument");

                counter++;
                bulkQuery.put(counter + SQLQueryFractionnement.SAVE_DETAIL_ECHELONNEMENT, new Object[]{refDoc, typDoc});

                if (!typDoc.equals(DocumentConst.DocumentCode.BON_A_PAYER)) {

                    NotePerception np = NotePerceptionBusiness.getNotePerceptionByCode(refDoc);

                    if (np != null) {

                        counter++;
                        bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
                            np.getNoteCalcul().getNumero().trim(),
                            np.getNumero().trim(),
                            propertiesMessage.getContent(PropertiesConst.Message.DEMANDE_ECHELONNEMENT),
                            GeneralConst.Numeric.ZERO,
                            GeneralConst.Numeric.ZERO,
                            propertiesMessage.getContent(PropertiesConst.Message.DEMANDE_ECHELONNEMENT) + GeneralConst.TWO_POINTS + np.getNumero().trim(),
                            np.getDevise()
                        });
                    }

                } else {

//                    BonAPayer bonAPayer = PaiementBusiness.getBonAPayerByBpManuel(refDoc.trim());
//
//                    if (bonAPayer != null) {
//
//                        counter++;
//                        bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
//                            bonAPayer.getFkNoteCalcul().getNumero().trim(),
//                            bonAPayer.getCode().trim(),
//                            propertiesMessage.getContent(PropertiesConst.Message.DEMANDE_ECHELONNEMENT),
//                            GeneralConst.Numeric.ZERO,
//                            GeneralConst.Numeric.ZERO,
//                            propertiesMessage.getContent(PropertiesConst.Message.DEMANDE_ECHELONNEMENT) + GeneralConst.TWO_POINTS + bonAPayer.getCode(),
//                            bonAPayer.getDevise()
//                        });
//                    }
                }

            } catch (JSONException ex) {
                Logger.getLogger(FractionnementBusiness.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (documentList.size() > 0) {
            for (String docu : documentList) {
                counter++;
                bulkQuery.put(counter + SQLQueryNotePerception.ARCHIVE_ACCUSER_RECEPTION_v2,
                        new Object[]{
                            DocumentConst.DocumentCode.NP,
                            docu, fkDocument, observationDoc
                        });
            }
        }

        String result = GeneralConst.EMPTY_STRING;

        try {
            result = myDao.getDaoImpl().execBulkQueryv4(storedProcedure, storedReturnValueKey, paramsStored, bulkQuery);
        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static Assujeti getAssujetiId(String id, boolean isCorrection) {

        String sqlQuery;
        if (isCorrection) {
            sqlQuery = "SELECT * FROM T_ASSUJETI WITH (READPAST) "
                    + " WHERE ID IN (SELECT ASSUJETISSEMENT FROM T_PERIODE_DECLARATION WITH (READPAST) WHERE ID = ?1) AND ID NOT IN(SELECT ID_ASS FROM T_ASSUJ_AB WITH (READPAST)) AND ETAT = 1";
        } else {
            sqlQuery = "SELECT * FROM T_ASSUJETI WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";

        }

        List<Assujeti> assujetis = (List<Assujeti>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Assujeti.class
                ), false, id.trim());
        return assujetis.isEmpty()
                ? null : assujetis.get(0);
    }

    public static Palier getPalierByAbTarifAndFJuridique(String codeArticle, String codeTarif, String codeFormeJ, float valeurBase, boolean isPalier) {

        List<Palier> listPaliers;

        if (!isPalier) {

            String sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST)"
                    + " WHERE ARTICLE_BUDGETAIRE = ?1 AND TARIF = ?2 AND (TYPE_PERSONNE = ?3 OR TYPE_PERSONNE = '*')"
                    + " AND ETAT = 1";
            listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Palier.class), false,
                    codeArticle.trim(), codeTarif.trim(), codeFormeJ.trim());

        } else {

            String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_PALIER_BY_TARIF_AND_FORME_JURIDIQUE_AND_BASE;
            listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Palier.class), false,
                    codeArticle.trim(), codeTarif.trim(), codeFormeJ.trim(), valeurBase);
        }

        return listPaliers.isEmpty() ? null : listPaliers.get(0);
    }

    public static List<PeriodeDeclaration> getListPeriodeDeclarationByAssujettissement(String codeAssujettissement,
            boolean isCorrection) {
        String sqlQuery;

        if (isCorrection) {
            sqlQuery = "SELECT * FROM T_PERIODE_DECLARATION WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";
        } else {
            sqlQuery = "SELECT * FROM T_PERIODE_DECLARATION WITH (READPAST) WHERE ASSUJETISSEMENT = ?1 AND ETAT = 1 "
                    + " AND (NOTE_CALCUL IS NULL OR NOTE_CALCUL = '') ORDER BY DEBUT";

        }

        List<PeriodeDeclaration> listPeriodeDeclaration = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class
                ), false, codeAssujettissement.trim());
        return listPeriodeDeclaration;
    }

    public static PeriodeDeclaration getPeriodeDeclarationId(String idPeriode) {
        String sqlQuery = "SELECT * FROM T_PERIODE_DECLARATION WITH (READPAST) "
                + " WHERE ID = ?1 AND ETAT = 1";
        List<PeriodeDeclaration> periodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class
                ), false, idPeriode.trim());
        return periodeDeclarations.isEmpty()
                ? null : periodeDeclarations.get(0);
    }

    public static List<ArchiveAccuseReception> getListArchiveAccuseByIdDocumenet(String idDocument) {
        String sqlQuery = SQLQueryTaxation.MasterQuery.SELECT_LIST_ARCHIVE_ACCUSER_BY_ID_DOCUMENT;
        List<ArchiveAccuseReception> listAccuserReception = (List<ArchiveAccuseReception>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ArchiveAccuseReception.class),
                false, idDocument.trim());
        return listAccuserReception;
    }

    public static ArticleBudgetaire getAmendeTransactionnelBySecteur(String codeNature, String codeSecteur) {
        String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_AB_AMENDE_BY_SERVICE_ASSIETTE;
        List<ArticleBudgetaire> listArticleBudgetaire = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class
                ), false,
                codeNature.trim(), codeSecteur.trim());
        return listArticleBudgetaire.isEmpty()
                ? null : listArticleBudgetaire.get(0);
    }

    public static Palier getPalierByAbAndTarif(String codeAB, String codeTarif) {
        String sqlQuery = "select * from t_palier p with (readPast) where p.article_budgetaire = ?1 and p.tarif = ?2";
        List<Palier> listPalier = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB.trim(), codeTarif.trim());
        return listPalier.isEmpty() ? null : listPalier.get(0);
    }

    public static Integer getLastIDBonAPayer() {
        String queryStr = SQLQueryMed.F_GET_LAST_ID_BON_A_PAYER;
        Integer result = myDao.getDaoImpl().getSingleResultToInteger(queryStr);
        return result;
    }

    public static List<Bien> getListMotoCycleByPersonne(String codePersonne) {

        String sqlQuery = "SELECT B.* FROM T_BIEN B WITH (READPAST) WHERE B.ETAT = 1 \n"
                + "AND B.TYPE_BIEN = 'TB00000002' \n"
                + "AND B.ID IN (SELECT A.BIEN FROM T_ACQUISITION A WITH (READPAST) WHERE A.PERSONNE = ?1 \n"
                + "AND A.PROPRIETAIRE = 1 AND A.ETAT = 1)";

        List<Bien> listBien = (List<Bien>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Bien.class), false, codePersonne);
        return listBien;
    }

    public static String saveNoteCalculWithShortProcesure(NewNoteCalcul newNoteCalcul, int nbreTax, BigDecimal amount) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        HashMap<String, Object> paramsStored = new HashMap<>();

        String storedProcedure = "P_NEW_OPERATION_COURTE_NEW";
        String storedReturnValueKey = "CPI_RETURN";

        paramsStored.put("PERSONNE", newNoteCalcul.getCodePersonne());
        paramsStored.put("EXERCICE", newNoteCalcul.getExerciceFiscal());
        paramsStored.put("SITE", newNoteCalcul.getCodeSite());
        paramsStored.put("SERVICE", newNoteCalcul.getCodeService());
        paramsStored.put("AGENT_CREAT", newNoteCalcul.getAgentCreate());
        paramsStored.put("BIEN", newNoteCalcul.getCodeBien() == GeneralConst.EMPTY_STRING
                ? null : newNoteCalcul.getCodeBien());
        paramsStored.put("NBRE_TAX", nbreTax);
        paramsStored.put("AMOUNT", amount);

        PropertiesMessage propertiesMessage = null;

        try {
            propertiesMessage = new PropertiesMessage();
        } catch (IOException ex) {
            Logger.getLogger(FractionnementBusiness.class.getName()).log(Level.SEVERE, null, ex);
        }

        String result = GeneralConst.EMPTY_STRING;

        try {
            result = myDao.getDaoImpl().execBulkQueryWithOneLevel(storedProcedure, storedReturnValueKey, paramsStored, bulkQuery);
        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static boolean updateEcheanePayment(
            String query) {

        boolean result;

        result = myDao.getDaoImpl().executeStoredProcedure(query);
        return result;
    }

    public static Integer getDateDiffBetwenTwoDates(String echeance) {
        String queryStr = "SELECT dbo.F_DATEDIFF_BY_MONTH_BETWEN_TWO_DATES('" + echeance.trim() + "')";
        Integer result = myDao.getDaoImpl().getSingleResultToInteger(queryStr);
        return result;
    }

    public static Integer getDateDiffBetwenTwoDates_V2(String debut, String fin) {
        String queryStr = "SELECT dbo.F_DATEDIFF_BY_MONTH_BETWEN_TWO_DATES_V2('" + debut.trim() + "','" + fin.trim() + "')";
        Integer result = myDao.getDaoImpl().getSingleResultToInteger(queryStr);
        //System.out.println("Mois retard : " + result);
        return result;
    }

    public static List<Bien> getListBienTaxation(String nc) {
        String sqlQuery = "SELECT B.* FROM T_BIEN B WITH (READPAST) WHERE B.ID IN (SELECT DISTINCT FK_BIEN "
                + " FROM T_BIEN_NOTE_CALCUL WITH (READPAST) WHERE FK_NC = ?1 AND ETAT = 1) ORDER BY B.INTITULE";
        List<Bien> listBiens = (List<Bien>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Bien.class), false, nc);
        return listBiens;
    }

    public static BienNoteCalcul getBienByNoteCalcul(String nc) {
        String sqlQuery = "select top 1 * FROM T_BIEN_NOTE_CALCUL WITH (READPAST) WHERE FK_NC = ?1 AND ETAT = 1";
        List<BienNoteCalcul> listBienNoteCalcul = (List<BienNoteCalcul>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(BienNoteCalcul.class), false, nc);
        return listBienNoteCalcul.isEmpty() ? null : listBienNoteCalcul.get(0);
    }

    public static BienNoteCalcul getBienByNoteCalculAndBien(String nc, String bien) {
        String sqlQuery = "select * FROM T_BIEN_NOTE_CALCUL WITH (READPAST) WHERE FK_NC = ?1 AND FK_BIEN = ?2 AND ETAT = 1";
        List<BienNoteCalcul> listBienNoteCalcul = (List<BienNoteCalcul>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(BienNoteCalcul.class), false, nc, bien);
        return listBienNoteCalcul.isEmpty() ? null : listBienNoteCalcul.get(0);
    }

    public static Acquisition getAcquisitionByBienAndPersonne(String bien, String nc) {
        String sqlQuery = "SELECT a.* from T_ACQUISITION a WITH (READPAST) WHERE a.BIEN = ?1 \n"
                + " and a.PERSONNE = (select nc.PERSONNE from T_NOTE_CALCUL nc WITH (READPAST) WHERE nc.NUMERO = ?2)";
        List<Acquisition> listAcquisition = (List<Acquisition>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Acquisition.class), false, bien, nc);
        return listAcquisition.isEmpty() ? null : listAcquisition.get(0);
    }

    public static List<ArchiveTaxation> getListDeclarationByNc(String numeroNc) {
        String sqlQuery = "SELECT AT.* FROM T_ARCHIVE_TAXATION AT WITH (READPAST) WHERE AT.FK_NC = ?1";
        List<ArchiveTaxation> listDeclarations = (List<ArchiveTaxation>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ArchiveTaxation.class
                ), false, numeroNc.trim());
        return listDeclarations;
    }

    public static List<ArchiveAccuseReception> getListArchiveDocumentTaxationByNc(String numeroNc) {
        String sqlQuery = "select a.* from T_ARCHIVE_ACCUSE_RECEPTION  a WITH (READPAST) WHERE a.DOCUMENT_REFERENCE = ?1";
        List<ArchiveAccuseReception> listDeclarations = (List<ArchiveAccuseReception>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ArchiveAccuseReception.class
                ), false, numeroNc.trim());
        return listDeclarations;
    }

    public static ComplementInfoTaxe getComplementInfoTaxeByID(int id) {
        String sqlQuery = "select * FROM T_COMPLEMENT_INFO_TAXE WITH (READPAST) WHERE ID = ?1";
        List<ComplementInfoTaxe> complementInfoTaxes = (List<ComplementInfoTaxe>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ComplementInfoTaxe.class), false, id);
        return complementInfoTaxes.isEmpty() ? null : complementInfoTaxes.get(0);
    }

    public static ComplementInfoTaxe getComplementInfoTaxeByNP(String np) {
        String sqlQuery = "select * FROM T_COMPLEMENT_INFO_TAXE WITH (READPAST) WHERE FK_NOTE_PERCEPTION = ?1";
        List<ComplementInfoTaxe> complementInfoTaxes = (List<ComplementInfoTaxe>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ComplementInfoTaxe.class), false, np);
        return complementInfoTaxes.isEmpty() ? null : complementInfoTaxes.get(0);
    }

    public static ComplementInfoTaxe getComplementInfoTaxeByNC(String nc) {
        String sqlQuery = "select * FROM T_COMPLEMENT_INFO_TAXE WITH (READPAST) WHERE FK_NOTE_CALCUL = ?1";
        List<ComplementInfoTaxe> complementInfoTaxes = (List<ComplementInfoTaxe>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ComplementInfoTaxe.class), false, nc);
        return complementInfoTaxes.isEmpty() ? null : complementInfoTaxes.get(0);
    }

    public static List<ComplementInfoTaxe> getListComplementInfoTaxeByNC(String numeroNc) {
        String sqlQuery = "select * FROM T_COMPLEMENT_INFO_TAXE WITH (READPAST) WHERE FK_NOTE_CALCUL = ?1";
        List<ComplementInfoTaxe> complementInfoTaxes = (List<ComplementInfoTaxe>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ComplementInfoTaxe.class
                ), false, numeroNc.trim());
        return complementInfoTaxes;
    }

    public static List<ComplementInfoTaxe> getListComplementInfoTaxeByNP(String numeroNp) {
        String sqlQuery = "select * FROM T_COMPLEMENT_INFO_TAXE WITH (READPAST) WHERE FK_NOTE_PERCEPTION = ?1 ORDER BY ID";
        List<ComplementInfoTaxe> complementInfoTaxes = (List<ComplementInfoTaxe>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ComplementInfoTaxe.class
                ), false, numeroNp.trim());
        return complementInfoTaxes;
    }

    public static List<ComplementInfoTaxe> getListComplementInfoTaxeByMinierAndTaxe(String codeMinier, String codeTaxe) {
        String sqlQuery = "select * FROM T_COMPLEMENT_INFO_TAXE WITH (READPAST) "
                + " WHERE FK_NOTE_PERCEPTION IN (SELECT NUMERO FROM T_NOTE_PERCEPTION NP WITH (READPAST) "
                + " WHERE NP.NOTE_CALCUL IN (SELECT NC.NUMERO FROM T_NOTE_CALCUL NC WITH (READPAST) "
                + " WHERE NC.PERSONNE = ?1 AND NC.NUMERO IN (SELECT DNC.NOTE_CALCUL FROM T_DETAILS_NC DNC WITH (READPAST) "
                + " WHERE DNC.ARTICLE_BUDGETAIRE = ?2))) ORDER BY ID";
        List<ComplementInfoTaxe> complementInfoTaxes = (List<ComplementInfoTaxe>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ComplementInfoTaxe.class
                ), false, codeMinier.trim(), codeTaxe);
        return complementInfoTaxes;
    }

    public static List<Personne> getListPersonneMinier() {

        String sqlQuery = "select p.* from T_PERSONNE p WITH (READPAST) "
                + " WHERE p.CODE IN (SELECT nc.PERSONNE FROM T_NOTE_CALCUL nc WITH (READPAST) "
                + " WHERE nc.NUMERO IN (SELECT dnc.NOTE_CALCUL FROM T_DETAILS_NC dnc WITH (READPAST) "
                + " WHERE dnc.ARTICLE_BUDGETAIRE IN ('00000000000001212016','00000000000001072016'))) AND p.ETAT = 1 ORDER BY p.NOM";

        List<Personne> listDeclarations = (List<Personne>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Personne.class), false);
        return listDeclarations;
    }

    public static List<Jdossier> getListSousprovisionByMinier(String codePersonne, String periode, String dateDebut, String dateFin) {

        String sqlQuery = "";

        String sqlParamPeriode = "";

        if (!periode.equals("*")) {
            sqlParamPeriode = " AND DATEPART(YEAR,j.DATE_CREAT) = %s";
            sqlParamPeriode = String.format(sqlParamPeriode, periode);
        }

        String sqlParamDates = " AND DBO.FDATE(j.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamDates = String.format(sqlParamDates, dateDebut, dateFin);

        switch (codePersonne) {
            case "*":
                sqlQuery = "select j.* FROM T_JDOSSIER j WITH (READPAST) JOIN T_PERSONNE p WITH (READPAST) ON j.DOSSIER = p.CODE"
                        + " WHERE FK_AB IN ('00000000000001212016','00000000000001072016') %s %s ORDER BY p.NOM";
                sqlQuery = String.format(sqlQuery, sqlParamPeriode, sqlParamDates);
                break;
            default:
                sqlQuery = "select j.* FROM T_JDOSSIER j WITH (READPAST) JOIN T_ARTICLE_BUDGETAIRE a ON j.FK_AB = a.CODE"
                        + " WHERE DOSSIER = '%s' %s %s ORDER BY a.INTITULE";
                sqlQuery = String.format(sqlQuery, codePersonne, sqlParamPeriode, sqlParamDates);
                break;
        }

        List<Jdossier> jdossiers = (List<Jdossier>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Jdossier.class), false);
        return jdossiers;
    }

    public static List<Jdossier> getListSousprovisionByMinierByTaxeCode(String codeTaxe) {

        String sqlQuery = "";

        sqlQuery = "select j.* FROM T_JDOSSIER j WITH (READPAST) JOIN T_PERSONNE p WITH (READPAST) ON j.DOSSIER = p.CODE"
                + " WHERE FK_AB = ?1 ORDER BY p.NOM";

        List<Jdossier> jdossiers = (List<Jdossier>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Jdossier.class), false, codeTaxe);
        return jdossiers;
    }

    public static List<Jdossier> getListSousprovisionByMinierByTaxeCodeV2(String codeTaxe, String codePersonne, String periode, String dateDebut, String dateFin) {

        String sqlQuery = "";

        String sqlParamPeriode = "";

        if (!periode.equals("*")) {
            sqlParamPeriode = " AND DATEPART(YEAR,j.DATE_CREAT) = %s";
            sqlParamPeriode = String.format(sqlParamPeriode, periode);
        }

        String sqlParamDates = " AND DBO.FDATE(j.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamDates = String.format(sqlParamDates, dateDebut, dateFin);

        switch (codePersonne) {
            case "*":
                sqlQuery = "select j.* FROM T_JDOSSIER j WITH (READPAST) JOIN T_PERSONNE p WITH (READPAST) ON j.DOSSIER = p.CODE"
                        + " WHERE FK_AB = ?1 %s %s ORDER BY p.NOM";
                sqlQuery = String.format(sqlQuery, sqlParamPeriode, sqlParamDates);
                break;
            default:
                sqlQuery = "select j.* FROM T_JDOSSIER j WITH (READPAST) JOIN T_ARTICLE_BUDGETAIRE a ON j.FK_AB = a.CODE"
                        + " WHERE DOSSIER = '%s' %s %s FK_AB = ?1 ORDER BY a.INTITULE";
                sqlQuery = String.format(sqlQuery, codePersonne, sqlParamPeriode, sqlParamDates);
                break;
        }

        List<Jdossier> jdossiers = (List<Jdossier>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Jdossier.class), false, codeTaxe);
        return jdossiers;
    }

    public static boolean updateManifesteCaseOne(String transitaire, String produit, String plaque, String modepaiement, String np,
            String nc, int codeAgent, BigDecimal tonnage, BigDecimal amount, int manifesteId, int sousProvisionId, String status) {

        boolean result = false;
        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int count = 0;

        try {

            String sqlQueryUpdateManifeste = ":INSERT INTO T_COMPLEMENT_INFO_TAXE (TRANSPORTEUR,NATURE_PRODUIT,NUMERO_PLAQUE,MODE_PAIEMENT,"
                    + " FK_NOTE_CALCUL,FK_NOTE_PERCEPTION,TONNAGE_SORTIE,MONTANT_TONNAGE_SORTIE,AGENT_VALIDATE,DATE_VALIDATION,ETAT,STATUS_MANIFESTE) "
                    + " VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,GETDATE(),1,?10)";

            count++;
            firstBulk.put(count + sqlQueryUpdateManifeste, new Object[]{
                transitaire, produit, plaque, modepaiement, nc, np,
                tonnage, amount, codeAgent, status
            });

            String sqlQueryUpdateSousProvision = ":UPDATE T_JDOSSIER SET TONNAGE_RESTANT = TONNAGE_RESTANT - ?1, AGENT_MAJ = ?2, DATE_MAJ = GETDATE(),"
                    + "DEBIT = DEBIT + ?3, CREDIT = CREDIT - ?3, SOLDE = SOLDE - ?3 WHERE ID = ?4";

            count++;
            firstBulk.put(count + sqlQueryUpdateSousProvision, new Object[]{
                tonnage, codeAgent, amount, sousProvisionId
            });

            result = executeQueryBulkUpdate(firstBulk);

        } catch (Exception e) {
        }

        return result;
    }

    public static boolean updateManifesteCaseTwo(int codeAgent, BigDecimal tonnage, BigDecimal amount,
            int manifesteId, int sousProvisionId, String status) {

        boolean result = false;
        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int count = 0;

        try {

            String sqlQueryUpdateManifeste = ":UPDATE T_COMPLEMENT_INFO_TAXE SET TONNAGE_SORTIE = ?1,MONTANT_TONNAGE_SORTIE = ?2, "
                    + " AGENT_VALIDATE = ?3, DATE_VALIDATION = GETDATE(),STATUS_MANIFESTE = ?4 WHERE ID = ?5";

            count++;
            firstBulk.put(count + sqlQueryUpdateManifeste, new Object[]{
                tonnage, amount, codeAgent, status, manifesteId
            });

            String sqlQueryUpdateSousProvision = ":UPDATE T_JDOSSIER SET TONNAGE_RESTANT = TONNAGE_INITIAL - ?1, AGENT_MAJ = ?2, DATE_MAJ = GETDATE(),"
                    + "DEBIT = DEBIT + ?3, CREDIT = CREDIT - ?3, SOLDE = SOLDE - ?3 WHERE ID = ?4";

            count++;
            firstBulk.put(count + sqlQueryUpdateSousProvision, new Object[]{
                tonnage, codeAgent, amount, sousProvisionId
            });

            result = executeQueryBulkUpdate(firstBulk);

        } catch (Exception e) {
        }

        return result;
    }

    public static ComplementFraisCertification getComplementFraisCertificationByNP(String np) {
        String sqlQuery = "select * FROM T_COMPLEMENT_FRAIS_CERTIFICATION WITH (READPAST) WHERE FK_NOTE_PERCEPTION = ?1";
        List<ComplementFraisCertification> complementInfoTaxes = (List<ComplementFraisCertification>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ComplementFraisCertification.class), false, np);
        return complementInfoTaxes.isEmpty() ? null : complementInfoTaxes.get(0);
    }

    public static ComplementFraisCertification getComplementFraisCertificationByNC(String nc) {
        String sqlQuery = "select * FROM T_COMPLEMENT_FRAIS_CERTIFICATION WITH (READPAST) WHERE FK_NOTE_CALCUL = ?1";
        List<ComplementFraisCertification> complementInfoTaxes = (List<ComplementFraisCertification>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ComplementFraisCertification.class), false, nc);
        return complementInfoTaxes.isEmpty() ? null : complementInfoTaxes.get(0);
    }

    public static String checkNumberNoteDebit(String number) {
        String queryStr = "SELECT dbo.F_CHECK_NOTE_DEBIT_NUMBER('" + number.trim() + "')";
        String result = myDao.getDaoImpl().getSingleResultToString(queryStr);
        return result;
    }

    public static List<Personne> getListSeachPersonnes(String nameLike, String codeForme) {

        String sqlQuery = "";

        /*if (codeForme.equals("0")) {
         sqlQuery = "SELECT p.* FROM T_PERSONNE p WITH (READPAST) WHERE p.NOM LIKE ?1 AND p.ETAT = 1 ORDER BY p.NOM";
         } else {
         sqlQuery = "SELECT p.* FROM T_PERSONNE p WITH (READPAST) WHERE p.FORME_JURIDIQUE = '%s' AND p.NOM LIKE ?1 AND p.ETAT = 1 ORDER BY p.NOM";
         sqlQuery = String.format(sqlQuery, codeForme);
         }*/
        sqlQuery = "SELECT p.* FROM T_PERSONNE p WITH (READPAST) WHERE p.NOM LIKE ?1 AND p.ETAT = 1 ORDER BY p.NOM";

        List<Personne> jdossiers = (List<Personne>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Personne.class), true, nameLike);
        return jdossiers;
    }

    public static List<ArticleBudgetaire> getListAllArticleBudgetaireImpot() {
        String sqlQuery = "SELECT * FROM T_ARTICLE_BUDGETAIRE a WITH (READPAST) WHERE a.ETAT = 1 AND a.ASSUJETISSABLE = 1 ORDER BY a.INTITULE";
        List<ArticleBudgetaire> listSite = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false);
        return listSite;
    }

    public static List<TypeBien> getListAllATypeBiensByCategorie(int categorie) {
        String sqlQuery = "SELECT * FROM T_TYPE_BIEN tb WITH (READPAST)  WHERE tb.ETAT = 1 \n"
                + "AND tb.CODE IN (SELECT tbs.TYPE_BIEN FROM T_TYPE_BIEN_SERVICE tbs WITH (READPAST) WHERE tbs.ETAT = 1 AND tbs.TYPE = ?1)\n"
                + "ORDER BY tb.INTITULE";
        List<TypeBien> listTypeBien = (List<TypeBien>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(TypeBien.class), false, categorie);
        return listTypeBien;
    }

    /*public static List<Unite> getListAllUnites() {
     String sqlQuery = "SELECT * from T_UNITE u WITH (READPAST) WHERE u.ETAT = 1 ORDER BY u.INTITULE";
     List<Unite> listPalier = (List<Unite>) myDao.getDaoImpl().find(sqlQuery,
     Casting.getInstance().convertIntoClassType(Unite.class), false);
     return listPalier;
     }*/
    public static List<FormeJuridique> getListAllAFormeJuridiques() {
        String sqlQuery = "SELECT * FROM T_FORME_JURIDIQUE WITH (READPAST) WHERE ETAT = 1 ORDER BY INTITULE";
        List<FormeJuridique> listFormeJuridique = (List<FormeJuridique>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(FormeJuridique.class), false);
        return listFormeJuridique;
    }

    public static String getCodeTypeComplementBienByTypeBien(String codeTypeBien) {
        String queryStr = "SELECT dbo.F_GET_TYPE_COMPLEMENT_BIEN('" + codeTypeBien.trim() + "')";
        String result = myDao.getDaoImpl().getSingleResultToString(queryStr);
        return result;
    }

    public static List<Banque> getListAllBanque() {
        String sqlQuery = "SELECT * FROM T_BANQUE WITH (READPAST) WHERE ETAT = 1 ORDER BY INTITULE";
        List<Banque> listAvis = (List<Banque>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Banque.class), false);
        return listAvis;
    }

    public static List<CompteBancaire> getListAllCompteBancaireByBanque(String codeBanque) {
        String sqlQuery = "SELECT * FROM T_COMPTE_BANCAIRE WITH (READPAST) WHERE BANQUE = ?1 AND ETAT = 1 ORDER BY INTITULE";
        List<CompteBancaire> listCompteBancaire = (List<CompteBancaire>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(CompteBancaire.class), false, codeBanque);
        return listCompteBancaire;
    }

    public static List<BanqueAb> getListAllBanqueAb() {
        String sqlQuery = "SELECT * FROM T_BANQUE_AB WITH (READPAST) WHERE ETAT = 1";
        List<BanqueAb> listBanqueAb = (List<BanqueAb>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(BanqueAb.class), false);
        return listBanqueAb;
    }

    public static Assujeti getAssujetiByPersonneAndBien(String codePersonne, String codeBien) {

        String sqlQuery = "SELECT * FROM T_ASSUJETI WITH (READPAST) WHERE PERSONNE = ?1 AND BIEN = ?2";

        List<Assujeti> assujetis = (List<Assujeti>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Assujeti.class), false, codePersonne, codeBien);
        return assujetis.isEmpty() ? null : assujetis.get(0);
    }

    public static ComplementBienAutomobile getComplementBienAutomobileByBien(String codeBien) {
        String sqlQuery = "SELECT * FROM T_COMPLEMENT_BIEN_AUTOMOBILE WHERE FK_BIEN = ?1 AND ETAT = 1";
        List<ComplementBienAutomobile> complementBienAutomobiles = (List<ComplementBienAutomobile>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ComplementBienAutomobile.class), false, codeBien);
        return complementBienAutomobiles.isEmpty() ? null : complementBienAutomobiles.get(0);
    }

    public static NotePerceptionVignette getNotePerceptionVignetteByNoteTaxationAndBien(String noteTaxation, String codeBien) {
        String sqlQuery = "SELECT * FROM T_NOTE_PERCEPTION_VIGNETTE WITH (READPAST) WHERE NOTE_TAXATION = ?1 AND FK_BIEN = ?2 AND ETAT = 1";
        List<NotePerceptionVignette> notePerceptionVignettes = (List<NotePerceptionVignette>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NotePerceptionVignette.class), false, noteTaxation, codeBien);
        return notePerceptionVignettes.isEmpty() ? null : notePerceptionVignettes.get(0);
    }

    public static NotePerceptionVignette getNotePerceptionVignetteByNoteTaxation(String noteTaxation) {
        String sqlQuery = "SELECT * FROM T_NOTE_PERCEPTION_VIGNETTE WITH (READPAST) WHERE NOTE_TAXATION = ?1 AND ETAT = 1";
        List<NotePerceptionVignette> notePerceptionVignettes = (List<NotePerceptionVignette>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NotePerceptionVignette.class), false, noteTaxation);
        return notePerceptionVignettes.isEmpty() ? null : notePerceptionVignettes.get(0);
    }

    public static NotePerceptionVignette getNotePerceptionVignetteByID(int id) {
        String sqlQuery = "SELECT * FROM T_NOTE_PERCEPTION_VIGNETTE WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";
        List<NotePerceptionVignette> notePerceptionVignettes = (List<NotePerceptionVignette>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NotePerceptionVignette.class), false, id);
        return notePerceptionVignettes.isEmpty() ? null : notePerceptionVignettes.get(0);
    }

    public static NotePerceptionVignette getNotePerceptionVignetteByFkNoteBank(int id) {
        String sqlQuery = "SELECT * FROM T_NOTE_PERCEPTION_VIGNETTE WITH (READPAST) WHERE FK_NOTE_PERCEPTION_BANQUE = ?1 AND ETAT = 1 ORDER BY DATE_IMPRESSION DESC";
        List<NotePerceptionVignette> notePerceptionVignettes = (List<NotePerceptionVignette>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NotePerceptionVignette.class), false, id);
        return notePerceptionVignettes.isEmpty() ? null : notePerceptionVignettes.get(0);
    }

    public static NotePerceptionBanque getNotePerceptionBanqueByID(int id) {
        String sqlQuery = "SELECT * FROM T_NOTE_PERCEPTION_BANQUE WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";
        List<NotePerceptionBanque> notePerceptionBanques = (List<NotePerceptionBanque>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NotePerceptionBanque.class), false, id);
        return notePerceptionBanques.isEmpty() ? null : notePerceptionBanques.get(0);
    }

    public static NotePerceptionBanque getNotePerceptionBanqueByNP(String np) {
        String sqlQuery = "SELECT * FROM T_NOTE_PERCEPTION_BANQUE WITH (READPAST) WHERE NOTE_PERCEPTION = ?1 AND ETAT = 1";
        List<NotePerceptionBanque> notePerceptionBanques = (List<NotePerceptionBanque>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NotePerceptionBanque.class), false, np);
        return notePerceptionBanques.isEmpty() ? null : notePerceptionBanques.get(0);
    }

    public static NotePerceptionBanque getNotePerceptionBanqueByNPAndEa(String np, String eaCode) {
        String sqlQuery = "SELECT * FROM T_NOTE_PERCEPTION_BANQUE WITH (READPAST) WHERE NOTE_PERCEPTION = ?1 AND FK_EA = ?2 AND ETAT = 1";
        List<NotePerceptionBanque> notePerceptionBanques = (List<NotePerceptionBanque>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NotePerceptionBanque.class), false, np, eaCode);
        return notePerceptionBanques.isEmpty() ? null : notePerceptionBanques.get(0);
    }

    public static TaxationBienAutomobile getTaxationBienAutomobileByBien(String codeBien, String exercice) {
        String sqlQuery = "SELECT * FROM T_TAXATION_BIEN_AUTOMOBILE WHERE FK_BIEN = ?1 AND ETAT = 1 AND EXERCICE = ?2";
        List<TaxationBienAutomobile> taxationBienAutomobiles = (List<TaxationBienAutomobile>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(TaxationBienAutomobile.class), false, codeBien, exercice);
        return taxationBienAutomobiles.isEmpty() ? null : taxationBienAutomobiles.get(0);
    }

    public static TaxationBienAutomobile getTaxationBienAutomobileByBienV2(String codeBien) {
        String sqlQuery = "SELECT * FROM T_TAXATION_BIEN_AUTOMOBILE WHERE FK_BIEN = ?1 AND ETAT = 1";
        List<TaxationBienAutomobile> taxationBienAutomobiles = (List<TaxationBienAutomobile>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(TaxationBienAutomobile.class), false, codeBien);
        return taxationBienAutomobiles.isEmpty() ? null : taxationBienAutomobiles.get(0);
    }

    public static TaxationBienAutomobile getTaxationBienAutomobileByNoteTaxation(String note) {
        String sqlQuery = "SELECT top 1 * FROM T_TAXATION_BIEN_AUTOMOBILE WHERE FK_NOTE_TAXATION = ?1 AND ETAT = 1";
        List<TaxationBienAutomobile> taxationBienAutomobiles = (List<TaxationBienAutomobile>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(TaxationBienAutomobile.class), false, note);
        return taxationBienAutomobiles.isEmpty() ? null : taxationBienAutomobiles.get(0);
    }

    public static MarqueVehicule getMarqueVehiculeByID(int id) {
        String sqlQuery = "SELECT * FROM T_MARQUE_VEHICULE WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";
        List<MarqueVehicule> marqueVehicules = (List<MarqueVehicule>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(MarqueVehicule.class), false, id);
        return marqueVehicules.isEmpty() ? null : marqueVehicules.get(0);
    }

    public static ModeleVehicule getModeleVehiculeByID(int id) {
        String sqlQuery = "SELECT * FROM T_MODELE_VEHICULE WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";
        List<ModeleVehicule> modeleVehicules = (List<ModeleVehicule>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ModeleVehicule.class), false, id);
        return modeleVehicules.isEmpty() ? null : modeleVehicules.get(0);
    }

    public static List<NotePerceptionVignette> getListNotePerceptionVignetteByID(int id) {
        String sqlQuery = "SELECT * FROM T_NOTE_PERCEPTION_VIGNETTE WITH (READPAST) WHERE FK_NOTE_PERCEPTION_BANQUE = ?1";
        List<NotePerceptionVignette> notePerceptionVignettes = (List<NotePerceptionVignette>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NotePerceptionVignette.class), false, id);
        return notePerceptionVignettes;
    }

    public static ExonerationVignette getExonerationVignetteByPersonne(String codePersonne) {
        String sqlQuery = "select * from T_EXONERATION_VIGNETTE WITH (READPAST) WHERE FK_PERSONNE = ?1 AND ETAT = 1";
        List<ExonerationVignette> exonerationVignettes = (List<ExonerationVignette>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ExonerationVignette.class), false, codePersonne);
        return exonerationVignettes.isEmpty() ? null : exonerationVignettes.get(0);
    }

    public static NotePerceptionVignette getNotePerceptionVignetteByNoteAndBien(String noteTaxation, String codeBien) {
        String sqlQuery = "select * from T_NOTE_PERCEPTION_VIGNETTE WITH (READPAST) WHERE NOTE_TAXATION = ?1 AND FK_BIEN = ?2 AND ETAT = 1";
        List<NotePerceptionVignette> notePerceptionVignettes = (List<NotePerceptionVignette>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NotePerceptionVignette.class), false, noteTaxation, codeBien);
        return notePerceptionVignettes.isEmpty() ? null : notePerceptionVignettes.get(0);
    }

    public static String getJsonBienTaxarionByNote(int note) {
        String sqlQuery = "SELECT dbo.F_GET_DETAIL_BIEN_TAXATION(%s)";
        sqlQuery = String.format(sqlQuery, note);
        String code = (String) myDao.getDaoImpl().getSingleResult(sqlQuery);
        return code;
    }

    public static String getJsonCotationBienAutomobile(String codePersonne, String codeAB, String codeFormeJ) {
        String sqlQuery = "SELECT dbo.F_GET_COTATION_BIEN_AUTOMOBILE('%s','%s','%s')";
        sqlQuery = String.format(sqlQuery, codePersonne, codeAB, codeFormeJ);
        String code = (String) myDao.getDaoImpl().getSingleResult(sqlQuery);
        return code;
    }
}
