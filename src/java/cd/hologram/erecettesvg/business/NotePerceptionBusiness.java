/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.AssujettissementBusiness.executeQueryBulkInsert;
import static cd.hologram.erecettesvg.business.IdentificationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.Archive;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.BanqueAb;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.Commande;
import cd.hologram.erecettesvg.models.Complement;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.DocumentOfficiel;
import cd.hologram.erecettesvg.models.LogNoteTaxation;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.NotePerceptionBanque;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.RetraitDeclaration;
import cd.hologram.erecettesvg.models.Serie;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.sql.SQLQueryGeneral;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.sql.SQLQueryTaxation;
import cd.hologram.erecettesvg.util.Casting;
import cd.hologram.erecettesvg.util.ConvertDate;
import entites.RemiseSousProvisionPeage;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author : WILLY KASHALA Tel : 00243 81 27 20 560
 */
public class NotePerceptionBusiness {

    private static final Dao myDao = new Dao();

    public static List<NotePerception> getListNotePreception(String monCle) {

        try {
            String query = SQLQueryNotePerception.SELECT_NOTE_PERCEPTION;
            List<NotePerception> listNP = (List<NotePerception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), true, monCle);
            return listNP;
        } catch (Exception e) {
            throw e;
        }

    }

    public static Personne getAssujetti(String code) {
        try {
            String query = SQLQueryNotePerception.GET_ASSUJETTI_BY_CODE;
            List<Personne> personne = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), true, code);
            return personne.size() > 0 ? personne.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static NotePerception getNotePerceptionByCode(String numero) {
        try {
            String query = SQLQueryNotePerception.SELECT_NOTE_PERCEPTION_BY_CODE;
            List<NotePerception> noteperceptions = (List<NotePerception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false, numero);
            return noteperceptions.isEmpty() ? null : noteperceptions.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static NotePerception getNotePerceptionByNpManuel(String npManuel) {
        try {
            String query = "SELECT * FROM T_NOTE_PERCEPTION WITH (READPAST) WHERE ETAT > 0 AND NOTE_PERCEPTION_MANUEL = ?1";
            List<NotePerception> noteperceptions = (List<NotePerception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false, npManuel);
            return noteperceptions.isEmpty() ? null : noteperceptions.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Commande getCommandeByCode(String numero) {
        try {
            String query = "SELECT C.* FROM T_COMMANDE C WITH (READPAST) WHERE (CAST(C.ID AS VARCHAR(20)) = ?1 OR C.REFERENCE = ?1) AND C.ETAT = 2";
            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, numero);
            return commandes.isEmpty() ? null : commandes.get(0);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public static Commande getCommandeByPersonne(String codePersonne) {
        try {
            String query = "SELECT C.* FROM T_COMMANDE C WITH (READPAST) WHERE C.FK_PERSONNE = ?1 AND C.ETAT > 0";
            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, codePersonne);
            return commandes.isEmpty() ? null : commandes.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Commande> getListCommandesPaid(String codeSite, String compteBancaire, String dateDebut, String dateFin) {
        try {

            String sqlQueryAccountBank = "";

            if (!compteBancaire.equals("*")) {
                sqlQueryAccountBank = " AND C.FK_COMPTE_BANCAIRE = '%s'";
                sqlQueryAccountBank = String.format(sqlQueryAccountBank, compteBancaire);
            }

            /*if (dateDebut != null) {
             if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
             dateDebut = ConvertDate.getValidFormatDate(dateDebut);
             }
             }

             if (dateFin != null) {
             if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
             dateFin = ConvertDate.getValidFormatDate(dateFin);
             }
             }*/
            String sqlQueryDates = " AND DBO.FDATE(C.DATE_PAIEMENT) BETWEEN '%s' AND '%s'";
            sqlQueryDates = String.format(sqlQueryDates, dateDebut, dateFin);

            String query = "SELECT C.* FROM T_COMMANDE C WITH (READPAST) WHERE C.SITE_PAIEMENT = ?1 %s %s";
            query = String.format(query, sqlQueryAccountBank, sqlQueryDates);

            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, codeSite);
            return commandes;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Amr getAmrByCode(String numero) {
        try {
            String query = SQLQueryNotePerception.SELECT_AMR_BY_CODE;
            List<Amr> amrs = (List<Amr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Amr.class), false, numero);
            return amrs.isEmpty() ? null : amrs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static RetraitDeclaration getRetraitDeclarationByCodeDeclaration(String code) {
        try {
            String query = SQLQueryNotePerception.SELECT_RETRAIT_DECLARATION_BY_DECLARATION;
            List<RetraitDeclaration> retraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, code);
            return retraitDeclarations.isEmpty() ? null : retraitDeclarations.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static RetraitDeclaration getRetraitDeclarationByDeclaration(String code) {
        try {
            String query = SQLQueryNotePerception.SELECT_RETRAIT_DECLARATION_BY_DECLARATION;
            List<RetraitDeclaration> retraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, code);
            return retraitDeclarations.isEmpty() ? null : retraitDeclarations.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static RetraitDeclaration getRetraitDeclarationPrincipalByNewid(String newid) {
        try {
            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ETAT = 1 AND NEW_ID = ?1";
            List<RetraitDeclaration> retraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, newid);
            return retraitDeclarations.isEmpty() ? null : retraitDeclarations.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static RetraitDeclaration getRetraitDeclarationPenaliteByNewid(String newid) {
        try {
            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ETAT = 4 AND NEW_ID = ?1";
            List<RetraitDeclaration> retraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, newid);
            return retraitDeclarations.isEmpty() ? null : retraitDeclarations.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationByCodeDeclaration(String code) {

        try {
            String query = SQLQueryNotePerception.SELECT_RETRAIT_DECLARATION_BY_DECLARATION;
            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, code);
            return declarations;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationByCodeDeclarationV2(String code) {

        try {
            String query = SQLQueryNotePerception.SELECT_RETRAIT_DECLARATION_BY_DECLARATION_V2;
            query = String.format(query, code);

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, code);
            return declarations;
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean executeQueryBulkInsert(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }

    public static List<NotePerception> getListNotePerceptions(
            String valueSearch, int typeSearch,
            String typeRegister, boolean allSite,
            boolean allService,
            String codeSite,
            String codeService,
            String userId) {

        String secondaryQuery_One = "";
        String secondaryQuery_Two;
        String secondaryQuery_Three;
        String primaryQuery = "";

        boolean like = false;

        if (typeSearch == 1) {
            secondaryQuery_One = " AND NP.NOTE_CALCUL IN (SELECT NC.NUMERO FROM T_NOTE_CALCUL NC "
                    + " WHERE NC.PERSONNE  IN (SELECT P.CODE FROM T_PERSONNE P "
                    + " WHERE (P.NOM LIKE ?1 OR P.POSTNOM LIKE ?1 OR P.PRENOMS LIKE ?1)))";
            like = true;
        } else if (typeSearch == 2) {
            secondaryQuery_One = " AND (NP.NUMERO = ?1 OR NP.NOTE_PERCEPTION_MANUEL = ?1)";
            like = false;
        }

        if (!allSite) {
            /*secondaryQuery_Two = " AND NP.SITE = '" + codeSite.trim() + "'"
             + " AND NP.SITE IN (SELECT DISTINCT AST.CODESITE FROM T_AGENT_SITE AST WITH (READPAST) "
             + " WHERE AST.CODEAGENT = '" + userId + "' AND AST.ETAT = 1)";*/

            secondaryQuery_Two = " AND NP.SITE = '" + codeSite.trim() + "'";
        } else {
            secondaryQuery_Two = "";
        }

        if (!allService) {
            secondaryQuery_Three = " AND NP.NOTE_CALCUL IN (SELECT DISTINCT NC.NUMERO FROM T_NOTE_CALCUL NC WITH (READPAST)"
                    + " WHERE NC.SERVICE = '" + codeService.trim() + "')";
        } else {
            secondaryQuery_Three = "";
        }

        switch (typeRegister.trim()) {
            case "NP":
                primaryQuery = "SELECT NP.* FROM T_NOTE_PERCEPTION NP WITH (READPAST) "
                        + " WHERE NP.ETAT > 0 %s %s AND NP.NP_MERE IS NULL ORDER BY NP.NUMERO DESC";
                break;
            case "NPR":
                primaryQuery = "";
                break;
        }

        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two, secondaryQuery_Three);
        try {

            List<NotePerception> listNotePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(primaryQuery,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), like, valueSearch.trim());
            return listNotePerceptions;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<NotePerception> getListNotePerceptionsBySearchAvanced(
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin) {

        String secondaryQuery_One;
        String secondaryQuery_Two;
        String primaryQuery;

        switch (codeSite.trim()) {
            case "*":
                secondaryQuery_One = " AND DBO.FDATE(NP.DATE_CREAT) BETWEEN '" + dateDebut.trim() + "' AND '" + dateFin.trim() + "'";
                break;
            default:
                secondaryQuery_One = " AND NP.SITE = '" + codeSite.trim() + "' AND "
                        + " DBO.FDATE(NP.DATE_CREAT) BETWEEN '" + dateDebut.trim() + "' AND '" + dateFin.trim() + "'";
                break;
        }

        if (!codeService.equals("*")) {
            secondaryQuery_Two = " AND NP.NOTE_CALCUL IN (SELECT DISTINCT NC.NUMERO FROM T_NOTE_CALCUL NC WITH (READPAST)"
                    + " WHERE NC.SERVICE = '" + codeService.trim() + "')";
        } else {
            secondaryQuery_Two = "";
        }

        primaryQuery = "SELECT NP.* FROM T_NOTE_PERCEPTION NP WITH (READPAST) WHERE  "
                + " NP.ETAT > 0 %s %s AND NP.NP_MERE IS NULL ORDER BY NP.NUMERO DESC";

        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two);

        try {
            List<NotePerception> listNotePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(primaryQuery,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false, "");
            return listNotePerceptions;
        } catch (Exception e) {
            throw e;
        }
    }

    public static CompteBancaire getAccountBankByAccount(String code) {
        try {
            String query = SQLQueryNotePerception.SELECT_ACCOUNT_BANK_BY_CODE;
            List<CompteBancaire> compteBancaires = (List<CompteBancaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CompteBancaire.class), false, code.trim());
            return compteBancaires.size() > 0 ? compteBancaires.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Complement getComplementByParams(
            String codeFormeJuridique,
            String codeTypeComplement,
            String codePersonne) {

        try {
            String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_COMPLEMENT_PERSONNE_BY_PARAMS;
            List<Complement> lisComplement = (List<Complement>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Complement.class), false, codeFormeJuridique.trim(),
                    codeTypeComplement.trim(), codePersonne.trim());
            return lisComplement.isEmpty() ? null : lisComplement.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static String getLetterOfNumber(int number) {
        try {
            String queryStr = "SELECT dbo.NombreEnLettres(" + number + ")";
            String letter = (String) myDao.getDaoImpl().getSingleResult(queryStr);
            return letter;
        } catch (Exception e) {
            throw e;
        }
    }

    public static ArticleBudgetaire getArticleBudgetaireByCode(String numeroNC) {
        try {
            String sqlQuery = SQLQueryNotePerception.SELECT_LIST_BUDGET_ARTICLES_BY_NC;
            List<ArticleBudgetaire> listArticleBudgetaire = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, numeroNC.trim());
            return listArticleBudgetaire.isEmpty() ? null : listArticleBudgetaire.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static DocumentOfficiel getDocumentOfficielByCode(String code) {
        try {
            String sqlQuery = SQLQueryNotePerception.SELECT_DOCUMENT_OFFICIEL_BY_CODE;
            List<DocumentOfficiel> listDocumentOfficiel = (List<DocumentOfficiel>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(DocumentOfficiel.class), false, code.trim());
            return listDocumentOfficiel.isEmpty() ? null : listDocumentOfficiel.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<DetailsNc> getListDetailsNcsNoteCalcul(String numeroNC) {

        try {
            String sqlQuery = SQLQueryNotePerception.SELECT_DETAILS_NC_BY_NC;
            List<DetailsNc> listDetailsNcs = (List<DetailsNc>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(DetailsNc.class), false, numeroNC.trim());
            return listDetailsNcs;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Tarif getTarifByCode(String code) {
        try {
            String sqlQuery = SQLQueryNotePerception.SELECT_TARIF_BY_CODE;
            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false, code);
            return tarifs.isEmpty() ? null : tarifs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Archive getArchiveByRefDocument(String reference) {
        try {
            String sqlQuery = SQLQueryNotePerception.SELECT_ARCHIVE_BY_REF_DOCUMENT;
            List<Archive> archives = (List<Archive>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Archive.class), false, reference.trim());
            return archives.isEmpty() ? null : archives.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Serie getLastSerieBySite(String site) {
        try {
            String sqlQuery = SQLQueryNotePerception.SELECT_LAST_SERIE_BY_SITE;
            List<Serie> archives = (List<Serie>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Serie.class), false, site.trim());
            return archives.isEmpty() ? null : archives.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<BanqueAb> getListArticleBudgetaireByBankCode(String banqueCode, String articleCode) {

        try {
            String query = SQLQueryGeneral.SELECT_ARTICLE_BUDGETAIRE_BY_BANK;
            List<BanqueAb> banqueAbs = (List<BanqueAb>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(BanqueAb.class), false, banqueCode, articleCode);
            return banqueAbs;
        } catch (Exception e) {
            throw e;
        }
    }

    public static NotePerception getNotePerceptionByAssujetti(String numeroNP, String codeAssujetti) {

        try {
            String query = SQLQueryNotePerception.SELECT_NOTE_PERCEPTION_FOR_FRACTION;
            List<NotePerception> noteperceptions = (List<NotePerception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false,
                    numeroNP.trim(), codeAssujetti.trim());
            return noteperceptions.isEmpty() ? null : noteperceptions.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static RetraitDeclaration getRetraitDeclarationByCode(int code, String codeAssujetti) {

        try {
            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ID = ?1 AND FK_ASSUJETTI = ?2";
            List<RetraitDeclaration> retraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, code, codeAssujetti);
            return retraitDeclarations.isEmpty() ? null : retraitDeclarations.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean checkIfTitreIsEnroler(String titre) {

        try {
            String query = "SELECT COUNT(*) FROM T_DETAILS_ROLE WITH (READPAST) WHERE DOCUMENT_REF = '" + titre + "'";
            int existingCount = (int) myDao.getDaoImpl().getSingleResult(query);
            if (existingCount > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            throw e;
        }
    }

    public static BonAPayer getBonAPayerByAssujetti(String codeBP, String codeAssujetti) {

        try {

            String query = "SELECT BP.* FROM T_BON_A_PAYER BP WITH (READPAST) WHERE BP.CODE = ?1 AND BP.FK_PERSONNE = ?2 AND BP.ETAT = 1";

            List<BonAPayer> bonAPayer = (List<BonAPayer>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(BonAPayer.class), false,
                    codeBP.trim(), codeAssujetti.trim());
            return bonAPayer.isEmpty() ? null : bonAPayer.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Amr getAmrByAssujetti(String numeroAmr, String codeAssujetti) {

        try {

            String query = "SELECT A.* FROM T_AMR A WITH (READPAST) INNER JOIN T_FICHE_PRISE_CHARGE F ON A.FICHE_PRISE_CHARGE = F.CODE"
                    + " WHERE A.NUMERO = ?1 AND F.PERSONNE = ?2 AND A.ETAT = 2";

            List<Amr> amrs = (List<Amr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Amr.class), false,
                    numeroAmr.trim(), codeAssujetti.trim());
            return amrs.isEmpty() ? null : amrs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<BonAPayer> getBpInteretByNP(String np) {

        try {
            String query = SQLQueryNotePerception.SELECT_BP_INTERET_BY_NP;
            List<BonAPayer> bonAPayers = (List<BonAPayer>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(BonAPayer.class), false, np);
            return bonAPayers;
        } catch (Exception e) {
            throw e;
        }

    }

    public static List<NotePerception> getNpInteretByBP(String bp) {

        try {
            String query = SQLQueryNotePerception.SELECT_BP_INTERET_BY_BP;
            List<NotePerception> notePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false, bp);
            return notePerceptions;
        } catch (Exception e) {
            throw e;
        }

    }

    public static NotePerception getNotePerceptionFractionne(String numero) {
        try {
            String query = SQLQueryNotePerception.SELECT_NOTE_PERCEPTION_FRACTIONNE;
            List<NotePerception> noteperceptions = (List<NotePerception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false, numero);
            return noteperceptions.isEmpty() ? null : noteperceptions.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<NotePerception> getListNpFilleByMere(String npMere) {

        try {
            String sqlQuery = SQLQueryNotePerception.SELECT_LIST_NP_FILLE_BY_Mere;
            List<NotePerception> listNotePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false, npMere.trim());
            return listNotePerceptions;
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean updateRetraiteDeclaration(String avis, int codeDeclaration, String observation) {

        boolean result = false;
        try {
            String query = SQLQueryNotePerception.UPDATE_RETRAIT_DECLARATION;

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    avis.trim(),
                    observation.trim(),
                    codeDeclaration);

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<NotePerception> getListNotePerceptionsByExtraitRole(String extraitRoleId) {

        try {
            String sqlQuery = SQLQueryNotePerception.SELECT_LIST_NOTE_PERCEPTION_BY_EXTRAIT_ROLE;
            List<NotePerception> listNotePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false, extraitRoleId.trim());
            return listNotePerceptions;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Boolean deleteNotePerception(int numeroNote, Integer agentID, int saveLogActivate, LogNoteTaxation logNoteTaxation) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":EXEC F_DELETE_NOTE_PERCEPTION ?1", new Object[]{
                numeroNote
            });

            if (saveLogActivate == 1) {

                counter++;

                String sqlQuery = ":INSERT INTO T_LOG_NOTE_TAXATION (CODE_AGENT,NOM_AGENT,NOTE_TAXATION,OBSERVATION,ACTION) VALUES (?1,?2,?3,?4,?5)";

                bulkQuery.put(counter + sqlQuery, new Object[]{
                    logNoteTaxation.getCodeAgent(),
                    logNoteTaxation.getNomAgent(),
                    logNoteTaxation.getNoteTaxation(),
                    logNoteTaxation.getObservation(),
                    logNoteTaxation.getAction()
                });
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean updateAccountBankNP(String numeroNote, Integer agentID, String codeBanque,
            String codeCompteBancaire, int saveLogActivate, LogNoteTaxation logNoteTaxation) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String sqlQuery = "";

            sqlQuery = ":UPDATE T_NOTE_PERCEPTION SET COMPTE_BANCAIRE = ?1 WHERE NUMERO = ?2";

            bulkQuery.put(counter + sqlQuery, new Object[]{
                codeCompteBancaire,
                numeroNote
            });

            if (saveLogActivate == 1) {

                counter++;

                sqlQuery = ":INSERT INTO T_LOG_NOTE_TAXATION (CODE_AGENT,NOM_AGENT,NOTE_TAXATION,OBSERVATION,ACTION) VALUES (?1,?2,?3,?4,?5)";

                bulkQuery.put(counter + sqlQuery, new Object[]{
                    logNoteTaxation.getCodeAgent(),
                    logNoteTaxation.getNomAgent(),
                    logNoteTaxation.getNoteTaxation(),
                    logNoteTaxation.getObservation(),
                    logNoteTaxation.getAction()
                });
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean updateModePaiementNotePerception(int saveLogActivate, LogNoteTaxation logNoteTaxation) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            if (saveLogActivate == 1) {

                counter++;

                String sqlQuery = ":INSERT INTO T_LOG_NOTE_TAXATION (CODE_AGENT,NOM_AGENT,NOTE_TAXATION,OBSERVATION,ACTION) VALUES (?1,?2,?3,?4,?5)";

                bulkQuery.put(counter + sqlQuery, new Object[]{
                    logNoteTaxation.getCodeAgent(),
                    logNoteTaxation.getNomAgent(),
                    logNoteTaxation.getNoteTaxation(),
                    logNoteTaxation.getObservation(),
                    logNoteTaxation.getAction()
                });

            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean updateDeviseNoteTaxation(String numeroNote, String deviseCode, int agentID, BigDecimal newAmount,
            int saveLogActivate, LogNoteTaxation logNoteTaxation) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":UPDATE T_NOTE_PERCEPTION SET DEVISE = ?1,NET_A_PAYER = ?2, SOLDE = ?2 WHERE NUMERO = ?3", new Object[]{
                deviseCode,
                newAmount,
                numeroNote
            });

            if (saveLogActivate == 1) {

                counter++;

                String sqlQuery = ":INSERT INTO T_LOG_NOTE_TAXATION (CODE_AGENT,NOM_AGENT,NOTE_TAXATION,OBSERVATION,ACTION) VALUES (?1,?2,?3,?4,?5)";

                bulkQuery.put(counter + sqlQuery, new Object[]{
                    logNoteTaxation.getCodeAgent(),
                    logNoteTaxation.getNomAgent(),
                    logNoteTaxation.getNoteTaxation(),
                    logNoteTaxation.getObservation(),
                    logNoteTaxation.getAction()
                });
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean updated456(String sqlQueryMaster, int saveLogActivate, LogNoteTaxation logNoteTaxation) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + sqlQueryMaster, new Object[]{});

            if (saveLogActivate == 1) {

                counter++;

                String sqlQuery = ":INSERT INTO T_LOG_NOTE_TAXATION (CODE_AGENT,NOM_AGENT,NOTE_TAXATION,OBSERVATION,ACTION) VALUES (?1,?2,?3,?4,?5)";

                bulkQuery.put(counter + sqlQuery, new Object[]{
                    logNoteTaxation.getCodeAgent(),
                    logNoteTaxation.getNomAgent(),
                    logNoteTaxation.getNoteTaxation(),
                    logNoteTaxation.getObservation(),
                    logNoteTaxation.getAction()
                });

            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean updated89(String sqlQueryMaster, int saveLogActivate, LogNoteTaxation logNoteTaxation, boolean isTaxCfc) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + sqlQueryMaster, new Object[]{});

            if (saveLogActivate == 1) {

                counter++;

                String sqlQuery = ":INSERT INTO T_LOG_NOTE_TAXATION (CODE_AGENT,NOM_AGENT,NOTE_TAXATION,OBSERVATION,ACTION) VALUES (?1,?2,?3,?4,?5)";

                bulkQuery.put(counter + sqlQuery, new Object[]{
                    logNoteTaxation.getCodeAgent(),
                    logNoteTaxation.getNomAgent(),
                    logNoteTaxation.getNoteTaxation(),
                    logNoteTaxation.getObservation(),
                    logNoteTaxation.getAction()
                });

            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<NotePerceptionBanque> getListAllNotePerceptionBanque() {

        try {
            String query = "SELECT NPB.* FROM T_NOTE_PERCEPTION_BANQUE NPB WITH (READPAST) \n"
                    + "  WHERE NPB.ETAT > 0 AND NOT NPB.NOTE_TAXATION IS NULL ORDER BY NPB.DATE_MAJ DESC ";
            List<NotePerceptionBanque> listNP = (List<NotePerceptionBanque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerceptionBanque.class), false);
            return listNP;
        } catch (Exception e) {
            throw e;
        }

    }

    public static List<NotePerceptionBanque> getListAllNotePerceptionBanqueNotUse() {

        try {
            String query = "SELECT NPB.* FROM T_NOTE_PERCEPTION_BANQUE NPB WITH (READPAST) \n"
                    + " JOIN T_BANQUE B WITH (READPAST) ON NPB.FK_BANQUE = B.CODE WHERE NPB.ETAT > 0 AND NPB.NOTE_TAXATION IS NULL"
                    + " ORDER BY B.INTITULE,NPB.NOTE_PERCEPTION ";
            List<NotePerceptionBanque> listNP = (List<NotePerceptionBanque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerceptionBanque.class), false);
            return listNP;
        } catch (Exception e) {
            throw e;
        }

    }

    public static List<NotePerceptionBanque> getListFirstNotePerceptionBanque() {

        try {
            String query = "SELECT TOP 5 NPB.* FROM T_NOTE_PERCEPTION_BANQUE NPB WITH (READPAST) \n"
                    + "JOIN T_BANQUE B WITH (READPAST) ON NPB.FK_BANQUE = B.CODE WHERE NPB.ETAT > 0 ORDER BY B.INTITULE,NPB.NOTE_PERCEPTION ";
            List<NotePerceptionBanque> listNP = (List<NotePerceptionBanque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerceptionBanque.class), false);
            return listNP;
        } catch (Exception e) {
            throw e;
        }

    }

    public static NotePerceptionBanque getNotePerceptionBanqueByNp(String numero) {
        try {
            String query = "SELECT * FROM T_NOTE_PERCEPTION_BANQUE WITH (READPAST) WHERE NOTE_PERCEPTION = ?1 AND ETAT > 0";
            List<NotePerceptionBanque> notePerceptionBanques = (List<NotePerceptionBanque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerceptionBanque.class), false, numero);
            return notePerceptionBanques.isEmpty() ? null : notePerceptionBanques.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean saveNotePerceptionBanqueForEdit(NotePerceptionBanque notePerceptionBanque) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = ":INSERT INTO T_NOTE_PERCEPTION_BANQUE (NOTE_PERCEPTION,FK_BANQUE,PLAGE_DEBUT,PLAGE_FIN,AGENT_CREATE,FK_EA) VALUES (?1,?2,?3,?4,?5,?6)";

            counter++;

            bulkQuery.put(counter + query, new Object[]{
                notePerceptionBanque.getNotePerception(),
                notePerceptionBanque.getFkBanque(),
                notePerceptionBanque.getPlageDebut(),
                notePerceptionBanque.getPlageFin(),
                notePerceptionBanque.getAgentCreate(),
                notePerceptionBanque.getFkEa()
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static boolean saveNotePerceptionBanqueBatch(List<NotePerceptionBanque> listNotePerceptionBanque) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            for (NotePerceptionBanque npb : listNotePerceptionBanque) {

                String query = ":EXEC F_SAVE_NOTE_PERCEPTION_BANK_BATCH ?1,?2,?3,?4,?5,?6";

                bulkQuery.put(counter + query, new Object[]{
                    npb.getNotePerception(),
                    npb.getPlageDebut(),
                    npb.getPlageFin(),
                    npb.getFkBanque(),
                    npb.getFkEa(),
                    npb.getAgentCreate()

                });

                counter++;

            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static boolean modifyNotePerception(NotePerceptionBanque notePerceptionBanque) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = ":UPDATE T_NOTE_PERCEPTION_BANQUE SET NOTE_PERCEPTION = ?1,AGENT_MAJ = ?2, DATE_MAJ = GETDATE() WHERE ID = ?3";

            counter++;
            bulkQuery.put(counter + query, new Object[]{
                notePerceptionBanque.getNotePerception(),
                notePerceptionBanque.getAgentMaj(),
                notePerceptionBanque.getId()
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static boolean modifyBankNotePerception(NotePerceptionBanque notePerceptionBanque) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = ":UPDATE T_NOTE_PERCEPTION_BANQUE SET FK_BANQUE = ?1,AGENT_MAJ = ?2, DATE_MAJ = GETDATE() WHERE ID = ?3";

            counter++;
            bulkQuery.put(counter + query, new Object[]{
                notePerceptionBanque.getFkBanque(),
                notePerceptionBanque.getAgentMaj(),
                notePerceptionBanque.getId()
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static boolean modifyPlageNotePerception(NotePerceptionBanque notePerceptionBanque) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = ":UPDATE T_NOTE_PERCEPTION_BANQUE SET PLAGE_DEBUT = ?1,PLAGE_FIN = ?2,AGENT_MAJ = ?3, DATE_MAJ = GETDATE() WHERE ID = ?4";

            counter++;
            bulkQuery.put(counter + query, new Object[]{
                notePerceptionBanque.getPlageDebut(),
                notePerceptionBanque.getPlageFin(),
                notePerceptionBanque.getAgentMaj(),
                notePerceptionBanque.getId()
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static boolean modifyVilleNotePerception(int id, int userId, String newVille) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = ":UPDATE T_NOTE_PERCEPTION_BANQUE SET FK_EA = ?1,AGENT_MAJ = ?2, DATE_MAJ = GETDATE() WHERE ID = ?3";

            counter++;
            bulkQuery.put(counter + query, new Object[]{
                newVille,
                userId,
                id
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static boolean deleteNotePerceptionBank(int id, int userId) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = ":UPDATE T_NOTE_PERCEPTION_BANQUE SET ETAT = 0, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE ID = ?2";

            counter++;
            bulkQuery.put(counter + query, new Object[]{userId, id});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }
}
