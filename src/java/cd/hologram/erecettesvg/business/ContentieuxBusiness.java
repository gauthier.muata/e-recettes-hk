/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.TaxationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.ContentieuxConst;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.PropertiesConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.ArchiveAccuseReception;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.Decision;
import cd.hologram.erecettesvg.models.DetailFichePriseCharge;
import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.DetailsReclamation;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.Reclamation;
import cd.hologram.erecettesvg.models.RecoursJuridictionnel;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.SuiviComptableDeclaration;
import cd.hologram.erecettesvg.pojo.DetailsReclamationPrint;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.properties.PropertiesConfig;
import cd.hologram.erecettesvg.properties.PropertiesMessage;
import cd.hologram.erecettesvg.sql.SQLQueryContentieux;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.sql.SQLQueryPaiement;
import cd.hologram.erecettesvg.sql.SQLQueryTaxation;
import cd.hologram.erecettesvg.util.Casting;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author WILLY KASHALA
 */
public class ContentieuxBusiness {

    private static final Dao myDao = new Dao();

    static PropertiesMessage propertiesMessage;
    static PropertiesConfig propertiesConfig;

    public static Amr getAmrByNumero(String numero, String codePersonne) {
        String query = SQLQueryContentieux.SELECT_AMR_BY_NUMERO;
        List<Amr> listAmr = (List<Amr>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Amr.class), false, numero.trim(), codePersonne.trim());
        return listAmr.isEmpty() ? null : listAmr.get(0);
    }

    public static Reclamation getReclamationByTitre(String titreCode) {
        try {
            String query = SQLQueryContentieux.SELECT_RECLAMATION_BY_REFERENCE_DOCUMENT;
            List<Reclamation> reclamations = (List<Reclamation>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Reclamation.class), false, titreCode);
            return reclamations.isEmpty() ? null : reclamations.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static BonAPayer getBonAPayerByAMR(String titreCode) {
        try {
            String query = SQLQueryContentieux.SELECT_BON_A_PAYER_BY_AMR;
            List<BonAPayer> bonP = (List<BonAPayer>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(BonAPayer.class), false, titreCode);
            return bonP.isEmpty() ? null : bonP.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static DetailFichePriseCharge getArticleBudgetaireByFPC(String codeFPC) {
        try {
            String query = SQLQueryContentieux.SELECT_ARTICLE_BUDGETAIRE_BY_FPC;
            List<DetailFichePriseCharge> detailFPC = (List<DetailFichePriseCharge>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailFichePriseCharge.class), false, codeFPC);
            return detailFPC.isEmpty() ? null : detailFPC.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Amr loaderAmrByNumero(String numero) {
        String query = SQLQueryContentieux.LOADER_AMR_BY_NUMERO;
        List<Amr> listAmr = (List<Amr>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Amr.class), false, numero.trim());
        return listAmr.isEmpty() ? null : listAmr.get(0);
    }
    public static Amr loaderAmrByNumero_V2(String numero) {
        String query = SQLQueryContentieux.LOADER_AMR_BY_NUMERO_V2;
        List<Amr> listAmr = (List<Amr>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Amr.class), false, numero.trim());
        return listAmr.isEmpty() ? null : listAmr.get(0);
    }

    public static SuiviComptableDeclaration getSuiviComptableDeclarationByNc(String numero) {
        try {
            String query = SQLQueryContentieux.SELECT_SUIVI_COMPTABLE_BY_NC;
            List<SuiviComptableDeclaration> scds = (List<SuiviComptableDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(SuiviComptableDeclaration.class), false, numero);
            return scds.isEmpty() ? null : scds.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean saveReclamation(
            Reclamation reclamation,
            List<DetailsReclamationPrint> listsReclamation, List<String> documentList,
            String fk_Document, String observationArch, String natureAB) throws IOException {

        int counter = GeneralConst.Numeric.ZERO;

        Boolean result = false;

        HashMap<String, Object> reclamationParams = new HashMap<>();
        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        propertiesMessage = new PropertiesMessage();

        String returnValue = ContentieuxConst.ParamQuery.RECLAMMATION_ID;
        String quetyreclamation = SQLQueryContentieux.SAVE_RECLAMATION;

        try {
            reclamationParams.put(ContentieuxConst.ParamQuery.AGENT_CREAT,
                    reclamation.getAgentCreat().getCode());
            reclamationParams.put(ContentieuxConst.ParamQuery.OBSERVATION,
                    reclamation.getObservation().trim());
            reclamationParams.put(ContentieuxConst.ParamQuery.TYPE_RECL,
                    reclamation.getTypeReclamation());
            reclamationParams.put(ContentieuxConst.ParamQuery.FK_PESONNE,
                    reclamation.getFkPersonne().getCode().trim());
            reclamationParams.put(ContentieuxConst.ParamQuery.REFERENCE_COURIER_RECLAMATION,
                    reclamation.getReferenceCourierReclamation().trim());
            reclamationParams.put(ContentieuxConst.ParamQuery.ETAT_RECLAMATION,
                    reclamation.getEtatReclamation());

            for (DetailsReclamationPrint derecl : listsReclamation) {

                counter++;

                bulkQuery.put(counter + SQLQueryContentieux.F_NEW_DETAILS_RECLAMATION, new Object[]{
                    derecl.getReferenceDocument().trim(),
                    derecl.getTypeDocument().trim(),
                    derecl.getMotif().trim(),
                    derecl.getMontantContester(),
                    derecl.getPourcentageMontantNonContster(),
                    reclamation.getAgentCreat().getCode()});

                counter++;
                bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
                    derecl.getNcDocumentReference().trim(),
                    derecl.getReferenceDocument().trim(),
                    propertiesMessage.getContent(PropertiesConst.Message.TXT_CONTENTIEUX_RECLAMATION),
                    GeneralConst.Numeric.ZERO,
                    GeneralConst.Numeric.ZERO,
                    derecl.getMotif().trim(),
                    derecl.getDevise().trim()});
            }

            if (documentList.size() > 0) {
                for (String docu : documentList) {
                    counter++;
                    bulkQuery.put(counter + SQLQueryNotePerception.ARCHIVE_ACCUSER_RECEPTION,
                            new Object[]{
                                DocumentConst.DocumentCode.LETTRE_RECLAMATION,
                                docu, fk_Document, observationArch.trim()
                            });
                }
            }

            counter++;
            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.EXEC_NEW_DOSSIER_WEB, new Object[]{
                reclamation.getObservation().trim(),
                reclamation.getFkPersonne().getCode().trim(),
                reclamation.getAgentCreat().getCode(),
                fk_Document,
                natureAB
            });

            counter++;
            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.F_UPDATE_AMR, new Object[]{
                GeneralConst.Numeric.TWO,
                fk_Document
            });

            result = myDao.getDaoImpl().execBulkQuery(quetyreclamation, returnValue, reclamationParams, bulkQuery);

        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    public static List<Reclamation> loadListReclamations(
            String isAdvancedSearch,
            String typeSearch,
            String valueSearch,
            String dateDebut,
            String dateFin,
            String typeReclamation
    ) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlQueryByAssujetti = GeneralConst.EMPTY_STRING;
        String sqlQueryByReferenceCourrier = GeneralConst.EMPTY_STRING;
        String sqlQueryByDates = GeneralConst.EMPTY_STRING;
        String sqlQueryByReferenceDocument = GeneralConst.EMPTY_STRING;
        String sqlQueryByTypeReclamation = GeneralConst.EMPTY_STRING;

        boolean isLike = false;

        if (isAdvancedSearch.trim().equals(GeneralConst.Number.ZERO)) {
            switch (typeSearch.trim()) {
                case GeneralConst.Number.ONE:
                    isLike = true;
                    sqlQueryByAssujetti = SQLQueryContentieux.SQL_QUERY_SECONDARY_RECLAMATION_BY_CODE_ASSUJETTI;
                    sqlQueryByAssujetti = String.format(sqlQueryByAssujetti, valueSearch.trim());
                    break;
                case GeneralConst.Number.TWO:
                    sqlQueryByReferenceCourrier = SQLQueryContentieux.SQL_QUERY_SECONDARY_RECLAMATION_BY_REFERENCE_COURRIER;
                    break;
                case GeneralConst.Number.THREE:
                    sqlQueryByReferenceDocument = SQLQueryContentieux.SQL_QUERY_SECONDARY_RECLAMATION_BY_REFERENCE_DOCUMENT;
                    break;
            }
        } else if (isAdvancedSearch.trim().equals(GeneralConst.Number.ONE)) {

            if (dateDebut != null) {
                if (dateDebut.contains("-")) {
                    dateDebut = ConvertDate.getValidFormatDate(dateDebut);
                }
            } else {
                dateDebut = GeneralConst.EMPTY_STRING;
            }

            if (dateFin != null) {
                if (dateFin.contains("-")) {
                    dateFin = ConvertDate.getValidFormatDate(dateFin);
                }
            } else {
                dateFin = GeneralConst.EMPTY_STRING;
            }
            if (!dateDebut.isEmpty() && !dateFin.isEmpty()) {
                sqlQueryByDates = SQLQueryContentieux.SQL_QUERY_SECONDARY_RECLAMATION_BY_DATES;
                sqlQueryByDates = String.format(sqlQueryByDates, dateDebut.trim(), dateFin.trim());
            }
            if (!typeReclamation.equals(GeneralConst.ALL)) {
                sqlQueryByTypeReclamation = SQLQueryContentieux.SQL_QUERY_SECONDARY_RECLAMATION_BY_TYPE_RECLAMATION;
                sqlQueryByTypeReclamation = String.format(sqlQueryByTypeReclamation, typeReclamation);
            }
        }

        sqlQueryMaster = SQLQueryContentieux.SELECT_LIST_MASTER_RECLAMATION_V2;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlQueryByAssujetti, sqlQueryByReferenceCourrier,
                sqlQueryByReferenceDocument, sqlQueryByDates, sqlQueryByTypeReclamation);

        try {
            List<Reclamation> listReclamation = (List<Reclamation>) myDao.getDaoImpl().find(sqlQueryMaster,
                    Casting.getInstance().convertIntoClassType(Reclamation.class), isLike, valueSearch.trim());
            return listReclamation;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Reclamation getReclamationById(String idReclamation) {
        try {
            String query = SQLQueryContentieux.SELECT_RECLAMATION_BY_ID;
            List<Reclamation> reclamations = (List<Reclamation>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Reclamation.class), false, idReclamation.trim());
            return reclamations.isEmpty() ? null : reclamations.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<DetailsReclamation> getDetailReclamationByFkReclamation(String fkReclamation) {

        String sqlQuery = SQLQueryContentieux.SELECT_DETAIL_RECLAMATION_BY_FK_RECLAMATION;
        try {
            List<DetailsReclamation> listDetailReclamation = (List<DetailsReclamation>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(DetailsReclamation.class), false, fkReclamation.trim());
            return listDetailReclamation;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public static List<BonAPayer> getListBonAPayerByAMR(String codeAMR) {
        
        String query = SQLQueryPaiement.SELECT_BON_A_PAYE_BY_AMR;
        
        try {
             List<BonAPayer> listBonAPayer = (List<BonAPayer>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(BonAPayer.class), false, codeAMR.trim());
        return listBonAPayer;
        } catch (Exception e) {
             throw e;
        }       
    }
    public static List<BonAPayer> getListBonAPayerByAMR_V2(String codeAMR) {
        
        String query = SQLQueryPaiement.SELECT_BON_A_PAYE_BY_AMR_V2;
        
        try {
             List<BonAPayer> listBonAPayer = (List<BonAPayer>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(BonAPayer.class), false, codeAMR.trim());
        return listBonAPayer;
        } catch (Exception e) {
             throw e;
        }       
    }

    public static Decision getDecisionById(int decisionId) {
        try {
            String query = SQLQueryContentieux.SELECT_DECISION_BY_ID;
            List<Decision> decisions = (List<Decision>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Decision.class), false, decisionId);
            return decisions.isEmpty() ? null : decisions.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Service getServiceByCode(String numNC) {
        try {
            String query = SQLQueryContentieux.SELECT_SERVICE_BY_CODE;
            List<Service> service = (List<Service>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Service.class), false, numNC);
            return service.isEmpty() ? null : service.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Reclamation checkReclamationById(String idReclamation) {
        try {
            String query = SQLQueryContentieux.CHECK_RECLAMATION;
            List<Reclamation> reclamations = (List<Reclamation>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Reclamation.class), false, idReclamation.trim());
            return reclamations.isEmpty() ? null : reclamations.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean validerReclamation(String valueState, String idReclamatiob,
            String codeAssujetti, String numAMR, String userId,
            String libelleAB, List<DetailsReclamationPrint> listDetailReclamation) throws IOException {

        HashMap<String, Object[]> bulkContentieux = new HashMap<>();
        propertiesMessage = new PropertiesMessage();
        propertiesConfig = new PropertiesConfig();
        int counter = GeneralConst.Numeric.ZERO;
        boolean result = false;
        String etatReclamation;

        try {
            if (valueState.equals(GeneralConst.Number.ONE)) {
                etatReclamation = GeneralConst.Number.ZERO;
            } else {
                etatReclamation = GeneralConst.Number.THREE;
            }
            Date dateReception = new Date();

            bulkContentieux.put(counter + SQLQueryContentieux.ExecuteQuery.VALIDER_RECLAMATION, new Object[]{
                etatReclamation,
                idReclamatiob,
                dateReception
            });
            counter++;

            //insert to BP with 1/5
            for (DetailsReclamationPrint derecl : listDetailReclamation) {

                if (derecl.getPourcentageMontantNonContster().compareTo(BigDecimal.ZERO)
                        > GeneralConst.Numeric.ZERO) {
                    bulkContentieux.put(counter + SQLQueryContentieux.ExecuteQuery.EXEC_F_NEW_BON_A_PAYER_CONTENTIEUX, new Object[]{
                        derecl.getPourcentageMontantNonContster(),
                        null,
                        userId,
                        numAMR,
                        propertiesMessage.getContent(PropertiesConst.Message.TXT_MOTIF_CONTENTIEUX),
                        codeAssujetti,
                        propertiesConfig.getContent(PropertiesConst.Config.TXT_ACCOUNT_BANK_BAP_CODE),
                        libelleAB.trim().trim(),
                        null,
                        GeneralConst.Numeric.ONE});
                    counter++;
                }

            }

            result = executeQueryBulkInsert(bulkContentieux);

        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    public static boolean executeQueryBulkInsert(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }

    public static Agent getAgentByCode(String codeagent) {
        try {
            String query = SQLQueryContentieux.SELECT_AGENT_BY_CODE;
            List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, codeagent.trim());
            return agent.isEmpty() ? null : agent.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Journal getJournalByDocumentApure(String reference) {
        String query = SQLQueryContentieux.SELECT_JOURNAL_BY_DOCUMENT_APURE;
        List<Journal> listJournal = (List<Journal>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Journal.class), false, reference.trim());
        Journal journal = !listJournal.isEmpty() ? listJournal.get(0) : null;
        return journal;
    }

    public static DetailsNc getDetailNcByNc(String numero) {
        String query = SQLQueryContentieux.LOADER_DETAIL_NC_BY_NC;
        List<DetailsNc> listDetailNC = (List<DetailsNc>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(DetailsNc.class), false, numero.trim());
        return listDetailNC.isEmpty() ? null : listDetailNC.get(0);
    }

    public static Reclamation loadReclamationByTitreAndAssujetti(String titreCode, String codeAssujet) {
        try {
            String query = SQLQueryContentieux.SELECT_RECLAMATION_BY_REFERENCE_DOCUMENT_AND_ASSUJETTI_V2;
            List<Reclamation> reclamations = (List<Reclamation>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Reclamation.class), false, titreCode.trim(), codeAssujet.trim());
            return reclamations.isEmpty() ? null : reclamations.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Amr getAmrCloturer(String numero) {
        String query = SQLQueryContentieux.GET_AMR_CLOTURER;
        List<Amr> listAmr = (List<Amr>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Amr.class), false, numero.trim());
        return listAmr.isEmpty() ? null : listAmr.get(0);
    }

    public static boolean saveRecours(RecoursJuridictionnel recours) throws IOException {

        int counter = GeneralConst.Numeric.ZERO;
        HashMap<String, Object[]> bulkContentieux = new HashMap<>();
        boolean result = false;

        try {
            bulkContentieux.put(counter + SQLQueryContentieux.ExecuteQuery.F_NEW_RECOURS_JURIDICTIONNEL, new Object[]{
                recours.getFkReclamation().getId().trim(),
                recours.getAgentCreat().getCode(),
                recours.getMotifRecours().trim(),
                recours.getNumeroEnregistrementGreffe().trim(),
                recours.getDateDepotRecours()
            });
            counter++;

            result = executeQueryBulkInsert(bulkContentieux);

        } catch (Exception e) {
        }
        return result;
    }

    public static RecoursJuridictionnel getRecoursJuridictionnelByReclamation(String codereclamation) {
        try {
            String query = SQLQueryContentieux.SELECT_SECOURS_JURIDICTIONNEL_BY_RECLEMATION;
            List<RecoursJuridictionnel> recoursJuridict = (List<RecoursJuridictionnel>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RecoursJuridictionnel.class), false, codereclamation.trim());
            return recoursJuridict.isEmpty() ? null : recoursJuridict.get(0);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public static List<ArchiveAccuseReception> getListArchiveByReference(String reference) {
        String sqlQuery = SQLQueryContentieux.SELECT_LIST_ARCHIVE_BY_REFERENCE;
        List<ArchiveAccuseReception> listArchiveAccuseReception = (List<ArchiveAccuseReception>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ArchiveAccuseReception.class), false, reference.trim());
        return listArchiveAccuseReception;
    }
    
    public static boolean saveRecoursJuridictionnel(RecoursJuridictionnel recours) {

        boolean result = false;

        try {

            String query = SQLQueryContentieux.ExecuteQuery.F_NEW_RECOURS_JURIDICTIONNEL;

            result = myDao.getDaoImpl().executeStoredProcedure(
                    query,
                    recours.getFkReclamation().getId().trim(),
                    recours.getAgentCreat().getCode(),
                    recours.getMotifRecours(),
                    recours.getNumeroEnregistrementGreffe(),
                    recours.getDateDepotRecours(),
                    recours.getDateAudience());

        } catch (Exception e) {
            throw e;
        }
        return result;
    }
    
    public static List<RecoursJuridictionnel> loadListRecours(
            String isAdvancedSearch,
            String typeSearch,
            String valueSearch,
            String dateDebut,
            String dateFin,
            String typeReclamation,
            String typeage
    ) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlQueryByAssujetti = GeneralConst.EMPTY_STRING;
        String sqlQueryByReferenceCourrier = GeneralConst.EMPTY_STRING;
        String sqlQueryByDates = GeneralConst.EMPTY_STRING;
        String sqlQueryByReferenceDocument = GeneralConst.EMPTY_STRING;
        String sqlQueryByTypeReclamation = GeneralConst.EMPTY_STRING;

        boolean isLike = false;

        if (isAdvancedSearch.trim().equals(GeneralConst.Number.ZERO)) {
            switch (typeSearch.trim()) {
                case GeneralConst.Number.ONE:
//                    isLike = true;
                    
                    if (typeage.trim().equals(GeneralConst.Number.ONE)){
                        sqlQueryByAssujetti = SQLQueryContentieux.SQL_QUERY_SECONDARY_RECLAMATION_BY_CODE_ASSUJETTI_V3;
                    }else{
                        sqlQueryByAssujetti = SQLQueryContentieux.SQL_QUERY_SECONDARY_RECLAMATION_BY_CODE_ASSUJETTI_V4;
                    }
                    
                    sqlQueryByAssujetti = String.format(sqlQueryByAssujetti, valueSearch.trim());
                    break;
                case GeneralConst.Number.TWO:
                    sqlQueryByReferenceCourrier = SQLQueryContentieux.SQL_QUERY_SECONDARY_RECOURS_BY_NUMERO_REGISTRE;
                    break;
                case GeneralConst.Number.THREE:
                    sqlQueryByReferenceDocument = SQLQueryContentieux.SQL_QUERY_SECONDARY_RECOURS_BY_REFERENCE_DOCUMENT;
                    break;
            }
        } else if (isAdvancedSearch.trim().equals(GeneralConst.Number.ONE)) {

            if (dateDebut != null) {
                if (dateDebut.contains("-")) {
                    dateDebut = ConvertDate.getValidFormatDate(dateDebut);
                }
            } else {
                dateDebut = GeneralConst.EMPTY_STRING;
            }

            if (dateFin != null) {
                if (dateFin.contains("-")) {
                    dateFin = ConvertDate.getValidFormatDate(dateFin);
                }
            } else {
                dateFin = GeneralConst.EMPTY_STRING;
            }
            if (!dateDebut.isEmpty() && !dateFin.isEmpty()) {
                sqlQueryByDates = SQLQueryContentieux.SQL_QUERY_SECONDARY_RECOURS_BY_DATE_V2;
                sqlQueryByDates = String.format(sqlQueryByDates, dateDebut.trim(), dateFin.trim());
            }
        }

        sqlQueryMaster = SQLQueryContentieux.SELECT_LIST_MASTER_RECOURS_JURIDICTIONNEL_V2;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlQueryByAssujetti, sqlQueryByReferenceCourrier,
                sqlQueryByReferenceDocument, sqlQueryByDates);

        try {
            List<RecoursJuridictionnel> listRecours = (List<RecoursJuridictionnel>) myDao.getDaoImpl().find(sqlQueryMaster,
                    Casting.getInstance().convertIntoClassType(RecoursJuridictionnel.class), isLike, valueSearch.trim());
            return listRecours;
        } catch (Exception e) {
            throw e;
        }
    }

}
