/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.AssujettissementBusiness.propertiesConfig;
import static cd.hologram.erecettesvg.business.IdentificationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Assujettissement;
import cd.hologram.erecettesvg.models.Axe;
import cd.hologram.erecettesvg.models.AxePeage;
import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.CarteVoucher;
import cd.hologram.erecettesvg.models.Commande;
import cd.hologram.erecettesvg.models.CommandeCarte;
import cd.hologram.erecettesvg.models.ComplementBien;
import cd.hologram.erecettesvg.models.DetailAssujettissement;
import cd.hologram.erecettesvg.models.DetailVoucher;
import cd.hologram.erecettesvg.models.DetailsPrevisionCredit;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.PersonneExemptee;
import cd.hologram.erecettesvg.models.PrevisionCredit;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.TarifSite;
import cd.hologram.erecettesvg.models.TicketPeage;
import cd.hologram.erecettesvg.models.Voucher;
import cd.hologram.erecettesvg.sql.SQLQueryPeage;
import cd.hologram.erecettesvg.util.Casting;
import entites.RemiseSousProvisionPeage;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author WILLY KASHALA
 */
public class PeageBusiness {

    private static final Dao myDao = new Dao();

    public static DetailAssujettissement getDetailAssujettissementByID(int id) {
        try {
            String query = "SELECT * FROM T_DETAIL_ASSUJETTISSEMENT da WITH (READPAST) WHERE da.ID = ?1";
            List<DetailAssujettissement> detailAssujettissements = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, id);
            return detailAssujettissements.isEmpty() ? null : detailAssujettissements.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static TarifSite getTarifSiteBySiteAndTarif(String codeSite, String codeTatif) {
        try {
            String query = "select * from T_TARIF_SITE WITH (READPAST) WHERE FK_SITE_PROVENANCE = ?1 AND FK_SITE_DESTINANTION = ?1 AND FK_TARIF = ?2 AND ETAT = 1";
            List<TarifSite> detailAssujettissements = (List<TarifSite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TarifSite.class), false, codeSite, codeTatif);
            return detailAssujettissements.isEmpty() ? null : detailAssujettissements.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Personne> getAssujettiByName(String libelle) {
        try {
            String query = SQLQueryPeage.SELECT_ASSUJETTI_BY_NAME;
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), true, libelle);
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getAssujettiByCodeOrNif(String nif, boolean byCode) {
        try {
            String query = byCode
                    ? SQLQueryPeage.SELECT_ASSUJETTI_BY_CODE
                    : SQLQueryPeage.SELECT_ASSUJETTI_BY_NIF;

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, nif);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<ArticleBudgetaire> getArticleBudgetaireOfAssujetti(String codeAssujetti) {
        try {
            String query = SQLQueryPeage.SELECT_ARTICLE_OF_ASSUJETTI;
            List<ArticleBudgetaire> articleList = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, codeAssujetti);
            return articleList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Assujettissement> getAssujettissementOfAssujetti(String codeAssujetti) {
        try {
            String query = "SELECT a.* FROM T_ASSUJETTISSEMENT a WITH (READPAST) WHERE a.PERSONNE = ?1 AND a.ETAT = 1";
            List<Assujettissement> assujettissements = (List<Assujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujettissement.class), false, codeAssujetti);
            return assujettissements;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailAssujettissement> getDetailAssujettissementOfAssujetti(String codeAssujetti) {
        try {
            String query = "SELECT da.* FROM T_DETAIL_ASSUJETTISSEMENT da WITH (READPAST) JOIN T_BIEN b WITH (READPAST) "
                    + " ON da.BIEN = b.ID WHERE da.ASSUJETTISSEMENT IN "
                    + " (SELECT a.CODE FROM T_ASSUJETTISSEMENT a WITH (READPAST) WHERE a.PERSONNE = ?1 AND a.ETAT = 1) "
                    + " AND da.FK_PERSONNE = ?1 AND da.ETAT IN (1,2) ORDER BY b.INTITULE";
            List<DetailAssujettissement> detailAssujettissements = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, codeAssujetti);
            return detailAssujettissements;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<PrevisionCredit> getPrepaiementListByAssujetti(String codeAssujetti) {
        try {
            String query = "SELECT p.* FROM T_PREVISION_CREDIT p WITH (READPAST) WHERE p.FK_PERSONNE = ?1 AND p.ETAT > 0 "
                    + " AND p.CODE IN (SELECT dp.FK_PREVISION_CREDIT FROM T_DETAILS_PREVISION_CREDIT dp WITH (READPAST) WHERE dp.TYPE = 1)";
            List<PrevisionCredit> prepaiements = (List<PrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PrevisionCredit.class), false, codeAssujetti);
            return prepaiements;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<PrevisionCredit> getCreditListByAssujetti(String codeAssujetti) {
        try {
            String query = "SELECT p.* FROM T_PREVISION_CREDIT p WITH (READPAST) WHERE p.FK_PERSONNE = ?1 AND p.ETAT > 0 "
                    + " AND p.CODE IN (SELECT dp.FK_PREVISION_CREDIT FROM T_DETAILS_PREVISION_CREDIT dp WITH (READPAST) WHERE dp.TYPE = 2)";
            List<PrevisionCredit> prepaiements = (List<PrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PrevisionCredit.class), false, codeAssujetti);
            return prepaiements;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailAssujettissement> getDetailsAssujettissementByArticle(String codeArticle, String codeAssujetti) {
        try {
            String query = SQLQueryPeage.SELECT_DETAIL_ASSUJETTISSEMENT_BY_ARTICLE;
            List<DetailAssujettissement> dtlAssujettissement = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, codeArticle, codeAssujetti);
            return dtlAssujettissement;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailAssujettissement> getDetailsAssujettissementByPersonne(String codeAssujetti) {
        try {
            String query = "SELECT * from T_DETAIL_ASSUJETTISSEMENT da WITH (READPAST) WHERE da.FK_PERSONNE = ?1 AND da.ETAT = 1 "
                    + " AND da.ID NOT IN (SELECT dpc.FK_DETAIL_ASSUJETTISSEMENT FROM T_DETAILS_PREVISION_CREDIT dpc WITH (READPAST) WHERE dpc.ETAT = 1)";
            List<DetailAssujettissement> dtlAssujettissement = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, codeAssujetti);
            return dtlAssujettissement;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailsPrevisionCredit> getDetailsPrePaiements(String codePrepaiement) {
        try {
            String query = "SELECT * from T_DETAILS_PREVISION_CREDIT dp WITH (READPAST) "
                    + "WHERE dp.FK_PREVISION_CREDIT = ?1 AND dp.TYPE = 1 AND dp.ETAT > 0 ORDER BY dp.FK_BIEN";
            List<DetailsPrevisionCredit> detailsPrevisionCredits = (List<DetailsPrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsPrevisionCredit.class), false, codePrepaiement);
            return detailsPrevisionCredits;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailsPrevisionCredit> getDetailsCredits(String codeCredit) {
        try {
            String query = "SELECT * from T_DETAILS_PREVISION_CREDIT dp WITH (READPAST) "
                    + "WHERE dp.FK_PREVISION_CREDIT = ?1 AND dp.TYPE = 2 AND dp.ETAT > 0 ORDER BY dp.FK_BIEN";
            List<DetailsPrevisionCredit> detailsPrevisionCredits = (List<DetailsPrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsPrevisionCredit.class), false, codeCredit);
            return detailsPrevisionCredits;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static String savePrevisionCredit(
            List<DetailsPrevisionCredit> listDetailPrevisionCredit, PrevisionCredit previsionCredit) {

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int counter = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> paramsNC = new HashMap<>();

        String firstStoredProcedure = "F_NEW_PREVISION_CREDIT";
        String firstStoredReturnValueKey = "ID";

        paramsNC.put("FK_PERSONNE", previsionCredit.getFkPersonne().trim());
        paramsNC.put("AGENT_CREATE", previsionCredit.getAgentCreate());

        for (DetailsPrevisionCredit detailsPrevisionCredit : listDetailPrevisionCredit) {

            counter++;

            firstBulk.put(counter + SQLQueryPeage.SAVE_DETAILS_PREVISION_CREDIT_V2, new Object[]{
                detailsPrevisionCredit.getFkBien(),
                detailsPrevisionCredit.getTaux(),
                detailsPrevisionCredit.getType(),
                detailsPrevisionCredit.getNbreTourSouscrit(),
                detailsPrevisionCredit.getTotalTaux(),
                detailsPrevisionCredit.getFkDevise(),
                detailsPrevisionCredit.getFkDetailAssujettissement()
            });

        }

        String result = myDao.getDaoImpl().execBulkQueryv4(
                firstStoredProcedure, firstStoredReturnValueKey, paramsNC, firstBulk);

        return result;
    }

    public static Assujettissement getAssujettissementByArticleAndAssujetti(String codeArticle, String CodeAssujetti) {
        try {
            String query = SQLQueryPeage.SELECT_ASSUJETTISSEMENT_BY_ARTICLE_AND_ASSUJETTI;

            List<Assujettissement> assujettissement = (List<Assujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujettissement.class), false, codeArticle, CodeAssujetti);
            return assujettissement.isEmpty() ? null : assujettissement.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Personne> getPersonnesByName(String libelle) {
        try {
            String query = SQLQueryPeage.SELECT_PERSONNES_BY_NAME;
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), true, libelle);
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getPersonneByCodeOrNif(String nif, boolean byCode) {
        try {
            String query = byCode
                    ? SQLQueryPeage.SELECT_PERSONNE_BY_CODE
                    : SQLQueryPeage.SELECT_PERSONNE_BY_NIF;

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, nif);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean exempteAssujettis(PersonneExemptee personneExempte) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;

        try {

            bulkQuery.put(counter + SQLQueryPeage.EXEMPTE_ASSUJETTI, new Object[]{
                personneExempte.getPersonne().getCode().trim(),
                personneExempte.getAgentCreat(),
                personneExempte.getDateCreate(),
                personneExempte.getEtat()
            });

            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static PersonneExemptee checkPersonneExemptes(String codePersonne) {
        try {
            String query = SQLQueryPeage.SELECT_PERSONNE_EXEMPTE;

            List<PersonneExemptee> personnes = (List<PersonneExemptee>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PersonneExemptee.class), false, codePersonne);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean disabledPersonneExemptes(String codePersonne) {

        boolean result = false;
        int idCodePer = Integer.valueOf(codePersonne);
        try {
            String query = SQLQueryPeage.DISABLED_PERSONNE_EXEMPTE;

            result = myDao.getDaoImpl().executeStoredProcedure(query, idCodePer);

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TicketPeage> getTicketPeages(String codeTicket, String codeSite, int percepteur) {

        try {

            String query = "";

            if (codeSite.equals("*")) {

                query = "SELECT * FROM T_TICKET_PEAGE WITH (READPAST) WHERE CODE = ?1 AND ETAT IN (1,2)";

                return (List<TicketPeage>) myDao.getDaoImpl().find(query, Casting.getInstance().convertIntoClassType(TicketPeage.class),
                        false, codeTicket.trim());

            } else {

                query = "SELECT * FROM T_TICKET_PEAGE WITH (READPAST) WHERE CODE = ?1 AND ETAT IN (1,2) AND SITE = ?2 %s";

                String sqlParam = GeneralConst.EMPTY_STRING;

                if (percepteur > 0) {
                    sqlParam = " AND AGENT_CREAT = %s";
                    sqlParam = String.format(sqlParam, percepteur);
                }

                query = String.format(query, sqlParam);

                return (List<TicketPeage>) myDao.getDaoImpl().find(query, Casting.getInstance().convertIntoClassType(TicketPeage.class),
                        false, codeTicket.trim(), codeSite.trim());
            }

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TicketPeage> getTicketCanceledPeages(String codeTicket, String codeSite, int percepteur) {

        try {

            String query = "SELECT * FROM T_TICKET_PEAGE WITH (READPAST) WHERE CODE = ?1 AND ETAT = 0 AND SITE = ?2 %s";

            String sqlParam = GeneralConst.EMPTY_STRING;

            if (percepteur > 0) {
                sqlParam = " AND AGENT_CREAT = %s";
                sqlParam = String.format(sqlParam, percepteur);
            }

            query = String.format(query, sqlParam);

            return (List<TicketPeage>) myDao.getDaoImpl().find(query, Casting.getInstance().convertIntoClassType(TicketPeage.class),
                    false, codeTicket.trim(), codeSite.trim());

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TicketPeage> getTicketPeages(String siteCode, String typePaiement, String dateDebut, String dateFin,
            String tarif, String percepteur, String shift) {

        try {

            String query = "SELECT t.* FROM T_TICKET_PEAGE t WITH (READPAST) WHERE t.ETAT IN (1,2) %s %s %s %s %s %s ORDER BY t.CODE";

            String sqlParam_1 = GeneralConst.EMPTY_STRING;
            String sqlParam_2 = GeneralConst.EMPTY_STRING;
            String sqlParam_3 = GeneralConst.EMPTY_STRING;
            String sqlParam_4 = GeneralConst.EMPTY_STRING;
            String sqlParam_5 = GeneralConst.EMPTY_STRING;

            if (!siteCode.equals("*")) {
                sqlParam_1 = " AND t.SITE = '%s'";
                sqlParam_1 = String.format(sqlParam_1, siteCode.trim());
            }

            if (!typePaiement.equals("*")) {
                sqlParam_2 = " AND t.TYPE_PAIEMENT = '%s'";
                sqlParam_2 = String.format(sqlParam_2, typePaiement.trim());
            }

            if (!tarif.equals("*")) {
                sqlParam_3 = " AND t.FK_TARIF = '%s'";
                sqlParam_3 = String.format(sqlParam_3, tarif.trim());
            }

            if (!percepteur.equals("*")) {
                sqlParam_4 = " AND t.AGENT_CREAT = %s";
                sqlParam_4 = String.format(sqlParam_4, Integer.valueOf(percepteur.trim()));
            }

            sqlParam_5 = " AND DBO.FDATE(DATE_PROD) BETWEEN '%s' AND '%s'";
            sqlParam_5 = String.format(sqlParam_5, dateDebut, dateFin);

            String sqlParamShift = GeneralConst.EMPTY_STRING;

            if (!shift.equals("*")) {
                sqlParamShift = " AND t.SHIFT_TRAVAIL = '%s'";
                sqlParamShift = String.format(sqlParamShift, shift);
            }

            query = String.format(query, sqlParam_1, sqlParam_2, sqlParam_3, sqlParam_4, sqlParam_5, sqlParamShift);

            List<TicketPeage> ticketPeageList = (List<TicketPeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TicketPeage.class), false);

            return ticketPeageList;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TicketPeage> getTicketCanceledPeages(String siteCode, String typePaiement, String dateDebut, String dateFin,
            String tarif, String percepteur) {

        try {

            String query = "SELECT t.* FROM T_TICKET_PEAGE t WITH (READPAST) WHERE t.ETAT = 0 %s %s %s %s %s ORDER BY t.CODE";

            String sqlParam_1 = GeneralConst.EMPTY_STRING;
            String sqlParam_2 = GeneralConst.EMPTY_STRING;
            String sqlParam_3 = GeneralConst.EMPTY_STRING;
            String sqlParam_4 = GeneralConst.EMPTY_STRING;
            String sqlParam_5 = GeneralConst.EMPTY_STRING;

            if (!siteCode.equals("*")) {
                sqlParam_1 = " AND t.SITE = '%s'";
                sqlParam_1 = String.format(sqlParam_1, siteCode.trim());
            }

            if (!typePaiement.equals("*")) {
                sqlParam_2 = " AND t.TYPE_PAIEMENT = '%s'";
                sqlParam_2 = String.format(sqlParam_2, typePaiement.trim());
            }

            if (!tarif.equals("*")) {
                sqlParam_3 = " AND t.FK_TARIF = '%s'";
                sqlParam_3 = String.format(sqlParam_3, tarif.trim());
            }

            if (!percepteur.equals("*")) {
                sqlParam_4 = " AND t.AGENT_CREAT = %s";
                sqlParam_4 = String.format(sqlParam_4, Integer.valueOf(percepteur.trim()));
            }

            sqlParam_5 = " AND DBO.FDATE(DATE_PROD) BETWEEN '%s' AND '%s'";
            sqlParam_5 = String.format(sqlParam_5, dateDebut, dateFin);

            query = String.format(query, sqlParam_1, sqlParam_2, sqlParam_3, sqlParam_4, sqlParam_5);

            List<TicketPeage> ticketPeageList = (List<TicketPeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TicketPeage.class), false);

            return ticketPeageList;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static ComplementBien getComplementBienOfImm(String idBien, String tcImm) {
        try {
            String query = "SELECT * FROM T_COMPLEMENT_BIEN WHERE BIEN = ?1 AND TYPE_COMPLEMENT IN "
                    + "(SELECT CODE FROM T_TYPE_COMPLEMENT_BIEN WHERE COMPLEMENT = ?2 AND ETAT = 1) AND ETAT = 1";

            List<ComplementBien> complementBienList = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class), false, idBien, tcImm);
            return complementBienList.isEmpty() ? null : complementBienList.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailAssujettissement getDetailAssujettissementByBienAndAssuj(String idBien, String ab, String codePers) {
        try {
            String query = "SELECT * FROM T_DETAIL_ASSUJETTISSEMENT WHERE BIEN = ?1 AND ASSUJETTISSEMENT = "
                    + "(SELECT TOP(1) CODE FROM T_ASSUJETTISSEMENT WHERE ARTICLE_BUDGETAIRE = ?2 AND PERSONNE = ?3 AND ETAT = 1) AND ETAT = 1";

            List<DetailAssujettissement> complementBienList = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, idBien, ab, codePers);
            return complementBienList.isEmpty() ? null : complementBienList.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static ComplementBien getComplementBienByBienAndPlaque(String plaque, String bien) {
        try {
            String query = "SELECT cb.* from T_COMPLEMENT_BIEN cb WITH (READPAST) "
                    + " WHERE cb.TYPE_COMPLEMENT IN (SELECT  tcb.COMPLEMENT FROM T_TYPE_COMPLEMENT_BIEN tcb WITH (READPAST) "
                    + " WHERE tcb.COMPLEMENT = ?1) "
                    + " AND cb.BIEN = ?2";
            List<ComplementBien> complementBiens = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class), false, plaque, bien);
            return complementBiens.isEmpty() ? null : complementBiens.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean editPrepaiement(String code, int userId, String state) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            switch (state) {
                case "1":
                    bulkQuery.put(0 + ":UPDATE T_PREVISION_CREDIT SET ETAT = 3, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE CODE = ?2 ", new Object[]{
                        userId,
                        code.trim()
                    });

                    bulkQuery.put(1 + ":UPDATE T_DETAILS_PREVISION_CREDIT SET ETAT = 3, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE FK_PREVISION_CREDIT = ?2 ", new Object[]{
                        userId,
                        code.trim()
                    });
                    break;
                case "2":
                    bulkQuery.put(0 + ":UPDATE T_PREVISION_CREDIT SET ETAT = 1, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE CODE = ?2 ", new Object[]{
                        userId,
                        code.trim()
                    });
                    break;
                case "3":
                    bulkQuery.put(0 + ":UPDATE T_PREVISION_CREDIT SET ETAT = 1, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE CODE = ?2 ", new Object[]{
                        userId,
                        code.trim()
                    });
                    bulkQuery.put(1 + ":UPDATE T_DETAILS_PREVISION_CREDIT SET ETAT = 1, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE FK_PREVISION_CREDIT = ?2 AND ETAT = 1", new Object[]{
                        userId,
                        code.trim()
                    });
                    break;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean saveRenouvellementSouProvision(int userId, int numberTour,
            BigDecimal inputTotalAPayer, int type, String devsie, BigDecimal taux, String bien,
            int detailAssujId, String prePaiementCode) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":INSERT INTO T_DETAILS_PREVISION_CREDIT (FK_PREVISION_CREDIT,FK_BIEN,TAUX,TYPE,NBRE_TOUR_SOUSCRIT,TOTAL_TAUX,"
                    + "FK_DEVISE,FK_DETAIL_ASSUJETTISSEMENT,AGENT_CREATE,ETAT) VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,2)";

            bulkQuery.put(0 + query, new Object[]{
                prePaiementCode, bien,
                taux, type, numberTour, inputTotalAPayer,
                devsie, detailAssujId, userId

            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean activateDP(int userId, int id) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_DETAILS_PREVISION_CREDIT SET ETAT = 1,AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE ID = ?2";

            bulkQuery.put(0 + query, new Object[]{
                userId, id
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean suspendreBienPrevisionCredit(int userId, int id) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_DETAILS_PREVISION_CREDIT SET ETAT = 3,AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE ID = ?2";

            bulkQuery.put(0 + query, new Object[]{
                userId, id
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean activateExemption(int userId, int id) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_DETAIL_ASSUJETTISSEMENT SET ETAT = 2,AGENT_CREATE_EXEMPTION = ?1, DATE_CREATE_EXEMPTION = GETDATE() WHERE ID = ?2";

            bulkQuery.put(0 + query, new Object[]{
                userId, id
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean activateAllExemption(int userId, List<String> listId) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;
        int count = 0;

        try {

            String query = GeneralConst.EMPTY_STRING;

            for (String id : listId) {

                query = ":UPDATE T_DETAIL_ASSUJETTISSEMENT SET ETAT = 2,AGENT_CREATE_EXEMPTION = ?1, DATE_CREATE_EXEMPTION = GETDATE() WHERE ID = ?2";

                bulkQuery.put(count + query, new Object[]{
                    userId, Integer.valueOf(id)
                });

                count++;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean canceledTicket(int userId, List<String> listTicket, String observation, String codeTicket) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;
        int count = 0;

        try {

            String query = GeneralConst.EMPTY_STRING;

            for (String ticket : listTicket) {

                query = ":UPDATE T_TICKET_PEAGE SET ETAT = 0,DATE_MAJ = GETDATE(), AGENT_MAJ = ?1, OBSERVATION = ?2,TICKET_DOCUMENT = ?3 WHERE CODE = ?4";

                bulkQuery.put(count + query, new Object[]{
                    userId, observation, ticket, codeTicket
                });

                count++;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean desactivateAllExemption(int userId, List<String> listId) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;
        int count = 0;

        try {

            String query = GeneralConst.EMPTY_STRING;

            for (String id : listId) {

                query = ":UPDATE T_DETAIL_ASSUJETTISSEMENT SET ETAT = 1,AGENT_MAJ_EXEMPTION = ?1, DATE_MAJ_EXEMPTION = GETDATE() WHERE ID = ?2";

                bulkQuery.put(count + query, new Object[]{
                    userId, Integer.valueOf(id)
                });

                count++;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean desactivateExemption(int userId, int id) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_DETAIL_ASSUJETTISSEMENT SET ETAT = 1,AGENT_MAJ_EXEMPTION = ?1, DATE_MAJ_EXEMPTION = GETDATE() WHERE ID = ?2";

            bulkQuery.put(0 + query, new Object[]{
                userId, id
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean remplacerBienPrevisionCredit(int userId, String codePersonne, String bien) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_DETAILS_PREVISION_CREDIT SET AGENT_MAJ = ?1, FK_BIEN = ?2, DATE_MAJ = GETDATE() "
                    + " WHERE FK_PREVISION_CREDIT IN (SELECT CODE FROM T_PREVISION_CREDIT WITH (READPAST) WHERE FK_PERSONNE = ?3) AND ETAT = 1";

            bulkQuery.put(0 + query, new Object[]{
                userId, bien, codePersonne
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<Bien> getListBiensRemplacementByAssujetti(String codeAssujtti, String codeBien, String codeTarif) {

        try {

            String query = "SELECT B.* FROM T_BIEN B WITH (READPAST) "
                    + " WHERE B.ID IN (SELECT DA.BIEN FROM T_DETAIL_ASSUJETTISSEMENT DA WITH (READPAST) "
                    + " WHERE DA.FK_PERSONNE = ?1 AND DA.BIEN <> ?2 AND DA.TARIF = ?3 AND DA.ETAT = 1) "
                    + " AND B.ETAT = 1 ORDER BY B.INTITULE";
            return (List<Bien>) myDao.getDaoImpl().find(query, Casting.getInstance().convertIntoClassType(Bien.class), false,
                    codeAssujtti.trim(), codeBien.trim(), codeTarif.trim());

        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailsPrevisionCredit getDetailsPrevisionCreditById(String id) {
        try {
            String query = "SELECT * FROM T_DETAILS_PREVISION_CREDIT WITH (READPAST) WHERE ID = ?1";

            List<DetailsPrevisionCredit> detailsPrevisionCredits = (List<DetailsPrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsPrevisionCredit.class), false, id);
            return detailsPrevisionCredits.isEmpty() ? null : detailsPrevisionCredits.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static PrevisionCredit getPrevisionCreditByDetailId(String detailId) {
        try {
            String query = "SELECT * FROM T_PREVISION_CREDIT WITH (READPAST) "
                    + " WHERE CODE IN (SELECT FK_PREVISION_CREDIT FROM T_DETAILS_PREVISION_CREDIT WITH (READPAST) WHERE ID = ?1)";

            List<PrevisionCredit> proCredits = (List<PrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PrevisionCredit.class), false, detailId);
            return proCredits.isEmpty() ? null : proCredits.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Site> getAllSitePeage() {
        try {
            String query = "SELECT s.* FROM T_SITE s WITH (READPAST) WHERE s.PEAGE = 1 AND s.ETAT = 1 ORDER BY s.INTITULE";
            List<Site> sites = (List<Site>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Site.class), false);
            return sites;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static TicketPeage getTicketPeageByCode(String code) {
        try {
            String query = "SELECT * FROM T_TICKET_PEAGE WITH (READPAST) WHERE CODE = ?1";

            List<TicketPeage> ticketPeages = (List<TicketPeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TicketPeage.class), false, code);
            return ticketPeages.isEmpty() ? null : ticketPeages.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Commande> getCommandeVoucherByDate(String dateDebut, String dateFin, String codeAxe, String codeState, String typeCmd) {

        String sqlQueryAxe = "";

        if (!codeAxe.equals("*")) {
            sqlQueryAxe = " AND C.ID IN (SELECT V.FK_COMMANDE FROM T_VOUCHER V WITH (READPAST) WHERE V.FK_AXE = %s)";
            sqlQueryAxe = String.format(sqlQueryAxe, Integer.valueOf(codeAxe));
        }

        String sqlQueryState = "";

        if (!codeState.equals("*")) {

            int state = 0;

            switch (codeState) {
                case "4":
                    state = 3;
                    break;
                case "3":
                    state = 2;
                    break;
                case "2":
                    state = 1;
                    break;
                case "1":
                    state = 4;
                    break;

            }

            sqlQueryState = " AND C.ETAT = %s";
            sqlQueryState = String.format(sqlQueryState, state);
        }

        String sqlQueryTypeCmd = "";

        if (!typeCmd.equals("*")) {

            switch (typeCmd) {
                case "CREDIT":
                    //sqlQueryTypeCmd = " AND C.ID IN (SELECT V.FK_COMMANDE FROM T_VOUCHER V WITH (READPAST) WHERE V.REMISE = 0)";
                    sqlQueryTypeCmd = " AND C.TYPE_CMD = 2";
                    break;
                case "PRE-PAIEMENT":
                    //sqlQueryTypeCmd = " AND C.ID IN (SELECT V.FK_COMMANDE FROM T_VOUCHER V WITH (READPAST) WHERE V.REMISE > 0)";
                    sqlQueryTypeCmd = " AND C.TYPE_CMD = 1";
                    break;
                case "EXONERATION":
                    sqlQueryTypeCmd = " AND C.TYPE_CMD = 3";
                    break;
            }

        }

        String sqlQuery = " SELECT C.* FROM T_COMMANDE C WITH (READPAST) WHERE DBO.FDATE(C.DATE_CREAT) BETWEEN '%s' AND '%s' %s %s %s";

        sqlQuery = String.format(sqlQuery, dateDebut, dateFin, sqlQueryAxe, sqlQueryState, sqlQueryTypeCmd);

        List<Commande> cmdVoucherList = new ArrayList<>();

        try {

            cmdVoucherList = (List<Commande>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Commande.class), false);

        } catch (Exception e) {
        }

        return cmdVoucherList;

    }

    public static List<Commande> getCommandeVouche(String valueSearch, int typeSearch) {

        String secondaryQuery_One = "";
        String secondaryQuery_Two = "";
        String primaryQuery;

        boolean like = false;

        if (typeSearch == 1) {
            secondaryQuery_One = "AND C.FK_PERSONNE  IN (SELECT P.CODE FROM T_PERSONNE P "
                    + " WHERE (P.NOM LIKE '%" + valueSearch.trim() + "%' OR P.POSTNOM LIKE '%" + valueSearch.trim() + "%' OR P.PRENOMS LIKE '%" + valueSearch.trim() + "%')) ";
            like = true;
        } else if (typeSearch == 2) {
            secondaryQuery_Two = " AND (CAST(C.ID AS VARCHAR(20)) = '" + valueSearch.trim() + "' OR C.REFERENCE = '" + valueSearch.trim() + "')";
            like = true;
        }

        primaryQuery = " SELECT C.* FROM T_COMMANDE C WITH (READPAST) WHERE C.ETAT <> 0 %s %s";
        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two);

        try {

            List<Commande> cmdVoucherList = (List<Commande>) myDao.getDaoImpl().find(primaryQuery,
                    Casting.getInstance().convertIntoClassType(Commande.class), like);
            return cmdVoucherList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Commande> getCmdVoucherByAssujetti(String codeAssujetti) {
        try {
            String query = "SELECT C.* FROM T_COMMANDE C WITH (READPAST) WHERE C.FK_PERSONNE = ?1";
            List<Commande> cmdVoucherList = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, codeAssujetti);
            return cmdVoucherList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Commande> loadCmdVoucherByAssujetti() {
        try {
            String query = "SELECT C.* FROM T_COMMANDE C WITH (READPAST) WHERE C.ETAT <> 0";
            List<Commande> cmdVoucherList = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false);
            return cmdVoucherList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Voucher> getVoucherByCommande(String codeCmd) {
        try {
            String query = "SELECT V.* FROM T_VOUCHER V WITH (READPAST) WHERE V.ETAT <> 0 AND V.FK_COMMANDE = ?1 AND V.STATUT = 1";
            List<Voucher> voucherList = (List<Voucher>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Voucher.class), false, codeCmd);
            return voucherList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean updateVoucher(List<Voucher> voucherList, BigDecimal sumFinal, String codeCptBank) throws Exception {

        int counter = 0;
        Boolean result = false;
        String query;
        String query2;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean checkRemise = Boolean.valueOf(propertiesConfig.getProperty("checkRemise"));

        Integer commandID = voucherList.get(0).getFkCommande().getId();

        try {
            if (checkRemise) {
                query = ":UPDATE T_VOUCHER WITH (READPAST) SET REMISE = ?1, "
                        + "MONTANT_FINAL = ?2, AGENT_MAJ = ?3 ,ETAT = 2 WHERE ID = ?4";

                query2 = ":UPDATE T_COMMANDE WITH (READPAST) SET MONTANT = ?1, ETAT = 2, FK_COMPTE_BANCAIRE = ?2 WHERE ID = ?3";

                for (Voucher vchr : voucherList) {
                    bulkQuery.put(counter + query, new Object[]{
                        vchr.getRemise(),
                        vchr.getMontantFinal(),
                        vchr.getAgentMaj().getCode(),
                        vchr.getId()});
                    counter++;
                }

                counter++;
                bulkQuery.put(counter + query2, new Object[]{
                    sumFinal,
                    codeCptBank.trim(),
                    commandID});

            } else {
                query = ":UPDATE T_VOUCHER WITH (READPAST) SET ETAT = 2, AGENT_MAJ = ?1 WHERE ID = ?2";

                query2 = ":UPDATE T_COMMANDE WITH (READPAST) SET  ETAT = 2, FK_COMPTE_BANCAIRE = ?1 WHERE ID = ?2";
                for (Voucher vchr : voucherList) {
                    bulkQuery.put(counter + query, new Object[]{
                        vchr.getAgentMaj().getCode(),
                        vchr.getId()});
                    counter++;
                }

                counter++;
                bulkQuery.put(counter + query2, new Object[]{
                    codeCptBank.trim(),
                    commandID});
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static List<DetailVoucher> getDetailVoucherOfVoucher(String codeVoucher) {
        try {
            String query = "SELECT DV.* FROM T_DETAIL_VOUCHER DV WITH (READPAST)  "
                    + " WHERE DV.FK_VAUCHER IN (SELECT V.FK_COMMANDE FROM T_VOUCHER V WITH (READPAST) "
                    + " WHERE V.ETAT = 2 AND FK_COMMANDE = ?1) AND DV.ETAT = 4 ";
            List<DetailVoucher> detailVouchers = (List<DetailVoucher>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailVoucher.class), false, codeVoucher);
            return detailVouchers;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean saveRemisePeageForEdit(RemiseSousProvisionPeage remiseSousProvisionPeage) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = ":INSERT INTO T_REMISE_SOUS_PROVISION_PEAGE (FK_PERSONNE,FK_TARIF,TAUX,TYPE,FK_AXE,AGENT_CREATE,DEVISE_TAUX,ETAT) "
                    + " VALUES (?1,?2,?3,?4,?5,?6,?7,1)";

            counter++;
            bulkQuery.put(counter + query, new Object[]{
                remiseSousProvisionPeage.getFkPersonne().getCode(),
                remiseSousProvisionPeage.getFkTarif().getCode(),
                remiseSousProvisionPeage.getTaux(),
                remiseSousProvisionPeage.getType(),
                remiseSousProvisionPeage.getFkAxe(),
                remiseSousProvisionPeage.getAgentCreate(),
                remiseSousProvisionPeage.getDeviseTaux(),
                1});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static boolean saveRemisePeageForModification(RemiseSousProvisionPeage remiseSousProvisionPeage) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = ":UPDATE T_REMISE_SOUS_PROVISION_PEAGE SET FK_PERSONNE = ?1, FK_TARIF = ?2,TAUX = ?3,TYPE = ?4,FK_AXE =?5,AGENT_MAJ = ?6,DEVISE_TAUX = ?7,DATE_MAJ = GETDATE() WHERE ID = ?8";

            counter++;
            bulkQuery.put(counter + query, new Object[]{
                remiseSousProvisionPeage.getFkPersonne().getCode(),
                remiseSousProvisionPeage.getFkTarif().getCode(),
                remiseSousProvisionPeage.getTaux(),
                remiseSousProvisionPeage.getType(),
                remiseSousProvisionPeage.getFkAxe(),
                remiseSousProvisionPeage.getAgentMaj(),
                remiseSousProvisionPeage.getDeviseTaux(),
                remiseSousProvisionPeage.getId()});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static boolean deleteRemisePeageForModification(int id, int userId) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = ":UPDATE T_REMISE_SOUS_PROVISION_PEAGE SET ETAT = 0,AGENT_MAJ = ?1,DATE_MAJ = GETDATE() WHERE ID = ?2";

            counter++;
            bulkQuery.put(counter + query, new Object[]{userId, id});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static List<RemiseSousProvisionPeage> getListRemisePeage() {

        String sqlQuery = "SELECT r.* FROM T_REMISE_SOUS_PROVISION_PEAGE r WITH (READPAST) JOIN T_PERSONNE p WITH (READPAST)"
                + " ON r.FK_PERSONNE = p.CODE WHERE r.ETAT = 1 ORDER BY p.NOM";

        List<RemiseSousProvisionPeage> listRemiseSousProvisionPeage = new ArrayList<>();

        try {

            listRemiseSousProvisionPeage = (List<RemiseSousProvisionPeage>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(RemiseSousProvisionPeage.class), false);

        } catch (Exception e) {
        }

        return listRemiseSousProvisionPeage;

    }

    public static Commande getCommandeVoucherByID(int id) {
        try {
            String query = "SELECT * FROM T_COMMANDE WITH (READPAST) WHERE ID = ?1";

            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, id);
            return commandes.isEmpty() ? null : commandes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Axe getAxePeageByID(int id) {
        try {
            String query = "SELECT * FROM T_AXE WITH (READPAST) WHERE ID = ?1";

            List<Axe> axes = (List<Axe>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Axe.class), false, id);
            return axes.isEmpty() ? null : axes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static AxePeage getAxeByPeage(String codeSite) {
        try {
            String query = "SELECT * FROM T_AXE_PEAGE WITH (READPAST) WHERE FK_SITE = ?1 AND ETAT = 1";

            List<AxePeage> axePeages = (List<AxePeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AxePeage.class), false, codeSite);
            return axePeages.isEmpty() ? null : axePeages.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean savePaiementCommandeVoucher(String referenceDocument, String referencePaiement, int agentPaiement, String codeSite) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = ":UPDATE T_COMMANDE SET ETAT = 2, DATE_PAIEMENT = GETDATE(),REFERENCE_PAIEMENT = ?1,AGENT_PAIEMENT = ?2, SITE_PAIEMENT = ?3"
                    + "  WHERE (CAST(ID AS VARCHAR(20)) = ?4 OR REFERENCE = ?4)";

            bulkQuery.put(counter + query, new Object[]{referencePaiement, agentPaiement, codeSite, referenceDocument});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static Boolean validerPaiementVoucher(String idCommande, int userId) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_COMMANDE SET ETAT = 1,AGENT_VALIDATION_PAIEMENT = ?1,DATE_VALIDATION_PAIEMENT = GETDATE() WHERE ID = ?2";
            bulkQuery.put(0 + query, new Object[]{userId, idCommande});

            String query2 = ":UPDATE T_VOUCHER SET ETAT = 1 WHERE FK_COMMANDE = ?1";
            bulkQuery.put(1 + query2, new Object[]{idCommande});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean addNewCategorieRemise(String codeTarif) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_TARIF SET EST_TARIF_SOUS_PROVISION = 1 WHERE CODE = ?1";
            bulkQuery.put(0 + query, new Object[]{codeTarif});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static RemiseSousProvisionPeage getRemiseSousProvisionPeage(String codePersonne, String codeTarif, int codeAxe) {
        try {
            String query = "SELECT * FROM T_REMISE_SOUS_PROVISION_PEAGE WITH (READPAST) WHERE FK_PERSONNE = ?1 "
                    + " AND FK_TARIF = ?2 AND FK_AXE = ?3 AND ETAT = 1";

            List<RemiseSousProvisionPeage> axePeages = (List<RemiseSousProvisionPeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RemiseSousProvisionPeage.class), false, codePersonne, codeTarif, codeAxe);
            return axePeages.isEmpty() ? null : axePeages.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TarifSite> getListTarifSitesPeage(int idAxe, String idCat, String devise) {
        try {
            String query = "SELECT * FROM T_TARIF_SITE WITH (READPAST) WHERE "
                    + "FK_SITE_DESTINANTION IN(SELECT FK_SITE FROM T_AXE_PEAGE WITH (READPAST) "
                    + "WHERE FK_AXE = ?1) AND FK_TARIF = ?2 AND FK_DEVISE = ?3 AND ETAT = 1";
            List<TarifSite> tses = (List<TarifSite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TarifSite.class), false, idAxe, idCat, devise);
            return tses;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean saveCommandeV2(Commande commande, Voucher voucher) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            int numberRows = voucher.getQte() / 100;
            double rest = voucher.getQte() % 100;

            String query = ":EXEC F_NEW_COMMANDE_V2 ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14,?15,?16";
            bulkQuery.put(0 + query, new Object[]{
                commande.getFkPersonne().getCode(),
                commande.getMontant(),
                commande.getDevise().getCode(),
                commande.getReference(),
                commande.getFkCompteBancaire().getCode(),
                voucher.getFkSite().getCode(),
                voucher.getFkAxe().getId(),
                voucher.getFkTarif().getCode(),
                voucher.getQte(),
                voucher.getPrixUnitaire(),
                voucher.getRemise(),
                commande.getDatePaiement(),
                voucher.getAgentMaj().getCode(),
                numberRows,
                rest,
                commande.getTypeCmd()
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<CommandeCarte> getCommandeCarteByDate(String dateDebut, String dateFin) {

        String sqlQuery = " SELECT C.* FROM T_COMMANDE_CARTE C WITH (READPAST) "
                + "WHERE  DBO.FDATE(C.DATE_CREATE) BETWEEN '%s' AND '%s'";

        List<CommandeCarte> cmdCarteList = new ArrayList<>();

        try {
            sqlQuery = String.format(sqlQuery, dateDebut, dateFin);

            cmdCarteList = (List<CommandeCarte>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(CommandeCarte.class), false);
        } catch (Exception e) {
        }
        return cmdCarteList;
    }

    public static List<CommandeCarte> getCommandeCarte(String valueSearch, int typeSearch) {

        String secondaryQuery_One = "";
        String secondaryQuery_Two = "";
        String primaryQuery;

        boolean like = false;

        if (typeSearch == 1) {
            secondaryQuery_One = "AND C.FK_PERSONNE  IN (SELECT P.CODE FROM T_PERSONNE P "
                    + " WHERE (P.NOM LIKE '%" + valueSearch.trim() + "%' OR P.POSTNOM LIKE '%" + valueSearch.trim() + "%' OR P.PRENOMS LIKE '%" + valueSearch.trim() + "%')) ";
            like = true;
        } else if (typeSearch == 2) {
//            secondaryQuery_Two = " AND C.REFERENCE = '" + valueSearch.trim() + "'";
//            like = true;
        }
        primaryQuery = " SELECT C.* FROM T_COMMANDE_CARTE C WITH (READPAST) WHERE C.ETAT <> 0 %s %s";
        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two);

        try {
            List<CommandeCarte> cmdCarteList = (List<CommandeCarte>) myDao.getDaoImpl().find(primaryQuery,
                    Casting.getInstance().convertIntoClassType(CommandeCarte.class), like);
            return cmdCarteList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean traiterCmdCodeCarte(List<String> listCodeCmdCarte, String codePersonne, String Observation, int codeCmd, int agentMaj) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;
        int counter = 0;

        try {
            String query2 = ":UPDATE T_COMMANDE_CARTE SET ETAT = 1, DATE_UPDATE = GETDATE(), AGENT_UPDATE = ?1 WHERE ID = ?2";

            String query = ":INSERT INTO T_CARTE_VOUCHER (NUMERO_CARTE,FK_COMMANDE_CARTE,FK_PERSONNE,OBSERVATION,ETAT,DATE_GENERATE) VALUES "
                    + " (?1, ?2, ?3, ?4, 3, GETDATE())";

            for (int i = 0; i < listCodeCmdCarte.size(); i++) {
                bulkQuery.put(counter + query, new Object[]{
                    listCodeCmdCarte.get(i), codeCmd, codePersonne, Observation
                });
                counter++;
            }

            counter++;
            bulkQuery.put(counter + query2, new Object[]{
                agentMaj, codeCmd
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<CarteVoucher> getCarteVoucherByCmde(int codeCmd) {
        try {
            String query = "SELECT C.* FROM T_CARTE_VOUCHER C WITH (READPAST) "
                    + " WHERE C.FK_COMMANDE_CARTE = ?1";
            List<CarteVoucher> carteVoucherList = (List<CarteVoucher>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CarteVoucher.class), false, codeCmd);
            return carteVoucherList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static CarteVoucher getCarteVoucherById(String idCarteVchr) {
        try {
            String query = " SELECT C.* FROM T_CARTE_VOUCHER C WITH (READPAST)  WHERE C.ETAT <> 0 AND  C.id = ?1";

            List<CarteVoucher> cartevchr = (List<CarteVoucher>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CarteVoucher.class), false, idCarteVchr);
            return cartevchr.isEmpty() ? null : cartevchr.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean updateCarteNFC(CarteVoucher carteNfc) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;
        int counter = 0;

        try {
            String query = ":UPDATE T_CARTE_VOUCHER SET ETAT = 2 WHERE ID = ?1";

            counter++;
            bulkQuery.put(counter + query, new Object[]{
                carteNfc.getId()
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean disableCarteNFC(int idCarte, int etat) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;
        int counter = 0;

        try {
            String query = ":UPDATE T_CARTE_VOUCHER SET ETAT = ?1 WHERE ID = ?2";

            counter++;
            bulkQuery.put(counter + query, new Object[]{
                etat,
                idCarte
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<DetailVoucher> getListDetailVoucherUsingByVoucherId(int voucherId) {

        try {
            String sqlQuery = "SELECT * FROM T_DETAIL_VOUCHER dv WITH (READPAST) WHERE dv.FK_VAUCHER = ?1 "
                    + " AND NOT dv.DATE_UTILISATION IS NULL AND dv.FK_VAUCHER IN (SELECT v.ID FROM T_VOUCHER v WITH (READPAST) WHERE v.STATUT = 1) "
                    + " ORDER BY dv.DATE_UTILISATION DESC";
            List<DetailVoucher> listDetailVouchers = (List<DetailVoucher>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(DetailVoucher.class), false, voucherId);
            return listDetailVouchers;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailVoucher geDetailVoucherByTicket(String codeTicket) {
        try {
            String query = "SELECT * FROM T_DETAIL_VOUCHER WITH (READPAST) WHERE CODE_TICKET_GENERATE = ?1";

            List<DetailVoucher> detailVouchers = (List<DetailVoucher>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailVoucher.class), false, codeTicket);
            return detailVouchers.isEmpty() ? null : detailVouchers.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Integer getNumberTicketGenerate(int agent, String codeSite) {
        String queryStr = "SELECT dbo.F_GET_NUMBER_TICKET_GENERATE(" + agent + ",'" + codeSite.trim() + "')";
        Integer result = myDao.getDaoImpl().getSingleResultToInteger(queryStr);
        return result;
    }

    public static Integer getNumberTicketGenerate(int agent, String codeSite, String day) {
        String queryStr = "SELECT dbo.F_GET_NUMBER_TICKET_GENERATE_BY_DAY(" + agent + ",'" + codeSite.trim() + "','" + day.trim() + "')";
        Integer result = myDao.getDaoImpl().getSingleResultToInteger(queryStr);
        return result;
    }

    public static BigDecimal getSumTicketGenerate(int agent, String codeSite, String devise) {
        String queryStr = "SELECT dbo.F_GET_SUM_TICKET_GENERATE(" + agent + ",'" + codeSite.trim() + "','" + devise + "')";
        BigDecimal result = myDao.getDaoImpl().getSingleResultToBigDecimal2(queryStr);
        return result;
    }

    public static BigDecimal getSumTicketGenerate(int agent, String codeSite, String day, String devise) {
        String queryStr = "SELECT dbo.F_GET_SUM_TICKET_GENERATE_BY_DAY(" + agent + ",'" + codeSite.trim() + "','" + devise + "','" + day.trim() + "')";
        BigDecimal result = myDao.getDaoImpl().getSingleResultToBigDecimal2(queryStr);
        return result;
    }

    public static Integer getNumberVoucherUseByCommande(int cmdID) {
        String queryStr = "SELECT dbo.F_GET_NUMBER_VOUCHER_USE(" + cmdID + ")";
        Integer result = myDao.getDaoImpl().getSingleResultToInteger(queryStr);
        return result;
    }

}
