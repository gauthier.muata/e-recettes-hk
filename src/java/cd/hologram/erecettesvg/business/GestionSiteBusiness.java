/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.TaxationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.Division;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.SiteBanque;
import cd.hologram.erecettesvg.models.UtilisateurDivision;
import cd.hologram.erecettesvg.sql.SQLQueryGeneral;
import cd.hologram.erecettesvg.util.Casting;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author gauthier.muata
 */
public class GestionSiteBusiness {

    private static final Dao myDao = new Dao();

    public static List<Site> getSiteList(boolean isPeage) {
        try {
            String query = "";

            if (isPeage) {
                query = "select s.* from t_site s with (readpast) where s.etat = 1  and s.code <> '*' AND PEAGE = 1 ORDER BY s.INTITULE";
            } else {
                query = "select s.* from t_site s with (readpast) where s.etat = 1  and s.code <> '*' AND PEAGE = 0 ORDER BY s.INTITULE";
            }

            List<Site> sites = (List<Site>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Site.class), false);
            return sites;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean editionSite(
            String code,
            String intitule,
            String responsable,
            String numero,
            String codeAvenue,
            String codeQuartier,
            String codeCommune,
            String codeDistrict,
            String codeVille,
            String codeProvince,
            String idAdresse,
            boolean typeSite,
            String codeDivision,
            String sigle,
            int typeEntite) {

        boolean result = false;

        String sqlQuery = GeneralConst.EMPTY_STRING;

        EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(codeCommune);

        if (code.isEmpty()) {

            sqlQuery = "EXEC F_NEW_SITES ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14";
            result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                    intitule.trim(),
                    responsable.trim(),
                    numero.trim(),
                    codeAvenue.trim(),
                    codeQuartier.trim(),
                    codeCommune.trim(),
                    ea.getEntiteMere().getCode(),
                    codeVille.trim(),
                    ea.getEntiteMere().getEntiteMere().getCode(),
                    typeSite,
                    null,
                    codeDivision,
                    sigle,
                    typeEntite);

        } else {

            sqlQuery = "EXEC F_UPDATE_SITE ?1,?2,?3,?4,?5,?6,?7,?8,?9";
            result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                    code,
                    intitule.trim(),
                    idAdresse.trim(),
                    responsable.trim(),
                    typeSite,
                    null,
                    codeDivision,
                    sigle,
                    typeEntite);
        }

        return result;
    }

    public static List<Personne> getPersonneResponsables(String value) {
        try {
            String query = "select * from t_personne p with (readpast)\n"
                    + " where code in (select a.personne from t_agent a with (readpast) where a.etat = 1)\n"
                    + " and p.etat = 1 and (p.nom like ?1 or p.postnom like ?1 or p.prenoms like ?1) order by p.nom,p.postnom";
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), true, value.trim());
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean updateAffactationBankSite(int id, int status) {

        boolean result = false;

        String sqlQuery = GeneralConst.EMPTY_STRING;

        sqlQuery = "UPDATE t_site_banque set ETAT = %s WHERE id = ?1";
        sqlQuery = String.format(sqlQuery, status);
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery, id);

        return result;
    }

    public static boolean addBankToSite(String site, String banque) {

        boolean result = false;

        String sqlQuery = GeneralConst.EMPTY_STRING;

        sqlQuery = "Insert into t_site_banque (site,banque,etat) values (?1,?2,?3)";
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery, site.trim(), banque.trim(), 1);

        return result;
    }

    public static SiteBanque getSiteBanqueBySiteAndBanque(String site, String banque) {
        try {

            String query = "select * from t_site_banque sb with (readpast) where sb.site = ?1 and sb.banque = ?2";

            List<SiteBanque> siteBanques = (List<SiteBanque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(SiteBanque.class), false, site.trim(), banque.trim());

            return siteBanques.isEmpty() ? null : siteBanques.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean saveDivision(Division division, String checkUpdate) {
        boolean result;

        String sqlQuery;
        if (division.getCode().equals("")) {
            //Nouveau
            sqlQuery = "EXEC F_NEW_DIVISION ?1,?2,?3,?4";
            result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                    division.getEntiteAdministrative().getCode().trim(), division.getSigle(), division.getIntitule().trim(), division.getEtat());
        } else {
            if (checkUpdate.equals("1")) {
                //Modification
                sqlQuery = "UPDATE T_DIVISION SET FK_ENTITE = ?1, INTITULE = ?2, SIGLE = ?3  WHERE CODE = ?4";
                result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                        division.getEntiteAdministrative().getCode().trim(), division.getIntitule().trim(), division.getSigle(), division.getCode().trim());
            } else {
                //Supression
                sqlQuery = "UPDATE T_DIVISION SET ETAT = 0 WHERE CODE = ?1";
                result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery, division.getCode().trim());
            }
        }

        return result;
    }

    public static List<Division> loadDivision(String codeEntite) {
        try {

            String secondQuery = GeneralConst.EMPTY_STRING;

            if (!codeEntite.equals(GeneralConst.EMPTY_STRING)) {
                secondQuery = " AND FK_ENTITE = '" + codeEntite + "'";
            }
            String query = "SELECT * FROM T_DIVISION WITH (READPAST) WHERE ETAT = 1 %s ORDER BY INTITULE";
            query = String.format(query, secondQuery);

            List<Division> divisionList = (List<Division>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Division.class), false);
            return divisionList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Division> getDivision(boolean isPeage) {

        try {

            String query = GeneralConst.EMPTY_STRING;

            if (isPeage) {
                query = "SELECT * FROM T_DIVISION WITH (READPAST) WHERE ETAT = 1 AND CODE = 'DIV00000001' ORDER BY INTITULE";
            } else {
                query = "SELECT * FROM T_DIVISION WITH (READPAST) WHERE ETAT = 1 AND CODE <> 'DIV00000001' ORDER BY INTITULE";
            }

            List<Division> divisionList = (List<Division>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Division.class), false);
            return divisionList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Site> getBureauByDivision(String codedivision) {

        try {

            String query = "SELECT * FROM T_SITE WITH (READPAST) WHERE ETAT = 1 AND FK_DIVISION = ?1 ORDER BY INTITULE";

            List<Site> bureauList = (List<Site>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Site.class), false, codedivision.trim());
            return bureauList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static UtilisateurDivision getBureauByUser(String codeBureau, String codeDivision, String codeUser) {
        try {

            String query = "SELECT * FROM T_UTILISATEUR_DIVISION WITH (READPAST) WHERE FK_SITE = ?1 AND FK_DIVISION = ?2 AND FK_PERSONNE = ?3  AND ETAT = 1";

            List<UtilisateurDivision> utilisateurDivision = (List<UtilisateurDivision>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(UtilisateurDivision.class), false, codeBureau.trim(), codeDivision.trim(), codeUser.trim());

            return utilisateurDivision.isEmpty() ? null : utilisateurDivision.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean affecterUserDivision(List<UtilisateurDivision> userDivision) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            for (UtilisateurDivision userDivis : userDivision) {
                if (userDivis.getEtat() == 1) {
                    bulkQuery.put(counter + SQLQueryGeneral.F_NEW_UTILISATEUR_DIVISION, new Object[]{
                        userDivis.getSite().getCode(),
                        userDivis.getDivision().getCode(),
                        userDivis.getFkPersonne(),
                        userDivis.getEtat()
                    });
                    counter++;
                } else if (userDivis.getEtat() == 0) {
                    bulkQuery.put(counter + SQLQueryGeneral.DESACTIVER_UTILISATEUR_DIVISION, new Object[]{
                        userDivis.getCode()
                    });
                    counter++;
                }

            }
            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }
}
