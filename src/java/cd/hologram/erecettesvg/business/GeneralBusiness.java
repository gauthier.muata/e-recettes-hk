/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Banque;
import cd.hologram.erecettesvg.models.Bordereau;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.Fonction;
import cd.hologram.erecettesvg.models.JourFerie;
import cd.hologram.erecettesvg.models.ParamGlobal;
import cd.hologram.erecettesvg.models.Penalite;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.SiteBanque;
import cd.hologram.erecettesvg.models.Taux;
import cd.hologram.erecettesvg.models.TypeDocument;
import cd.hologram.erecettesvg.sql.SQLQueryGeneral;
import cd.hologram.erecettesvg.sql.SQLQueryPaiement;
import cd.hologram.erecettesvg.util.Casting;
import cd.hologram.erecettesvg.util.CustumException;
import java.util.HashMap;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author moussa.toure
 */
public class GeneralBusiness {

    private static final Dao myDao = new Dao();

    public static List<SiteBanque> getListBanqueBySite(String codeSite) {
        try {

            String query = SQLQueryGeneral.LIST_SITES_BANQUE;
            List<SiteBanque> sitesUser = (List<SiteBanque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(SiteBanque.class), false, codeSite);
            return sitesUser;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Site> getSite(boolean isPeage) {

        try {

            String query = GeneralConst.EMPTY_STRING;

            if (isPeage) {
                query = "SELECT * FROM T_SITE WITH (READPAST) WHERE ETAT = 1 AND CODE <> '*' AND PEAGE = 1 Order by intitule";
            } else {
                query = "SELECT * FROM T_SITE WITH (READPAST) WHERE ETAT = 1 AND CODE <> '*' AND PEAGE = 0 Order by intitule";
            }
          
            List<Site> sites = (List<Site>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Site.class), false);
            return sites;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Fonction> getFonction() {

        try {
            String query = SQLQueryGeneral.LIST_FONCTION;
            List<Fonction> fonctions = (List<Fonction>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Fonction.class), false);
            return fonctions;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Bordereau getBordereauByDeclaration(String codeDeclaration) {
        try {
            String query = SQLQueryGeneral.GET_BORDEREAU_BY_DECLARATION;
            List<Bordereau> bordereaus = (List<Bordereau>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Bordereau.class), false, codeDeclaration);
            return bordereaus.isEmpty() ? null : bordereaus.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static SiteBanque getSiteBanqueBySite(String codeSite) {
        try {
            String query = SQLQueryGeneral.GET_SITE_BANQUE;
            List<SiteBanque> siteBanques = (List<SiteBanque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(SiteBanque.class), false, codeSite);
            return siteBanques.isEmpty() ? null : siteBanques.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Service> getServiceAssiete() {

        try {
            String query = SQLQueryGeneral.LIST_SERVICES;
            List<Service> services = (List<Service>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Service.class), false);
            return services;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Taux> getTaux() {

        try {
            String query = SQLQueryGeneral.SELECT_TAUX;
            List<Taux> taux = (List<Taux>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Taux.class), false);
            return taux;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Banque getBanqueByCode(String codeBanque) {
        try {
            String query = SQLQueryGeneral.GET_BANQUE;
            List<Banque> banques = (List<Banque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Banque.class), false, codeBanque);
            return banques.isEmpty() ? null : banques.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static CompteBancaire getCompteBancaireByCode(String codeCompteBancaire) {
        try {
            String query = "SELECT * FROM T_COMPTE_BANCAIRE WITH (READPAST) WHERE CODE = ?1 AND ETAT = 1";
            List<CompteBancaire> compteBancaires = (List<CompteBancaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CompteBancaire.class), false, codeCompteBancaire);
            return compteBancaires.isEmpty() ? null : compteBancaires.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Site getSiteByCode(String codeSite) {
        try {
            String query = SQLQueryGeneral.GET_SITE;
            List<Site> sites = (List<Site>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Site.class), false, codeSite);
            return sites.isEmpty() ? null : sites.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Personne getRequerantByCode(int code) {
        try {

            String query = SQLQueryPaiement.SELECT_AGENT_BY_ID;

            List<Personne> requerant = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, code);
            return requerant.isEmpty() ? null : requerant.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static String getLetterOfNumber(int number) {
        String queryStr = String.format(SQLQueryGeneral.LETTER_OF_NUMBER, number);
        String letter = (String) myDao.getDaoImpl().getSingleResult(queryStr);
        return letter;
    }

    public static List<EntiteAdministrative> getAllEntiteAdministrative() {
        try {

            String query = SQLQueryGeneral.SELECT_ENTITES_ADMINISTRATIVE;
            List<EntiteAdministrative> entitiesAdmin = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false);
            return entitiesAdmin;
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean saveEntity(EntiteAdministrative entity, String idUser) {

        boolean result;

        try {

            result = myDao.getDaoImpl().executeNativeQuery(SQLQueryGeneral.EXEC_F_NEW_EA,
                    entity.getIntitule(), entity.getTypeEntite().getCode(),
                    entity.getEntiteMere().getCode(), idUser);

            return result;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<TypeDocument> getListTypeDocumentUploading() {
        try {

            String query = SQLQueryGeneral.SELECT_LIST_TYPE_DOCUMENT_UPLOADING;
            List<TypeDocument> typeDocuments = (List<TypeDocument>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeDocument.class), false);
            return typeDocuments;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<JourFerie> getDaysOff() {
        try {

            String query = SQLQueryGeneral.SELECT_FERIE;
            List<JourFerie> daysOff = (List<JourFerie>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(JourFerie.class), false);
            return daysOff;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Agent getAgentByCode(String codeAgent) {
        try {
            String query = SQLQueryGeneral.GET_AGENT;
            List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, codeAgent);
            return agent.isEmpty() ? null : agent.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Agent getAgentByPersonne(String codePersonne) {
        try {
            String query = SQLQueryGeneral.GET_AGENT_BY_PERSONNE;
            List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, codePersonne);
            return agent.isEmpty() ? null : agent.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static TypeDocument getTypeDocumentByCode(String codeTypeDocument) {
        try {
            String query = SQLQueryGeneral.SELECT_TYPE_DOCUMENT_BY_CODE;
            List<TypeDocument> typeDocuments = (List<TypeDocument>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeDocument.class), false, codeTypeDocument);
            return typeDocuments.isEmpty() ? null : typeDocuments.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static TypeDocument getTypeDocumentByCode_V2(String codeTypeDocument) {
        try {
            String query = SQLQueryGeneral.SELECT_TYPE_DOCUMENT_BY_CODE_V2;
            List<TypeDocument> typeDocuments = (List<TypeDocument>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeDocument.class), false, codeTypeDocument);
            return typeDocuments.isEmpty() ? null : typeDocuments.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static EntiteAdministrative getEntiteAdministrativeByCodeV2(String code) {
        try {
            String query = SQLQueryGeneral.SELECT_ENTITES_BY_CODE;
            List<EntiteAdministrative> entiteAdministratives = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, code);
            return entiteAdministratives.isEmpty() ? null : entiteAdministratives.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Penalite> getPenalite() {

        try {
            String query = SQLQueryGeneral.LISTE_PENALITE;
            List<Penalite> penalites = (List<Penalite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Penalite.class), false);
            return penalites;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<EntiteAdministrative> getEntiteAdministrativeByMere(String entiteMere) {
        try {

            String query = SQLQueryGeneral.SELECT_ENTITES_BY_MERE;
            List<EntiteAdministrative> entitiesAdmin = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, entiteMere.trim());
            return entitiesAdmin;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<EntiteAdministrative> getEntiteAdministrativeByMereV2(String entiteMere, int typeEntite) {
        try {

            String query = GeneralConst.EMPTY_STRING;

            switch (typeEntite) {
                case 6:
                    query = "select * from T_ENTITE_ADMINISTRATIVE WITH (READPAST) WHERE  TYPE_ENTITE = 4 and ENTITE_MERE = '00000000000427472016' AND ETAT = 1 order by INTITULE";
                    break;
                case 5:
                    query = "select * from T_ENTITE_ADMINISTRATIVE WITH (READPAST) WHERE TYPE_ENTITE = 3 AND ENTITE_MERE = ?1 AND ETAT = 1";
                    break;
                case 4:
                    query = "select * from T_ENTITE_ADMINISTRATIVE WITH (READPAST) WHERE TYPE_ENTITE = 5 AND ENTITE_MERE = ?1 AND ETAT = 1";
                    break;
                case 3:
                case 2:
                    //case 5:
                    query = "select * from T_ENTITE_ADMINISTRATIVE WITH (READPAST) where  ENTITE_MERE = ?1 AND ETAT = 1 order by INTITULE";
                    break;
            }

            List<EntiteAdministrative> entitiesAdmin = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, entiteMere.trim(), typeEntite);
            return entitiesAdmin;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<EntiteAdministrative> getEntiteAdministrativeByCode(String codeEntite) {
        try {

            String query = SQLQueryGeneral.SELECT_ENTITES_BY_CODE;
            List<EntiteAdministrative> entitiesAdmin = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, codeEntite.trim());
            return entitiesAdmin;
        } catch (Exception e) {
            throw e;
        }
    }

    public static String saveEntitys(EntiteAdministrative entity, String idUser) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = IdentificationConst.ParamQUERY.UTILISATEUR;
        try {
            String query = SQLQueryGeneral.EXEC_F_NEW_EA_V2;

            params.put(GeneralConst.ParamName.INTITULE, entity.getIntitule().trim());
            params.put(GeneralConst.ParamName.TYPE_ENTITE, entity.getTypeEntite().getCode());
            params.put(GeneralConst.ParamName.MERE_ENTITY, entity.getEntiteMere().getCode().trim());
            params.put(GeneralConst.ParamName.AGENT_CREAT, idUser);

            String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

            if (!result.isEmpty()) {
                returnValue = result;
            }
        } catch (Exception e) {
            throw (e);
        }
        return returnValue;
    }

    public static boolean updateNumeroDoc(
            String query) {

        boolean result;

        result = myDao.getDaoImpl().executeStoredProcedure(query);
        return result;
    }

    public static int getLastIncrement(String req) {
        String request = req;
        return myDao.getDaoImpl().getSingleResultToInteger(request);
    }
    
     public static Long getLastNumberOfCode() {
        
        try {
            String queryStr = String.format("SELECT dbo.F_GET_LAST_INCREMENT_CARTE_VOUVCHER()");
        Long numberCode = (Long) myDao.getDaoImpl().getSingleResult(queryStr);
        return numberCode;
        } catch (Exception e) {
            throw (e);
        }
        
    }
     
     public static ParamGlobal getParamGlobalByVerifyPaymentBankVignette(String paramValue) {
        try {
            String query = "select * from T_PARAM_GLOBAL WITH (READPAST) where INTITULE = ?1";
            List<ParamGlobal> bordereaus = (List<ParamGlobal>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ParamGlobal.class), false, paramValue);
            return bordereaus.isEmpty() ? null : bordereaus.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

}
