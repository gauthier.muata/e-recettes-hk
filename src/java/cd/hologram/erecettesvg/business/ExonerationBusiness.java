/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.Exoneration;
import cd.hologram.erecettesvg.util.Casting;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Administrateur
 */
public class ExonerationBusiness {

    private static final Dao myDao = new Dao();

    public static boolean executeQueryBulkInsert(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }

    public static boolean saveExoneration(Exoneration exon) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":EXEC F_NEW_EXONERATION ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12", new Object[]{
                exon.getFkPersonne().getCode(),
                exon.getFkArticleBudgetaire().getCode(),
                exon.getFkPeriodeDeclaration().getId(),
                exon.getFkSite().getCode(),
                exon.getFkBien().getId(),
                exon.getAgentCreat().getCode(),
                null, //exon.getAgentMaj(),
                null, //exon.getDateMaj(),
                exon.getObservation(),
                exon.getMontantExonere(),
                exon.getDevise().getCode(),
                exon.getArchive()
            });
            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<Exoneration> getBiensExoneres(String fkPersonne, String fkBien, String fkSite, String fkAgent) {
        try {

            String query = "SELECT * FROM T_EXONERATION WITH (READPAST) WHERE FK_PERSONNE = ?1 AND "
                    + "FK_BIEN = ?2 AND FK_SITE = ?3 AND AGENT_CREAT = ?4 AND ETAT = 1";
            List<Exoneration> exonerations = (List<Exoneration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Exoneration.class), false, fkPersonne, fkBien, fkSite, fkAgent);
            return exonerations;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Exoneration> getListExonation(
            String fkSite,
            String fkAgent,
            String fkAB,
            String dateDebut,
            String dateFin,
            String assujetti,
            String typeRecherche
    ) {

        String query;
        boolean like = false;

        try {

            if (typeRecherche.equals(GeneralConst.Number.ZERO)) {
                query = "SELECT * FROM T_EXONERATION WITH (READPAST) WHERE FK_SITE = ?1 AND AGENT_CREAT = ?2 "
                        + "AND CAST(DATE_CREATION AS date) >= ?3 AND CAST(DATE_CREATION AS date) <= ?4 AND ETAT = 1 "
                        + "%s ORDER BY FK_PERSONNE";
                if (!fkAB.equals("*")) {
                    query = String.format(query, "AND FK_ARTICLE_BUDGETAIRE = ?5");
                } else {
                    query = String.format(query, "");
                }

            } else {
                like = true;
                query = "SELECT * FROM T_EXONERATION WITH (READPAST) WHERE FK_PERSONNE IN ("
                        + "SELECT CODE FROM T_PERSONNE WHERE NOM LIKE ?6) AND ETAT = 1 ORDER BY FK_PERSONNE";
            }

            List<Exoneration> exonerations = (List<Exoneration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Exoneration.class), like, fkSite, fkAgent,
                    dateDebut, dateFin, fkAB, assujetti);
            return exonerations;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean annulerExoneration(String code, int agent, String observation) {

        String query = "EXEC F_UPDATE_EXONERATION ?1,?2,?3";

        try {
            return myDao.getDaoImpl().executeStoredProcedure(query, code, agent, observation);
        } catch (Exception e) {
            throw e;
        }
    }

}
