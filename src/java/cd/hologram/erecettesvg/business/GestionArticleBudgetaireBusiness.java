/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.TaxationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.GestionArticleBudgetaireConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.ArticleGenerique;
import cd.hologram.erecettesvg.models.AssignationBudgetaire;
import cd.hologram.erecettesvg.models.Banque;
import cd.hologram.erecettesvg.models.BanqueAb;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.DetailsAssignationBudgetaire;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.DocumentOfficiel;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.ExerciceFiscale;
import cd.hologram.erecettesvg.models.FormeJuridique;
import cd.hologram.erecettesvg.models.NatureArticleBudgetaire;
import cd.hologram.erecettesvg.models.Palier;
import cd.hologram.erecettesvg.models.Periodicite;
import cd.hologram.erecettesvg.models.ProrogationEcheanceLegaleDeclaration;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.models.TarifSite;
import cd.hologram.erecettesvg.models.TauxIfExerciceAnterieur;
import cd.hologram.erecettesvg.models.TauxVignette;
import cd.hologram.erecettesvg.models.TypeBien;
import cd.hologram.erecettesvg.models.Unite;
import cd.hologram.erecettesvg.sql.SQLQueryGestionArticleBudgetaire;
import cd.hologram.erecettesvg.sql.SQLQueryCleRepartition;
import cd.hologram.erecettesvg.util.Casting;
import cd.hologram.erecettesvg.util.ConvertDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class GestionArticleBudgetaireBusiness {

    private static final Dao myDao = new Dao();

    public static List<ExerciceFiscale> getListExerciceFiscal() {

        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_EXERCICE_FISCAL;
            List<ExerciceFiscale> efs = (List<ExerciceFiscale>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ExerciceFiscale.class), false);
            return efs;

        } catch (Exception e) {
            throw e;
        }

    }

    public static Agent getAgentByCode(String code) {

        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_AGENT;
            List<Agent> efs = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, code);
            return efs.get(0);

        } catch (Exception e) {
            throw e;
        }

    }

    public static boolean saveExerciceFiscal(ExerciceFiscale ef, String dateDebut, String dateFin) {
        boolean result;
        String sqlQuery = SQLQueryGestionArticleBudgetaire.EXEC_P_NEW_EXERCICE_FISCALE;
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                ef.getCode(),
                ef.getIntitule(),
                dateDebut,
                dateFin,
                ef.getAgentCreat()
        );
        return result;
    }

    public static boolean saveFaitGenerateur(ArticleGenerique at) {
        boolean result;

        String sqlQuery;
        if (at.getCode().equals("")) {
            //Nouveau
            sqlQuery = SQLQueryGestionArticleBudgetaire.EXEC_P_NEW_ARTICLE_GENERIQUE;
            result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                    at.getIntitule(), at.getServiceAssiette().getCode(), at.getFormeJuridique().getCode(), "");
        } else {
            if (at.getEtat()) {
                //Modification
                sqlQuery = SQLQueryGestionArticleBudgetaire.EXEC_UPDATE_ARTICLE_GENERIQUE;
                result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                        at.getIntitule(), at.getServiceAssiette().getCode(), at.getFormeJuridique().getCode(), at.getCode());
            } else {
                //Supression
                sqlQuery = SQLQueryGestionArticleBudgetaire.EXEC_DELETE_ARTICLE_GENERIQUE;
                result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery, at.getCode());
            }
        }

        return result;
    }

    public static List<FormeJuridique> getFormeJuridiques() {
        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_FORMES_JURIDIQUES;
            List<FormeJuridique> formeJuridiques = (List<FormeJuridique>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(FormeJuridique.class), false);
            return formeJuridiques;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Periodicite> getPeriodicite() {
        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_PERIODICITE;
            List<Periodicite> formperiodicite = (List<Periodicite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(FormeJuridique.class), false);
            return formperiodicite;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<ArticleGenerique> getArticlegeneriquee() {
        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_ARTICLE_GENERIQUE;
            List<ArticleGenerique> articlegenerique = (List<ArticleGenerique>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(FormeJuridique.class), false);
            return articlegenerique;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean deleteExerciceFiscal(String code) {
        boolean result;
        String sqlQuery = SQLQueryGestionArticleBudgetaire.DELETE_EXERCICE_FISCALE;
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery, code);
        return result;
    }

    public static boolean updateExerciceFiscal(ExerciceFiscale ef, String debut, String dateFin) {

        boolean result;

        String sqlQuery = SQLQueryGestionArticleBudgetaire.UPDATE_EXERCICE_FISCALE;
        try {
            result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                    ef.getIntitule(),
                    debut,
                    dateFin,
                    ef.getAgentMaj(),
                    ef.getCode());
            return result;
        } catch (Exception e) {
            throw e;
        }

    }

    public static List<Service> loadMinistere(String indice) {

        try {

            if (indice.equals("1")) {
                String query = SQLQueryGestionArticleBudgetaire.SELECT_ALL_MINISTERE;
                List<Service> services = (List<Service>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Service.class), false);
                return services;

            } else if (indice.equals("2")) {
                String query = SQLQueryGestionArticleBudgetaire.SELECT_ALL_SERVICES_MINISTERE;
                List<Service> services = (List<Service>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Service.class), false);
                return services;

            } else {
                String query = SQLQueryGestionArticleBudgetaire.SELECT_ALL_MINISTERE_V2;
                List<Service> services = (List<Service>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Service.class), false, indice);
                return services;
            }

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<ArticleBudgetaire> ArticleBugdetaireRepartiByMinistere(String codeministere) {

        try {

            String query = SQLQueryCleRepartition.SELECT_ARTICLE_BUDGETAIRE_REPARTITION_BY_CODE;
            List<ArticleBudgetaire> articleclerep = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, codeministere);
            return articleclerep;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<Service> loadServices(String codeMinistere, String indice) {

        try {
            String query;
            List<Service> services;

            if (indice.equals("1")) {
                query = SQLQueryGestionArticleBudgetaire.SELECT_SERVICES_MINISTERE;
                services = (List<Service>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Service.class), false, codeMinistere);
            } else {

                query = SQLQueryGestionArticleBudgetaire.SELECT_ALL_SERVICES;
                services = (List<Service>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Service.class), false);
            }

            return services;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<Service> loadServicesAssiettes(String code) {

        try {
            String query;
            List<Service> services;
            query = SQLQueryGestionArticleBudgetaire.SELECT_ALL_SERVICES_ASSIETTES;
            services = (List<Service>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Service.class), false, code);
            return services;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<ArticleGenerique> loadFaitGenerateurByService(String codeService) {

        try {
            String secondQuery = GeneralConst.EMPTY_STRING;

            if (!codeService.equals(GeneralConst.EMPTY_STRING)) {
                secondQuery = " AND SERVICE_ASSIETTE = '" + codeService + "'";
            }
            String query = SQLQueryGestionArticleBudgetaire.SELECT_FAITS_GENERATEURS_BY_SERVICE;
            query = String.format(query, secondQuery);

            List<ArticleGenerique> faitGenerateurList = (List<ArticleGenerique>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleGenerique.class), false, codeService);
            return faitGenerateurList;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<ArticleBudgetaire> loadArticleBudgetaire(String codearticle) {

        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_ARTICLE_BUGDETAIRE;
            List<ArticleBudgetaire> faitGenerateurList = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, codearticle);
            return faitGenerateurList;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<ArticleBudgetaire> loadArticleBudgetaireByGenerique(String codeAG) {

        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_ARTICLE_BUGDETAIRE_GENERIQUE;
            List<ArticleBudgetaire> faitGenerateurList = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, codeAG);
            return faitGenerateurList;

        } catch (Exception e) {
            throw e;
        }

    }

    public static ArticleBudgetaire getArticleBudgetaireByAG(String code) {

        try {

            String query = SQLQueryGestionArticleBudgetaire.SELECT_ARTICLE_BUGDETAIRE_GENERIQUE_ByCode;

            List<ArticleBudgetaire> ab = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, code);
            return ab.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static AssignationBudgetaire getAssignationByCode(int code) {
        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_ASSIGNATION;
            List<AssignationBudgetaire> asb = (List<AssignationBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AssignationBudgetaire.class), false, code);
            return asb.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static Float getRealisation(String service, String ab, String exercice, String op) {
        try {
            Float response = Float.valueOf(GeneralConst.Number.ZERO);;

            String query = SQLQueryGestionArticleBudgetaire.GET_DEVISE_FUNCTION;
            query = String.format(query, service, ab, exercice, op);
            response = myDao.getDaoImpl().getSingleResultToBigDecimal(query);
            return response;

        } catch (Exception e) {
            throw e;
        }
    }

    public static Float getAssignation(String service, String ab, String exercice, String op) {
        try {
            Float response = Float.valueOf(GeneralConst.Number.ZERO);;

            String query = SQLQueryGestionArticleBudgetaire.GET_GET_TOTAL_MONT_ASSIGNATION_AB;
            query = String.format(query, service, ab, exercice, op);
            response = myDao.getDaoImpl().getSingleResultToBigDecimal(query);
            return response;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<DetailsAssignationBudgetaire> loadArticleBudgetaireByExercice(
            String codeAG, String exerciceFiscal, String Secteur) {

        String query = SQLQueryGestionArticleBudgetaire.SELECT_DETAIL_ASSIGNATION_EXERCICE_SERVICE;
        String secondQuery = GeneralConst.EMPTY_STRING;

        try {
            if (codeAG.equals(GeneralConst.Number.ZERO)) {
                secondQuery = SQLQueryGestionArticleBudgetaire.SELECT_DETAIL_ASSIGNATION_EXERCICE_SERVICE_FGEN;
                secondQuery = String.format(secondQuery, codeAG);
            }

            query = query + secondQuery;

            List<DetailsAssignationBudgetaire> detailAssignation = (List<DetailsAssignationBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsAssignationBudgetaire.class), false, Secteur, exerciceFiscal);
            return detailAssignation;

        } catch (Exception e) {
            throw e;
        }

    }

    public static AssignationBudgetaire loadAssignationByExercice(String exerciceFiscal) {

        String query = SQLQueryGestionArticleBudgetaire.LOAD_ASSIGNATION_BY_EXERCICE;

        try {

            List<AssignationBudgetaire> assignationList = (List<AssignationBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AssignationBudgetaire.class), false, exerciceFiscal);
            return assignationList.isEmpty() ? null : assignationList.get(0);

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<DetailsAssignationBudgetaire> loadAssignationByExercice(String service, String exerciceFiscal) {

        String query = SQLQueryGestionArticleBudgetaire.SELECT_DETAIL_ASSIGNATION_EXERCICE_SERVICE;
        String secondQuery = GeneralConst.EMPTY_STRING;

        try {
            query = query + secondQuery;

            List<DetailsAssignationBudgetaire> detailAssignation = (List<DetailsAssignationBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsAssignationBudgetaire.class), false, service, exerciceFiscal);
            return detailAssignation;

        } catch (Exception e) {
            throw e;
        }

    }

    public static boolean saveService(String intitule, String fkService, String agent) {

        boolean result;
        String query = SQLQueryGestionArticleBudgetaire.EXEC_F_NEW_SERVICES;

        try {
            result = myDao.getDaoImpl().executeStoredProcedure(query, intitule, fkService, agent);
            return result;

        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean updateService(String intitule, String code, String fkService) {

        boolean result;
        String query = SQLQueryGestionArticleBudgetaire.UPDATE_SERVICE;

        try {
            result = myDao.getDaoImpl().executeStoredProcedure(query, intitule, code);
            return result;

        } catch (Exception e) {
            throw e;
        }

    }

    public static boolean activeAndDeactiveService(int etat, String code) {

        boolean result;
        String query = SQLQueryGestionArticleBudgetaire.ACTIVE_DEACTIVE_SERVICE;

        try {
            result = myDao.getDaoImpl().executeStoredProcedure(query, etat, code);
            return result;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<DocumentOfficiel> loadDocumentOfficiel() {

        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_DOCUMENT_OFFICIEL;
            List<DocumentOfficiel> dos = (List<DocumentOfficiel>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DocumentOfficiel.class), false);
            return dos;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<NatureArticleBudgetaire> loadNatureArticleBudgetaire() {

        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_NATURE_ARTICLE_BUDGETAIRE;
            List<NatureArticleBudgetaire> nabs = (List<NatureArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NatureArticleBudgetaire.class), false);
            return nabs;

        } catch (Exception e) {
            throw e;
        }

    }

    public static Boolean createDocumentOfficiel(DocumentOfficiel document) {

        String returnValue = GestionArticleBudgetaireConst.ParamQUERY.CODE;
        Boolean result = false;

        Date dateArrete = new Date();

        dateArrete = ConvertDate.formatDateV2(document.getDateArrete());
        try {
            String query = SQLQueryGestionArticleBudgetaire.F_CREATE_DOCUMENT_OFFICIEL;

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    document.getIntitule().trim(),
                    dateArrete,
                    document.getNumero().trim());

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean updateDocumentOfficiel(DocumentOfficiel document) {

        boolean result = false;

        Date dateArrete = new Date();

        dateArrete = ConvertDate.formatDateV2(document.getDateArrete());

        String query = SQLQueryGestionArticleBudgetaire.UPDATE_DOCUMENT_OFFICIEL;
        result = myDao.getDaoImpl().executeStoredProcedure(
                query,
                document.getIntitule().trim(),
                dateArrete,
                document.getNumero().trim(),
                document.getCode().trim());

        return result;
    }

    public static List<DocumentOfficiel> getDocumentOfficielByIntitule(String intitule) {
        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_DOCUMENT_OFFICIEL_BY_INTITULE;
            List<DocumentOfficiel> documents = (List<DocumentOfficiel>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DocumentOfficiel.class), true, intitule);
            return documents;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DocumentOfficiel getDocumentOfficielByNumeroArrete(String numero) {
        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_DOCUMENT_OFFICIEL_BY_NUMERE_ARRETE;

            List<DocumentOfficiel> documents = (List<DocumentOfficiel>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DocumentOfficiel.class), false, numero);
            return documents.isEmpty() ? null : documents.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<ArticleGenerique> getListArticleGenerique(String valueSearch) {
        String sqlQuerySecondary = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;

        sqlQuerySecondary = SQLQueryGestionArticleBudgetaire.SQL_QUERY_LIST_ARTICLE_GENERIQUE_WITH_LIKE;
        String sqlQueryMaster = SQLQueryGestionArticleBudgetaire.SELECT_MASTER_LIST_GESTIONNAIRE_ARTICLE_GENERIQUE;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlQuerySecondary);

        List<ArticleGenerique> listArticleGenerique = (List<ArticleGenerique>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(ArticleGenerique.class), true, valueSearch.trim());
        return listArticleGenerique;
    }

    public static List<DocumentOfficiel> getListDocumentOfficiel(String valueSearch) {

        String sqlQueryMaster = SQLQueryGestionArticleBudgetaire.SELECT_DOCUMENT_OFFICIEL_BY_INTITULE;

        List<DocumentOfficiel> listDocumentOfficiel = (List<DocumentOfficiel>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(DocumentOfficiel.class), true, valueSearch.trim());
        return listDocumentOfficiel;
    }

    public static List<Periodicite> getPeriodiciteList() {
        try {
            String query = SQLQueryGestionArticleBudgetaire.GET_PERIODICITE;
            List<Periodicite> periodiciteList = (List<Periodicite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Periodicite.class), false);
            return periodiciteList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<FormeJuridique> getFormeJuridiqueList() {
        try {
            String query = SQLQueryGestionArticleBudgetaire.GET_FORME_JURIDIQUE;
            List<FormeJuridique> formeJuriqueList = (List<FormeJuridique>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(FormeJuridique.class), false);
            return formeJuriqueList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Devise> getDeviseList() {
        try {
            String query = SQLQueryGestionArticleBudgetaire.GET_DEVISE;
            List<Devise> deviseList = (List<Devise>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Devise.class), false);
            return deviseList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Unite> getUniteList() {
        try {
            String query = SQLQueryGestionArticleBudgetaire.GET_UNITE;
            List<Unite> uniteList = (List<Unite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Unite.class), false);
            return uniteList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Tarif> getListTarif(String valueSearch, boolean isPeage) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        if (isPeage) {
            sqlQueryMaster = SQLQueryGestionArticleBudgetaire.SELECT_TARIF_BY_INTITULE_FOR_PEAGE;
        } else {
            sqlQueryMaster = SQLQueryGestionArticleBudgetaire.SELECT_TARIF_BY_INTITULE_FOR_DRHKAT;
            //sqlQueryMaster = SQLQueryGestionArticleBudgetaire.SELECT_TARIF_BY_INTITULE;
        }

        List<Tarif> listTarif = (List<Tarif>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Tarif.class), true, valueSearch.trim());
        return listTarif;
    }

    public static Boolean saveArticleBudgetaire(
            ArticleBudgetaire article,
            List<Palier> palier) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {
            if (article.getCode() == null) {
                // NOUVEL ARTICLE

                String codeArticleBudg = createArticle(article);
//                String codeArticleBudg = "00000000000004182016";

                if (codeArticleBudg != null && !codeArticleBudg.isEmpty()) {
                    //INSERT INTO PALIER
                    for (Palier pal : palier) {
                        bulkQuery.put(counter + SQLQueryGestionArticleBudgetaire.INSERT_PALIER, new Object[]{
                            codeArticleBudg.trim(),
                            pal.getBorneInferieure(),
                            pal.getBorneSuperieure(),
                            pal.getTaux(),
                            pal.getTypeTaux().trim(),
                            pal.getTarif().getCode().trim(),
                            pal.getTypePersonne().getCode().trim(),
                            pal.getEtat(),
                            pal.getAgentCreat().trim(),
                            pal.getDateCreat(),
                            pal.getAgentMaj(),
                            pal.getDateMaj(),
                            pal.getMultiplierValeurBase(),
                            pal.getDevise().getCode(),
                            pal.getUnite().getCode()
                        });
                        counter++;
                    }
                    result = executeQueryBulkInsert(bulkQuery);
                }

            } else {

            }
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static String createArticle(ArticleBudgetaire article) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = GestionArticleBudgetaireConst.ParamName.ARTICLE_BUDGETAIRE;
        try {
            String query = SQLQueryGestionArticleBudgetaire.F_NEW_ARTICLE_BUDGETAIRE_WEB;

            params.put(GestionArticleBudgetaireConst.ParamQUERY.ARTICLE_GENERIQUE, article.getArticleGenerique().getCode().trim());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.INTITULE, article.getIntitule().trim());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.UNITE, article.getUnite().getCode().trim());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.PALIER, article.getPalier());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.PERIODICITE, article.getPeriodicite().getCode().trim());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.ASSUJETISSABLE, article.getAssujetissable());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.ARTICLE_MERE, article.getArticleMere());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.TRANSACTIONNEL, article.getTransactionnel());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.DEBUT, article.getDebut());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.FIN, article.getFin());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.AGENT_CREAT, article.getAgentCreat().trim());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.PERIODICITE_VARIABLE, article.getPeriodiciteVariable());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.NBR_JOUR_LIMITE, article.getNbrJourLimite());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.TARIF_VARIABLE, article.getTarifVariable());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.TARIF, article.getTarif().getCode().trim());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.PROPRIETAIRE, article.getProprietaire());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.NATURE, article.getNature().getCode().trim());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.ARRETE, article.getArrete());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.QUANTITE_VARIABLE, article.getQuantiteVariable());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.CODE_OFFICIEL, article.getCodeOfficiel().trim());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.DATE_DEBUT_PENALITE, article.getDateDebutPenalite());
            params.put(GestionArticleBudgetaireConst.ParamQUERY.DATE_DEBUT_PERIODE, article.getDateDebutPeriode());

            String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

            if (!result.isEmpty()) {
                returnValue = result;
            }
        } catch (Exception e) {
            throw (e);
        }
        return returnValue;
    }

    public static boolean saveArticleBudgetairePaliers(ArticleBudgetaire article, List<Palier> palierList) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object> params = new HashMap<>();
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            if (!article.getCode().equals(GeneralConst.EMPTY_STRING) || !article.getCode().isEmpty()) {

                String articleBudgetaire = article.getCode();

                bulkQuery.put(counter + SQLQueryGestionArticleBudgetaire.UPDATE_ARTICLE_BUDGETAIRE, new Object[]{
                    article.getArticleGenerique().getCode(),
                    article.getIntitule(),
                    article.getUnite().getCode(),
                    article.getPalier(),
                    article.getPeriodicite().getCode(),
                    article.getAssujetissable(),
                    article.getArticleMere(),
                    article.getTarif().getCode(),
                    article.getTarifVariable(),
                    article.getAgentMaj(),
                    article.getProprietaire(),
                    article.getTransactionnel(),
                    article.getDebut(),
                    article.getFin(),
                    article.getPeriodiciteVariable(),
                    article.getNbrJourLimite(),
                    article.getNature().getCode(),
                    article.getQuantiteVariable(),
                    article.getArrete(),
                    article.getCodeOfficiel(),
                    article.getDateDebutPeriode(),
                    article.getDateDebutPenalite(),
                    article.getEcheanceLegale(),
                    article.getDateLimitePaiement(),
                    article.getPeriodeEcheance(),
                    article.getCode()
                });
                counter++;

                for (Palier pal : palierList) {
                    if (pal.getCode() > 0) {
                        bulkQuery.put(counter + SQLQueryGestionArticleBudgetaire.UPDATE_PALIER, new Object[]{
                            articleBudgetaire,
                            pal.getBorneInferieure(),
                            pal.getBorneSuperieure(),
                            pal.getTaux(),
                            pal.getTypeTaux().trim(),
                            pal.getTarif().getCode().trim(),
                            pal.getTypePersonne().getCode().trim(),
                            pal.getAgentMaj(),
                            pal.getDateMaj(),
                            pal.getMultiplierValeurBase(),
                            pal.getDevise().getCode(),
                            pal.getUnite().getCode(),
                            pal.getFkEntiteAdministrative(),
                            pal.getEtat(),
                            pal.getFkTypeBien(),
                            pal.getCode()

                        });
                        counter++;
                    } else {
                        bulkQuery.put(counter + SQLQueryGestionArticleBudgetaire.INSERT_PALIER_V3, new Object[]{
                            articleBudgetaire,
                            pal.getBorneInferieure(),
                            pal.getBorneSuperieure(),
                            pal.getTaux(),
                            pal.getTypeTaux().trim(),
                            pal.getTarif().getCode().trim(),
                            pal.getTypePersonne().getCode().trim(),
                            pal.getEtat(),
                            pal.getAgentCreat().trim(),
                            pal.getDateCreat(),
                            pal.getAgentMaj(),
                            pal.getDateMaj(),
                            pal.getMultiplierValeurBase(),
                            pal.getDevise().getCode(),
                            pal.getUnite().getCode(),
                            pal.getFkEntiteAdministrative(),
                            pal.getFkTypeBien()
                        });
                        counter++;
                    }

                }
                result = executeQueryBulkInsert(bulkQuery);

            } else {
                String query = SQLQueryGestionArticleBudgetaire.F_NEW_ARTICLE_BUDGETAIRE_WEB_V2;
                String returnValue = GestionArticleBudgetaireConst.ParamName.ARTICLE_BUDGETAIRE;;

                params.put(GestionArticleBudgetaireConst.ParamQUERY.ARTICLE_GENERIQUE, article.getArticleGenerique().getCode().trim());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.INTITULE, article.getIntitule().trim());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.UNITE, article.getUnite().getCode().trim());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.PALIER, article.getPalier());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.PERIODICITE, article.getPeriodicite().getCode().trim());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.ASSUJETISSABLE, article.getAssujetissable());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.ARTICLE_MERE, article.getArticleMere());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.TRANSACTIONNEL, article.getTransactionnel());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.DEBUT, article.getDebut());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.FIN, article.getFin());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.AGENT_CREAT, article.getAgentCreat().trim());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.PERIODICITE_VARIABLE, article.getPeriodiciteVariable());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.NBR_JOUR_LIMITE, article.getNbrJourLimite());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.TARIF_VARIABLE, article.getTarifVariable());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.TARIF, article.getTarif().getCode().trim());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.PROPRIETAIRE, article.getProprietaire());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.NATURE, article.getNature().getCode().trim());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.ARRETE, article.getArrete());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.QUANTITE_VARIABLE, article.getQuantiteVariable());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.CODE_OFFICIEL, article.getCodeOfficiel().trim());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.DATE_ECHEANCE_LEGALE, article.getEcheanceLegale());
                params.put(GestionArticleBudgetaireConst.ParamQUERY.DATE_LIMITE_PAIEMENT, article.getDateLimitePaiement());

                for (Palier pal : palierList) {
                    bulkQuery.put(counter + SQLQueryGestionArticleBudgetaire.INSERT_PALIER_V2, new Object[]{
                        pal.getBorneInferieure(),
                        pal.getBorneSuperieure(),
                        pal.getTaux(),
                        pal.getTypeTaux().trim(),
                        pal.getTarif().getCode().trim(),
                        pal.getTypePersonne().getCode().trim(),
                        pal.getEtat(),
                        pal.getAgentCreat().trim(),
                        pal.getDateCreat(),
                        pal.getAgentMaj(),
                        pal.getDateMaj(),
                        pal.getMultiplierValeurBase(),
                        pal.getDevise().getCode(),
                        pal.getUnite().getCode(),
                        pal.getFkEntiteAdministrative(),
                        pal.getFkTypeBien()
                    });
                    counter++;
                }

                result = myDao.getDaoImpl().execBulkQuery(query, returnValue, params, bulkQuery);
            }

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<Banque> getBanqueListBySite(String site) {
        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_BANQUE_SITE;
            List<Banque> banqueList = (List<Banque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Banque.class), false, site.trim());
            return banqueList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<CompteBancaire> getCompteByBanque(String codeBanque) {
        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_COMPTE_BANQUE;
            List<CompteBancaire> comptList = (List<CompteBancaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CompteBancaire.class), false, codeBanque.trim());
            return comptList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<BanqueAb> getArticleBanqueByCompteBancaire(String codeCmpteBancaire) {
        try {
            String secondQuery = GeneralConst.EMPTY_STRING;

            if (!codeCmpteBancaire.equals("*")) {
                secondQuery = "WHERE BA.FK_COMPTE = '" + codeCmpteBancaire + "'";
            }
            String query = SQLQueryGestionArticleBudgetaire.SELECT_ARTICLE_BANQUE;
            query = String.format(query, secondQuery);

            List<BanqueAb> articleList = (List<BanqueAb>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(BanqueAb.class), false);
            return articleList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Service> loadServiceByMinistre(String codeMinistere) {

        try {

            String query = SQLQueryGestionArticleBudgetaire.SELECT_SERVICE_BY_MINISTRE;
            List<Service> services = (List<Service>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Service.class), false, codeMinistere);
            return services;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<ArticleBudgetaire> loadArticleBudgetaireByService(String codeService, String compteBancaire) {

        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_ARTICLE_BUDGETAIRE_BY_SERVICE;
            List<ArticleBudgetaire> faitGenerateurList = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, codeService, compteBancaire);
            return faitGenerateurList;

        } catch (Exception e) {
            throw e;
        }

    }

    public static Boolean affecterArticleCompte(
            List<BanqueAb> listBanqueAb) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            if (listBanqueAb.size() > 0) {

                for (BanqueAb banqueAb : listBanqueAb) {

                    bulkQuery.put(counter + SQLQueryGestionArticleBudgetaire.AFFECTER_ARTICLE_COMPTE, new Object[]{
                        banqueAb.getFkBanque(),
                        banqueAb.getFkCompte().getCode(),
                        banqueAb.getFkAb().getCode(),
                        banqueAb.getEtat(),
                        banqueAb.getAgentCreat(),
                        banqueAb.getDateCreat()
                    });
                    counter++;

                }

                result = executeQueryBulkInsert(bulkQuery);
            }

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean desaffecterArticleCompte(
            List<BanqueAb> listBanqueAb) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            if (listBanqueAb.size() > 0) {

                for (BanqueAb banqueAb : listBanqueAb) {

                    bulkQuery.put(counter + SQLQueryGestionArticleBudgetaire.DESAFFECTER_ARTICLE_COMPTE, new Object[]{
                        banqueAb.getId()
                    });
                    counter++;

                }

                result = executeQueryBulkInsert(bulkQuery);
            }

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<Tarif> getTarif(boolean isPeage) {
        try {

            String query = GeneralConst.EMPTY_STRING;

            if (isPeage) {
                query = SQLQueryGestionArticleBudgetaire.SELECT_TARIF_FOR_PEAGE;
            } else {
                query = SQLQueryGestionArticleBudgetaire.SELECT_TARIF_FOR_DRHKAT;
            }

            List<Tarif> tarif = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false);
            return tarif;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean saveTarif(Tarif tarif) {

        String query = SQLQueryGestionArticleBudgetaire.F_NEW_TARIF;

        boolean result = myDao.getDaoImpl().executeNativeQuery(query, tarif.getIntitule(),
                tarif.getValeur(),
                tarif.getTypeValeur(),
                tarif.getAgentCreat(),
                tarif.getEstTarifPeage());

        return result;
    }

    public static boolean disableTarif(String codeTarif) {

        boolean result = false;
        try {
            String query = SQLQueryGestionArticleBudgetaire.DISABLE_TARIF;

            result = myDao.getDaoImpl().executeStoredProcedure(query, codeTarif);

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean updateTarif(Tarif tarif) {

        boolean result = false;
        try {
            String query = SQLQueryGestionArticleBudgetaire.UPDATE_TARIF;

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    tarif.getIntitule(),
                    tarif.getValeur(),
                    tarif.getTypeValeur(),
                    tarif.getAgentMaj(),
                    tarif.getDateMaj(),
                    tarif.getEstTarifPeage(),
                    tarif.getCode());

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean savePeriodicite(Periodicite period) {

        String query = SQLQueryGestionArticleBudgetaire.F_NEW_PERIODICITE;

        boolean result = myDao.getDaoImpl().executeStoredProcedure(query, period.getIntitule(),
                period.getNbrJour(),
                period.getPeriodeRecidive());

        return result;
    }

    public static boolean updatePeriodicite(Periodicite period) {

        boolean result = false;
        try {
            String query = SQLQueryGestionArticleBudgetaire.UPDATE_PERIODICITE;

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    period.getIntitule(),
                    period.getNbrJour(),
                    period.getPeriodeRecidive(),
                    period.getCode());

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean disablePeriodicite(String codePeriodicite) {

        boolean result = false;
        try {
            String query = SQLQueryGestionArticleBudgetaire.DISABLE_PERIODICITE;

            result = myDao.getDaoImpl().executeStoredProcedure(query, codePeriodicite);

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TarifSite> getListTarifSite() {

        try {
            String query = "SELECT * FROM T_TARIF_SITE ts WITH (READPAST) JOIN T_SITE s WITH (READPAST) ON ts.FK_SITE_PROVENANCE = s.CODE "
                    + " WHERE ts.ETAT = 1 ORDER BY s.INTITULE";
            List<TarifSite> tarifSites = (List<TarifSite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TarifSite.class), false);
            return tarifSites;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<Tarif> getListTarifPeage() {

        try {
            String query = "SELECT * FROM T_TARIF t WITH (READPAST) WHERE t.ETAT = 1 AND t.EST_TARIF_PEAGE  = 1 ORDER BY t.INTITULE";
            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false);
            return tarifs;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<Tarif> getListTarifByPeage(String codePeage) {

        try {
            String query = "SELECT t.* FROM T_TARIF t WITH (READPAST) WHERE t.ETAT = 1 AND t.EST_TARIF_PEAGE  = 1 "
                    + " AND t.CODE in (SELECT ts.FK_TARIF FROM T_TARIF_SITE ts WITH (READPAST) WHERE ts.FK_SITE_PROVENANCE = ?1 AND ts.ETAT = 1) "
                    + " ORDER BY t.INTITULE";
            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false, codePeage);
            return tarifs;

        } catch (Exception e) {
            throw e;
        }

    }

    public static boolean saveTarifSite(TarifSite tarifSite) {

        boolean result = false;
        try {

            String query = "EXEC F_NEW_TARIF_SITE ?1,?2,?3,?4,?5,?6,?7";

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    tarifSite.getId(),
                    tarifSite.getFkSiteProvenance(),
                    tarifSite.getFkSiteDestination(),
                    tarifSite.getFkTarif(),
                    tarifSite.getTaux(),
                    tarifSite.getFkDevise(),
                    tarifSite.getAgentCreate()
            );

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean deleteTarifSite(int id, int userId) {

        boolean result = false;
        try {

            String query = "UPDATE T_TARIF_SITE SET ETAT = 0, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE ID = ?2";

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    userId, id);

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Unite> getUnite() {
        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_UNITE;
            List<Unite> unite = (List<Unite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Unite.class), false);
            return unite;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean saveUnite(Unite unite) {

        boolean result = false;
        try {
            String query = SQLQueryGestionArticleBudgetaire.F_NEW_UNITE;

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    unite.getIntitule().trim(),
                    unite.getEtat());

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean saveProrogation(ProrogationEcheanceLegaleDeclaration prorogation, boolean isSaved) {

        boolean result = false;
        try {
            String query = GeneralConst.EMPTY_STRING;

            if (isSaved) {

                query = "EXEC F_NEW_PROROGATION_ECHEANCE_LEGALE ?1,?2,?3,?4,?5,?6,?7,?8";
                result = myDao.getDaoImpl().executeStoredProcedure(query,
                        prorogation.getExercice(),
                        prorogation.getFkArticleBudgetaire(),
                        prorogation.getDateDeclarationLegale(),
                        prorogation.getDateDeclarationProrogee(),
                        prorogation.getDateLimitePaiement(),
                        prorogation.getDateLimitePaiementProrogee(),
                        prorogation.getMotif(),
                        prorogation.getAgentCreate()
                );

            } else {
                query = "EXEC F_MODIFY_PROROGATION_ECHEANCE_LEGALE ?1,?2,?3,?4,?5,?6,?7,?8,?9";
                result = myDao.getDaoImpl().executeStoredProcedure(query,
                        prorogation.getId(),
                        prorogation.getExercice(),
                        prorogation.getFkArticleBudgetaire(),
                        prorogation.getDateDeclarationLegale(),
                        prorogation.getDateDeclarationProrogee(),
                        prorogation.getDateLimitePaiement(),
                        prorogation.getDateLimitePaiementProrogee(),
                        prorogation.getMotif(),
                        prorogation.getAgentMaj()
                );
            }

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean deleteProrogation(ProrogationEcheanceLegaleDeclaration prorogation) {

        boolean result = false;
        try {
            String query = GeneralConst.EMPTY_STRING;

            query = "EXEC F_DELETE_PROROGATION_ECHEANCE_LEGALE ?1,?2,?3,?4,?5";
            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    prorogation.getId(),
                    prorogation.getMotif(),
                    prorogation.getAgentMaj(),
                    prorogation.getExercice(),
                    prorogation.getFkArticleBudgetaire()
            );

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean updateUnite(Unite unite) {

        boolean result = false;
        try {
            String query = SQLQueryGestionArticleBudgetaire.UPDATE_UNITE;

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    unite.getIntitule().trim(),
                    unite.getCode().trim());

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean disableUnite(String codeUnite) {

        boolean result = false;
        try {
            String query = SQLQueryGestionArticleBudgetaire.DISABLE_UNITE;

            result = myDao.getDaoImpl().executeStoredProcedure(query, codeUnite);

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<EntiteAdministrative> getCommuneByVille(String codeVille) {
        try {
            String query = SQLQueryGestionArticleBudgetaire.GET_COMMUNE_BY_VILEE;
            List<EntiteAdministrative> entiteList = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, codeVille);
            return entiteList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static ArticleBudgetaire loadInfosArticleBudgetaire(String codeArticle) {

        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_ARTICLE_BUDGETAIRE_BY_CODE;
            List<ArticleBudgetaire> faitGenerateurList = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, codeArticle);
            return faitGenerateurList.isEmpty() ? null : faitGenerateurList.get(0);

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<Palier> loadPalierByArticle(String codeArticle) {

        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_PALIER_BY_ARTICLE;
            List<Palier> palierList = (List<Palier>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle);
            return palierList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static EntiteAdministrative loadEntiteAdministrativeByCode(String codeEntite) {

        //String query = SQLQueryGestionArticleBudgetaire.LOAD_ENTITE_ADMINISTRATIVE_BY_CODE;
        String query = SQLQueryGestionArticleBudgetaire.LOAD_ENTITE_ADMINISTRATIVE_BY_CODE_V2;

        try {

            List<EntiteAdministrative> entiteAdmin = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, codeEntite);
            return entiteAdmin.get(0);

        } catch (Exception e) {
            throw e;
        }

    }

    public static DocumentOfficiel loadDocumentOfficielByCode(String codeDocument) {

        String query = SQLQueryGestionArticleBudgetaire.LOAD_DOCUMENT_OFFICIEL_BY_CODE;

        try {

            List<DocumentOfficiel> document = (List<DocumentOfficiel>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DocumentOfficiel.class), false, codeDocument);
            return document.get(0);

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<ArticleBudgetaire> getArticleBudgetaireByService(String codeService) {

        try {
            String query = SQLQueryGestionArticleBudgetaire.GET_ARTICLE_BUDGETAIRE_BY_SERVICE;
            List<ArticleBudgetaire> faitGenerateurList = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, codeService);
            return faitGenerateurList;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<ArticleBudgetaire> getArticleBudgetaireByIntitule(String valueSearch, String codeService, String codeMinistere) {

        String sqlQueryMaster;
        String secondQuery = GeneralConst.EMPTY_STRING;

        if (!codeService.equals("")) {
            secondQuery = "AND AG.SERVICE_ASSIETTE = '" + codeService + "'";
        }
        sqlQueryMaster = SQLQueryGestionArticleBudgetaire.SELECT_ARTICLE_BUDGETAIRE_BY_SERVICE_BY_INTITULE_V2;

        sqlQueryMaster = String.format(sqlQueryMaster, secondQuery);

        List<ArticleBudgetaire> listArticle = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), true, valueSearch.trim());
        return listArticle;
    }

    public static List<EntiteAdministrative> loadEntiteAdministrativeFille(String codeMere) {
        try {
            String query = SQLQueryGestionArticleBudgetaire.LOAD_ENTITE_ADMINISTRATIVE_FILLE;
            List<EntiteAdministrative> entiteList = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, codeMere);
            return entiteList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<EntiteAdministrative> loadEntiteAdministrativeFilleV2(String codeMere) {
        try {
            String query = SQLQueryGestionArticleBudgetaire.LOAD_ENTITE_ADMINISTRATIVE_FILLE_V2;
            List<EntiteAdministrative> entiteList = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, codeMere);
            return entiteList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<TypeBien> loadAllNatureBien() {
        try {
            String query = "select * from T_TYPE_BIEN  tb WITH (READPAST) where tb.ETAT = 1 "
                    + " and tb.CODE in (select TYPE_BIEN from T_TYPE_BIEN_SERVICE WITH (READPAST) where ETAT = 1) order by INTITULE";
            List<TypeBien> entiteList = (List<TypeBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeBien.class), false);
            return entiteList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ArticleBudgetaire> getLstArticleBudgetaireAssujetti() {
        try {
            String query = "SELECT * FROM T_ARTICLE_BUDGETAIRE WITH (READPAST) WHERE ETAT = 1 AND ASSUJETISSABLE = 1 ORDER BY INTITULE";
            List<ArticleBudgetaire> articleBudgetaires = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false);
            return articleBudgetaires;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ExerciceFiscale> getLstExerciceFiscale() {
        try {
            String query = "SELECT * FROM T_EXERCICE_FISCALE WITH (READPAST) WHERE ETAT = 1 ORDER BY CODE";
            List<ExerciceFiscale> exerciceFiscales = (List<ExerciceFiscale>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ExerciceFiscale.class), false);
            return exerciceFiscales;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ProrogationEcheanceLegaleDeclaration> getListProrogationEcheanceLegaleDeclaration() {
        try {
            String query = "SELECT * FROM T_PROROGATION_ECHEANCE_LEGALE_DECLARATION WITH (READPAST) WHERE ETAT = 1";
            List<ProrogationEcheanceLegaleDeclaration> prorogationEcheanceLegaleDeclarations = (List<ProrogationEcheanceLegaleDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ProrogationEcheanceLegaleDeclaration.class), false);
            return prorogationEcheanceLegaleDeclarations;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<EntiteAdministrative> loadEntiteAdministrativeByType(String codeType) {
        try {
            String query = SQLQueryGestionArticleBudgetaire.LOAD_ENTITE_ADMINISTRATIVE_BY_TYPE;
            List<EntiteAdministrative> entiteList = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, codeType);
            return entiteList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ArticleBudgetaire> getArticleBudgetaireImpot(String code1, String code2, String code3, String code4, String code5, String code6) {

        try {
            String query = SQLQueryGestionArticleBudgetaire.GET_ARTICLE_BUDGETAIRE_IMPOT;
            List<ArticleBudgetaire> faitGenerateurList = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, code1, code2, code3, code4, code5, code6);
            return faitGenerateurList;

        } catch (Exception e) {
            throw e;
        }

    }

    public static TypeBien getTypeBienByCode(String code) {

        try {
            String query = "select * from T_TYPE_BIEN  tb WITH (READPAST) where tb.ETAT = 1 and tb.CODE = ?1";
            List<TypeBien> typeBiens = (List<TypeBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeBien.class), false, code);
            return typeBiens.isEmpty() ? null : typeBiens.get(0);

        } catch (Exception e) {
            throw e;
        }

    }

    public static FormeJuridique getFormeJuridiqueByCode(String code) {

        try {
            String query = "select * from T_FORME_JURIDIQUE WITH (READPAST) where CODE = ?1 AND ETAT = 1";
            List<FormeJuridique> formeJuridiques = (List<FormeJuridique>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(FormeJuridique.class), false, code);
            return formeJuridiques.isEmpty() ? null : formeJuridiques.get(0);

        } catch (Exception e) {
            throw e;
        }

    }

    public static boolean saveConfigTauxExercice(TauxIfExerciceAnterieur tauxIfExerciceAnterieur, boolean isCreate) {

        boolean result = false;
        String sqlQuery = "";

        if (isCreate) {

            sqlQuery = "INSERT INTO T_TAUX_IF_EXERCICE_ANTERIEUR(ANNEE,FK_TARIF,FK_TYPE_BIEN,FK_FORME_JURIDIQUE,"
                    + " FK_AB,TAUX,UNITE,MULTIPLIER_PAR_VB,AGENT_CREATE,BASE) VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,?10)";

            result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                    tauxIfExerciceAnterieur.getAnnee(),
                    tauxIfExerciceAnterieur.getFkTarif(),
                    tauxIfExerciceAnterieur.getFkTypeBien(),
                    tauxIfExerciceAnterieur.getFkFormeJuridique(),
                    tauxIfExerciceAnterieur.getFkAb(),
                    tauxIfExerciceAnterieur.getTaux(),
                    tauxIfExerciceAnterieur.getUnite(),
                    tauxIfExerciceAnterieur.getMultiplierParVb(),
                    tauxIfExerciceAnterieur.getAgentCreate(),
                    tauxIfExerciceAnterieur.getBase()
            );

        } else {

            sqlQuery = "UPDATE T_TAUX_IF_EXERCICE_ANTERIEUR SET ANNEE = ?1,FK_TARIF = ?2,FK_TYPE_BIEN = ?3,FK_FORME_JURIDIQUE = ?4,"
                    + " FK_AB = ?5,TAUX = ?6,UNITE = ?7,MULTIPLIER_PAR_VB = ?8,BASE = ?9 WHERE ID = ?10";

            result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                    tauxIfExerciceAnterieur.getAnnee(),
                    tauxIfExerciceAnterieur.getFkTarif(),
                    tauxIfExerciceAnterieur.getFkTypeBien(),
                    tauxIfExerciceAnterieur.getFkFormeJuridique(),
                    tauxIfExerciceAnterieur.getFkAb(),
                    tauxIfExerciceAnterieur.getTaux(),
                    tauxIfExerciceAnterieur.getUnite(),
                    tauxIfExerciceAnterieur.getMultiplierParVb(),
                    tauxIfExerciceAnterieur.getBase(),
                    tauxIfExerciceAnterieur.getId()
            );

        }

        return result;
    }

    public static List<TauxIfExerciceAnterieur> loadAllTauxIfExerciceAnterieur() {
        try {
            String query = "SELECT t.* FROM T_TAUX_IF_EXERCICE_ANTERIEUR t WITH (READPAST) "
                    + " JOIN T_TYPE_BIEN tb ON t.FK_TYPE_BIEN = tb.CODE WHERE t.ETAT = 1 ORDER BY t.ANNEE,tb.INTITULE";
            List<TauxIfExerciceAnterieur> tauxIfExerciceAnterieurs = (List<TauxIfExerciceAnterieur>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TauxIfExerciceAnterieur.class), false);
            return tauxIfExerciceAnterieurs;

        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean deleteConfigTauxExercice(int id, int agentMaj) {

        boolean result = false;
        String sqlQuery = "";

        sqlQuery = "UPDATE T_TAUX_IF_EXERCICE_ANTERIEUR SET ETAT = 0, DATE_MAJ = GETDATE(), AGENT_MAJ = ?1 WHERE ID = ?2";

        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                agentMaj, id
        );

        return result;
    }

    public static List<TauxVignette> getAllTauxVignettes() {
        try {
            String query = "select * from T_TAUX_VIGNETTE WITH (READPAST) WHERE ETAT = 1 order BY FK_FORME_JURIDIQUE";
            List<TauxVignette> tauxVignettes = (List<TauxVignette>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TauxVignette.class), false);
            return tauxVignettes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Tarif> getAllTarifVignette(String codeABVignette) {
        try {
            String query = "select t.* from T_TARIF t WITH (READPAST) WHERE t.ETAT = 1 AND "
                    + " t.CODE IN (SELECT TARIF FROM T_PALIER p WITH (READPAST) WHERE p.ETAT = 1 "
                    + " AND p.ARTICLE_BUDGETAIRE = ?1) ORDER BY t.INTITULE";
            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false, codeABVignette);
            return tarifs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean saveTauxVignette(TauxVignette tauxVignette) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            String sqlQuery = "";

            if (tauxVignette.getId() == null) {

                sqlQuery = ":INSERT INTO T_TAUX_VIGNETTE (FK_FORME_JURIDIQUE,FK_TARIF,VIGNETTE,TSCR,POURCENTAGE_VIGNETTE,POURCENTAGE_TSCR,ETAT) VALUES (?1,?2,?3,?4,?5,?6,1)";

                bulkQuery.put(counter + sqlQuery, new Object[]{
                    tauxVignette.getFkFormeJuridique(),
                    tauxVignette.getFkTarif(),
                    tauxVignette.getVignette(),
                    tauxVignette.getTscr(),
                    tauxVignette.getPourcentageVignette(),
                    tauxVignette.getPourcentageTscr()
                });

            } else {

                sqlQuery = ":UPDATE T_TAUX_VIGNETTE SET FK_FORME_JURIDIQUE = ?1,FK_TARIF = ?2,VIGNETTE = ?3,TSCR = ?4,POURCENTAGE_VIGNETTE = ?5,POURCENTAGE_TSCR = ?6 WHERE ID = ?7";

                bulkQuery.put(counter + sqlQuery, new Object[]{
                    tauxVignette.getFkFormeJuridique(),
                    tauxVignette.getFkTarif(),
                    tauxVignette.getVignette(),
                    tauxVignette.getTscr(),
                    tauxVignette.getPourcentageVignette(),
                    tauxVignette.getPourcentageTscr(),
                    tauxVignette.getId()
                });

            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean deleteTauxVignette(int id) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            String sqlQuery = ":UPDATE T_TAUX_VIGNETTE SET ETAT = 0 WHERE ID = ?1";

            bulkQuery.put(counter + sqlQuery, new Object[]{id});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }
}
