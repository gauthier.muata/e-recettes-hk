/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package cd.hologram.erecettesvg.etats;

import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import static cd.hologram.erecettesvg.etats.UtilitaireSession.getTextSession;
import java.util.*;
import javax.faces.context.FacesContext;

/**
*
* @author hids
*/
public class TraitReporting {

    String txtImpression;

    public String getTxtImpression() {
        return txtImpression;
    }

    public void setTxtImpression(String txtImpression) {
        this.txtImpression = txtImpression;
    }

    public String loadContenuPageFromSession() {
        String btn = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(
                DocumentConst.ButtonName.BTN);
        if (btn == null) {
            String contenuPage = getTextSession(DocumentConst.DocumentParamName.TEXT_A_AFFICHE);
            if (contenuPage.equals(GeneralConst.EMPTY_STRING)) {
                txtImpression = "<h1 align=\"center\">Impossible d'afficher le contenu du document, </h1><h1 align=\"center\">Veuillez reessayer!</h1>";
                return txtImpression;
            }
            this.txtImpression = contenuPage;
        }
        return txtImpression;
    }

    public boolean saveDataImpression() {
        boolean save = false;
        String btn = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(
                DocumentConst.ButtonName.BTN);
        if (btn != null) {
            if (btn.equals(DocumentConst.ButtonName.VISUALISER)) {
                save = false;
                List<Anchor> AncorList = UtilitaireSession.getAnchorsSession(DocumentConst.DocumentParamName.LIST_ANCHOR);
                String textTtraiter = UtilitaireSession.getTextSession(DocumentConst.DocumentParamName.TEXT_TRAITER_A_VISUALISER);
                String textTraiteReplace = textTtraiter;
                for (Anchor anchor : AncorList) {
                    String parameterInputName = DocumentConst.DocumentParamName.INPUT.concat(anchor.getIndex());
                    String parameterAreaName = DocumentConst.DocumentParamName.AREA.concat(anchor.getIndex());
                    String textInputTrouver = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(parameterInputName);
                    String textAreaTrouver = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(parameterAreaName);

                    if (textInputTrouver != null && !textInputTrouver.equals(GeneralConst.EMPTY_STRING)) {
                       textTraiteReplace = textTraiteReplace.replace(anchor.getAnchorTxt(), textInputTrouver);
                    }
                    if (textAreaTrouver != null && !textAreaTrouver.equals(GeneralConst.EMPTY_STRING)) {
                        textTraiteReplace = textTraiteReplace.replace(anchor.getAnchorTxt(), textAreaTrouver);
                    }
                }

                UtilitaireSession.setSession(DocumentConst.DocumentParamName.TEXT_TRAITER_VISUALISER, textTraiteReplace);
                textTtraiter = "<form action=\"savet\" method=\"post\" name=\"document\">" + textTraiteReplace + "<br/> <input type=\"button\" onclick=\"history.back()\" value=\"Pr&eacute;c&eacute;dent\" class=\"impression\"/> <input name=\"btn\" type=\"submit\" value=\"Enregistrer\" /> </form>";
                textTraiteReplace = textTtraiter;
                this.txtImpression = textTtraiter;
            }

            if (btn.equals(DocumentConst.ButtonName.ENREGISTRER)) {
                String entete = "<head> <script src=\"ressources/js/disableSelect.js\" type=\"text/javascript\"></script>"
                        + "<link href=\"ressources/css/print.css\" rel=\"stylesheet\" type=\"text/css\" media=\"print\" /> "
                        + "</head> <body> <form action=\"\" method=\"\" name=\"document\"> <br/><br/><br/><br/>";
                String pied = "<br/><input type=\"button\" onclick=\"window.print()\" value=\"Imprimer\" class=\"impression\"/> </form> </body>";

                String textTtraiterEnregistrer = (String) UtilitaireSession.getTextSession(DocumentConst.DocumentParamName.TEXT_TRAITER_VISUALISER);
                textTtraiterEnregistrer = entete.concat(textTtraiterEnregistrer).concat(pied);
                if (textTtraiterEnregistrer != null && !textTtraiterEnregistrer.isEmpty()) { 
                    this.txtImpression = textTtraiterEnregistrer;
                    save = true;
                }
            }
        }
        return save;
    }
}
