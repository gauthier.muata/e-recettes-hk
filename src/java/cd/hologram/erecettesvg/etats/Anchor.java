package cd.hologram.erecettesvg.etats;

/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/

import cd.hologram.erecettesvg.constants.GeneralConst;
import java.util.*;
import java.util.regex.*;

/**
*
* @author hids
*/
public class Anchor {

    private String index,
            anchorTxt,
            field,
            titre,
            table,
            outPutTable,
            updateTable,
            attribut;
    public static Pattern pattern;
    public static Matcher matcher;

    public Anchor(String index, String anchorTxt, String field, String titre, String table, String outPutTable, String updateTable, String attribut) {
        this.index = index;
        this.anchorTxt = anchorTxt;
        this.field = field;
        this.titre = titre;
        this.table = table;
        this.outPutTable = outPutTable;
        this.updateTable = updateTable;
        this.attribut = attribut;
    }

    public Anchor(){
    }

    public String getAnchorTxt() {
        return anchorTxt;
    }

    public void setAnchorTxt(String anchorTxt) {
        this.anchorTxt = anchorTxt;
    }

    public String getAttribut() {
        return attribut;
    }

    public void setAttribut(String attribut) {
        this.attribut = attribut;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getOutPutTable() {
        return outPutTable;
    }

    public void setOutPutTable(String outPutTable) {
        this.outPutTable = outPutTable;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getUpdateTable() {
        return updateTable;
    }

    public void setUpdateTable(String updateTable) {
        this.updateTable = updateTable;
    }

    public List<Anchor> getAnchorList(String document) {

        List<Anchor> anchirList = new ArrayList<>();

        pattern = Pattern.compile("<a *(\\w*=\"[a-zA-Z0-9\\=*,*;*:*\\-*?*�*�*�*�*&* *\\(*\\)*]*\" *)* */>");

        matcher = pattern.matcher(document);
        int i = 0;

        while (matcher.find()) {
            anchorTxt = matcher.group();

            int pos1 = anchorTxt.indexOf("\"");
            int pos2 = anchorTxt.lastIndexOf("\"");
            if (pos1 != pos2) {
                index = String.valueOf(i++);

                String[] anchorContent = anchorTxt.substring(pos1, pos2).split("[;:]");
               
                field = titre = table = outPutTable = updateTable = attribut = GeneralConst.EMPTY_STRING;

                if (anchorContent.length > 0) {
                    field = anchorContent[0].replace("\"", GeneralConst.EMPTY_STRING);
                }

                if (anchorContent.length > 1) {
                    titre = anchorContent[1];
                }

                if (anchorContent.length > 2) {
                    table = anchorContent[2];
                }

                if (anchorContent.length > 3) {
                    outPutTable = anchorContent[3];
                }

                if (anchorContent.length > 4) {
                    updateTable = anchorContent[4];
                }

                if (anchorContent.length > 5) {
                    attribut = anchorContent[5];
                }

            } else {
            }

            Anchor anchor = new Anchor(index, anchorTxt, field, titre, table, outPutTable, updateTable, attribut);

            anchirList.add(anchor);
        }

        return anchirList;
    }
}
