/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.etats;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author gauthier.muata
 */
public class DocumentReturnString {

    public static String getDocument(
            String cle,
            Object object,
            String htmlTextAnchor,
            String initieCodDocument,
            String codeModelDocConfig) {

        String documentToString;

        Map listNp = new HashMap<>();
        String textTtraiter = htmlTextAnchor;
        String textAnchore = textTtraiter;

        try {
            String resultatTraitement;
            Anchor anchor = new Anchor();
            listNp.put(cle, object);
            List<Anchor> listAnchor = anchor.getAnchorList(textTtraiter);
            TraiteAnchor traiteAnchor = new TraiteAnchor();

            for (Anchor monAnchor : listAnchor) {
                resultatTraitement = traiteAnchor.Traitement(monAnchor, listNp);
                if (!resultatTraitement.contains("<")) {
                    textAnchore = textAnchore.replace(monAnchor.getAnchorTxt(), resultatTraitement);
                } else if (resultatTraitement.contains("<table")) {
                    textAnchore = textAnchore.replace(monAnchor.getAnchorTxt(), resultatTraitement);
                }
                textTtraiter = textTtraiter.replace(monAnchor.getAnchorTxt(), resultatTraitement);
            }
        } catch (Exception e) {
            throw e;
        }

        documentToString = textTtraiter;

        return documentToString;
    }

}
