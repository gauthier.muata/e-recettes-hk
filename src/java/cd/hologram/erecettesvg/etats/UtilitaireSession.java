/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package cd.hologram.erecettesvg.etats;

import cd.hologram.erecettesvg.constants.GeneralConst;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.*;

/**
*
* @author hids
*/
public class UtilitaireSession {
    
    public static String getTextSession(String var) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        return session.getAttribute(var) == null ? GeneralConst.EMPTY_STRING : (String) session.getAttribute(var);
    }
    
    public static List<Anchor> getAnchorsSession(String var) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        return session.getAttribute(var) == null ? null : (List<Anchor>) session.getAttribute(var);
    }
    
    public static void setSession(String cle, Object object) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.setAttribute(cle, object);
    }
    
    public static void removeAttributeSession(String var) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.removeAttribute(var);
    }
    
}
