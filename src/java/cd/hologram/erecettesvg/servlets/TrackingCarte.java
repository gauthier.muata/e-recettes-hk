/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.AcquitLiberatoireBusiness;
import cd.hologram.erecettesvg.business.AssujettissementBusiness;
import static cd.hologram.erecettesvg.business.AssujettissementBusiness.getComplements;
import static cd.hologram.erecettesvg.business.AssujettissementBusiness.getValeurComplement;
import static cd.hologram.erecettesvg.business.TrackingCarteBusiness.getDetailsCarte;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.util.*;
import static cd.hologram.erecettesvg.util.ConvertDate.formatDateToString;
import java.io.*;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.*;

/**
 *
 * @author juslin.tshiamua
 */
@WebServlet(name = "TrackingCarte", urlPatterns = {"/trackingCarte_servlet"})
public class TrackingCarte extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    static List<ComplementBien> infoComplementaires;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case AssujettissementConst.Operation.LOAD_INFORMATIONS_CARTE:
                result = loadInformationsCarte(request);
                break;
        }
        out.print(result);
    }

    public String loadInformationsCarte(HttpServletRequest request) {
        String numero_carte = request.getParameter("numero_carte");
        String nom_conducteur = request.getParameter("nom_conducteur");
        String prenom_conducteur = request.getParameter("prenom_conducteur");
        String filtre = request.getParameter("filtre");

        String complementLieuNaissance = propertiesConfig.getProperty("CODE_COMPLEMENT_FORME_LIEU_NAISSANCE");
        String complementDateNaissance = propertiesConfig.getProperty("CODE_COMPLEMENT_FORME_DATE_NAISSANCE");
        String complementSexe = propertiesConfig.getProperty("CODE_TYPE_COMPLEMENT_SEXE");

        List<JSONObject> listObject = new ArrayList<>();

        int nbreTaxe = 0;

        try {
            List<Carte> cartes = getDetailsCarte(numero_carte);
            if (!cartes.isEmpty()) {
                for (Carte carte : cartes) {

                    cd.hologram.erecettesvg.models.AcquitLiberatoire cpi = AcquitLiberatoireBusiness.getAcquitLiberatoireByNumero(
                            carte.getNumeroCpi());

                    if (cpi != null) {

                        nbreTaxe = cpi.getNotePerception().getNoteCalcul().getDetailsNcList().size();

                    }

                    String dateNaiss = GeneralConst.EMPTY_STRING, lieuNaiss = GeneralConst.EMPTY_STRING,
                            sexe = GeneralConst.EMPTY_STRING, postnomConducteur, prenomConducteur, postnomProprietaire, prenomProprietaire;
                    JSONObject object = new JSONObject();

                    postnomConducteur = carte.getFkConducteurVehicule().getConducteur().getPostnom();
                    prenomConducteur = carte.getFkConducteurVehicule().getConducteur().getPrenoms();
                    postnomProprietaire = carte.getFkConducteurVehicule().getProprietaire().getPostnom();
                    prenomProprietaire = carte.getFkConducteurVehicule().getProprietaire().getPrenoms();

                    List<Complement> complements = getComplements(carte.getFkConducteurVehicule().getConducteur().getCode());

                    if (!complements.isEmpty()) {
                        for (Complement complement : complements) {
                            if (complement.getFkComplementForme().getCode().equals(complementDateNaissance)) {
                                dateNaiss = complement.getValeur();
                            }
                            if (complement.getFkComplementForme().getCode().equals(complementLieuNaissance)) {
                                lieuNaiss = complement.getValeur();
                            }
                            if (complement.getFkComplementForme().getComplement().getCode().equals(complementSexe)) {
                                ValeurPredefinie vp = getValeurComplement(complement.getValeur());
                                sexe = vp == null ? complement.getValeur() : vp.getValeur();
                                sexe = sexe == null ? GeneralConst.DASH_SEPARATOR : sexe;
                            }
                        }
                    }

                    object.put("idCarte", carte.getId());

                    object.put("nbreTaxe", nbreTaxe);
                    object.put("idConducteur", carte.getFkConducteurVehicule().getConducteur().getCode());
                    object.put("nomConducteur", carte.getFkConducteurVehicule().getConducteur().getNom());
                    object.put("postnomConducteur", postnomConducteur.equalsIgnoreCase(GeneralConst.EMPTY_STRING) ? GeneralConst.DASH_SEPARATOR : postnomConducteur);
                    object.put("prenomConducteur", prenomConducteur.equalsIgnoreCase(GeneralConst.EMPTY_STRING) ? GeneralConst.DASH_SEPARATOR : prenomConducteur);
                    object.put("dateNaissanceConducteur", dateNaiss.equalsIgnoreCase(GeneralConst.EMPTY_STRING) ? GeneralConst.DASH_SEPARATOR : ConvertDate.getValidFormatDatePrint(dateNaiss));
                    object.put("lieuNaissanceConducteur", lieuNaiss.equalsIgnoreCase(GeneralConst.EMPTY_STRING) ? GeneralConst.DASH_SEPARATOR : lieuNaiss);
                    object.put("sexeConducteur", sexe.equalsIgnoreCase(GeneralConst.EMPTY_STRING) ? GeneralConst.DASH_SEPARATOR : sexe);

                    object.put("bien", carte.getFkConducteurVehicule().getBien() != null ? carte.getFkConducteurVehicule().getBien().getIntitule() : GeneralConst.EMPTY_STRING);
                    object.put("typeBien", carte.getFkConducteurVehicule().getBien() != null ? carte.getFkConducteurVehicule().getBien().getTypeBien().getIntitule() : GeneralConst.EMPTY_STRING);

                    if (carte.getFkConducteurVehicule().getBien() != null) {

                        Bien bien = carte.getFkConducteurVehicule().getBien();

                        object.put("autocollant", carte.getNumeroAutocollan() != null ? carte.getNumeroAutocollan() : GeneralConst.EMPTY_STRING);
                        object.put("autorisaionTransport", carte.getNumeroAutorisation() != null ? carte.getNumeroAutorisation() : GeneralConst.EMPTY_STRING);

                        object.put("bienExist", GeneralConst.Number.ONE);

                        if (!bien.getComplementBienList().isEmpty()) {

                            for (ComplementBien cb : bien.getComplementBienList()) {

                                TypeComplement typeComplement;
                                ValeurPredefinie valeurPredefinie = new ValeurPredefinie();

                                typeComplement = cb.getTypeComplement().getComplement();

                                if (typeComplement != null) {

                                    switch (typeComplement.getCode()) {
                                        case "00000000000000062015": // IMMATRICULATION
                                            if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {

                                                object.put("IMMATRICULATION", cb.getValeur());

                                            } else {
                                                object.put("IMMATRICULATION", GeneralConst.EMPTY_STRING);
                                            }

                                            break;
                                        case "00000000000000132015": // COULEUR

                                            if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {

                                                valeurPredefinie = AssujettissementBusiness.getValeurComplement(cb.getValeur());

                                                if (valeurPredefinie != null) {
                                                    object.put("COULEUR", valeurPredefinie.getValeur());
                                                } else {
                                                    object.put("COULEUR", GeneralConst.EMPTY_STRING);
                                                }

                                            } else {
                                                object.put("COULEUR", GeneralConst.EMPTY_STRING);
                                            }

                                            break;

                                        case "00000000000001482015": // MARQUE

                                            if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {

                                                valeurPredefinie = AssujettissementBusiness.getValeurComplement(cb.getValeur());

                                                if (valeurPredefinie != null) {
                                                    object.put("MARQUE", valeurPredefinie.getValeur());
                                                } else {
                                                    object.put("MARQUE", GeneralConst.EMPTY_STRING);
                                                }

                                            } else {
                                                object.put("MARQUE", GeneralConst.EMPTY_STRING);
                                            }

                                            break;

                                        case "00000000000001562015": // NUMERO CHASIS

                                            if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {

                                                object.put("NUMEROCHASIS", cb.getValeur());

                                            } else {
                                                object.put("NUMEROCHASIS", GeneralConst.EMPTY_STRING);
                                            }

                                            break;
                                        case "00000000000000052015": // CV

                                            if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {

                                                object.put("CV", cb.getValeur());

                                            } else {
                                                object.put("CV", GeneralConst.EMPTY_STRING);
                                            }
                                            break;

                                    }

                                } else {
                                    object.put("IMMATRICULATION", GeneralConst.EMPTY_STRING);
                                    object.put("COULEUR", GeneralConst.EMPTY_STRING);
                                    object.put("MARQUE", GeneralConst.EMPTY_STRING);
                                    object.put("NUMEROCHASIS", GeneralConst.EMPTY_STRING);
                                    object.put("CV", GeneralConst.EMPTY_STRING);
                                }
                            }
                        } else {
                            object.put("IMMATRICULATION", GeneralConst.EMPTY_STRING);
                            object.put("COULEUR", GeneralConst.EMPTY_STRING);
                            object.put("MARQUE", GeneralConst.EMPTY_STRING);
                            object.put("NUMEROCHASIS", GeneralConst.EMPTY_STRING);
                            object.put("CV", GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        object.put("bienExist", GeneralConst.Number.ZERO);
                        object.put("IMMATRICULATION", GeneralConst.EMPTY_STRING);
                        object.put("COULEUR", GeneralConst.EMPTY_STRING);
                        object.put("MARQUE", GeneralConst.EMPTY_STRING);
                        object.put("NUMEROCHASIS", GeneralConst.EMPTY_STRING);
                        object.put("CV", GeneralConst.EMPTY_STRING);
                        object.put("autocollant", GeneralConst.EMPTY_STRING);
                        object.put("autorisaionTransport", GeneralConst.Number.ZERO);
                    }

                    object.put("dateDebutContrat", carte.getDateFinValidite() == null ? GeneralConst.DASH_SEPARATOR : formatDateToString(carte.getDateGeneration()));
                    object.put("etat", carte.getEtat());
                    object.put("dateFinValiditeCarte", carte.getDateFinValidite() == null ? GeneralConst.DASH_SEPARATOR : formatDateToString(carte.getDateFinValidite()));
                    object.put("dateLivraison", carte.getDateLivraison() == null ? GeneralConst.DASH_SEPARATOR : formatDateToString(carte.getDateLivraison()));
                    object.put("nomProprietaire", carte.getFkConducteurVehicule().getProprietaire().getNom());
                    object.put("postnomProprietaire", postnomConducteur.equalsIgnoreCase(GeneralConst.EMPTY_STRING) ? GeneralConst.DASH_SEPARATOR : postnomProprietaire);
                    object.put("prenomProprietaire", prenomProprietaire.equalsIgnoreCase(GeneralConst.EMPTY_STRING) ? GeneralConst.DASH_SEPARATOR : prenomProprietaire);
                    boolean isValideDate = Compare.before(carte.getDateFinValidite(), new Date());
                    object.put("carteExpiree", isValideDate);
                    listObject.add(object);
                }

                return listObject.toString();

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
