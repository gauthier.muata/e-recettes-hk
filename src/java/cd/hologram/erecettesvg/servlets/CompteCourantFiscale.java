/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.CompteCourantFiscalBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import static cd.hologram.erecettesvg.business.TaxationBusiness.getServiceByCode;
import static cd.hologram.erecettesvg.business.TaxationBusiness.getSiteByCode;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.TaxationConst;
import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Avis;
import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.Jdossier;
import cd.hologram.erecettesvg.models.NoteCalcul;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.Property;
import cd.hologram.erecettesvg.util.Tools;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author gauthier.muata
 */
@WebServlet(name = "CompteCourantFiscale", urlPatterns = {"/compteCourantFiscale"})
public class CompteCourantFiscale extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    Service service;
    Site site;
    Agent agent;
    String dateTraitement;
    Avis avis;
    ArticleBudgetaire articleBudgetaire;
    Tarif tarif;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case "loadCompteFiscalAssujetti":
                result = loadCompteFiscalAssujetti(request);
                break;

        }
        out.print(result);
    }

    public String loadCompteFiscalAssujetti(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        //List<NotePerception> listNotePerception = new ArrayList<>();
        //List<Med> listMed = new ArrayList<>();
        //List<DemandeEchelonnement> listDemandeEchelonnement = new ArrayList<>();
        //List<Journal> listJournal = new ArrayList<>();
        try {

            String assujetti = request.getParameter("assujetti");
            String module = request.getParameter("module");

            switch (module) {
                case "TAX":
                    dataReturn = getListTaxationAssujetti(assujetti);
                    break;
                case "REC":
                    break;
                case "FRAC":
                    break;
                case "MED":
                    break;
                case "POUR":
                    break;
                case "CONT":
                    break;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getListTaxationAssujetti(String assujetti) {

        List<NoteCalcul> listNoteCalcul = new ArrayList<>();
        List<Jdossier> listJdossier = new ArrayList<>();
        String dataTaxation = GeneralConst.EMPTY_STRING;

        JsonObject jsonData = new JsonObject();

        try {

            Personne personne = IdentificationBusiness.getPersonneByCode(assujetti.trim());
            Adresse adresse = new Adresse();

            if (personne != null) {

                jsonData.addProperty("code", personne.getCode());
                jsonData.addProperty("module", "TAX");

                if (personne.getNif() != null && !personne.getNif().isEmpty()) {
                    jsonData.addProperty("nif", personne.getNif());
                } else {
                    jsonData.addProperty("nif", "AUCUN");
                }

                jsonData.addProperty("nom", personne.toString().toUpperCase().concat(
                        GeneralConst.SPACE).concat("(".concat(
                                        personne.getFormeJuridique().getIntitule().toUpperCase().concat(")"))));

                adresse = TaxationBusiness.getAdresseDefaultByAssujetti(assujetti.trim());
                jsonData.addProperty("adresse", adresse != null ? adresse.toString().toUpperCase() : GeneralConst.EMPTY_STRING);

                listJdossier = CompteCourantFiscalBusiness.getListJDossierByAssujetti(assujetti.trim());

                jsonData.addProperty("lblModule", "Nombre taxation");

                BigDecimal creditEnUsd = new BigDecimal("0");
                BigDecimal creditEnCdf = new BigDecimal("0");

                if (!listJdossier.isEmpty()) {

                    for (Jdossier jdossier : listJdossier) {

                        switch (jdossier.getDevise()) {
                            case GeneralConst.Devise.DEVISE_CDF:
                                creditEnCdf = creditEnCdf.add(jdossier.getSolde());
                                break;
                            case GeneralConst.Devise.DEVISE_USD:
                                creditEnUsd = creditEnUsd.add(jdossier.getSolde());
                                break;
                        }
                    }

                }

                jsonData.addProperty("creditUsd", creditEnUsd);
                jsonData.addProperty("creditCdf", creditEnCdf);

                listNoteCalcul = CompteCourantFiscalBusiness.getListNoteCalculByAssujetti(assujetti);

                jsonData.addProperty("nbre", listNoteCalcul.size());

                String devise = GeneralConst.EMPTY_STRING;

                if (!listNoteCalcul.isEmpty()) {

                    jsonData.addProperty("TAXATION_EXIST", GeneralConst.Number.ONE);

                    List<JsonObject> ncJsonList = new ArrayList<>();

                    for (NoteCalcul nc : listNoteCalcul) {

                        devise = GeneralConst.EMPTY_STRING;

                        JsonObject ncJson = new JsonObject();

                        service = new Service();
                        site = new Site();
                        agent = new Agent();
                        avis = new Avis();

                        BigDecimal amountNc = new BigDecimal("0");

                        ncJson.addProperty("numero", nc.getNumero());
                        ncJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL, nc.getExercice().trim());

                        String dateCreate = ConvertDate.formatDateToStringOfFormat(nc.getDateCreat(),
                                GeneralConst.Format.FORMAT_DATE);

                        ncJson.addProperty("DATE_TAXATION", dateCreate);

                        service = getServiceByCode(nc.getService().trim());

                        ncJson.addProperty("SERVICE", service != null
                                ? service.getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);

                        site = getSiteByCode(nc.getSite().trim());

                        ncJson.addProperty("SITE", site != null
                                ? site.getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);

                        agent = GeneralBusiness.getAgentByCode(nc.getAgentCreat());

                        ncJson.addProperty("TAXATEUR", agent != null
                                ? agent.toString().toUpperCase() : GeneralConst.EMPTY_STRING);

                        if (nc.getDateCloture() != null) {

                            dateTraitement = ConvertDate.formatDateHeureToString(nc.getDateCloture());
                            agent = GeneralBusiness.getAgentByCode(nc.getAgentCloture());

                            ncJson.addProperty("VALIDATEUR", agent != null
                                    ? agent.toString().toUpperCase() : GeneralConst.EMPTY_STRING);
                            ncJson.addProperty("DATE_VALIDATION", dateTraitement);

                            ncJson.addProperty("ETAT_VALIDATION", nc.getEtat() == 1 ? "VALIDER" : "REJETTER");

                            if (nc.getEtat() == 1) {
                                ncJson.addProperty("ETAT_VALIDATION_ID", 1);
                            } else {
                                ncJson.addProperty("ETAT_VALIDATION_ID", 0);
                            }

                        } else {

                            ncJson.addProperty("VALIDATEUR", GeneralConst.EMPTY_STRING);
                            ncJson.addProperty("DATE_VALIDATION", GeneralConst.EMPTY_STRING);
                            ncJson.addProperty("ETAT_VALIDATION", GeneralConst.EMPTY_STRING);
                            ncJson.addProperty("ETAT_VALIDATION_ID", 0);
                        }

                        if (nc.getDateValidation() != null) {

                            dateTraitement = ConvertDate.formatDateHeureToString(nc.getDateValidation());
                            agent = GeneralBusiness.getAgentByCode(nc.getAgentValidation());

                            ncJson.addProperty("ORDONNATEUR", agent != null
                                    ? agent.toString().toUpperCase() : GeneralConst.EMPTY_STRING);

                            ncJson.addProperty("DATE_ORDONNACEMENT", dateTraitement);

                            if (nc.getAvis() != null && !nc.getAvis().isEmpty()) {

                                avis = TaxationBusiness.getAvisById(nc.getAvis());

                                ncJson.addProperty("AVIS_ORDONNACEMENT", avis.getIntitule());

                                if (nc.getAvis().equals("AV0012015")) {
                                    ncJson.addProperty("AVIS_ORDONNACEMENT_ID", 1);
                                } else {
                                    ncJson.addProperty("AVIS_ORDONNACEMENT_ID", 0);
                                }

                            } else {
                                ncJson.addProperty("AVIS_ORDONNACEMENT_ID", 0);
                                ncJson.addProperty("AVIS_ORDONNACEMENT", GeneralConst.EMPTY_STRING);
                            }

                        } else {

                            ncJson.addProperty("ORDONNATEUR", GeneralConst.EMPTY_STRING);
                            ncJson.addProperty("DATE_ORDONNACEMENT", GeneralConst.EMPTY_STRING);
                            ncJson.addProperty("AVIS_ORDONNACEMENT", GeneralConst.EMPTY_STRING);
                            ncJson.addProperty("AVIS_ORDONNACEMENT_ID", 0);
                        }

                        if (!nc.getDetailsNcList().isEmpty()) {

                            ncJson.addProperty("DETAIL_EXIST", GeneralConst.Number.ONE);

                            List<JsonObject> dncJsonList = new ArrayList<>();

                            for (DetailsNc dnc : nc.getDetailsNcList()) {

                                JsonObject dncJson = new JsonObject();
                                tarif = new Tarif();

                                amountNc = amountNc.add(dnc.getMontantDu());

                                if (devise.isEmpty()) {
                                    devise = dnc.getDevise();
                                }

                                dncJson.addProperty("ARTICLE_BUDGETAIRE", dnc.getArticleBudgetaire().getIntitule());

                                if (dnc.getTarif() != null && !dnc.getTarif().isEmpty()) {

                                    tarif = TaxationBusiness.getTarifByCode(dnc.getTarif());

                                    dncJson.addProperty("TARIF", tarif != null
                                            ? tarif.getIntitule() : GeneralConst.EMPTY_STRING);

                                } else {
                                    dncJson.addProperty("TARIF", GeneralConst.EMPTY_STRING);
                                }

                                dncJson.addProperty("MONTANT_DU", dnc.getMontantDu());
                                dncJson.addProperty("DEVISE", dnc.getDevise());
                                dncJson.addProperty("NC", nc.getNumero());
                                dncJson.addProperty("QTE", dnc.getQte());
                                dncJson.addProperty("TAUX", dnc.getTauxArticleBudgetaire());
                                dncJson.addProperty("TYPE_TAUX", dnc.getTypeTaux().equals("%") ? dnc.getTypeTaux() : GeneralConst.EMPTY_STRING);
                                dncJson.addProperty("BASE", dnc.getValeurBase());

                                if (dnc.getPenaliser() == GeneralConst.Numeric.ONE) {

                                    dncJson.addProperty("PERIODE",
                                            Tools.getPeriodeIntitule(dnc.getPeriodeDeclaration().getDebut(),
                                                    articleBudgetaire.getPeriodicite().getCode()));

                                } else {
                                    dncJson.addProperty("PERIODE", "AUCUNE");
                                }

                                dncJsonList.add(dncJson);
                            }

                            ncJson.addProperty("DETAIL_LIST", dncJsonList.toString());

                        } else {
                            ncJson.addProperty("DETAIL_EXIST", GeneralConst.Number.ZERO);
                            ncJson.addProperty("DETAIL_LIST", GeneralConst.EMPTY_STRING);
                        }

                        ncJson.addProperty("amountNc", amountNc);
                        ncJson.addProperty("deviseNc", devise);

                        ncJsonList.add(ncJson);

                    }

                    jsonData.addProperty("TAXATION_LIST", ncJsonList.toString());

                } else {
                    jsonData.addProperty("TAXATION_LIST", GeneralConst.EMPTY_STRING);
                    jsonData.addProperty("TAXATION_EXIST", GeneralConst.Number.ZERO);
                }

                dataTaxation = jsonData.toString();

            } else {
                dataTaxation = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataTaxation = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataTaxation;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
