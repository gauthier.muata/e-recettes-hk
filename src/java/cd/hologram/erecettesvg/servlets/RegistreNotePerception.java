/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.ContentieuxBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import static cd.hologram.erecettesvg.business.IdentificationBusiness.getPersonneByCode;
import cd.hologram.erecettesvg.business.NotePerceptionBusiness;
import cd.hologram.erecettesvg.business.PaiementBusiness;
import cd.hologram.erecettesvg.business.RecouvrementBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.constants.NotePerceptionConst;
import cd.hologram.erecettesvg.constants.TaxationConst;
import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Archive;
import cd.hologram.erecettesvg.models.ComplementFraisCertification;
import cd.hologram.erecettesvg.models.ComplementInfoTaxe;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.Jdossier;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.LogNoteTaxation;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.NotePerceptionBanque;
import cd.hologram.erecettesvg.models.NotePerceptionVignette;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.SuiviComptableDeclaration;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.models.Taux;
import cd.hologram.erecettesvg.sql.SQLQueryGeneral;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.CustumException;
import cd.hologram.erecettesvg.util.PrintDocument;
import cd.hologram.erecettesvg.util.Property;
import cd.hologram.erecettesvg.util.Tools;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author : WILLY KASHALA Tel : 00243 81 27 20 560
 */
@WebServlet(name = "RegistreNotePerception", urlPatterns = {"/registrenoteperception"})
public class RegistreNotePerception extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE);
    Properties propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    LogNoteTaxation logNoteTaxation;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case NotePerceptionConst.Operation.LOAD_NOTE_PERCEPTION:
                result = getNotePerception(request);
                break;
            case NotePerceptionConst.Operation.PRINT_NOTE_PERCEPTION:
                result = printNP(request);
                break;
            case "printNpFille":
                result = printNPFille(request);
                break;
            case NotePerceptionConst.Operation.UPDATE_DATE_ECHEANCE:
                result = accuserReception(request);
                break;
            case "getMinier":
                result = loadMiniers(request);
                break;
            case "getSousProvisionMinier":
                result = loadSousProvisionMinier(request);
                break;
            case "saveManifeste":
                result = saveManifeste(request);
                break;
            case "printRapportSituationEntrepriseTaxeConcentre":
                result = printRapportSituationEntrepriseTaxeConcentre(request);
                break;
            case "printRapportSituationEntrepriseTaxeVoirie":
                result = printRapportSituationEntrepriseTaxeVoirie(request);
                break;
            case "deleteNotePerception":
                result = deleteNotePerception(request);
                break;
            case "updateAccountBank":
                result = updateAccountBank(request);
                break;
            case "updateModePaiementNotePerception":
                result = updateModePaiementNotePerception(request);
                break;
            case "updateDeviseNotePerception":
                result = updateDeviseNotePerception(request);
                break;
            case "updated456":
                result = updated456(request);
                break;
            case "updated89":
                result = updated89(request);
                break;
            case "loadNotePerceptionBank":
                result = loadNotePerceptionBank(request);
                break;
            case "loadNotePerceptionBankNotUse":
                result = loadNotePerceptionBankNotUse(request);
                break;
            case "loadFirstRowNotePerceptionBank":
                result = loadFirstRowNotePerceptionBank(request);
                break;
            case "saveNotePerceptionBankForEdit":
                result = saveNotePerceptionBankForEdit(request);
                break;
            case "saveNotePerceptionBankBatch":
                result = saveNotePerceptionBankBatch(request);
                break;
            case "modifyNotePerception":
                result = modifyNotePerception(request);
                break;
            case "modifyBankNotePerception":
                result = modifyBankNotePerception(request);
                break;
            case "modifyPlageNotePerception":
                result = modifyPlageNotePerception(request);
                break;
            case "deleteNotePerceptionBank":
                result = deleteNotePerceptionBank(request);
                break;
            case "modifyVilleNotePerception":
                result = modifyVilleNotePerception(request);
                break;

        }
        out.print(result);
    }

    NotePerception notePerception;
    HashMap<String, Object[]> bulkUpdateNP;

    public String printNP(HttpServletRequest request) {

        bulkUpdateNP = new HashMap<>();
        PrintDocument printDocument = new PrintDocument();
        BigDecimal netAPayerValue, negativeNetAPayerValue;
        int counter = 0;
        boolean result;

        try {

            String numeroNP = request.getParameter(NotePerceptionConst.ParamName.NUMERO);
            String notecalcul = request.getParameter(NotePerceptionConst.ParamName.NOTE_CALCUL);
            String compteBancaire = request.getParameter(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE);
            String devise = request.getParameter(NotePerceptionConst.ParamName.DEVISE);
            String initialDevise = request.getParameter(NotePerceptionConst.ParamName.INITIAL_DEVISE);
            String timbre = request.getParameter(NotePerceptionConst.ParamName.TIMBRE);
            String papier = request.getParameter(NotePerceptionConst.ParamName.PAPIER_SECURISE);
            String netApayer = request.getParameter(NotePerceptionConst.ParamName.NET_A_PAYER);
            String tauxApplique = request.getParameter(NotePerceptionConst.ParamName.TAUX_APPLIQUE);
            String initialAmount = request.getParameter(NotePerceptionConst.ParamName.MONTANT_INITIAL);
            String receptionniste = request.getParameter(NotePerceptionConst.ParamName.RECEPTIONNISTE);
            Integer printCount = Integer.valueOf(request.getParameter(NotePerceptionConst.ParamName.PRINT_COUNT));
            Integer addEcheance = Integer.valueOf(request.getParameter(NotePerceptionConst.ParamName.ADD_ECHEANCE));

            notePerception = new NotePerception();
            SuiviComptableDeclaration suiviComptable = new SuiviComptableDeclaration();
            netAPayerValue = BigDecimal.valueOf(Float.valueOf(netApayer));
            negativeNetAPayerValue = BigDecimal.valueOf(Float.valueOf(netApayer) * -1);
            notePerception.setNumero(numeroNP);

            boolean addEchanceDate = (addEcheance == GeneralConst.Numeric.ONE);

            notePerception = NotePerceptionBusiness.getNotePerceptionByCode(notePerception.getNumero());

            if (addEchanceDate) {

                String DAY_NP_ECHEANCE = propertiesConfig.getProperty("NBRE_DAY_NP_ECHEANCE");
                Date dateEcheance = Tools.getEcheanceDate(Tools.formatStringFullToDate(notePerception.getDateCreat()), Integer.valueOf(DAY_NP_ECHEANCE));
                Personne p = IdentificationBusiness.getPersonneByCode(receptionniste);
                if (p != null) {
                    notePerception.setReceptionniste(p);
                } else {
                    notePerception.setReceptionniste(new Personne());
                }
                notePerception.setDateReception(Tools.formatDateToString(new Date()));
                notePerception.setDateEcheancePaiement(Tools.formatDateToString(dateEcheance));

            } else {
                notePerception.setReceptionniste(null);
                notePerception.setDateReception(null);
                notePerception.setDateEcheancePaiement(null);
            }

            String codeAB = notePerception.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getCode();

            if (printCount == 0) {

                notePerception.setNetAPayer(netAPayerValue);
                notePerception.setSolde(netAPayerValue);
                notePerception.setCompteBancaire(compteBancaire);
                notePerception.setTauxApplique(BigDecimal.valueOf(Float.valueOf(tauxApplique)));
                notePerception.setMontantInitial(BigDecimal.valueOf(Float.valueOf(initialAmount)));
                notePerception.setDevise(devise);
                notePerception.setPapierSecurise(papier);
                notePerception.setTimbre(timbre);

                counter++;
                bulkUpdateNP.put(counter + SQLQueryNotePerception.UPDATE_NOTE_PERCEPTION_PRINT_CASE_2,
                        new Object[]{
                            notePerception.getCompteBancaire(),
                            notePerception.getDateReception(),
                            notePerception.getReceptionniste() == null ? null : notePerception.getReceptionniste().getCode(),
                            notePerception.getTimbre(),
                            notePerception.getPapierSecurise(),
                            notePerception.getSolde(),
                            notePerception.getDevise(),
                            notePerception.getTauxApplique(),
                            notePerception.getMontantInitial(),
                            notePerception.getNetAPayer(),
                            notePerception.getDateEcheancePaiement(),
                            notePerception.getNumero()
                        });

                if (!initialDevise.equals(devise)) {

                    suiviComptable.setDebit(netAPayerValue);
                    suiviComptable.setSolde(negativeNetAPayerValue);
                    suiviComptable.setDevise(devise);

                    counter++;
                    bulkUpdateNP.put(counter + SQLQueryNotePerception.UPDATE_SUIVI_COMPTABLE_PRINT,
                            new Object[]{
                                suiviComptable.getDebit(),
                                suiviComptable.getSolde(),
                                suiviComptable.getDevise(),
                                notecalcul
                            });

                }

            }

            counter++;
            printCount++;
            suiviComptable.setNc(notecalcul);
            suiviComptable.setDebit(new BigDecimal(BigInteger.ZERO));
            suiviComptable.setCredit(new BigDecimal(BigInteger.ZERO));
            suiviComptable.setDocumentReference(notePerception.getNumero());
            suiviComptable.setDevise(devise);
            suiviComptable.setLibelleOperation(propertiesMessage.getProperty("MESSAGE_PRINT_NP").concat(GeneralConst.SPACE).concat(printCount.toString()));
            suiviComptable.setObservation(propertiesMessage.getProperty("MESSAGE_PRINT_NP_SUCCESS"));

            bulkUpdateNP.put(counter + SQLQueryGeneral.EXEC_F_NEW_SUIVI_DECLARATION,
                    new Object[]{
                        suiviComptable.getNc(),
                        suiviComptable.getDocumentReference(),
                        suiviComptable.getLibelleOperation(),
                        suiviComptable.getDebit(),
                        suiviComptable.getCredit(),
                        suiviComptable.getObservation(),
                        suiviComptable.getDevise()
                    });

            if (codeAB.equals(propertiesConfig.getProperty("CODE_TAXE_CONCENTRE"))
                    || codeAB.equals(propertiesConfig.getProperty("CODE_TAXE_VOIRIE"))) {

                counter++;

                bulkUpdateNP.put(counter + ":EXEC F_NEW_SOUS_PROVISION_MINIER ?1,?2,?3,?4,?5,?6,?7",
                        new Object[]{
                            notePerception.getNumero(),
                            notePerception.getNetAPayer(),
                            notePerception.getDevise(),
                            notePerception.getNoteCalcul().getPersonne(),
                            notePerception.getNoteCalcul().getDetailsNcList().get(0).getValeurBase(),
                            Integer.valueOf(notePerception.getAgentCreat()),
                            codeAB
                        });

            }

            result = NotePerceptionBusiness.executeQueryBulkInsert(bulkUpdateNP);

            if (result) {
                String dataReturn = printDocument.createNP(notePerception);
                return dataReturn;

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (NumberFormatException | InvocationTargetException e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public String printNPFille(HttpServletRequest request) {

        PrintDocument printDocument = new PrintDocument();
        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String NUMERO = request.getParameter(NotePerceptionConst.ParamName.NUMERO);

            notePerception = new NotePerception();

            Archive archive = NotePerceptionBusiness.getArchiveByRefDocument(NUMERO.trim());

            if (archive != null) {

                dataReturn = archive.getDocumentString();

            } else {

                notePerception = NotePerceptionBusiness.getNotePerceptionByCode(NUMERO.trim());

                if (notePerception != null) {

                    dataReturn = printDocument.createNP(notePerception);

                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (NumberFormatException | InvocationTargetException e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getNotePerception(HttpServletRequest request) {

        String valueSearch, typeSearch,
                viewAllSite, viewAllService,
                codeSite, codeService, userId, typeRegister, isAvancedSearch,
                datedebut, dateFin;

        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
            viewAllSite = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SITE);
            isAvancedSearch = request.getParameter(TaxationConst.ParamName.IS_AVANCED_SEARCH);
            viewAllService = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SERVICE);
            codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            userId = request.getParameter(TaxationConst.ParamName.USER_ID);
            typeRegister = request.getParameter(TaxationConst.ParamName.TYPE_REGISTER);
            datedebut = Tools.getValidFormat(request.getParameter(GeneralConst.ParamName.DATE_DEBUT));
            dateFin = Tools.getValidFormat(request.getParameter(GeneralConst.ParamName.DATE_FIN));

            List<NotePerception> listNotePerception;

            if (Integer.valueOf(isAvancedSearch) == 1) {

                listNotePerception = NotePerceptionBusiness.getListNotePerceptionsBySearchAvanced(codeSite, codeService, datedebut, dateFin);

            } else {

                listNotePerception = NotePerceptionBusiness.getListNotePerceptions(
                        valueSearch,
                        Integer.valueOf(typeSearch),
                        typeRegister,
                        viewAllSite.equals("true") ? Boolean.TRUE : Boolean.FALSE,
                        viewAllService.equals("true") ? Boolean.TRUE : Boolean.FALSE,
                        codeSite,
                        codeService,
                        userId);
            }

            List<JsonObject> jsonNotePerceptionList = new ArrayList<>();

            JsonObject jsonNP;

            if (listNotePerception != null && !listNotePerception.isEmpty()) {

                for (NotePerception np : listNotePerception) {

                    jsonNP = new JsonObject();

                    Personne personne;
                    Personne personneReception;
                    Adresse adresse;

                    personne = IdentificationBusiness.getPersonneByCode(np.getNoteCalcul().getPersonne().trim());

                    String codeAB = np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getCode();

                    jsonNP.addProperty("abParam", codeAB);
                    jsonNP.addProperty("npManuel", np.getNotePerceptionManuel() == null ? "" : np.getNotePerceptionManuel().toUpperCase());

                    switch (codeAB) {

                        case "00000000000001212016": //VOIRIE
                        case "00000000000001072016": //CONCENTRES

                            ComplementInfoTaxe complementInfoTaxe = TaxationBusiness.getComplementInfoTaxeByNP(np.getNumero());

                            if (complementInfoTaxe != null) {

                                jsonNP.addProperty("estTaxeMine", true);
                                jsonNP.addProperty("transiteur", complementInfoTaxe.getTransporteur().toUpperCase());
                                jsonNP.addProperty("produit", complementInfoTaxe.getNatureProduit().toUpperCase());
                                jsonNP.addProperty("tonage", np.getNoteCalcul().getDetailsNcList().get(0).getValeurBase());

                                float amountTotal = (np.getNoteCalcul().getDetailsNcList().get(0).getValeurBase().floatValue()
                                        * np.getNoteCalcul().getDetailsNcList().get(0).getTauxArticleBudgetaire().floatValue());

                                jsonNP.addProperty("amountTotal", amountTotal);
                                jsonNP.addProperty("tauxUnit", np.getNoteCalcul().getDetailsNcList().get(0).getTauxArticleBudgetaire());

                                if (codeAB.equals("00000000000001072016")) {
                                    jsonNP.addProperty("typeAB", "concentrés".toUpperCase());
                                    jsonNP.addProperty("typeAB2", "C");
                                } else {
                                    jsonNP.addProperty("typeAB", "voirie".toUpperCase());
                                    jsonNP.addProperty("typeAB2", "V");
                                }

                                jsonNP.addProperty("codeBankCfc", "");
                                jsonNP.addProperty("codeAccountBankCfc", "");

                            } else {
                                jsonNP.addProperty("estTaxeMine", true);
                                jsonNP.addProperty("transiteur", "");
                                jsonNP.addProperty("produit", "");
                                jsonNP.addProperty("tonage", "");
                                jsonNP.addProperty("tauxUnit", "");
                                jsonNP.addProperty("amountTotal", "");
                                jsonNP.addProperty("typeAB", "");
                                jsonNP.addProperty("typeAB2", "");
                                jsonNP.addProperty("codeBankCfc", "");
                                jsonNP.addProperty("codeAccountBankCfc", "");
                            }

                            break;

                        case "00000000000002392021": //FRAIS CERTIFICATION

                            ComplementFraisCertification cfc = TaxationBusiness.getComplementFraisCertificationByNC(np.getNoteCalcul().getNumero());

                            jsonNP.addProperty("codeBankCfc", cfc == null ? "" : cfc.getBanque());
                            jsonNP.addProperty("codeAccountBankCfc", cfc == null ? "" : cfc.getCompteBancaire());

                            String noteDebit = cfc.getNoteVersement().substring(0, 6);

                            jsonNP.addProperty("noteCalcul", cfc == null ? "" : noteDebit);
                            jsonNP.addProperty("nombreLot", cfc == null ? 0 : cfc.getNumeroLot());

                            if (cfc.getAgence() != null) {

                                String agence = cfc.getAgence() + " / ";
                                jsonNP.addProperty("raisonSociale", agence + personne.toString().toUpperCase());

                            } else {
                                jsonNP.addProperty("raisonSociale", personne.toString().toUpperCase());
                            }

                            break;
                        default:
                            jsonNP.addProperty("estTaxeMine", false);
                            jsonNP.addProperty("transiteur", "");
                            jsonNP.addProperty("produit", "");
                            jsonNP.addProperty("tonage", "");
                            jsonNP.addProperty("tauxUnit", "");
                            jsonNP.addProperty("amountTotal", "");
                            jsonNP.addProperty("typeAB", "");
                            jsonNP.addProperty("typeAB2", "");
                            jsonNP.addProperty("codeBankCfc", "");
                            jsonNP.addProperty("codeAccountBankCfc", "");
                            break;
                    }

                    Date dateCreateNP;
                    String dateEcheanceNP = GeneralConst.EMPTY_STRING;

                    if (np.getNoteCalcul().getFkAdressePersonne() != null
                            && !np.getNoteCalcul().getFkAdressePersonne().isEmpty()) {

                        adresse = TaxationBusiness.getAdressByCode(np.getNoteCalcul().getFkAdressePersonne().trim());

                        jsonNP.addProperty(TaxationConst.ParamName.CODE_ADRESSE_ASSUJETTI, np.getNoteCalcul().getFkAdressePersonne().trim());

                        if (adresse != null) {
                            jsonNP.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI, adresse.toString().toUpperCase().trim());
                        } else {
                            jsonNP.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI, GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        jsonNP.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI, GeneralConst.EMPTY_STRING);
                    }

                    Date dateCreate = Tools.formatStringFullToDate(np.getDateCreat());

                    jsonNP.addProperty(TaxationConst.ParamName.DATE_CREATE, Tools.formatDateToStringV2(dateCreate));
                    jsonNP.addProperty(NotePerceptionConst.ParamName.NUMERO, np.getNumero());
                    jsonNP.addProperty(TaxationConst.ParamName.CODE_SERVICE, np.getNoteCalcul().getService());
                    jsonNP.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL, np.getNoteCalcul().getExercice());

                    dateEcheanceNP = GeneralConst.EMPTY_STRING;

                    if (np.getDateEcheancePaiement() != null) {

                        dateEcheanceNP = Tools.formatDateToStringV2(Tools.formatStringFullToDate(
                                np.getDateEcheancePaiement()));
                    }

                    jsonNP.addProperty(TaxationConst.ParamName.ECHEANCE, dateEcheanceNP);

                    String libelleAB = GeneralConst.EMPTY_STRING;

                    if (!np.getNoteCalcul().getDetailsNcList().isEmpty()) {

                        Integer size = np.getNoteCalcul().getDetailsNcList().size();

                        for (int i = 0; i < size; i++) {

                            String tempAb;
                            String libelleTarif = GeneralConst.EMPTY_STRING;

                            Tarif tarif = TaxationBusiness.getTarifByCode(np.getNoteCalcul().getDetailsNcList().get(i).getTarif().trim());
                            if (tarif != null) {
                                libelleTarif = tarif.getIntitule();
                            }
                            tempAb = np.getNoteCalcul().getDetailsNcList().get(i).getArticleBudgetaire().getIntitule() + " : " + libelleTarif;
                            if (i == 0) {
                                libelleAB = tempAb;
                            } else {
                                libelleAB += GeneralConst.AB_SEPARTOR.concat(tempAb);
                            }
                        }
                    }

                    jsonNP.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE, libelleAB);
                    Service service = TaxationBusiness.getServiceByCode(np.getNoteCalcul().getService());

                    if (service != null) {
                        jsonNP.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE, service.getIntitule().toUpperCase().trim());
                    } else {
                        jsonNP.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE, GeneralConst.EMPTY_STRING);
                    }

                    personneReception = np.getReceptionniste();
                    jsonNP.addProperty(NotePerceptionConst.ParamName.CODE_ASSUJETTI, np.getNoteCalcul().getPersonne());

                    if (personne != null) {
                        jsonNP.addProperty(NotePerceptionConst.ParamName.ASSUJETTI, personne.toString().toUpperCase().trim());

                        jsonNP.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE, personne.getFormeJuridique().getCode().trim());
                        jsonNP.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE, personne.getFormeJuridique().getIntitule().toUpperCase().trim());
                        jsonNP.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());
                    } else {
                        jsonNP.addProperty(NotePerceptionConst.ParamName.ASSUJETTI, GeneralConst.EMPTY_STRING);
                        jsonNP.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE, GeneralConst.EMPTY_STRING);
                        jsonNP.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE, GeneralConst.EMPTY_STRING);
                        jsonNP.addProperty(IdentificationConst.ParamName.USER_NAME, GeneralConst.EMPTY_STRING);
                    }

                    if (personneReception != null) {
                        jsonNP.addProperty(NotePerceptionConst.ParamName.CODE_ASSUJETTI_RECEPTION, personneReception.getCode());
                        jsonNP.addProperty(NotePerceptionConst.ParamName.RECEPTIONNISTE, personneReception.toString());

                    } else {
                        jsonNP.addProperty(NotePerceptionConst.ParamName.CODE_ASSUJETTI_RECEPTION, GeneralConst.EMPTY_STRING);
                        jsonNP.addProperty(NotePerceptionConst.ParamName.RECEPTIONNISTE, GeneralConst.EMPTY_STRING);
                    }

                    jsonNP.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, np.getNetAPayer());

                    String deviseInitial = GeneralConst.EMPTY_STRING;

                    if (!np.getNoteCalcul().getDetailsNcList().isEmpty()) {
                        deviseInitial = np.getNoteCalcul().getDetailsNcList().get(0).getDevise() != null ? np.getNoteCalcul().getDetailsNcList().get(0).getDevise() : GeneralConst.Devise.DEVISE_CDF;
                        jsonNP.addProperty(NotePerceptionConst.ParamName.INITIAL_DEVISE, deviseInitial);
                    }

                    jsonNP.addProperty(NotePerceptionConst.ParamName.DEVISE, np.getDevise() == null ? deviseInitial : np.getDevise());

                    jsonNP.addProperty(NotePerceptionConst.ParamName.NOTE_CALCUL, np.getNoteCalcul().getNumero().trim());
                    jsonNP.addProperty(NotePerceptionConst.ParamName.NBR_IMPRESSION, np.getNbrImpression());

                    jsonNP.addProperty(NotePerceptionConst.ParamName.MONTANT_INITIAL, np.getDevise() == null ? np.getNetAPayer() : np.getMontantInitial());

                    if (np.getNbrImpression() > 0) {

                        jsonNP.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE, np.getCompteBancaire());
                    }

                    if (np.getFractionnee() == GeneralConst.Numeric.ONE || !np.getNotePerceptionList().isEmpty()) {

                        jsonNP.addProperty("estFractionnee", GeneralConst.Number.ONE);

                        List<NotePerception> npFilleList = np.getNotePerceptionList();
                        List<JsonObject> jsonNPFilleList = new ArrayList<>();

                        for (NotePerception npFille : npFilleList) {

                            JsonObject jsonNPFille = new JsonObject();

                            dateCreateNP = Tools.formatStringFullToDate(npFille.getDateCreat());
                            dateEcheanceNP = GeneralConst.EMPTY_STRING;

                            if (npFille.getDateEcheancePaiement() != null) {

                                dateEcheanceNP = Tools.formatDateToStringV2(Tools.formatStringFullToDate(
                                        npFille.getDateEcheancePaiement()));
                            }

                            jsonNPFille.addProperty("npMere", npFille.getNpMere().getNumero());
                            jsonNPFille.addProperty(NotePerceptionConst.ParamName.NUMERO, npFille.getNumero());
                            jsonNPFille.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT, "NOTE DE PERCEPTION FILLE");
                            jsonNPFille.addProperty(NotePerceptionConst.ParamName.DEVISE, npFille.getDevise());
                            jsonNPFille.addProperty(NotePerceptionConst.ParamName.NBR_IMPRESSION, npFille.getNbrImpression());
                            jsonNPFille.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, npFille.getNetAPayer());
                            jsonNPFille.addProperty(TaxationConst.ParamName.DATE_CREATE, Tools.formatDateToStringV2(dateCreateNP));
                            jsonNPFille.addProperty(TaxationConst.ParamName.ECHEANCE, dateEcheanceNP);

                            Journal journal = PaiementBusiness.getJournalByDocument(npFille.getNumero());

                            jsonNPFille.addProperty("isPaid", journal != null ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                            jsonNPFilleList.add(jsonNPFille);

                        }

                        jsonNP.addProperty("npFilleList", jsonNPFilleList.toString());

                    } else {
                        jsonNP.addProperty("estFractionnee", GeneralConst.Number.ZERO);
                        jsonNP.addProperty("npFilleList", GeneralConst.EMPTY_STRING);
                    }

                    //System.out.println("N° " + np.getNumero());
                    jsonNotePerceptionList.add(jsonNP);

                }
                return jsonNotePerceptionList.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public String accuserReception(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            String typeDocument = request.getParameter(GeneralConst.AccuserReception.TYPE_DOCUMENT);

            String DAY_NP_ECHEANCE = GeneralConst.EMPTY_STRING;

            switch (typeDocument) {
                case DocumentConst.DocumentCode.NP:
                    DAY_NP_ECHEANCE = propertiesMessage.getProperty(NotePerceptionConst.ParamName.NBRE_DAY_NP_ECHEANCE);
                    break;
                case DocumentConst.DocumentCode.AMR:
                    DAY_NP_ECHEANCE = propertiesMessage.getProperty(NotePerceptionConst.ParamName.NBRE_DAY_AMR_ECHEANCE);
                    break;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadMiniers(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<Personne> listPersonneMinier = TaxationBusiness.getListPersonneMinier();

            if (!listPersonneMinier.isEmpty()) {

                List<JsonObject> jsonObjectList = new ArrayList<>();

                for (Personne personneMinier : listPersonneMinier) {

                    JsonObject jsonObject = new JsonObject();

                    jsonObject.addProperty("codeMinier", personneMinier.getCode());
                    jsonObject.addProperty("nameMinier", personneMinier.toString().toUpperCase());

                    jsonObjectList.add(jsonObject);

                }

                dataReturn = jsonObjectList.toString();

            }

        } catch (Exception e) {
            dataReturn = "";
        }

        return dataReturn;
    }

    public String saveManifeste(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String inputTransporteur = request.getParameter("inputTransporteur");
            String inputNatureProduit = request.getParameter("inputNatureProduit");
            String inputNumeroPlaque = request.getParameter("inputNumeroPlaque");
            String inputTonnage = request.getParameter("inputTonnage");
            String inputMontantDu = request.getParameter("inputMontantDu");
            String isUpdateManifeste = request.getParameter("isUpdateManifeste");
            String userId = request.getParameter("userId");
            String sousProvisionID = request.getParameter("sousProvisionID");
            String manifesteID = request.getParameter("manifesteID");
            String statuSousProvision = request.getParameter("statuSousProvision");

            BigDecimal tonnage = new BigDecimal("0");
            BigDecimal amount = new BigDecimal("0");

            tonnage = tonnage.add(BigDecimal.valueOf(Double.valueOf(inputTonnage)));
            amount = amount.add(BigDecimal.valueOf(Double.valueOf(inputMontantDu)));

            if (isUpdateManifeste.equals("1")) {

                if (TaxationBusiness.updateManifesteCaseTwo(Integer.valueOf(userId), tonnage, amount, Integer.valueOf(manifesteID),
                        Integer.valueOf(sousProvisionID), statuSousProvision)) {

                    dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

                } else {

                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {

                ComplementInfoTaxe ciT = TaxationBusiness.getComplementInfoTaxeByID(Integer.valueOf(manifesteID));

                if (TaxationBusiness.updateManifesteCaseOne(inputTransporteur, inputNatureProduit, inputNumeroPlaque,
                        ciT.getModePaiement(), ciT.getFkNotePerception(), ciT.getFkNoteCalcul(),
                        Integer.valueOf(userId), tonnage, amount, Integer.valueOf(manifesteID),
                        Integer.valueOf(sousProvisionID), statuSousProvision)) {

                    dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

                } else {

                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadSousProvisionMinier(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codePersonne = request.getParameter("codePersonne");
            String periode = request.getParameter("periode");
            String datedebut = request.getParameter("datedebut");
            String datefin = request.getParameter("datefin");

            datedebut = ConvertDate.getValidFormatDateStringV2(datedebut);
            datefin = ConvertDate.getValidFormatDateStringV2(datefin);

            List<Jdossier> listSousProvision = TaxationBusiness.getListSousprovisionByMinier(codePersonne, periode, datedebut, datefin);

            if (!listSousProvision.isEmpty()) {

                List<JsonObject> jsonObjectList = new ArrayList<>();

                for (Jdossier jdossier : listSousProvision) {

                    JsonObject jsonObject = new JsonObject();
                    Personne personne = getPersonneByCode(jdossier.getDossier());

                    jsonObject.addProperty("id", jdossier.getId());
                    jsonObject.addProperty("nameMinier", personne.toString().toUpperCase());
                    jsonObject.addProperty("credit", jdossier.getCredit() == null ? 0 : jdossier.getCredit());
                    jsonObject.addProperty("debit", jdossier.getDebit() == null ? 0 : jdossier.getDebit());
                    jsonObject.addProperty("solde", jdossier.getSolde() == null ? 0 : jdossier.getSolde());
                    jsonObject.addProperty("devise", jdossier.getDevise());
                    jsonObject.addProperty("tonnageInit", jdossier.getTonnageInitial());
                    jsonObject.addProperty("montantInit", jdossier.getMontantInitial());
                    jsonObject.addProperty("tonnageRestant", jdossier.getTonnageRestant() == null ? 0 : jdossier.getTonnageRestant());
                    jsonObject.addProperty("dateCreate", ConvertDate.formatDateHeureToString(jdossier.getDateCreat()));
                    jsonObject.addProperty("dateDerniereSortie", ConvertDate.formatDateHeureToString(jdossier.getDateMaj()));
                    jsonObject.addProperty("notePerception", jdossier.getReference());

                    NotePerception notePerception = NotePerceptionBusiness.getNotePerceptionByCode(jdossier.getReference());

                    if (notePerception != null) {
                        jsonObject.addProperty("taux", notePerception.getNoteCalcul().getDetailsNcList().get(0).getTauxArticleBudgetaire());
                    } else {
                        jsonObject.addProperty("taux", 1);
                    }

                    if (jdossier.getSolde().floatValue() <= 0) {
                        jsonObject.addProperty("status", "NEGATIF");
                    } else {
                        jsonObject.addProperty("status", "POSITIF");
                    }

                    jsonObject.addProperty("taxeCode", jdossier.getFkAb());

                    if (jdossier.getFkAb().equals("00000000000001072016")) {
                        jsonObject.addProperty("taxeName", "concentré".toUpperCase());
                    } else if (jdossier.getFkAb().equals("00000000000001212016")) {
                        jsonObject.addProperty("taxeName", "VOIRIE");
                    } else {
                        jsonObject.addProperty("taxeName", "");
                    }

                    List<ComplementInfoTaxe> listComplementInfoTaxe = TaxationBusiness.getListComplementInfoTaxeByNP(jdossier.getReference().trim());

                    if (!listComplementInfoTaxe.isEmpty()) {

                        List<JsonObject> jsonComplementInfoTaxeList = new ArrayList<>();

                        for (ComplementInfoTaxe complementInfoTaxe : listComplementInfoTaxe) {

                            JsonObject jsonComplementInfoTaxe = new JsonObject();

                            jsonComplementInfoTaxe.addProperty("numeroNP", jdossier.getReference());
                            jsonComplementInfoTaxe.addProperty("sousProvisionID", jdossier.getId());
                            jsonComplementInfoTaxe.addProperty("devise", jdossier.getDevise());
                            jsonComplementInfoTaxe.addProperty("manifestId", complementInfoTaxe.getId());
                            jsonComplementInfoTaxe.addProperty("transporteur", complementInfoTaxe.getTransporteur().toUpperCase());
                            jsonComplementInfoTaxe.addProperty("produit", complementInfoTaxe.getNatureProduit().toUpperCase());
                            jsonComplementInfoTaxe.addProperty("plaque", complementInfoTaxe.getNumeroPlaque().toUpperCase());
                            jsonComplementInfoTaxe.addProperty("modePaiement", complementInfoTaxe.getModePaiement().toUpperCase());
                            jsonComplementInfoTaxe.addProperty("status", complementInfoTaxe.getStatusManifeste() == null ? "" : complementInfoTaxe.getStatusManifeste());

                            jsonComplementInfoTaxe.addProperty("tonnageSortie", complementInfoTaxe.getTonnageSortie() == null ? 0 : complementInfoTaxe.getTonnageSortie());
                            jsonComplementInfoTaxe.addProperty("montantTonnageSortie", complementInfoTaxe.getMontantTonnageSortie() == null ? 0 : complementInfoTaxe.getMontantTonnageSortie());

                            String dateValidation = GeneralConst.EMPTY_STRING;

                            if (complementInfoTaxe.getAgentValidate() != null) {

                                Agent agentValidate = new Agent();
                                agentValidate = GeneralBusiness.getAgentByCode(complementInfoTaxe.getAgentValidate() + "");

                                jsonComplementInfoTaxe.addProperty("agentValidate", agentValidate.toString().toUpperCase());

                            } else {
                                jsonComplementInfoTaxe.addProperty("agentValidate", "");
                            }

                            if (complementInfoTaxe.getDateValidation() != null) {

                                dateValidation = ConvertDate.formatDateHeureToString(complementInfoTaxe.getDateValidation());

                                jsonComplementInfoTaxe.addProperty("dateValidation", dateValidation);

                            } else {
                                jsonComplementInfoTaxe.addProperty("dateValidation", "");

                            }

                            jsonComplementInfoTaxeList.add(jsonComplementInfoTaxe);
                        }

                        jsonObject.addProperty("complementList", jsonComplementInfoTaxeList.toString());

                    } else {
                        jsonObject.addProperty("complementExist", GeneralConst.Number.ZERO);
                        jsonObject.addProperty("complementList", "");
                    }

                    jsonObjectList.add(jsonObject);

                }

                dataReturn = jsonObjectList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String printRapportSituationEntrepriseTaxeConcentre(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codePersonne = request.getParameter("codePersonne");
            String periode = request.getParameter("periode");
            String datedebut = request.getParameter("datedebut");
            String datefin = request.getParameter("datefin");

            datedebut = ConvertDate.getValidFormatDateStringV2(datedebut);
            datefin = ConvertDate.getValidFormatDateStringV2(datefin);

            List<Jdossier> listSousProvision = TaxationBusiness.getListSousprovisionByMinierByTaxeCodeV2(
                    propertiesConfig.getProperty("CODE_TAXE_CONCENTRE"), codePersonne, periode, datedebut, datefin);

            if (!listSousProvision.isEmpty()) {

                List<JsonObject> jsonComplementInfoTaxeList = new ArrayList<>();

                for (Jdossier jdossier : listSousProvision) {

                    List<ComplementInfoTaxe> listComplementInfoTaxe = TaxationBusiness.getListComplementInfoTaxeByMinierAndTaxe(
                            jdossier.getDossier(), propertiesConfig.getProperty("CODE_TAXE_CONCENTRE"));

                    if (!listComplementInfoTaxe.isEmpty()) {

                        for (ComplementInfoTaxe complementInfoTaxe : listComplementInfoTaxe) {

                            JsonObject jsonComplementInfoTaxe = new JsonObject();
                            Personne personne = getPersonneByCode(jdossier.getDossier());

                            jsonComplementInfoTaxe.addProperty("nameMinier", personne.toString().toUpperCase());
                            jsonComplementInfoTaxe.addProperty("devise", jdossier.getDevise());
                            jsonComplementInfoTaxe.addProperty("transiteur", complementInfoTaxe.getTransporteur().toUpperCase());
                            jsonComplementInfoTaxe.addProperty("taxe", "concentrée".toUpperCase());
                            jsonComplementInfoTaxe.addProperty("tonnage", complementInfoTaxe.getTonnageSortie() == null ? 0 : complementInfoTaxe.getTonnageSortie());
                            jsonComplementInfoTaxe.addProperty("observation", complementInfoTaxe.getStatusManifeste() == null ? "" : complementInfoTaxe.getStatusManifeste());

                            DetailsNc detailsNc = ContentieuxBusiness.getDetailNcByNc(complementInfoTaxe.getFkNoteCalcul());

                            jsonComplementInfoTaxe.addProperty("taux", detailsNc.getTauxArticleBudgetaire());
                            jsonComplementInfoTaxe.addProperty("montantDu", complementInfoTaxe.getMontantTonnageSortie() == null ? 0 : complementInfoTaxe.getMontantTonnageSortie());

                            jsonComplementInfoTaxeList.add(jsonComplementInfoTaxe);
                        }

                    }
                }

                dataReturn = jsonComplementInfoTaxeList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String printRapportSituationEntrepriseTaxeVoirie(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codePersonne = request.getParameter("codePersonne");
            String periode = request.getParameter("periode");
            String datedebut = request.getParameter("datedebut");
            String datefin = request.getParameter("datefin");

            datedebut = ConvertDate.getValidFormatDateStringV2(datedebut);
            datefin = ConvertDate.getValidFormatDateStringV2(datefin);

            List<Jdossier> listSousProvision = TaxationBusiness.getListSousprovisionByMinierByTaxeCodeV2(
                    propertiesConfig.getProperty("CODE_TAXE_CONCENTRE"), codePersonne, periode, datedebut, datefin);

            /*List<Jdossier> listSousProvision = TaxationBusiness.getListSousprovisionByMinierByTaxeCode(
             propertiesConfig.getProperty("CODE_TAXE_VOIRIE"));*/
            if (!listSousProvision.isEmpty()) {

                List<JsonObject> jsonComplementInfoTaxeList = new ArrayList<>();

                for (Jdossier jdossier : listSousProvision) {

                    List<ComplementInfoTaxe> listComplementInfoTaxe = TaxationBusiness.getListComplementInfoTaxeByMinierAndTaxe(
                            jdossier.getDossier(), propertiesConfig.getProperty("CODE_TAXE_VOIRIE"));

                    if (!listComplementInfoTaxe.isEmpty()) {

                        for (ComplementInfoTaxe complementInfoTaxe : listComplementInfoTaxe) {

                            JsonObject jsonComplementInfoTaxe = new JsonObject();
                            Personne personne = getPersonneByCode(jdossier.getDossier());

                            jsonComplementInfoTaxe.addProperty("nameMinier", personne.toString().toUpperCase());
                            jsonComplementInfoTaxe.addProperty("devise", jdossier.getDevise());
                            jsonComplementInfoTaxe.addProperty("transiteur", complementInfoTaxe.getTransporteur().toUpperCase());
                            jsonComplementInfoTaxe.addProperty("taxe", "voirie".toUpperCase());
                            jsonComplementInfoTaxe.addProperty("tonnage", complementInfoTaxe.getTonnageSortie() == null ? 0 : complementInfoTaxe.getTonnageSortie());
                            jsonComplementInfoTaxe.addProperty("observation", complementInfoTaxe.getStatusManifeste() == null ? "" : complementInfoTaxe.getStatusManifeste());

                            DetailsNc detailsNc = ContentieuxBusiness.getDetailNcByNc(complementInfoTaxe.getFkNoteCalcul());

                            jsonComplementInfoTaxe.addProperty("taux", detailsNc.getTauxArticleBudgetaire());
                            jsonComplementInfoTaxe.addProperty("montantDu", complementInfoTaxe.getMontantTonnageSortie() == null ? 0 : complementInfoTaxe.getMontantTonnageSortie());

                            jsonComplementInfoTaxeList.add(jsonComplementInfoTaxe);
                        }

                    }
                }

                dataReturn = jsonComplementInfoTaxeList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String deleteNotePerception(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String numero = request.getParameter("numero");
            String userId = request.getParameter("userId");

            Journal journal = PaiementBusiness.getJournalByDocumentForAllState(numero);

            if (journal == null) {

                NotePerception notePerception = NotePerceptionBusiness.getNotePerceptionByCode(numero);

                if (notePerception != null) {

                    int saveActivate = Integer.valueOf(propertiesConfig.getProperty("SAVE_LOG_NOTE_TAXATION"));

                    if (saveActivate == 1) {

                        logNoteTaxation = new LogNoteTaxation();
                        Agent agent = GeneralBusiness.getAgentByCode(userId);

                        logNoteTaxation.setCodeAgent(agent.getCode());
                        logNoteTaxation.setNomAgent(agent.toString().toUpperCase());
                        logNoteTaxation.setNoteTaxation(numero);
                        logNoteTaxation.setAction("Suppresion de la note de perception");

                        String observation = "Suppresion de la note de perception n° ".concat(numero);

                        logNoteTaxation.setObservation(observation);
                    }

                    if (NotePerceptionBusiness.deleteNotePerception(Integer.valueOf(numero), Integer.valueOf(userId), saveActivate, logNoteTaxation)) {
                        dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }

                } else {
                    dataReturn = "2";
                }
            } else {
                dataReturn = "3";
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String updateAccountBank(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String numero = request.getParameter("numero");
            String userId = request.getParameter("userId");
            String compteBancaire = request.getParameter("compteBancaire");
            String banque = request.getParameter("banque");

            Journal journal = PaiementBusiness.getJournalByDocumentForAllState(numero);

            if (journal == null) {

                NotePerception notePerception = NotePerceptionBusiness.getNotePerceptionByCode(numero);

                if (notePerception != null) {

                    int saveActivate = Integer.valueOf(propertiesConfig.getProperty("SAVE_LOG_NOTE_TAXATION"));

                    if (saveActivate == 1) {

                        logNoteTaxation = new LogNoteTaxation();

                        Agent agent = GeneralBusiness.getAgentByCode(userId);
                        CompteBancaire cb = GeneralBusiness.getCompteBancaireByCode(notePerception.getCompteBancaire());
                        CompteBancaire cbNew = GeneralBusiness.getCompteBancaireByCode(compteBancaire);

                        logNoteTaxation.setCodeAgent(agent.getCode());
                        logNoteTaxation.setNomAgent(agent.toString().toUpperCase());
                        logNoteTaxation.setNoteTaxation(numero);
                        logNoteTaxation.setAction("Modification du compte bancaire");

                        String oldAccountBankInfo = "Compte Bancaire initial : ".concat(" ").concat(cb.getIntitule().toUpperCase().concat(" <br/> Banque : ").concat(cb.getBanque().getIntitule().toUpperCase()));
                        String newAccountBankInfo = "Compte Bancaire nouveau : ".concat(" ").concat(cbNew.getIntitule().toUpperCase().concat(" <br/> Banque : ").concat(cbNew.getBanque().getIntitule().toUpperCase()));

                        String infoAccountBank = oldAccountBankInfo + "<br/>" + newAccountBankInfo;

                        logNoteTaxation.setObservation(infoAccountBank);
                    }

                    if (NotePerceptionBusiness.updateAccountBankNP(numero, Integer.valueOf(userId), banque, compteBancaire, saveActivate,
                            logNoteTaxation)) {

                        dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }

                } else {
                    dataReturn = "2";
                }
            } else {
                dataReturn = "3";
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String updateModePaiementNotePerception(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String numero = request.getParameter("numero");
            String userId = request.getParameter("userId");
            String modePaiementNote = request.getParameter("modePaiementNote");

            Journal journal = PaiementBusiness.getJournalByDocumentForAllState(numero);

            if (journal == null) {

                NotePerception notePerception = NotePerceptionBusiness.getNotePerceptionByCode(numero);

                if (notePerception != null) {

                    String oldModePaiement = "";
                    String newModePaiement = "";
                    String observation = "";

                    String codeAB = notePerception.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getCode();

                    switch (codeAB) {

                        case "00000000000001212016": //VOIRIE
                        case "00000000000001072016": //CONCENTRE

                            ComplementInfoTaxe complementInfoTaxe = TaxationBusiness.getComplementInfoTaxeByNP(numero);

                            oldModePaiement = "Mode paiement initial : ".concat(complementInfoTaxe == null ? "" : complementInfoTaxe.getModePaiement());
                            newModePaiement = "Mode paiement actuel : ".concat(modePaiementNote);
                            observation = oldModePaiement + "<br/>" + newModePaiement;

                            int saveActivate = Integer.valueOf(propertiesConfig.getProperty("SAVE_LOG_NOTE_TAXATION"));

                            if (saveActivate == 1) {

                                logNoteTaxation = new LogNoteTaxation();
                                Agent agent = GeneralBusiness.getAgentByCode(userId);

                                logNoteTaxation.setCodeAgent(agent.getCode());
                                logNoteTaxation.setNomAgent(agent.toString().toUpperCase());
                                logNoteTaxation.setNoteTaxation(numero);
                                logNoteTaxation.setAction("Modification du mode de paiement de la note de perception");

                                logNoteTaxation.setObservation(observation);

                                if (NotePerceptionBusiness.updateModePaiementNotePerception(saveActivate, logNoteTaxation)) {

                                    dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

                                } else {
                                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                                }
                            }

                            break;

                        default:
                            dataReturn = "4";
                            break;

                    }

                } else {
                    dataReturn = "2";
                }
            } else {
                dataReturn = "3";
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String updateDeviseNotePerception(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String numero = request.getParameter("numero");
            String userId = request.getParameter("userId");
            String deviseNote = request.getParameter("deviseNote");

            Journal journal = PaiementBusiness.getJournalByDocumentForAllState(numero);

            if (journal == null) {

                NotePerception notePerception = NotePerceptionBusiness.getNotePerceptionByCode(numero);

                if (notePerception != null) {

                    if (deviseNote.equals(notePerception.getDevise())) {

                        dataReturn = "4";

                    } else {

                        Taux taux = RecouvrementBusiness.getTauxByDevise(deviseNote);
                        BigDecimal amountConvert = new BigDecimal("0");

                        if (deviseNote.equals(GeneralConst.Devise.DEVISE_USD)) {
                            amountConvert = amountConvert.add(notePerception.getNetAPayer().divide(taux.getTaux()));
                        } else {
                            amountConvert = amountConvert.add(notePerception.getNetAPayer().multiply(taux.getTaux()));
                        }

                        int saveActivate = Integer.valueOf(propertiesConfig.getProperty("SAVE_LOG_NOTE_TAXATION"));

                        if (saveActivate == 1) {

                            logNoteTaxation = new LogNoteTaxation();

                            Agent agent = GeneralBusiness.getAgentByCode(userId);

                            logNoteTaxation.setCodeAgent(agent.getCode());
                            logNoteTaxation.setNomAgent(agent.toString().toUpperCase());
                            logNoteTaxation.setNoteTaxation(numero);
                            logNoteTaxation.setAction("Modification de la devise de la note de perception");

                            String oldDevise = "Devise initiale : ".concat(notePerception.getDevise());
                            String newDevise = "Devise actuelle : ".concat(deviseNote);

                            String observation = oldDevise + "<br/>" + newDevise;

                            logNoteTaxation.setObservation(observation);
                        }

                        if (NotePerceptionBusiness.updateDeviseNoteTaxation(numero, deviseNote,
                                Integer.valueOf(userId), amountConvert, saveActivate, logNoteTaxation)) {

                            dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

                        } else {

                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;

                        }
                    }

                } else {
                    dataReturn = "2";
                }
            } else {
                dataReturn = "3";
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String updated456(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String numero = request.getParameter("numero");
            String userId = request.getParameter("userId");
            String value456 = request.getParameter("value456");
            String typeValueSearch = request.getParameter("typeValueSearch");

            Journal journal = PaiementBusiness.getJournalByDocumentForAllState(numero);
            logNoteTaxation = new LogNoteTaxation();

            if (journal == null) {

                NotePerception notePerception = NotePerceptionBusiness.getNotePerceptionByCode(numero);

                if (notePerception != null) {

                    String beginValue = "";
                    String actualyValue = "";

                    String codeAB = notePerception.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getCode();

                    ComplementFraisCertification cfc = new ComplementFraisCertification();
                    ComplementInfoTaxe cif = new ComplementInfoTaxe();

                    String sqlQuery = "";

                    switch (typeValueSearch) {

                        case "4":

                            if (codeAB.equals("00000000000001212016") || codeAB.equals("00000000000001072016")) {

                                cif = TaxationBusiness.getComplementInfoTaxeByNP(numero);

                                beginValue = "Agence initiale : ".concat(cif == null ? "" : cif.getTransporteur().toUpperCase());
                                actualyValue = "Agence actuelle : ".concat(value456.toUpperCase());

                                sqlQuery = ":UPDATE T_COMPLEMENT_INFO_TAXE SET TRANSPORTEUR = '%s' WHERE FK_NOTE_PERCEPTION = '%s'";
                                sqlQuery = String.format(sqlQuery, value456.toUpperCase(), numero);

                            } else if (codeAB.equals("00000000000002392021")) {

                                cfc = TaxationBusiness.getComplementFraisCertificationByNC(notePerception.getNoteCalcul().getNumero());

                                beginValue = "Agence initiale : ".concat(cfc == null ? "" : cfc.getAgence() == null ? "" : cfc.getAgence().toUpperCase().toUpperCase());
                                actualyValue = "Agence actuelle : ".concat(value456.toUpperCase());

                                sqlQuery = ":UPDATE T_COMPLEMENT_FRAIS_CERTIFICATION SET AGENCE = '%s' WHERE ID = %s";
                                sqlQuery = String.format(sqlQuery, value456.toUpperCase(), cfc.getId());

                            }

                            logNoteTaxation.setAction("Modification de l'agence");

                            break;
                        case "5":

                            logNoteTaxation.setAction("Modification de la nature du produit");

                            if (codeAB.equals("00000000000001212016") || codeAB.equals("00000000000001072016")) {

                                cif = TaxationBusiness.getComplementInfoTaxeByNP(numero);

                                beginValue = "Produit initial : ".concat(cif == null ? "" : cif.getNatureProduit().toUpperCase());
                                actualyValue = "Produit actuel : ".concat(value456.toUpperCase());

                                sqlQuery = ":UPDATE T_COMPLEMENT_INFO_TAXE SET NATURE_PRODUIT = '%s' WHERE FK_NOTE_PERCEPTION = '%s'";
                                sqlQuery = String.format(sqlQuery, value456.toUpperCase(), numero);

                            } else if (codeAB.equals("00000000000002392021")) {

                                cfc = TaxationBusiness.getComplementFraisCertificationByNC(notePerception.getNoteCalcul().getNumero());

                                beginValue = "Agence initiale : ".concat(cfc == null ? "" : cfc.getAgence() == null ? "" : cfc.getAgence().toUpperCase().toUpperCase());
                                actualyValue = "Montant actuel : ".concat(value456.toUpperCase());

                                sqlQuery = ":UPDATE T_COMPLEMENT_FRAIS_CERTIFICATION SET PRODUIT = '%s' WHERE ID = %s";
                                sqlQuery = String.format(sqlQuery, value456.toUpperCase(), cfc.getId());

                            }

                            break;
                        case "6":

                            if (codeAB.equals("00000000000001212016") || codeAB.equals("00000000000001072016")) {

                                logNoteTaxation.setAction("Modification du numéro de plaque");

                                cif = TaxationBusiness.getComplementInfoTaxeByNP(numero);

                                beginValue = "Plaque initiale : ".concat(cif == null ? "" : cif.getNumeroPlaque() == null ? "" : cif.getNumeroPlaque().toUpperCase());
                                actualyValue = "Plaque actuelle : ".concat(value456.toUpperCase());

                                sqlQuery = ":UPDATE T_COMPLEMENT_INFO_TAXE SET NUMERO_PLAQUE = '%s' WHERE FK_NOTE_PERCEPTION = '%s'";
                                sqlQuery = String.format(sqlQuery, value456.toUpperCase(), numero);

                            }

                            break;
                    }

                    int saveActivate = Integer.valueOf(propertiesConfig.getProperty("SAVE_LOG_NOTE_TAXATION"));

                    if (saveActivate == 1) {

                        Agent agent = GeneralBusiness.getAgentByCode(userId);

                        logNoteTaxation.setCodeAgent(agent.getCode());
                        logNoteTaxation.setNomAgent(agent.toString().toUpperCase());
                        logNoteTaxation.setNoteTaxation(numero);

                        String observation = beginValue + "<br/>" + actualyValue;

                        logNoteTaxation.setObservation(observation);
                    }

                    if (NotePerceptionBusiness.updated456(sqlQuery, saveActivate, logNoteTaxation)) {

                        dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

                    } else {

                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;

                    }

                } else {
                    dataReturn = "2";
                }
            } else {
                dataReturn = "3";
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadNotePerceptionBank(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<NotePerceptionBanque> listNotePerceptionBanque = NotePerceptionBusiness.getListAllNotePerceptionBanque();

            if (!listNotePerceptionBanque.isEmpty()) {

                List<JsonObject> jsonObjList = new ArrayList<>();

                for (NotePerceptionBanque npb : listNotePerceptionBanque) {

                    JsonObject jsonObject = new JsonObject();

                    Agent agentMaj = PaiementBusiness.getAgentByCode(npb.getAgentMaj());

                    String nameAgentMaj = "";
                    String siteAgentMaj = "";
                    String infoAgenMajComposite = "";

                    if (agentMaj != null) {

                        nameAgentMaj = agentMaj.toString().toUpperCase();

                        if (agentMaj.getSite() != null) {

                            siteAgentMaj = "(Bureau : ".concat(agentMaj.getSite().getIntitule().toUpperCase()) + ")";

                            infoAgenMajComposite = nameAgentMaj.concat(" ").concat(siteAgentMaj);
                        }
                    }

                    cd.hologram.erecettesvg.models.Banque banque = GeneralBusiness.getBanqueByCode(npb.getFkBanque());

                    NotePerceptionVignette notePerceptionVignette = TaxationBusiness.getNotePerceptionVignetteByFkNoteBank(npb.getId());

                    jsonObject.addProperty("id", npb.getId());
                    jsonObject.addProperty("nameAgentMaj", infoAgenMajComposite);
                    jsonObject.addProperty("notePerception", npb.getNotePerception().toUpperCase());
                    jsonObject.addProperty("banqueCode", npb.getFkBanque());
                    jsonObject.addProperty("banqueName", banque.getIntitule().toUpperCase());
                    jsonObject.addProperty("plageDebut", npb.getPlageDebut().toUpperCase());
                    jsonObject.addProperty("plageFin", npb.getPlageFin().toUpperCase());

                    jsonObject.addProperty("nbreImpression", npb.getNbreImpression());

                    jsonObject.addProperty("noteTaxation", npb.getNoteTaxation() == null ? "--" : npb.getNoteTaxation());
                    jsonObject.addProperty("estUtilise", npb.getNoteTaxation() == null ? 0 : 1);
                    jsonObject.addProperty("dateCreate", ConvertDate.formatDateHeureToStringV2(npb.getDateCreate()));
                    jsonObject.addProperty("dateUtilisation", notePerceptionVignette == null ? "" : ConvertDate.formatDateHeureToStringV2(notePerceptionVignette.getDateImpression()));

                    EntiteAdministrative ea = null;

                    if (npb.getFkEa() != null) {

                        //ea = IdentificationBusiness.getEntiteAdministrativeByCode(npb.getFkEa());
                        jsonObject.addProperty("villeCode", npb.getFkEa());

                        switch (npb.getFkEa()) {

                            case "00000000000429052016":
                                jsonObject.addProperty("villeName", "LUBUMBASHI");
                                break;
                            case "00000000000428162016":
                                jsonObject.addProperty("villeName", "Likasi".toUpperCase());
                                break;
                            case "00000000000428392016":
                                jsonObject.addProperty("villeName", "Kasumbalesa".toUpperCase());
                                break;
                            case "00000000000428382016":
                                jsonObject.addProperty("villeName", "Kipushi".toUpperCase());
                                break;
                            default:
                                jsonObject.addProperty("villeName", "");
                                break;
                        }

                    } else {
                        jsonObject.addProperty("villeCode", "");
                        jsonObject.addProperty("villeName", "");
                    }

                    jsonObjList.add(jsonObject);
                }

                dataReturn = jsonObjList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadNotePerceptionBankNotUse(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<NotePerceptionBanque> listNotePerceptionBanque = NotePerceptionBusiness.getListAllNotePerceptionBanqueNotUse();

            if (!listNotePerceptionBanque.isEmpty()) {

                List<JsonObject> jsonObjList = new ArrayList<>();

                for (NotePerceptionBanque npb : listNotePerceptionBanque) {

                    JsonObject jsonObject = new JsonObject();

                    cd.hologram.erecettesvg.models.Banque banque = GeneralBusiness.getBanqueByCode(npb.getFkBanque());

                    NotePerceptionVignette notePerceptionVignette = TaxationBusiness.getNotePerceptionVignetteByFkNoteBank(npb.getId());

                    jsonObject.addProperty("id", npb.getId());
                    jsonObject.addProperty("nameAgentMaj", "");
                    jsonObject.addProperty("notePerception", npb.getNotePerception().toUpperCase());
                    jsonObject.addProperty("banqueCode", npb.getFkBanque());
                    jsonObject.addProperty("banqueName", banque.getIntitule().toUpperCase());
                    jsonObject.addProperty("plageDebut", npb.getPlageDebut().toUpperCase());
                    jsonObject.addProperty("plageFin", npb.getPlageFin().toUpperCase());

                    jsonObject.addProperty("nbreImpression", npb.getNbreImpression());

                    jsonObject.addProperty("noteTaxation", npb.getNoteTaxation() == null ? "--" : npb.getNoteTaxation());
                    jsonObject.addProperty("estUtilise", npb.getNoteTaxation() == null ? 0 : 1);
                    jsonObject.addProperty("dateCreate", ConvertDate.formatDateHeureToStringV2(npb.getDateCreate()));
                    jsonObject.addProperty("dateUtilisation", notePerceptionVignette == null ? "" : ConvertDate.formatDateHeureToStringV2(notePerceptionVignette.getDateImpression()));

                    EntiteAdministrative ea = null;

                    if (npb.getFkEa() != null) {

                        //ea = IdentificationBusiness.getEntiteAdministrativeByCode(npb.getFkEa());
                        jsonObject.addProperty("villeCode", npb.getFkEa());

                        switch (npb.getFkEa()) {

                            case "00000000000429052016":
                                jsonObject.addProperty("villeName", "LUBUMBASHI");
                                break;
                            case "00000000000428162016":
                                jsonObject.addProperty("villeName", "Likasi".toUpperCase());
                                break;
                            case "00000000000428392016":
                                jsonObject.addProperty("villeName", "Kasumbalesa".toUpperCase());
                                break;
                            case "00000000000428382016":
                                jsonObject.addProperty("villeName", "Kipushi".toUpperCase());
                                break;
                            default:
                                jsonObject.addProperty("villeName", "");
                                break;
                        }

                    } else {
                        jsonObject.addProperty("villeCode", "");
                        jsonObject.addProperty("villeName", "");
                    }

                    jsonObjList.add(jsonObject);
                }

                dataReturn = jsonObjList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadFirstRowNotePerceptionBank(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<NotePerceptionBanque> listNotePerceptionBanque = NotePerceptionBusiness.getListFirstNotePerceptionBanque();

            if (!listNotePerceptionBanque.isEmpty()) {

                List<JsonObject> jsonObjList = new ArrayList<>();

                for (NotePerceptionBanque npb : listNotePerceptionBanque) {

                    JsonObject jsonObject = new JsonObject();

                    cd.hologram.erecettesvg.models.Banque banque = GeneralBusiness.getBanqueByCode(npb.getFkBanque());

                    NotePerceptionVignette notePerceptionVignette = TaxationBusiness.getNotePerceptionVignetteByFkNoteBank(npb.getId());

                    jsonObject.addProperty("id", npb.getId());
                    jsonObject.addProperty("nameAgentMaj", "");
                    jsonObject.addProperty("notePerception", npb.getNotePerception().toUpperCase());
                    jsonObject.addProperty("banqueCode", npb.getFkBanque());
                    jsonObject.addProperty("banqueName", banque.getIntitule().toUpperCase());
                    jsonObject.addProperty("plageDebut", npb.getPlageDebut().toUpperCase());
                    jsonObject.addProperty("plageFin", npb.getPlageFin().toUpperCase());

                    jsonObject.addProperty("nbreImpression", npb.getNbreImpression());

                    jsonObject.addProperty("noteTaxation", npb.getNoteTaxation() == null ? "--" : npb.getNoteTaxation());
                    jsonObject.addProperty("estUtilise", npb.getNoteTaxation() == null ? 0 : 1);
                    jsonObject.addProperty("dateCreate", ConvertDate.formatDateHeureToStringV2(npb.getDateCreate()));
                    jsonObject.addProperty("dateUtilisation", notePerceptionVignette == null ? "" : ConvertDate.formatDateHeureToStringV2(notePerceptionVignette.getDateImpression()));

                    EntiteAdministrative ea = null;

                    if (npb.getFkEa() != null) {

                        //ea = IdentificationBusiness.getEntiteAdministrativeByCode(npb.getFkEa());
                        jsonObject.addProperty("villeCode", npb.getFkEa());

                        switch (npb.getFkEa()) {

                            case "00000000000429052016":
                                jsonObject.addProperty("villeName", "LUBUMBASHI");
                                break;
                            case "00000000000428162016":
                                jsonObject.addProperty("villeName", "Likasi".toUpperCase());
                                break;
                            case "00000000000428392016":
                                jsonObject.addProperty("villeName", "Kasumbalesa".toUpperCase());
                                break;
                            case "00000000000428382016":
                                jsonObject.addProperty("villeName", "Kipushi".toUpperCase());
                                break;
                            default:
                                jsonObject.addProperty("villeName", "");
                                break;
                        }

                    } else {
                        jsonObject.addProperty("villeCode", "");
                        jsonObject.addProperty("villeName", "");
                    }

                    jsonObjList.add(jsonObject);
                }

                dataReturn = jsonObjList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String updated89(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String numero = request.getParameter("numero");
            String userId = request.getParameter("userId");
            String valueQteOrLot = request.getParameter("valueQteOrLot");
            String typeValueSearch = request.getParameter("typeValueSearch");

            boolean isTaxCfc = false;

            Journal journal = PaiementBusiness.getJournalByDocumentForAllState(numero);

            if (journal == null) {

                NotePerception notePerception = NotePerceptionBusiness.getNotePerceptionByCode(numero);

                if (notePerception != null) {

                    String beginValue = "";
                    String actualyValue = "";

                    BigDecimal lotUnit = new BigDecimal("37.5");
                    BigDecimal numberLot = new BigDecimal(valueQteOrLot);
                    BigDecimal valBase = new BigDecimal("0");
                    BigDecimal amountTotal = new BigDecimal("0");

                    valBase = valBase.add(notePerception.getNoteCalcul().getDetailsNcList().get(0).getValeurBase());

                    String dncCode = notePerception.getNoteCalcul().getDetailsNcList().get(0).getId();

                    ComplementFraisCertification cfc = new ComplementFraisCertification();
                    ComplementInfoTaxe cif = new ComplementInfoTaxe();

                    String sqlQuery = "";

                    switch (typeValueSearch) {

                        case "9":

                            cif = TaxationBusiness.getComplementInfoTaxeByNP(numero);

                            beginValue = "Tonnage initial : ".concat(cif == null ? "" : cif.getTransporteur().toUpperCase());
                            actualyValue = "Tonnage actuel : ".concat(valueQteOrLot);

                            sqlQuery = ":UPDATE T_DETAILS_NC SET VALEUR_BASE = %s, MONTANT_DU = %s WHERE ID = '%s'";
                            sqlQuery = String.format(sqlQuery, valueQteOrLot, dncCode);

                            logNoteTaxation.setAction("Modification du tonnage");

                            break;

                        case "8":
                            isTaxCfc = true;
                            logNoteTaxation.setAction("Modification du nombre de lot");

                            amountTotal = amountTotal.add(numberLot.multiply(lotUnit));

                            cfc = TaxationBusiness.getComplementFraisCertificationByNC(notePerception.getNoteCalcul().getNumero());

                            beginValue = "Nombre lot initial : ".concat(cfc == null ? "" : cfc.getNumeroLot() == null ? "" : cfc.getNumeroLot() + "");
                            actualyValue = "Nombre lot actuel : ".concat(valueQteOrLot);

                            sqlQuery = ":UPDATE T_COMPLEMENT_FRAIS_CERTIFICATION SET NOMBRE_LOT = %s, COTE_PART = %s WHERE ID = %s";
                            sqlQuery = String.format(sqlQuery, Integer.valueOf(valueQteOrLot), amountTotal, cfc.getId());

                            break;

                    }

                    int saveActivate = Integer.valueOf(propertiesConfig.getProperty("SAVE_LOG_NOTE_TAXATION"));

                    if (saveActivate == 1) {

                        logNoteTaxation = new LogNoteTaxation();
                        Agent agent = GeneralBusiness.getAgentByCode(userId);

                        logNoteTaxation.setCodeAgent(agent.getCode());
                        logNoteTaxation.setNomAgent(agent.toString().toUpperCase());
                        logNoteTaxation.setNoteTaxation(numero);

                        String observation = beginValue + "<br/>" + actualyValue;

                        logNoteTaxation.setObservation(observation);
                    }

                    if (NotePerceptionBusiness.updated89(sqlQuery, saveActivate, logNoteTaxation, isTaxCfc)) {

                        dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

                    } else {

                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;

                    }

                } else {
                    dataReturn = "2";
                }
            } else {
                dataReturn = "3";
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    private String saveNotePerceptionBankForEdit(HttpServletRequest request) {

        String result = "";

        try {

            String notePerception = request.getParameter("notePerception");
            String banque = request.getParameter("banque");
            String plageDebut = request.getParameter("plageDebut");
            String plageFin = request.getParameter("plageFin");
            String userId = request.getParameter("userId");
            String ville = request.getParameter("ville");

            NotePerceptionBanque notePerceptionBanque = NotePerceptionBusiness.getNotePerceptionBanqueByNp(
                    notePerception.trim());

            if (notePerceptionBanque != null) {

                return result = "2";

            } else {

                notePerceptionBanque = new NotePerceptionBanque();

                notePerceptionBanque.setNotePerception(notePerception.toUpperCase().trim());
                notePerceptionBanque.setFkBanque(banque.trim());
                notePerceptionBanque.setPlageDebut(plageDebut.trim());
                notePerceptionBanque.setPlageFin(plageFin.trim());
                notePerceptionBanque.setAgentCreate(Integer.valueOf(userId));
                notePerceptionBanque.setFkEa(ville);

                if (NotePerceptionBusiness.saveNotePerceptionBanqueForEdit(notePerceptionBanque)) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String saveNotePerceptionBankBatch(HttpServletRequest request) {

        String result = "";

        try {

            String banque = request.getParameter("banque");
            String plageDebut = request.getParameter("plageDebut");
            String plageFin = request.getParameter("plageFin");
            String userId = request.getParameter("userId");
            String ville = request.getParameter("ville");

            int note = Integer.valueOf(plageDebut);

            List<NotePerceptionBanque> listNotePerceptionBanque = new ArrayList<>();

            while (note <= Integer.valueOf(plageFin)) {

                //System.out.println(note);
                NotePerceptionBanque npb = new NotePerceptionBanque();

                npb.setNotePerception(note + "");
                npb.setFkBanque(banque.trim());
                npb.setPlageDebut(plageDebut.trim());
                npb.setPlageFin(plageFin.trim());
                npb.setAgentCreate(Integer.valueOf(userId));
                npb.setFkEa(ville);

                listNotePerceptionBanque.add(npb);

                note++;
            }

            if (NotePerceptionBusiness.saveNotePerceptionBanqueBatch(listNotePerceptionBanque)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String modifyNotePerception(HttpServletRequest request) {

        String result = "";

        try {

            String id = request.getParameter("id");
            String notePerception = request.getParameter("notePerception");
            String userId = request.getParameter("userId");

            NotePerceptionBanque notePerceptionBanque = NotePerceptionBusiness.getNotePerceptionBanqueByNp(
                    notePerception.trim());

            if (notePerceptionBanque != null) {

                return result = "2";

            } else {

                notePerceptionBanque = new NotePerceptionBanque();

                notePerceptionBanque.setId(Integer.valueOf(id));
                notePerceptionBanque.setNotePerception(notePerception.toUpperCase().trim());
                notePerceptionBanque.setAgentMaj(Integer.valueOf(userId));

                if (NotePerceptionBusiness.modifyNotePerception(notePerceptionBanque)) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String modifyBankNotePerception(HttpServletRequest request) {

        String result = "";

        try {

            String id = request.getParameter("id");
            String notePerception = request.getParameter("notePerception");
            String banque = request.getParameter("banque");
            String userId = request.getParameter("userId");

            NotePerceptionBanque notePerceptionBanque = new NotePerceptionBanque();

            notePerceptionBanque.setId(Integer.valueOf(id));
            notePerceptionBanque.setFkBanque(banque.trim());
            notePerceptionBanque.setAgentMaj(Integer.valueOf(userId));

            if (NotePerceptionBusiness.modifyBankNotePerception(notePerceptionBanque)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String modifyVilleNotePerception(HttpServletRequest request) {

        String result = "";

        try {

            String id = request.getParameter("id");
            String ville = request.getParameter("ville");
            String userId = request.getParameter("userId");

            if (NotePerceptionBusiness.modifyVilleNotePerception(Integer.valueOf(id), Integer.valueOf(userId), ville)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    private String modifyPlageNotePerception(HttpServletRequest request) {

        String result = "";

        try {

            String id = request.getParameter("id");
            String plageDebut = request.getParameter("plageDebut");
            String plageFin = request.getParameter("plageFin");
            String userId = request.getParameter("userId");

            NotePerceptionBanque notePerceptionBanque = new NotePerceptionBanque();

            notePerceptionBanque.setId(Integer.valueOf(id));
            notePerceptionBanque.setPlageDebut(plageDebut.trim());
            notePerceptionBanque.setPlageFin(plageFin.trim());
            notePerceptionBanque.setAgentMaj(Integer.valueOf(userId));

            if (NotePerceptionBusiness.modifyPlageNotePerception(notePerceptionBanque)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String deleteNotePerceptionBank(HttpServletRequest request) {

        String result = "";

        try {

            String id = request.getParameter("id");
            String userId = request.getParameter("userId");

            if (NotePerceptionBusiness.deleteNotePerceptionBank(Integer.valueOf(id), Integer.valueOf(userId))) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
