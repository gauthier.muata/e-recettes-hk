/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.GestionArticleBudgetaireBusiness;
import cd.hologram.erecettesvg.business.GestionBanqueBusiness;
import cd.hologram.erecettesvg.business.GestionSiteBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Division;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.SiteBanque;
import cd.hologram.erecettesvg.models.TypeBien;
import cd.hologram.erecettesvg.models.UtilisateurDivision;
import cd.hologram.erecettesvg.util.*;
import com.google.gson.JsonObject;
import java.io.*;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanuel.tsasa
 */
@WebServlet(name = "GestionSite", urlPatterns = {"/site_servlet"})
public class GestionSite extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case "loadSite":
                result = getSites(request);
                break;
            case "editSite":
                result = editionSite(request);
                break;
            case "loadResponsable":
                result = getResponsables(request);
                break;
            case "loadDataBanqueSite":
                result = getDataBanqueSites(request);
                break;
            case "update":
                result = updateAffactationBankSite(request);
                break;
            case "addBankToSite":
                result = addBankToSite(request);
                break;
            case "saveDivision":
                result = saveDivision(request);
                break;
            case "loadDivision":
                result = loadDivision(request);
                break;
            case "getDivision":
                result = getDivision(request);
                break;
            case "getBureauByDivision":
                result = getBureauByDivision(request);
                break;
            case "affecterUserDivision":
                result = affecterUserDansBureau(request);
                break;
            case "getsiteByDivision":
                result = getSitesByDivision(request);
                break;
            case "loadCommuneByVille":
                result = loadCommuneByVille(request);
                break;
            case "initDataNatureBien":
                result = initDataNatureBien(request);
                break;
            case "loadEntiteByCode":
                result = loadEntiteByCode(request);
                break;
            case "loadQuertierByCommune":
                result = loadQuartierByCommune(request);
                break;

        }

        out.print(result);
    }

    public String getResponsables(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String value = request.getParameter("value");

            List<Personne> personnes = GestionSiteBusiness.getPersonneResponsables(value);

            if (!personnes.isEmpty()) {

                List<JsonObject> personneJsonList = new ArrayList<>();

                for (Personne personne : personnes) {

                    JsonObject personneJson = new JsonObject();

                    personneJson.addProperty("code", personne.getCode());
                    personneJson.addProperty("nom", personne.getNom().toUpperCase());
                    personneJson.addProperty("postnom", personne.getPostnom() != null ? personne.getPostnom().toUpperCase() : GeneralConst.EMPTY_STRING);
                    personneJson.addProperty("prenom", personne.getPrenoms() != null ? personne.getPrenoms().toUpperCase() : GeneralConst.EMPTY_STRING);
                    personneJson.addProperty("nomComplet", personne.toString().toUpperCase());

                    personneJsonList.add(personneJson);

                }

                dataReturn = personneJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getSites(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            String userId = request.getParameter("userId");

            Agent agent = GeneralBusiness.getAgentByCode(userId);

            List<Site> sites = GestionSiteBusiness.getSiteList(agent.getSite().getPeage());

            if (!sites.isEmpty()) {

                List<JsonObject> siteJsonList = new ArrayList<>();

                for (Site site : sites) {

                    JsonObject siteJson = new JsonObject();
                    Personne personne = new Personne();

                    siteJson.addProperty("code", site.getCode());
                    siteJson.addProperty("intitule", site.getIntitule().toUpperCase());
                    siteJson.addProperty("adresseName", site.getAdresse() != null ? site.getAdresse().toString().toUpperCase() : GeneralConst.EMPTY_STRING);
                    siteJson.addProperty("adresseCode", site.getAdresse() != null ? site.getAdresse().getId() : GeneralConst.EMPTY_STRING);

                    if (site.getPeage() != null) {
                        siteJson.addProperty("typeSite", site.getPeage());
                    } else {
                        siteJson.addProperty("typeSite", false);
                    }

                    siteJson.addProperty("adresseCode", site.getAdresse() != null ? site.getAdresse().getId() : GeneralConst.EMPTY_STRING);

                    if (site.getPersonne() != null && !site.getPersonne().isEmpty()) {

                        personne = IdentificationBusiness.getPersonneByCode(site.getPersonne());
                        siteJson.addProperty("responsableCode", personne.getCode());
                        siteJson.addProperty("responsableName", personne.toString().toUpperCase());
                    } else {
                        siteJson.addProperty("responsableCode", GeneralConst.EMPTY_STRING);
                        siteJson.addProperty("responsableName", GeneralConst.EMPTY_STRING);
                    }

                    siteJson.addProperty("codeDivision", site.getDivision() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getDivision().getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : site.getDivision().getCode());

                    siteJson.addProperty("intituleDivision", site.getDivision() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getDivision().getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : site.getDivision().getIntitule());

                    siteJson.addProperty("sigle", site.getSigle() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getSigle());

                    siteJson.addProperty("typeEntite", site.getFkTypeEntite());

                    siteJsonList.add(siteJson);

                }

                dataReturn = siteJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getDataBanqueSites(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userId");

            Agent agent = GeneralBusiness.getAgentByCode(userId);

            List<Site> sites = GestionSiteBusiness.getSiteList(agent.getSite().getPeage());
            List<cd.hologram.erecettesvg.models.Banque> banques = GestionBanqueBusiness.getBanqueList();

            List<JsonObject> siteJsonList = new ArrayList<>();
            List<JsonObject> banqueJsonList = new ArrayList<>();

            if (!sites.isEmpty()) {

                for (Site site : sites) {

                    JsonObject siteJson = new JsonObject();

                    siteJson.addProperty("siteCode", site.getCode());
                    siteJson.addProperty("siteName", site.getIntitule().toUpperCase());

                    if (!site.getSiteBanqueList().isEmpty()) {

                        siteJson.addProperty("bankSiteExist", GeneralConst.Number.ONE);

                        List<JsonObject> jsonSiteBanqueList = new ArrayList<>();

                        for (SiteBanque siteBanque : site.getSiteBanqueList()) {

                            JsonObject jsonSiteBanque = new JsonObject();

                            jsonSiteBanque.addProperty("banqueSiteId", siteBanque.getId());
                            jsonSiteBanque.addProperty("siteCode", site.getCode());

                            String sigleBank = GeneralConst.EMPTY_STRING;

                            jsonSiteBanque.addProperty("bankCode", siteBanque.getBanque().getCode());

                            if (siteBanque.getBanque().getSigle() != null && !siteBanque.getBanque().getSigle().isEmpty()) {
                                sigleBank = "(" + siteBanque.getBanque().getSigle().toUpperCase() + ")";
                            }

                            jsonSiteBanque.addProperty("bankName", siteBanque.getBanque().getIntitule().concat(GeneralConst.SPACE).concat(sigleBank));
                            jsonSiteBanque.addProperty("status", siteBanque.getEtat());

                            jsonSiteBanqueList.add(jsonSiteBanque);

                        }

                        siteJson.addProperty("bankSiteList", jsonSiteBanqueList.toString());

                    } else {

                        siteJson.addProperty("bankSiteList", GeneralConst.EMPTY_STRING);
                        siteJson.addProperty("bankSiteExist", GeneralConst.Number.ZERO);
                    }

                    siteJsonList.add(siteJson);
                }
            }

            if (!banques.isEmpty()) {

                for (cd.hologram.erecettesvg.models.Banque banque : banques) {

                    JsonObject banqueJson = new JsonObject();

                    banqueJson.addProperty("codeBanque", banque.getCode());
                    banqueJson.addProperty("libelleBanque", banque.getIntitule());

                    banqueJsonList.add(banqueJson);

                }

            }

            JsonObject dataSiteBankJson = new JsonObject();

            dataSiteBankJson.addProperty("siteList", siteJsonList.toString());
            dataSiteBankJson.addProperty("banqueList", banqueJsonList.toString());

            dataReturn = dataSiteBankJson.toString();

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String editionSite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String numero = GeneralConst.EMPTY_STRING;
        String codeAvenue = GeneralConst.EMPTY_STRING;
        String codeQuartier = GeneralConst.EMPTY_STRING;
        String codeCommune = GeneralConst.EMPTY_STRING;
        String codeDistrict = GeneralConst.EMPTY_STRING;
        String codeVille = GeneralConst.EMPTY_STRING;
        String codeProvince = GeneralConst.EMPTY_STRING;
        String divisionSite = GeneralConst.EMPTY_STRING;

        try {

            String code = request.getParameter("code");
            String intitule = request.getParameter("intitule");
            String idAdresse = request.getParameter("idAdresse");
            String responsable = request.getParameter("responsable");
            String typeSite = request.getParameter("typeSite");
            String codeDivision = request.getParameter("codeDivision");
            String sigle = request.getParameter("sigle");
            String typeEntite = request.getParameter("typeEntite");

            if (responsable.isEmpty()) {
                responsable = "NIF20AA00001";
            }

            if (typeEntite == null) {
                typeEntite = "3";
            } else if (typeEntite.isEmpty()) {
                typeEntite = "3";
            }

            if (code.isEmpty()) {

                numero = request.getParameter("numero");
                codeAvenue = request.getParameter("codeAvenue");
                codeQuartier = request.getParameter("codeQuartier");
                codeCommune = request.getParameter("codeCommune");
                codeDistrict = request.getParameter("codeDistrict");
                codeVille = request.getParameter("codeVille");
                codeProvince = request.getParameter("codeProvince");
            }

            boolean siteType = typeSite.equals("1") ? false : true;

            if (!codeDivision.equals("0")) {
                divisionSite = codeDivision;
            } else {
                divisionSite = null;
            }

            if (GestionSiteBusiness.editionSite(
                    code, intitule, responsable,
                    numero, codeAvenue, codeQuartier,
                    codeCommune, codeDistrict, codeVille,
                    codeProvince, idAdresse, siteType, divisionSite,
                    sigle, Integer.valueOf(typeEntite))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String updateAffactationBankSite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String id = request.getParameter("id");
            String status = request.getParameter("status");

            if (GestionSiteBusiness.updateAffactationBankSite(Integer.valueOf(id), Integer.valueOf(status))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String addBankToSite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String siteCode = request.getParameter("siteCode");
            String bankCode = request.getParameter("bankCode");

            SiteBanque siteBanque = GestionSiteBusiness.getSiteBanqueBySiteAndBanque(siteCode, bankCode);

            if (siteBanque != null) {

                dataReturn = GeneralConst.Number.TWO;

            } else {

                if (GestionSiteBusiness.addBankToSite(siteCode.trim(), bankCode.trim())) {

                    dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveDivision(HttpServletRequest request) {
        String result;
        String code = request.getParameter("code");
        String intitule = request.getParameter("intitule");
        String codeEntite = request.getParameter("codeEntite");
        String checkUpdate = request.getParameter("checkUpdate");
        String sigle = request.getParameter("sigle");

        try {
            Division division = new Division();
            if (!code.equals("")) {
                division.setCode(code);
            } else {
                division.setCode("");
            }

            if (!sigle.equals("")) {
                division.setSigle(sigle);
            } else {
                division.setSigle(null);
            }

            division.setIntitule(intitule);
            division.setEtat(Short.valueOf("1"));
            division.setEntiteAdministrative(new EntiteAdministrative(codeEntite));

            if (GestionSiteBusiness.saveDivision(division, checkUpdate)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    private String loadDivision(HttpServletRequest request) {

        String result;
        String codeEntite = request.getParameter("codeEntite");

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<Division> divisionList = GestionSiteBusiness.loadDivision(codeEntite);
            if (!divisionList.isEmpty()) {
                for (Division division : divisionList) {

                    JSONObject object = new JSONObject();

                    object.put("intitule", division.getIntitule() == null
                            ? GeneralConst.EMPTY_STRING
                            : division.getIntitule());
                    object.put("code", division.getCode() == null
                            ? GeneralConst.EMPTY_STRING
                            : division.getCode());
                    object.put("sigle", division.getSigle() == null
                            ? GeneralConst.EMPTY_STRING
                            : division.getSigle());
                    object.put("intituleVille", division.getEntiteAdministrative() == null
                            ? GeneralConst.EMPTY_STRING
                            : division.getEntiteAdministrative().getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : division.getEntiteAdministrative().getIntitule());
                    object.put("codeEntite", division.getEntiteAdministrative() == null
                            ? GeneralConst.EMPTY_STRING
                            : division.getEntiteAdministrative().getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : division.getEntiteAdministrative().getCode());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String getDivision(HttpServletRequest request) {

        String result;

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            String userId = request.getParameter("userId");

            Agent agent = GeneralBusiness.getAgentByCode(userId);

            List<Division> divisionList = GestionSiteBusiness.getDivision(agent.getSite().getPeage());

            if (!divisionList.isEmpty()) {
                for (Division division : divisionList) {

                    JSONObject object = new JSONObject();

                    object.put("intituleDivision", division.getIntitule() == null
                            ? GeneralConst.EMPTY_STRING
                            : division.getIntitule());
                    object.put("codeDivision", division.getCode() == null
                            ? GeneralConst.EMPTY_STRING
                            : division.getCode());
                    object.put("sigle", division.getSigle() == null
                            ? GeneralConst.EMPTY_STRING
                            : division.getSigle());
                    object.put("intituleVille", division.getEntiteAdministrative() == null
                            ? GeneralConst.EMPTY_STRING
                            : division.getEntiteAdministrative().getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : division.getEntiteAdministrative().getIntitule());
                    object.put("codeEntite", division.getEntiteAdministrative() == null
                            ? GeneralConst.EMPTY_STRING
                            : division.getEntiteAdministrative().getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : division.getEntiteAdministrative().getCode());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String getBureauByDivision(HttpServletRequest request) {

        String result;

        List<JSONObject> jSONObject = new ArrayList<>();

        String codeDivision = request.getParameter("codeDivision");
        String codeUser = request.getParameter("codeUser");

        UtilisateurDivision userDivision;

        try {

            List<Site> bureauList = GestionSiteBusiness.getBureauByDivision(codeDivision);
            if (!bureauList.isEmpty()) {
                for (Site bureau : bureauList) {

                    JSONObject object = new JSONObject();

                    object.put("codeSite", bureau.getCode() == null
                            ? GeneralConst.EMPTY_STRING
                            : bureau.getCode());

                    if (!codeUser.equals("")) {
                        userDivision = GestionSiteBusiness.getBureauByUser(bureau.getCode(), codeDivision, codeUser);

                        if (userDivision != null) {
                            object.put("checkAffectation", true);
                            object.put("affectationExist", true);
                            object.put("codeUserDivision", userDivision.getCode());
                        } else {
                            object.put("checkAffectation", false);
                            object.put("affectationExist", false);
                            object.put("codeUserDivision", "");
                        }
                    }

                    object.put("libelleSite", bureau.getIntitule() == null
                            ? GeneralConst.EMPTY_STRING
                            : bureau.getIntitule());
                    object.put("codeUser", bureau.getPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : bureau.getPersonne());
                    object.put("sigle", bureau.getSigle() == null
                            ? GeneralConst.EMPTY_STRING
                            : bureau.getSigle());
                    object.put("typeEntite", bureau.getFkTypeEntite() == null
                            ? GeneralConst.EMPTY_STRING
                            : bureau.getFkTypeEntite());
                    object.put("centre", bureau.getCentre() == null
                            ? GeneralConst.EMPTY_STRING
                            : bureau.getCentre());
                    object.put("codeDivision", bureau.getDivision() == null
                            ? GeneralConst.EMPTY_STRING
                            : bureau.getDivision().getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : bureau.getDivision().getCode());
                    object.put("intituleDivision", bureau.getDivision() == null
                            ? GeneralConst.EMPTY_STRING
                            : bureau.getDivision().getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : bureau.getDivision().getIntitule());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String affecterUserDansBureau(HttpServletRequest request) {

        String result = null;

        try {

            List<UtilisateurDivision> userDivisionList = new ArrayList<>();

            JSONArray jsonUserDivisionList = new JSONArray(request.getParameter("userDivisionList"));

            for (int i = 0; i < jsonUserDivisionList.length(); i++) {

                JSONObject jsonobject = jsonUserDivisionList.getJSONObject(i);

                UtilisateurDivision userdiv = new UtilisateurDivision();

                boolean checkAffectation = Boolean.valueOf(jsonobject.getString("checkAffectation"));
                boolean affectationExist = Boolean.valueOf(jsonobject.getString("affectationExist"));
                String codeDivision = jsonobject.getString("codeDivision");
                String codeSite = jsonobject.getString("codeSite");
                String codeUser = jsonobject.getString("codeUser");
                String codeUserDivision = jsonobject.getString("codeUserDivision");

                if (affectationExist) {
                    if (checkAffectation) {
                        userdiv.setEtat(Short.parseShort(GeneralConst.Number.TWO));
                    } else {
                        userdiv.setEtat(Short.parseShort(GeneralConst.Number.ZERO));
                        userdiv.setCode(codeUserDivision);
                    }
                } else {
                    if (checkAffectation) {
                        userdiv.setEtat(Short.parseShort(GeneralConst.Number.ONE));
                        userdiv.setSite(new Site(codeSite));
                        userdiv.setDivision(new Division(codeDivision));
                        userdiv.setFkPersonne(codeUser);
                    } else {
                        userdiv.setEtat(Short.parseShort(GeneralConst.Number.TWO));
                    }
                }

                userDivisionList.add(userdiv);
            }

            if (GestionSiteBusiness.affecterUserDivision(userDivisionList)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String getSitesByDivision(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeDivision = request.getParameter("codeDivision");

            List<Site> siteList = GestionSiteBusiness.getBureauByDivision(codeDivision);

            if (!siteList.isEmpty()) {

                List<JSONObject> jsonSiteList = new ArrayList<>();

                for (Site site : siteList) {

                    JSONObject jsonsite = new JSONObject();

                    jsonsite.put(IdentificationConst.ParamName.CODE_SITE, site.getCode());
                    jsonsite.put(IdentificationConst.ParamName.LIBELLE_SITE, site.getIntitule() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getIntitule());
                    jsonsite.put("typeSite", site.getPeage() == true ? "1" : "0");
                    jsonsite.put("etat", site.getEtat());
                    jsonsite.put("personne", site.getPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getPersonne());
                    jsonsite.put("centre", site.getCentre() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getCentre());
                    jsonsite.put("intituleDivision", site.getDivision() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getDivision().getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : site.getDivision().getIntitule());
                    jsonsite.put("codeDivision", site.getDivision() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getDivision().getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : site.getDivision().getCode());

                    jsonSiteList.add(jsonsite);
                }

                dataReturn = jsonSiteList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    private String loadCommuneByVille(HttpServletRequest request) {

        String result = null;

        try {

            String codeMere = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE);

            EntiteAdministrative eaVille = IdentificationBusiness.getEntiteAdministrativeByCode(codeMere);

            EntiteAdministrative eaDistrict = IdentificationBusiness.getEntiteAdministrativeByMere(eaVille.getCode());

            List<EntiteAdministrative> entiteList = GestionArticleBudgetaireBusiness.loadEntiteAdministrativeFilleV2(eaDistrict.getCode());

            if (!entiteList.isEmpty()) {
                // recuperer les communes
                List<JSONObject> jsonEntiteList = new ArrayList<>();
                for (EntiteAdministrative entite : entiteList) {

                    JSONObject jsonEntite = new JSONObject();

                    jsonEntite.put(GestionArticleBudgetaireConst.ParamName.CODE_COMMUNE, entite.getCode());
                    jsonEntite.put(GestionArticleBudgetaireConst.ParamName.INTITULE_COMMUNE, entite.getIntitule().toUpperCase());

                    jsonEntiteList.add(jsonEntite);
                }
                result = jsonEntiteList.toString();

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String initDataNatureBien(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<TypeBien> natureBienList = GestionArticleBudgetaireBusiness.loadAllNatureBien();

            if (!natureBienList.isEmpty()) {

                List<JSONObject> jsonNatureBienList = new ArrayList<>();

                for (TypeBien typeBien : natureBienList) {

                    JSONObject jsonNatureBien = new JSONObject();

                    jsonNatureBien.put("natureCode", typeBien.getCode());
                    jsonNatureBien.put("natureName", typeBien.getIntitule().toUpperCase());

                    jsonNatureBienList.add(jsonNatureBien);
                }

                dataReturn = jsonNatureBienList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    private String loadEntiteByCode(HttpServletRequest request) {

        String result = null;

        try {

            String codeMere = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE);

            EntiteAdministrative objetEntite = GestionArticleBudgetaireBusiness.loadEntiteAdministrativeByCode(codeMere);

            if (objetEntite != null) {

                JSONObject jsonEntite = new JSONObject();

                jsonEntite.put(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE, objetEntite.getCode());
                jsonEntite.put(GestionArticleBudgetaireConst.ParamName.INTITULE_ENTITE, objetEntite.getIntitule());

                result = jsonEntite.toString();

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String loadQuartierByCommune(HttpServletRequest request) {

        String result = null;

        try {

            String codeMere = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE);

            List<EntiteAdministrative> entiteList = GestionArticleBudgetaireBusiness.loadEntiteAdministrativeFille(codeMere);

            if (entiteList.size() > 0) {

                List<JSONObject> jsonEntiteList = new ArrayList<>();

                for (EntiteAdministrative entite : entiteList) {

                    JSONObject jsonEntite = new JSONObject();

                    jsonEntite.put(GestionArticleBudgetaireConst.ParamName.CODE_QUARTIER, entite.getCode());
                    jsonEntite.put(GestionArticleBudgetaireConst.ParamName.INTITULE_QUARTIER, entite.getIntitule());

                    jsonEntiteList.add(jsonEntite);
                }
                result = jsonEntiteList.toString();

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
