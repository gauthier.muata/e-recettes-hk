/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.GestionUtilisateurBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import static cd.hologram.erecettesvg.business.IdentificationBusiness.getEntiteAdministrativeByCode;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.models.AccesUser;
import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.AdressePersonne;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Division;
import cd.hologram.erecettesvg.models.Droit;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.Fonction;
import cd.hologram.erecettesvg.models.Gestionnaire;
import cd.hologram.erecettesvg.models.GroupUser;
import cd.hologram.erecettesvg.models.LoginWeb;
import cd.hologram.erecettesvg.models.Module;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.SitePeageUser;
import cd.hologram.erecettesvg.pojo.DroitUser;
import cd.hologram.erecettesvg.util.*;
import com.google.gson.JsonObject;
import java.io.*;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author emmanuel.tsasa
 */
@WebServlet(name = "GestionUtilisateur", urlPatterns = {"/utilisateur_servlet"})
public class GestionUtilisateur extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    List<Agent> listGestionnaire = new ArrayList<>();

    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case GeneralConst.ParamName.LISTE_FONCTION:
                result = loadFonction();
                break;
            case GeneralConst.ParamName.INIT_DATA_USER:
                result = initDataUser(request);
                break;
            case GeneralConst.ParamName.AJOUT_FONCTION:
                result = addFonction(request);
                break;
            case GeneralConst.ParamName.MODIF_FONCTION:
                result = editFonction(request);
                break;
            case GeneralConst.ParamName.DESACTIVER_FONCTION:
                result = desactiverFonction(request);
                break;
            case GeneralConst.ParamName.ACTIVER_FONCTION:
                result = activerFonction(request);
                break;
            case GeneralConst.ParamName.LISTE_GROUPES:
                result = loadGroup();
                break;
            case GeneralConst.ParamName.LISTE_DROITS:
                result = loadDroitsAndMember(request);
                break;
            case GeneralConst.ParamName.AJOUT_GROUPE:
                result = editGroupe(request);
                break;
            case GeneralConst.ParamName.MODIF_GROUPE:
                result = editGroupe(request);
                break;
            case GeneralConst.ParamName.ACTIVER_GROUPE:
                result = enableDisableGroupe(request);
                break;
            case GeneralConst.ParamName.DESACTIVER_GROUPE:
                result = enableDisableGroupe(request);
                break;
            case GeneralConst.ParamName.LISTE_AUTRES_DROITS:
                result = loadOtherRight(request);
                break;
            case GeneralConst.ParamName.AJOUTER_DROIT_AU_GROUPE:
                result = addDroitToGroupe(request);
            case GeneralConst.ParamName.SAVE_USER:
                result = saveUser(request);
                break;
            case GeneralConst.ParamName.CHECK_LOGIN:
                result = checkLogin(request);
                break;
            case GeneralConst.ParamName.SEARCH_GESTIONNAIRE:
                result = loadGestionnaire(request);
                break;
            case GeneralConst.ParamName.AFFECTER_ASSUJETTI:
                result = affecterAssujetti(request);
                break;
            case GeneralConst.ParamName.LOAD_ASSUJETTI:
                result = loadAssujettis(request);
                break;
            case GeneralConst.ParamName.LOAD_ASSUJETTI_AFFECTER:
                result = loadAssujettisAffecter(request);
                break;
            case GeneralConst.ParamName.DESAFFECTER_ASSUJETTI:
                result = desaffecterAssujetti(request);
                break;
            case GeneralConst.ParamName.LOAD_USER:
                result = loadUser(request);
                break;
            case GeneralConst.ParamName.LOAD_INFOS_USER:
                result = loadInfosUser(request);
                break;
            case GeneralConst.ParamName.LOAD_INFOS_USER_BY_SITE:
                result = loadListsUserBySite(request);
                break;
            case GeneralConst.ParamName.DISABLE_USER:
                result = disabledUser(request);
                break;
            case GeneralConst.ParamName.LOAD_DROITS:
                result = loadDroits(request);
                break;
            case GeneralConst.ParamName.LOAD_DROIT_AFFECTER_USER:
                result = loadDroitAffecterUser(request);
                break;
            case GeneralConst.ParamName.AFFECTER_DROITS:
                result = affecterDroit(request);
                break;
            case GeneralConst.ParamName.RETIRER_DROITS:
                result = retirerDroits(request);
                break;
            case GeneralConst.ParamName.GET_DROITS:
                result = getDroits();
                break;
            case GeneralConst.ParamName.LOAD_MODULE:
                result = loadModule();
                break;
            case GeneralConst.ParamName.SAVE_DROIT:
                result = saveDroit(request);
                break;
            case "saveNewDroit":
                result = saveNewDroit(request);
                break;
            case "loadDroitUser":
                result = loadDroitUser(request);
                break;
            case "loadDroitUserV2":
                result = loadDroitUserV2(request);
                break;
            case "loadDroitUserV3":
                result = loadDroitUserV3(request);
                break;
            case "editAccessUser":
                result = editDroitUser(request);
                break;
            case "saveFonctionUser":
                result = saveFonctionUser(request);
                break;
            case "resetPasswordUser":
                result = resetPasswordUser(request);
                break;
            case "deleteOrActivateUser":
                result = deleteOrActivateUser(request);
                break;

        }
        out.print(result);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String loadFonction() {

        List<Fonction> fonctionList = GestionUtilisateurBusiness.getFonctionList2();

        List<JSONObject> jsonFonctionList = new ArrayList<>();

        try {
            if (!fonctionList.isEmpty()) {
                for (Fonction fonction : fonctionList) {

                    JSONObject jsonFonction = new JSONObject();
                    jsonFonction.put(IdentificationConst.ParamName.CODE_FONCTION, fonction.getCode());
                    jsonFonction.put(IdentificationConst.ParamName.LIBELLE_FONCTION, fonction.getIntitule());
                    jsonFonction.put(IdentificationConst.ParamName.ETAT_FONCTION, fonction.getEtat());
                    jsonFonctionList.add(jsonFonction);

                }
            } else {
                return GeneralConst.EMPTY_STRING;
            }

        } catch (JSONException ex) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonFonctionList.toString();

    }

    public String initDataUser(HttpServletRequest request) {

        JSONObject jsonResult = new JSONObject();

        try {

            String userId = request.getParameter("userId");

            List<Fonction> fonctionList = new ArrayList<>();
            List<Site> siteList = new ArrayList<>();
            List<Site> sitePeageList = new ArrayList<>();
            List<Service> serviceList = new ArrayList<>();
            List<Division> divisionList = new ArrayList<>();

            Agent agent = GeneralBusiness.getAgentByCode(userId);

            if (agent != null) {

                fonctionList = GestionUtilisateurBusiness.getFonctionList(agent.getFonction().getCode());
                siteList = GestionUtilisateurBusiness.getSiteList(agent.getFonction().getCode());
                sitePeageList = GestionUtilisateurBusiness.getSitePeageList();
                serviceList = GestionUtilisateurBusiness.getServiceList(agent.getService().getCode());

                if (agent.getFonction().getCode().equals(propertiesConfig.getProperty("CODE_FONCTION_ADMIN_BANK"))) {

                    divisionList = GestionUtilisateurBusiness.getListDivisionByCode(agent.getSite().getDivision().getCode());

                    siteList = new ArrayList<>();
                    siteList.add(agent.getSite());

                } else {
                    divisionList = GestionUtilisateurBusiness.getDivisionList(agent.getSite().getDivision().getCode());
                }

            } else {

                fonctionList = GestionUtilisateurBusiness.getFonctionList(agent.getFonction().getCode());
                siteList = GestionUtilisateurBusiness.getSiteList(agent.getFonction().getCode());
                sitePeageList = GestionUtilisateurBusiness.getSitePeageList();
                serviceList = GestionUtilisateurBusiness.getServiceList(agent.getService().getCode());
                divisionList = GestionUtilisateurBusiness.getDivisionList(agent.getSite().getDivision().getCode());

            }

            if (!fonctionList.isEmpty()) {
                List<JSONObject> jsonFonctionList = new ArrayList<>();
                for (Fonction fonction : fonctionList) {
                    JSONObject jsonFonction = new JSONObject();
                    jsonFonction.put(IdentificationConst.ParamName.CODE_FONCTION, fonction.getCode());
                    jsonFonction.put(IdentificationConst.ParamName.LIBELLE_FONCTION, fonction.getIntitule());
                    jsonFonctionList.add(jsonFonction);
                }
                jsonResult.put(IdentificationConst.ParamName.FONCTION, jsonFonctionList.toString());
            }

            if (!siteList.isEmpty()) {

                List<JSONObject> jsonSiteList = new ArrayList<>();

                for (Site site : siteList) {

                    JSONObject jsonsite = new JSONObject();

                    jsonsite.put(IdentificationConst.ParamName.CODE_SITE, site.getCode());
                    jsonsite.put(IdentificationConst.ParamName.LIBELLE_SITE, site.getIntitule() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getIntitule());
                    jsonsite.put("typeSite", site.getPeage() == true ? "1" : "0");
                    jsonsite.put("etat", site.getEtat());
                    jsonsite.put("personne", site.getPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getPersonne());
                    jsonsite.put("centre", site.getCentre() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getCentre());
                    jsonsite.put("intituleDivision", site.getDivision() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getDivision().getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : site.getDivision().getIntitule());
                    jsonsite.put("codeDivision", site.getDivision() == null
                            ? GeneralConst.EMPTY_STRING
                            : site.getDivision().getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : site.getDivision().getCode());

                    jsonSiteList.add(jsonsite);
                }
                jsonResult.put(IdentificationConst.ParamName.SITE, jsonSiteList.toString());
            }

            if (!sitePeageList.isEmpty()) {

                List<JSONObject> jsonSitePeageList = new ArrayList<>();

                for (Site site : sitePeageList) {

                    JSONObject jsonsitePeage = new JSONObject();

                    jsonsitePeage.put(IdentificationConst.ParamName.CODE_SITE, site.getCode());
                    jsonsitePeage.put(IdentificationConst.ParamName.LIBELLE_SITE, site.getIntitule());

                    jsonSitePeageList.add(jsonsitePeage);
                }
                jsonResult.put("sitePeage", jsonSitePeageList.toString());
            }

            if (!serviceList.isEmpty()) {
                List<JSONObject> jsonServiceList = new ArrayList<>();
                for (Service service : serviceList) {
                    JSONObject jsonservice = new JSONObject();
                    jsonservice.put(IdentificationConst.ParamName.CODE_SERVICE, service.getCode());
                    jsonservice.put(IdentificationConst.ParamName.LIBELLE_SERVICE, service.getIntitule());
                    jsonServiceList.add(jsonservice);
                }
                jsonResult.put(IdentificationConst.ParamName.SERVICE, jsonServiceList.toString());
            }

            if (!divisionList.isEmpty()) {
                List<JSONObject> jsonDivisionList = new ArrayList<>();
                for (Division division : divisionList) {
                    JSONObject jsonservice = new JSONObject();
                    jsonservice.put(IdentificationConst.ParamName.CODE_DIVISION, division.getCode());
                    jsonservice.put(IdentificationConst.ParamName.LIBELLE_DIVISION, division.getIntitule());
                    jsonDivisionList.add(jsonservice);
                }
                jsonResult.put(IdentificationConst.ParamName.DIVISION, jsonDivisionList.toString());
            }

            /*if (!distrinctList.isEmpty()) {
             List<JSONObject> jsonProvinceList = new ArrayList<>();
             for (EntiteAdministrative distrinct : distrinctList) {
             JSONObject jsondistrinct = new JSONObject();
             jsondistrinct.put(IdentificationConst.ParamName.CODE_DISTRICT, distrinct.getCode());
             jsondistrinct.put(IdentificationConst.ParamName.LIBELLE_DISTRINCT, distrinct.getIntitule());
             jsonProvinceList.add(jsondistrinct);
             }
             jsonResult.put(IdentificationConst.ParamName.DISTRICT, jsonProvinceList.toString());
             }*/

            /*if (!unitAdmList.isEmpty()) {
             List<JSONObject> jsonUaList = new ArrayList<>();
             for (Ua ua : unitAdmList) {
             JSONObject jsonuadm = new JSONObject();
             jsonuadm.put(IdentificationConst.ParamName.CODE_UA, ua.getCode());
             jsonuadm.put(IdentificationConst.ParamName.LIBELLE_UA, ua.getIntitule());
             jsonUaList.add(jsonuadm);
             }
             jsonResult.put(IdentificationConst.ParamName.UA, jsonUaList.toString());
             }*/
        } catch (JSONException e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonResult.toString();
    }

    public String addFonction(HttpServletRequest request) {

        String result;

        try {
            // Initialisation des paramètres
            String intitule = request.getParameter(GeneralConst.ParamName.INTITULE_FONCTION);

            //Creation d'une nouvelle fonction
            Fonction fonction = new Fonction();
            fonction.setEtat(Boolean.TRUE);
            fonction.setIntitule(intitule);

            boolean res = GestionUtilisateurBusiness.createFonction(fonction);

            if (res) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String editFonction(HttpServletRequest request) {

        String result;

        try {
            // Initialisation des paramètres
            String intitule = request.getParameter(GeneralConst.ParamName.INTITULE_FONCTION);
            String code = request.getParameter(GeneralConst.ParamName.CODE_FONCTION);
            //Creation d'une nouvelle fonction
            Fonction fonction = new Fonction();
            fonction.setCode(code);
            fonction.setIntitule(intitule);

            boolean res = GestionUtilisateurBusiness.createFonction(fonction);

            if (res) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String desactiverFonction(HttpServletRequest request) {

        String result;

        try {
            // Initialisation des paramètres
            String code = request.getParameter(GeneralConst.ParamName.CODE_FONCTION);

            //Creation de l'objet fonction
            Fonction fonction = new Fonction();
            fonction.setCode(code);

            boolean res = GestionUtilisateurBusiness.desableFonction(fonction);

            if (res) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String activerFonction(HttpServletRequest request) {

        String result;

        try {
            // Initialisation des paramètres
            String code = request.getParameter(GeneralConst.ParamName.CODE_FONCTION);

            //Creation de l'objet fonction
            Fonction fonction = new Fonction();
            fonction.setCode(code);

            boolean res = GestionUtilisateurBusiness.enableFonction(fonction);

            if (res) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String saveUser(HttpServletRequest request) {

        String result = null;

        try {
            String codeFonction = request.getParameter(IdentificationConst.ParamName.CODE_FONCTION);
            String codeSite = request.getParameter(IdentificationConst.ParamName.CODE_SITE);
            String codeService = request.getParameter(IdentificationConst.ParamName.CODE_SERVICE);
            //String codeDistrict = request.getParameter(IdentificationConst.ParamName.CODE_DISTRICT);
            //String codeUa = request.getParameter(IdentificationConst.ParamName.CODE_UA);
            String nom = request.getParameter(IdentificationConst.ParamName.NOM);
            String prenom = request.getParameter(IdentificationConst.ParamName.PRENOM);
            String telephone = request.getParameter(IdentificationConst.ParamName.TELEPHONE);
            String email = request.getParameter(IdentificationConst.ParamName.EMAIL);
            String idUser = request.getParameter(GeneralConst.ID_USER);
            String login = request.getParameter(IdentificationConst.ParamName.LOGIN);
            //String compteExpire = request.getParameter(IdentificationConst.ParamName.COMPTE_EXPIRE);
            String matricule = request.getParameter(IdentificationConst.ParamName.MATRICULE);
            String grade = request.getParameter(IdentificationConst.ParamName.GRADE);
            //String dateExpiration = request.getParameter(IdentificationConst.ParamName.DATE_EXPIRATION);
            String codeUser = request.getParameter(IdentificationConst.ParamName.CODE_USER);
            String codeSitePeage = request.getParameter("codeSitePeage");
            JSONArray jsonAdresses = new JSONArray(request.getParameter(IdentificationConst.ParamName.ADRESSES));

            Agent agent = new Agent();
            if (!codeUser.equals(GeneralConst.EMPTY_STRING)) {
                agent.setCode(Integer.valueOf(codeUser));
            }

            //Agent agentCreate = GeneralBusiness.getAgentByCode(idUser);
            agent.setFonction(new Fonction(codeFonction));
            agent.setSite(new Site(codeSite));

            if (codeFonction.equals("F0019")) {
                agent.setService(new Service("00003"));
            } else {
                agent.setService(new Service(codeService));
            }

            agent.setUa(null);//agent.setUa(new Ua(codeUa));
            agent.setNom(nom);
            agent.setPrenoms(prenom);
            agent.setAgentCreat(idUser);
            agent.setLogin(login);
            agent.setMatricule(matricule);
            agent.setGrade(grade);
            agent.setDistrict(null);//agent.setDistrict(codeDistrict);
            agent.setChangePwd(GeneralConst.Number.ONE);

            agent.setCompteExpire(Short.parseShort("0"));
            agent.setEtat(Short.parseShort(GeneralConst.Number.ONE));

            agent.setDateExpiration(null);

            agent.setMdp(propertiesConfig.getProperty("DEFAULT_PASSWORD"));

            LoginWeb loginWeb = new LoginWeb();

            if (telephone != GeneralConst.EMPTY_STRING) {
                loginWeb.setTelephone(telephone);
            } else {
                loginWeb.setTelephone(null);
            }

            if (email != GeneralConst.EMPTY_STRING) {
                loginWeb.setMail(email);
            } else {
                loginWeb.setMail(null);
            }

            loginWeb.setPassword(propertiesConfig.getProperty("DEFAULT_PASSWORD"));

            List<AdressePersonne> adresse = getAdresseAgent(jsonAdresses);

            if (GestionUtilisateurBusiness.saveUser(agent, adresse, loginWeb, codeSitePeage)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String checkLogin(HttpServletRequest request) {

        String result = null;

        try {
            String login = request.getParameter(IdentificationConst.ParamName.LOGIN);

            Agent agent = GestionUtilisateurBusiness.checkLogin(login);

            if (agent != null) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public List<AdressePersonne> getAdresseAgent(JSONArray jsonAdresses) {

        List<AdressePersonne> adressePersonnes = new ArrayList<>();
        try {
            for (int i = 0; i < jsonAdresses.length(); i++) {
                JSONObject jsonobject = jsonAdresses.getJSONObject(i);
                String codeAP = jsonobject.getString(IdentificationConst.ParamName.CODE_ADRESSE_PERSONNE);
                String code = jsonobject.getString(IdentificationConst.ParamName.CODE_ENTITE_ADMINISTRATIVE);
                String numero = jsonobject.getString(IdentificationConst.ParamName.NUMERO);
                String defaut = jsonobject.getString(IdentificationConst.ParamName.DEFAUT);
                String etat = jsonobject.getString(IdentificationConst.ParamName.ETAT);

                EntiteAdministrative avenue = getEntiteAdministrativeByCode(code);
                EntiteAdministrative quartier = avenue.getEntiteMere();
                EntiteAdministrative commune = quartier.getEntiteMere();
                EntiteAdministrative district = commune.getEntiteMere();
                EntiteAdministrative ville = district.getEntiteMere();
                EntiteAdministrative province = ville.getEntiteMere();

                Adresse adresse = new Adresse();
                adresse.setNumero(numero);
                adresse.setAvenue(avenue);
                adresse.setQuartier(quartier);
                adresse.setCommune(commune);
                adresse.setDistrict(district);
                adresse.setVille(ville);
                adresse.setProvince(province);

                AdressePersonne adressePersonne = new AdressePersonne();
                adressePersonne.setEtat(etat.equals(GeneralConst.Number.ONE));
                adressePersonne.setCode(codeAP);
                adressePersonne.setParDefaut(defaut.equals(GeneralConst.YES_VALUE));
                adressePersonne.setAdresse(adresse);
                adressePersonnes.add(adressePersonne);
            }
        } catch (JSONException e) {
            CustumException.LogException(e);
        }
        return adressePersonnes;
    }

    public String loadGroup() {

        List<GroupUser> groupList = GestionUtilisateurBusiness.getGroupList();

        List<JSONObject> jsonGroupList = new ArrayList<>();

        try {
            if (!groupList.isEmpty()) {
                for (GroupUser group : groupList) {

                    JSONObject jsonGroup = new JSONObject();
                    jsonGroup.put(IdentificationConst.ParamName.ID_GROUPE, group.getId());
                    jsonGroup.put(IdentificationConst.ParamName.INTITULE_GROUPE, group.getIntitule());
                    jsonGroup.put(IdentificationConst.ParamName.ETAT_DROIT, group.getEtat());
                    jsonGroup.put(IdentificationConst.ParamName.DESCRIPTION_GROUPE, group.getDescription());
                    jsonGroupList.add(jsonGroup);
                }
            } else {
                return GeneralConst.EMPTY_STRING;
            }

        } catch (JSONException ex) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonGroupList.toString();
    }

    public String loadDroitsAndMember(HttpServletRequest request) {

        // Initialisation des paramètres
        String code = request.getParameter(IdentificationConst.ParamName.ID_GROUPE);

        List<Droit> droitList = GestionUtilisateurBusiness.getAllDroits(code);
        List<Agent> membres = GestionUtilisateurBusiness.getAllMembres(code);

        JSONObject object = new JSONObject();
        List<JSONObject> jsonDroitList = new ArrayList<>();
        List<JSONObject> jsonMembresList = new ArrayList<>();

        try {

            if (!droitList.isEmpty()) {
                for (Droit droit : droitList) {
                    JSONObject jsonGroup = new JSONObject();
                    jsonGroup.put(IdentificationConst.ParamName.ID_DROIT, droit.getCode());
                    jsonGroup.put(IdentificationConst.ParamName.INTITULE_DROIT, droit.getIntitule());
                    jsonGroup.put(IdentificationConst.ParamName.ETAT_DROIT, droit.getEtat());
                    jsonDroitList.add(jsonGroup);
                }

            }
            if (!membres.isEmpty()) {
                for (Agent membre : membres) {
                    JSONObject jsonGroup = new JSONObject();
                    jsonGroup.put(IdentificationConst.ParamName.ID_AGENT, membre.getCode());
                    jsonGroup.put(IdentificationConst.ParamName.NOM_AGENT, membre.getNom());
                    jsonGroup.put(IdentificationConst.ParamName.PRENOM_AGENT, membre.getPrenoms());
                    jsonMembresList.add(jsonGroup);
                }
            }
            object.put("droits", jsonDroitList);
            object.put("membres", jsonMembresList);

        } catch (JSONException ex) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return object.toString();

    }

    public String editGroupe(HttpServletRequest request) {
        String result;

        try {

            // Initialisation des paramètres
            String code = request.getParameter(IdentificationConst.ParamName.ID_GROUPE);
            String intitule = request.getParameter(IdentificationConst.ParamName.INTITULE_GROUPE);
            String description = request.getParameter(IdentificationConst.ParamName.DESCRIPTION_GROUPE);
            String agent_creat = request.getParameter(IdentificationConst.ParamName.AGENT_CREAT);

            GroupUser groupe = new GroupUser();

            if (code.isEmpty()) {
                groupe.setDateCreat(new Date());
                groupe.setAgentCreat(agent_creat);
            } else {
                groupe.setId(Integer.parseInt(code));
                groupe.setAgentMaj(agent_creat);
                groupe.setDateMaj(new Date());
            }
            groupe.setEtat(Short.valueOf("1"));
            groupe.setIntitule(intitule);
            groupe.setDescription(description);

            boolean res = GestionUtilisateurBusiness.editGroupe(groupe);

            if (res) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String enableDisableGroupe(HttpServletRequest request) {
        String result;

        try {
            // Initialisation des paramètres
            String code = request.getParameter(IdentificationConst.ParamName.ID_GROUPE);
            String operation = request.getParameter(GeneralConst.OPERATION);

            result = GestionUtilisateurBusiness.enableDisableGroupe(code, operation);

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String loadOtherRight(HttpServletRequest request) {
        // Initialisation des paramètres
        String code = request.getParameter(IdentificationConst.ParamName.ID_GROUPE);

        List<Droit> droitList = GestionUtilisateurBusiness.getAllOtherDroits(code);

        JSONObject object = new JSONObject();
        List<JSONObject> jsonDroitList = new ArrayList<>();

        try {

            if (!droitList.isEmpty()) {
                for (Droit droit : droitList) {
                    JSONObject jsonGroup = new JSONObject();
                    jsonGroup.put(IdentificationConst.ParamName.ID_DROIT, droit.getCode());
                    jsonGroup.put(IdentificationConst.ParamName.INTITULE_DROIT, droit.getIntitule());
                    jsonGroup.put(IdentificationConst.ParamName.ETAT_DROIT, droit.getEtat());
                    jsonDroitList.add(jsonGroup);
                }

            }
            object.put("droits", jsonDroitList);

        } catch (JSONException ex) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return object.toString();
    }

    public String addDroitToGroupe(HttpServletRequest request) {
        String result;

        try {

            // Initialisation des paramètres
            String code = request.getParameter(IdentificationConst.ParamName.ID_GROUPE);
            String droits = request.getParameter(IdentificationConst.ParamName.TABLEAU_DROIT);
            String user = request.getParameter(IdentificationConst.ParamName.AGENT_CREAT);

            boolean res = GestionUtilisateurBusiness.insertToGroupe(code, droits, user);

            if (res) {
                result = loadDroitsAndMember(request);
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String loadGestionnaire(HttpServletRequest request) {

        String valueSearch, typeSearch = GeneralConst.EMPTY_STRING;
        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            JsonObject jsonObject = new JsonObject();

            List<JsonObject> listJsonObjects = new ArrayList<>();

            if (valueSearch != null && !valueSearch.isEmpty()) {

                listGestionnaire = GestionUtilisateurBusiness.getListGestionnaireByCriterion(valueSearch.trim());

                if (listGestionnaire != null && !listGestionnaire.isEmpty()) {

                    for (Agent agent : listGestionnaire) {

                        jsonObject = new JsonObject();

                        jsonObject.addProperty(IdentificationConst.ParamName.ID_AGENT, agent.getCode());
                        jsonObject.addProperty(IdentificationConst.ParamName.NOM_AGENT, agent.getNom() != null
                                ? agent.toString() : GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(IdentificationConst.ParamName.PRENOM_AGENT, agent.getPrenoms() != null
                                ? agent.getPrenoms() : GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(IdentificationConst.ParamName.FONCTION, agent.getFonction() != null
                                ? agent.getFonction().getIntitule() : GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(IdentificationConst.ParamName.CODE_FONCTION, agent.getFonction() != null
                                ? agent.getFonction().getCode() : GeneralConst.EMPTY_STRING);

                        listJsonObjects.add(jsonObject);
                    }

                } else {
                    return GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                jsonObject.addProperty(TaxationConst.ParamName.SESSION, false);
            }

            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String affecterAssujetti(HttpServletRequest request) {

        String codeGestionnaire = request.getParameter(ContentieuxConst.ParamName.CODE_GESTIONNAIRE);
        String assujettiList = request.getParameter(IdentificationConst.ParamName.ASSUJETTI_LIST);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            JSONArray jsonArray = new JSONArray(assujettiList);
            List<Gestionnaire> listAssujetti = new ArrayList<>();

            for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                Gestionnaire gestionnaire = new Gestionnaire();

                gestionnaire.setPersonne(new Personne(jsonObject.getString(IdentificationConst.ParamName.CODE_PERSONNE)));
                gestionnaire.setAgent(new Agent(Integer.valueOf(codeGestionnaire)));
                gestionnaire.setDateCreat(new Date());
                gestionnaire.setEtat(Short.valueOf(GeneralConst.Number.ONE));

                listAssujetti.add(gestionnaire);
            }

            boolean result = GestionUtilisateurBusiness.affecterAssujetti(listAssujetti);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadAssujettis(HttpServletRequest request) {

        List<JSONObject> jsonPersonnes = new ArrayList<>();
        String typeSearch = request.getParameter(IdentificationConst.ParamName.TYPE_SEARCH);
        String libelle = request.getParameter(IdentificationConst.ParamName.LIBELLE);

        try {

            List<Personne> personnes = new ArrayList<>();

            if (typeSearch.equals(GeneralConst.Number.ZERO)) {
                personnes = GestionUtilisateurBusiness.getAssujettiByName(libelle);
            } else {
                Personne personne = GestionUtilisateurBusiness.getAssujettiByCodeOrNif(libelle, false);
                if (personne != null) {
                    personnes.add(personne);
                }
            }

            for (Personne personne : personnes) {
                JSONObject jsonPersonne = new JSONObject();

                jsonPersonne.put(IdentificationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getFormeJuridique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getFormeJuridique().getIntitule());

                jsonPersonne.put(IdentificationConst.ParamName.NIF,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getNif());

                jsonPersonne.put(IdentificationConst.ParamName.NOM_COMPLET, personne.toString().toUpperCase().trim());

                jsonPersonne.put(IdentificationConst.ParamName.NOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getNom());

                jsonPersonne.put(IdentificationConst.ParamName.POST_NOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getPostnom());

                jsonPersonne.put(IdentificationConst.ParamName.PRENOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getPrenoms());

                jsonPersonne.put(IdentificationConst.ParamName.TELEPHONE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getTelephone());

                jsonPersonne.put(IdentificationConst.ParamName.EMAIL,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getMail());

                jsonPersonne.put(IdentificationConst.ParamName.CHAINE_ADRESSE,
                        getDefaultAdresse(personne).toUpperCase());

                jsonPersonne.put(IdentificationConst.ParamName.CODE_PERSONNE,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getCode());

                jsonPersonnes.add(jsonPersonne);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonPersonnes.toString();
    }

    public String getDefaultAdresse(Personne personne) {

        String adresse = GeneralConst.EMPTY_STRING;
        if (personne != null) {
            if (personne.getAdressePersonneList() != null) {
                for (AdressePersonne adressePersonne : personne.getAdressePersonneList()) {
                    if (adressePersonne.getParDefaut() == true && adressePersonne.getEtat() == true) {
                        adresse = adressePersonne.getAdresse().toString();
                        break;
                    }
                }
            }
        }
        return adresse;
    }

    public String loadAssujettisAffecter(HttpServletRequest request) {

        List<JSONObject> jsonPersonnes = new ArrayList<>();
        String codeGestionnaire = request.getParameter(ContentieuxConst.ParamName.CODE_GESTIONNAIRE);

        try {

            List<Personne> personnes = new ArrayList<>();

            personnes = GestionUtilisateurBusiness.getAssujettiByGestionnaire(codeGestionnaire);

            if (personnes.size() > 0) {

                for (Personne personne : personnes) {
                    JSONObject jsonPersonne = new JSONObject();

                    jsonPersonne.put(IdentificationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                            personne == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getFormeJuridique() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getFormeJuridique().getIntitule());

                    jsonPersonne.put(IdentificationConst.ParamName.NIF,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getNif());

                    jsonPersonne.put(IdentificationConst.ParamName.NOM_COMPLET, personne.toString().toUpperCase().trim());

                    jsonPersonne.put(IdentificationConst.ParamName.NOM,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getNom());

                    jsonPersonne.put(IdentificationConst.ParamName.POST_NOM,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getPostnom());

                    jsonPersonne.put(IdentificationConst.ParamName.PRENOM,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getPrenoms());

                    jsonPersonne.put(IdentificationConst.ParamName.TELEPHONE,
                            personne == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getLoginWeb() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getLoginWeb().getTelephone());

                    jsonPersonne.put(IdentificationConst.ParamName.EMAIL,
                            personne == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getLoginWeb() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getLoginWeb().getMail());

                    jsonPersonne.put(IdentificationConst.ParamName.CHAINE_ADRESSE,
                            getDefaultAdresse(personne).toUpperCase());

                    jsonPersonne.put(IdentificationConst.ParamName.CODE_PERSONNE,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getCode());

                    jsonPersonnes.add(jsonPersonne);
                }

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonPersonnes.toString();
    }

    public String desaffecterAssujetti(HttpServletRequest request) {

        String codeGestionnaire = request.getParameter(ContentieuxConst.ParamName.CODE_GESTIONNAIRE);
        String assujettiList = request.getParameter(IdentificationConst.ParamName.ASSUJETTI_LIST);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            JSONArray jsonArray = new JSONArray(assujettiList);
            List<Gestionnaire> listAssujetti = new ArrayList<>();

            for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                Gestionnaire gestionnaire = new Gestionnaire();

                gestionnaire.setPersonne(new Personne(jsonObject.getString(IdentificationConst.ParamName.CODE_PERSONNE)));
                gestionnaire.setAgent(new Agent(Integer.valueOf(codeGestionnaire)));
                gestionnaire.setDateCreat(new Date());
                gestionnaire.setEtat(Short.valueOf(GeneralConst.Number.ONE));

                listAssujetti.add(gestionnaire);
            }

            boolean result = GestionUtilisateurBusiness.desaffecterAssujetti(listAssujetti);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadUser(HttpServletRequest request) {

        List<JsonObject> jsonUserLis = new ArrayList<>();
        JsonObject jsonObject = new JsonObject();

        String dataReturn = GeneralConst.EMPTY_STRING;
        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;

        String site = GeneralConst.EMPTY_STRING;
        String service = GeneralConst.EMPTY_STRING;
        String division = GeneralConst.EMPTY_STRING;
        String status = GeneralConst.EMPTY_STRING;

        String typeSearch = request.getParameter("typeSearch");
        String valueSearch = request.getParameter("valueSearch");
        String isAdvance = request.getParameter("isAdvance");
        String userId = request.getParameter("userId");

        if (isAdvance.equals("1")) {

            service = request.getParameter("service");
            site = request.getParameter("site");
            division = request.getParameter("division");
            status = request.getParameter("status");

            dateDebut = request.getParameter(GeneralConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(GeneralConst.ParamName.DATE_FIN);

            if (dateDebut != null) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }

            if (dateFin != null) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        try {

            List<Agent> agents = new ArrayList<>();

            Agent a = GeneralBusiness.getAgentByCode(userId);

            switch (isAdvance) {

                case GeneralConst.Number.ZERO:

                    agents = GestionUtilisateurBusiness.getUserBySimpleSearch(typeSearch, valueSearch, a.getSite().getPeage());

                    break;

                case GeneralConst.Number.ONE:

                    agents = GestionUtilisateurBusiness.getUserByAdvancedSearch(service, site, dateDebut, dateFin, division, a.getSite().getPeage(), status);

                    break;

            }

            if (!agents.isEmpty()) {

                for (Agent agent : agents) {

                    jsonObject = new JsonObject();

                    jsonObject.addProperty(IdentificationConst.ParamName.ID_AGENT, agent.getCode());

                    jsonObject.addProperty("isConnected", agent.getConnecte());

                    if (agent.getDateDerniereConnexion() != null) {
                        jsonObject.addProperty("dateLastConnexion", ConvertDate.formatDateHeureToStringV2(agent.getDateDerniereConnexion()));
                    } else {
                        jsonObject.addProperty("dateLastConnexion", "");
                    }

                    if (agent.getDateDerniereDeconnexion() != null) {
                        jsonObject.addProperty("dateLastDeconnexion", ConvertDate.formatDateHeureToStringV2(agent.getDateDerniereDeconnexion()));
                    } else {
                        jsonObject.addProperty("dateLastDeconnexion", "");
                    }

                    jsonObject.addProperty(IdentificationConst.ParamName.NOM_AGENT, agent.getNom() != null
                            ? agent.getNom() : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(IdentificationConst.ParamName.PRENOM_AGENT, agent.getPrenoms() != null
                            ? agent.getPrenoms() : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(IdentificationConst.ParamName.LIBELLE_FONCTION, agent.getFonction() != null
                            ? agent.getFonction().getIntitule() : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(IdentificationConst.ParamName.CODE_FONCTION, agent.getFonction() != null
                            ? agent.getFonction().getCode() : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(IdentificationConst.ParamName.CODE_SERVICE, agent.getService() != null
                            ? agent.getService().getCode() : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(IdentificationConst.ParamName.LIBELLE_SERVICE, agent.getService() != null
                            ? agent.getService().getIntitule() : GeneralConst.EMPTY_STRING);

                    jsonObject.addProperty(IdentificationConst.ParamName.LOGIN, agent.getLogin() != null
                            ? agent.getLogin() : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(IdentificationConst.ParamName.MATRICULE, agent.getMatricule() != null
                            ? agent.getMatricule() : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(IdentificationConst.ParamName.CODE_SITE, agent.getSite() != null
                            ? agent.getSite().getCode() : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(IdentificationConst.ParamName.LIBELLE_SITE, agent.getSite() != null
                            ? agent.getSite().getIntitule() : GeneralConst.EMPTY_STRING);

                    if (agent.getSite().getDivision() != null) {

                        jsonObject.addProperty("divisionCode", agent.getSite().getDivision().getCode());
                        jsonObject.addProperty("divisionName", agent.getSite().getDivision().getIntitule().toUpperCase());

                    } else {
                        jsonObject.addProperty("divisionCode", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("divisionName", GeneralConst.EMPTY_STRING);
                    }

                    if (agent.getGrade() != null) {
                        jsonObject.addProperty("grade", agent.getGrade().toUpperCase());
                    } else {
                        jsonObject.addProperty("grade", GeneralConst.EMPTY_STRING);
                    }

                    Personne personne = null;

                    if (agent.getPersonne() != null) {

                        personne = IdentificationBusiness.getPersonneByCode(agent.getPersonne());

                        jsonObject.addProperty("phone", personne == null ? "" : personne.getLoginWeb() == null ? "" : personne.getLoginWeb().getTelephone());
                        jsonObject.addProperty("mail", personne == null ? "" : personne.getLoginWeb() == null ? "" : personne.getLoginWeb().getMail());

                    } else {
                        jsonObject.addProperty("phone", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("mail", GeneralConst.EMPTY_STRING);
                    }

                    if (agent.getSite().getPeage()) {
                        jsonObject.addProperty("isPeage", 1);
                    } else {
                        jsonObject.addProperty("isPeage", 0);
                    }

                    jsonObject.addProperty("stateUser", agent.getEtat());

                    if (agent.getEtat() == 0) {
                        jsonObject.addProperty("dateSuppression", ConvertDate.formatDateHeureToStringV2(agent.getDateSuppression()));
                    } else {
                        jsonObject.addProperty("dateSuppression", "");
                    }

                    jsonUserLis.add(jsonObject);

                }

                dataReturn = jsonUserLis.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadInfosUser(HttpServletRequest request) {

        JSONObject jsonUser = new JSONObject();
        try {

            String codeAgentEdition = request.getParameter(IdentificationConst.ParamName.CODE_AGENT_EDITION);

            if (codeAgentEdition != null) {

                Agent agent = new Agent();

                agent = GestionUtilisateurBusiness.getUserByCode(codeAgentEdition);

                if (agent != null) {

                    jsonUser.put(IdentificationConst.ParamName.ID_AGENT,
                            agent.getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getCode());
                    jsonUser.put(IdentificationConst.ParamName.NOM_AGENT,
                            agent.getNom() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getNom());
                    jsonUser.put(IdentificationConst.ParamName.MATRICULE,
                            agent.getMatricule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getMatricule());
                    jsonUser.put(IdentificationConst.ParamName.PRENOM_AGENT,
                            agent.getPrenoms() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getPrenoms());
                    jsonUser.put(IdentificationConst.ParamName.CODE_FONCTION,
                            agent.getFonction() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getFonction().getCode() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : agent.getFonction().getCode());
                    jsonUser.put(IdentificationConst.ParamName.GRADE,
                            agent.getGrade() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getGrade());
                    jsonUser.put(IdentificationConst.ParamName.CODE_DISTRICT,
                            agent.getDistrict() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getDistrict());
                    jsonUser.put(IdentificationConst.ParamName.CODE_SITE,
                            agent.getSite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getSite().getCode() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : agent.getSite().getCode());

                    if (agent.getSite() != null) {

                        if (agent.getSite().getDivision() != null) {

                            jsonUser.put("divisionCode", agent.getSite().getDivision().getCode());
                            jsonUser.put("divisionName", agent.getSite().getDivision().getIntitule().toUpperCase());

                        } else {
                            jsonUser.put("divisionCode", GeneralConst.EMPTY_STRING);
                            jsonUser.put("divisionName", GeneralConst.EMPTY_STRING);
                        }
                    }

                    jsonUser.put("typeSite", agent.getSite().getPeage() == true ? "1" : "0");

                    jsonUser.put(IdentificationConst.ParamName.CODE_SERVICE,
                            agent.getService() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getService().getCode() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : agent.getService().getCode());
                    jsonUser.put(IdentificationConst.ParamName.CODE_UA,
                            agent.getUa() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getUa().getCode() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : agent.getUa().getCode());

                    SitePeageUser sitePeageUser = GestionUtilisateurBusiness.getSitePeageUserByUser(agent.getCode());

                    if (sitePeageUser != null) {
                        jsonUser.put("codeSitePeage", sitePeageUser.getFkSitePeage());
                    } else {
                        jsonUser.put("codeSitePeage", GeneralConst.EMPTY_STRING);
                    }

                    if (agent.getPersonne() != null) {

                        Personne personne = IdentificationBusiness.getPersonneByCode(agent.getPersonne().trim());

                        if (personne != null) {
                            jsonUser.put(IdentificationConst.ParamName.EMAIL,
                                    personne == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getLoginWeb() == null
                                                    ? GeneralConst.EMPTY_STRING
                                                    : personne.getLoginWeb().getMail());
                            jsonUser.put(IdentificationConst.ParamName.TELEPHONE,
                                    personne == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getLoginWeb() == null
                                                    ? GeneralConst.EMPTY_STRING
                                                    : personne.getLoginWeb().getTelephone());
                            jsonUser.put(IdentificationConst.ParamName.ADRESSES,
                                    Tools.getAdressesByPersonne(personne));
                        } else {
                            jsonUser.put(IdentificationConst.ParamName.EMAIL, GeneralConst.EMPTY_STRING);
                            jsonUser.put(IdentificationConst.ParamName.TELEPHONE, GeneralConst.EMPTY_STRING);
                            jsonUser.put(IdentificationConst.ParamName.ADRESSES, GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        jsonUser.put(IdentificationConst.ParamName.EMAIL, GeneralConst.EMPTY_STRING);
                        jsonUser.put(IdentificationConst.ParamName.TELEPHONE, GeneralConst.EMPTY_STRING);
                        jsonUser.put(IdentificationConst.ParamName.ADRESSES, GeneralConst.EMPTY_STRING);
                    }

                    jsonUser.put(IdentificationConst.ParamName.ETAT,
                            agent.getEtat());
                    jsonUser.put(IdentificationConst.ParamName.DATE_EXPIRATION,
                            agent.getDateExpiration() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getDateExpiration());
                    jsonUser.put(IdentificationConst.ParamName.COMPTE_EXPIRE,
                            agent.getCompteExpire());
                    jsonUser.put(IdentificationConst.ParamName.LOGIN,
                            agent.getLogin() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getLogin());
                }

            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonUser.toString();
    }

    public String loadListsUserBySite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeSite = request.getParameter("codeSite");
            List<Agent> listAgent = new ArrayList<>();

            listAgent = GestionUtilisateurBusiness.getListAgentBySite(codeSite);

            if (!listAgent.isEmpty()) {

                List<JSONObject> jsonUserList = new ArrayList<>();

                for (Agent agent : listAgent) {

                    JSONObject jsonUser = new JSONObject();

                    jsonUser.put(IdentificationConst.ParamName.ID_AGENT,
                            agent.getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getCode());
                    jsonUser.put(IdentificationConst.ParamName.NOM_AGENT,
                            agent.getNom() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getNom());
                    jsonUser.put(IdentificationConst.ParamName.MATRICULE,
                            agent.getMatricule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getMatricule());
                    jsonUser.put(IdentificationConst.ParamName.PRENOM_AGENT,
                            agent.getPrenoms() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : agent.getPrenoms());
                    jsonUser.put("fonctionName", agent.getFonction().getIntitule().toUpperCase());

                    jsonUserList.add(jsonUser);
                }

                dataReturn = jsonUserList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;

        }
        return dataReturn;
    }

    public String disabledUser(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String codeUser = request.getParameter(IdentificationConst.ParamName.ID_AGENT);

            if (codeUser != null) {
                if (GestionUtilisateurBusiness.disabledUser(codeUser)) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String loadDroitAffecterUser(HttpServletRequest request) {

        List<JsonObject> jsonUserLis = new ArrayList<>();
        JsonObject jsonObject = new JsonObject();

        String typeSearch = request.getParameter(IdentificationConst.ParamName.TYPE_SEARCH);
        String libelle = request.getParameter(IdentificationConst.ParamName.LIBELLE);
        String codeAgent = request.getParameter(IdentificationConst.ParamName.ID_AGENT);
        String codeModule = request.getParameter(IdentificationConst.ParamName.CODE_MODULE);

        try {

            List<Droit> droits = new ArrayList<>();

            if (typeSearch.equals(GeneralConst.Number.ZERO)) {
                droits = GestionUtilisateurBusiness.getDroitrAffecterUserByIntitule(libelle.trim(), codeAgent, codeModule);
            } else {
                droits = GestionUtilisateurBusiness.getDroitAffecterByCode(libelle.trim(), codeAgent, codeModule);

            }
            for (Droit droit : droits) {

                jsonObject = new JsonObject();

                jsonObject.addProperty(IdentificationConst.ParamName.ID_DROIT, droit.getCode());
                jsonObject.addProperty(IdentificationConst.ParamName.INTITULE_DROIT,
                        droit.getIntitule() == null ? GeneralConst.EMPTY_STRING : droit.getIntitule());
                jsonObject.addProperty(IdentificationConst.ParamName.ETAT_DROIT, droit.getEtat());

                jsonUserLis.add(jsonObject);

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonUserLis.toString();
    }

    public String loadDroits(HttpServletRequest request) {

        List<JsonObject> jsonUserLis = new ArrayList<>();
        JsonObject jsonObject = new JsonObject();

        String typeSearch = request.getParameter(IdentificationConst.ParamName.TYPE_SEARCH);
        String libelle = request.getParameter(IdentificationConst.ParamName.LIBELLE);
        String idUser = request.getParameter(GeneralConst.ID_USER);
        String codeAgent = request.getParameter(IdentificationConst.ParamName.ID_AGENT);
        String codeModule = request.getParameter(IdentificationConst.ParamName.CODE_MODULE);

        try {

            List<Droit> droits = new ArrayList<>();

            if (typeSearch.equals(GeneralConst.Number.ZERO)) {
                droits = GestionUtilisateurBusiness.getDroitrByIntitule(libelle.trim(), idUser.trim(), codeAgent.trim(), codeModule);
            } else {
                droits = GestionUtilisateurBusiness.getDroitrByCode(libelle.trim(), idUser, codeModule);

            }
            if (!droits.isEmpty() || droits.size() > 0) {

                for (Droit droit : droits) {

                    jsonObject = new JsonObject();

                    jsonObject.addProperty(IdentificationConst.ParamName.ID_DROIT, droit.getCode());
                    jsonObject.addProperty(IdentificationConst.ParamName.INTITULE_DROIT,
                            droit.getIntitule() == null ? GeneralConst.EMPTY_STRING : droit.getIntitule());
                    jsonObject.addProperty(IdentificationConst.ParamName.ETAT_DROIT, droit.getEtat());

                    jsonUserLis.add(jsonObject);

                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonUserLis.toString();
    }

    public String affecterDroit(HttpServletRequest request) {

        String codeAgent = request.getParameter(IdentificationConst.ParamName.ID_AGENT);
        String droitList = request.getParameter(IdentificationConst.ParamName.DROIT_LIST);
        String idUser = request.getParameter(GeneralConst.ID_USER);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            JSONArray jsonArray = new JSONArray(droitList);
            List<AccesUser> listAccesUser = new ArrayList<>();

            for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                AccesUser accesuser = new AccesUser();

                accesuser.setDroit(jsonObject.getString(IdentificationConst.ParamName.ID_DROIT));
                accesuser.setAgent(codeAgent);
                accesuser.setAgentCreat(idUser);

                listAccesUser.add(accesuser);
            }

            boolean result = GestionUtilisateurBusiness.affecterDroits(listAccesUser);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String retirerDroits(HttpServletRequest request) {

        String codeAgent = request.getParameter(IdentificationConst.ParamName.ID_AGENT);
        String droitList = request.getParameter(IdentificationConst.ParamName.DROIT_LIST);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<String> droitsList = new ArrayList<>();
            JSONArray jsonDroitArray;

            if (droitList != null && !droitList.isEmpty()) {

                jsonDroitArray = new JSONArray(droitList);

                for (int i = 0; i < jsonDroitArray.length(); i++) {
                    JSONObject jsonObject = jsonDroitArray.getJSONObject(i);
                    droitsList.add(jsonObject.getString(IdentificationConst.ParamName.ID_DROIT));
                }

            }

            boolean result = GestionUtilisateurBusiness.desaffecterroits(droitsList, codeAgent);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveFonctionUser(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        boolean result = false;

        try {

            String libelle = request.getParameter("libelle");
            String code = request.getParameter("code");

            result = GestionUtilisateurBusiness.saveFonctionUser(libelle, code);

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String resetPasswordUser(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        boolean result = false;

        try {

            String userID = request.getParameter("userID");
            String agentMaj = request.getParameter("agentMaj");

            result = GestionUtilisateurBusiness.resetPasswordUser(Integer.valueOf(userID), Integer.valueOf(agentMaj));

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String deleteOrActivateUser(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        boolean result = false;

        try {

            String userID = request.getParameter("userId");
            String agentOperation = request.getParameter("agentOperation");
            String stateUser = request.getParameter("stateUser");
            String observation = request.getParameter("observation");

            int stateUser2;

            if (stateUser.equals("1")) {
                stateUser2 = 0;
            } else {
                stateUser2 = 1;
            }

            result = GestionUtilisateurBusiness.deleteOrActivateUser(Integer.valueOf(userID), Integer.valueOf(agentOperation), stateUser2, observation);

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String editDroitUser(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        boolean result = false;

        try {

            String userId = request.getParameter("userId");
            String droitId = request.getParameter("droitId");
            String action = request.getParameter("action");

            AccesUser accesUser = GestionUtilisateurBusiness.getAccesByUserAndRight(
                    Integer.valueOf(userId), droitId);

            int idAccessUser = accesUser != null ? accesUser.getId() : 0;

            switch (action) {

                case "add":
                    result = GestionUtilisateurBusiness.addRightUser(Integer.valueOf(userId), droitId);
                    break;

                case "remove":
                    result = GestionUtilisateurBusiness.removeRightUser(idAccessUser);
                    break;
            }

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getDroits() {

        List<JsonObject> jsonUserLis = new ArrayList<>();
        JsonObject jsonObject = new JsonObject();

        try {

            List<Droit> droits = new ArrayList<>();

            droits = GestionUtilisateurBusiness.loadDroits();

            if (!droits.isEmpty() || droits.size() > 0) {

                for (Droit droit : droits) {

                    jsonObject = new JsonObject();

                    jsonObject.addProperty(IdentificationConst.ParamName.ID_DROIT, droit.getCode());
                    jsonObject.addProperty(IdentificationConst.ParamName.INTITULE_DROIT,
                            droit.getIntitule() == null ? GeneralConst.EMPTY_STRING : droit.getIntitule());
                    jsonObject.addProperty(IdentificationConst.ParamName.ETAT_DROIT, droit.getEtat());
                    jsonObject.addProperty(IdentificationConst.ParamName.INTITULE_MODULE,
                            droit.getFkModule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : droit.getFkModule().getIntitule() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : droit.getFkModule().getIntitule());

                    jsonObject.addProperty(IdentificationConst.ParamName.FK_MODULE,
                            droit.getFkModule() == null
                                    ? GeneralConst.Numeric.ZERO
                                    : droit.getFkModule().getCode() == null
                                            ? GeneralConst.Numeric.ZERO
                                            : droit.getFkModule().getCode());

                    jsonUserLis.add(jsonObject);

                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonUserLis.toString();
    }

    public String loadDroitUser(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<DroitUser> droitUsers = new ArrayList<>();

            String userId = request.getParameter("userId");

            List<Droit> accesUsers = GestionUtilisateurBusiness.loadDroitAccorderByUser(Integer.valueOf(userId));

            if (!accesUsers.isEmpty()) {

                for (Droit droit : accesUsers) {

                    DroitUser droitUser = new DroitUser();

                    droitUser.setAccess(1);
                    droitUser.setId(droit.getCode());
                    droitUser.setDroit(droit.getIntitule() != null ? droit.getIntitule() : GeneralConst.EMPTY_STRING);
                    droitUser.setModule(droit.getFkModule() != null ? droit.getFkModule().getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);
                    droitUser.setModuleId(droit.getFkModule() != null ? droit.getFkModule().getCode() + "" : GeneralConst.EMPTY_STRING);

                    droitUsers.add(droitUser);

                }
            }

            String filter = GeneralConst.EMPTY_STRING;

            Agent agent = GeneralBusiness.getAgentByCode(userId);

            if (agent != null) {

                switch (agent.getFonction().getCode()) {
                    case "F0002":
                        filter = propertiesConfig.getProperty("TAXATEUR");
                        break;
                    case "F0001":
                        filter = propertiesConfig.getProperty("ENCODEUR");
                        break;
                    case "F0003":
                        filter = propertiesConfig.getProperty("ORDONNATEUR");
                        break;
                    case "F0005":
                        filter = propertiesConfig.getProperty("RECEVEUR");
                        break;
                    case "F0029":
                        filter = propertiesConfig.getProperty("CONTENTIEUX");
                        break;
                    case "F0034":
                        filter = propertiesConfig.getProperty("DG");
                        break;
                    case "F0019":
                        filter = propertiesConfig.getProperty("BANQUIER");
                        break;
                    case "F0043":
                        filter = propertiesConfig.getProperty("ADMIN_BANQUE");
                        break;
                    case "F0020":
                        filter = propertiesConfig.getProperty("ADMIN");
                        break;
                    case "F0036":
                    case "F0037":
                    case "F0038":
                    case "F0039":
                        filter = propertiesConfig.getProperty("PEAGE");
                        break;
                    case "F0041":
                        filter = propertiesConfig.getProperty("ADMIN_PEAGE");
                        break;
                    case "F0042":
                    case "F0007":
                        filter = propertiesConfig.getProperty("TAXATEUR_TAXE");
                        break;
                }
            }

            List<Droit> droitsOthers = new ArrayList<>();

            if (agent.getFonction().getCode().equals(propertiesConfig.getProperty("CODE_FONCTION_ADMIN_PEAGE"))) {

                droitsOthers = GestionUtilisateurBusiness.loadDroitNonAccorderByUserV3(Integer.valueOf(userId),
                        propertiesConfig.getProperty("CODE_LIST_OTHERS_RIGHT_ADMIN_PEAGE"));

            }

            List<Droit> droits = new ArrayList<>();

            droits = GestionUtilisateurBusiness.loadDroitNonAccorderByUserV2(Integer.valueOf(userId), filter);

            droits.addAll(droitsOthers);

            if (!droits.isEmpty()) {

                for (Droit droit : droits) {

                    DroitUser droitUser = new DroitUser();

                    droitUser.setAccess(0);
                    droitUser.setId(droit.getCode());
                    droitUser.setDroit(droit.getIntitule() != null ? droit.getIntitule() : GeneralConst.EMPTY_STRING);
                    droitUser.setModule(droit.getFkModule() != null ? droit.getFkModule().getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);
                    droitUser.setModuleId(droit.getFkModule() != null ? droit.getFkModule().getCode() + "" : GeneralConst.EMPTY_STRING);
                    droitUsers.add(droitUser);

                }
            }

            List<JsonObject> accessUserJonList = new ArrayList<>();

            if (!droitUsers.isEmpty()) {

                for (DroitUser droitUser : droitUsers) {

                    JsonObject accessUserJon = new JsonObject();

                    accessUserJon.addProperty("droitId", droitUser.getId());
                    accessUserJon.addProperty("droitName", droitUser.getDroit());
                    accessUserJon.addProperty("module", droitUser.getModule().trim());
                    accessUserJon.addProperty("moduleId", droitUser.getModuleId());
                    accessUserJon.addProperty("access", droitUser.getAccess());

                    accessUserJonList.add(accessUserJon);

                }

                dataReturn = accessUserJonList.toString();
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadDroitUserV2(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<DroitUser> droitUsers = new ArrayList<>();

            String userId = request.getParameter("userId");

            List<Droit> accesUsers = GestionUtilisateurBusiness.loadDroitAccorderByUserV2(Integer.valueOf(userId));

            if (!accesUsers.isEmpty()) {

                List<JsonObject> jSONObjectList = new ArrayList<>();

                for (Droit droit : accesUsers) {

                    JsonObject jSONObject = new JsonObject();

                    jSONObject.addProperty("code", droit.getCode());
                    jSONObject.addProperty("module", droit.getFkModule() == null ? "" : droit.getFkModule().getIntitule().toUpperCase());

                    jSONObjectList.add(jSONObject);

                }

                dataReturn = jSONObjectList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadDroitUserV3(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userId");

            List<Droit> accesUsers = GestionUtilisateurBusiness.loadDroitNonAccorderByUserV2(Integer.valueOf(userId));

            if (!accesUsers.isEmpty()) {

                List<JsonObject> jSONObjectList = new ArrayList<>();

                for (Droit droit : accesUsers) {

                    JsonObject jSONObject = new JsonObject();

                    jSONObject.addProperty(IdentificationConst.ParamName.ID_DROIT, droit.getCode());
                    jSONObject.addProperty(IdentificationConst.ParamName.INTITULE_DROIT,
                            droit.getIntitule() == null ? GeneralConst.EMPTY_STRING : droit.getIntitule());
                    jSONObject.addProperty(IdentificationConst.ParamName.ETAT_DROIT, droit.getEtat());
                    jSONObject.addProperty(IdentificationConst.ParamName.INTITULE_MODULE,
                            droit.getFkModule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : droit.getFkModule().getIntitule() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : droit.getFkModule().getIntitule());

                    jSONObject.addProperty(IdentificationConst.ParamName.FK_MODULE,
                            droit.getFkModule() == null
                                    ? GeneralConst.Numeric.ZERO
                                    : droit.getFkModule().getCode() == null
                                            ? GeneralConst.Numeric.ZERO
                                            : droit.getFkModule().getCode());

                    jSONObjectList.add(jSONObject);

                }

                dataReturn = jSONObjectList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadModule() {

        List<JSONObject> jsonModuleList = new ArrayList<>();
        JSONObject jsonModule = new JSONObject();

        try {

            List<Module> modules = GestionUtilisateurBusiness.loadModules();

            for (Module mdl : modules) {

                jsonModule = new JSONObject();

                jsonModule.put(IdentificationConst.ParamName.INTITULE_MODULE,
                        mdl == null
                                ? GeneralConst.EMPTY_STRING
                                : mdl.getIntitule());
                jsonModule.put(IdentificationConst.ParamName.CODE_MODULE,
                        mdl == null
                                ? GeneralConst.EMPTY_STRING
                                : mdl.getCode());
                jsonModuleList.add(jsonModule);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonModuleList.toString();
    }

    public String saveDroit(HttpServletRequest request) {

        String codeDroit = request.getParameter(IdentificationConst.ParamName.ID_DROIT);
        String intituleDroit = request.getParameter(IdentificationConst.ParamName.INTITULE_DROIT);
        String idUser = request.getParameter(GeneralConst.ID_USER);
        String codeModule = request.getParameter(IdentificationConst.ParamName.CODE_MODULE);
        String etat = request.getParameter(IdentificationConst.ParamName.ETAT);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            Droit droit = new Droit();

            droit.setCode(codeDroit);
            droit.setFkModule(new Module(Integer.valueOf(codeModule)));
            droit.setIntitule(intituleDroit);
            droit.setAgentCreat(idUser);
            droit.setDateCreat(new Date());
            droit.setAgentMaj(idUser);
            droit.setDateMaj(new Date());
            droit.setEtat(Short.valueOf(etat));

            boolean result = GestionUtilisateurBusiness.saveDroit(droit);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveNewDroit(HttpServletRequest request) {

        String codeUser = request.getParameter("codeUser");
        String droitAccordeList = request.getParameter("droitAccordeList");
        String idUser = request.getParameter(GeneralConst.ID_USER);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            JSONArray jsonArray = null;

            if (droitAccordeList != null) {
                jsonArray = new JSONArray(droitAccordeList);
            }

            List<String> droitList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonDataDroitObject = jsonArray.getJSONObject(i);

                droitList.add(jsonDataDroitObject.getString("code_droit"));

            }

            boolean result = GestionUtilisateurBusiness.saveDroitSupp(droitList, Integer.valueOf(codeUser), Integer.valueOf(idUser));

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

}
