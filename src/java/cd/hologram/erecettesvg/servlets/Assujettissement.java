/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.*;

import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.pojo.ComplementMockIF;
import cd.hologram.erecettesvg.pojo.DeclarationMock;
import cd.hologram.erecettesvg.pojo.DeclarationPenaliteMock;
import cd.hologram.erecettesvg.pojo.EtatSoldePaiement;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.pojo.PeriodeDeclarationSelection;
import cd.hologram.erecettesvg.pojo.PeriodiciteData;
import cd.hologram.erecettesvg.util.*;
import static cd.hologram.erecettesvg.util.Tools.formatNombreToString;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.*;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.*;

/**
 *
 * @author emmanuel.tsasa
 */
@WebServlet(name = "Assujetissement", urlPatterns = {"/assujetissement_servlet"})
public class Assujettissement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    static List<Devise> devises = null;
    static List<Unite> unites = null;
    int type_;

    static List<ComplementBien> infoComplementaires;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case AssujettissementConst.Operation.LOAD_TYPE_BIEN:
                result = loadTypeBiens();
                break;
            case AssujettissementConst.Operation.LOAD_TYPE_COMPLEMENT_BIEN:
                result = loadTypeComplementBiens(request);
                break;
            case AssujettissementConst.Operation.SAVE_BIEN:
                result = createBien(request);
                break;
            case "saveBienAutomobile":
                result = saveBienAutomobile(request);
                break;
            case AssujettissementConst.Operation.SAVE_BIEN_IMMOBILIER:
                result = createBienImmobilier(request);
                break;
            case AssujettissementConst.Operation.LOAD_BIENS_PERSONNE:
                result = loadBiensPersonne(request);
                break;
            case AssujettissementConst.Operation.GET_BIENS_PERSONNE:
                result = loadBiens(request);
                break;
            case AssujettissementConst.Operation.DESACTIVATE_BIEN_ACQUISITION:
                result = desactivateBienAcquisition(request);
                break;
            case "desactivateBienAcquisitionV2":
                result = desactivateBienAcquisitionV2(request);
                break;
            case AssujettissementConst.Operation.SAVE_BIEN_LOCATION:
                result = createAcquisition(request);
                break;
            case AssujettissementConst.Operation.LOAD_ARTICLES_BUDGETAIRES:
                result = loadArticlesBudgetaires(request);
                break;
            case AssujettissementConst.Operation.LOAD_DATA_ASSUJETTISSEMENT:
                result = loadPeriodiciteAndTarifs(request);
                break;
            case AssujettissementConst.Operation.GENERATE_PERIODES_DECLARATIONS:
                result = getPeriodesDeclarations(request);
                break;
            case AssujettissementConst.Operation.SAVE_ASSUJETTISSEMENT:
                result = createAssujettissement(request);
                break;
            case AssujettissementConst.Operation.GET_AB_ASSUJETTI:
                result = getABAssujetti(request);
                break;
            case AssujettissementConst.Operation.SAVE_RETRAIT:
                result = saveRetraitDeclaration(request);
                break;
            case AssujettissementConst.Operation.GET_BIEN_FOR_EDITION:
                result = getBienForEdition(request);
                break;
            case AssujettissementConst.Operation.LOAD_ARTICLE_BUDGETAIRE_ASSUJETTISSABLE:
                result = loadArticlesBudgetairesAssujettissable(request);
                break;
            case AssujettissementConst.Operation.CREATE_PERIODE_DECLARATION:
                result = createPeriodesDeclarations(request);
                break;
            /* case "createPeriodesDeclarationsV2":
             result = createPeriodesDeclarationsV2(request);
             break;*/
            case AssujettissementConst.Operation.LOAD_TYPE_BIEN_BY_SERVICE:
                result = loadTypeBienByService(request);
                break;
            case "saveTypeBien":
                result = saveTypeBien(request);
                break;
            case "updateTypeBien":
                result = updateTypeBien(request);
                break;
            case "disableTypeBien":
                result = disableTypeBien(request);
                break;
            case "loadUsageBien":
                result = loadUsageBien(request);
                break;
            case "initData":
                result = initData(request);
                break;
            case "saveUsageBien":
                result = saveUsageBien(request);
                break;
            case "disableUsageBien":
                result = disableUsageBien(request);
                break;
            case "saveBienPermute":
                result = createPermutation(request);
                break;
            case "loadTarifByQuartier":
                result = loadTarifByQuartier(request);
                break;
            case "getAgentByBureau":
                result = getAgentByBureau(request);
                break;
            case "redresement":
                result = redresement(request);
                break;
            case "loadPeriodeDeclarationByAssyj":
                result = loadPeriodeDeclarationByAssyj(request);
                break;
            case "canceledPenalite":
                result = canceledPenalite(request);
                break;
            case "displayDetailTaxation":
                result = loadDetailTaxation(request);
                break;
            case "getNewPeriodeDeclarationBien":
                result = getPeriodesDeclarationsV2(request);
                break;
            case "saveAddNewPriodeDeclaration":
                result = saveAddNewPriodeDeclaration(request);
                break;
            case "loadTarifByImpotAndNature":
                result = loadTarifByImpotAndNature(request);
                break;
            case "loadingAccountBankDataV2":
                result = loadingAccountBankDataV2(request);
                break;
            case "loadArticleBudgetaireAssujetissable":
                result = loadArticleBudgetaireAssujetissable(request);
                break;
            case "loadingAccountBankDataV3":
                result = loadingAccountBankDataV3(request);
                break;
            case "getInfoAccountBankAB":
                result = getInfoAccountBankAB(request);
                break;
            case "deleteData":
                result = deleteData(request);
                break;
            case "saveAssociation":
                result = saveAssociation(request);
                break;
            case "deleteAssujettissement":
                result = deleteAssujettissement(request);
                break;
            case "loadBienAutomobileByContribuable":
                result = loadBienAutomobileByContribuable(request);
                break;
            case "loadFiveLastBienAutomobileByContribuable":
                result = loadFiveLastBienAutomobileByContribuable(request);
                break;
            case "addNewModeleVehicule":
                result = addNewModeleVehicule(request);
                break;
            case "addNewMarqueVehicule":
                result = addNewMarqueVehicule(request);
                break;
            case "addNewGenreVehicule":
                result = addNewGenreVehicule(request);
                break;
            case "deleteBienAutomobile":
                result = deleteBienAutomobile(request);
                break;
            case "saveTaxationDupplicatVignette":
                result = saveTaxationDupplicatVignette(request);
                break;
            case "saveAccordExonerationVignette":
                result = saveAccordExonerationVignette(request);
                break;
            case "deleteAccordExonerationVignette":
                result = deleteAccordExonerationVignette(request);
                break;
            case "taxationExonerationVignette":
                result = taxationExonerationVignette(request);
                break;
            case "loadingPeriodeDeclaration":
                result = loadingPeriodeDeclaration(request);
                break;
            case "taxationBatchVignette":
                result = taxationBatchVignette(request);
                break;
            case "deleteBienAutomobileTaxation":
                result = deleteBienAutomobileTaxation(request);
                break;
            case "modifyAmountBienAutomobileTaxation":
                result = modifyAmountBienAutomobileTaxation(request);
                break;

            case "loadBienAutomobileByIDBien":
                result = loadBienAutomobileByIDBien(request);
                break;
            case "loadBienAutomobileNotTaxed":
                result = loadBienAutomobileContribuableNoteTaxed(request);
                break;
            case "changeBienAutomobileTaxation":
                result = changeBienAutomobileTaxation(request);
                break;
            case "deleteAllBienAutomobileNotTaxed":
                result = deleteAllBienAutomobileNotTaxed(request);
                break;
            case "getCotationBienAutomobileByContribuable":
                result = getCotationBienAutomobileByContribuable(request);
                break;
        }
        out.print(result);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String createPeriodesDeclarations(HttpServletRequest request) {

        String result;

        try {
            String idAssujettissement = request.getParameter(AssujettissementConst.ParamName.ID_ASSUJETTISSEMENT);
            String date = request.getParameter(AssujettissementConst.ParamName.DATE_FIN);
            String codePeriodicite = request.getParameter(AssujettissementConst.ParamName.CODE_PERIODICITE_AB);
            String nombreJour = request.getParameter(AssujettissementConst.ParamName.NOMBRE_JOUR);
            String nombreJourLimite = request.getParameter(AssujettissementConst.ParamName.NOMBRE_JOUR_LIMITE);
            String echeanceLegale = request.getParameter(AssujettissementConst.ParamName.ECHEANCE_LEGALE);
            String periodeEcheance = request.getParameter(AssujettissementConst.ParamName.PERIODE_ECHEANCE);
            String nombreJourLimitePaiement = request.getParameter(AssujettissementConst.ParamName.NBRE_JOUR_LEGALE_PAIEMENT);
            String isGenerate = request.getParameter("isGenerate");

            List<PeriodeDeclaration> periodeDeclarations = AssujettissementBusiness.getPeriodeDeclarationDispo(idAssujettissement);

            boolean isNullAndEmpty = false;
            boolean generate = false;

            if (periodeDeclarations == null || periodeDeclarations.isEmpty()) {
                isNullAndEmpty = true;
            } else {
                Assujeti assujettissement = periodeDeclarations.get(0).getAssujetissement();
                String dateFin = assujettissement.getDateFin();

                if (isGenerate.equals(GeneralConst.Number.ONE)) {
                    generate = true;
                    isNullAndEmpty = true;
                } else {
                    generate = ifNotExistDernierePeriode(periodeDeclarations, dateFin);
                }
            }

            if (isNullAndEmpty || generate) {

                int nombreDeJours = Integer.parseInt(nombreJour.equals(GeneralConst.EMPTY_STRING)
                        ? GeneralConst.Number.ZERO
                        : nombreJour);

                String[] dateArray = date.split(GeneralConst.SEPARATOR_SLASH_NO_SPACE);
                String jour = dateArray[0];
                String mois = dateArray[1];
                String annee = dateArray[2];

                List<JSONObject> jsonPeriodes = generatePeriodesDeclarations(
                        codePeriodicite,
                        jour,
                        mois,
                        annee,
                        nombreDeJours,
                        nombreJourLimite,
                        echeanceLegale,
                        nombreJourLimitePaiement,
                        true,
                        periodeEcheance);

                if (jsonPeriodes != null) {

                    int size = jsonPeriodes.size();
                    String dateFin = GeneralConst.EMPTY_STRING;

                    periodeDeclarations = new ArrayList<>();

                    for (JSONObject jsonPeriode : jsonPeriodes) {

                        int ordrePeriode = Integer.parseInt(jsonPeriode.getString(AssujettissementConst.ParamName.ORDRE_PERIODE));

                        if (ordrePeriode == size) {
                            dateFin = jsonPeriode.getString(AssujettissementConst.ParamName.DATE_FIN);
                        }

                        PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();

                        periodeDeclaration.setDebut(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_DEBUT)));

                        periodeDeclaration.setFin(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_FIN)));

                        periodeDeclaration.setDateLimite(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_LIMITE)));

                        periodeDeclaration.setDateLimitePaiement(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT)));

                        periodeDeclarations.add(periodeDeclaration);

                    }

                    boolean succes = AssujettissementBusiness.savePeriodesDeclarations(
                            idAssujettissement,
                            periodeDeclarations,
                            dateFin);

                    if (succes) {
                        result = GeneralConst.ResultCode.SUCCES_OPERATION;
                    } else {
                        result = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            }

        } catch (JSONException | NumberFormatException e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public void createPeriodesDeclarationsV2(int idPeriode) {

        String result;

        try {

            String date = GeneralConst.EMPTY_STRING;
            String codePeriodicite = GeneralConst.EMPTY_STRING;
            String nombreJour = GeneralConst.EMPTY_STRING;
            String nombreJourLimite = GeneralConst.EMPTY_STRING;
            String echeanceLegale = GeneralConst.EMPTY_STRING;
            String periodeEcheance = GeneralConst.EMPTY_STRING;
            String nombreJourLimitePaiement = GeneralConst.EMPTY_STRING;
            String isGenerate = GeneralConst.EMPTY_STRING;

            PeriodeDeclaration pd = RecouvrementBusiness.getPeriodeDeclarationById(idPeriode);

            String idAssujettissement = pd.getAssujetissement().getId();

            cd.hologram.erecettesvg.models.Assujeti assujeti = AssujettissementBusiness.getAssujettiNormalById(idAssujettissement);

            ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();

            if (assujeti != null) {

                articleBudgetaire = assujeti.getArticleBudgetaire();
                date = ConvertDate.getValidFormatDateString(assujeti.getDateFin());
            }

            codePeriodicite = articleBudgetaire.getPeriodicite().getCode();
            nombreJour = articleBudgetaire.getPeriodicite().getNbrJour() + "";
            nombreJourLimite = articleBudgetaire.getNbrJourLimite() + "";
            echeanceLegale = articleBudgetaire.getEcheanceLegale() == null ? "1" : articleBudgetaire.getEcheanceLegale();

            if (articleBudgetaire.getPeriodeEcheance() != null) {
                periodeEcheance = articleBudgetaire.getPeriodeEcheance() == true ? "1" : "0";
            } else {
                periodeEcheance = "0";
            }

            nombreJourLimitePaiement = articleBudgetaire.getDateLimitePaiement();
            isGenerate = "1";

            List<PeriodeDeclaration> periodeDeclarations = AssujettissementBusiness.getPeriodeDeclarationDispo(idAssujettissement);

            boolean isNullAndEmpty = false;
            boolean generate = false;

            if (periodeDeclarations == null || periodeDeclarations.isEmpty()) {
                isNullAndEmpty = true;
            } else {
                Assujeti assujettissement = periodeDeclarations.get(0).getAssujetissement();
                String dateFin = assujettissement.getDateFin();

                if (isGenerate.equals(GeneralConst.Number.ONE)) {
                    generate = true;
                    isNullAndEmpty = true;
                } else {
                    generate = ifNotExistDernierePeriode(periodeDeclarations, dateFin);
                }
            }

            if (isNullAndEmpty || generate) {

                int nombreDeJours = Integer.parseInt(nombreJour.equals(GeneralConst.EMPTY_STRING)
                        ? GeneralConst.Number.ZERO
                        : nombreJour);

                String[] dateArray = date.split(GeneralConst.SEPARATOR_SLASH_NO_SPACE);
                String jour = dateArray[0];
                String mois = dateArray[1];
                String annee = dateArray[2];

                List<JSONObject> jsonPeriodes = generatePeriodesDeclarations(
                        codePeriodicite,
                        jour,
                        mois,
                        annee,
                        nombreDeJours,
                        nombreJourLimite,
                        echeanceLegale,
                        nombreJourLimitePaiement,
                        true,
                        periodeEcheance);

                if (jsonPeriodes != null) {

                    int size = jsonPeriodes.size();
                    String dateFin = GeneralConst.EMPTY_STRING;

                    periodeDeclarations = new ArrayList<>();

                    for (JSONObject jsonPeriode : jsonPeriodes) {

                        int ordrePeriode = Integer.parseInt(jsonPeriode.getString(AssujettissementConst.ParamName.ORDRE_PERIODE));

                        if (ordrePeriode == size) {
                            dateFin = jsonPeriode.getString(AssujettissementConst.ParamName.DATE_FIN);
                        }

                        PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();

                        periodeDeclaration.setDebut(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_DEBUT)));

                        periodeDeclaration.setFin(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_FIN)));

                        periodeDeclaration.setDateLimite(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_LIMITE)));

                        periodeDeclaration.setDateLimitePaiement(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT)));

                        periodeDeclarations.add(periodeDeclaration);

                    }

                    boolean succes = AssujettissementBusiness.savePeriodesDeclarations(
                            idAssujettissement,
                            periodeDeclarations,
                            dateFin);

                    if (succes) {
                        result = GeneralConst.ResultCode.SUCCES_OPERATION;
                    } else {
                        result = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            }

        } catch (JSONException | NumberFormatException e) {
            CustumException.LogException(e);

        }

    }

    public boolean ifNotExistDernierePeriode(
            List<PeriodeDeclaration> periodeDeclarations,
            String dateFinAssujettissement) {

        boolean result = true;

        for (PeriodeDeclaration declaration : periodeDeclarations) {

            String dateFin = ConvertDate.formatDateToStringOfFormat(declaration.getFin(), "yyyy-MM-dd");

            if (dateFin.equals(dateFinAssujettissement)) {

                result = false;
                break;
            }
        }
        return result;
    }

    public String saveRetraitDeclaration(HttpServletRequest request) {

        String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
        String idUser = request.getParameter(GeneralConst.ID_USER);
        String listRetrait = request.getParameter(AssujettissementConst.ParamName.LISTE_RETRAIT);
        String codeAB = request.getParameter(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE);
        String codeBanque = request.getParameter("codeBanque");
        String codeCompteBancaire = request.getParameter("codeCompteBancaire");
        String listPenalite = request.getParameter("listPenalite");
        List<DeclarationMock> declarationListMock = new ArrayList<>();
        String returnValue = "";

        RetraitDeclaration retraitdeclaration = null;

        Date dateCreateDeclaration = new Date();

        try {

            List<TaxationBienAutomobile> listTaxationBienAutomobile = new ArrayList<>();

            Personne personne = IdentificationBusiness.getPersonneByCode(codePersonne);

            String nomRequerant = personne == null ? "" : personne.toString().toUpperCase();

            JSONArray jsonRetrait = new JSONArray(listRetrait);

            JSONArray jsonPenalites = null;

            if (listPenalite != null) {
                jsonPenalites = new JSONArray(listPenalite);
            }

            BigDecimal amountPrincipGlobal = new BigDecimal("0");

            String numDeclaration = AssujettissementBusiness.getNewid();

            DeclarationPenaliteMock declarationPenaliteMock = null;
            BigDecimal amountPean = new BigDecimal("0");

            if (jsonPenalites != null) {

                for (int j = 0; j < jsonPenalites.length(); j++) {

                    declarationPenaliteMock = new DeclarationPenaliteMock();

                    JSONObject jsonobject = jsonPenalites.getJSONObject(j);

                    amountPean = amountPean.add(BigDecimal.valueOf(Double.valueOf(jsonobject.getString("amountPenalite"))));

                    declarationPenaliteMock.setAmountPenalite(amountPean);
                    declarationPenaliteMock.setObservationRemise(jsonobject.getString("observationRemise"));
                    declarationPenaliteMock.setTauxRemise(jsonobject.getInt("tauxRemise"));
                }
            }

            for (int i = 0; i < jsonRetrait.length(); i++) {

                //récuper la valeur de base dans assuj de la periode
                DeclarationMock declarationMock = new DeclarationMock();

                JSONObject jsonobject = jsonRetrait.getJSONObject(i);

                BigDecimal amountPrincip = new BigDecimal("0");
                amountPrincip = amountPrincip.add(BigDecimal.valueOf(Double.valueOf(jsonobject.getString("montant"))));

                amountPrincipGlobal = amountPrincipGlobal.add(amountPrincip);

                String periodes = Tools.getListToStringV2(jsonobject.getString("periode"));

                String periodeNames = Tools.getListToStringV2(jsonobject.getString("periodeName"));

                String[] dateArrayPD = periodes.split(",");

                List<PeriodeDeclarationSelection> listPD = new ArrayList<>();

                for (String pd : dateArrayPD) {

                    PeriodeDeclarationSelection periodeDeclarationSelection = new PeriodeDeclarationSelection();

                    periodeDeclarationSelection.setPeriode(Integer.valueOf(pd));
                    periodeDeclarationSelection.setPenalise(jsonPenalites == null ? 0 : 1);
                    //periodeDeclarationSelection.setPenalite(new BigDecimal(Double.valueOf(jsonobject.getString("penalite"))));
                    periodeDeclarationSelection.setPrincipal(amountPrincip.floatValue() / dateArrayPD.length);

                    listPD.add(periodeDeclarationSelection);

                    if (codeAB.equals(propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"))) {

                        TaxationBienAutomobile taxationBienAutomobile = new TaxationBienAutomobile();

                        PeriodeDeclaration pdAssuj = AssujettissementBusiness.getPeriodeDeclarationByCode(pd);

                        taxationBienAutomobile.setFkBien(pdAssuj.getAssujetissement().getBien().getId());
                        taxationBienAutomobile.setExercice(propertiesConfig.getProperty("VALUE_CURRENT_EXERCICE"));

                        Palier palier = null;

                        switch (pdAssuj.getAssujetissement().getBien().getFkTarif()) {

                            case "0000071020": // 1 - 10 CV
                            case "0000071120": // 11 - 15 CV
                            case "0000071220": // +15 CV

                                palier = DeclarationBusiness.getTauxPalierVignette(
                                        propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                        pdAssuj.getAssujetissement().getBien().getFkTarif(),
                                        pdAssuj.getAssujetissement().getPersonne().getFormeJuridique().getCode());

                                break;

                            default:

                                palier = DeclarationBusiness.getTauxPalierVignetteV2(
                                        propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                        pdAssuj.getAssujetissement().getBien().getFkTarif());

                                break;

                        }

                        if (pdAssuj.getAssujetissement().getBien().getTypeBien() != null) {

                            if (pdAssuj.getAssujetissement().getBien().getTypeBien().getCode().equals(propertiesConfig.getProperty("CODE_TYPE_BIEN_AMBULANCE"))) {

                                palier = DeclarationBusiness.getTauxPalierVignetteV3(
                                        propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                        pdAssuj.getAssujetissement().getBien().getFkTarif(),
                                        pdAssuj.getAssujetissement().getPersonne().getFormeJuridique().getCode(),
                                        propertiesConfig.getProperty("CODE_TYPE_BIEN_AMBULANCE"));
                            }
                        }

                        taxationBienAutomobile.setPrincipalDu(palier == null ? new BigDecimal(BigInteger.ONE) : palier.getTaux());

                        long moisRetard = 0;

                        if (Compare.before(pdAssuj.getDateLimite(), new Date())) {

                            moisRetard = ConvertDate.getMonthsBetween(pdAssuj.getDateLimite(), new Date());

                            float penalAss = ((palier == null ? 1 : palier.getTaux().floatValue()) * Float.valueOf("0.25"));
                            float penalRec = (((palier == null ? 1 : palier.getTaux().floatValue()) * Float.valueOf("0.02")) * Float.valueOf(moisRetard));

                            float penalTotal = (penalAss + penalRec);

                            String penalTotalStr = String.valueOf(penalTotal);

                            taxationBienAutomobile.setPenaliteDu(BigDecimal.valueOf(Double.valueOf(penalTotalStr)));

                        } else {
                            taxationBienAutomobile.setPenaliteDu(BigDecimal.ZERO);
                        }

                        listTaxationBienAutomobile.add(taxationBienAutomobile);
                    }

                }

                //declarationMock.setPeriode(jsonobject.getString("periode"));
                declarationMock.setListPeriode(listPD);
                declarationMock.setPeriode(periodeNames);

                declarationMock.setIcmParamId(jsonobject.getString("icmParamId"));
                declarationMock.setCodeAssujettissement(jsonobject.getString("assuj"));
                declarationMock.setAmountPrincipal(amountPrincip);
                declarationMock.setAmountPenalite(new BigDecimal(Double.valueOf(jsonobject.getString("penalite"))));
                declarationMock.setDevise(jsonobject.getString("devise"));
                declarationMock.setRequerant(nomRequerant);
                declarationMock.setAssujetti(codePersonne);
                //declarationMock.setCodeDeclaration(AssujettissementBusiness.getNewid());
                declarationMock.setCodeDeclaration("");
                declarationMock.setCodeArticleBudgetaire(codeAB);
                declarationMock.setCodeAgentCreate(idUser);
                declarationMock.setDateCreate(dateCreateDeclaration);
                declarationMock.setEstPenalise(jsonobject.getInt("estPenalise"));

                declarationMock.setBanque(codeBanque);
                declarationMock.setCompteBancaire(codeCompteBancaire);

                declarationMock.setTauxRemise(jsonobject.getInt("tauxRemise"));
                declarationMock.setObservationRemise(jsonobject.getString("observationRemise"));
                declarationMock.setAmountRemisePenalite(new BigDecimal(Double.valueOf(jsonobject.getString("remise"))));

                Date dateEcheance = Tools.getEcheanceDate(dateCreateDeclaration, Integer.valueOf("3"));

                declarationMock.setDateEcheancePaiement(Tools.getEcheanceDate(dateEcheance, Integer.valueOf("3"), false));

                declarationListMock.add(declarationMock);
            }

            boolean result = AssujettissementBusiness.saveRetraitDeclarationV2(declarationListMock, numDeclaration, declarationPenaliteMock);
            //boolean result = false;

            String noteTaxationPrincipale = GeneralConst.EMPTY_STRING;
            String noteTaxationPenalite = GeneralConst.EMPTY_STRING;
            PrintDocument printDocument;

            if (result) {

                retraitdeclaration = AssujettissementBusiness.getRetraitDeclarationByCodeV2(numDeclaration);

                if (codeAB.equals(propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"))) {

                    AssujettissementBusiness.saveTaxationBienAutomobile(listTaxationBienAutomobile, retraitdeclaration.getId());
                }

                int id = retraitdeclaration.getId();

                printDocument = new PrintDocument();
                JSONObject jSONObject = new JSONObject();

                switch (jsonRetrait.length()) {

                    case 1:

                        for (DeclarationMock declarationMock : declarationListMock) {

                            PeriodeDeclaration pd = null;

                            if (retraitdeclaration != null) {

                                if (retraitdeclaration.getFkAb().equals("00000000000002302020")) {

                                    pd = AssujettissementBusiness.getLastPeriodeDeclarationByAssujetissement(declarationMock.getCodeAssujettissement());
                                    createPeriodesDeclarationsV2(Integer.valueOf(pd.getId()));

                                    noteTaxationPrincipale = printDocument.createNoteTaxationVignette(retraitdeclaration);
                                    jSONObject.put("noteTaxationPrincipal", noteTaxationPrincipale);

                                    returnValue = jSONObject.toString();

                                } else {

                                    pd = AssujettissementBusiness.getLastPeriodeDeclarationByAssujetissement(declarationMock.getCodeAssujettissement());
                                    createPeriodesDeclarationsV2(Integer.valueOf(pd.getId()));

                                    noteTaxationPrincipale = printDocument.createNoteTaxationDeclaration(retraitdeclaration, idUser, declarationMock.getAmountPrincipal(), declarationMock.getPeriode());

                                    if (retraitdeclaration.getEstPenalise() == GeneralConst.Numeric.ONE) {

                                        retraitdeclaration = AssujettissementBusiness.getRetraitDeclarationByCodeMere(id);

                                        if (retraitdeclaration != null) {

                                            printDocument = new PrintDocument();
                                            noteTaxationPenalite = printDocument.createNoteTaxationDeclaration(retraitdeclaration, idUser, amountPean, declarationMock.getPeriode());

                                        }
                                    }

                                    jSONObject.put("noteTaxationPrincipal", noteTaxationPrincipale);
                                    jSONObject.put("noteTaxationPenalite", noteTaxationPenalite);

                                    returnValue = jSONObject.toString();
                                }

                            }
                        }

                        break;

                    default:

                        for (DeclarationMock declarationMock : declarationListMock) {

                            PeriodeDeclaration pd = AssujettissementBusiness.getLastPeriodeDeclarationByAssujetissement(declarationMock.getCodeAssujettissement());
                            createPeriodesDeclarationsV2(Integer.valueOf(pd.getId()));

                        }

                        printDocument = new PrintDocument();

                        if (retraitdeclaration.getFkAb().equals("00000000000002302020")) {

                            noteTaxationPrincipale = printDocument.createNoteTaxationVignette(retraitdeclaration);
                            jSONObject.put("noteTaxationPrincipal", noteTaxationPrincipale);

                            returnValue = jSONObject.toString();

                        } else {

                            noteTaxationPrincipale = printDocument.createNoteTaxationDeclarationWithNBien(retraitdeclaration, idUser, amountPrincipGlobal, declarationListMock);

                            if (retraitdeclaration.getEstPenalise() == GeneralConst.Numeric.ONE) {

                                retraitdeclaration = AssujettissementBusiness.getRetraitDeclarationByCodeMere(id);

                                if (retraitdeclaration != null) {

                                    printDocument = new PrintDocument();
                                    noteTaxationPenalite = printDocument.createNoteTaxationDeclarationWithNBien(retraitdeclaration, idUser, amountPean, declarationListMock);

                                }
                            }

                            jSONObject.put("noteTaxationPrincipal", noteTaxationPrincipale);
                            jSONObject.put("noteTaxationPenalite", noteTaxationPenalite);

                            returnValue = jSONObject.toString();
                        }

                        break;
                }

            } else {
                returnValue = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (JSONException | NumberFormatException e) {

            CustumException.LogException(e);
            returnValue = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return returnValue;
    }

    public String getABAssujetti(HttpServletRequest request) {

        String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
        String vignetteOnly = request.getParameter("taxationVignetteOnly");

        JSONObject allDataAssujettisement = new JSONObject();
        List<JSONObject> dataAssujettisement = new ArrayList<>();
        List<JSONObject> dataAB = new ArrayList<>();

        String infosComplementaire = GeneralConst.EMPTY_STRING;

        try {

            List<Assujeti> assujettissements = AssujettissementBusiness.getAssujettissementByPersonne(codePersonne);

            List<String> codeABList = new ArrayList<>();

            for (Assujeti assujeti : assujettissements) {

                JSONObject jsonAssujetti = new JSONObject();

                if (codeABList.contains(assujeti.getArticleBudgetaire().getCode())) {
                    continue;
                }

                if (vignetteOnly.equals("1")) {

                    if (assujeti.getArticleBudgetaire().getCode().equals("00000000000002302020")) {

                        codeABList.add(assujeti.getArticleBudgetaire().getCode());

                        jsonAssujetti.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                assujeti.getArticleBudgetaire() == null
                                        ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCode());
                        jsonAssujetti.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
                                assujeti.getArticleBudgetaire() == null
                                        ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getIntitule().toUpperCase());
                        jsonAssujetti.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
                                assujeti.getArticleBudgetaire() == null
                                        ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCodeOfficiel().toUpperCase());

                        jsonAssujetti.put("sigleAB", assujeti.getArticleBudgetaire().getSigle() == null ? assujeti.getArticleBudgetaire().getIntitule().toUpperCase() : assujeti.getArticleBudgetaire().getSigle().toUpperCase());

                        dataAB.add(jsonAssujetti);
                    }

                } else {

                    /*codeABList.add(assujeti.getArticleBudgetaire().getCode());

                     jsonAssujetti.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                     assujeti.getArticleBudgetaire() == null
                     ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCode());
                     jsonAssujetti.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
                     assujeti.getArticleBudgetaire() == null
                     ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getIntitule().toUpperCase());
                     jsonAssujetti.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
                     assujeti.getArticleBudgetaire() == null
                     ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCodeOfficiel().toUpperCase());

                     jsonAssujetti.put("sigleAB", assujeti.getArticleBudgetaire().getSigle() == null ? assujeti.getArticleBudgetaire().getIntitule().toUpperCase() : assujeti.getArticleBudgetaire().getSigle().toUpperCase());

                     dataAB.add(jsonAssujetti);*/
                    if (!assujeti.getArticleBudgetaire().getCode().equals("00000000000002302020")) {

                        codeABList.add(assujeti.getArticleBudgetaire().getCode());

                        jsonAssujetti.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                assujeti.getArticleBudgetaire() == null
                                        ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCode());
                        jsonAssujetti.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
                                assujeti.getArticleBudgetaire() == null
                                        ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getIntitule().toUpperCase());
                        jsonAssujetti.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
                                assujeti.getArticleBudgetaire() == null
                                        ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCodeOfficiel().toUpperCase());

                        jsonAssujetti.put("sigleAB", assujeti.getArticleBudgetaire().getSigle() == null ? assujeti.getArticleBudgetaire().getIntitule().toUpperCase() : assujeti.getArticleBudgetaire().getSigle().toUpperCase());

                        dataAB.add(jsonAssujetti);
                    }
                }

            }

            for (Assujeti assujeti : assujettissements) {

                JSONObject jsonAssujetti = new JSONObject();
                List<JSONObject> jsonPeriodes = new ArrayList<>();

                jsonAssujetti.put(AssujettissementConst.ParamName.ID_ASSUJETTISSEMENT, assujeti.getId());
                jsonAssujetti.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                        assujeti.getArticleBudgetaire() == null
                                ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCode());

                Bien bien;

                int _type = 0;

                TypeBienService typeBienService = AssujettissementBusiness.getTypeBienServiceByTypeBien(assujeti.getBien().getTypeBien().getCode());

                _type = typeBienService.getType();

                jsonAssujetti.put("type", _type);
                jsonAssujetti.put("codeBien", assujeti.getBien().getId());

                if (assujeti.getBien() != null) {

                    bien = assujeti.getBien();

                    jsonAssujetti.put(AssujettissementConst.ParamName.ID_BIEN, bien.getId());
                    jsonAssujetti.put(AssujettissementConst.ParamName.INTITULE_BIEN, bien.getIntitule().toUpperCase());

                    jsonAssujetti.put(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN, bien.getTypeBien().getIntitule().toUpperCase());
                    jsonAssujetti.put(AssujettissementConst.ParamName.CODE_TYPE_BIEN, bien.getTypeBien().getCode());

                    if (bien.getFkAdressePersonne() != null) {

                        jsonAssujetti.put(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                bien.getFkAdressePersonne().getAdresse().toString().toUpperCase());

                    } else {

                        jsonAssujetti.put(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (bien.getDescription() != null && !bien.getDescription().isEmpty()) {

                        jsonAssujetti.put("descriptionBien", bien.getDescription());

                    } else {
                        jsonAssujetti.put("descriptionBien", GeneralConst.EMPTY_STRING);
                    }

                    if (bien.getFkCommune() != null && !bien.getFkCommune().isEmpty()) {

                        EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(bien.getFkCommune());

                        if (ea != null) {

                            String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                            jsonAssujetti.put("communeCode", ea.getCode());
                            jsonAssujetti.put("communeName", ea.getIntitule().toUpperCase().concat(ville));

                        } else {
                            jsonAssujetti.put("communeCode", GeneralConst.EMPTY_STRING);
                            jsonAssujetti.put("communeName", GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        jsonAssujetti.put("communeCode", GeneralConst.EMPTY_STRING);
                        jsonAssujetti.put("communeName", GeneralConst.EMPTY_STRING);
                    }

                    if (bien.getFkQuartier() != null && !bien.getFkQuartier().isEmpty()) {

                        EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(bien.getFkQuartier());

                        jsonAssujetti.put("quartierCode", eaQuartier == null ? "" : eaQuartier.getCode());
                        jsonAssujetti.put("quartierName", eaQuartier == null ? "" : eaQuartier.getIntitule().toUpperCase());

                    } else {
                        jsonAssujetti.put("quartierCode", GeneralConst.EMPTY_STRING);
                        jsonAssujetti.put("quartierName", GeneralConst.EMPTY_STRING);
                    }

                    if (bien.getFkTarif() != null && !bien.getFkTarif().isEmpty() && !bien.getFkTarif().equals("0")) {

                        Tarif tarif = TaxationBusiness.getTarifByCode(bien.getFkTarif());

                        jsonAssujetti.put("tarifCode", tarif == null ? "" : tarif.getCode());
                        jsonAssujetti.put("tarifName", tarif == null ? "" : tarif.getIntitule().toUpperCase());

                    } else {
                        jsonAssujetti.put("tarifCode", GeneralConst.EMPTY_STRING);
                        jsonAssujetti.put("tarifName", GeneralConst.EMPTY_STRING);
                    }

                    infosComplementaire = GeneralConst.EMPTY_STRING;

                    if (bien.getFkUsageBien() != null) {

                        UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(bien.getFkUsageBien());

                        jsonAssujetti.put("usageCode", usageBien == null ? "" : usageBien.getId());
                        jsonAssujetti.put("usageName", usageBien == null ? "" : usageBien.getIntitule().toUpperCase());
                        jsonAssujetti.put("isImmobilier", GeneralConst.Number.ONE);

                    } else {
                        jsonAssujetti.put("usageCode", GeneralConst.EMPTY_STRING);
                        jsonAssujetti.put("usageName", GeneralConst.EMPTY_STRING);
                        jsonAssujetti.put("isImmobilier", GeneralConst.Number.ZERO);

                    }

                    String valUnity = GeneralConst.EMPTY_STRING;

                    for (ComplementBien cb : bien.getComplementBienList()) {

                        if (cb.getDevise() != null && !cb.getDevise().isEmpty()) {

                            switch (cb.getDevise()) {
                                case GeneralConst.Devise.DEVISE_CDF:
                                case GeneralConst.Devise.DEVISE_USD:

                                    valUnity = cb.getDevise();

                                    break;
                                default:
                                    Unite unite = TaxationBusiness.getUnitebyCode(cb.getDevise());
                                    valUnity = unite != null ? unite.getIntitule() : "";

                                    break;
                            }

                        } else {
                            valUnity = GeneralConst.EMPTY_STRING;
                        }

                        ValeurPredefinie valeurPredefinie = new ValeurPredefinie();
                        String txtValeurPredefinie = GeneralConst.EMPTY_STRING;

                        if (infosComplementaire.isEmpty()) {

                            if (cb.getTypeComplement().getComplement().getValeurPredefinie()) {

                                valeurPredefinie = AssujettissementBusiness.getValeurPredefinieByCode(cb.getValeur(), cb.getTypeComplement().getComplement().getCode());
                                txtValeurPredefinie = valeurPredefinie != null ? valeurPredefinie.getValeur().toUpperCase() : "";

                                infosComplementaire = cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(txtValeurPredefinie).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");

                            } else {

                                infosComplementaire = cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(cb.getValeur()).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");
                            }

                        } else {

                            if (cb.getTypeComplement().getComplement().getValeurPredefinie()) {

                                valeurPredefinie = AssujettissementBusiness.getValeurPredefinieByCode(cb.getValeur(), cb.getTypeComplement().getComplement().getCode());
                                txtValeurPredefinie = valeurPredefinie != null ? valeurPredefinie.getValeur().toUpperCase() : "";

                                infosComplementaire = infosComplementaire += cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(txtValeurPredefinie).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");

                            } else {
                                infosComplementaire = infosComplementaire += cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(cb.getValeur()).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");
                            }

                        }

                    }

                    jsonAssujetti.put("complement", infosComplementaire);

                }

                String deviseCode = GeneralConst.EMPTY_STRING;
                String uniteCode = GeneralConst.EMPTY_STRING;

                Tarif tarif = new Tarif();
                BigDecimal tauxHA = new BigDecimal(propertiesConfig.getProperty("TAUX_ICM"));

                switch (assujeti.getArticleBudgetaire().getCode().trim()) {

                    case "00000000000002312021": //ICM
                    case "00000000000002312020": //ICM

                        jsonAssujetti.put("taux", 0);
                        jsonAssujetti.put("devise", "USD");

                        tarif = TaxationBusiness.getTarifByCode(assujeti.getBien().getFkTarif());

                        jsonAssujetti.put(AssujettissementConst.ParamName.TARIF, tarif == null ? "" : tarif.getIntitule().toUpperCase());
                        jsonAssujetti.put("tauxCumule", 0);
                        jsonAssujetti.put("typeTaux", "F");
                        jsonAssujetti.put("valeurBase", assujeti.getValeur());
                        jsonAssujetti.put("uniteAssuj", assujeti.getUniteValeur());
                        break;

                    case "00000000000002302020": // VIGNETTE

                        ComplementBienAutomobile complementBienAutomobile = TaxationBusiness.getComplementBienAutomobileByBien(assujeti.getBien().getId());

                        if (complementBienAutomobile != null) {

                            if (complementBienAutomobile.getFkMarque() != null) {

                                MarqueVehicule marqueVehicule = TaxationBusiness.getMarqueVehiculeByID(complementBienAutomobile.getFkMarque());
                                jsonAssujetti.put("marque", marqueVehicule == null ? "" : marqueVehicule.getIntitule().toUpperCase());
                            } else {
                                jsonAssujetti.put("marque", "");
                            }

                            if (complementBienAutomobile.getFkModele() != null) {

                                ModeleVehicule modeleVehicule = TaxationBusiness.getModeleVehiculeByID(complementBienAutomobile.getFkModele());

                                jsonAssujetti.put("modele", modeleVehicule == null ? "" : modeleVehicule.getIntitule().toUpperCase());
                            } else {
                                jsonAssujetti.put("modele", "");
                            }

                            jsonAssujetti.put("plaque", complementBienAutomobile == null ? "" : complementBienAutomobile.getNumeroPlaque());
                            jsonAssujetti.put("chassis", complementBienAutomobile == null ? "" : complementBienAutomobile.getNumeroChassis());

                            jsonAssujetti.put("puissanceFiscal", complementBienAutomobile == null ? "" : complementBienAutomobile.getPuissanceFiscale());

                            if (complementBienAutomobile != null) {

                                Unite unite = TaxationBusiness.getUnitebyCode(complementBienAutomobile.getUnitePuissanceFiscale());
                                jsonAssujetti.put("nameUnitePuissance", unite == null ? "" : unite.getIntitule().toUpperCase());
                            } else {
                                jsonAssujetti.put("nameUnitePuissance", "CV");
                            }

                        } else {
                            jsonAssujetti.put("marque", "");
                            jsonAssujetti.put("modele", "");
                            jsonAssujetti.put("plaque", "");
                            jsonAssujetti.put("chassis", "");
                            jsonAssujetti.put("puissanceFiscal", "");
                            jsonAssujetti.put("nameUnitePuissance", "CV");
                        }

                        jsonAssujetti.put("devise", "USD");
                        jsonAssujetti.put("uniteAssuj", "USD");

                        /*Palier palierABVignette = DeclarationBusiness.getTauxPalierVignette(
                         propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                         assujeti.getBien().getFkTarif(), assujeti.getPersonne().getFormeJuridique().getCode());*/
                        Palier palierABVignette = null;

                        if (assujeti.getBien().getFkTarif() != null 
                                && !assujeti.getBien().getFkTarif().isEmpty() 
                                && !assujeti.getBien().getFkTarif().equals("0")) {

                            switch (assujeti.getBien().getFkTarif()) {

                                case "0000071020": // 1 - 10 CV
                                case "0000071120": // 11 - 15 CV
                                case "0000071220": // +15 CV

                                    palierABVignette = DeclarationBusiness.getTauxPalierVignette(
                                            propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                            assujeti.getBien().getFkTarif(), assujeti.getPersonne().getFormeJuridique().getCode());

                                    break;

                                default:

                                    palierABVignette = DeclarationBusiness.getTauxPalierVignetteV2(
                                            propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"), assujeti.getBien().getFkTarif());

                                    break;

                            }

                            if (assujeti.getBien().getTypeBien() != null) {

                                if (assujeti.getBien().getTypeBien().getCode().equals(propertiesConfig.getProperty("CODE_TYPE_BIEN_AMBULANCE"))) {

                                    palierABVignette = DeclarationBusiness.getTauxPalierVignetteV3(
                                            propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                            assujeti.getBien().getFkTarif(),
                                            assujeti.getPersonne().getFormeJuridique().getCode(),
                                            propertiesConfig.getProperty("CODE_TYPE_BIEN_AMBULANCE"));
                                }
                            }

                        }

                        float tauxF = 0;

                        if (palierABVignette != null) {

                            if (palierABVignette.getMultiplierValeurBase() == 1) {

                                tauxF = (Float.valueOf(complementBienAutomobile.getPuissanceFiscale()) * palierABVignette.getTaux().floatValue());
                                jsonAssujetti.put("tauxCumule", tauxF);

                                jsonAssujetti.put("typeTaux", "%");

                            } else {
                                jsonAssujetti.put("tauxCumule", palierABVignette == null ? 0 : palierABVignette.getTaux());
                                jsonAssujetti.put("typeTaux", "F");
                            }

                            jsonAssujetti.put("taux", palierABVignette == null ? 0 : palierABVignette.getTaux());
                            jsonAssujetti.put("valeurBase", assujeti.getValeur());

                        } else {

                            jsonAssujetti.put("tauxCumule", 0);
                            jsonAssujetti.put("typeTaux", "");
                            jsonAssujetti.put("taux", 0);
                            jsonAssujetti.put("valeurBase", 0);
                        }

                        break;

                    default:

                        boolean isPalier = assujeti.getArticleBudgetaire().getPalier();

                        Palier palier = AssujettissementBusiness.getPalierForBienImmobilier(
                                assujeti.getArticleBudgetaire().getCode().trim(),
                                assujeti.getBien().getFkTarif(),
                                assujeti.getPersonne().getFormeJuridique().getCode(),
                                assujeti.getBien().getFkQuartier(),
                                assujeti.getValeur().floatValue(), isPalier,
                                assujeti.getBien().getTypeBien().getCode());

                        uniteCode = palier == null ? "" : palier.getUnite().getCode();

                        if (assujeti.getArticleBudgetaire().getCode().equals(propertiesConfig.getProperty("CODE_IMPOT_FONCIER"))
                                || assujeti.getArticleBudgetaire().getCode().equals(propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"))) {

                            deviseCode = palier == null ? "CDF" : palier.getDevise().getCode();
                        } else {
                            deviseCode = assujeti.getUniteValeur().toUpperCase();
                        }
                        
                        jsonAssujetti.put("uniteAssuj", deviseCode);

                        BigDecimal taux = new BigDecimal(BigInteger.ZERO);
                        BigDecimal tauxATCom = new BigDecimal(BigInteger.ZERO);

                        BigDecimal valeurBase = new BigDecimal(BigInteger.ONE);

                        if (palier != null) {

                            if (assujeti.getBien().getFkUsageBien() != null) {

                                if (assujeti.getBien().getFkUsageBien() > 1
                                        && (assujeti.getArticleBudgetaire().getCode().equals(propertiesConfig.getProperty("CODE_IMPOT_FONCIER")))) {

                                    switch (assujeti.getBien().getTypeBien().getCode()) {

                                        case "TB00000027":
                                        case "TB00000029":

                                            tauxATCom = BigDecimal.valueOf(Double.valueOf(
                                                    Tools.getTauxPalierATCommercial(assujeti.getPersonne().getFormeJuridique().getCode(),
                                                            assujeti.getBien().getFkTarif())));

                                            palier.setTaux(tauxATCom);

                                            break;
                                    }

                                }
                            }

                            if (palier.getTypeTaux().equals("F")) {

                                if (assujeti.getValeur().floatValue() == 0) {

                                    taux = palier.getMultiplierValeurBase() == 0 ? palier.getTaux()
                                            : palier.getTaux().multiply(valeurBase);

                                } else {

                                    taux = palier.getMultiplierValeurBase() == 0 ? palier.getTaux()
                                            : palier.getTaux().multiply(assujeti.getValeur());
                                }

                            } else {

                                taux = assujeti.getValeur().multiply(palier.getTaux()).divide(BigDecimal.valueOf(GeneralConst.Numeric.CENT));
                            }

                            jsonAssujetti.put("taux", palier.getTaux());

                        }

                        jsonAssujetti.put(AssujettissementConst.ParamName.TARIF, assujeti.getTarif().getIntitule());
                        jsonAssujetti.put("tauxCumule", taux);
                        jsonAssujetti.put("typeTaux", palier == null ? 0 : palier.getTypeTaux());

                        if (palier != null) {

                            if (palier.getTypeTaux().equals("%")) {

                                if (assujeti.getValeur().floatValue() > 0) {

                                    jsonAssujetti.put("valeurBase", assujeti.getValeur());
                                } else {
                                    jsonAssujetti.put("valeurBase", 0);

                                }

                            } else {
                                //jsonAssujetti.put("valeurBase", 0);
                                jsonAssujetti.put("valeurBase", assujeti.getValeur());
                            }
                        } else {
                            jsonAssujetti.put("valeurBase", 0);
                        }

                        jsonAssujetti.put(AssujettissementConst.ParamName.DEVISE, palier == null ? "" : palier.getDevise().getCode());
                        break;
                }

                long moisRetard = 0;

                int annee = 0;

                for (PeriodeDeclaration periode : assujeti.getPeriodeDeclarationList()) {

                    annee++;

                    IcmParam icmParam = AssujettissementBusiness.getIcmParamByTarifAndAnnee(assujeti.getBien().getFkTarif(), annee);

                    if (!assujeti.getEtat() && periode.getNoteCalcul() != null) {
                        continue;
                    }

                    RetraitDeclaration declaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(periode.getId().toString());

                    if (declaration == null) {

                        JSONObject jsonPeriode = new JSONObject();
                        jsonPeriode.put(AssujettissementConst.ParamName.PERIODE_ID, periode.getId());
                        jsonPeriode.put(AssujettissementConst.ParamName.PERIODE, Tools.getPeriodeIntitule(periode.getDebut(),
                                periode.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode()));

                        if (icmParam != null) {

                            jsonPeriode.put("tauxPeriode", icmParam.getTaux());
                            jsonPeriode.put("devise", icmParam.getDevise());
                            jsonPeriode.put("icmParamId", icmParam.getId());

                            BigDecimal valeurCuConvertiEnHA = new BigDecimal("0");

                            valeurCuConvertiEnHA = valeurCuConvertiEnHA.add(assujeti.getValeur().multiply(tauxHA));

                            BigDecimal valeurICM = new BigDecimal("0");

                            valeurICM = valeurICM.add(valeurCuConvertiEnHA.multiply(icmParam.getTaux()));

                            jsonPeriode.put("tauxCumule", valeurICM);

                        } else {

                            jsonPeriode.put("tauxPeriode", 0);
                            jsonPeriode.put("devise", GeneralConst.EMPTY_STRING);
                            jsonPeriode.put("icmParamId", GeneralConst.EMPTY_STRING);
                        }

                        if (periode.getDateLimite() != null) {

                            if (Compare.before(periode.getDateLimite(), new Date())) {

                                moisRetard = ConvertDate.getMonthsBetween(periode.getDateLimite(), new Date());

                            } else {
                                moisRetard = 0;
                            }

                        } else {

                            if (Compare.before(periode.getFin(), new Date())) {

                                moisRetard = ConvertDate.getMonthsBetween(periode.getFin(), new Date());

                            } else {
                                moisRetard = 0;
                            }
                        }

                        jsonPeriode.put(TaxationConst.ParamName.NBRE_MOIS, moisRetard);
                        jsonPeriode.put("estPenalise", moisRetard == 0 ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE);

                        int resultRecidiviste = PoursuiteBusiness.checkRecidivisteDeclaration(
                                periode.getAssujetissement().getId(), periode.getId());

                        jsonPeriode.put(TaxationConst.ParamName.EST_RECIDIVISTE, resultRecidiviste == 0 ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE);

                        jsonPeriodes.add(jsonPeriode);
                    }

                }

                Unite unite = AssujettissementBusiness.getUnite(uniteCode);
                TypeComplement complement = AssujettissementBusiness.getTypeComplementbyComplementBien(assujeti.getComplementBien());

                if (unite != null && complement != null) {

                    String valBase = !unite.getIntitule().equalsIgnoreCase("Montant") ? unite.getIntitule() : deviseCode;
                    jsonAssujetti.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN, complement.getIntitule() + GeneralConst.TWO_POINTS + assujeti.getValeur() + GeneralConst.SPACE + valBase);

                } else {

                    jsonAssujetti.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN, GeneralConst.EMPTY_STRING);
                }

                jsonAssujetti.put(AssujettissementConst.ParamName.LIST_PERIODES_DECLARATIONS, jsonPeriodes);

                dataAssujettisement.add(jsonAssujetti);

            }

            allDataAssujettisement.put("dataAB", dataAB.toString());
            allDataAssujettisement.put("dataAssujettisement", dataAssujettisement.toString());

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return allDataAssujettisement.toString();
    }

    public String loadTypeBiens() {

        List<JSONObject> jsonTypeBiens = new ArrayList<>();

        try {

            List<TypeBien> typeBiens = AssujettissementBusiness.getTypeBien();

            for (TypeBien typeBien : typeBiens) {
                JSONObject jsonTypeBien = new JSONObject();

                jsonTypeBien.put(AssujettissementConst.ParamName.CODE_TYPE_BIEN,
                        typeBien == null
                                ? GeneralConst.EMPTY_STRING
                                : typeBien.getCode());

                jsonTypeBien.put(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                        typeBien == null
                                ? GeneralConst.EMPTY_STRING
                                : typeBien.getIntitule());

                jsonTypeBien.put(AssujettissementConst.ParamName.ETAT,
                        typeBien == null
                                ? GeneralConst.EMPTY_STRING
                                : typeBien.getEtat());

                jsonTypeBien.put(AssujettissementConst.ParamName.EST_CONTRACTUEL,
                        typeBien == null
                                ? GeneralConst.EMPTY_STRING
                                : (typeBien.getContractuel())
                                        ? GeneralConst.Number.ONE
                                        : GeneralConst.Number.ZERO);

                jsonTypeBiens.add(jsonTypeBien);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonTypeBiens.toString();
    }

    public String loadBiensPersonne(HttpServletRequest request) {

        List<JSONObject> jsonBiens = new ArrayList<>();
        List<JSONObject> jsonAssujettis = new ArrayList<>();
        JSONObject jsonAssujettissement = new JSONObject();
        ArticleBudgetaire articleBudgetaire;
        String dateAcquitBien = GeneralConst.EMPTY_STRING;

        String codeFormeJurique = GeneralConst.EMPTY_STRING;

        String infosComplementaire = GeneralConst.EMPTY_STRING;

        try {

            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String forLocation = request.getParameter(AssujettissementConst.ParamName.FOR_LOCATION);
            String codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            String cmbTarif = request.getParameter("cmbTarif");
            String ab = request.getParameter("articleB");
            boolean estExonere = Boolean.valueOf(request.getParameter("estExonere"));

            List<Acquisition> acquisitions = new ArrayList<>();

            if (codeService.equals(propertiesConfig.getProperty("CODE_SERVICE_TRANSPORT"))) {
                acquisitions = AssujettissementBusiness.getBiensOfPersonne(codePersonne.trim(), codeService, estExonere, ab);
            } else if (codeService.equals(propertiesConfig.getProperty("CODE_SERVICE_CULTURE_ART"))) {

                switch (cmbTarif) {
                    case "*":
                        acquisitions = AssujettissementBusiness.getBiensOfPersonne(codePersonne.trim(), codeService, estExonere, ab);
                        break;
                    default:
                        acquisitions = AssujettissementBusiness.getBiensOfPersonneV4(codePersonne.trim(), codeService, cmbTarif, estExonere, ab);
                        break;
                }

            } else {
                acquisitions = AssujettissementBusiness.getBiensOfPersonneV3(codePersonne.trim(), estExonere, ab);
            }

            int _type = 0;

            for (Acquisition acquisition : acquisitions) {

                JSONObject jsonBien = new JSONObject();
                ComplementBien complementBien = new ComplementBien();

                Assujeti assujeti = TaxationBusiness.getAssujetiByPersonneAndBien(acquisition.getPersonne().getCode(), acquisition.getBien().getId());

                TypeBienService typeBienService = AssujettissementBusiness.getTypeBienServiceByTypeBien(acquisition.getBien().getTypeBien().getCode());

                _type = typeBienService.getType();

                codeFormeJurique = acquisition.getPersonne().getFormeJuridique().getCode();

                jsonBien.put(AssujettissementConst.ParamName.ID_ACQUISITION,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getId());

                jsonBien.put("type", _type);
                jsonBien.put("codeAB", assujeti == null ? "" : assujeti.getArticleBudgetaire().getCode());

//               dateAcquitBien = ConvertDate.getValidFormatDateString(acquisition.getDateAcquisition());
                jsonBien.put(AssujettissementConst.ParamName.DATE_ACQUISITION,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getDateAcquisition());

                jsonBien.put("dateAcquisition2",
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : ConvertDate.getValidFormatDateString(acquisition.getDateAcquisition()));

                jsonBien.put(AssujettissementConst.ParamName.CODE_TYPE_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getTypeBien() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : acquisition.getBien().getTypeBien().getCode());

                jsonBien.put(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getTypeBien() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : acquisition.getBien().getTypeBien().getIntitule().toUpperCase());

                jsonBien.put(AssujettissementConst.ParamName.ID_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getId());

                if (acquisition.getBien().getIntitule() != null) {

                    jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN, acquisition.getBien().getIntitule().toUpperCase());

                } else {
                    jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN, "");
                }

                jsonBien.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN,
                        acquisition.getBien().getDescription() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getDescription());

                if (acquisition.getBien().getFkCommune() != null && !acquisition.getBien().getFkCommune().isEmpty()) {

                    EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(
                            acquisition.getBien().getFkCommune());

                    String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                    jsonBien.put("communeCode", ea.getCode());
                    jsonBien.put("communeName", ea.getIntitule().toUpperCase().concat(ville));

                } else {
                    jsonBien.put("communeCode", GeneralConst.EMPTY_STRING);
                    jsonBien.put("communeName", GeneralConst.EMPTY_STRING);
                }

                if (acquisition.getBien().getFkQuartier() != null && !acquisition.getBien().getFkQuartier().isEmpty()) {

                    EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(
                            acquisition.getBien().getFkQuartier());

                    jsonBien.put("quartierCode", eaQuartier.getCode());
                    jsonBien.put("quartierName", eaQuartier.getIntitule().toUpperCase());

                } else {
                    jsonBien.put("quartierCode", GeneralConst.EMPTY_STRING);
                    jsonBien.put("quartierName", GeneralConst.EMPTY_STRING);
                }

                if (acquisition.getBien().getFkTarif() != null && !acquisition.getBien().getFkTarif().isEmpty()) {

                    Tarif tarif = TaxationBusiness.getTarifByCode(acquisition.getBien().getFkTarif());

                    jsonBien.put("tarifCode", tarif.getCode());
                    jsonBien.put("tarifName", tarif.getIntitule().toUpperCase());

                } else {
                    jsonBien.put("tarifCode", GeneralConst.EMPTY_STRING);
                    jsonBien.put("tarifName", GeneralConst.EMPTY_STRING);
                }

                infosComplementaire = GeneralConst.EMPTY_STRING;

                if (acquisition.getBien().getFkUsageBien() != null) {

                    UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(acquisition.getBien().getFkUsageBien());

                    jsonBien.put("usageCode", usageBien.getId());
                    jsonBien.put("usageName", usageBien.getIntitule().toUpperCase());
                    jsonBien.put("isImmobilier", GeneralConst.Number.ONE);

                } else {
                    jsonBien.put("usageCode", GeneralConst.EMPTY_STRING);
                    jsonBien.put("usageName", GeneralConst.EMPTY_STRING);
                    jsonBien.put("isImmobilier", GeneralConst.Number.ZERO);
                    //jsonBien.put("complement", GeneralConst.EMPTY_STRING);
                }

                String valUnity = GeneralConst.EMPTY_STRING;

                if (!acquisition.getBien().getComplementBienList().isEmpty()) {

                    for (ComplementBien cb : acquisition.getBien().getComplementBienList()) {

                        if (cb.getDevise() != null && !cb.getDevise().isEmpty()) {

                            switch (cb.getDevise()) {
                                case GeneralConst.Devise.DEVISE_CDF:
                                case GeneralConst.Devise.DEVISE_USD:

                                    valUnity = cb.getDevise();

                                    break;
                                default:
                                    Unite unite = TaxationBusiness.getUnitebyCode(cb.getDevise());
                                    valUnity = unite != null ? unite.getIntitule() : "";

                                    break;
                            }

                        } else {
                            valUnity = GeneralConst.EMPTY_STRING;
                        }

                        ValeurPredefinie valeurPredefinie = new ValeurPredefinie();
                        String txtValeurPredefinie = GeneralConst.EMPTY_STRING;

                        if (infosComplementaire.isEmpty()) {

                            if (cb.getTypeComplement().getComplement().getValeurPredefinie()) {

                                valeurPredefinie = AssujettissementBusiness.getValeurPredefinieByCode(cb.getValeur(), cb.getTypeComplement().getComplement().getCode());
                                txtValeurPredefinie = valeurPredefinie == null ? "" : valeurPredefinie.getValeur().toUpperCase();

                                infosComplementaire = cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(txtValeurPredefinie).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");

                            } else {

                                infosComplementaire = cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(cb.getValeur()).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");
                            }

                        } else {

                            if (cb.getTypeComplement().getComplement().getValeurPredefinie()) {

                                valeurPredefinie = AssujettissementBusiness.getValeurPredefinieByCode(cb.getValeur(), cb.getTypeComplement().getComplement().getCode());
                                txtValeurPredefinie = valeurPredefinie == null ? "" : valeurPredefinie.getValeur().toUpperCase();

                                infosComplementaire = infosComplementaire += cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(txtValeurPredefinie).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");

                            } else {
                                infosComplementaire = infosComplementaire += cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(cb.getValeur()).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");
                            }

                        }

                    }

                    jsonBien.put("complement", infosComplementaire);

                } else {
                    jsonBien.put("complement", "");
                }

                jsonBien.put(AssujettissementConst.ParamName.CHAINE_ADRESSE,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getFkAdressePersonne() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : acquisition.getBien().getFkAdressePersonne().getAdresse().toString());

                jsonBien.put(AssujettissementConst.ParamName.CODE_ADRESSE_PERSONNE,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getFkAdressePersonne() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : acquisition.getBien().getFkAdressePersonne().getCode());

                jsonBien.put(AssujettissementConst.ParamName.PROPRIETAIRE, acquisition.getProprietaire() == null
                        ? false : acquisition.getProprietaire());

                String responsable = GeneralConst.MY_SELF;
                String intituleBien = String.format(
                        GeneralConst.INTITULE_DATA,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getIntitule().toUpperCase());

                boolean isMaster = true;

                if (!acquisition.getProprietaire()) {

                    if (forLocation != null) {
                        if (forLocation.equals(GeneralConst.Number.ONE)) {
                            continue;
                        }
                    }

                    isMaster = false;

                    Bien bien = acquisition.getBien();
                    if (bien != null) {
                        Personne personne = AssujettissementBusiness.getResponsableBien(bien.getId());
                        if (personne != null) {
                            responsable = personne.toString();
                        }
                    }
                    intituleBien += GeneralConst.TEXT_LOCATION;

                }

                jsonBien.put("isMaster", isMaster);
                jsonBien.put(AssujettissementConst.ParamName.RESPONSABLE, responsable);
                jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN, intituleBien);
                jsonBiens.add(jsonBien);
            }

            List<Assujeti> assujettissements = new ArrayList<>();

            if (codeService.equals(propertiesConfig.getProperty("CODE_SERVICE_TRANSPORT"))) {
                assujettissements = AssujettissementBusiness.getAssujettissementByPersonne(codePersonne, codeService, estExonere, ab);
            } else {
                assujettissements = AssujettissementBusiness.getAssujettissementByPersonneV2(codePersonne, estExonere, ab);
            }

            for (Assujeti assujeti : assujettissements) {

                JSONObject jsonAssujetti = new JSONObject();

                jsonAssujetti.put(AssujettissementConst.ParamName.ID_ASSUJETTISSEMENT, assujeti.getId());
                Bien bien;

                if (assujeti.getBien() != null) {
                    bien = assujeti.getBien();
                    jsonAssujetti.put(AssujettissementConst.ParamName.ID_BIEN, bien.getId());
                    jsonAssujetti.put(AssujettissementConst.ParamName.INTITULE_BIEN, bien.getIntitule().toUpperCase());

                    if (bien.getFkAdressePersonne() != null) {
                        jsonAssujetti.put(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                bien.getFkAdressePersonne().getAdresse().toString());
                    } else {
                        jsonAssujetti.put(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonAssujetti.put("valeurBase", assujeti.getValeur() == null ? 0 : assujeti.getValeur());
                    jsonAssujetti.put("uniteValeurBase", assujeti.getUniteValeur() == null ? assujeti.getArticleBudgetaire().getUnite().getIntitule().toUpperCase() : assujeti.getUniteValeur());

                    String description = GeneralConst.EMPTY_STRING;
                    List<ComplementBien> complementBiens = bien.getComplementBienList();
                    if (!complementBiens.isEmpty()) {

                        for (ComplementBien cb : complementBiens) {

                            TypeComplement typeComplement;

                            typeComplement = cb.getTypeComplement().getComplement();

                            if (typeComplement.getTaxable()) {

                                String intitule = typeComplement.getIntitule();
                                String valeur = GeneralConst.EMPTY_STRING;

                                switch (typeComplement.getObjetInteraction()) {
                                    case "NOMBRE":
                                        if (cb.getDevise() != null && !cb.getDevise().isEmpty()) {
                                            String unite = TaxationBusiness.getUniteComplement(cb.getDevise());
                                            valeur = cb.getValeur() + " " + unite;
                                        } else {
                                            valeur = cb.getValeur();
                                        }

                                        break;
                                    case "MONTANT":

                                        String montant = "";

                                        if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {
                                            montant = formatNombreToString("###,###.###", BigDecimal.valueOf(Double.valueOf(cb.getValeur())));
                                        }

                                        String devise = GeneralConst.EMPTY_STRING;

                                        if (cb.getDevise() != null && !cb.getDevise().isEmpty()) {
                                            devise = " " + cb.getDevise();
                                        }

                                        valeur = montant + " " + devise;

                                        break;
                                    default:

                                }

                                if (description.isEmpty()) {
                                    description = " - " + intitule + " : <b>" + valeur + "</b>";
                                } else {
                                    description += "<br/> - " + intitule + " : <b>" + valeur + "</b>";
                                }

                            }
                        }

                        jsonAssujetti.put(AssujettissementConst.ParamName.INFO_COMPLEMENT_BIEN, description);

                    } else {

                        jsonAssujetti.put(AssujettissementConst.ParamName.INFO_COMPLEMENT_BIEN, GeneralConst.EMPTY_STRING);
                    }

                } else {

                    jsonAssujetti.put(AssujettissementConst.ParamName.ID_BIEN, GeneralConst.EMPTY_STRING);
                    jsonAssujetti.put(AssujettissementConst.ParamName.INTITULE_BIEN, GeneralConst.EMPTY_STRING);
                    jsonAssujetti.put(AssujettissementConst.ParamName.ADRESSE_BIEN, GeneralConst.EMPTY_STRING);
                    jsonAssujetti.put(AssujettissementConst.ParamName.INFO_COMPLEMENT_BIEN, GeneralConst.EMPTY_STRING);
                }

                jsonAssujetti.put(AssujettissementConst.ParamName.IS_MANY_ARTICLE_BUDGETAIRE, false);

                jsonAssujetti.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                        assujeti.getArticleBudgetaire() == null
                                ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCode());

                jsonAssujetti.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
                        assujeti.getArticleBudgetaire() == null
                                ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getIntitule());

                jsonAssujetti.put(AssujettissementConst.ParamName.SECTEUR_ACTIVITE,
                        assujeti.getArticleBudgetaire() == null || assujeti.getArticleBudgetaire().getArticleGenerique() == null
                        || assujeti.getArticleBudgetaire().getArticleGenerique().getServiceAssiette() == null
                                ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getArticleGenerique().getServiceAssiette().getIntitule());

                jsonAssujetti.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
                        assujeti.getArticleBudgetaire() == null
                                ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCodeOfficiel());

                jsonAssujetti.put(AssujettissementConst.ParamName.VALEUR_BASE,
                        assujeti.getValeur() == null ? GeneralConst.EMPTY_STRING : assujeti.getValeur());

                jsonAssujetti.put(AssujettissementConst.ParamName.ETAT_ASSUJETTISSEMENT,
                        assujeti.getEtat() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                if (assujeti.getArticleBudgetaire() != null) {

                    articleBudgetaire = assujeti.getArticleBudgetaire();

                    boolean isPalier = assujeti.getArticleBudgetaire().getPalier();

                    Palier palierAB = null;

                    switch (assujeti.getArticleBudgetaire().getCode()) {

                        case "00000000000002302020":

                            switch (assujeti.getBien().getFkTarif()) {

                                case "0000071020": // 1 - 10 CV
                                case "0000071120": // 11 - 15 CV
                                case "0000071220": // +15 CV

                                    palierAB = DeclarationBusiness.getTauxPalierVignette(
                                            propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                            assujeti.getBien().getFkTarif(), assujeti.getPersonne().getFormeJuridique().getCode());

                                    break;

                                default:

                                    palierAB = DeclarationBusiness.getTauxPalierVignetteV2(
                                            propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"), assujeti.getBien().getFkTarif());

                                    break;

                            }

                            break;

                        default:

                            palierAB = AssujettissementBusiness.getPalierForBienImmobilier(
                                    assujeti.getArticleBudgetaire().getCode().trim(),
                                    assujeti.getBien().getFkTarif(),
                                    assujeti.getPersonne().getFormeJuridique().getCode(),
                                    assujeti.getBien().getFkQuartier(),
                                    assujeti.getValeur().floatValue(), isPalier,
                                    assujeti.getBien().getTypeBien().getCode());

                            break;
                    }

                    jsonAssujetti.put("codeUnite", palierAB == null ? null : palierAB.getUnite().getCode());

                    AbComplementBien abComplementBien = AssujettissementBusiness.getAbComplementBienByArticleBudgetaire(articleBudgetaire.getCode());

                    if (abComplementBien != null) {

                        if (abComplementBien.getTypeComplementBien().getComplement().getObjetInteraction().equals(IdentificationConst.ObjetInteraction.MONTANT)) {

                            Palier palier = AssujettissementBusiness.getFistPalierByArticleBudgetaireV2(articleBudgetaire.getCode(), codeFormeJurique);

                            //jsonAssujetti.put(AssujettissementConst.ParamName.UNITE, palier == null ? GeneralConst.EMPTY_ZERO : palier.getDevise().getCode());
                            jsonAssujetti.put(AssujettissementConst.ParamName.UNITE, assujeti.getUniteValeur() == null ? GeneralConst.EMPTY_ZERO : assujeti.getUniteValeur());

                        } else {
                            //jsonAssujetti.put(AssujettissementConst.ParamName.UNITE, articleBudgetaire.getUnite().getIntitule());
                            jsonAssujetti.put(AssujettissementConst.ParamName.UNITE, assujeti.getUniteValeur() == null ? GeneralConst.EMPTY_ZERO : assujeti.getUniteValeur());
                        }
                    } else {
                        //jsonAssujetti.put(AssujettissementConst.ParamName.UNITE, articleBudgetaire.getUnite().getIntitule());
                        jsonAssujetti.put(AssujettissementConst.ParamName.UNITE, assujeti.getUniteValeur() == null ? GeneralConst.EMPTY_ZERO : assujeti.getUniteValeur());
                    }

                } else {
                    jsonAssujetti.put(AssujettissementConst.ParamName.UNITE, GeneralConst.EMPTY_STRING);
                }

                jsonAssujetti.put(AssujettissementConst.ParamName.DUREE,
                        assujeti.getDuree() == null ? GeneralConst.EMPTY_STRING : assujeti.getDuree());

                Date dateDebut = ConvertDate.formatDate(ConvertDate.getValidFormatDatePrint(assujeti.getDateDebut()));
                Date dateFin = ConvertDate.formatDate(ConvertDate.getValidFormatDatePrint(assujeti.getDateFin()));

                jsonAssujetti.put(AssujettissementConst.ParamName.DATE_DEBUT,
                        assujeti.getPeriodicite() == null ? GeneralConst.EMPTY_STRING : Tools.getPeriodeIntitule(dateDebut, assujeti.getArticleBudgetaire() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : assujeti.getArticleBudgetaire().getPeriodicite() == null
                                                        ? GeneralConst.EMPTY_STRING
                                                        : assujeti.getArticleBudgetaire().getPeriodicite().getCode()));

                jsonAssujetti.put(AssujettissementConst.ParamName.DATE_FIN,
                        assujeti.getPeriodicite() == null ? GeneralConst.EMPTY_STRING : Tools.getPeriodeIntitule(dateFin, assujeti.getArticleBudgetaire() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : assujeti.getArticleBudgetaire().getPeriodicite() == null
                                                        ? GeneralConst.EMPTY_STRING
                                                        : assujeti.getArticleBudgetaire().getPeriodicite().getCode()));

                jsonAssujetti.put(AssujettissementConst.ParamName.DATE_FIN2, assujeti.getDateFin() == null
                        ? GeneralConst.EMPTY_STRING
                        : ConvertDate.getValidFormatDatePrint(assujeti.getDateFin()));

                jsonAssujetti.put(AssujettissementConst.ParamName.TACITE_RECONDUCTION,
                        assujeti.getRenouvellement() == null ? GeneralConst.EMPTY_STRING : assujeti.getRenouvellement());

                jsonAssujetti.put(AssujettissementConst.ParamName.NOMBRE_JOUR,
                        assujeti.getArticleBudgetaire() == null
                                ? GeneralConst.EMPTY_STRING
                                : assujeti.getArticleBudgetaire().getPeriodicite().getNbrJour());

                jsonAssujetti.put(AssujettissementConst.ParamName.NOMBRE_JOUR_LIMITE,
                        assujeti.getArticleBudgetaire().getNbrJourLimite() == null
                                ? GeneralConst.EMPTY_STRING
                                : assujeti.getArticleBudgetaire().getNbrJourLimite());

                String nouvellesEcheances = assujeti.getNouvellesEcheances();
                String echeanceLegale, nbreJourLegalePaiement;

                if (nouvellesEcheances == null || nouvellesEcheances.isEmpty()) {

                    echeanceLegale = assujeti.getArticleBudgetaire().getEcheanceLegale() == null
                            ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getEcheanceLegale();

                    nbreJourLegalePaiement = assujeti.getArticleBudgetaire().getDateLimitePaiement() == null
                            ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getDateLimitePaiement();

                } else {

                    JSONObject nvlEcheanceJson = new JSONObject(nouvellesEcheances);
                    echeanceLegale = nvlEcheanceJson.getString("echeanceLegale");
                    nbreJourLegalePaiement = nvlEcheanceJson.getString("dateLimitePaiement");
                }

                jsonAssujetti.put(AssujettissementConst.ParamName.ECHEANCE_LEGALE, echeanceLegale);
                jsonAssujetti.put(AssujettissementConst.ParamName.NBRE_JOUR_LEGALE_PAIEMENT, nbreJourLegalePaiement);

                if (assujeti.getArticleBudgetaire() == null) {
                    jsonAssujetti.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE, GeneralConst.EMPTY_STRING);
                } else {
                    if (assujeti.getArticleBudgetaire().getPeriodeEcheance() != null) {
                        jsonAssujetti.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE,
                                assujeti.getArticleBudgetaire().getPeriodeEcheance() == true
                                        ? GeneralConst.Number.ONE
                                        : GeneralConst.Number.ZERO);
                    } else {
                        jsonAssujetti.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE, GeneralConst.EMPTY_STRING);
                    }
                }

                Periodicite periodicite = AssujettissementBusiness.getPeriodiciteByCode(assujeti.getPeriodicite());

                jsonAssujetti.put(AssujettissementConst.ParamName.CODE_PERIODICITE_AB,
                        assujeti.getArticleBudgetaire().getCode() == null
                                ? GeneralConst.EMPTY_STRING
                                : assujeti.getPeriodicite());

                jsonAssujetti.put(AssujettissementConst.ParamName.LIBELLE_PERIODICITE_AB,
                        assujeti.getArticleBudgetaire().getPeriodicite() == null
                                ? GeneralConst.EMPTY_STRING
                                : periodicite == null ? "" : periodicite.getIntitule().toUpperCase());

                jsonAssujetti.put(AssujettissementConst.ParamName.MONTANT, GeneralConst.Numeric.ZERO);
                jsonAssujetti.put(AssujettissementConst.ParamName.DEVISE, GeneralConst.Devise.DEVISE_USD);

                jsonAssujetti.put(AssujettissementConst.ParamName.INTITULE_TARIF, assujeti.getTarif() == null
                        ? GeneralConst.EMPTY_STRING
                        : assujeti.getTarif().getIntitule());

                List<JSONObject> jsonPeriodes = new ArrayList<>();

                for (PeriodeDeclaration periode : assujeti.getPeriodeDeclarationList()) {

                    if (!assujeti.getEtat() && periode.getNoteCalcul() == null) {
                        continue;
                    }

                    JSONObject jsonPeriode = new JSONObject();
                    jsonPeriode.put(AssujettissementConst.ParamName.PERIODE_ID, periode.getId());
                    jsonPeriode.put(AssujettissementConst.ParamName.PERIODE, Tools.getPeriodeIntitule(periode.getDebut(),
                            periode.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode()));

                    AdressePersonne adressePersonne = periode.getAssujetissement().getFkAdressePersonne();

                    jsonPeriode.put(TaxationOfficeConst.ParamName.libelleAdresse, (adressePersonne == null) ? GeneralConst.EMPTY_STRING : adressePersonne.getAdresse().toString());
                    jsonPeriode.put(TaxationOfficeConst.ParamName.dateLimite, periode.getDateLimite() != null ? Tools.formatDateToStringV2(periode.getDateLimite()) : GeneralConst.EMPTY_STRING);
                    jsonPeriode.put(TaxationOfficeConst.ParamName.dateLimitePaiement, periode.getDateLimitePaiement() != null ? Tools.formatDateToStringV2(periode.getDateLimitePaiement()) : GeneralConst.EMPTY_STRING);

                    jsonPeriode.put(TaxationOfficeConst.ParamName.statePeriodeDeclaration, periode.getEtat());

                    jsonPeriode.put(AssujettissementConst.ParamName.OBSERVATION, periode.getObservation() != null
                            ? periode.getObservation() : GeneralConst.EMPTY_STRING);

                    if (periode.getEtat() == GeneralConst.Numeric.ZERO) {

                        Agent agent = GeneralBusiness.getAgentByCode(String.valueOf(periode.getAgentMaj()));

                        if (agent != null) {
                            jsonPeriode.put(AssujettissementConst.ParamName.AGENT_MAJ, agent.toString().toUpperCase());
                        } else {
                            jsonPeriode.put(AssujettissementConst.ParamName.AGENT_MAJ, GeneralConst.EMPTY_STRING);
                        }

                        String dateMaj = ConvertDate.formatDateHeureToStringV2(periode.getDateMaj());

                        jsonPeriode.put(AssujettissementConst.ParamName.DATE_MAJ, dateMaj);

                    } else {
                        jsonPeriode.put(AssujettissementConst.ParamName.AGENT_MAJ, GeneralConst.EMPTY_STRING);
                        jsonPeriode.put(AssujettissementConst.ParamName.DATE_MAJ, GeneralConst.EMPTY_STRING);
                    }
                    //here

                    RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(periode.getId() + "");

                    jsonPeriode.put(TaxationOfficeConst.ParamName.dateDeclaration, retraitDeclaration == null ? "" : Tools.formatDateWithTimeToString(retraitDeclaration.getDateCreate()));
                    jsonPeriode.put(TaxationOfficeConst.ParamName.noteTaxation, retraitDeclaration == null ? "" : retraitDeclaration.getCodeDeclaration());
                    jsonPeriode.put(TaxationOfficeConst.ParamName.notePerception, retraitDeclaration == null ? "" : retraitDeclaration.getCodeDeclaration());

                    jsonPeriode.put("montantDu", retraitDeclaration == null ? "" : retraitDeclaration.getMontant());
                    jsonPeriode.put("devise", retraitDeclaration == null ? "" : retraitDeclaration.getDevise());

                    if (retraitDeclaration != null) {

                        if (retraitDeclaration.getRetraitDeclarationMere() != null) {

                            RetraitDeclaration rdPenalite = AssujettissementBusiness.getRetraitDeclarationByCodeMere(
                                    retraitDeclaration.getRetraitDeclarationMere());

                            jsonPeriode.put("penaliteDu", rdPenalite.getMontant());

                        } else {
                            jsonPeriode.put("penaliteDu", 0);
                        }

                        Bordereau bordereau = GeneralBusiness.getBordereauByDeclaration(retraitDeclaration.getCodeDeclaration());

                        if (bordereau != null) {
                            jsonPeriode.put("montantPayer", bordereau.getTotalMontantPercu());
                        } else {
                            jsonPeriode.put("montantPayer", 0);
                        }

                    } else {
                        jsonPeriode.put("penaliteDu", 0);
                        jsonPeriode.put("montantPayer", 0);
                    }

                    jsonPeriode.put(TaxationOfficeConst.ParamName.estPenalise,
                            GeneralConst.Number.ZERO);

                    jsonPeriode.put(TaxationOfficeConst.ParamName.estPenalisePaiement,
                            GeneralConst.Number.ZERO);

                    if (periode.getDateLimite() != null) {
                        if (Compare.before(periode.getDateLimite(), new Date())) {
                            jsonPeriode.put(TaxationOfficeConst.ParamName.estPenalise,
                                    GeneralConst.Number.ONE);
                        }
                    }

                    if (periode.getDateLimitePaiement() != null) {

                        if (Compare.before(periode.getDateLimitePaiement(), new Date())) {
                            jsonPeriode.put(TaxationOfficeConst.ParamName.estPenalisePaiement,
                                    GeneralConst.Number.ONE);
                        }
                    }

                    if (periode.getNoteCalcul() != null) {

                        NoteCalcul nc = TaxationBusiness.getNoteCalculByNumero(periode.getNoteCalcul());

                        BigDecimal montantDu = TaxationBusiness.getMontantTotalDuDetailNoteCalculv2(periode.getNoteCalcul());
                        BigDecimal totalDu = montantDu;

                        String devise = Tools.getDeviseByNC(nc);

                        if (nc.getDateCloture() != null) {
                            jsonPeriode.put("waittingClosing", "0");
                        } else {
                            jsonPeriode.put("waittingClosing", "1");
                        }

                        jsonPeriode.put(TaxationOfficeConst.ParamName.devise, devise);
                        jsonPeriode.put(TaxationOfficeConst.ParamName.noteTaxation, periode.getNoteCalcul());
                        jsonPeriode.put(TaxationOfficeConst.ParamName.dateDeclaration, Tools.formatDateToString(nc.getDateCreat()));
                        jsonPeriode.put(TaxationOfficeConst.ParamName.montantDu, montantDu);

                        jsonAssujetti.put(AssujettissementConst.ParamName.MONTANT, montantDu);
                        jsonAssujetti.put(AssujettissementConst.ParamName.DEVISE, devise);

                        jsonPeriode.put(TaxationConst.ParamName.CODE_OFFICIEL,
                                periode.getAssujetissement().getArticleBudgetaire().getCodeOfficiel().toUpperCase());

                        FichePriseCharge fpch = TaxationBusiness.getPriseEnChargeByNP(nc.getNumero().trim());

                        if (fpch != null) {

                            jsonPeriode.put(TaxationOfficeConst.ParamName.penaliteDu, fpch.getTotalPenalitedu());
                            totalDu = totalDu.add(fpch.getTotalPenalitedu());

                        } else {

                            jsonPeriode.put(TaxationOfficeConst.ParamName.penaliteDu, GeneralConst.Numeric.ZERO);
                        }

                        jsonPeriode.put(TaxationOfficeConst.ParamName.totalDu, totalDu);
                        jsonPeriode.put(TaxationOfficeConst.ParamName.montantpayer, GeneralConst.Numeric.ZERO);
                        jsonPeriode.put(TaxationOfficeConst.ParamName.resteApayer, totalDu);

                        NotePerception np = nc.getDetailsNcList().get(GeneralConst.Numeric.ZERO).getNotePerception();

                        if (np != null) {

                            jsonPeriode.put(TaxationOfficeConst.ParamName.notePerception, np.getNumero());
                            jsonPeriode.put(TaxationOfficeConst.ParamName.notePerceptionManuel, np.getNumero());
                            jsonPeriode.put(TaxationOfficeConst.ParamName.montantDu, np.getNetAPayer());

                            if (np.getNbrImpression() > GeneralConst.Numeric.ZERO) {
                                jsonPeriode.put(TaxationOfficeConst.ParamName.devise, np.getDevise());
                            }

                            EtatSoldePaiement etatSoldePaiement = AcquitLiberatoireBusiness.getEtatSoldePaiementByNc(np.getNoteCalcul().getNumero());

                            Double solde = Double.valueOf(GeneralConst.Number.ZERO);

                            if (etatSoldePaiement != null) {

                                jsonPeriode.put(TaxationOfficeConst.ParamName.totalDu, etatSoldePaiement.getDebit());
                                jsonPeriode.put(TaxationOfficeConst.ParamName.montantpayer, Math.abs(etatSoldePaiement.getCredit()));

                                if (etatSoldePaiement.getSolde() < GeneralConst.Numeric.ZERO) {

                                    solde = Math.abs(etatSoldePaiement.getSolde());
                                }

                                jsonPeriode.put(TaxationOfficeConst.ParamName.resteApayer, solde);
                            }

                        } else {
                            jsonPeriode.put(TaxationOfficeConst.ParamName.notePerceptionManuel,
                                    GeneralConst.EMPTY_STRING);
                        }

                    }

                    jsonPeriodes.add(jsonPeriode);
                }

                jsonAssujetti.put(AssujettissementConst.ParamName.LIST_PERIODES_DECLARATIONS, jsonPeriodes);

                jsonAssujettis.add(jsonAssujetti);
            }

            jsonAssujettissement.put(AssujettissementConst.ParamName.LIST_BIENS, jsonBiens);
            jsonAssujettissement.put(AssujettissementConst.ParamName.LIST_ARTICLE_BUDGETAIRES, jsonAssujettis);

        } catch (JSONException | NumberFormatException e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonAssujettissement.toString();
    }

    public String loadTypeComplementBiens(HttpServletRequest request) {

        JSONObject data = new JSONObject();

        try {

            List<JSONObject> jsonTypeComplementBiens = new ArrayList<>();

            infoComplementaires = null;

            String codeTypeBien = request.getParameter(AssujettissementConst.ParamName.CODE_TYPE_BIEN);
            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);
            String codePersonne = request.getParameter("codePersonne");

            int number = AssujettissementBusiness.getNumberOfBienPersonne(codeTypeBien, codePersonne);

            Personne personne = DeclarationBusiness.getPersonneByCode(codePersonne);

            data.put("numberOfBien", number);
            data.put("nomePersonne", personne == null ? "" : personne.getNom().toUpperCase());

            if (idBien.equals(GeneralConst.EMPTY_STRING) || idBien == null) {
                data.put("bien", GeneralConst.EMPTY_STRING);
            } else {
                List<ComplementBien> complementBienList = AssujettissementBusiness.getListComplementBien(idBien);
                if (complementBienList == null || complementBienList.isEmpty()) {
                    data.put("bien", GeneralConst.EMPTY_STRING);
                } else {
                    data.put("bien", getValeurComplementBiens(complementBienList));
                }
            }

            jsonTypeComplementBiens = getTypeComplementBiens(codeTypeBien);

            data.put("typeComplementBienList", jsonTypeComplementBiens);

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return data.toString();
    }

    public List<JSONObject> getTypeComplementBiens(String codeTypeBien) {

        List<JSONObject> jsonTypeComplementBiens = new ArrayList<>();

        try {
            List<TypeComplementBien> typeComplementBiens = AssujettissementBusiness
                    .getTypeComplementBienByTypeBien(codeTypeBien);

            if (typeComplementBiens != null) {
                int counter = 0;
                for (TypeComplementBien typeComplementBien : typeComplementBiens) {

                    JSONObject jsonTypeComplementBien = new JSONObject();
                    TypeComplement typeComplement = typeComplementBien.getComplement();

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getCode());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.LIBELLE_TYPE_COMPLEMENT,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getIntitule());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.OBJET_INTERACTION,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getObjetInteraction());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.VALEUR_PREDEFINIE,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getValeurPredefinie());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.INPUT_VALUE,
                            createControl(typeComplementBien, counter));

                    jsonTypeComplementBiens.add(jsonTypeComplementBien);
                    counter++;
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
        }
        return jsonTypeComplementBiens;
    }

    public static String createControl(final TypeComplementBien typeComplementBien, int counter) {

        String CONTROL_ID = GeneralConst.PREFIX_IDENTIFIANT_KEY.concat(String.valueOf(counter));
        String value = GeneralConst.EMPTY_STRING;

        TypeComplement typeComplement = typeComplementBien.getComplement();

        final String OBJET_INTERACTION = (typeComplement == null)
                ? GeneralConst.EMPTY_STRING : typeComplement.getObjetInteraction();

        final String CONTROL_VALUE = (typeComplement == null)
                ? GeneralConst.EMPTY_STRING : typeComplement.getIntitule();

        final boolean HAS_VALUES_LIST = (typeComplement == null)
                ? false : typeComplement.getValeurPredefinie();

        if (HAS_VALUES_LIST) {

            value = getControlList(
                    CONTROL_ID,
                    CONTROL_VALUE,
                    typeComplementBien,
                    GeneralConst.EMPTY_STRING);

        } else {

            switch (OBJET_INTERACTION) {
                case AssujettissementConst.ObjetInteraction.EMPTY:
                    value = getInputControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            AssujettissementConst.TypeData.TYPE_TEXT,
                            typeComplementBien);
                    break;
                case AssujettissementConst.ObjetInteraction.DATE:
                    value = getInputControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            AssujettissementConst.TypeData.TYPE_DATE,
                            typeComplementBien);
                    break;
                case AssujettissementConst.ObjetInteraction.MONTANT:
                    value = getBoxControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            AssujettissementConst.TypeData.TYPE_NUMBER,
                            typeComplementBien, true);
                    break;
                case AssujettissementConst.ObjetInteraction.NOMBRE:
                    value = getBoxControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            AssujettissementConst.TypeData.TYPE_NUMBER,
                            typeComplementBien, false);
                    break;
            }
        }

        return value;
    }

    public static String getBoxControl(String controlId, String controlValue, String type, TypeComplementBien typeComplementBien, boolean hasDevise) {

        String selectId, options = GeneralConst.EMPTY_STRING;

        if (hasDevise) {

            selectId = controlId + GeneralConst.DASH_SEPARATOR + GeneralConst.FIRST_LETTRE_DEVISE;

            if (devises == null) {
                devises = TaxationBusiness.getListAllDevises();
            }

            if (devises != null) {
                options = "<option value=\"0\">--- Devise ---</option>";
                for (Devise devise : devises) {
                    options += String.format(AssujettissementConst.ParamControl.ITEM_OPTION,
                            devise.getCode(),
                            GeneralConst.EMPTY_STRING,
                            devise.getIntitule());
                }
            }

        } else {

            selectId = controlId + GeneralConst.DASH_SEPARATOR + GeneralConst.FIRST_LETTRE_UNITE;

            if (unites == null) {
                unites = TaxationBusiness.getListAllUnites();
            }

            if (unites != null) {
                options = "<option value=\"0\">--- Unité ---</option>";
                for (Unite unite : unites) {
                    options += String.format(AssujettissementConst.ParamControl.ITEM_OPTION,
                            unite.getCode(),
                            GeneralConst.EMPTY_STRING,
                            unite.getIntitule());
                }
            }

        }

        ComplementData complementData = getKeyNameComplement(typeComplementBien);

        String requiredValue = construstRequiredValue(complementData == null
                ? GeneralConst.Number.ZERO
                : complementData.keyName);

        String value = String.format(AssujettissementConst.ParamControl.BOX_CONTROL,
                controlId,
                controlValue,
                requiredValue,
                type,
                controlId,
                controlValue,
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.keyName == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.keyName),
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.complement == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.complement.getValeur()),
                selectId,
                options);

        return value;
    }

    public static String getControlList(String controlId, String controlValue, TypeComplementBien typeComplementBien, final String objetInteraction) {

        ComplementData complementData = getKeyNameComplement(typeComplementBien);

        String options = String.format(AssujettissementConst.ParamControl.ITEM_OPTION,
                GeneralConst.Number.ZERO,
                GeneralConst.EMPTY_STRING,
                GeneralConst.TEXT_SELECTED_VALUE);

        TypeComplement typeComplement = (typeComplementBien == null) ? null : typeComplementBien.getComplement();
        if (typeComplement != null) {
            List<ValeurPredefinie> valeurPredefinies = typeComplement.getValeurPredefinieList();
            if (valeurPredefinies != null) {
                for (ValeurPredefinie valeurPredefinie : valeurPredefinies) {
                    options += String.format(AssujettissementConst.ParamControl.ITEM_OPTION,
                            valeurPredefinie.getCode(),
                            selectedOption(complementData == null
                                            ? null
                                            : complementData.complement, valeurPredefinie.getCode()),
                            valeurPredefinie.getValeur().toUpperCase());
                }
            }
        }

        String requiredValue = construstRequiredValue(complementData == null
                ? GeneralConst.Number.ZERO
                : complementData.keyName);

        String value = String.format(AssujettissementConst.ParamControl.COMPLEMENT_LIST,
                controlId,
                controlValue,
                requiredValue,
                controlId,
                controlValue,
                complementData == null ? GeneralConst.EMPTY_STRING : complementData.keyName,
                options);

        return value;
    }

    public static class ComplementData {

        public String keyName;
        public ComplementBien complement;
    }

    public static ComplementData getKeyNameComplement(TypeComplementBien typeComplementBien) {

        ComplementData complementData = new ComplementData();

        complementData.complement = getCorrespondingComplement(typeComplementBien);

        String idComplement = (complementData.complement == null)
                ? GeneralConst.Number.ZERO
                : complementData.complement.getId();

        TypeComplement typeComplement = typeComplementBien.getComplement();

        complementData.keyName = idComplement
                .concat(GeneralConst.DASH_SEPARATOR)
                .concat(typeComplementBien.getCode())
                .concat(GeneralConst.DASH_SEPARATOR)
                .concat(typeComplement == null
                                ? GeneralConst.Number.ZERO
                                : (typeComplementBien.getComplement().getObligatoire()
                                        ? GeneralConst.Number.ONE
                                        : GeneralConst.Number.ZERO));
        return complementData;
    }

    public static ComplementBien getCorrespondingComplement(TypeComplementBien typeComplementBien) {

        if (infoComplementaires != null) {
            for (ComplementBien complementBien : infoComplementaires) {
                if (complementBien.getTypeComplement().equals(typeComplementBien)) {
                    return complementBien;
                }
            }
        }

        return null;
    }

    public static String selectedOption(ComplementBien complementBien, String item) {

        String result = GeneralConst.EMPTY_STRING;
        if (complementBien != null) {
            if (complementBien.getValeur().trim().equals(item.trim())) {
                return AssujettissementConst.ParamControl.SELECTED_OPTION_ATTR;
            }
        }
        return result;
    }

    public static String construstRequiredValue(String codeTCB) {

        String requiredValue;
        String[] array = codeTCB.split(GeneralConst.DASH_SEPARATOR);

        if (array.length > 1) {
            requiredValue = array[2].equals(GeneralConst.Number.ZERO)
                    ? GeneralConst.EMPTY_STRING
                    : AssujettissementConst.ParamControl.SPAN_REQUIRED_VALUE;
        } else {
            requiredValue = GeneralConst.EMPTY_STRING;
        }
        return requiredValue;
    }

    public static String getInputControl(String controlId, String controlValue, String type, TypeComplementBien typeComplementBien) {

        ComplementData complementData = getKeyNameComplement(typeComplementBien);

        String requiredValue = construstRequiredValue(complementData == null
                ? GeneralConst.Number.ZERO
                : complementData.keyName);

        String value = String.format(AssujettissementConst.ParamControl.INPUT_CONTROL,
                controlId,
                controlValue,
                requiredValue,
                type,
                controlId,
                controlValue,
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.keyName == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.keyName),
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.complement == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.complement.getValeur()));
        return value;
    }

    public String loadAdresses(HttpServletRequest request) {

        List<JSONObject> jsonAdresses = new ArrayList<>();
        try {

            String libelle = request.getParameter(IdentificationConst.ParamName.LIBELLE);
            List<EntiteAdministrative> avenues = IdentificationBusiness.getEntiteAdministratives(libelle == null
                    ? GeneralConst.EMPTY_STRING
                    : libelle);

            for (EntiteAdministrative avenue : avenues) {

                JSONObject jsonAdresse = new JSONObject();
                jsonAdresse.put(IdentificationConst.ParamName.CODE_AVENUE, avenue == null
                        ? GeneralConst.EMPTY_STRING : avenue.getCode());

                jsonAdresse.put(IdentificationConst.ParamName.AVENUE, avenue == null
                        ? GeneralConst.EMPTY_STRING : avenue.getIntitule());

                if (avenue != null) {
                    EntiteAdministrative quartier = avenue.getEntiteMere();
                    jsonAdresse.put(IdentificationConst.ParamName.CODE_QUARTIER, quartier == null
                            ? GeneralConst.EMPTY_STRING : quartier.getCode());

                    jsonAdresse.put(IdentificationConst.ParamName.QUARTIER, quartier == null
                            ? GeneralConst.EMPTY_STRING : quartier.getIntitule());

                    if (quartier != null) {
                        EntiteAdministrative commune = quartier.getEntiteMere();
                        jsonAdresse.put(IdentificationConst.ParamName.CODE_COMMUNE, commune == null
                                ? GeneralConst.EMPTY_STRING : commune.getCode());

                        jsonAdresse.put(IdentificationConst.ParamName.COMMUNE, commune == null
                                ? GeneralConst.EMPTY_STRING : commune.getIntitule());

                        if (commune != null) {
                            EntiteAdministrative district = commune.getEntiteMere();
                            jsonAdresse.put(IdentificationConst.ParamName.CODE_DISTRICT, district == null
                                    ? GeneralConst.EMPTY_STRING : district.getCode());

                            jsonAdresse.put(IdentificationConst.ParamName.DISTRICT, district == null
                                    ? GeneralConst.EMPTY_STRING : district.getIntitule());

                            if (district != null) {
                                EntiteAdministrative ville = district.getEntiteMere();
                                jsonAdresse.put(IdentificationConst.ParamName.CODE_VILLE, ville == null
                                        ? GeneralConst.EMPTY_STRING : ville.getCode());

                                jsonAdresse.put(IdentificationConst.ParamName.VILLE, ville == null
                                        ? GeneralConst.EMPTY_STRING : ville.getIntitule());

                                if (ville != null) {
                                    EntiteAdministrative province = ville.getEntiteMere();
                                    jsonAdresse.put(IdentificationConst.ParamName.CODE_PROVINCE, province == null
                                            ? GeneralConst.EMPTY_STRING : province.getCode());

                                    jsonAdresse.put(IdentificationConst.ParamName.PROVINCE, province == null
                                            ? GeneralConst.EMPTY_STRING : province.getIntitule());
                                }
                            }
                        }
                    }
                }
                jsonAdresses.add(jsonAdresse);
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonAdresses.toString();
    }

    public String createBien(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;

        try {
            String codeTypeBien = request.getParameter(AssujettissementConst.ParamName.CODE_TYPE_BIEN);
            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);
            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String intituleBien = request.getParameter(AssujettissementConst.ParamName.INTITULE_BIEN);
            String descriptionBien = request.getParameter(AssujettissementConst.ParamName.DESCRIPTION_BIEN);
            String dateAcquisition = request.getParameter(AssujettissementConst.ParamName.DATE_ACQUISITION);
            String codeAP = request.getParameter(AssujettissementConst.ParamName.CODE_ADRESSE_PERSONNE);
            String codeCategorie = request.getParameter("codeCategorie");

            String idUser = request.getParameter(GeneralConst.ID_USER);

            JSONArray jsonComplementBiens = new JSONArray(request.getParameter(AssujettissementConst.ParamName.COMPLEMENTS_BIEN));

            Bien bien = new Bien();
            bien.setDescription(descriptionBien);
            bien.setIntitule(intituleBien);
            bien.setTypeBien(new TypeBien(codeTypeBien));
            bien.setFkAdressePersonne(codeAP.equals("") ? null : new AdressePersonne(codeAP));
            bien.setFkTarif(codeCategorie);

            Acquisition acquisition = new Acquisition();
            acquisition.setDateAcquisition(dateAcquisition);
            acquisition.setPersonne(new Personne(codePersonne));

            List<ComplementBien> complementBiens = getComplementBiens(jsonComplementBiens);

            if (idBien != null && !idBien.equals(GeneralConst.EMPTY_STRING)) {
                bien.setId(idBien);
                if (AssujettissementBusiness.updateBien(bien, acquisition, complementBiens, Integer.valueOf(idUser))) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            } else {
                if (AssujettissementBusiness.saveBien(bien, acquisition, complementBiens, Integer.valueOf(idUser))) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String createBienImmobilier(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;

        try {
            String codeTypeBien = request.getParameter(AssujettissementConst.ParamName.CODE_TYPE_BIEN);
            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);
            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String intituleBien = request.getParameter(AssujettissementConst.ParamName.INTITULE_BIEN);
            String descriptionBien = request.getParameter(AssujettissementConst.ParamName.DESCRIPTION_BIEN);
            String dateAcquisition = request.getParameter(AssujettissementConst.ParamName.DATE_ACQUISITION);
            String codeAP = request.getParameter(AssujettissementConst.ParamName.CODE_ADRESSE_PERSONNE);

            String codeUsage = request.getParameter("codeUsage");
            String codeCommune = request.getParameter("codeCommune");
            String codeCategorie = request.getParameter("codeCategorie");
            String codeQuartier = request.getParameter("codeQuartier");

            if (codeCommune.equals("0")) {
                codeCommune = null;
            }

            if (codeCategorie.equals("0")) {
                codeCategorie = null;
            }

            if (codeQuartier.equals("0")) {
                codeQuartier = null;
            }

            String idUser = request.getParameter(GeneralConst.ID_USER);

            JSONArray jsonComplementBiens = new JSONArray(request.getParameter(AssujettissementConst.ParamName.COMPLEMENTS_BIEN));

            Bien bien = new Bien();
            bien.setDescription(descriptionBien);
            bien.setIntitule(intituleBien);
            bien.setTypeBien(new TypeBien(codeTypeBien));
            bien.setFkAdressePersonne(codeAP.equals("") ? null : new AdressePersonne(codeAP));

            bien.setFkCommune(codeCommune);
            bien.setFkTarif(codeCategorie);
            bien.setFkUsageBien(Integer.valueOf(codeUsage));
            bien.setFkQuartier(codeQuartier);

            Acquisition acquisition = new Acquisition();
            acquisition.setDateAcquisition(dateAcquisition);
            acquisition.setPersonne(new Personne(codePersonne));

            List<ComplementBien> complementBiens = getComplementBiens(jsonComplementBiens);

            if (idBien != null && !idBien.equals(GeneralConst.EMPTY_STRING)) {
                bien.setId(idBien);
                if (AssujettissementBusiness.updateBienImmobilier(bien, acquisition, complementBiens, Integer.valueOf(idUser))) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            } else {
                if (AssujettissementBusiness.saveBienImmobilier(bien, acquisition, complementBiens, Integer.valueOf(idUser))) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public List<ComplementBien> getComplementBiens(JSONArray complementsArray) {

        List<ComplementBien> complementBiens = new ArrayList<>();
        try {
            for (int i = 0; i < complementsArray.length(); i++) {
                JSONObject jsonobject = complementsArray.getJSONObject(i);
                String id = jsonobject.getString(AssujettissementConst.ParamName.ID_COMPLEMENT_BIEN);
                String code = jsonobject.getString(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT_BIEN);
                String valeur = jsonobject.getString(AssujettissementConst.ParamName.VALEUR_COMPLEMENT_BIEN);
                String unite = jsonobject.getString(AssujettissementConst.ParamName.UNITE_DEVISE);

                ComplementBien complementBien = new ComplementBien();
                complementBien.setId(id.equals(GeneralConst.Number.ZERO) ? GeneralConst.EMPTY_STRING : id);
                complementBien.setTypeComplement(new TypeComplementBien(code));
                complementBien.setValeur(valeur);
                complementBien.setDevise(unite.equals(GeneralConst.EMPTY_STRING) ? null : unite);
                complementBiens.add(complementBien);
            }
        } catch (JSONException e) {
            CustumException.LogException(e);
        }
        return complementBiens;
    }

    public String loadBiens(HttpServletRequest request) {

        List<JSONObject> jsonBiens = new ArrayList<>();
        JSONObject jsonAssujettissement = new JSONObject();

        try {

            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);

            List<Acquisition> acquisitions = AssujettissementBusiness.getBiensByIdBienAndPersonne(codePersonne, idBien);

            for (Acquisition acquisition : acquisitions) {
                JSONObject jsonBien = new JSONObject();

                jsonBien.put(AssujettissementConst.ParamName.ID_ACQUISITION,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getId());

                jsonBien.put(AssujettissementConst.ParamName.CHAINE_ADRESSE,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getFkAdressePersonne() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : acquisition.getBien().getFkAdressePersonne().getAdresse().toString());

                jsonBien.put(AssujettissementConst.ParamName.CODE_ADRESSE_PERSONNE,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getFkAdressePersonne() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : acquisition.getBien().getFkAdressePersonne().getCode());

                jsonBien.put(AssujettissementConst.ParamName.ID_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getId());

                jsonBien.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getDescription());

                jsonBien.put(AssujettissementConst.ParamName.DATE_ACQUISITION,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getDateAcquisition());

                jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getIntitule());

                String responsable = GeneralConst.MY_SELF;
                String intituleBien = acquisition.getBien() == null
                        ? GeneralConst.EMPTY_STRING
                        : acquisition.getBien().getIntitule();

                jsonBien.put(AssujettissementConst.ParamName.CODE_PERSONNE,
                        acquisition.getPersonne() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getPersonne().getCode());

                jsonBien.put(AssujettissementConst.ParamName.PERSONNE_ACQUIT,
                        acquisition.getPersonne() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getPersonne().toString());

                Bien bien = acquisition.getBien();
                List<JSONObject> jsonListComplement = new ArrayList<>();

                if (bien != null) {

                    List<ComplementBien> listComplementBien = new ArrayList<>();

                    if (!acquisition.getProprietaire()) {
                        Personne personne = AssujettissementBusiness.getResponsableBien(bien.getId());
                        if (personne != null) {
                            responsable = personne.toString();
                        }
                        intituleBien += " <br/>(En location)";
                    }
                    listComplementBien = AssujettissementBusiness.getListComplementBien(bien.getId());
                    if (listComplementBien.size() > 0) {
                        for (ComplementBien cplBien : listComplementBien) {

                            JSONObject jsonComplement = new JSONObject();

                            jsonComplement.put(AssujettissementConst.ParamName.ID_BIEN,
                                    cplBien.getBien() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : cplBien.getBien().getId());

                            jsonComplement.put(AssujettissementConst.ParamName.ID_COMPLEMENT_BIEN,
                                    cplBien.getId() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : cplBien.getId());

                            jsonComplement.put(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT_BIEN,
                                    cplBien.getTypeComplement() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : cplBien.getTypeComplement().getCode());

                            jsonComplement.put(AssujettissementConst.ParamName.VALEUR_COMPLEMENT_BIEN,
                                    cplBien.getValeur() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : cplBien.getValeur());

                            jsonListComplement.add(jsonComplement);

                        }
                    }
                }
                jsonBien.put(AssujettissementConst.ParamName.COMPLEMENTS_BIEN, jsonListComplement);
                jsonBien.put(AssujettissementConst.ParamName.RESPONSABLE, responsable);
                jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN, intituleBien);

                jsonBiens.add(jsonBien);
            }

            jsonAssujettissement.put(AssujettissementConst.ParamName.LIST_BIENS, jsonBiens);

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonAssujettissement.toString();
    }

    public String desactivateBienAcquisition(HttpServletRequest request) {

        String result;

        try {

            String idAcquisition = request.getParameter(AssujettissementConst.ParamName.ID_ACQUISITION);

            if (AssujettissementBusiness.removeBienAcquisition(idAcquisition)) {

                result = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String desactivateBienAcquisitionV2(HttpServletRequest request) {

        String result;

        try {

            String idAcquisition = request.getParameter(AssujettissementConst.ParamName.ID_ACQUISITION);

            Acquisition acquisition = AssujettissementBusiness.getAcquisistionByCode(idAcquisition);

            if (AssujettissementBusiness.removeBienAcquisitionV2(idAcquisition, acquisition.getBien().getId(), acquisition.getPersonne().getCode())) {

                result = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String createAcquisition(HttpServletRequest request) {

        String result;

        try {
            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);
            String dateAcquisition = request.getParameter(AssujettissementConst.ParamName.DATE_ACQUISITION);
            String referenceContrat = request.getParameter(AssujettissementConst.ParamName.REFERENCE_CONTRAT);
            String numActeNotarie = request.getParameter(AssujettissementConst.ParamName.NUM_ACTE_NOTARIE);
            String dateActeNotarie = request.getParameter(AssujettissementConst.ParamName.DATE_ACTE_NOTARIE);

            Acquisition acquisition = new Acquisition();
            acquisition.setPersonne(new Personne(codePersonne));
            acquisition.setBien(new Bien(idBien));
            acquisition.setDateAcquisition(dateAcquisition);
            acquisition.setProprietaire(false);
            acquisition.setReferenceContrat(referenceContrat);
            acquisition.setNumActeNotarie(numActeNotarie);
            acquisition.setDateActeNotarie(dateActeNotarie);

            if (AssujettissementBusiness.createAcquisition(acquisition)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String loadArticlesBudgetaires(HttpServletRequest request) {

        List<JSONObject> jsonArticlesBudgetaires = new ArrayList<>();

        try {

            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);
            String ab = request.getParameter(AssujettissementConst.ParamName.ID_AB);
            String type = request.getParameter(AssujettissementConst.ParamName.FORME_JURIDIQUE);

            ArticleBudgetaire articleBudgetaire;
            Bien bien = null;
            ComplementBien cBien = null;
            List<AbComplementBien> abComplementBiens = new ArrayList<>();

            if (!ab.isEmpty()) {

                String impotFoncier = propertiesConfig.getProperty("CODE_IMPOT_FONCIER");
                String impotIcm = propertiesConfig.getProperty("CODE_IMPOT_ICM");
                String taxePublicite = propertiesConfig.getProperty("CODE_TAXE_PUBLICITE");
                String paramComplementBien = propertiesConfig.getProperty("PARAM_TYPE_BIEN_COMPLEMENT_TAXABLE");

                articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(ab);
                bien = AssujettissementBusiness.getBienById(idBien);

                if (ab.equals(impotFoncier) || ab.equals(impotIcm) || ab.equals(taxePublicite)) {

                    Gson gson = new Gson();

                    Type listType = new TypeToken<List<ComplementMockIF>>() {
                    }.getType();

                    List<ComplementMockIF> complementMockIFs = gson.fromJson(paramComplementBien, listType);

                    String coComplementTyBien;

                    for (ComplementMockIF complementMockIF : complementMockIFs) {

                        if (complementMockIF.getTypeBien().equals(bien.getTypeBien().getCode())) {

                            coComplementTyBien = complementMockIF.getTypeComplementBien();
                            cBien = AssujettissementBusiness.getComplementBien(coComplementTyBien, idBien);
                        }

                    }

                }

                if (articleBudgetaire != null) {

                    AbComplementBien abCompl = new AbComplementBien();
                    abCompl.setArticleBudgetaire(articleBudgetaire);
                    abComplementBiens.add(abCompl);
                }

            } else {

                abComplementBiens = AssujettissementBusiness.getAssujettissableAbByBien(idBien);

            }

            for (AbComplementBien abComplementBien : abComplementBiens) {

                articleBudgetaire = abComplementBien.getArticleBudgetaire();
                TypeComplementBien typeComplementBien = abComplementBien.getTypeComplementBien();

                String codeTypeComplement = typeComplementBien == null
                        ? GeneralConst.EMPTY_STRING
                        : typeComplementBien.getComplement().getCode();

                String codeTypeComplementBien = typeComplementBien == null
                        ? GeneralConst.EMPTY_STRING
                        : typeComplementBien.getCode();

                JSONObject jsonArticleBudgetaire = new JSONObject();

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.INTITULE_COMPLEMENT_IF,
                        cBien == null
                                ? GeneralConst.EMPTY_STRING
                                : cBien.getTypeComplement().getComplement().getIntitule());

                /*if (cBien.getTypeComplement().getComplement().getValeurPredefinie()) {

                 } else {
                 jsonArticleBudgetaire.put(AssujettissementConst.ParamName.VALEUR_COMPLEMENT_IF,
                 cBien == null
                 ? GeneralConst.EMPTY_STRING
                 : cBien.getValeur());
                 }*/
                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.VALEUR_COMPLEMENT_IF,
                        cBien == null
                                ? GeneralConst.EMPTY_STRING
                                : cBien.getValeur());

                switch (ab) {

                    case "00000000000002312021":
                    case "00000000000002272020":

                        if (cBien.getDevise() != null) {

                            Unite unite = TaxationBusiness.getUnitebyCode(cBien.getDevise());

                            jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_UNITE_COMPLEMENT,
                                    unite == null
                                            ? GeneralConst.EMPTY_STRING
                                            : unite.getIntitule());
                        } else {
                            jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_UNITE_COMPLEMENT, GeneralConst.EMPTY_STRING);
                        }

                        break;
                    /*case "00000000000002302020": // VIGNETTE
                     jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_UNITE_COMPLEMENT, "CV");
                     break;*/

                    default:
                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_UNITE_COMPLEMENT,
                                cBien == null
                                        ? GeneralConst.EMPTY_STRING
                                        : cBien.getDevise());
                        break;
                }

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getCode());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getIntitule());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getCodeOfficiel());

                jsonArticlesBudgetaires.add(jsonArticleBudgetaire);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_PERIODICITE_AB,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getPeriodicite().getCode());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.LIBELLE_PERIODICITE_AB,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getPeriodicite().getIntitule());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_TARIF_AB,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getTarif().getCode());

                String tarif = bien != null ? bien.getFkTarif() : GeneralConst.EMPTY_STRING;

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_TARIF_BIEN, tarif);

                List<JSONObject> jsonPaliers = getJsonPaliersByAB(articleBudgetaire, tarif, type);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PALIERS, jsonPaliers);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT,
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : codeTypeComplement);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.LIBELLE_TYPE_COMPLEMENT,
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : typeComplementBien.getComplement().getIntitule());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.BASE_VARIABLE,
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : (typeComplementBien.getComplement().getBaseVariable())
                                        ? GeneralConst.Number.ONE
                                        : GeneralConst.Number.ZERO);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODICITE_VARIABLE,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getPeriodiciteVariable());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.TARIF_VARIABLE,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getTarifVariable());

                ComplementBien complementBien = AssujettissementBusiness.getComplementBien(codeTypeComplementBien, idBien);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT_BIEN,
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : typeComplementBien.getCode());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.ID_COMPLEMENT_BIEN,
                        complementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : complementBien.getId());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.VALEUR_COMPLEMENT_BIEN,
                        complementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : complementBien.getValeur());

                if (complementBien != null) {

                    if (complementBien.getTypeComplement().getComplement().getObjetInteraction().equals(IdentificationConst.ObjetInteraction.MONTANT)) {
                        Palier palier = AssujettissementBusiness.getFistPalierByArticleBudgetaire(articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getCode());
                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.UNITE, palier.getDevise().getCode());
                    } else {
                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.UNITE, articleBudgetaire.getUnite().getIntitule());
                    }
                } else {
                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.UNITE, GeneralConst.EMPTY_STRING);
                }

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.NOMBRE_JOUR_LIMITE,
                        articleBudgetaire == null ? GeneralConst.Number.ZERO
                                : articleBudgetaire.getNbrJourLimite());

                String echeanceLegale = articleBudgetaire == null ? GeneralConst.EMPTY_STRING
                        : articleBudgetaire.getEcheanceLegale() == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getEcheanceLegale();

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.ECHEANCE_LEGALE, echeanceLegale);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.NBRE_JOUR_LEGALE_PAIEMENT,
                        articleBudgetaire == null ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getDateLimitePaiement() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getDateLimitePaiement());

                if (articleBudgetaire == null) {
                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE, GeneralConst.EMPTY_STRING);
                } else {
                    if (articleBudgetaire.getPeriodeEcheance() != null) {
                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE,
                                articleBudgetaire.getPeriodeEcheance() == true
                                        ? GeneralConst.Number.ONE
                                        : GeneralConst.Number.ZERO);
                    } else {
                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE, GeneralConst.EMPTY_STRING);
                    }
                }

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonArticlesBudgetaires.toString();
    }

    public List<JSONObject> getJsonPaliersByAB(ArticleBudgetaire articleBudgetaire, String tarif, String type) {

        List<JSONObject> jsonPaliers = new ArrayList<>();

        try {

            List<Palier> paliers = AssujettissementBusiness.getListPaliersByAB(articleBudgetaire.getCode(), type, tarif);

            if (paliers != null) {

                for (Palier palier : paliers) {

                    JSONObject jsonPalier = new JSONObject();

                    jsonPalier.put(AssujettissementConst.ParamName.CODE_TARIF, palier.getTarif() == null
                            ? GeneralConst.EMPTY_STRING : palier.getTarif().getCode());

                    jsonPalier.put(AssujettissementConst.ParamName.PALIER_TYPE_TAUX, palier.getTypeTaux() == null
                            ? GeneralConst.EMPTY_STRING : palier.getTypeTaux());

                    jsonPalier.put(AssujettissementConst.ParamName.UNITE, palier.getUnite() == null
                            ? GeneralConst.EMPTY_STRING : palier.getUnite().getIntitule());

                    jsonPalier.put(AssujettissementConst.ParamName.TAUX_AB, palier.getTaux() == null
                            ? GeneralConst.Number.ZERO : palier.getTaux());

                    jsonPalier.put(AssujettissementConst.ParamName.CODE_TYPE_PERSONNE,
                            palier.getTypePersonne() == null ? GeneralConst.ALL : palier.getTypePersonne().getCode());

                    jsonPalier.put(AssujettissementConst.ParamName.DEVISE, palier.getDevise() == null
                            ? GeneralConst.Number.ZERO : palier.getDevise().getCode());

                    jsonPalier.put(AssujettissementConst.ParamName.BORNE_INFERIEUR, palier.getBorneInferieure());

                    jsonPalier.put(AssujettissementConst.ParamName.BORNE_SUPERIEUR, palier.getBorneSuperieure());

                    jsonPalier.put(AssujettissementConst.ParamName.MULTIPLIER_VALEUR_BASE,
                            palier.getMultiplierValeurBase().equals(Short.valueOf(GeneralConst.Number.ONE))
                                    ? GeneralConst.Number.ONE : palier.getTypeTaux().equals("%")
                                            ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                    jsonPaliers.add(jsonPalier);
                }

            } else {
                JSONObject jsonPalier = new JSONObject();

                jsonPalier.put(AssujettissementConst.ParamName.CODE_TARIF, articleBudgetaire.getTarif() == null
                        ? GeneralConst.EMPTY_STRING : articleBudgetaire.getTarif().getCode());

                jsonPalier.put(AssujettissementConst.ParamName.TAUX_AB, articleBudgetaire.getTarif() == null
                        ? GeneralConst.EMPTY_STRING
                        : articleBudgetaire.getTarif().getValeur() + articleBudgetaire.getTarif().getTypeValeur());

                jsonPalier.put(AssujettissementConst.ParamName.MULTIPLIER_VALEUR_BASE, GeneralConst.Number.ONE);

                jsonPaliers.add(jsonPalier);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
        }

        return jsonPaliers;
    }

    public List<JSONObject> getJsonPaliersByAB(List<Palier> paliers) {

        List<JSONObject> jsonPaliers = new ArrayList<>();

        try {

            for (Palier palier : paliers) {

                JSONObject jsonPalier = new JSONObject();

                jsonPalier.put(AssujettissementConst.ParamName.CODE_TARIF, palier.getTarif() == null
                        ? GeneralConst.EMPTY_STRING : palier.getTarif().getCode());

                jsonPalier.put(AssujettissementConst.ParamName.INTITULE_TARIF, palier.getTarif() == null
                        ? GeneralConst.EMPTY_STRING : palier.getTarif().getIntitule());

                jsonPalier.put(AssujettissementConst.ParamName.PALIER_TYPE_TAUX, palier.getTypeTaux() == null
                        ? GeneralConst.EMPTY_STRING : palier.getTypeTaux());

                jsonPalier.put(AssujettissementConst.ParamName.UNITE, palier.getUnite() == null
                        ? GeneralConst.EMPTY_STRING : palier.getUnite().getIntitule());

                jsonPalier.put(AssujettissementConst.ParamName.TAUX_AB, palier.getTaux() == null
                        ? GeneralConst.Number.ZERO : palier.getTaux());

                jsonPalier.put(AssujettissementConst.ParamName.CODE_TYPE_PERSONNE,
                        palier.getTypePersonne() == null ? GeneralConst.ALL : palier.getTypePersonne().getCode());

                jsonPalier.put(AssujettissementConst.ParamName.DEVISE, palier.getDevise() == null
                        ? GeneralConst.Number.ZERO : palier.getDevise().getCode());

                jsonPalier.put(AssujettissementConst.ParamName.BORNE_INFERIEUR, palier.getBorneInferieure());

                jsonPalier.put(AssujettissementConst.ParamName.BORNE_SUPERIEUR, palier.getBorneSuperieure());

                jsonPalier.put(AssujettissementConst.ParamName.MULTIPLIER_VALEUR_BASE,
                        palier.getMultiplierValeurBase().equals(Short.valueOf(GeneralConst.Number.ONE))
                                ? GeneralConst.Number.ONE : palier.getTypeTaux().equals("%")
                                        ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                jsonPaliers.add(jsonPalier);

            }

        } catch (Exception e) {
            CustumException.LogException(e);
        }

        return jsonPaliers;
    }

    public String loadPeriodiciteAndTarifs(HttpServletRequest request) {

        JSONObject jsonDataAssujettissement = new JSONObject();
        List<JSONObject> jsonPeriodicites = new ArrayList<>();
        List<JSONObject> jsonTarifs = new ArrayList<>();

        try {

            String codeAB = request.getParameter(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE);
            String formeJuridique = request.getParameter(AssujettissementConst.ParamName.FORME_JURIDIQUE);
            String tarifBien = request.getParameter(AssujettissementConst.ParamName.TARIF);
            String codeBien = request.getParameter("codeBien");
            String codePersonne = request.getParameter("codePersonne");

            List<Periodicite> periodicites = AssujettissementBusiness.getListPeriodicites();

            for (Periodicite periodicite : periodicites) {

                JSONObject jsonPeriodicite = new JSONObject();

                jsonPeriodicite.put(AssujettissementConst.ParamName.CODE_PERIODICITE,
                        periodicite == null
                                ? GeneralConst.EMPTY_STRING
                                : periodicite.getCode());

                jsonPeriodicite.put(AssujettissementConst.ParamName.INTITULE_PERIODICITE,
                        periodicite == null
                                ? GeneralConst.EMPTY_STRING
                                : periodicite.getIntitule());

                jsonPeriodicite.put(AssujettissementConst.ParamName.NOMBRE_JOUR,
                        periodicite == null
                                ? GeneralConst.Number.ZERO
                                : periodicite.getNbrJour());

                jsonPeriodicites.add(jsonPeriodicite);
            }

            Bien bien = AssujettissementBusiness.getBienByCode(codeBien);

            Acquisition acquisition = AssujettissementBusiness.getAcquisistionByLocataire(codePersonne, codeBien);

            if (acquisition != null) {
                jsonDataAssujettissement.put("monthPart", acquisition.getDateAcquisition().equals("") ? "" : Tools.getPartMonthDate(ConvertDate.formatDateV2(acquisition.getDateAcquisition())));
            } else {
                jsonDataAssujettissement.put("monthPart", "");
            }

            String quartier = bien == null ? "" : bien.getFkQuartier();

            if (formeJuridique.isEmpty()) {
                formeJuridique = bien.getAcquisitionList().get(0).getPersonne().getFormeJuridique().getCode();
            }

            List<Palier> paliers = AssujettissementBusiness.getListPaliersByABV2(codeAB, formeJuridique, tarifBien, quartier, bien.getTypeBien().getCode());

            for (Palier palier : paliers) {

                Tarif tarif = palier.getTarif();

                JSONObject jsonTarif = new JSONObject();

                jsonTarif.put(AssujettissementConst.ParamName.CODE_TARIF,
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : tarif.getCode());

                jsonTarif.put(AssujettissementConst.ParamName.INTITULE_TARIF,
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : tarif.getIntitule().toUpperCase());

                jsonTarifs.add(jsonTarif);
            }

            jsonDataAssujettissement.put(AssujettissementConst.ParamName.LIST_PERIODICITES, jsonPeriodicites);
            jsonDataAssujettissement.put(AssujettissementConst.ParamName.LIST_TARIFS, jsonTarifs);
            List<JSONObject> jsonPaliers = getJsonPaliersByAB(paliers);
            jsonDataAssujettissement.put(AssujettissementConst.ParamName.PALIERS, jsonPaliers);

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonDataAssujettissement.toString();
    }

    public String getPeriodesDeclarations(HttpServletRequest request) {

        List<JSONObject> jsonPeriodes = new ArrayList();

        try {

            String codePeriodicite = request.getParameter(AssujettissementConst.ParamName.CODE_PERIODICITE_AB);
            String mois = request.getParameter(AssujettissementConst.ParamName.MOIS);
            String annee = request.getParameter(AssujettissementConst.ParamName.ANNEE);
            String nombreJour = request.getParameter(AssujettissementConst.ParamName.NOMBRE_JOUR);
            String nombreJourLimite = request.getParameter(AssujettissementConst.ParamName.NOMBRE_JOUR_LIMITE);
            String echeanceLegale = request.getParameter(AssujettissementConst.ParamName.ECHEANCE_LEGALE);
            String periodeEcheance = request.getParameter(AssujettissementConst.ParamName.PERIODE_ECHEANCE);
            String nombreJourLimitePaiement = request.getParameter(AssujettissementConst.ParamName.NBRE_JOUR_LEGALE_PAIEMENT);

            int nombreDeJours = Integer.parseInt(nombreJour.equals(GeneralConst.EMPTY_STRING)
                    ? GeneralConst.Number.ZERO
                    : nombreJour);

            jsonPeriodes = generatePeriodesDeclarations(
                    codePeriodicite,
                    GeneralConst.Number.ZERO,
                    mois,
                    annee,
                    nombreDeJours,
                    nombreJourLimite,
                    echeanceLegale,
                    nombreJourLimitePaiement,
                    false,
                    periodeEcheance
            );

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return jsonPeriodes.toString();
    }

    public String getPeriodesDeclarationsV2(HttpServletRequest request) {

        List<JSONObject> jsonPeriodes = new ArrayList();
        List<JSONObject> jsonObjList = new ArrayList();
        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeAssuj = request.getParameter("codeAssuj");

            Assujeti assujeti = AssujettissementBusiness.getAssujettissementDrhkatByCode(codeAssuj);

            String codePeriodicite = GeneralConst.EMPTY_STRING;
            String mois = GeneralConst.EMPTY_STRING;
            //String annee = GeneralConst.EMPTY_STRING;
            String nombreJour = GeneralConst.EMPTY_STRING;
            String nombreJourLimite = GeneralConst.EMPTY_STRING;
            String echeanceLegale = GeneralConst.EMPTY_STRING;
            String periodeEcheance = GeneralConst.EMPTY_STRING;
            String nombreJourLimitePaiement = GeneralConst.EMPTY_STRING;

            String beginPeriod = Tools.getPeriodeIntitule(ConvertDate.formatDateV2(assujeti.getDateDebut()),
                    assujeti.getArticleBudgetaire().getPeriodicite().getCode());

            if (assujeti != null) {

                codePeriodicite = assujeti.getArticleBudgetaire().getPeriodicite().getCode();

                nombreJour = assujeti.getArticleBudgetaire().getPeriodicite().getNbrJour() + "";
                echeanceLegale = assujeti.getArticleBudgetaire().getEcheanceLegale() == null ? "1" : assujeti.getArticleBudgetaire().getEcheanceLegale();
                nombreJourLimitePaiement = assujeti.getArticleBudgetaire().getDateLimitePaiement() == null ? assujeti.getDateFin() : assujeti.getArticleBudgetaire().getDateLimitePaiement();

                periodeEcheance = "1";

                String anneeX = assujeti.getDateDebut().replaceAll("-", ",");
                String[] anneeTab = anneeX.split(GeneralConst.VIRGULE);

                int anneeOld = Integer.valueOf(anneeTab[0]);
                int anneeNew = Integer.valueOf(anneeTab[0]);
                int montNow = Integer.valueOf(anneeTab[1]);

                switch (codePeriodicite) {

                    case "PR0032015": //Mensuelle
                        mois = anneeTab[1];
                        break;
                    default:
                        mois = "0";
                        break;
                }

                anneeNew = (anneeOld - 5);

                int nombreDeJours = Integer.parseInt(nombreJour.equals(GeneralConst.EMPTY_STRING)
                        ? GeneralConst.Number.ZERO
                        : nombreJour);

                jsonPeriodes = generatePeriodesDeclarationsV2(
                        codePeriodicite,
                        GeneralConst.Number.ZERO,
                        mois,
                        anneeNew + "",
                        nombreDeJours,
                        nombreJourLimite,
                        echeanceLegale,
                        nombreJourLimitePaiement,
                        false,
                        periodeEcheance,
                        anneeOld + "", montNow
                );

                JSONObject jSONObject = new JSONObject();

                jSONObject.put("beginPeriod", beginPeriod);
                jSONObject.put("periodeList", jsonPeriodes);

                jsonObjList.add(jSONObject);

                dataReturn = jsonObjList.toString();

            } else {
                return dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getPeriodesDeclarationsV3(String exercice) {

        List<JSONObject> jsonPeriodes = new ArrayList();

        try {

            ArticleBudgetaire articleBudgetaire = DeclarationBusiness.getArticleBudgetaireByCode("00000000000002302020");

            String codePeriodicite = "PR0042015";
            String mois = "0";
            String annee = exercice;
            String nombreJour = "365";
            String nombreJourLimite = "0";
            String echeanceLegale = articleBudgetaire.getEcheanceLegale();
            String periodeEcheance = "";
            String nombreJourLimitePaiement = articleBudgetaire.getEcheanceLegale();

            int nombreDeJours = Integer.parseInt(nombreJour.equals(GeneralConst.EMPTY_STRING)
                    ? GeneralConst.Number.ZERO
                    : nombreJour);

            jsonPeriodes = generatePeriodesDeclarations(
                    codePeriodicite,
                    GeneralConst.Number.ZERO,
                    mois,
                    annee,
                    nombreDeJours,
                    nombreJourLimite,
                    echeanceLegale,
                    nombreJourLimitePaiement,
                    false,
                    periodeEcheance
            );

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return jsonPeriodes.toString();
    }

    public List<JSONObject> generatePeriodesDeclarations(
            String codePeriodicite, String jour, String mois, String annee, int nombreJour,
            String nombreJourLimite, String echeanceLegale, String nombreJourLimitePaiement, boolean forTaxation, String periodeEcheance) {

        final String PONC = "PR0012015",
                JOUR = "PR0022015",
                MENS = "PR0032015",
                TRIME = "PR0052015",
                SEMS = "PR0072015",
                ANNEE = "PR0042015";

        List<JSONObject> jsonPeriodes = new ArrayList();

        Date dateDepart = ConvertDate.formatDate("01/" + mois + GeneralConst.SEPARATOR_SLASH_NO_SPACE + annee);
        int anneeDepart = Integer.parseInt(annee);
        int moisDepart = Integer.parseInt(mois);

        String DATE_DAY_ECHANCE = GeneralConst.EMPTY_STRING, DATE_MONTH_ECHEANCE = GeneralConst.EMPTY_STRING;
        String DATE_DAY_ECHANCE_PAIEMENT = GeneralConst.EMPTY_STRING, DATE_MONTH_ECHEANCE_PAIEMENT = GeneralConst.EMPTY_STRING;

        if (!echeanceLegale.equals(GeneralConst.Number.ZERO) && !echeanceLegale.equals(GeneralConst.EMPTY_STRING)) {
            String[] dateArray = echeanceLegale.split(GeneralConst.DASH_SEPARATOR);
            DATE_DAY_ECHANCE = dateArray[2];
            DATE_MONTH_ECHEANCE = dateArray[1];
        }

        if (!nombreJourLimitePaiement.equals(GeneralConst.Number.ZERO) && !nombreJourLimitePaiement.equals(GeneralConst.EMPTY_STRING)) {
            String[] dateArray = nombreJourLimitePaiement.split(GeneralConst.DASH_SEPARATOR);
            DATE_DAY_ECHANCE_PAIEMENT = dateArray[2];
            DATE_MONTH_ECHEANCE_PAIEMENT = dateArray[1];
        }

        switch (codePeriodicite) {
            case PONC:
            case JOUR:

                int length = 1;
                Date dateJour = new Date();
                if (forTaxation) {
                    length = 10;
                    dateJour = ConvertDate.formatDate(jour + "/" + mois + "/" + annee);
                    dateJour = ConvertDate.addDayOfDate(dateJour, 1);
                }

                jsonPeriodes = PeriodiciteData.getPeriodeJournaliere(dateJour, length, codePeriodicite, nombreJourLimitePaiement);
                break;

            case MENS:
            case "BIMEN":
            case TRIME:
            case SEMS:

                if (forTaxation) {
                    moisDepart += 1;
                    if (moisDepart > 12) {
                        moisDepart = 1;
                        anneeDepart += 1;
                    }
                }

                jsonPeriodes = PeriodiciteData.getPeriodesMensuelles(
                        nombreJour,
                        forTaxation,
                        dateDepart,
                        moisDepart,
                        anneeDepart,
                        DATE_DAY_ECHANCE,
                        DATE_DAY_ECHANCE_PAIEMENT,
                        codePeriodicite,
                        periodeEcheance);
                break;

            case ANNEE:
            case "2ANS":
            case "5ANS":

                anneeDepart = (forTaxation) ? (anneeDepart + 1) : anneeDepart;

                jsonPeriodes = PeriodiciteData.getPeriodesAnnuelles(
                        nombreJour,
                        forTaxation,
                        dateDepart,
                        anneeDepart,
                        DATE_DAY_ECHANCE,
                        DATE_MONTH_ECHEANCE,
                        DATE_DAY_ECHANCE_PAIEMENT,
                        DATE_MONTH_ECHEANCE_PAIEMENT,
                        codePeriodicite,
                        periodeEcheance);
        }

        return jsonPeriodes;
    }

    public List<JSONObject> generatePeriodesDeclarationsV2(
            String codePeriodicite, String jour, String mois, String anneeNew, int nombreJour,
            String nombreJourLimite, String echeanceLegale, String nombreJourLimitePaiement, boolean forTaxation, String periodeEcheance,
            String anneeOld, int numberMonthNow) {

        final String PONC = "PR0012015",
                JOUR = "PR0022015",
                MENS = "PR0032015",
                TRIME = "PR0052015",
                SEMS = "PR0072015",
                ANNEE = "PR0042015";

        List<JSONObject> jsonPeriodes = new ArrayList();

        Date dateDepart = ConvertDate.formatDate("01/" + mois + GeneralConst.SEPARATOR_SLASH_NO_SPACE + anneeNew);
        Date dateDepartOld = ConvertDate.formatDate("01/" + mois + GeneralConst.SEPARATOR_SLASH_NO_SPACE + anneeOld);
        int anneeDepart = Integer.parseInt(anneeNew);
        int moisDepart = Integer.parseInt(mois);

        String DATE_DAY_ECHANCE = GeneralConst.EMPTY_STRING, DATE_MONTH_ECHEANCE = GeneralConst.EMPTY_STRING;
        String DATE_DAY_ECHANCE_PAIEMENT = GeneralConst.EMPTY_STRING, DATE_MONTH_ECHEANCE_PAIEMENT = GeneralConst.EMPTY_STRING;

        if (!echeanceLegale.equals(GeneralConst.Number.ZERO) && !echeanceLegale.equals(GeneralConst.EMPTY_STRING)) {
            String[] dateArray = echeanceLegale.split(GeneralConst.DASH_SEPARATOR);
            DATE_DAY_ECHANCE = dateArray[2];
            DATE_MONTH_ECHEANCE = dateArray[1];
        }

        if (!nombreJourLimitePaiement.equals(GeneralConst.Number.ZERO) && !nombreJourLimitePaiement.equals(GeneralConst.EMPTY_STRING)) {
            String[] dateArray = nombreJourLimitePaiement.split(GeneralConst.DASH_SEPARATOR);
            DATE_DAY_ECHANCE_PAIEMENT = dateArray[2];
            DATE_MONTH_ECHEANCE_PAIEMENT = dateArray[1];
        }

        switch (codePeriodicite) {
            case PONC:
            case JOUR:

                int length = 1;
                Date dateJour = new Date();
                if (forTaxation) {
                    length = 10;
                    dateJour = ConvertDate.formatDate(jour + "/" + mois + "/" + anneeNew);
                    dateJour = ConvertDate.addDayOfDate(dateJour, 1);
                }

                jsonPeriodes = PeriodiciteData.getPeriodeJournaliere(dateJour, length, codePeriodicite, nombreJourLimitePaiement);
                break;

            case MENS:
            case "BIMEN":
            case TRIME:
            case SEMS:

                if (forTaxation) {
                    moisDepart += 1;
                    if (moisDepart > 12) {
                        moisDepart = 1;
                        anneeDepart += 1;
                    }
                }

                jsonPeriodes = PeriodiciteData.getPeriodesMensuellesV2(
                        nombreJour,
                        forTaxation,
                        dateDepart,
                        moisDepart,
                        anneeDepart,
                        DATE_DAY_ECHANCE,
                        DATE_DAY_ECHANCE_PAIEMENT,
                        codePeriodicite,
                        periodeEcheance,
                        Integer.valueOf(anneeOld));
                break;

            case ANNEE:
            case "2ANS":
            case "5ANS":

                anneeDepart = (forTaxation) ? (anneeDepart + 1) : anneeDepart;

                jsonPeriodes = PeriodiciteData.getPeriodesAnnuellesV2(
                        nombreJour,
                        forTaxation,
                        dateDepart,
                        anneeDepart,
                        DATE_DAY_ECHANCE,
                        DATE_MONTH_ECHEANCE,
                        DATE_DAY_ECHANCE_PAIEMENT,
                        DATE_MONTH_ECHEANCE_PAIEMENT,
                        codePeriodicite,
                        periodeEcheance,
                        dateDepartOld);
        }

        return jsonPeriodes;
    }

    public String createAssujettissement(HttpServletRequest request) {

        String result;

        try {

            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);
            String codeArticleBudgetaire = request.getParameter(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE);
            String dateDebut = request.getParameter(AssujettissementConst.ParamName.DATE_DEBUT);
            String dateFin = request.getParameter(AssujettissementConst.ParamName.DATE_FIN);
            String valeurBase = request.getParameter(AssujettissementConst.ParamName.VALEUR_BASE);
            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String nombreJourLimite = request.getParameter(AssujettissementConst.ParamName.NOMBRE_JOUR_LIMITE);
            String codePeriodicite = request.getParameter(AssujettissementConst.ParamName.CODE_PERIODICITE);
            String codeTarif = request.getParameter(AssujettissementConst.ParamName.CODE_TARIF);
            String codeGestionnaire = request.getParameter(AssujettissementConst.ParamName.CODE_GESTIONNAIRE);
            String codeAP = request.getParameter(AssujettissementConst.ParamName.CODE_ADRESSE_PERSONNE);
            String idComplementBien = request.getParameter(AssujettissementConst.ParamName.ID_COMPLEMENT_BIEN);
            //String idUser = request.getParameter(AssujettissementConst.ParamName.ID_USER);
            //String nvlEcheance = request.getParameter(AssujettissementConst.ParamName.NVL_ECHEANCE);
            String uniteValeur = request.getParameter("uniteValeur");

            JSONArray jsonPeriodesDeclarations = new JSONArray(
                    request.getParameter(AssujettissementConst.ParamName.PERIODES_DECLARATIONS)
            );

            JSONArray selectedABArray = new JSONArray();

            try {

                selectedABArray = new JSONArray(
                        request.getParameter(AssujettissementConst.ParamName.SELECTED_AB)
                );
            } catch (Exception e) {
            }

            Assujeti assujettissement = AssujettissementBusiness.getExistAssujettissement(
                    idBien,
                    codeArticleBudgetaire,
                    idComplementBien,
                    codePersonne, codeTarif);

            Date dateFinAssuj, dateDebutNewAssuj;

            if (assujettissement != null) {

                dateDebutNewAssuj = ConvertDate.formatDate(dateDebut);
                dateFinAssuj = ConvertDate.formatDate(ConvertDate.getValidFormatDatePrint(assujettissement.getDateFin()));

                if (dateDebutNewAssuj.before(dateFinAssuj)) {
                    return AssujettissementConst.ResultCode.EXIST_ASSUJETTISSEMENT;
                }
            }

            if (codeArticleBudgetaire.equals(propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"))) {

                Bien bien = AssujettissementBusiness.getBienByCode(idBien);

                String codeTypeComplementBien = TaxationBusiness.getCodeTypeComplementBienByTypeBien(bien.getTypeBien().getCode());

                for (ComplementBien cb : bien.getComplementBienList()) {

                    if (cb.getTypeComplement().getCode().equals(codeTypeComplementBien)) {
                        valeurBase = cb.getValeur();
                    }
                }
            }

            Assujeti assujeti = new Assujeti();
            assujeti.setBien(new Bien(idBien));
            assujeti.setArticleBudgetaire(new ArticleBudgetaire(codeArticleBudgetaire));
            assujeti.setRenouvellement(Short.valueOf(GeneralConst.Number.ZERO));
            assujeti.setDateDebut(dateDebut);
            assujeti.setDateFin(dateFin);

            Double base = Double.valueOf(valeurBase.equals(GeneralConst.EMPTY_STRING) ? GeneralConst.Number.ZERO : valeurBase);
            assujeti.setValeur(BigDecimal.valueOf(base));
            assujeti.setPersonne(new Personne(codePersonne));
            assujeti.setNbrJourLimite(Integer.valueOf(nombreJourLimite));
            assujeti.setPeriodicite(codePeriodicite);
            assujeti.setTarif(new Tarif(codeTarif));
            assujeti.setGestionnaire(codeGestionnaire);
            assujeti.setFkAdressePersonne(new AdressePersonne(codeAP));
            assujeti.setComplementBien(idComplementBien);
            assujeti.setUniteValeur(uniteValeur);

            List<PeriodeDeclaration> periodeDeclarations = getPeriodesDeclarations(jsonPeriodesDeclarations);

            //LogUser logUser = new LogUser(request, AssujettissementConst.Operation.SAVE_ASSUJETTISSEMENT);
            LogUser logUser = null;

            if (AssujettissementBusiness.saveAssujettissement(assujeti, periodeDeclarations, selectedABArray, logUser)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public List<PeriodeDeclaration> getPeriodesDeclarations(JSONArray jsonPeriodesDeclarations) {

        List<PeriodeDeclaration> periodeDeclarations = new ArrayList<>();
        try {
            for (int i = 0; i < jsonPeriodesDeclarations.length(); i++) {

                JSONObject jsonobject = jsonPeriodesDeclarations.getJSONObject(i);
                String dateDebut = jsonobject.getString(AssujettissementConst.ParamName.DATE_DEBUT);
                String dateFin = jsonobject.getString(AssujettissementConst.ParamName.DATE_FIN);
                String dateLimite = jsonobject.getString(AssujettissementConst.ParamName.DATE_LIMITE);
                String dateLimitePaiement = jsonobject.getString(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT);

                PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();
                periodeDeclaration.setDebut(ConvertDate.formatDate(dateDebut));
                periodeDeclaration.setFin(ConvertDate.formatDate(dateFin));
                periodeDeclaration.setDateLimite(ConvertDate.formatDate(dateLimite));
                periodeDeclaration.setDateLimitePaiement(ConvertDate.formatDate(dateLimitePaiement));
                periodeDeclarations.add(periodeDeclaration);

            }
        } catch (JSONException e) {
            CustumException.LogException(e);
        }
        return periodeDeclarations;
    }

    public JSONObject getValeurComplementBiens(List<ComplementBien> complementBiens) {

        JSONObject data = new JSONObject();

        try {

            Bien bien = complementBiens.get(0).getBien();
            data.put("idBien", bien.getId());
            data.put("intituleBien", bien.getIntitule());

            data.put("codeUsage", bien.getFkUsageBien() != null ? bien.getFkUsageBien() : GeneralConst.EMPTY_STRING);
            data.put("codeCommune", bien.getFkCommune() != null ? bien.getFkCommune() : GeneralConst.EMPTY_STRING);
            data.put("codeCategorie", bien.getFkTarif() != null ? bien.getFkTarif() : GeneralConst.EMPTY_STRING);
            data.put("codeQuartier", bien.getFkQuartier() != null ? bien.getFkQuartier() : GeneralConst.EMPTY_STRING);

            data.put("descriptionBien", bien.getDescription() != null ? bien.getDescription() : GeneralConst.EMPTY_STRING);
            data.put("dateAcquisition", bien.getAcquisitionList() == null ? GeneralConst.EMPTY_STRING : bien.getAcquisitionList().get(0).getDateAcquisition());
            data.put("codeTypeBien", bien.getTypeBien() == null ? GeneralConst.EMPTY_STRING : bien.getTypeBien().getCode());
            data.put("codeAdressePersonne", bien.getFkAdressePersonne() == null ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne().getCode());
            data.put("chaineAdressePersonne", bien.getFkAdressePersonne() == null ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne().getAdresse() == null ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne().getAdresse().toString());

            List<JSONObject> dataList = new ArrayList<>();

            for (ComplementBien cb : complementBiens) {

                JSONObject dataCB = new JSONObject();
                dataCB.put("id", cb.getId());
                dataCB.put("codeTypeComplementBien", cb.getTypeComplement() == null ? GeneralConst.EMPTY_STRING : cb.getTypeComplement().getCode());
                dataCB.put("valeur", cb.getValeur());
                dataCB.put("uniteDevise", cb.getDevise() == null ? GeneralConst.EMPTY_STRING : cb.getDevise());
                dataList.add(dataCB);

            }

            data.put("complementList", dataList);

        } catch (Exception e) {

        }
        return data;
    }

    public String getBienForEdition(HttpServletRequest request) {

        JSONObject jsonBien = new JSONObject();

        try {

            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);

            Bien bien = AssujettissementBusiness.getBienById(idBien);

            jsonBien.put(AssujettissementConst.ParamName.CODE_TYPE_BIEN, bien == null
                    ? GeneralConst.EMPTY_STRING : bien.getTypeBien() == null ? GeneralConst.EMPTY_STRING : bien.getTypeBien().getCode());

            jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN,
                    bien == null ? GeneralConst.EMPTY_STRING : bien.getIntitule());

            jsonBien.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN,
                    bien == null ? GeneralConst.EMPTY_STRING : bien.getDescription());

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonBien.toString();
    }

    public String loadArticlesBudgetairesAssujettissable(HttpServletRequest request) {

        List<JSONObject> jsonArticlesBudgetaires = new ArrayList<>();
        List<ArticleBudgetaire> articleBudgetaires;
        //List<ArticleMere> articleBudgetairesMereList = new ArrayList<>();

        boolean getMere = false;

        try {
            String libelle = request.getParameter(AssujettissementConst.ParamName.LIBELLE);
            String codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            String codeEntite = request.getParameter(TaxationConst.ParamName.CODE_ENTITE);
            String codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            String getMereValue = request.getParameter(TaxationConst.ParamName.GET_MERE);

            if (getMereValue != null) {

                getMere = getMereValue.equals(GeneralConst.Number.ONE);
            }

            //articleBudgetaires = AssujettissementBusiness.getArticlesBudgetairesAssujettissableBySite(codeSite, codeService, codeEntite);
            articleBudgetaires = AssujettissementBusiness.getArticlesBudgetairesAssujettissable(libelle);

            if (getMere) {

                //articleBudgetairesMereList = AssujettissementBusiness.getArticlesBudgetairesAssujettissableBySiteMere(codeSite, codeService, codeEntite);
            }

            if (articleBudgetaires.isEmpty()) {

                articleBudgetaires = AssujettissementBusiness.
                        getArticlesBudgetairesAssujettissable(libelle, codeService, codeEntite);

                if (getMere) {

                    //articleBudgetairesMereList = AssujettissementBusiness.getArticlesBudgetairesAssujettissableMere(libelle, codeService, codeEntite);
                }

            }

            if (!articleBudgetaires.isEmpty()) {

                for (ArticleBudgetaire articleBudgetaire : articleBudgetaires) {

                    boolean canGo = true;

                    if (getMere) {

//                        if (articleBudgetaire.getArticleReference() != null) {
//                            for (ArticleMere mere : articleBudgetairesMereList) {
//                                if (articleBudgetaire.getArticleReference().equals(mere.getId().toString())) {
//                                    canGo = false;
//                                    break;
//                                }
//                            }
//                        }
//                        if (!canGo) {
//                            continue;
//                        }
                    }

                    JSONObject jsonArticleBudgetaire = new JSONObject();

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                            articleBudgetaire.getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getCode());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_GENERIQUE,
                            articleBudgetaire.getArticleGenerique() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getArticleGenerique().getIntitule());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
                            articleBudgetaire.getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getIntitule());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
                            articleBudgetaire.getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getCodeOfficiel());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_PERIODICITE_AB,
                            articleBudgetaire.getPeriodicite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getPeriodicite().getCode());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.LIBELLE_PERIODICITE_AB,
                            articleBudgetaire.getPeriodicite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getPeriodicite().getIntitule());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_TARIF_AB,
                            articleBudgetaire.getTarif() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getTarif().getCode());

                    List<JSONObject> jsonPaliers = getJsonPaliersByAB(articleBudgetaire, "", "*");
                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PALIERS, jsonPaliers);

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODICITE_VARIABLE,
                            articleBudgetaire.getPeriodiciteVariable() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getPeriodiciteVariable());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.TARIF_VARIABLE,
                            articleBudgetaire.getTarifVariable() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getTarifVariable());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.UNITE,
                            articleBudgetaire.getUnite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getUnite() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : articleBudgetaire.getUnite().getIntitule());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.NOMBRE_JOUR_LIMITE,
                            articleBudgetaire.getNbrJourLimite() == null
                                    ? GeneralConst.Number.ZERO
                                    : articleBudgetaire.getNbrJourLimite());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.ECHEANCE_LEGALE,
                            articleBudgetaire.getEcheanceLegale() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getEcheanceLegale());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.NBRE_JOUR_LEGALE_PAIEMENT,
                            articleBudgetaire.getDateLimitePaiement() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getDateLimitePaiement());

                    if (articleBudgetaire == null) {

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE, GeneralConst.EMPTY_STRING);

                    } else {
                        if (articleBudgetaire.getPeriodeEcheance() != null) {
                            jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE,
                                    articleBudgetaire.getPeriodeEcheance() == true
                                            ? GeneralConst.Number.ONE
                                            : GeneralConst.Number.ZERO);
                        } else {
                            jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE, GeneralConst.EMPTY_STRING);
                        }
                    }

                    if (articleBudgetaire.getArticleGenerique().getServiceAssiette() != null) {

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.SECTEUR_ACTIVITE_CODE,
                                articleBudgetaire.getArticleGenerique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getServiceAssiette().getCode());

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.SECTEUR_ACTIVITE,
                                articleBudgetaire.getArticleGenerique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getServiceAssiette().getIntitule());

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.MINISTERE_LIBELLE,
                                articleBudgetaire.getArticleGenerique().getServiceAssiette().getService() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getServiceAssiette().getService().getIntitule());

                    } else {

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.SECTEUR_ACTIVITE_CODE,
                                GeneralConst.EMPTY_STRING);

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.SECTEUR_ACTIVITE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.IS_PALIER,
                            articleBudgetaire.getPalier() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.TYPE_AB,
                            GeneralConst.Number.ZERO);

                    //jsonArticleBudgetaire.put(AssujettissementConst.ParamName.ARTICLE_REFERENCE, articleBudgetaire.getArticleReference() == null ? GeneralConst.EMPTY_STRING : articleBudgetaire.getArticleReference());
                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.ARTICLE_REFERENCE, GeneralConst.EMPTY_STRING);

                    jsonArticlesBudgetaires.add(jsonArticleBudgetaire);

                }

//                for (ArticleMere articleMere : articleBudgetairesMereList) {
//
//                    JSONObject jsonArticleBudgetaire = new JSONObject();
//
//                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
//                            articleMere.getId());
//
//                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
//                            articleMere.getIntitule() == null
//                                    ? GeneralConst.EMPTY_STRING
//                                    : articleMere.getIntitule());
//
//                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
//                            articleMere.getCodeOfficiel() == null
//                                    ? GeneralConst.EMPTY_STRING
//                                    : articleMere.getCodeOfficiel());
//
//                    ArticleBudgetaire ab = TaxationBusiness.getArticleBudgetaireByMere(articleMere.getId().toString());
//
//                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_PERIODICITE_AB,
//                            ab.getPeriodicite() == null
//                                    ? GeneralConst.EMPTY_STRING
//                                    : ab.getPeriodicite().getCode());
//
//                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.TYPE_AB,
//                            GeneralConst.Number.ONE);
//
//                    jsonArticlesBudgetaires.add(jsonArticleBudgetaire);
//                }
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonArticlesBudgetaires.toString();
    }

    public String loadUsageBien(HttpServletRequest request) {

        List<JSONObject> jsonUsageBienList = new ArrayList<>();
        String result = null;

        try {

            List<UsageBien> listUsageBien = AssujettissementBusiness.getListUsageBiens();

            if (listUsageBien.size() > 0) {

                for (UsageBien usageBien : listUsageBien) {

                    JSONObject jsonUsageBien = new JSONObject();

                    jsonUsageBien.put("id", usageBien.getId());
                    jsonUsageBien.put("intitule", usageBien.getIntitule());

                    jsonUsageBienList.add(jsonUsageBien);
                }
                result = jsonUsageBienList.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String loadTarifByQuartier(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeQuartier = request.getParameter("codeQuartier");
            String codeNature = request.getParameter("codeNature");

            List<Tarif> listTarifs = new ArrayList<>();

            switch (codeNature) {

                case "TB00000028": //TERRAIN
                case "TB00000026": //AP
                case "TB00000027": //AT
                case "TB00000025": //VILLA
                case "TB00000029": //ETAGE

                    listTarifs = AssujettissementBusiness.getListCategorieByNatureImpot(propertiesConfig.getProperty("CODE_IMPOT_FONCIER"),
                            codeNature);

                    break;

            }


            /*if (listTarifs.size() > 0) {
                
             }*/
            if (listTarifs.size() > 0) {

                List<JSONObject> jsonCategorieList = new ArrayList<>();

                for (Tarif tarif : listTarifs) {

                    JSONObject jsonTarif = new JSONObject();

                    jsonTarif.put("code", tarif.getCode());
                    jsonTarif.put("intitule", tarif.getIntitule().toUpperCase());

                    jsonCategorieList.add(jsonTarif);
                }

                dataReturn = jsonCategorieList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadingAccountBankDataV2(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeAb = request.getParameter("codeAb");
            String codeBanque = request.getParameter("codeBanque");
            String codeDevise = request.getParameter("codeDevise");

            List<CompteBancaire> listCompteBancaires = AssujettissementBusiness.getListCompteBancaireByABandBank(
                    codeAb.trim(), codeBanque.trim(), codeDevise);

            if (!listCompteBancaires.isEmpty()) {

                List<JSONObject> jsonCompteBankList = new ArrayList<>();

                for (CompteBancaire compteBancaire : listCompteBancaires) {

                    JSONObject jsonCompteBank = new JSONObject();

                    jsonCompteBank.put("accountCode", compteBancaire.getCode());
                    jsonCompteBank.put("accountName", compteBancaire.getIntitule().toUpperCase());
                    jsonCompteBank.put("accountDevise", compteBancaire.getDevise().getCode());

                    jsonCompteBankList.add(jsonCompteBank);
                }

                dataReturn = jsonCompteBankList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadingAccountBankDataV3(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeBanque = request.getParameter("codeBanque");

            List<CompteBancaire> listCompteBancaires = TaxationBusiness.getListAllCompteBancaireByBanque(codeBanque);

            if (!listCompteBancaires.isEmpty()) {

                List<JSONObject> jsonCompteBankList = new ArrayList<>();

                for (CompteBancaire compteBancaire : listCompteBancaires) {

                    JSONObject jsonCompteBank = new JSONObject();

                    jsonCompteBank.put("accountCode", compteBancaire.getCode());
                    jsonCompteBank.put("accountName", compteBancaire.getIntitule().toUpperCase());

                    jsonCompteBankList.add(jsonCompteBank);
                }

                dataReturn = jsonCompteBankList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getInfoAccountBankAB(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<BanqueAb> listBanqueAbs = TaxationBusiness.getListAllBanqueAb();

            if (!listBanqueAbs.isEmpty()) {

                List<JSONObject> jsonBankABList = new ArrayList<>();

                for (BanqueAb banqueAb : listBanqueAbs) {

                    JSONObject jsonCompteBank = new JSONObject();

                    if (banqueAb.getFkBanque() != null) {

                        cd.hologram.erecettesvg.models.Banque banque = GeneralBusiness.getBanqueByCode(banqueAb.getFkBanque());

                        jsonCompteBank.put("id", banqueAb.getId());
                        jsonCompteBank.put("libelleBanque", banque == null ? "" : banque.getIntitule().toUpperCase());
                        jsonCompteBank.put("libelleAccountBank", banqueAb.getFkCompte() == null ? "" : banqueAb.getFkCompte().getIntitule().toUpperCase());
                        jsonCompteBank.put("libelleAB", banqueAb.getFkAb().getIntitule().toUpperCase());

                        jsonBankABList.add(jsonCompteBank);
                    }

                }

                dataReturn = jsonBankABList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadTarifByImpotAndNature(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeAb = request.getParameter("codeAb");
            String codeNature = request.getParameter("codeNature");

            List<Tarif> listTarifs = new ArrayList<>();

            switch (codeNature) {

                case "TB00000028": //TERRAIN
                case "TB00000026": //AP
                case "TB00000027": //AT
                case "TB00000025": //VILLA
                case "TB00000029": //ETAGE

                    listTarifs = AssujettissementBusiness.getListCategorieByNatureImpot(codeAb, codeNature);

                    break;

            }

            if (listTarifs.size() > 0) {

                List<JSONObject> jsonCategorieList = new ArrayList<>();

                for (Tarif tarif : listTarifs) {

                    JSONObject jsonTarif = new JSONObject();

                    jsonTarif.put("code", tarif.getCode());
                    jsonTarif.put("intitule", tarif.getIntitule().toUpperCase());

                    jsonCategorieList.add(jsonTarif);
                }

                dataReturn = jsonCategorieList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String getAgentByBureau(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String bureauCode = request.getParameter("bureauCode");

            List<Agent> listAgents = AssujettissementBusiness.getListAgentByBureau(bureauCode);

            if (listAgents.size() > 0) {

                List<JSONObject> jsonAgentList = new ArrayList<>();

                for (Agent agent : listAgents) {

                    JSONObject jsonAgent = new JSONObject();

                    jsonAgent.put("code", agent.getCode());
                    jsonAgent.put("nom", agent.toString().toUpperCase());

                    jsonAgentList.add(jsonAgent);
                }

                dataReturn = jsonAgentList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String redresement(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String id = request.getParameter("id");
            String observation = request.getParameter("observation");
            String uniteCode = request.getParameter("uniteCode");
            String userId = request.getParameter("userId");
            String newBase = request.getParameter("newBase");

            BigDecimal base = new BigDecimal("0");
            base = BigDecimal.valueOf(Double.valueOf(newBase));

            //float baseToInt = Float.valueOf(newBase);
            String baseToInt = newBase;

            Assujeti assujeti = AssujettissementBusiness.getAssujettissementDrhkatByCode(id);

            String codeTypeComplementBien = TaxationBusiness.getCodeTypeComplementBienByTypeBien(assujeti.getBien().getTypeBien().getCode());

            if (AssujettissementBusiness.saveRedressement(id, base, uniteCode, observation, assujeti.getBien().getId(), baseToInt, Integer.valueOf(userId), codeTypeComplementBien)) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String initData(HttpServletRequest request) {

        List<JSONObject> jsonUsageBienList = new ArrayList<>();
        List<JSONObject> jsonTypeBiens = new ArrayList<>();
        List<JSONObject> jsonEaList = new ArrayList<>();
        List<JSONObject> jsonCategorieList = new ArrayList<>();
        List<JSONObject> jsonQuartierList = new ArrayList<>();

        JSONObject initDataJson = new JSONObject();
        String datatReturn;

        try {
            List<EntiteAdministrative> listCommune = null;

            String codeDivision = request.getParameter("codeDivision");

            //codeDivision = "DIV00000009";
            //codeDivision = "DIV00000008";
            switch (codeDivision) {

                //LUBUMBASI
                case "DIV00000001":
                case "DIV00000002":
                case "DIV00000003":
                case "DIV00000004":
                case "DIV00000005":
                case "DIV00000006":
                case "DIV00000007":
                case "DIV00000008":
                case "DIV00000012":
                case "DIV00000013":

                    listCommune = AssujettissementBusiness.getListCommuneHautKatanga(
                            propertiesConfig.getProperty("CODE_DISTRICT_HAUT_KATANGA"));

                    break;
                case "DIV00000009": // LIKASI

                    listCommune = AssujettissementBusiness.getListCommuneHautKatanga(
                            propertiesConfig.getProperty("CODE_DISTRICT_HAUT_KATANGA_LIKASI"));
                    break;
            }

            if (listCommune.size() > 0) {

                for (EntiteAdministrative ea : listCommune) {

                    JSONObject jsonEa = new JSONObject();

                    String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");
                    jsonEa.put("code", ea.getCode());
                    jsonEa.put("communeComposite", ea.getIntitule().toUpperCase().concat(ville));
                    jsonEa.put("commune", ea.getIntitule().toUpperCase());
                    jsonEa.put("districtName", ea.getEntiteMere().getIntitule().toUpperCase());
                    jsonEa.put("districtCode", ea.getEntiteMere().getCode());

                    List<EntiteAdministrative> listQuartiers = AssujettissementBusiness.getListQuartierByCommune(ea.getCode());

                    for (EntiteAdministrative eaQuaertier : listQuartiers) {

                        JSONObject jsonEaQuartier = new JSONObject();

                        jsonEaQuartier.put("communeCode", ea.getCode());
                        jsonEaQuartier.put("quartierCode", eaQuaertier.getCode());
                        jsonEaQuartier.put("quartierName", eaQuaertier.getIntitule().toUpperCase());

                        jsonQuartierList.add(jsonEaQuartier);

                    }

                    jsonEa.put("quartierList", jsonQuartierList);

                    jsonEaList.add(jsonEa);
                }

            }

            List<UsageBien> listUsageBien = AssujettissementBusiness.getListUsageBiens();

            if (listUsageBien.size() > 0) {

                for (UsageBien usageBien : listUsageBien) {

                    JSONObject jsonUsageBien = new JSONObject();

                    jsonUsageBien.put("id", usageBien.getId());
                    jsonUsageBien.put("intitule", usageBien.getIntitule());

                    jsonUsageBienList.add(jsonUsageBien);
                }

            }

            String codeService = request.getParameter(AssujettissementConst.ParamName.CODE_SERVICE);
            String type = request.getParameter("type");

            type_ = Integer.valueOf(type);

            List<TypeBien> typeBiens = AssujettissementBusiness.getTypeBienByType(Integer.valueOf(type));

            if (typeBiens.size() > 0) {

                for (TypeBien typeBien : typeBiens) {

                    JSONObject jsonTypeBien = new JSONObject();

                    jsonTypeBien.put(AssujettissementConst.ParamName.CODE_TYPE_BIEN,
                            typeBien == null
                                    ? GeneralConst.EMPTY_STRING
                                    : typeBien.getCode());

                    jsonTypeBien.put(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                            typeBien == null
                                    ? GeneralConst.EMPTY_STRING
                                    : typeBien.getIntitule());

                    jsonTypeBien.put(AssujettissementConst.ParamName.EST_CONTRACTUEL,
                            typeBien == null
                                    ? GeneralConst.EMPTY_STRING
                                    : (typeBien.getContractuel())
                                            ? GeneralConst.Number.ONE
                                            : GeneralConst.Number.ZERO);

                    jsonTypeBiens.add(jsonTypeBien);
                }

            }

            if (jsonUsageBienList.size() > 0) {
                initDataJson.put("usageBienList", jsonUsageBienList);
            } else {
                initDataJson.put("usageBienList", GeneralConst.EMPTY_STRING);
            }

            if (jsonTypeBiens.size() > 0) {
                initDataJson.put("typeBienList", jsonTypeBiens);
            } else {
                initDataJson.put("typeBienList", GeneralConst.EMPTY_STRING);
            }

            if (jsonEaList.size() > 0) {
                initDataJson.put("eaList", jsonEaList);
            } else {
                initDataJson.put("eaList", GeneralConst.EMPTY_STRING);
            }

            if (jsonCategorieList.size() > 0) {
                initDataJson.put("categorieList", jsonCategorieList);
            } else {
                initDataJson.put("categorieList", GeneralConst.EMPTY_STRING);
            }

            datatReturn = initDataJson.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            datatReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return datatReturn;
    }

    public String loadTypeBienByService(HttpServletRequest request) {

        List<JSONObject> jsonTypeBiens = new ArrayList<>();
        List<JSONObject> jsonCategorieList = new ArrayList<>();

        List<JSONObject> jsonMarqueList = new ArrayList<>();
        List<JSONObject> jsonModeleList = new ArrayList<>();

        List<JSONObject> jsonPlageCategorieList = new ArrayList<>();
        List<JSONObject> jsonPlageCategorieListVU = new ArrayList<>();

        JSONObject initDataJson = new JSONObject();

        String result = null;

        String codeService = request.getParameter(AssujettissementConst.ParamName.CODE_SERVICE);

        String type = request.getParameter("type");

        if (type == null) {
            type = type_ + "";
        } else {
            type_ = Integer.valueOf(type);
        }

        try {

            List<TypeBien> typeBiens = AssujettissementBusiness.getTypeBienByType(Integer.valueOf(type));

            if (typeBiens.size() > 0) {

                for (TypeBien typeBien : typeBiens) {

                    JSONObject jsonTypeBien = new JSONObject();

                    jsonTypeBien.put(AssujettissementConst.ParamName.CODE_TYPE_BIEN,
                            typeBien == null
                                    ? GeneralConst.EMPTY_STRING
                                    : typeBien.getCode());

                    jsonTypeBien.put(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                            typeBien == null
                                    ? GeneralConst.EMPTY_STRING
                                    : typeBien.getIntitule());

                    jsonTypeBien.put(AssujettissementConst.ParamName.EST_CONTRACTUEL,
                            typeBien == null
                                    ? GeneralConst.EMPTY_STRING
                                    : (typeBien.getContractuel())
                                            ? GeneralConst.Number.ONE
                                            : GeneralConst.Number.ZERO);

                    jsonTypeBiens.add(jsonTypeBien);
                }

                List<Tarif> tarifList = new ArrayList<>();

                switch (type_) {
                    case 1:
                        tarifList = AssujettissementBusiness.getListTarifVignetteOrConcessionMine(propertiesConfig.getProperty("CODE_TARIF_VIGNETTE"));
                        break;
                    case 2:
                        tarifList = AssujettissementBusiness.getListTarifVignetteOrConcessionMine(propertiesConfig.getProperty("CODE_TARIF_IMMOBILIER"));
                        break;
                    case 3:

                        if (propertiesConfig.getProperty("LOAD_TARIF_ICM_ON_LIGNE").equals("1")) {
                            tarifList = AssujettissementBusiness.getListTarifVignetteOrConcessionMine(propertiesConfig.getProperty("CODE_TARIF_ICM_ON_LIGNE"));
                        } else {
                            tarifList = AssujettissementBusiness.getListTarifVignetteOrConcessionMine(propertiesConfig.getProperty("CODE_TARIF_ICM"));
                        }

                        break;
                    case 4:
                        tarifList = AssujettissementBusiness.getListTarifPublicite(propertiesConfig.getProperty("CODE_ARTICLE_BUDGETAIRE_PUBLICITE"));
                        break;
                }

                if (tarifList.size() > 0) {

                    for (Tarif tarif : tarifList) {

                        JSONObject jsonTarifBien = new JSONObject();

                        jsonTarifBien.put("tarifCode", tarif.getCode());
                        jsonTarifBien.put("tarifName", tarif.getIntitule().toUpperCase());

                        jsonCategorieList.add(jsonTarifBien);

                    }
                }

                List<MarqueVehicule> listMarqueVehicule = AssujettissementBusiness.getListAllMarqueVehicule();

                if (!listMarqueVehicule.isEmpty()) {

                    for (MarqueVehicule marqueVehicule : listMarqueVehicule) {

                        JSONObject jsonMarqueVehicule = new JSONObject();

                        jsonMarqueVehicule.put("marqueCode", marqueVehicule.getId());
                        jsonMarqueVehicule.put("marqueName", marqueVehicule.getIntitule().toUpperCase());

                        jsonMarqueList.add(jsonMarqueVehicule);

                    }
                }

                if (jsonMarqueList.size() > 0) {

                    initDataJson.put("marqueVehiculeList", jsonMarqueList);
                } else {
                    initDataJson.put("marqueVehiculeList", GeneralConst.EMPTY_STRING);
                }

                List<ModeleVehicule> listModeleVehicule = AssujettissementBusiness.getListAllModeleVehicule();

                if (!listModeleVehicule.isEmpty()) {

                    for (ModeleVehicule modeleVehicule : listModeleVehicule) {

                        JSONObject jsonModeleVehicule = new JSONObject();

                        jsonModeleVehicule.put("modeleCode", modeleVehicule.getId());
                        jsonModeleVehicule.put("modeleName", modeleVehicule.getIntitule().toUpperCase());

                        jsonModeleList.add(jsonModeleVehicule);

                    }
                }

                List<PlageCategorieBienAutomobile> listPlageCategorieBienAutomobile = AssujettissementBusiness.getListAllPlageCategorieBienAutomobile();

                for (PlageCategorieBienAutomobile plageCategorieBienAutomobile : listPlageCategorieBienAutomobile) {

                    JSONObject jsonPlageCategorieBienAutomobile = new JSONObject();

                    jsonPlageCategorieBienAutomobile.put("codeTarif", plageCategorieBienAutomobile.getFkTarif());
                    jsonPlageCategorieBienAutomobile.put("codeUnite", plageCategorieBienAutomobile.getFkUnite());
                    jsonPlageCategorieBienAutomobile.put("plageMin", plageCategorieBienAutomobile.getPlageMin());
                    jsonPlageCategorieBienAutomobile.put("plageMax", plageCategorieBienAutomobile.getPlageMax());
                    jsonPlageCategorieBienAutomobile.put("codeGenre", plageCategorieBienAutomobile.getFkTypeBien() == null
                            ? "" : plageCategorieBienAutomobile.getFkTypeBien());

                    jsonPlageCategorieList.add(jsonPlageCategorieBienAutomobile);

                    switch (plageCategorieBienAutomobile.getFkTarif()) {

                        case "0000074320":
                        case "0000074220":
                        case "0000074420":

                            jsonPlageCategorieListVU.add(jsonPlageCategorieBienAutomobile);
                            break;
                    }

                }

                if (jsonPlageCategorieList.size() > 0) {
                    initDataJson.put("plageCategorieList", jsonPlageCategorieList);

                } else {
                    initDataJson.put("plageCategorieList", "");
                }

                if (jsonPlageCategorieListVU.size() > 0) {

                    initDataJson.put("plageCategorieListVU", jsonPlageCategorieListVU);

                } else {
                    initDataJson.put("plageCategorieListVU", "");
                }

                if (jsonModeleList.size() > 0) {

                    initDataJson.put("modeleVehiculeList", jsonModeleList);
                } else {
                    initDataJson.put("modeleVehiculeList", GeneralConst.EMPTY_STRING);
                }

                if (jsonTypeBiens.size() > 0) {

                    initDataJson.put("typeBienList", jsonTypeBiens);
                } else {
                    initDataJson.put("typeBienList", GeneralConst.EMPTY_STRING);
                }

                if (jsonCategorieList.size() > 0) {

                    initDataJson.put("categorieBienList", jsonCategorieList);
                } else {
                    initDataJson.put("categorieBienList", GeneralConst.EMPTY_STRING);
                }

                result = initDataJson.toString();

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String saveTypeBien(HttpServletRequest request) {

        String libelleTypeBien = request.getParameter(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN);
        String estContractuel = request.getParameter(AssujettissementConst.ParamName.EST_CONTRACTUEL);
        String codeService = request.getParameter(AssujettissementConst.ParamName.CODE_SERVICE);
        String codeTypeBien = request.getParameter("code");

        estContractuel = "0";

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            TypeBien typeBien = new TypeBien();

            typeBien.setCode(codeTypeBien);
            typeBien.setIntitule(libelleTypeBien);
            typeBien.setContractuel(Boolean.valueOf(estContractuel));

            boolean result = AssujettissementBusiness.saveTypeBien(typeBien, codeService);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String updateTypeBien(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String libelleTypeBien = request.getParameter(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN);
            String estContractuel = request.getParameter(AssujettissementConst.ParamName.EST_CONTRACTUEL);
            String codeTypeBien = request.getParameter(AssujettissementConst.ParamName.CODE_TYPE_BIEN);

            TypeBien typeBien = new TypeBien();

            typeBien.setIntitule(libelleTypeBien);
            typeBien.setContractuel(Boolean.valueOf(estContractuel));
            typeBien.setCode(codeTypeBien);

            if (AssujettissementBusiness.updateTypeBien(typeBien)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String disableTypeBien(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String codeTypeBien = request.getParameter(AssujettissementConst.ParamName.CODE_TYPE_BIEN);
            String libelleTypeBien = request.getParameter(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN);

            TypeBien typeBien = new TypeBien();

            typeBien.setIntitule(libelleTypeBien);
            typeBien.setCode(codeTypeBien);

            if (AssujettissementBusiness.disableTypeBien(typeBien)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String saveUsageBien(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String code = request.getParameter("code");
            String libelleUsageBien = request.getParameter("libelleUsageBien");

            UsageBien usageBien = new UsageBien();

            usageBien.setIntitule(libelleUsageBien);
            usageBien.setId(Integer.valueOf(code));

            if (AssujettissementBusiness.sageUsageBien(usageBien)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String loadPeriodeDeclarationByAssyj(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String code = request.getParameter("code");
            BigDecimal tauxHA = new BigDecimal(propertiesConfig.getProperty("TAUX_ICM"));
            String isCurrentYear = GeneralConst.EMPTY_STRING;
            String currentYear = GeneralConst.EMPTY_STRING;

            String partYearDateToday = ConvertDate.getYearPartDateFromDateParam(ConvertDate.formatDateToString(new Date()));

            List<PeriodeDeclaration> listPeriodeDeclarations = AssujettissementBusiness.getListPeriodeDeclarationByAssuj(code);

            long moisRetard = 0;

            int annee = 0;

            List<JSONObject> jsonPeriodes = new ArrayList<>();

            int count = 0;

            if (listPeriodeDeclarations.size() > 0) {

                for (PeriodeDeclaration periode : listPeriodeDeclarations) {
                    count++;
                    annee++;

                    IcmParam icmParam = AssujettissementBusiness.getIcmParamByTarifAndAnnee(periode.getAssujetissement().getBien().getFkTarif(), annee);

                    JSONObject jsonPeriode = new JSONObject();

                    jsonPeriode.put(AssujettissementConst.ParamName.PERIODE_ID, periode.getId());
                    jsonPeriode.put(AssujettissementConst.ParamName.PERIODE, Tools.getPeriodeIntitule(periode.getDebut(),
                            periode.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode()));

                    if (icmParam != null) {

                        jsonPeriode.put("tauxPeriode", icmParam.getTaux());
                        jsonPeriode.put("devise", icmParam.getDevise());
                        jsonPeriode.put("icmParamId", icmParam.getId());

                        BigDecimal valeurCuConvertiEnHA = new BigDecimal("0");

                        valeurCuConvertiEnHA = valeurCuConvertiEnHA.add(periode.getAssujetissement().getValeur().multiply(tauxHA));

                        BigDecimal valeurICM = new BigDecimal("0");

                        valeurICM = valeurICM.add(valeurCuConvertiEnHA.multiply(icmParam.getTaux()));

                        jsonPeriode.put("tauxCumule", valeurICM);

                    } else {

                        jsonPeriode.put("tauxPeriode", 0);
                        jsonPeriode.put("devise", GeneralConst.EMPTY_STRING);
                        jsonPeriode.put("icmParamId", GeneralConst.EMPTY_STRING);
                    }

                    if (periode.getDateLimite() != null) {

                        if (Compare.before(periode.getDateLimite(), new Date())) {

                            //moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates(ConvertDate.formatDateToString(periode.getDateLimite()));
                            String partYearDateParm = ConvertDate.getYearPartDateFromDateParam(ConvertDate.formatDateToString(periode.getFin()));

                            if (partYearDateParm.equals(partYearDateToday)) {

                                moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates_V2(
                                        ConvertDate.formatDateToString(periode.getDateLimite()), ConvertDate.formatDateToString(new Date()));
                            } else {
                                moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates_V2(
                                        ConvertDate.formatDateToString(periode.getDateLimite()), ConvertDate.formatDateToString(periode.getFin()));
                            }

                            if (moisRetard == 11 && count > 1) {
                                moisRetard = moisRetard + 1;
                            }

                            if (periode.getAssujetissement().getArticleBudgetaire().getCode().equals(propertiesConfig.getProperty("CODE_IMPOT_FONCIER"))) {
                                currentYear = ConvertDate.getYearPartDateFromDateParam(ConvertDate.formatDateToString(periode.getDateLimite()));

                                if (currentYear.equals(partYearDateToday)) {
                                    isCurrentYear = "1";
                                } else {
                                    isCurrentYear = "0";
                                }

                                jsonPeriode.put("isCurrentYear", isCurrentYear);
                            }

                        } else {
                            moisRetard = 0;
                        }

                    } else {

                        if (Compare.before(periode.getFin(), new Date())) {

                            //moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates(ConvertDate.formatDateToString(periode.getFin()));
                            String partYearDateParm = ConvertDate.getYearPartDateFromDateParam(ConvertDate.formatDateToString(periode.getFin()));

                            if (partYearDateParm.equals(partYearDateToday)) {
                                moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates_V2(
                                        ConvertDate.formatDateToString(periode.getFin()), ConvertDate.formatDateToString(new Date()));
                            } else {
                                moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates_V2(
                                        ConvertDate.formatDateToString(periode.getFin()), ConvertDate.formatDateToString(periode.getFin()));
                            }

                            if (moisRetard == 11 && count > 1) {
                                moisRetard = moisRetard + 1;
                            }

                            if (periode.getAssujetissement().getArticleBudgetaire().getCode().equals(propertiesConfig.getProperty("CODE_IMPOT_FONCIER"))) {

                                currentYear = ConvertDate.getYearPartDateFromDateParam(ConvertDate.formatDateToString(periode.getFin()));

                                if (currentYear.equals(partYearDateToday)) {
                                    isCurrentYear = "1";
                                } else {
                                    isCurrentYear = "0";
                                }

                                jsonPeriode.put("isCurrentYear", isCurrentYear);
                            }

                        } else {
                            moisRetard = 0;
                        }
                    }

                    jsonPeriode.put(TaxationConst.ParamName.NBRE_MOIS, moisRetard);
                    jsonPeriode.put("estPenalise", moisRetard == 0 ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE);

                    int resultRecidiviste = PoursuiteBusiness.checkRecidivisteDeclaration(
                            periode.getAssujetissement().getId(), periode.getId());

                    jsonPeriode.put(TaxationConst.ParamName.EST_RECIDIVISTE, resultRecidiviste == 0 ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE);

                    jsonPeriodes.add(jsonPeriode);

                }

                dataReturn = jsonPeriodes.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String disableUsageBien(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String codeUsageBien = request.getParameter("codeUsageBien");

            if (AssujettissementBusiness.disableUsageBien(Integer.valueOf(codeUsageBien))) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String deleteData(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String id = request.getParameter("id");

            if (AssujettissementBusiness.disableAssociateAccountBankAB(Integer.valueOf(id))) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String saveAssociation(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String codeBanque = request.getParameter("codeBanque");
            String codeCompteBancaire = request.getParameter("codeCompteBancaire");
            String codeArticleBudgetaire = request.getParameter("codeArticleBudgetaire");
            String userId = request.getParameter("userId");

            BanqueAb banqueAb = AssujettissementBusiness.getBanqueAbByFilter(codeBanque, codeCompteBancaire, codeArticleBudgetaire);

            if (banqueAb != null) {
                result = "2";
            } else {
                if (AssujettissementBusiness.saveAssociation(codeBanque, codeCompteBancaire, codeArticleBudgetaire, Integer.valueOf(userId))) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String canceledPenalite(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String codeTaxation = request.getParameter("codeTaxation");

            if (AssujettissementBusiness.canceledPenalite(Integer.valueOf(codeTaxation))) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String createPermutation(HttpServletRequest request) {

        String result = null;
        Acquisition acquis;

        try {
            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);
            String dateAcquisition = request.getParameter(AssujettissementConst.ParamName.DATE_ACQUISITION);
            String referenceContrat = request.getParameter(AssujettissementConst.ParamName.REFERENCE_CONTRAT);
            String numActeNotarie = request.getParameter(AssujettissementConst.ParamName.NUM_ACTE_NOTARIE);
            String dateActeNotarie = request.getParameter(AssujettissementConst.ParamName.DATE_ACTE_NOTARIE);
            String codeProprieteBien = request.getParameter(AssujettissementConst.ParamName.CODE_PROPRIETAIRE_BIEN);

            acquis = AssujettissementBusiness.getAcquisistionByProprietaire(codeProprieteBien, idBien);

            if (acquis != null) {

                Acquisition acquisition = new Acquisition();
                acquisition.setPersonne(new Personne(codePersonne));
                acquisition.setBien(new Bien(idBien));
                acquisition.setDateAcquisition(dateAcquisition);
                acquisition.setProprietaire(true);
                acquisition.setReferenceContrat(referenceContrat);
                acquisition.setNumActeNotarie(numActeNotarie);
                acquisition.setDateActeNotarie(dateActeNotarie);

                if (AssujettissementBusiness.createPermutation(acquisition, acquis.getId())) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String loadDetailTaxation(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeTaxation = request.getParameter("codeTaxation");

            RetraitDeclaration rd = AssujettissementBusiness.getRetraitDeclarationByCode(codeTaxation);

            if (rd != null) {

                List<RetraitDeclaration> listRetraitDeclaration = DeclarationBusiness.getListRetraitDeclarationByNewID_V3(rd.getNewId());

                if (!listRetraitDeclaration.isEmpty()) {

                    List<JSONObject> jsonRetraitList = new ArrayList<>();

                    for (RetraitDeclaration retraitDeclaration : listRetraitDeclaration) {

                        JSONObject jsonRetrait = new JSONObject();
                        PeriodeDeclaration pd = new PeriodeDeclaration();
                        ArticleBudgetaire ab = new ArticleBudgetaire();

                        Personne personne = IdentificationBusiness.getPersonneByCode(retraitDeclaration.getFkAssujetti());

                        jsonRetrait.put("codePersonne", personne == null ? "" : personne.getCode());
                        jsonRetrait.put("namePersonne", personne == null ? "" : personne.toString().toUpperCase());
                        jsonRetrait.put("codFj", personne == null ? "" : personne.getFormeJuridique().getCode());

                        jsonRetrait.put("codeAB", retraitDeclaration.getFkAb());
                        jsonRetrait.put("codeBanque", retraitDeclaration.getFkBanque());
                        jsonRetrait.put("codeCompte", retraitDeclaration.getFkCompteBancaire());
                        jsonRetrait.put("newID", retraitDeclaration.getNewId());

                        jsonRetrait.put("code", retraitDeclaration.getId());
                        jsonRetrait.put("devise", retraitDeclaration.getDevise());
                        jsonRetrait.put("state", retraitDeclaration.getEtat());

                        pd = AssujettissementBusiness.getPeriodeDeclarationByCode(retraitDeclaration.getFkPeriode());
                        ab = DeclarationBusiness.getArticleBudgetaireByCode(retraitDeclaration.getFkAb());

                        TaxationBienAutomobile taxationBienAutomobile = DeclarationBusiness.getTaxationBienAutomobileByNoteAndBien(
                                Integer.valueOf(codeTaxation), pd.getAssujetissement().getBien().getId());

                        jsonRetrait.put("montant", taxationBienAutomobile == null ? retraitDeclaration.getMontant() : taxationBienAutomobile.getPrincipalDu());
                        jsonRetrait.put("penalite", taxationBienAutomobile == null ? 0 : taxationBienAutomobile.getPenaliteDu() == null ? 0 : taxationBienAutomobile.getPenaliteDu());

                        if (pd != null && ab != null) {
                            jsonRetrait.put("periodeName", Tools.getPeriodeIntitule(pd.getDebut(), ab.getPeriodicite().getCode()));
                        } else {
                            jsonRetrait.put("periodeName", "");
                        }

                        jsonRetrait.put("periodeCode", pd.getId());

                        if (pd != null) {

                            jsonRetrait.put("natureBien", pd.getAssujetissement().getBien().getTypeBien().getIntitule().toUpperCase());
                            jsonRetrait.put("valeurBase", pd.getAssujetissement().getValeur());
                            jsonRetrait.put("bienCode", pd.getAssujetissement().getBien().getId());
                            jsonRetrait.put("bienName", pd.getAssujetissement().getBien().getIntitule().toUpperCase());
                            jsonRetrait.put("codeTarifBien", pd.getAssujetissement().getBien().getFkTarif());
                            jsonRetrait.put("nameTarifBien", pd.getAssujetissement().getTarif().getIntitule().toUpperCase());
                            jsonRetrait.put("adresseBien", pd.getAssujetissement().getBien().getFkAdressePersonne().getAdresse().toString().toString());

                            ComplementBienAutomobile cba = TaxationBusiness.getComplementBienAutomobileByBien(
                                    pd.getAssujetissement().getBien().getId());

                            jsonRetrait.put("plaque", cba == null ? "" : cba.getNumeroPlaque().toUpperCase());

                        } else {

                            jsonRetrait.put("natureBien", GeneralConst.EMPTY_STRING);
                            jsonRetrait.put("valeurBase", GeneralConst.EMPTY_STRING);
                            jsonRetrait.put("bienName", GeneralConst.EMPTY_STRING);
                            jsonRetrait.put("adresseBien", GeneralConst.EMPTY_STRING);
                            jsonRetrait.put("plaque", GeneralConst.EMPTY_STRING);

                        }

                        NotePerceptionVignette npv = AssujettissementBusiness.getNotePerceptionVignetteByTaxationAndBien(
                                Integer.valueOf(codeTaxation), pd.getAssujetissement().getBien().getId());

                        if (codeTaxation.equals(retraitDeclaration.getCodeDeclaration())) {
                            jsonRetrait.put("vignetteExist", "2");
                        } else {
                            jsonRetrait.put("vignetteExist", npv == null ? "0" : "1");
                        }

                        jsonRetraitList.add(jsonRetrait);

                    }

                    dataReturn = jsonRetraitList.toString();

                } else {

                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;

                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveAddNewPriodeDeclaration(HttpServletRequest request) {

        String result;

        try {

            String codeAssuj = request.getParameter("codeAssuj");
            String periodeList = request.getParameter("periodeList");
            String userId = request.getParameter("userId");

            String dateDebut = request.getParameter(AssujettissementConst.ParamName.DATE_DEBUT);
            String dateFin = request.getParameter(AssujettissementConst.ParamName.DATE_FIN);

            JSONArray jsonNewPDList = null;

            if (periodeList != null) {
                jsonNewPDList = new JSONArray(periodeList);
            }

            if (jsonNewPDList != null) {

                List<PeriodeDeclaration> pdList = new ArrayList<>();

                for (int i = 0; i < jsonNewPDList.length(); i++) {

                    PeriodeDeclaration pd = new PeriodeDeclaration();

                    JSONObject jsonobject = jsonNewPDList.getJSONObject(i);

                    Date dateDebutAssuj, dateFinAssuj, dateLimiteAssuj;

                    dateDebutAssuj = ConvertDate.formatDate(jsonobject.getString("dateDebut"));
                    dateFinAssuj = ConvertDate.formatDate(jsonobject.getString("dateFin"));
                    dateLimiteAssuj = ConvertDate.formatDate(jsonobject.getString("dateLimitePaiement"));

                    pd.setDebut(dateDebutAssuj);
                    pd.setFin(dateFinAssuj);
                    pd.setDateLimitePaiement(dateLimiteAssuj);
                    pd.setAssujetissement(new Assujeti(codeAssuj));

                    pdList.add(pd);

                }

                Assujeti assujeti = new Assujeti();
                assujeti.setId(codeAssuj);
                assujeti.setDateDebut(dateDebut);
                assujeti.setDateFin(dateFin);
                assujeti.setGestionnaire(userId);

                if (AssujettissementBusiness.saveNewPeriodeDeclaration(assujeti, pdList)) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {

                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String loadArticleBudgetaireAssujetissable(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<ArticleBudgetaire> listArticleBudgetaires = AssujettissementBusiness.getListArticleBudgetaireImpot();

            if (!listArticleBudgetaires.isEmpty()) {

                List<JSONObject> jsonABList = new ArrayList<>();

                for (ArticleBudgetaire articleBudgetaire : listArticleBudgetaires) {

                    JSONObject jsonAB = new JSONObject();

                    jsonAB.put("codeAB", articleBudgetaire.getCode());
                    jsonAB.put("intituleAB", articleBudgetaire.getIntitule().toUpperCase());
                    jsonAB.put("sigleAB", articleBudgetaire.getSigle() == null ? articleBudgetaire.getIntitule().toUpperCase() : articleBudgetaire.getSigle().toUpperCase());

                    jsonABList.add(jsonAB);

                }

                dataReturn = jsonABList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String addNewModeleVehicule(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String modele = request.getParameter("modele");
            String userId = request.getParameter("userId");

            ModeleVehicule modeleVehicule = AssujettissementBusiness.getModeleVehiculeByName(modele);

            if (modeleVehicule != null) {
                return dataReturn = "2";
            } else {

                if (AssujettissementBusiness.addNewModeleVehicule(modele)) {
                    dataReturn = "1";
                } else {
                    dataReturn = "0";
                }
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String addNewMarqueVehicule(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String marque = request.getParameter("marque");
            String userId = request.getParameter("userId");

            MarqueVehicule marqueVehicule = AssujettissementBusiness.getMarqueVehiculeByName(marque);

            if (marqueVehicule != null) {
                return dataReturn = "2";
            } else {

                if (AssujettissementBusiness.addNewMarqueVehicule(marque)) {
                    dataReturn = "1";
                } else {
                    dataReturn = "0";
                }
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveAccordExonerationVignette(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codePersonne = request.getParameter("codePersonne");
            String typeExoneration = request.getParameter("typeExoneration");
            String idUser = request.getParameter("idUser");
            String typeExoCurrent = request.getParameter("typeExoCurrent");
            String codeAB = "00000000000002302020";

            ExonerationVignette exonerationVignette = new ExonerationVignette();

            exonerationVignette.setFkAb(codeAB);
            exonerationVignette.setFkPersonne(codePersonne);
            exonerationVignette.setTypeExoneration(Integer.valueOf(typeExoneration));
            exonerationVignette.setAgentCreate(Integer.valueOf(idUser));

            if (AssujettissementBusiness.saveAccordExonerationVignette(exonerationVignette,
                    Integer.valueOf(typeExoCurrent))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String modifyAmountBienAutomobileTaxation(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String noteTaxation = request.getParameter("noteTaxation");
            String periodeCode = request.getParameter("periodeCode");
            String codeBien = request.getParameter("codeBien");
            String amountBien = request.getParameter("amountBien");
            String idUser = request.getParameter("userId");

            BigDecimal amount = new BigDecimal("0");
            amount = BigDecimal.valueOf(Double.valueOf(amountBien));

            if (AssujettissementBusiness.modifyAmountBienAutomobileTaxation(Integer.valueOf(noteTaxation), periodeCode,
                    codeBien, Integer.valueOf(idUser), amount)) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {

                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;

            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String deleteBienAutomobileTaxation(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String noteTaxation = request.getParameter("noteTaxation");
            String periodeCode = request.getParameter("periodeCode");
            String codeBien = request.getParameter("codeBien");
            String idUser = request.getParameter("userId");

            /*RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(periodeCode);

             boolean isNoteArchive = false;
             String newNoteArchive = "";

             if (retraitDeclaration != null) {

             Archive archive = NotePerceptionBusiness.getArchiveByRefDocument(retraitDeclaration.getId() + "");

             if (archive != null) {

             isNoteArchive = true;

             RetraitDeclaration rd = AssujettissementBusiness.getRetraitDeclarationByNewID_V2(
             retraitDeclaration.getNewId(), retraitDeclaration.getId());

             newNoteArchive = rd == null ? "" : rd.getId() + "";
             }
             }*/
            if (AssujettissementBusiness.deleteBienAutomobileTaxation(Integer.valueOf(noteTaxation), periodeCode,
                    codeBien, Integer.valueOf(idUser))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;

    }

    public String deleteAccordExonerationVignette(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codePersonne = request.getParameter("codePersonne");
            String idUser = request.getParameter("userId");

            if (AssujettissementBusiness.deleteAccordExonerationVignette(codePersonne, Integer.valueOf(idUser))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveTaxationDupplicatVignette(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String amount = request.getParameter("amount");
            String devise = request.getParameter("devise");
            String codeAB = request.getParameter("codeAB");
            String requerant = request.getParameter("requerant");
            String codePersonne = request.getParameter("codePersonne");
            String codeBanque = request.getParameter("codeBanque");
            String codeCompte = request.getParameter("codeCompte");
            String periodeCode = request.getParameter("periodeCode");
            String codeAgent = request.getParameter("codeAgent");
            String codeSite = request.getParameter("codeSite");
            String newId = "";//request.getParameter("newId");
            String noteTaxationMere = request.getParameter("noteTaxationMere");
            String codeBien = request.getParameter("codeBien");

            String periodeName = "";

            ArticleBudgetaire ab = TaxationBusiness.getArticleBudgetaireByCode(codeAB);
            PeriodeDeclaration pd = AssujettissementBusiness.getPeriodeDeclarationByCode(periodeCode);

            periodeName = Tools.getPeriodeIntitule(pd.getDebut(), ab.getPeriodicite().getCode());

            RetraitDeclaration retraitDeclaration = new RetraitDeclaration();

            retraitDeclaration.setDevise(devise);
            retraitDeclaration.setRequerant(requerant);
            retraitDeclaration.setFkAssujetti(codePersonne);
            retraitDeclaration.setFkAb(codeAB);

            retraitDeclaration.setFkPeriode(periodeCode);
            retraitDeclaration.setFkAgentCreate(codeAgent);
            retraitDeclaration.setMontant(BigDecimal.valueOf(Double.valueOf(amount)));
            retraitDeclaration.setRetraitDeclarationMere(null);

            Date dateEcheance = Tools.getEcheanceDate(new Date(), Integer.valueOf("3"));

            retraitDeclaration.setDateEcheancePaiement(Tools.getEcheanceDate(dateEcheance, Integer.valueOf("3"), false));
            retraitDeclaration.setEstPenalise(0);
            retraitDeclaration.setFkBanque(codeBanque);
            retraitDeclaration.setFkCompteBancaire(codeCompte);
            retraitDeclaration.setFkSite(codeSite);

            newId = AssujettissementBusiness.getNewid();

            retraitDeclaration.setNewId(newId);

            if (AssujettissementBusiness.saveTaxationDupplicatVignette(retraitDeclaration, codeBien,
                    propertiesConfig.getProperty("VALUE_CURRENT_EXERCICE"))) {

                PrintDocument printDocument = new PrintDocument();

                retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationDupplicataByNewID(newId);

                String noteTaxationPrincipale = printDocument.createNoteTaxationVignette(retraitDeclaration);

                /*String noteTaxationPrincipale = printDocument.createNoteTaxationDeclaration(
                 retraitDeclaration, codeAgent,
                 retraitDeclaration.getMontant(),
                 periodeName);*/
                dataReturn = noteTaxationPrincipale;

            } else {
                dataReturn = "0";
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadingPeriodeDeclaration(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codePersonne = request.getParameter("codePersonne");
            String codeListBiens = request.getParameter("codeListBiens");

            JSONArray jsonArray = null;

            jsonArray = new JSONArray(codeListBiens);

            String codeBien = "";
            String codeBienComposite = "";

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);
                codeBien = jsonObject.getString("codeBien");

                if (codeBienComposite.isEmpty()) {

                    codeBienComposite = "'" + codeBien + "'";
                } else {
                    codeBienComposite += codeBienComposite = ",'" + codeBien + "'";
                }
            }

            List<PeriodeDeclaration> listPeriodeDeclarations = AssujettissementBusiness.getListPeriodeDeclarationByPersonneAndListBiens(codePersonne, codeBienComposite);

            int count = 0;

            if (!listPeriodeDeclarations.isEmpty()) {

                List<JSONObject> pdjNObjectList = new ArrayList<>();

                for (PeriodeDeclaration pd : listPeriodeDeclarations) {

                    count++;

                    JSONObject pdjNObject = new JSONObject();

                    pdjNObject.put("code", pd.getId());

                    pdjNObject.put("periode", Tools.getPeriodeIntitule(pd.getDebut(),
                            pd.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode()));

                    long moisRetard = 0;
                    String partYearDateToday = ConvertDate.getYearPartDateFromDateParam(ConvertDate.formatDateToString(new Date()));

                    if (Compare.before(pd.getDateLimite(), new Date())) {

                        String partYearDateParm = ConvertDate.getYearPartDateFromDateParam(ConvertDate.formatDateToString(pd.getFin()));

                        if (partYearDateParm.equals(partYearDateToday)) {

                            moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates_V2(
                                    ConvertDate.formatDateToString(pd.getDateLimite()), ConvertDate.formatDateToString(new Date()));
                        } else {
                            moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates_V2(
                                    ConvertDate.formatDateToString(pd.getDateLimite()), ConvertDate.formatDateToString(pd.getFin()));
                        }

                        if (moisRetard == 11 && count > 1) {
                            moisRetard = moisRetard + 1;
                        }

                    } else {
                        moisRetard = 0;
                    }

                    pdjNObject.put("moisRetard", moisRetard);
                    pdjNObject.put("estPenalise", moisRetard == 0 ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE);

                    pdjNObjectList.add(pdjNObject);

                }

                dataReturn = pdjNObjectList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String taxationExonerationVignette(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codePersonne = request.getParameter("codePersonne");
            String selectBanqueExo = request.getParameter("selectBanqueExo");
            String selectCompteBancaireExo = request.getParameter("selectCompteBancaireExo");
            String listBiens = request.getParameter("listBiens");
            String typeExoneration = request.getParameter("typeExoneration");
            String userId = request.getParameter("userId");
            String codeAB = request.getParameter("codeAB");
            String requerant = request.getParameter("requerant");
            String codeSite = request.getParameter("codeSite");

            JSONArray jsonBienTaxation = new JSONArray(listBiens);

            List<PeriodeDeclaration> listPeriodeDeclarations = new ArrayList<>();

            for (int i = 0; i < jsonBienTaxation.length(); i++) {

                JSONObject jsonObject = jsonBienTaxation.getJSONObject(i);

                String codeBien = jsonObject.getString("codeBien");

                PeriodeDeclaration periodeDeclaration = AssujettissementBusiness.getPeriodeDeclarationVignetteByPersonneAndBien(
                        codePersonne, codeAB, codeBien,
                        Integer.valueOf(propertiesConfig.getProperty("VALUE_CURRENT_EXERCICE")));

                if (periodeDeclaration != null) {
                    listPeriodeDeclarations.add(periodeDeclaration);
                }

            }

            if (!listPeriodeDeclarations.isEmpty()) {

                List<DeclarationMock> declarationListMock = new ArrayList<>();
                List<TaxationBienAutomobile> listTaxationBienAutomobile = new ArrayList<>();

                String newIDGenerate = AssujettissementBusiness.getNewid();
                String exerciceCurrentName = "";
                String exerciceCompositeName = "";

                BigDecimal amountPrincipGlobal = new BigDecimal("0");

                for (PeriodeDeclaration pd : listPeriodeDeclarations) {

                    DeclarationMock declarationMock = new DeclarationMock();

                    BigDecimal amountPrincipal = new BigDecimal("0");

                    if (typeExoneration.equals("1")) {

                        TauxVignette tauxVignette = null;

                        switch (pd.getAssujetissement().getBien().getFkTarif()) {

                            case "0000071020": // 1 - 10 CV
                            case "0000071120": // 11 - 15 CV
                            case "0000071220": // +15 CV

                                tauxVignette = DeclarationBusiness.getTauxVignetteV1(
                                        pd.getAssujetissement().getPersonne().getFormeJuridique().getCode(),
                                        pd.getAssujetissement().getBien().getFkTarif());

                                break;

                            default:

                                tauxVignette = DeclarationBusiness.getTauxVignetteV2(
                                        pd.getAssujetissement().getBien().getFkTarif());

                                break;

                        }

                        amountPrincipal = amountPrincipal.add(tauxVignette.getTscr());
                    }

                    amountPrincipGlobal = amountPrincipGlobal.add(amountPrincipal);

                    //Insérer les biens dans la table TaxationBienAutomobile
                    TaxationBienAutomobile taxationBienAutomobile = new TaxationBienAutomobile();

                    taxationBienAutomobile.setPrincipalDu(amountPrincipal);
                    taxationBienAutomobile.setPenaliteDu(BigDecimal.ZERO);
                    taxationBienAutomobile.setFkBien(pd.getAssujetissement().getBien().getId());
                    taxationBienAutomobile.setExercice(propertiesConfig.getProperty("VALUE_CURRENT_EXERCICE"));
                    listTaxationBienAutomobile.add(taxationBienAutomobile);

                    exerciceCurrentName = Tools.getPeriodeIntitule(pd.getDebut(),
                            pd.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode());

                    if (exerciceCompositeName.isEmpty()) {

                        exerciceCompositeName = exerciceCurrentName.concat(GeneralConst.SPACE);

                    } else {

                        if (!exerciceCompositeName.contains(propertiesConfig.getProperty("VALUE_CURRENT_EXERCICE"))) {
                            exerciceCompositeName += exerciceCompositeName = exerciceCurrentName.concat(GeneralConst.SPACE);
                        }

                    }

                    declarationMock.setAmountPrincipal(amountPrincipal);
                    declarationMock.setDevise("USD");
                    declarationMock.setRequerant(requerant);
                    declarationMock.setAssujetti(codePersonne);
                    declarationMock.setCodeDeclaration(null);
                    declarationMock.setCodeArticleBudgetaire(codeAB);
                    declarationMock.setCodeAgentCreate(userId);
                    declarationMock.setPeriode(pd.getId() + "");

                    Date dateCreateDeclaration = new Date();

                    declarationMock.setDateCreate(dateCreateDeclaration);
                    declarationMock.setEstPenalise(0);

                    declarationMock.setBanque(selectBanqueExo);
                    declarationMock.setCompteBancaire(selectCompteBancaireExo);

                    Date dateEcheance = Tools.getEcheanceDate(dateCreateDeclaration, Integer.valueOf("3"));

                    declarationMock.setDateEcheancePaiement(Tools.getEcheanceDate(dateEcheance, Integer.valueOf("3"), false));

                    declarationListMock.add(declarationMock);
                }

                boolean result = AssujettissementBusiness.saveRetraitDeclarationExonerationVignette(
                        declarationListMock, listTaxationBienAutomobile, codeSite, newIDGenerate);

                if (result) {

                    for (PeriodeDeclaration pd : listPeriodeDeclarations) {
                        createPeriodesDeclarationsV2(pd.getId());
                    }

                    PrintDocument printDocument = new PrintDocument();

                    RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getFirstRetraitDeclarationExoByNewID(newIDGenerate);

                    String noteTaxationPrincipale = printDocument.createNoteTaxationVignette(retraitDeclaration);

                    /*String noteTaxationPrincipale = printDocument.createNoteTaxationDeclaration(
                     retraitDeclaration, userId,
                     amountPrincipGlobal,
                     exerciceCompositeName);*/
                    dataReturn = noteTaxationPrincipale;

                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                dataReturn = "2";
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String deleteBienAutomobile(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codeBien = request.getParameter("codeBien");
            String codePersonne = request.getParameter("codePersonne");

            if (AssujettissementBusiness.deleteBienAutomobile(codeBien, codePersonne)) {
                dataReturn = "1";
            } else {
                dataReturn = "0";
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String addNewGenreVehicule(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String genre = request.getParameter("genre");
            String userId = request.getParameter("userId");

            TypeBien typeBien = AssujettissementBusiness.getTypeBienByName(genre);

            if (typeBien != null) {
                return dataReturn = "2";
            } else {

                if (AssujettissementBusiness.addNewGenreVehicule(genre)) {
                    dataReturn = "1";
                } else {
                    dataReturn = "0";
                }
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadBienAutomobileByIDBien(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codePersonne = request.getParameter("codePersonne");
            String codeBien = request.getParameter("codeBien");

            List<Bien> listBienAutomobile = AssujettissementBusiness.getListBienAutomobileByContribuableAndBien(
                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                    codePersonne,
                    codeBien);

            if (!listBienAutomobile.isEmpty()) {

                List<JSONObject> jsonBienList = new ArrayList<>();

                for (Bien bien : listBienAutomobile) {

                    JSONObject jsonBien = new JSONObject();

                    Tarif tarif = null;

                    if (bien.getFkTarif() != null) {
                        tarif = TaxationBusiness.getTarifByCode(bien.getFkTarif());
                    }

                    ComplementBienAutomobile complementBienAutomobile = TaxationBusiness.getComplementBienAutomobileByBien(bien.getId());
                    Personne personne = IdentificationBusiness.getPersonneByCode(codePersonne);

                    if (complementBienAutomobile.getFkMarque() != null) {

                        MarqueVehicule marqueVehicule = TaxationBusiness.getMarqueVehiculeByID(
                                complementBienAutomobile.getFkMarque());

                        jsonBien.put("marquecode", marqueVehicule.getId());
                        jsonBien.put("marqueName", marqueVehicule.getIntitule().toUpperCase());

                    } else {

                        jsonBien.put("marqueName", "");
                        jsonBien.put("marquecode", "");
                    }

                    if (complementBienAutomobile.getFkModele() != null) {

                        ModeleVehicule modeleVehicule = TaxationBusiness.getModeleVehiculeByID(
                                complementBienAutomobile.getFkModele());

                        jsonBien.put("modelecode", modeleVehicule.getId());
                        jsonBien.put("modeleName", modeleVehicule.getIntitule().toUpperCase());

                    } else {

                        jsonBien.put("modelecode", "");
                        jsonBien.put("modeleName", "");
                    }

                    TaxationBienAutomobile taxationBienAutomobile = TaxationBusiness.getTaxationBienAutomobileByBien(
                            bien.getId(),
                            propertiesConfig.getProperty("VALUE_CURRENT_EXERCICE"));

                    TaxationBienAutomobile taxationBienAutomobile2 = TaxationBusiness.getTaxationBienAutomobileByBienV2(
                            bien.getId());

                    if (taxationBienAutomobile2 != null) {
                        jsonBien.put("taxationExist", true);
                    } else {
                        jsonBien.put("taxationExist", false);
                    }

                    jsonBien.put("codeBien", bien.getId());
                    jsonBien.put("bienName", bien.getIntitule());

                    Assujeti assujeti = TaxationBusiness.getAssujetiByPersonneAndBien(codePersonne, bien.getId());

                    jsonBien.put("idAssujettissement", assujeti == null ? 0 : assujeti.getId());

                    jsonBien.put("codePersonne", codePersonne);

                    jsonBien.put("codeTypeBien", bien.getTypeBien().getCode());
                    jsonBien.put("nameTypeBien", bien.getTypeBien().getIntitule().toUpperCase());

                    jsonBien.put("codeTarifBien", tarif == null ? "" : tarif.getCode());
                    jsonBien.put("nameTarifBien", tarif == null ? "" : tarif.getIntitule().toUpperCase());

                    jsonBien.put("denomination", bien.getIntitule());

                    jsonBien.put("dateMiseCirculation1", complementBienAutomobile.getDateMiseCirculation() == null ? ""
                            : ConvertDate.formatDateToString(complementBienAutomobile.getDateMiseCirculation()));

                    jsonBien.put("dateMiseCirculation2", complementBienAutomobile.getDateMiseCirculation() == null ? ""
                            : ConvertDate.getValidFormatDateStringV3(ConvertDate.formatDateToString(complementBienAutomobile.getDateMiseCirculation())));

                    jsonBien.put("plaque", complementBienAutomobile == null ? "" : complementBienAutomobile.getNumeroPlaque());
                    jsonBien.put("chassis", complementBienAutomobile == null ? "" : complementBienAutomobile.getNumeroChassis());

                    jsonBien.put("puissanceFiscal", complementBienAutomobile == null ? "" : complementBienAutomobile.getPuissanceFiscale());
                    jsonBien.put("codeUnitePuissance", complementBienAutomobile == null ? "" : complementBienAutomobile.getUnitePuissanceFiscale());

                    if (complementBienAutomobile != null) {

                        Unite unite = TaxationBusiness.getUnitebyCode(complementBienAutomobile.getUnitePuissanceFiscale());
                        jsonBien.put("nameUnitePuissance", unite == null ? "" : unite.getIntitule().toUpperCase());
                    } else {
                        jsonBien.put("nameUnitePuissance", "CV");
                    }

                    jsonBien.put("anneeCirculation", complementBienAutomobile == null ? "" : complementBienAutomobile.getAnneeMiseCirculation());
                    jsonBien.put("codeAdressePersonneBien", bien.getFkAdressePersonne().getCode());
                    jsonBien.put("nameAdresse", bien.getFkAdressePersonne().getAdresse().toString().toUpperCase());

                    Palier palier = null;

                    switch (bien.getFkTarif()) {

                        case "0000071020": // 1 - 10 CV
                        case "0000071120": // 11 - 15 CV
                        case "0000071220": // +15 CV

                            palier = DeclarationBusiness.getTauxPalierVignette(
                                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                    bien.getFkTarif(), personne.getFormeJuridique().getCode());

                            break;

                        default:

                            palier = DeclarationBusiness.getTauxPalierVignetteV2(
                                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"), bien.getFkTarif());

                            break;

                    }

                    jsonBien.put("tauxVignette", palier == null ? 0 : palier.getTaux());
                    jsonBien.put("devise", palier == null ? "USD" : palier.getDevise().getCode());

                    jsonBien.put("isDeclared", taxationBienAutomobile == null ? "0" : "1");
                    jsonBien.put("noteTaxation", taxationBienAutomobile == null ? "" : taxationBienAutomobile.getFkNoteTaxation());

                    jsonBienList.add(jsonBien);

                }

                dataReturn = jsonBienList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;

    }

    public String loadBienAutomobileContribuableNoteTaxed(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codePersonne = request.getParameter("codePersonne");
            //String noteTaxation = request.getParameter("noteTaxation");

            List<Bien> listBienAutomobile = AssujettissementBusiness.getListBienAutomobileByContribuableNotTaxed(
                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                    codePersonne
            );

            if (!listBienAutomobile.isEmpty()) {

                List<JSONObject> jsonBienList = new ArrayList<>();

                for (Bien bien : listBienAutomobile) {

                    JSONObject jsonBien = new JSONObject();

                    Tarif tarif = null;

                    if (bien.getFkTarif() != null) {
                        tarif = TaxationBusiness.getTarifByCode(bien.getFkTarif());
                    }

                    ComplementBienAutomobile complementBienAutomobile = TaxationBusiness.getComplementBienAutomobileByBien(bien.getId());
                    Personne personne = IdentificationBusiness.getPersonneByCode(codePersonne);

                    if (complementBienAutomobile.getFkMarque() != null) {

                        MarqueVehicule marqueVehicule = TaxationBusiness.getMarqueVehiculeByID(
                                complementBienAutomobile.getFkMarque());

                        jsonBien.put("marquecode", marqueVehicule.getId());
                        jsonBien.put("marqueName", marqueVehicule.getIntitule().toUpperCase());

                    } else {

                        jsonBien.put("marqueName", "");
                        jsonBien.put("marquecode", "");
                    }

                    if (complementBienAutomobile.getFkModele() != null) {

                        ModeleVehicule modeleVehicule = TaxationBusiness.getModeleVehiculeByID(
                                complementBienAutomobile.getFkModele());

                        jsonBien.put("modelecode", modeleVehicule.getId());
                        jsonBien.put("modeleName", modeleVehicule.getIntitule().toUpperCase());

                    } else {

                        jsonBien.put("modelecode", "");
                        jsonBien.put("modeleName", "");
                    }

                    TaxationBienAutomobile taxationBienAutomobile = TaxationBusiness.getTaxationBienAutomobileByBien(
                            bien.getId(),
                            propertiesConfig.getProperty("VALUE_CURRENT_EXERCICE"));

                    TaxationBienAutomobile taxationBienAutomobile2 = TaxationBusiness.getTaxationBienAutomobileByBienV2(
                            bien.getId());

                    if (taxationBienAutomobile2 != null) {
                        jsonBien.put("taxationExist", true);
                    } else {
                        jsonBien.put("taxationExist", false);
                    }

                    jsonBien.put("codeBien", bien.getId());
                    jsonBien.put("bienName", bien.getIntitule());

                    Assujeti assujeti = TaxationBusiness.getAssujetiByPersonneAndBien(codePersonne, bien.getId());

                    jsonBien.put("idAssujettissement", assujeti == null ? 0 : assujeti.getId());

                    jsonBien.put("codePersonne", codePersonne);

                    jsonBien.put("codeTypeBien", bien.getTypeBien().getCode());
                    jsonBien.put("nameTypeBien", bien.getTypeBien().getIntitule().toUpperCase());

                    jsonBien.put("codeTarifBien", tarif == null ? "" : tarif.getCode());
                    jsonBien.put("nameTarifBien", tarif == null ? "" : tarif.getIntitule().toUpperCase());

                    jsonBien.put("denomination", bien.getIntitule());

                    jsonBien.put("dateMiseCirculation1", complementBienAutomobile.getDateMiseCirculation() == null ? ""
                            : ConvertDate.formatDateToString(complementBienAutomobile.getDateMiseCirculation()));

                    jsonBien.put("dateMiseCirculation2", complementBienAutomobile.getDateMiseCirculation() == null ? ""
                            : ConvertDate.getValidFormatDateStringV3(ConvertDate.formatDateToString(complementBienAutomobile.getDateMiseCirculation())));

                    jsonBien.put("plaque", complementBienAutomobile == null ? "" : complementBienAutomobile.getNumeroPlaque());
                    jsonBien.put("chassis", complementBienAutomobile == null ? "" : complementBienAutomobile.getNumeroChassis());

                    jsonBien.put("puissanceFiscal", complementBienAutomobile == null ? "" : complementBienAutomobile.getPuissanceFiscale());
                    jsonBien.put("codeUnitePuissance", complementBienAutomobile == null ? "" : complementBienAutomobile.getUnitePuissanceFiscale());

                    if (complementBienAutomobile != null) {

                        Unite unite = TaxationBusiness.getUnitebyCode(complementBienAutomobile.getUnitePuissanceFiscale());
                        jsonBien.put("nameUnitePuissance", unite == null ? "" : unite.getIntitule().toUpperCase());
                    } else {
                        jsonBien.put("nameUnitePuissance", "CV");
                    }

                    jsonBien.put("anneeCirculation", complementBienAutomobile == null ? "" : complementBienAutomobile.getAnneeMiseCirculation());
                    jsonBien.put("codeAdressePersonneBien", bien.getFkAdressePersonne().getCode());
                    jsonBien.put("nameAdresse", bien.getFkAdressePersonne().getAdresse().toString().toUpperCase());

                    Palier palier = null;

                    switch (bien.getFkTarif()) {

                        case "0000071020": // 1 - 10 CV
                        case "0000071120": // 11 - 15 CV
                        case "0000071220": // +15 CV

                            palier = DeclarationBusiness.getTauxPalierVignette(
                                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                    bien.getFkTarif(), personne.getFormeJuridique().getCode());

                            break;

                        default:

                            palier = DeclarationBusiness.getTauxPalierVignetteV2(
                                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"), bien.getFkTarif());

                            break;

                    }

                    jsonBien.put("tauxVignette", palier == null ? 0 : palier.getTaux());
                    jsonBien.put("devise", palier == null ? "USD" : palier.getDevise().getCode());

                    jsonBien.put("isDeclared", taxationBienAutomobile == null ? "0" : "1");

                    jsonBienList.add(jsonBien);

                }

                dataReturn = jsonBienList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;

    }

    public String getCotationBienAutomobileByContribuable(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codePersonne = request.getParameter("codePersonne");
            String codeFormeJ = request.getParameter("codeFormeJ");

            String dataCotation = TaxationBusiness.getJsonCotationBienAutomobile(
                    codePersonne, propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"), codeFormeJ);

            if (dataCotation.isEmpty()) {
                dataReturn = "0";
            } else {
                dataReturn = dataCotation;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadBienAutomobileByContribuable(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codePersonne = request.getParameter("codePersonne");

            List<Bien> listBienAutomobile = AssujettissementBusiness.getListBienAutomobileByContribuable(
                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                    codePersonne);

            if (!listBienAutomobile.isEmpty()) {

                List<JSONObject> jsonBienList = new ArrayList<>();

                for (Bien bien : listBienAutomobile) {

                    JSONObject jsonBien = new JSONObject();

                    Tarif tarif = null;

                    if (bien.getFkTarif() != null) {
                        tarif = TaxationBusiness.getTarifByCode(bien.getFkTarif());
                    }

                    ComplementBienAutomobile complementBienAutomobile = TaxationBusiness.getComplementBienAutomobileByBien(bien.getId());
                    Personne personne = IdentificationBusiness.getPersonneByCode(codePersonne);

                    if (complementBienAutomobile.getFkMarque() != null) {

                        MarqueVehicule marqueVehicule = TaxationBusiness.getMarqueVehiculeByID(
                                complementBienAutomobile.getFkMarque());

                        jsonBien.put("marquecode", marqueVehicule == null ? "" : marqueVehicule.getId());
                        jsonBien.put("marqueName", marqueVehicule == null ? "" : marqueVehicule.getIntitule().toUpperCase());

                    } else {

                        jsonBien.put("marqueName", "");
                        jsonBien.put("marquecode", "");
                    }

                    if (complementBienAutomobile.getFkModele() != null) {

                        ModeleVehicule modeleVehicule = TaxationBusiness.getModeleVehiculeByID(
                                complementBienAutomobile.getFkModele());

                        jsonBien.put("modelecode", modeleVehicule == null ? "" : modeleVehicule.getId());
                        jsonBien.put("modeleName", modeleVehicule == null ? "" : modeleVehicule.getIntitule().toUpperCase());

                    } else {

                        jsonBien.put("modelecode", "");
                        jsonBien.put("modeleName", "");
                    }

                    TaxationBienAutomobile taxationBienAutomobile = TaxationBusiness.getTaxationBienAutomobileByBien(
                            bien.getId(),
                            propertiesConfig.getProperty("VALUE_CURRENT_EXERCICE"));

                    TaxationBienAutomobile taxationBienAutomobile2 = TaxationBusiness.getTaxationBienAutomobileByBienV2(
                            bien.getId());

                    if (taxationBienAutomobile2 != null) {
                        jsonBien.put("taxationExist", true);
                    } else {
                        jsonBien.put("taxationExist", false);
                    }

                    jsonBien.put("codeBien", bien.getId());
                    jsonBien.put("bienName", bien.getIntitule());

                    Assujeti assujeti = TaxationBusiness.getAssujetiByPersonneAndBien(codePersonne, bien.getId());

                    jsonBien.put("idAssujettissement", assujeti == null ? 0 : assujeti.getId());

                    jsonBien.put("codePersonne", codePersonne);

                    jsonBien.put("codeTypeBien", bien.getTypeBien().getCode());
                    jsonBien.put("nameTypeBien", bien.getTypeBien().getIntitule().toUpperCase());

                    jsonBien.put("codeTarifBien", tarif == null ? "" : tarif.getCode());
                    jsonBien.put("nameTarifBien", tarif == null ? "" : tarif.getIntitule().toUpperCase());

                    jsonBien.put("denomination", bien.getIntitule());

                    jsonBien.put("dateMiseCirculation1", complementBienAutomobile.getDateMiseCirculation() == null ? ""
                            : ConvertDate.formatDateToString(complementBienAutomobile.getDateMiseCirculation()));

                    jsonBien.put("dateMiseCirculation2", complementBienAutomobile.getDateMiseCirculation() == null ? ""
                            : ConvertDate.getValidFormatDateStringV3(ConvertDate.formatDateToString(complementBienAutomobile.getDateMiseCirculation())));

                    jsonBien.put("plaque", complementBienAutomobile == null ? "" : complementBienAutomobile.getNumeroPlaque());
                    jsonBien.put("chassis", complementBienAutomobile == null ? "" : complementBienAutomobile.getNumeroChassis());

                    jsonBien.put("puissanceFiscal", complementBienAutomobile == null ? "" : complementBienAutomobile.getPuissanceFiscale());
                    jsonBien.put("codeUnitePuissance", complementBienAutomobile == null ? "" : complementBienAutomobile.getUnitePuissanceFiscale());

                    if (complementBienAutomobile != null) {

                        Unite unite = TaxationBusiness.getUnitebyCode(complementBienAutomobile.getUnitePuissanceFiscale());
                        jsonBien.put("nameUnitePuissance", unite == null ? "" : unite.getIntitule().toUpperCase());
                    } else {
                        jsonBien.put("nameUnitePuissance", "CV");
                    }

                    jsonBien.put("anneeCirculation", complementBienAutomobile == null ? "" : complementBienAutomobile.getAnneeMiseCirculation());
                    jsonBien.put("codeAdressePersonneBien", bien.getFkAdressePersonne().getCode());
                    jsonBien.put("nameAdresse", bien.getFkAdressePersonne() == null ? "" : bien.getFkAdressePersonne().getAdresse().toString().toUpperCase());

                    Palier palier = null;

                    switch (bien.getFkTarif()) {

                        case "0000071020": // 1 - 10 CV
                        case "0000071120": // 11 - 15 CV
                        case "0000071220": // +15 CV

                            palier = DeclarationBusiness.getTauxPalierVignette(
                                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                    bien.getFkTarif(), personne.getFormeJuridique().getCode());

                            break;

                        default:

                            palier = DeclarationBusiness.getTauxPalierVignetteV2(
                                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"), bien.getFkTarif());

                            break;

                    }

                    if (bien.getTypeBien().getCode().equals(propertiesConfig.getProperty("CODE_TYPE_BIEN_AMBULANCE"))) {

                        palier = DeclarationBusiness.getTauxPalierVignetteV3(
                                propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                bien.getFkTarif(), personne.getFormeJuridique().getCode(),
                                propertiesConfig.getProperty("CODE_TYPE_BIEN_AMBULANCE"));
                    }

                    jsonBien.put("tauxVignette", palier == null ? 0 : palier.getTaux());
                    jsonBien.put("devise", palier == null ? "USD" : palier.getDevise().getCode());

                    jsonBien.put("isDeclared", taxationBienAutomobile == null ? "0" : "1");
                    jsonBien.put("estVu", complementBienAutomobile == null ? "NON" : complementBienAutomobile.getEstVu() == 0 ? "NON" : "OUI");

                    jsonBienList.add(jsonBien);

                }

                dataReturn = jsonBienList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadFiveLastBienAutomobileByContribuable(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codePersonne = request.getParameter("codePersonne");

            List<Bien> listBienAutomobile = AssujettissementBusiness.getListLastFiveBienAutomobileByContribuable(
                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                    codePersonne);

            if (!listBienAutomobile.isEmpty()) {

                List<JSONObject> jsonBienList = new ArrayList<>();

                for (Bien bien : listBienAutomobile) {

                    JSONObject jsonBien = new JSONObject();

                    Tarif tarif = null;

                    if (bien.getFkTarif() != null) {
                        tarif = TaxationBusiness.getTarifByCode(bien.getFkTarif());
                    }

                    ComplementBienAutomobile complementBienAutomobile = TaxationBusiness.getComplementBienAutomobileByBien(bien.getId());
                    Personne personne = IdentificationBusiness.getPersonneByCode(codePersonne);

                    if (complementBienAutomobile.getFkMarque() != null) {

                        MarqueVehicule marqueVehicule = TaxationBusiness.getMarqueVehiculeByID(
                                complementBienAutomobile.getFkMarque());

                        jsonBien.put("marquecode", marqueVehicule == null ? "" : marqueVehicule.getId());
                        jsonBien.put("marqueName", marqueVehicule == null ? "" : marqueVehicule.getIntitule().toUpperCase());

                    } else {

                        jsonBien.put("marqueName", "");
                        jsonBien.put("marquecode", "");
                    }

                    if (complementBienAutomobile.getFkModele() != null) {

                        ModeleVehicule modeleVehicule = TaxationBusiness.getModeleVehiculeByID(
                                complementBienAutomobile.getFkModele());

                        jsonBien.put("modelecode", modeleVehicule == null ? "" : modeleVehicule.getId());
                        jsonBien.put("modeleName", modeleVehicule == null ? "" : modeleVehicule.getIntitule().toUpperCase());

                    } else {

                        jsonBien.put("modelecode", "");
                        jsonBien.put("modeleName", "");
                    }

                    TaxationBienAutomobile taxationBienAutomobile = TaxationBusiness.getTaxationBienAutomobileByBien(
                            bien.getId(),
                            propertiesConfig.getProperty("VALUE_CURRENT_EXERCICE"));

                    TaxationBienAutomobile taxationBienAutomobile2 = TaxationBusiness.getTaxationBienAutomobileByBienV2(
                            bien.getId());

                    if (taxationBienAutomobile2 != null) {
                        jsonBien.put("taxationExist", true);
                    } else {
                        jsonBien.put("taxationExist", false);
                    }

                    jsonBien.put("codeBien", bien.getId());
                    jsonBien.put("bienName", bien.getIntitule());

                    Assujeti assujeti = TaxationBusiness.getAssujetiByPersonneAndBien(codePersonne, bien.getId());

                    jsonBien.put("idAssujettissement", assujeti == null ? 0 : assujeti.getId());

                    jsonBien.put("codePersonne", codePersonne);

                    jsonBien.put("codeTypeBien", bien.getTypeBien().getCode());
                    jsonBien.put("nameTypeBien", bien.getTypeBien().getIntitule().toUpperCase());

                    jsonBien.put("codeTarifBien", tarif == null ? "" : tarif.getCode());
                    jsonBien.put("nameTarifBien", tarif == null ? "" : tarif.getIntitule().toUpperCase());

                    jsonBien.put("denomination", bien.getIntitule());

                    jsonBien.put("dateMiseCirculation1", complementBienAutomobile.getDateMiseCirculation() == null ? ""
                            : ConvertDate.formatDateToString(complementBienAutomobile.getDateMiseCirculation()));

                    jsonBien.put("dateMiseCirculation2", complementBienAutomobile.getDateMiseCirculation() == null ? ""
                            : ConvertDate.getValidFormatDateStringV3(ConvertDate.formatDateToString(complementBienAutomobile.getDateMiseCirculation())));

                    jsonBien.put("plaque", complementBienAutomobile == null ? "" : complementBienAutomobile.getNumeroPlaque());
                    jsonBien.put("chassis", complementBienAutomobile == null ? "" : complementBienAutomobile.getNumeroChassis());

                    jsonBien.put("puissanceFiscal", complementBienAutomobile == null ? "" : complementBienAutomobile.getPuissanceFiscale());
                    jsonBien.put("codeUnitePuissance", complementBienAutomobile == null ? "" : complementBienAutomobile.getUnitePuissanceFiscale());

                    if (complementBienAutomobile != null) {

                        Unite unite = TaxationBusiness.getUnitebyCode(complementBienAutomobile.getUnitePuissanceFiscale());
                        jsonBien.put("nameUnitePuissance", unite == null ? "" : unite.getIntitule().toUpperCase());
                    } else {
                        jsonBien.put("nameUnitePuissance", "CV");
                    }

                    jsonBien.put("anneeCirculation", complementBienAutomobile == null ? "" : complementBienAutomobile.getAnneeMiseCirculation());
                    jsonBien.put("codeAdressePersonneBien", bien.getFkAdressePersonne().getCode());
                    jsonBien.put("nameAdresse", bien.getFkAdressePersonne() == null ? "" : bien.getFkAdressePersonne().getAdresse().toString().toUpperCase());

                    Palier palier = null;

                    switch (bien.getFkTarif()) {

                        case "0000071020": // 1 - 10 CV
                        case "0000071120": // 11 - 15 CV
                        case "0000071220": // +15 CV

                            palier = DeclarationBusiness.getTauxPalierVignette(
                                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                    bien.getFkTarif(), personne.getFormeJuridique().getCode());

                            break;

                        default:

                            palier = DeclarationBusiness.getTauxPalierVignetteV2(
                                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"), bien.getFkTarif());

                            break;

                    }

                    if (bien.getTypeBien().getCode().equals(propertiesConfig.getProperty("CODE_TYPE_BIEN_AMBULANCE"))) {

                        palier = DeclarationBusiness.getTauxPalierVignetteV3(
                                propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                bien.getFkTarif(), personne.getFormeJuridique().getCode(),
                                propertiesConfig.getProperty("CODE_TYPE_BIEN_AMBULANCE"));
                    }

                    jsonBien.put("tauxVignette", palier == null ? 0 : palier.getTaux());
                    jsonBien.put("devise", palier == null ? "USD" : palier.getDevise().getCode());

                    jsonBien.put("isDeclared", taxationBienAutomobile == null ? "0" : "1");
                    jsonBien.put("estVu", complementBienAutomobile == null ? "NON" : complementBienAutomobile.getEstVu() == 0 ? "NON" : "OUI");

                    jsonBienList.add(jsonBien);

                }

                dataReturn = jsonBienList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String deleteAssujettissement(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String id = request.getParameter("id");
            String userId = request.getParameter("userId");

            if (AssujettissementBusiness.deleteAssujettissement(id, Integer.valueOf(userId))) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String saveBienAutomobile(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;

        try {

            String codeGenre = request.getParameter("codeGenre");
            String codePersonne = request.getParameter("codePersonne");
            String denomination = request.getParameter("denomination");
            String marque = request.getParameter("marque");
            String type = request.getParameter("type");
            String plaque = request.getParameter("plaque");
            String chassis = request.getParameter("chassis");
            String puissanceFiscale = request.getParameter("puissanceFiscale");
            String unitePuissance = request.getParameter("unitePuissance");
            String exercice = request.getParameter("exercice");
            String anneMiseCirculation = request.getParameter("anneMiseCirculation");
            String codeAP = request.getParameter("codeAP");
            String idUser = request.getParameter("idUser");
            String categorie = request.getParameter("categorie");
            String fjPersonne = request.getParameter("fjPersonne");
            String codeBien = request.getParameter("codeBien");
            String dateMiseCirculation = request.getParameter("dateMiseCirculation");

            if (denomination.contains("'")) {

                denomination = denomination.replaceAll("'", " ");
            }

            /*ComplementBienAutomobile cba1 = AssujettissementBusiness.getComplementBienAutomobileByPlaque(plaque);

             if (cba1 != null) {

             if (!codeBien.equals(cba1.getFkBien())) {
             return result = "2";
             }
             }*/

            /*ComplementBienAutomobile cba2 = AssujettissementBusiness.getComplementBienAutomobileByChassis(plaque);

             if (cba2 != null) {

             if (!codeBien.equals(cba2.getFkBien())) {
             return result = "3";
             }
             }*/
            AdressePersonne adressePersonne = TaxationBusiness.getAdressePersonneByCode(codeAP);

            Bien bien = new Bien();
            bien.setDescription(null);
            bien.setIntitule(denomination);
            bien.setTypeBien(new TypeBien(codeGenre));
            bien.setFkAdressePersonne(adressePersonne);
            bien.setFkTarif(categorie);

            int isVU = 0;

            switch (categorie) {

                case "0000074320":
                case "0000074220":
                case "0000074420":

                    isVU = 1;

                    break;
                default:
                    isVU = 0;
                    break;
            }

            ComplementBienAutomobile complementBienAutomobile = new ComplementBienAutomobile();

            complementBienAutomobile.setMarque(null);
            complementBienAutomobile.setType(null);

            complementBienAutomobile.setFkMarque(Integer.valueOf(marque));
            complementBienAutomobile.setFkModele(Integer.valueOf(type));

            Date dateBienAutomobile = ConvertDate.formatDate(ConvertDate.getValidFormatDatePrint(dateMiseCirculation));

            complementBienAutomobile.setDateMiseCirculation(dateBienAutomobile);
            complementBienAutomobile.setNumeroChassis(chassis);
            complementBienAutomobile.setNumeroPlaque(plaque);

            complementBienAutomobile.setAnneeMiseCirculation(
                    ConvertDate.getYearPartDateFromDateParam(
                            ConvertDate.formatDateToStringOfFormat(dateBienAutomobile, "dd/MM/yyyy")));

            //int valuePf = Integer.valueOf(puissanceFiscale);
            //complementBienAutomobile.setPuissanceFiscale(valuePf + "");
            complementBienAutomobile.setPuissanceFiscale(puissanceFiscale);
            complementBienAutomobile.setUnitePuissanceFiscale(unitePuissance);

            Acquisition acquisition = new Acquisition();
            acquisition.setDateAcquisition(ConvertDate.formatDateToStringOfFormat(dateBienAutomobile, "dd/MM/yyyy"));
            acquisition.setPersonne(new Personne(codePersonne));

            String codePeriodiciteAB = "PR0042015";

            ArticleBudgetaire articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(
                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"));

            Assujeti assujeti = new Assujeti();

            assujeti.setArticleBudgetaire(articleBudgetaire);
            assujeti.setPeriodicite(codePeriodiciteAB);
            assujeti.setPersonne(new Personne(codePersonne));
            assujeti.setDuree(Short.valueOf("1"));
            assujeti.setRenouvellement(Short.valueOf("0"));
            assujeti.setDateDebut("01/01/" + exercice);
            assujeti.setDateFin("31/12/" + exercice);
            assujeti.setValeur(BigDecimal.ONE);
            assujeti.setTarif(new Tarif(categorie));
            assujeti.setNbrJourLimite(0);
            assujeti.setGestionnaire(idUser);
            assujeti.setEtat(Boolean.TRUE);
            assujeti.setFkAdressePersonne(new AdressePersonne(codeAP));

            String dateLimite = ConvertDate.getValidFormatDatePrint(articleBudgetaire.getEcheanceLegale());

            if (AssujettissementBusiness.saveBienAutomobile(bien, complementBienAutomobile, assujeti, acquisition, dateLimite, codeBien, isVU)) {

                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String deleteAllBienAutomobileNotTaxed(HttpServletRequest request) {

        String dataReturn = "";

        try {
            String codePersonne = request.getParameter("codePersonne");
            String userId = request.getParameter("userId");

            if (AssujettissementBusiness.deleteAllBienAutomobileNotTaxed(codePersonne,
                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                    propertiesConfig.getProperty("VALUE_CURRENT_EXERCICE"))) {

                dataReturn = "1";

            } else {
                dataReturn = "0";
            }

        } catch (Exception e) {
            dataReturn = "-1";
        }

        return dataReturn;
    }

    public String changeBienAutomobileTaxation(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codeOldBien = request.getParameter("codeOldBien");
            String codeNewBien = request.getParameter("codeNewBien");
            String noteTaxation = request.getParameter("noteTaxation");
            String pdOld = request.getParameter("pdOld");
            String codePersonne = request.getParameter("codePersonne");

            PeriodeDeclaration periodeDeclaration = AssujettissementBusiness.getListPeriodeDeclarationByPersonneAndListBiensV3(
                    codePersonne, codeNewBien,
                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                    Integer.valueOf(propertiesConfig.getProperty("VALUE_CURRENT_EXERCICE")));

            if (periodeDeclaration != null) {

                String pdNew = periodeDeclaration.getId() + "";

                BigDecimal amountPrincipal_BD = new BigDecimal("0");
                float amountPrincipal = 0;

                Palier palier = null;

                switch (periodeDeclaration.getAssujetissement().getBien().getFkTarif()) {

                    case "0000071020": // 1 - 10 CV
                    case "0000071120": // 11 - 15 CV
                    case "0000071220": // +15 CV

                        palier = DeclarationBusiness.getTauxPalierVignette(
                                propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                periodeDeclaration.getAssujetissement().getBien().getFkTarif(),
                                periodeDeclaration.getAssujetissement().getPersonne().getFormeJuridique().getCode());

                        break;

                    default:

                        palier = DeclarationBusiness.getTauxPalierVignetteV2(
                                propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                periodeDeclaration.getAssujetissement().getBien().getFkTarif());

                        break;

                }

                if (palier.getMultiplierValeurBase() == 1) {

                    ComplementBienAutomobile complementBienAutomobile = TaxationBusiness.getComplementBienAutomobileByBien(
                            periodeDeclaration.getAssujetissement().getBien().getId());

                    amountPrincipal = (Float.valueOf(complementBienAutomobile.getPuissanceFiscale()) * palier.getTaux().floatValue());
                    String amountPrincipalStr = String.valueOf(amountPrincipal);
                    amountPrincipal_BD = BigDecimal.valueOf(Double.valueOf(amountPrincipalStr));

                } else {
                    amountPrincipal = palier.getTaux().floatValue();
                    amountPrincipal_BD = palier.getTaux();
                }

                if (AssujettissementBusiness.changeBienAutomobileTaxation(
                        pdOld, pdNew, Integer.valueOf(noteTaxation), codeOldBien,
                        codeNewBien, amountPrincipal_BD)) {

                    dataReturn = "1";

                } else {

                    dataReturn = "0";
                }

            } else {

                dataReturn = "2";
            }

        } catch (Exception e) {
            dataReturn = "-1";
        }

        return dataReturn;
    }

    public String taxationBatchVignette(HttpServletRequest request) {

        String dataReturn = "";

        try {

            String codePersonne = request.getParameter("codePersonne");
            String selectBanque = request.getParameter("selectBanque");
            String selectCompteBancaire = request.getParameter("selectCompteBancaire");
            String listBiens = request.getParameter("listBiens");
            String year = request.getParameter("codePeriode");
            String devise = request.getParameter("devise");
            String userId = request.getParameter("userId");
            String codeAB = request.getParameter("codeAB");
            String requerant = request.getParameter("requerant");
            String codeSite = request.getParameter("codeSite");

            JSONArray jsonBienTaxation = new JSONArray(listBiens);

            String codeBienComposite = "";

            for (int i = 0; i < jsonBienTaxation.length(); i++) {

                JSONObject jsonObject = jsonBienTaxation.getJSONObject(i);

                String codeBien = jsonObject.getString("codeBien");

                if (codeBienComposite.isEmpty()) {

                    codeBienComposite = "'" + codeBien + "'";
                } else {
                    codeBienComposite += codeBienComposite = ",'" + codeBien + "'";
                }
            }

            List<PeriodeDeclaration> listPeriodeDeclarations = AssujettissementBusiness.getListPeriodeDeclarationByPersonneAndListBiensV2(
                    codePersonne, codeBienComposite,
                    propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                    Integer.valueOf(year));

            List<DeclarationMock> declarationListMock = new ArrayList<>();
            List<TaxationBienAutomobile> listTaxationBienAutomobile = new ArrayList<>();

            String newIDGenerate = AssujettissementBusiness.getNewid();
            String exerciceCurrentName = "";
            String exerciceCompositeName = "";

            BigDecimal amountPrincipGlobal = new BigDecimal("0");
            float amountPrincipal = 0;

            for (PeriodeDeclaration pd : listPeriodeDeclarations) {

                DeclarationMock declarationMock = new DeclarationMock();

                BigDecimal amountPrincipal_BD = new BigDecimal("0");

                Palier palier = null;

                switch (pd.getAssujetissement().getBien().getFkTarif()) {

                    case "0000071020": // 1 - 10 CV
                    case "0000071120": // 11 - 15 CV
                    case "0000071220": // +15 CV

                        palier = DeclarationBusiness.getTauxPalierVignette(
                                propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                pd.getAssujetissement().getBien().getFkTarif(),
                                pd.getAssujetissement().getPersonne().getFormeJuridique().getCode());

                        break;

                    default:

                        palier = DeclarationBusiness.getTauxPalierVignetteV2(
                                propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                                pd.getAssujetissement().getBien().getFkTarif());

                        break;

                }

                if (pd.getAssujetissement().getBien().getTypeBien().getCode().equals(propertiesConfig.getProperty("CODE_TYPE_BIEN_AMBULANCE"))) {

                    palier = DeclarationBusiness.getTauxPalierVignetteV3(
                            propertiesConfig.getProperty("CODE_IMPOT_VIGNETTE"),
                            pd.getAssujetissement().getBien().getFkTarif(),
                            pd.getAssujetissement().getPersonne().getFormeJuridique().getCode(),
                            propertiesConfig.getProperty("CODE_TYPE_BIEN_AMBULANCE"));
                }

                amountPrincipal = 0;

                if (palier.getMultiplierValeurBase() == 1) {

                    ComplementBienAutomobile complementBienAutomobile = TaxationBusiness.getComplementBienAutomobileByBien(
                            pd.getAssujetissement().getBien().getId());

                    amountPrincipal = (Float.valueOf(complementBienAutomobile.getPuissanceFiscale()) * palier.getTaux().floatValue());
                    String amountPrincipalStr = String.valueOf(amountPrincipal);
                    amountPrincipal_BD = BigDecimal.valueOf(Double.valueOf(amountPrincipalStr));

                } else {
                    amountPrincipal = palier.getTaux().floatValue();
                    amountPrincipal_BD = palier.getTaux();
                }

                if (devise.equals("CDF")) {

                    Taux taux = RecouvrementBusiness.getTauxByDevise("USD");

                    if (taux != null) {

                        float amountPrincipal2 = (amountPrincipal * taux.getTaux().floatValue());

                        amountPrincipal = amountPrincipal2;

                        String amountPrincipalStr = String.valueOf(amountPrincipal);
                        amountPrincipal_BD = BigDecimal.valueOf(Double.valueOf(amountPrincipalStr));

                        amountPrincipal_BD.add(amountPrincipal_BD.multiply(taux.getTaux()));
                    }

                }

                amountPrincipGlobal = amountPrincipGlobal.add(amountPrincipal_BD);

                //Insérer les biens dans la table TaxationBienAutomobile
                TaxationBienAutomobile taxationBienAutomobile = new TaxationBienAutomobile();

                taxationBienAutomobile.setPrincipalDu(amountPrincipal_BD);
                taxationBienAutomobile.setPenaliteDu(BigDecimal.ZERO);
                taxationBienAutomobile.setFkBien(pd.getAssujetissement().getBien().getId());
                taxationBienAutomobile.setExercice(year);
                listTaxationBienAutomobile.add(taxationBienAutomobile);

                exerciceCurrentName = Tools.getPeriodeIntitule(pd.getDebut(),
                        pd.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode());

                if (exerciceCompositeName.isEmpty()) {

                    exerciceCompositeName = exerciceCurrentName.concat(GeneralConst.SPACE);

                } else {

                    if (!exerciceCompositeName.trim().contains(year.trim())) {
                        exerciceCompositeName += exerciceCompositeName = exerciceCurrentName.concat(GeneralConst.SPACE);
                    }

                }

                declarationMock.setAmountPrincipal(amountPrincipal_BD);
                declarationMock.setDevise(devise);
                declarationMock.setRequerant(requerant);
                declarationMock.setAssujetti(codePersonne);
                declarationMock.setCodeDeclaration(null);
                declarationMock.setCodeArticleBudgetaire(codeAB);
                declarationMock.setCodeAgentCreate(userId);
                declarationMock.setPeriode(pd.getId() + "");

                Date dateCreateDeclaration = new Date();

                declarationMock.setDateCreate(dateCreateDeclaration);
                declarationMock.setEstPenalise(0);

                declarationMock.setBanque(selectBanque);
                declarationMock.setCompteBancaire(selectCompteBancaire);

                Date dateEcheance = Tools.getEcheanceDate(dateCreateDeclaration, Integer.valueOf("3"));

                declarationMock.setDateEcheancePaiement(Tools.getEcheanceDate(dateEcheance, Integer.valueOf("3"), false));

                declarationListMock.add(declarationMock);
            }

            boolean result = AssujettissementBusiness.taxationBatchVignette(
                    declarationListMock, listTaxationBienAutomobile, codeSite, newIDGenerate);

            if (result) {

                for (PeriodeDeclaration pd : listPeriodeDeclarations) {
                    createPeriodesDeclarationsV2(pd.getId());
                }

                PrintDocument printDocument = new PrintDocument();

                RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getFirstRetraitDeclarationVinetteByNewID(newIDGenerate);

                String noteTaxationPrincipale = printDocument.createNoteTaxationVignette(retraitDeclaration);

                /*String noteTaxationPrincipale = printDocument.createNoteTaxationDeclaration(
                 retraitDeclaration, userId,
                 amountPrincipGlobal,
                 exerciceCompositeName);*/
                dataReturn = noteTaxationPrincipale;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }
}
