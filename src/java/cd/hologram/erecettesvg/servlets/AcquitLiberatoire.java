/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.*;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.pojo.*;
import cd.hologram.erecettesvg.util.*;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author emmanuel.tsasa
 */
@WebServlet(name = "AcquitLiberatoire", urlPatterns = {"/acquitLiberatoire_servlet"})
public class AcquitLiberatoire extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    Date datePeriode;
    List<Periodicite> listPeriodicite;
    String contenuXhtml;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case AcquitLiberatoireConst.Operation.LOAD_NOTES_CALCUL:
                result = loadNotesCalcul(request);
                break;
            case AcquitLiberatoireConst.Operation.PRINT_ACQUIT_LIBERATOIRE:
                result = imprimerAcquitLiberatoire(request);
                break;
            case AcquitLiberatoireConst.Operation.LOAD_ACQUITS_LIBERATOIRES:
                result = searchAcquitLiberatoire(request);
                break;
            case AcquitLiberatoireConst.Operation.RE_PRINT_ACQUIT_LIBERATOIRE:
                result = reImprimerAcquitLiberatoire(request);
                break;
        }
        out.print(result);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String loadNotesCalcul(HttpServletRequest request) {

        List<JSONObject> jsonNotescalcul = new ArrayList<>();
        String dataReturn;
        boolean isTax;
        RetraitDeclaration declaration = null;

        try {

            listPeriodicite = AcquitLiberatoireBusiness.getListPeriodicites();

            String advanced = request.getParameter(AcquitLiberatoireConst.ParamName.ADVANCED);
            String libelle = request.getParameter(AcquitLiberatoireConst.ParamName.LIBELLE);
            String typeSearch = request.getParameter(AcquitLiberatoireConst.ParamName.TYPE_SEARCH);
            String allSite = request.getParameter(AcquitLiberatoireConst.ParamName.ALL_SITE);
            String allService = request.getParameter(AcquitLiberatoireConst.ParamName.ALL_SERVICE);
            String idUser = request.getParameter(AcquitLiberatoireConst.ParamName.ID_USER);
            String codeService = request.getParameter(AcquitLiberatoireConst.ParamName.CODE_SERVICE);
            String codeSite = request.getParameter(AcquitLiberatoireConst.ParamName.CODE_SITE);
            String dateDebut = request.getParameter(AcquitLiberatoireConst.ParamName.DATE_DEBUT);
            String dateFin = request.getParameter(AcquitLiberatoireConst.ParamName.DATE_FIN);

            List<NoteCalcul> noteCalculs;

            if (advanced.equals(GeneralConst.Number.ZERO)) {

                noteCalculs = AcquitLiberatoireBusiness.getListNoteCalculForEditionCpi(
                        libelle, typeSearch.equals(GeneralConst.Number.ZERO),
                        allSite.equals(GeneralConst.Number.ONE),
                        allService.equals(GeneralConst.Number.ONE),
                        idUser, codeService, codeSite);

            } else {

                noteCalculs = AcquitLiberatoireBusiness.getListAdvancedNoteCalculForEditionCpi(
                        codeSite, codeService, ConvertDate.getValidFormatDate(dateDebut),
                        ConvertDate.getValidFormatDate(dateFin));

            }

            if (!noteCalculs.isEmpty()) {

                for (NoteCalcul noteCalcul : noteCalculs) {

                    NotePerception notePerception = AcquitLiberatoireBusiness.getNotePerceptionByNoteCalcul(noteCalcul.getNumero());

                    if (noteCalcul.getDetailsNcList().get(0).getArticleBudgetaire().getAssujetissable()) {

                        declaration = AssujettissementBusiness.getRetraitDeclarationByCode(notePerception.getNumero());

                        if (declaration != null) {
                            if (declaration.getRetraitDeclarationMere() != null) {
                                continue;
                            }
                        }
                    }

                    if (notePerception.getEtat().intValue() == GeneralConst.Numeric.THREE) {
                        continue;
                    }

                    isTax = declaration == null;

                    JSONObject jsonNC = new JSONObject();

                    BigDecimal montantDu;
                    BigDecimal montantPaye;
                    BigDecimal montantPenalite;
                    BigDecimal montantPenalitePaye;

                    String serviceCode = (noteCalcul.getService() == null)
                            ? GeneralConst.EMPTY_STRING
                            : noteCalcul.getService();

                    String personneCode = (noteCalcul.getPersonne() == null)
                            ? GeneralConst.EMPTY_STRING
                            : noteCalcul.getPersonne();

                    String codeTarif = noteCalcul.getDetailsNcList() == null
                            ? GeneralConst.EMPTY_STRING
                            : noteCalcul.getDetailsNcList().get(0).getTarif();

                    Service service = TaxationBusiness.getServiceByCode(serviceCode);
                    ArticleBudgetaire articleBudgetaire = AcquitLiberatoireBusiness.getArticleBudgetaireByNc(noteCalcul.getNumero());
                    Tarif tarif = TaxationBusiness.getTarifByCode(codeTarif);
                    Personne personne = IdentificationBusiness.getPersonneByCode(personneCode);

                    montantDu = notePerception.getNetAPayer();
                    montantPaye = notePerception.getPayer();

                    boolean payLate = false;

                    Date echeanceTitre;

                    if (isTax) {

                        echeanceTitre = ConvertDate.formatDateV2(notePerception.getDateEcheancePaiement());

                        List<NotePerception> npsPenelaites = NotePerceptionBusiness.getListNpFilleByMere(notePerception.getNumero());

                        BigDecimal sumPenalite = BigDecimal.ZERO;
                        BigDecimal sumPenalitePaye = BigDecimal.ZERO;

                        for (NotePerception paneliteNp : npsPenelaites) {

                            if (paneliteNp.getEtat().intValue() == GeneralConst.Numeric.THREE) {

                                sumPenalite = sumPenalite.add(paneliteNp.getNetAPayer());
                                sumPenalitePaye = sumPenalitePaye.add(paneliteNp.getPayer());
                            }
                        }

                        montantPenalite = sumPenalite;
                        montantPenalitePaye = sumPenalitePaye;

                        if (npsPenelaites.isEmpty()) {

                            Journal journal = PaiementBusiness.getJournalByDocument(notePerception.getNumero().trim(), true);

                            if (journal != null) {

                                if (Compare.before(echeanceTitre, journal.getDateCreat())) {
                                    payLate = true;
                                }
                            }

                        }

                    } else {

                        List<RetraitDeclaration> declarationPenalites = AssujettissementBusiness.getListRetraitDeclarationByCodeMere(declaration.getId());

                        echeanceTitre = declaration.getDateEcheancePaiement();

                        BigDecimal sumPenalite = BigDecimal.ZERO;
                        BigDecimal sumPenalitePaye = BigDecimal.ZERO;

                        BigDecimal zero = BigDecimal.ZERO;

                        for (RetraitDeclaration declarationPenalite : declarationPenalites) {

                            sumPenalite = sumPenalite.add(declarationPenalite.getMontant());
                            NotePerception npPenelaite = NotePerceptionBusiness.getNotePerceptionByCode(declarationPenalite.getCodeDeclaration());

                            if (npPenelaite != null) {
                                sumPenalitePaye = sumPenalitePaye.add(npPenelaite.getPayer());
                            } else {
                                sumPenalitePaye = sumPenalitePaye.add(zero);
                            }

                        }

                        montantPenalite = sumPenalite;
                        montantPenalitePaye = sumPenalitePaye;

                        if (declarationPenalites.isEmpty()) {

                            Journal journal = PaiementBusiness.getJournalByDocument(notePerception.getNumero().trim(), true);

                            if (journal != null) {

                                if (Compare.before(echeanceTitre, journal.getDateCreat())) {
                                    payLate = true;
                                }
                            }
                        }

                    }

                    System.out.println("payLate : " + payLate);

                    Adresse adresse = IdentificationBusiness.getDefaultAdressByPerson(noteCalcul.getPersonne());

                    jsonNC.put(AcquitLiberatoireConst.ParamName.SERVICE_ASSIETTE, service == null
                            ? GeneralConst.EMPTY_STRING
                            : service.getIntitule());

                    jsonNC.put(AcquitLiberatoireConst.ParamName.DATE, noteCalcul.getDateCreat() == null
                            ? GeneralConst.EMPTY_STRING
                            : ConvertDate.formatDateToStringOfFormat(noteCalcul.getDateCreat(), GeneralConst.Format.FORMAT_DATE));

                    jsonNC.put("codeArticleBudgetaire", articleBudgetaire.getCode());

                    jsonNC.put(AcquitLiberatoireConst.ParamName.ARTICLE_BUDGETAIRE, articleBudgetaire == null
                            ? GeneralConst.EMPTY_STRING
                            : articleBudgetaire.getIntitule()
                            .concat(tarif == null
                                            ? GeneralConst.EMPTY_STRING
                                            : GeneralConst.TWO_POINTS.concat(tarif.getIntitule())));

                    jsonNC.put(AcquitLiberatoireConst.ParamName.PERIODE, articleBudgetaire == null
                            ? GeneralConst.EMPTY_STRING
                            : Tools.getExerciceFiscal(notePerception.getNoteCalcul()));

                    jsonNC.put(AcquitLiberatoireConst.ParamName.NOTE_PERCEPTION, notePerception == null
                            ? GeneralConst.EMPTY_STRING
                            : notePerception.getNumero());

                    jsonNC.put(AcquitLiberatoireConst.ParamName.REQUERANT, personne == null
                            ? GeneralConst.EMPTY_STRING
                            : personne.getNom() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getNom()
                                    .concat(GeneralConst.SPACE)
                                    .concat(personne.getPostnom() == null
                                                    ? GeneralConst.EMPTY_STRING
                                                    : personne.getPostnom())
                                    .concat(GeneralConst.SPACE)
                                    .concat(personne.getPrenoms() == null
                                                    ? GeneralConst.EMPTY_STRING
                                                    : personne.getPrenoms()));

                    jsonNC.put(AcquitLiberatoireConst.ParamName.FORME_JURIDIQUE, personne == null
                            ? GeneralConst.EMPTY_STRING
                            : personne.getFormeJuridique().getIntitule());

                    jsonNC.put(AcquitLiberatoireConst.ParamName.MONTANT_DU, montantDu);

                    jsonNC.put(AcquitLiberatoireConst.ParamName.PAYE, montantPaye);

                    jsonNC.put(AcquitLiberatoireConst.ParamName.MONTANT_DU_PENALITE, montantPenalite);

                    jsonNC.put(AcquitLiberatoireConst.ParamName.MONTANT_PAYE_PENALITE, montantPenalitePaye);

                    jsonNC.put(AcquitLiberatoireConst.ParamName.DEVISE, getDeviseNP(notePerception));

                    jsonNC.put(AcquitLiberatoireConst.ParamName.DEFAULT_ADRESS, adresse.toString());

                    jsonNC.put(AcquitLiberatoireConst.ParamName.RECEPTIONNISTE, notePerception.getReceptionniste() == null
                            ? GeneralConst.EMPTY_STRING
                            : notePerception.getReceptionniste().toString());

                    jsonNC.put(AcquitLiberatoireConst.ParamName.NOTE_CALCUL, noteCalcul.getNumero());

                    jsonNC.put(AcquitLiberatoireConst.ParamName.PAYLATE, payLate);

                    jsonNotescalcul.add(jsonNC);
                }

                dataReturn = jsonNotescalcul.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getPeriodeExerciceFiscal(Date dateP, String codePeriode) {

        int dureePeriode = 0;
        String sExercice = GeneralConst.EMPTY_STRING,
                partYear = GeneralConst.EMPTY_STRING,
                datePeriodeToString,
                partMonth;

        for (Periodicite p : listPeriodicite) {
            if (p.getCode().equals(codePeriode.trim())) {
                dureePeriode = p.getNbrJour();
            }
        }

        if (dateP != null) {

            datePeriodeToString = ConvertDate.formatDateToStringOfFormat(dateP, GeneralConst.Format.FORMAT_DATE);

            if (dureePeriode == 365) {
                sExercice = datePeriodeToString.substring(6);
            } else {
                partMonth = Tools.getMoisByCode(datePeriodeToString.substring(3, 2));
                partYear = datePeriodeToString.substring(6);
                sExercice = partMonth + GeneralConst.DASH_SEPARATOR + partYear;
            }
        }

        if (sExercice == null || sExercice.isEmpty()) {
            sExercice = TaxationBusiness.getCurrentExerciceFiscal();
        }

        return sExercice;
    }

    public String searchAcquitLiberatoire(HttpServletRequest request) {

        List<JSONObject> jsonAcquitsLiberatoires = new ArrayList<>();

        try {

            listPeriodicite = AcquitLiberatoireBusiness.getListPeriodicites();

            String advanced = request.getParameter(AcquitLiberatoireConst.ParamName.ADVANCED);
            String libelle = request.getParameter(AcquitLiberatoireConst.ParamName.LIBELLE);
            String typeSearch = request.getParameter(AcquitLiberatoireConst.ParamName.TYPE_SEARCH);
            String allSite = request.getParameter(AcquitLiberatoireConst.ParamName.ALL_SITE);
            String allService = request.getParameter(AcquitLiberatoireConst.ParamName.ALL_SERVICE);
            String idUser = request.getParameter(AcquitLiberatoireConst.ParamName.ID_USER);
            String codeService = request.getParameter(AcquitLiberatoireConst.ParamName.CODE_SERVICE);
            String codeSite = request.getParameter(AcquitLiberatoireConst.ParamName.CODE_SITE);
            String dateDebut = request.getParameter(AcquitLiberatoireConst.ParamName.DATE_DEBUT);
            String dateFin = request.getParameter(AcquitLiberatoireConst.ParamName.DATE_FIN);

            List<cd.hologram.erecettesvg.models.AcquitLiberatoire> listAcquitLiberatoires;

            if (advanced.equals(GeneralConst.Number.ZERO)) {

                listAcquitLiberatoires = AcquitLiberatoireBusiness.getListAcquitLiberatoiresPrint(
                        libelle.trim(),
                        Integer.parseInt(typeSearch),
                        allSite.equals(GeneralConst.Number.ONE),
                        allService.equals(GeneralConst.Number.ONE),
                        codeSite,
                        idUser,
                        codeService);

            } else {

                listAcquitLiberatoires = AcquitLiberatoireBusiness.getListAdvancedAcquitLiberatoiresPrint(
                        codeSite,
                        codeService,
                        ConvertDate.getValidFormatDate(dateDebut),
                        ConvertDate.getValidFormatDate(dateFin));
            }

            if (listAcquitLiberatoires != null && !listAcquitLiberatoires.isEmpty()) {

                for (cd.hologram.erecettesvg.models.AcquitLiberatoire cpi : listAcquitLiberatoires) {

                    JSONObject jsonAcquitLiberatoire = new JSONObject();

                    Personne personne = new Personne();
                    ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();
                    EtatSoldePaiement etatSoldePaiement = AcquitLiberatoireBusiness.getEtatSoldePaiementByNc(
                            cpi.getNotePerception().getNoteCalcul().getNumero().trim());

                    jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.DATE, cpi.getDateCreat() == null
                            ? GeneralConst.EMPTY_STRING : ConvertDate.getValidFormatDatePrint(cpi.getDateCreat()));

                    jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.REFERENCE, cpi.getCode() == null
                            ? GeneralConst.EMPTY_STRING : cpi.getCode());

                    articleBudgetaire = TaxationBusiness.getArticleBudgetaireByNC(cpi.getNotePerception().getNoteCalcul().getNumero().trim());

                    if (articleBudgetaire != null) {

                        jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.PERIODE, articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : Tools.getExerciceFiscal(cpi.getNotePerception().getNoteCalcul()));

                        Tarif tarif = TaxationBusiness.getTarifByCode(cpi.getNotePerception().getNoteCalcul().getDetailsNcList().get(0).getTarif().trim());

                        jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.ARTICLE_BUDGETAIRE, articleBudgetaire.getIntitule()
                                .concat(tarif == null ? GeneralConst.EMPTY_STRING : GeneralConst.TWO_POINTS.concat(tarif.getIntitule())));

                        if (articleBudgetaire.getCode().equals(propertiesConfig.getProperty("CODE_ARTICLE_BUDGETAIRE_VIGNETTE"))) {
                            jsonAcquitLiberatoire.put("vignette", cpi.getObservation().toUpperCase());
                        } else {
                            jsonAcquitLiberatoire.put("vignette", GeneralConst.EMPTY_STRING);
                        }

                        jsonAcquitLiberatoire.put("codeArticleBudgetaire", articleBudgetaire.getCode());

                    } else {
                        jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.PERIODE, GeneralConst.EMPTY_STRING);
                        jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.ARTICLE_BUDGETAIRE, GeneralConst.EMPTY_STRING);
                    }

                    personne = IdentificationBusiness.getPersonneByCode(cpi.getNotePerception().getNoteCalcul().getPersonne().trim());

                    jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.REQUERANT, personne == null
                            ? GeneralConst.EMPTY_STRING
                            : personne.getNom() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getNom()
                                    .concat(GeneralConst.SPACE)
                                    .concat(personne.getPostnom() == null
                                                    ? GeneralConst.EMPTY_STRING
                                                    : personne.getPostnom())
                                    .concat(GeneralConst.SPACE)
                                    .concat(personne.getPrenoms() == null
                                                    ? GeneralConst.EMPTY_STRING
                                                    : personne.getPrenoms()));

                    jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.FORME_JURIDIQUE, personne == null
                            ? GeneralConst.EMPTY_STRING
                            : personne.getFormeJuridique().getIntitule());

                    jsonAcquitLiberatoire.put(IdentificationConst.ParamName.USER_NAME,
                            personne == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getLoginWeb() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getLoginWeb().getUsername());

                    jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.NOTE_PERCEPTION, cpi.getNotePerception() == null
                            ? GeneralConst.EMPTY_STRING
                            : cpi.getNotePerception().getNumero());

                    jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.PAYE, etatSoldePaiement == null
                            ? GeneralConst.EMPTY_STRING
                            : Math.abs(etatSoldePaiement.getCredit()));

                    jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.DEVISE, cpi.getNotePerception() == null
                            ? GeneralConst.EMPTY_STRING
                            : getDeviseNP(cpi.getNotePerception()));

                    jsonAcquitLiberatoire.put(AcquitLiberatoireConst.ParamName.IS_PRINT, cpi.getEtatImpression()
                            ? GeneralConst.Number.ONE
                            : GeneralConst.Number.ZERO);

                    jsonAcquitsLiberatoires.add(jsonAcquitLiberatoire);
                }
            }

        } catch (NumberFormatException | JSONException e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return jsonAcquitsLiberatoires.toString();
    }

    public String imprimerAcquitLiberatoire(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;

        try {

            String numeroNP = request.getParameter(AcquitLiberatoireConst.ParamName.NOTE_PERCEPTION);
            String numeroPapier = request.getParameter(AcquitLiberatoireConst.ParamName.NUMERO_PAPIER);
            String numeroTimbre = request.getParameter(AcquitLiberatoireConst.ParamName.NUMERO_TIMBRE);
            String idUser = request.getParameter(AcquitLiberatoireConst.ParamName.ID_USER);
            String codeSite = request.getParameter(AcquitLiberatoireConst.ParamName.CODE_SITE);
            String nameUser = request.getParameter(AcquitLiberatoireConst.ParamName.NAME_USER);
            String solde = request.getParameter(AcquitLiberatoireConst.ParamName.RESTE_A_PAYER);
            String credit = request.getParameter(AcquitLiberatoireConst.ParamName.PAYE);
            String inputNumeroVignette = request.getParameter("inputNumeroVignette");

            NotePerception notePerception = AcquitLiberatoireBusiness.getNotePerceptionByNumero(
                    numeroNP == null ? GeneralConst.EMPTY_STRING : numeroNP);

            double soldeCpiToDouble = Double.valueOf(solde);
            double creditCpiToDouble = Double.valueOf(credit);

            if (soldeCpiToDouble > 0) {

                result = AcquitLiberatoireConst.ResultCode.PAIEMENT_PARTIEL;

            } else {

                if (notePerception != null) {

                    if (isApurateByComptable(notePerception)) {

                        result = saveCpi(numeroPapier,
                                numeroTimbre,
                                notePerception,
                                idUser,
                                codeSite,
                                nameUser,
                                numeroPapier,
                                numeroTimbre,
                                creditCpiToDouble,
                                inputNumeroVignette);
                    } else {
                        result = AcquitLiberatoireConst.ResultCode.NON_APURE_COMPTABLE;
                    }
                }
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public boolean isApurateByComptable(NotePerception notePerception) {

        boolean isPrintable = false;

        try {

            Journal journal = PaiementBusiness.getJournalByDocument(notePerception.getNumero().trim(), true);

            isPrintable = journal != null;

            RetraitDeclaration declaration = AssujettissementBusiness.getRetraitDeclarationByCode(notePerception.getNumero());

            boolean isTax = declaration == null;

            if (isTax && isPrintable) {

                List<NotePerception> npsPenelaites = NotePerceptionBusiness.getListNpFilleByMere(notePerception.getNumero());

                for (NotePerception npsPenelaite : npsPenelaites) {

                    journal = PaiementBusiness.getJournalByDocument(npsPenelaite.getNumero().trim(), true);

                    isPrintable = journal != null;

                }

            } else {

                if (isPrintable && declaration != null) {

                    List<RetraitDeclaration> declarationPenalites = AssujettissementBusiness.getListRetraitDeclarationByCodeMere(declaration.getId());

                    for (RetraitDeclaration declarationPenalite : declarationPenalites) {

                        journal = PaiementBusiness.getJournalByDocument(declarationPenalite.getCodeDeclaration(), true);

                        isPrintable = journal != null;
                    }
                }

            }

        } catch (Exception e) {
            CustumException.LogException(e);
        }
        return isPrintable;
    }

    public boolean hasValidFPC(NoteCalcul nc) {
        boolean hasValid = false;
        boolean isTaxe = false;
        try {

            if (nc.getDepotDeclaration() == null && !nc.getEstTaxationOffice()) {
                isTaxe = true;
            }

            FichePriseCharge fichePriseCharge = AcquitLiberatoireBusiness.getFichePriseChargeByNc(nc.getNumero().trim(), isTaxe);

            if (fichePriseCharge != null) {
                hasValid = fichePriseCharge.getEtat().getCode() != 3;
            } else {
                hasValid = true;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
        }
        return hasValid;
    }

    public boolean hasValidAMR(NoteCalcul nc) {

        boolean hasValideAMR = false;
        boolean isOK = false;

        try {

            if (nc.getDepotDeclaration() == null && !nc.getEstTaxationOffice()) {
                isOK = true;
            }

            Amr amr = AcquitLiberatoireBusiness.getAmrByNc(nc.getNumero().trim(), isOK);

            if (amr != null) {
                Journal journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(amr.getNumero().trim());
                if (journal != null) {

                    hasValideAMR = journal.getEtat() != 1;
                } else {
                    hasValideAMR = true;
                }
            } else {
                hasValideAMR = true;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
        }
        return hasValideAMR;
    }

    public String saveCpi(String numeroPapier,
            String numeroTimbre,
            NotePerception notePerception,
            String idUser,
            String codeSite,
            String nameUser,
            String rubPapier,
            String rubTimbre,
            Double montant,
            String inputNumeroVignette) {

        String result;

        cd.hologram.erecettesvg.models.AcquitLiberatoire acquitLiberatoire
                = new cd.hologram.erecettesvg.models.AcquitLiberatoire();

        List<DetailPaiement> listDetailPaiements = new ArrayList<>();

        int partInteger, partVirgule;

        String numberConvert,
                detailPaiementCpi,
                codeCpiGenerate;

        boolean virguleExist;

        try {

            if (numeroPapier == null) {
                numeroPapier = GeneralConst.EMPTY_STRING;
            }

            if (numeroTimbre == null) {
                numeroTimbre = GeneralConst.EMPTY_STRING;
            }

            acquitLiberatoire.setNumeroPapier(numeroPapier);
            acquitLiberatoire.setNumeroTimbre(numeroTimbre);
            acquitLiberatoire.setReceptionniste(notePerception.getReceptionniste());
            acquitLiberatoire.setDateCreat(ConvertDate.formatDateToStringOfFormat(new Date(), GeneralConst.Format.FORMAT_DATE));
            acquitLiberatoire.setNotePerception(notePerception);
            acquitLiberatoire.setAgentCreat(idUser);
            acquitLiberatoire.setSite(codeSite);

            acquitLiberatoire.setObservation(inputNumeroVignette);

            //il reste le cas de fractionnement
            //principal
            Journal journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(notePerception.getNumero());

            DetailPaiement detailPaiementPrincipal = new DetailPaiement();

            detailPaiementPrincipal.setRubrique(propertiesConfig.getProperty("TXT_PRINCIPAL"));
            detailPaiementPrincipal.setNumeroPiece(journal.getNumeroAttestationPaiement() != null ? journal.getNumeroAttestationPaiement().trim() : journal.getBordereau());
            detailPaiementPrincipal.setDatePaiement(ConvertDate.formatDateToStringOfFormat(journal.getDateCreat(), GeneralConst.Format.FORMAT_DATE));
            detailPaiementPrincipal.setReference(journal.getDocumentApure());
            detailPaiementPrincipal.setCompteBancaire(journal.getCompteBancaire().toString().trim());

            partInteger = 0;
            partVirgule = 0;

            numberConvert = String.valueOf(journal.getMontantApurre());
            numberConvert = numberConvert.replace(".", ",");

            if (numberConvert != null && !numberConvert.isEmpty()) {
                if (numberConvert.contains(",")) {
                    String[] credi = numberConvert.split(",");
                    partInteger = Integer.valueOf(String.valueOf(credi[0]));
                    partVirgule = Integer.valueOf(String.valueOf(credi[1]));
                }
            }

            if (partInteger > 0) {
                virguleExist = partVirgule > 0;
            } else {
                virguleExist = false;
            }

            if (virguleExist) {

                detailPaiementPrincipal.setMontantPayer(Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                        BigDecimal.valueOf(Double.valueOf(String.valueOf(journal.getMontantApurre()))))
                        + GeneralConst.SPACE + journal.getDevise().trim());
            } else {

                if (journal.getMontantApurre().floatValue() < 1) {

                    detailPaiementPrincipal.setMontantPayer(Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                            BigDecimal.valueOf(Double.valueOf(String.valueOf(journal.getMontantApurre()))))
                            + GeneralConst.SPACE + journal.getDevise().trim());
                } else {

                    detailPaiementPrincipal.setMontantPayer(Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                            BigDecimal.valueOf(Double.valueOf(String.valueOf(journal.getMontantApurre()))))
                            + GeneralConst.Number.ZERO_FORMAT
                            + GeneralConst.SPACE + journal.getDevise().trim());
                }

            }

            detailPaiementPrincipal.setMontantTotaux(journal.getMontantApurre().floatValue());
            listDetailPaiements.add(detailPaiementPrincipal);

            //penalite
            RetraitDeclaration declaration = null;
            if (notePerception.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getAssujetissable()) {
                declaration = AssujettissementBusiness.getRetraitDeclarationByCode(notePerception.getNumero());
            }

            boolean isTax = declaration == null;

            //List<Journal> journalPenalities = Collections.EMPTY_LIST;
            List<Journal> journalPenalities = new ArrayList<>();

            if (isTax) {

                List<NotePerception> npsPenelaites = NotePerceptionBusiness.getListNpFilleByMere(notePerception.getNumero());

                for (NotePerception npsPenelaite : npsPenelaites) {

                    journal = PaiementBusiness.getJournalByDocument(npsPenelaite.getNumero().trim(), true);

                    if (journal != null) {
                        journalPenalities.add(journal);
                    }

                }

            } else {

                if (declaration != null) {

                    List<RetraitDeclaration> declarationPenalites = AssujettissementBusiness.getListRetraitDeclarationByCodeMere(declaration.getId());

                    for (RetraitDeclaration declarationPenalite : declarationPenalites) {

                        journal = PaiementBusiness.getJournalByDocument(declarationPenalite.getCodeDeclaration(), true);

                        if (journal != null) {
                            journalPenalities.add(journal);
                        }

                    }
                }

            }

            for (Journal journalPenality : journalPenalities) {

                DetailPaiement detailPaiementPenalite = new DetailPaiement();

                detailPaiementPenalite.setRubrique(propertiesConfig.getProperty("TXT_PENALITE"));
                detailPaiementPenalite.setNumeroPiece(journalPenality.getNumeroAttestationPaiement() != null ? journalPenality.getNumeroAttestationPaiement().trim() : journalPenality.getBordereau());
                detailPaiementPenalite.setDatePaiement(ConvertDate.formatDateToStringOfFormat(journalPenality.getDateCreat(), GeneralConst.Format.FORMAT_DATE));
                detailPaiementPenalite.setReference(journalPenality.getDocumentApure());
                detailPaiementPenalite.setCompteBancaire(journalPenality.getCompteBancaire().toString().trim());

                partInteger = 0;
                partVirgule = 0;

                numberConvert = String.valueOf(journalPenality.getMontantApurre());
                numberConvert = numberConvert.replace(".", ",");

                if (numberConvert != null && !numberConvert.isEmpty()) {
                    if (numberConvert.contains(",")) {
                        String[] credi = numberConvert.split(",");
                        partInteger = Integer.valueOf(String.valueOf(credi[0]));
                        partVirgule = Integer.valueOf(String.valueOf(credi[1]));
                    }
                }

                if (partInteger > 0) {
                    virguleExist = partVirgule > 0;
                } else {
                    virguleExist = false;
                }

                if (virguleExist) {

                    detailPaiementPenalite.setMontantPayer(Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                            BigDecimal.valueOf(Double.valueOf(String.valueOf(journalPenality.getMontantApurre()))))
                            + GeneralConst.SPACE + journalPenality.getDevise().trim());
                } else {

                    if (journalPenality.getMontantApurre().floatValue() < 1) {

                        detailPaiementPenalite.setMontantPayer(Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                BigDecimal.valueOf(Double.valueOf(String.valueOf(journalPenality.getMontantApurre()))))
                                + GeneralConst.SPACE + journalPenality.getDevise().trim());

                    } else {
                        detailPaiementPenalite.setMontantPayer(Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                BigDecimal.valueOf(Double.valueOf(String.valueOf(journalPenality.getMontantApurre()))))
                                + GeneralConst.Number.ZERO_FORMAT
                                + GeneralConst.SPACE + journalPenality.getDevise().trim());
                    }

                }

                detailPaiementPenalite.setMontantTotaux(journalPenality.getMontantApurre().floatValue());
                listDetailPaiements.add(detailPaiementPenalite);

            }

            //table list creation
            detailPaiementCpi = Tools.getTableHtmlV(listDetailPaiements);

            codeCpiGenerate = AcquitLiberatoireBusiness.getGenerateAcquitLiberatoire(acquitLiberatoire);

            if (codeCpiGenerate != null && !codeCpiGenerate.isEmpty()) {

                Archive archive = AcquitLiberatoireBusiness.getIdArchiveByReference(codeCpiGenerate.trim());

                if (archive != null) {

                    result = archive.getDocumentString();

                } else {

                    acquitLiberatoire = AcquitLiberatoireBusiness.getAcquitLiberatoireByNumero(codeCpiGenerate);

                    if (acquitLiberatoire != null) {
                        PrintDocument printDocument = new PrintDocument();
                        result = printDocument.createCPI(acquitLiberatoire, nameUser, rubPapier, rubTimbre, montant, detailPaiementCpi);
                    } else {
                        result = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                }
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (NumberFormatException | InvocationTargetException e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String reImprimerAcquitLiberatoire(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;

        try {

            String reference = request.getParameter(AcquitLiberatoireConst.ParamName.REFERENCE);

            if (reference != null) {

                Archive archive = AcquitLiberatoireBusiness.getIdArchiveByReference(reference.trim());

                if (archive != null) {
                    result = archive.getDocumentString();
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String getDeviseNP(NotePerception notePerception) {

        String devise = GeneralConst.EMPTY_STRING;

        if (notePerception.getCompteBancaire() != null && !notePerception.getCompteBancaire().isEmpty()) {
            CompteBancaire compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(notePerception.getCompteBancaire().trim());
            if (compteBancaire != null) {
                devise = compteBancaire.getDevise().getCode().trim();
            }
        }
        return devise;
    }

}
