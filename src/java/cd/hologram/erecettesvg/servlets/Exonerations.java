/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.AssujettissementBusiness;
import cd.hologram.erecettesvg.business.ExonerationBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import cd.hologram.erecettesvg.constants.AssujettissementConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Assujeti;
import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.Exoneration;
import cd.hologram.erecettesvg.models.Palier;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.models.UsageBien;
import cd.hologram.erecettesvg.util.CustumException;
import cd.hologram.erecettesvg.util.Property;
import cd.hologram.erecettesvg.util.Tools;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Administrateur
 */
@WebServlet(name = "Exonerations", urlPatterns = {"/exonerations_servlet"})
public class Exonerations extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG),
            propertiesMail = Property.getProperties(Property.FileData.FR_MAIL);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case "getTauxAssujetti":
                result = getTauxAssujetti(request);
                break;
            case "getListExoneration":
                result = getListExoneration(request);
                break;
            case "saveExoneration":
                result = saveExoneration(request);
                break;
            case "annulerExoneration":
                result = annulerExoneration(request);
                break;
        }
        out.print(result);
    }

    public String saveExoneration(HttpServletRequest request) {

        String objet = request.getParameter("objet");
        try {
            JSONObject object = new JSONObject(objet);
            Exoneration exo = new Exoneration();

            exo.setMontantExonere(new BigDecimal(object.getDouble("montant")));
            exo.setDevise(new Devise(object.getString("devise")));
            exo.setFkArticleBudgetaire(new ArticleBudgetaire(object.getString("fkAB")));
            exo.setFkBien(new Bien(object.getString("fkBien")));
            exo.setFkSite(new Site(object.getString("fkSite")));
            exo.setFkPersonne(new Personne(object.getString("fkPersonne")));
            exo.setAgentCreat(new Agent(object.getInt("idUser")));
            exo.setFkPeriodeDeclaration(new PeriodeDeclaration(object.getInt("fkPeriode")));
            exo.setObservation(object.getString("observation"));
            exo.setArchive("null".equals(object.getString("base64")) ? null : object.getString("base64"));

            if (ExonerationBusiness.saveExoneration(exo)) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String getTauxAssujetti(HttpServletRequest request) {

        BigDecimal taux;
        BigDecimal valeurBase = new BigDecimal(BigInteger.ONE);
        List<JSONObject> listTaux = new ArrayList<>();
        List<JSONObject> listPeriode = new ArrayList<>();
        List<JSONObject> listObjet = new ArrayList<>();
        JSONObject objects = new JSONObject();

        String fkPersonne = request.getParameter("fkPersonne");
        String fkBien = request.getParameter("fkBien");
        String fkSite = request.getParameter("fkSite");
        String fkAgent = request.getParameter("fkAgent");

        try {
            List<Exoneration> exonerations = ExonerationBusiness.getBiensExoneres(fkPersonne, fkBien, fkSite, fkAgent);
            if (exonerations.size() > 0) {
                for (Exoneration exoneration : exonerations) {
                    JSONObject object = new JSONObject();
                    object.put("dateExoneration", Tools.formatDateToString(exoneration.getDateCreation()));
                    object.put("montantExonere", exoneration.getMontantExonere());
                    object.put("idPeriode", exoneration.getFkPeriodeDeclaration().getId());
                    object.put("idBien", exoneration.getFkBien().getId());
                    object.put("devise", exoneration.getDevise().getCode());
                    object.put("id", exoneration.getCode());
                    listPeriode.add(object);
                }
            }

            List<Assujeti> assujettissements = AssujettissementBusiness.getAssujettissementByPersonne(fkPersonne);

            if (assujettissements.size() > 0) {

                for (Assujeti assujeti : assujettissements) {

                    JSONObject jsonAssujetti = new JSONObject();

                    boolean isPalier = assujeti.getArticleBudgetaire().getPalier();
                    Palier palier = AssujettissementBusiness.getPalierForBienImmobilier(
                            assujeti.getArticleBudgetaire().getCode().trim(),
                            assujeti.getBien().getFkTarif(),
                            assujeti.getPersonne().getFormeJuridique().getCode(),
                            assujeti.getBien().getFkQuartier(),
                            assujeti.getValeur().floatValue(), isPalier,
                            assujeti.getBien().getTypeBien().getCode());

                    if (palier.getTypeTaux().equals("F")) {

                        if (assujeti.getValeur().floatValue() == 0) {

                            taux = palier.getMultiplierValeurBase() == 0 ? palier.getTaux()
                                    : palier.getTaux().multiply(valeurBase);

                        } else {

                            taux = palier.getMultiplierValeurBase() == 0 ? palier.getTaux()
                                    : palier.getTaux().multiply(assujeti.getValeur());
                        }

                    } else {
                        taux = assujeti.getValeur().multiply(palier.getTaux()).divide(BigDecimal.valueOf(GeneralConst.Numeric.CENT));
                    }
                    jsonAssujetti.put("fkBien", assujeti.getBien().getId());
                    jsonAssujetti.put("taux", palier.getTaux());
                    jsonAssujetti.put("tauxCumule", taux);
                    jsonAssujetti.put("typeTaux", palier == null ? 0 : palier.getTypeTaux());
                    jsonAssujetti.put(AssujettissementConst.ParamName.DEVISE, palier == null ? "" : palier.getDevise().getCode());
                    listTaux.add(jsonAssujetti);
                }
            }
            objects.put("listPeriode", listPeriode);
            objects.put("listTaux", listTaux);
            listObjet.add(objects);
            return listObjet.toString();

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public String getListExoneration(HttpServletRequest request) {

        List<JSONObject> objects = new ArrayList<>();
        String fkAgent = request.getParameter("fkAgent"),
                fkSite = request.getParameter("fkSite"),
                fkAB = request.getParameter("fkAB"),
                dateDebut = request.getParameter("dateDebut"),
                dateFin = request.getParameter("dateFin"),
                assujetti = request.getParameter("assujetti"),
                typeRecherche = request.getParameter("typeRecherche");

        try {
            List<Exoneration> exonerations = ExonerationBusiness.getListExonation(
                    fkSite,
                    fkAgent,
                    fkAB,
                    dateDebut,
                    dateFin,
                    assujetti,
                    typeRecherche
            );
            if (exonerations.size() > 0) {

                for (Exoneration exoneration : exonerations) {
                    JSONObject object = new JSONObject();
                    object.put("code", exoneration.getCode());
                    object.put("noms", exoneration.getFkPersonne().toString());
                    object.put("intituleBien", exoneration.getFkBien().getIntitule());
                    object.put("typeBien", exoneration.getFkBien().getTypeBien().getIntitule());

                    if (exoneration.getFkBien().getFkCommune() != null && !exoneration.getFkBien().getFkCommune().isEmpty()) {
                        EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(exoneration.getFkBien().getFkCommune());

                        String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");
                        String ville2 = " (Ville : ".concat(ea.getEntiteMere().getIntitule().toUpperCase().concat(")"));

                        object.put("communeCode", ea.getCode());
                        object.put("communeName", ea.getIntitule().toUpperCase().concat(ville));
                        object.put("communeName2", ea.getIntitule().toUpperCase().concat(ville2));

                    } else {
                        object.put("communeCode", GeneralConst.EMPTY_STRING);
                        object.put("communeName", GeneralConst.EMPTY_STRING);
                    }

                    if (exoneration.getFkBien().getFkQuartier() != null && !exoneration.getFkBien().getFkQuartier().isEmpty()) {

                        EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(exoneration.getFkBien().getFkQuartier());

                        object.put("quartierCode", eaQuartier.getCode());
                        object.put("quartierName", eaQuartier.getIntitule().toUpperCase());

                    } else {
                        object.put("quartierCode", GeneralConst.EMPTY_STRING);
                        object.put("quartierName", GeneralConst.EMPTY_STRING);
                    }

                    if (exoneration.getFkBien().getFkTarif() != null && !exoneration.getFkBien().getFkTarif().isEmpty()) {

                        Tarif tarif = TaxationBusiness.getTarifByCode(exoneration.getFkBien().getFkTarif());

                        object.put("tarifCode", tarif.getCode());
                        object.put("tarifName", tarif.getIntitule().toUpperCase());

                    } else {
                        object.put("tarifCode", GeneralConst.EMPTY_STRING);
                        object.put("tarifName", GeneralConst.EMPTY_STRING);
                    }

                    if (exoneration.getFkBien().getFkUsageBien() != null) {

                        UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(exoneration.getFkBien().getFkUsageBien());

                        object.put("usageCode", usageBien.getId());
                        object.put("usageName", usageBien.getIntitule().toUpperCase());
                        object.put("isImmobilier", GeneralConst.Number.ONE);

                    } else {
                        object.put("usageCode", GeneralConst.EMPTY_STRING);
                        object.put("usageName", GeneralConst.EMPTY_STRING);
                        object.put("isImmobilier", GeneralConst.Number.ZERO);

                    }

                    object.put("montant", exoneration.getMontantExonere());
                    object.put("dateExoneration", Tools.formatDateToString(exoneration.getDateCreation()));
                    object.put("articleB", exoneration.getFkArticleBudgetaire().getIntitule());
                    object.put("devise", exoneration.getDevise().getCode());
                    object.put("periode", Tools.getPeriodeIntitule(exoneration.getFkPeriodeDeclaration().getDebut(), exoneration.getFkPeriodeDeclaration().getAssujetissement().getPeriodicite()));
                    object.put("adresseBien", exoneration.getFkBien().getFkAdressePersonne().getAdresse().toString());
                    objects.add(object);
                }
                return objects.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String annulerExoneration(HttpServletRequest request) {

        String code = request.getParameter("code");
        int agentId = Integer.valueOf(request.getParameter("userId"));
        String observation = request.getParameter("observation");
        try {
            if (ExonerationBusiness.annulerExoneration(code, agentId, observation)) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
