/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.GestionBanqueBusiness;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.Taux;
import cd.hologram.erecettesvg.util.*;
import com.google.gson.JsonObject;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.JSONObject;

/**
 *
 * @author emmanuel.tsasa
 */
@WebServlet(name = "GestionBanque", urlPatterns = {"/banque_servlet"})
public class GestionBanque extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case "loadBank":
                result = getBanques(request);
                break;
            case "loadBankv2":
                result = getBanquesV2(request);
                break;
            case "saveBank":
                result = saveBank(request);
                break;
            case "deleteBank":
                result = deleteBank(request);
                break;
            case "loadAccountBank":
                result = getCompteBanques(request);
                break;
            case "saveAccountBank":
            case "deleteAccountBank":
                result = editAccountBank(request);
                break;
            case "loadDevise":
                result = loadDevise();
                break;
            case "getTauxByDevise":
                result = getTauxByDevise(request);
                break;
            case "saveTaux":
                result = saveTauxChange(request);
                break;

        }

        out.print(result);
    }

    public String saveBank(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        Boolean result = false;

        try {

            String bankCode = request.getParameter("bankCode");
            String bankNameLong = request.getParameter("bankNameLong");
            String bankNameShort = request.getParameter("bankNameShort");
            String bankCodeSwift = request.getParameter("bankCodeSwift");
            String userId = request.getParameter("userId");

            result = GestionBanqueBusiness.saveBank(bankCode, bankNameLong, bankNameShort,
                    bankCodeSwift, Integer.valueOf(userId));

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String editAccountBank(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        Boolean result = false;

        try {

            String code = request.getParameter("code");
            String libelle = request.getParameter("libelle");
            String devise = request.getParameter("devise");
            String banque = request.getParameter("banque");
            String userId = request.getParameter("userId");
            String typeCompte = request.getParameter("typeCompte");

            result = GestionBanqueBusiness.editAccountBank(code, libelle, devise,
                    banque, Integer.valueOf(userId), Integer.valueOf(typeCompte));

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String deleteBank(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        Boolean result = false;

        try {

            String bankCode = request.getParameter("bankCode");
            String userId = request.getParameter("userId");

            result = GestionBanqueBusiness.deleteBank(bankCode, Integer.valueOf(userId));

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getBanques(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<cd.hologram.erecettesvg.models.Banque> banques = GestionBanqueBusiness.getBanqueList();

            if (!banques.isEmpty()) {

                List<JsonObject> bankJsonList = new ArrayList<>();
                int i = 0;
                for (cd.hologram.erecettesvg.models.Banque banque : banques) {

                    //i++;
                    //System.out.println("id : " + i);
                    JsonObject bankJson = new JsonObject();

                    bankJson.addProperty("bankCode", banque.getCode());
                    bankJson.addProperty("bankNameLong", banque.getIntitule().toUpperCase());
                    bankJson.addProperty("bankNameShort", banque.getSigle() != null
                            ? banque.getSigle() : GeneralConst.EMPTY_STRING);

                    bankJson.addProperty("bankSwiftCode", banque.getCodeSuift());

                    if (!banque.getCompteBancaireList().isEmpty()) {

                        List<JsonObject> cbJsonList = new ArrayList<>();

                        bankJson.addProperty("accountBankExist", GeneralConst.Number.ONE);

                        for (CompteBancaire cb : banque.getCompteBancaireList()) {

                            JsonObject cbJson = new JsonObject();

                            if (cb.getEtat() == GeneralConst.Numeric.ONE) {

                                cbJson.addProperty("acountBankCode", cb.getCode());
                                cbJson.addProperty("acountBankName", cb.getIntitule());
                                cbJson.addProperty("devise", cb.getDevise().getCode());

                                cbJsonList.add(cbJson);
                            }
                        }

                        bankJson.addProperty("accountBankList", cbJsonList.toString());

                    } else {
                        bankJson.addProperty("accountBankList", GeneralConst.EMPTY_STRING);
                        bankJson.addProperty("accountBankExist", GeneralConst.Number.ZERO);
                    }

                    bankJsonList.add(bankJson);

                }

                dataReturn = bankJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getBanquesV2(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<cd.hologram.erecettesvg.models.Banque> banques = GestionBanqueBusiness.getBanqueList();

            if (!banques.isEmpty()) {

                List<JsonObject> bankJsonList = new ArrayList<>();

                for (cd.hologram.erecettesvg.models.Banque banque : banques) {

                    JsonObject bankJson = new JsonObject();

                    bankJson.addProperty("bankCode", banque.getCode());
                    bankJson.addProperty("bankNameLong", banque.getIntitule().toUpperCase());
                    bankJson.addProperty("bankNameShort", banque.getSigle() != null
                            ? banque.getSigle() : GeneralConst.EMPTY_STRING);

                    bankJsonList.add(bankJson);

                }

                dataReturn = bankJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getCompteBanques(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<CompteBancaire> compteBancaires = GestionBanqueBusiness.getCompteBancaireList();

            if (!compteBancaires.isEmpty()) {

                List<JsonObject> accountBankJsonList = new ArrayList<>();

                for (CompteBancaire cb : compteBancaires) {

                    JsonObject accountBankJson = new JsonObject();

                    accountBankJson.addProperty("accountBankCode", cb.getCode());
                    accountBankJson.addProperty("accountBankLibelle", cb.getIntitule().toUpperCase());
                    accountBankJson.addProperty("accountBankDevise", cb.getDevise().getCode());
                    accountBankJson.addProperty("accountBankBanqueName", cb.getBanque().getIntitule().toUpperCase());
                    accountBankJson.addProperty("accountBankBanqueCode", cb.getBanque().getCode());
                    accountBankJson.addProperty("typeCompteCode", cb.getEstCompteTaxe());

                    switch (cb.getEstCompteTaxe()) {
                        case 1:
                            accountBankJson.addProperty("typeCompteName", "COMPTE BANCAIRE POUR TAXE");
                            break;
                        case 2:
                            accountBankJson.addProperty("typeCompteName", "COMPTE BANCAIRE POUR IMPÔT");
                            break;
                        case 3:
                            accountBankJson.addProperty("typeCompteName", "COMPTE BANCAIRE POUR TAXE & IMPÔT");
                            break;
                    }

                    accountBankJsonList.add(accountBankJson);

                }

                dataReturn = accountBankJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadDevise() {

        List<JSONObject> jsonDeviseList = new ArrayList<>();
        JSONObject jsonDevise = new JSONObject();

        try {

            List<Devise> deviseList = GestionBanqueBusiness.loadDevises();

            if (deviseList.size() > 0) {
                for (Devise dvs : deviseList) {

                    jsonDevise = new JSONObject();

                    jsonDevise.put("intitule",
                            dvs == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dvs.getIntitule());
                    jsonDevise.put("code",
                            dvs == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dvs.getCode());
                    jsonDevise.put("parDefaut",
                            dvs == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dvs.getParDefaut());

                    jsonDeviseList.add(jsonDevise);
                }
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonDeviseList.toString();
    }

    private String getTauxByDevise(HttpServletRequest request) {
        String result;
        String codeDevise = request.getParameter("codeDevise");

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<Taux> tauxList = GestionBanqueBusiness.loadTauxByDevises(codeDevise);

            if (!tauxList.isEmpty()) {
                for (Taux tx : tauxList) {

                    JSONObject object = new JSONObject();

                    object.put("code", tx.getId() == null
                            ? GeneralConst.EMPTY_STRING
                            : tx.getId());

                    object.put("devise", tx.getDevise() == null
                            ? GeneralConst.EMPTY_STRING
                            : tx.getDevise().getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : tx.getDevise().getCode());

                    object.put("deviseIntitule", tx.getDevise() == null
                            ? GeneralConst.EMPTY_STRING
                            : tx.getDevise().getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : tx.getDevise().getIntitule());

                    if (tx.getDeviseDest() != null) {

                        Devise devise = GestionBanqueBusiness.loadDevisesByCode(tx.getDeviseDest());

                        if (devise != null) {
                            object.put("deviseDistinIntitule", devise.getIntitule());
                        } else {
                            object.put("deviseDistinIntitule", GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        object.put("deviseDistinIntitule", GeneralConst.EMPTY_STRING);
                    }

                    object.put("deviseDist", tx.getDeviseDest() == null
                            ? GeneralConst.EMPTY_STRING
                            : tx.getDeviseDest());

                    Date dateCreate = tx.getDateCreat();
                    Date dateUpdate = tx.getDateMaj();

                    if (dateUpdate != null) {
                        object.put("dateCreate", Tools.formatDateToStringV2(dateUpdate));
                    } else {
                        if (dateCreate != null) {
                            object.put("dateCreate", Tools.formatDateToStringV2(dateCreate));
                        } else {
                            object.put("dateCreate", GeneralConst.EMPTY_STRING);
                        }
                    }

                    object.put("taux", tx.getTaux() == null
                            ? GeneralConst.EMPTY_STRING
                            : tx.getTaux());

                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String saveTauxChange(HttpServletRequest request) {

        String codeTaux = request.getParameter("codeTaux");
        String codeDevise = request.getParameter("codeDevise");
        String idUser = request.getParameter(GeneralConst.ID_USER);
        String codeCorresp = request.getParameter("codeCorresp");
        String tauxChange = request.getParameter("tauxChange");

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            Taux taux = new Taux();

            if (!codeTaux.equals(GeneralConst.EMPTY_STRING)) {
                taux.setId(Integer.valueOf(codeTaux));
            } else {
                taux.setId(GeneralConst.Numeric.ZERO);
            }

            taux.setDevise(new Devise(codeDevise));
            taux.setDeviseDest(codeCorresp);
            taux.setAgentCreat(idUser);
            taux.setDateCreat(new Date());
            taux.setTaux(BigDecimal.valueOf(Float.valueOf(tauxChange)));

            boolean result = GestionBanqueBusiness.saveTaux(taux);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
