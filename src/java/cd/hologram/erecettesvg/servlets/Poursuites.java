
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.AcquitLiberatoireBusiness;
import cd.hologram.erecettesvg.business.AssujettissementBusiness;
import cd.hologram.erecettesvg.business.ContentieuxBusiness;
import cd.hologram.erecettesvg.business.DeclarationBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.GestionBanqueBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.NotePerceptionBusiness;
import cd.hologram.erecettesvg.business.PaiementBusiness;
import cd.hologram.erecettesvg.business.PoursuiteBusiness;
import cd.hologram.erecettesvg.business.RecouvrementBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.constants.PoursuiteConst;
import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.Archive;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Atd;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.Bordereau;
import cd.hologram.erecettesvg.models.Commandement;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.Contrainte;
import cd.hologram.erecettesvg.models.DetailFichePriseCharge;
import cd.hologram.erecettesvg.models.DetailsRole;
import cd.hologram.erecettesvg.models.FichePriseCharge;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.Med;
import cd.hologram.erecettesvg.models.NoteCalcul;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.Palier;
import cd.hologram.erecettesvg.models.Penalite;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.RetraitDeclaration;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.pojo.DetailPenaliteMock;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.PrintDocument;
import cd.hologram.erecettesvg.util.Property;
import cd.hologram.erecettesvg.util.Tools;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gauthier.muata
 */
@WebServlet(name = "Poursuites", urlPatterns = {"/poursuites_servlet"})
public class Poursuites extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    List<Med> medList;
    List<Amr> amrList;
    List<Contrainte> contrainteList;
    List<Commandement> commandementList;
    List<Atd> atdList;

    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(PoursuiteConst.ParamName.OPERATION);

        switch (operation) {
            case PoursuiteConst.Operation.LOAD_MISE_EN_DEMEURE:
                result = getMiseEnDemeure(request);
                break;
            case "loadInvitationApayer":
                result = getInvitationApayer(request);
                break;
            case "loadInvitationServicePaimentNP":
                result = getInvitationServicePaiement(request);
                break;
            case "loadInvitationService":
                result = getInvitationService(request);
                break;
            case PoursuiteConst.Operation.LOAD_AMR:
                result = getAvisMiseEnRecouvrement(request);
                break;
            case "loadAmrTaxationOffice":
                result = getAmrTaxationOffice(request);
                break;
            case PoursuiteConst.Operation.PRINT_AMR:
                result = printAmr(request);
                break;
            case PoursuiteConst.Operation.GENERATE_CONTRAINTE:
                result = generateContrainte(request);
                break;
            case PoursuiteConst.Operation.PRINT_DOCUMENT:
                result = printDocument(request);
                break;
            case PoursuiteConst.Operation.LOAD_CONTRAINTE:
                result = getContraintes(request);
                break;
            case PoursuiteConst.Operation.ACTIVATE_COMMANDEMENT:
                result = activateGenerate(request);
                break;
            case PoursuiteConst.Operation.LOAD_COMMANDEMENT:
                result = getCommandements(request);
                break;
            case PoursuiteConst.Operation.GENERATE_ATD:
                result = generateAtd(request);
                break;
            case PoursuiteConst.Operation.LOAD_ATD:
                result = getAtds(request);
                break;
            case PoursuiteConst.Operation.PRINT_ATD:
                result = printAtd(request);
                break;
            case PoursuiteConst.Operation.LOAD_FICHE_PRISE_CHARGE:
                result = getFichePriseCharge(request);
                break;
            case PoursuiteConst.Operation.TRAITER_FICHE_PRISE_CHARGE:
                result = traiterFichePriseCharge(request);
                break;
            case PoursuiteConst.Operation.LOAD_BON_A_PAYER:
                result = getBonAPayer(request);
                break;
            case "saveFpc":
                result = saveFichePriseCharge(request);
                break;
            case "saveAndPrintAmr":
                result = saveAndPrintAmr(request);
                break;
            case "loadAgentHuissier":
                result = loadAgentHuissier(request);
                break;
            case "saveNoteTaxationPenalite":
                result = saveNoteTaxationPenalite(request);
                break;
            case "saveNoteTaxationPrincipalAndPenalite":
                result = saveNoteTaxationPrincipalAndPenalite(request);
                break;
            case "initDataBank":
                result = initDataBank(request);
                break;
        }

        out.print(result);
    }

    public String saveFichePriseCharge(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String principalDu = GeneralConst.EMPTY_STRING;
        String penaliteDu = GeneralConst.EMPTY_STRING;
        String caseFpc = GeneralConst.EMPTY_STRING;
        String deviseFpc = GeneralConst.EMPTY_STRING;
        String reference = GeneralConst.EMPTY_STRING;
        String periodeID = GeneralConst.EMPTY_STRING;
        String isRecidiviste = GeneralConst.EMPTY_STRING;
        String monthLate = GeneralConst.EMPTY_STRING;
        String penalitieList = GeneralConst.EMPTY_STRING;
        String medID = GeneralConst.EMPTY_STRING;

        try {

            principalDu = request.getParameter("principalDu");
            penaliteDu = request.getParameter("penaliteDu");
            caseFpc = request.getParameter("caseFpc");
            deviseFpc = request.getParameter("deviseFpc");
            reference = request.getParameter("reference");
            isRecidiviste = request.getParameter("isRecidiviste");
            monthLate = request.getParameter("monthLate");
            penalitieList = request.getParameter("penalitieList");
            medID = request.getParameter("medID");

            JSONArray jsonArrayPenalies = null;

            if (penalitieList != null) {
                jsonArrayPenalies = new JSONArray(penalitieList);
            }

            List<DetailPenaliteMock> detailPenaliteMocks = new ArrayList<>();

            String assujettiCode = GeneralConst.EMPTY_STRING;
            String articleBudgetaireCode = GeneralConst.EMPTY_STRING;
            String exercice = GeneralConst.EMPTY_STRING;
            String numeroNc = GeneralConst.EMPTY_STRING;

            Med med = RecouvrementBusiness.getMedById(medID);

            if (med != null) {

                assujettiCode = med.getPersonne();
                exercice = med.getExerciceFiscal();

                switch (med.getTypeMed()) {

                    case "DECLARATION":

                        PeriodeDeclaration periodeDeclaration = DeclarationBusiness.getPeriodeDeclarationById(
                                med.getPeriodeDeclaration() + "");

                        if (periodeDeclaration != null) {
                            articleBudgetaireCode = periodeDeclaration.getAssujetissement().getArticleBudgetaire().getCode();
                        }

                        break;

                    case "PAIEMENT":

                        NoteCalcul nc = TaxationBusiness.getNoteCalculByNumero(med.getNoteCalcul());

                        if (nc != null) {
                            numeroNc = nc.getNumero();
                            articleBudgetaireCode = nc.getDetailsNcList().get(0).getArticleBudgetaire().getCode();
                        }

                        break;
                }

            }

            BigDecimal amountPen = new BigDecimal("0");

            for (int i = 0; i < jsonArrayPenalies.length(); i++) {

                JSONObject jsonObjectPenalite = jsonArrayPenalies.getJSONObject(i);

                DetailPenaliteMock detailPenaliteMock = new DetailPenaliteMock();

                detailPenaliteMock.setCodePenalite(jsonObjectPenalite.getString("id"));
                detailPenaliteMock.setTypeTaux(jsonObjectPenalite.getString("type"));
                detailPenaliteMock.setReference(jsonObjectPenalite.getString("referenceNumber"));
                //detailPenaliteMock.setNumberMonth(jsonObjectPenalite.getInt("numberMonth"));
                detailPenaliteMock.setNumberMonth(Integer.valueOf(monthLate));
                detailPenaliteMock.setTaux(BigDecimal.valueOf(Double.valueOf(jsonObjectPenalite.getString("rate"))));
                detailPenaliteMock.setAmountPenalite(BigDecimal.valueOf(Double.valueOf(jsonObjectPenalite.getString("value"))));
                detailPenaliteMock.setAmountPrincipal(BigDecimal.valueOf(Double.valueOf(jsonObjectPenalite.getString("principal"))));

                detailPenaliteMock.setExercice(exercice);

                amountPen = amountPen.add(detailPenaliteMock.getAmountPenalite());

                detailPenaliteMocks.add(detailPenaliteMock);

            }

            FichePriseCharge fpc = new FichePriseCharge();

            fpc.setTotalPrincipaldu(BigDecimal.valueOf(Double.valueOf(principalDu)));
            fpc.setTotalPenalitedu(amountPen);
            fpc.setFkMed(medID);
            fpc.setFkArticleBudgetaire(articleBudgetaireCode);
            fpc.setPersonne(new Personne(assujettiCode));
            fpc.setCasFpc(caseFpc);
            fpc.setDevise(deviseFpc);
            fpc.setNumeroReference(reference);

            String codeFpcReturn = PoursuiteBusiness.createFichePriseCharge(fpc, detailPenaliteMocks, numeroNc);

            if (!codeFpcReturn.isEmpty()) {
                dataReturn = codeFpcReturn.trim();
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveAndPrintAmr(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String numeroMed = GeneralConst.EMPTY_STRING;
        String userId = GeneralConst.EMPTY_STRING;

        try {

            numeroMed = request.getParameter("numeroMed");
            userId = request.getParameter("userId");
            String valueMotif = request.getParameter("valueMotif");
            String valueDetailMotif = request.getParameter("valueDetailMotif");
            String noteTaxation = request.getParameter("noteTaxation");

            Med med = RecouvrementBusiness.getMedById(numeroMed);

            PrintDocument printDocument = new PrintDocument();

            if (med != null) {

                if (PoursuiteBusiness.saveAmrOfInvitationApayer(numeroMed, Integer.valueOf(userId), valueDetailMotif, valueMotif, noteTaxation)) {

                    Amr amr = PoursuiteBusiness.getAmrByMedV2(numeroMed);

                    if (amr != null) {
                        BigDecimal _zero = new BigDecimal("0");
                        
                        RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByCode(noteTaxation);
                        
                        dataReturn = printDocument.createAmrOfInvitationApayer(amr, userId, med, 1, _zero, retraitDeclaration.getNewId());

                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                } else {

                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;

                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadAgentHuissier(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<Agent> listAgent = PoursuiteBusiness.getistAgentHuissier(propertiesConfig.getProperty("CODE_FONCTION_HUISSIER"));

            if (listAgent != null) {

                List<JsonObject> agentJsonList = new ArrayList<>();

                for (Agent agentHuissier : listAgent) {

                    JsonObject agentJson = new JsonObject();

                    agentJson.addProperty("code", agentHuissier.getCode());
                    agentJson.addProperty("nom", agentHuissier.toString().toUpperCase());

                    agentJsonList.add(agentJson);
                }

                dataReturn = agentJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveNoteTaxationPenalite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String numeroMed = request.getParameter("numeroMed");
            String userId = request.getParameter("userId");

            Med med = RecouvrementBusiness.getMedById(numeroMed);

            RetraitDeclaration declarationPrincipal = AssujettissementBusiness.getRetraitDeclarationPrincipalByPeriode(
                    med.getPeriodeDeclaration() + "");

            Agent agent = GeneralBusiness.getAgentByCode(userId);

            String comptePenaliteProvince = "";
            String comptePenaliteDrhkat = "";

            switch (agent.getSite().getDivision().getCode()) {

                case "DIV00000002": //GRANNA

                    if (declarationPrincipal.getDevise().equals("USD")) {
                        comptePenaliteProvince = propertiesConfig.getProperty("IAP_QUOTA_PROVINCE_COMPTE_BANCAIRE_USD_DIRECTION");
                        comptePenaliteDrhkat = propertiesConfig.getProperty("IAP_QUOTA_DRHKAT_COMPTE_BANCAIRE_USD_DIRECTION");
                    } else {
                        comptePenaliteProvince = propertiesConfig.getProperty("IAP_QUOTA_PROVINCE_COMPTE_BANCAIRE_CDF_DIRECTION");
                        comptePenaliteDrhkat = propertiesConfig.getProperty("IAP_QUOTA_DRHKAT_COMPTE_BANCAIRE_CDF_DIRECTION");
                    }
                    break;
                default:
                    if (declarationPrincipal.getDevise().equals("USD")) {
                        comptePenaliteProvince = propertiesConfig.getProperty("IAP_QUOTA_PROVINCE_COMPTE_BANCAIRE_USD_DRL");
                        comptePenaliteDrhkat = propertiesConfig.getProperty("IAP_QUOTA_DRHKAT_COMPTE_BANCAIRE_USD_DRL");
                    } else {
                        comptePenaliteProvince = propertiesConfig.getProperty("IAP_QUOTA_PROVINCE_COMPTE_BANCAIRE_CDF_DRL");
                        comptePenaliteDrhkat = propertiesConfig.getProperty("IAP_QUOTA_DRHKAT_COMPTE_BANCAIRE_CDF_DRL");
                    }
                    break;
            }

            if (PoursuiteBusiness.saveNoteTaxationPenaliteOfInvitationAPayer(med,
                    declarationPrincipal,
                    userId,
                    propertiesConfig.getProperty("VALEUR_BANQUE_PAIEMENT_TITRE_PERCEPTION_PENALITE"),
                    comptePenaliteProvince,
                    comptePenaliteDrhkat)) {

                List<RetraitDeclaration> retraitDeclarationPenalitesList = PoursuiteBusiness.getListRetraitDeclarationPenaliteByPricipal(
                        declarationPrincipal.getId());

                if (!retraitDeclarationPenalitesList.isEmpty()) {

                    String document1 = "";
                    String document2 = "";

                    int id = 0;

                    for (RetraitDeclaration penalite : retraitDeclarationPenalitesList) {

                        PrintDocument printDocument = new PrintDocument();

                        BigDecimal amountPD = new BigDecimal("0");

                        if (document1.isEmpty()) {

                            document1 = printDocument.createNoteTaxationDeclaration(penalite, userId, amountPD, "");
                            id = penalite.getId();

                        } else {

                            if (id != penalite.getId()) {
                                document2 = printDocument.createNoteTaxationDeclaration(penalite, userId, amountPD, "");
                            }
                        }
                    }

                    JsonObject documentJSON = new JsonObject();

                    documentJSON.addProperty("ntPenaliteProvince", document1);
                    documentJSON.addProperty("ntPenaliteDrhKat", document2);

                    dataReturn = documentJSON.toString();

                } else {

                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;

                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveNoteTaxationPrincipalAndPenalite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String numeroMed = request.getParameter("numeroMed");
            String userId = request.getParameter("userId");
            String codeBanque = request.getParameter("codeBanque");

            String codeCompteBancaire = request.getParameter("codeCompteBancaire");
            String codeCompteBancairePenalite = request.getParameter("codeCompteBancairePenalite");
            Med med = RecouvrementBusiness.getMedById(numeroMed);

            RetraitDeclaration declarationPrincipal = AssujettissementBusiness.getRetraitDeclarationPrincipalByPeriode(
                    med.getPeriodeDeclaration() + "");

            CompteBancaire cbPrincipal = GeneralBusiness.getCompteBancaireByCode(codeCompteBancaire);
            CompteBancaire cbPenalite = GeneralBusiness.getCompteBancaireByCode(codeCompteBancairePenalite);

            String devise1 = cbPrincipal == null ? "USD" : cbPrincipal.getDevise().getCode();
            String devise2 = cbPenalite == null ? "USD" : cbPenalite.getDevise().getCode();

            String numDeclaration = AssujettissementBusiness.getNewid();

            if (PoursuiteBusiness.saveNoteTaxationPrincipalAndPenaliteOfInvitationService(med,
                    userId,
                    codeBanque,
                    codeCompteBancaire, codeCompteBancairePenalite, devise1, devise2, numDeclaration)) {

                RetraitDeclaration retraitDeclarationPricipal = AssujettissementBusiness.getRetraitDeclarationPrincipalByPeriode(
                        med.getPeriodeDeclaration() + "");

                String document1 = "";
                String document2 = "";

                if (retraitDeclarationPricipal != null) {

                    PrintDocument printDocument = new PrintDocument();

                    BigDecimal amountPD = new BigDecimal("0");

                    document1 = printDocument.createNoteTaxationDeclaration(retraitDeclarationPricipal, userId, amountPD, "");

                    RetraitDeclaration retraitDeclarationPenalite = AssujettissementBusiness.getRetraitDeclarationByCodeMere(
                            retraitDeclarationPricipal.getId());

                    document2 = printDocument.createNoteTaxationDeclaration(retraitDeclarationPenalite, userId, amountPD, "");

                    JsonObject documentJSON = new JsonObject();

                    documentJSON.addProperty("ntPenalitePrincipal", document1);
                    documentJSON.addProperty("ntPenalitePenalite", document2);

                    dataReturn = documentJSON.toString();

                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String initDataBank(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<cd.hologram.erecettesvg.models.Banque> banqueList = new ArrayList<>();

            banqueList = GestionBanqueBusiness.getBanqueList();

            if (!banqueList.isEmpty()) {

                List<JsonObject> banqueJSONList = new ArrayList<>();

                for (cd.hologram.erecettesvg.models.Banque banque : banqueList) {

                    JsonObject banqueJSON = new JsonObject();

                    banqueJSON.addProperty("codeBanque", banque.getCode());
                    banqueJSON.addProperty("libelleBanque", banque.getIntitule().toUpperCase());

                    if (!banque.getCompteBancaireList().isEmpty()) {

                        List<JsonObject> compteBancaireJSONList = new ArrayList<>();

                        for (CompteBancaire compteBancaire : banque.getCompteBancaireList()) {

                            JsonObject compteBancaireJSON = new JsonObject();

                            compteBancaireJSON.addProperty("codeBanque", banque.getCode());
                            compteBancaireJSON.addProperty("codeCompteBancaire", compteBancaire.getCode());
                            compteBancaireJSON.addProperty("libelleCompteBancaire", compteBancaire.getIntitule().toUpperCase());

                            compteBancaireJSONList.add(compteBancaireJSON);
                        }

                        banqueJSON.addProperty("compteBancaireList", compteBancaireJSONList.toString());

                    } else {
                        banqueJSON.addProperty("compteBancaireList", "");
                    }

                    banqueJSONList.add(banqueJSON);

                }

                dataReturn = banqueJSONList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getBonAPayer(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;
        String codeTypeFpc = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);
            codeTypeFpc = request.getParameter(PoursuiteConst.ParamName.CODE_TYPE);
            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);

            List<BonAPayer> bonAPayerList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    bonAPayerList = PoursuiteBusiness.getListBonAPayerBySimpleSearch(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    bonAPayerList = PoursuiteBusiness.getListBonAPayerByAdvancedSearch(codeService,
                            codeTypeFpc, dateDebut, dateFin);

                    break;
            }

            if (!bonAPayerList.isEmpty()) {

                List<JsonObject> bonAPayerJsonList = new ArrayList<>();

                for (BonAPayer bap : bonAPayerList) {

                    JsonObject jsonObject = new JsonObject();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();

                    jsonObject.addProperty("code", bap.getCode());

                    jsonObject.addProperty("dateCreate",
                            ConvertDate.formatDateToString(bap.getDateCreat()));

                    if (bap.getDateEcheance() != null) {

                        jsonObject.addProperty("dateEcheance",
                                ConvertDate.formatDateToString(bap.getDateEcheance()));

                    } else {
                        jsonObject.addProperty("dateEcheance", GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty("estFractionner", bap.getEstFraction() != null ? bap.getEstFraction() == true ? "1" : "0" : "0");
                    jsonObject.addProperty("enContentieux", bap.getEnContentieux() == true ? "1" : "0");
                    jsonObject.addProperty("numeroAmr", bap.getFkAmr().getNumero());

                    Agent agentReceveur = ContentieuxBusiness.getAgentByCode(bap.getAgentCreat());

                    if (agentReceveur != null) {

                        jsonObject.addProperty("agentReceveur", agentReceveur.toString().toUpperCase());

                    } else {
                        jsonObject.addProperty("agentReceveur", GeneralConst.EMPTY_STRING);
                    }

                    personne = bap.getFkPersonne();

                    if (personne != null) {

                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_CODE, personne.getCode());
                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME, personne.toString().toUpperCase());

                        String typeAssujetti = GeneralConst.EMPTY_STRING;
                        String assujettiName = GeneralConst.EMPTY_STRING;
                        String adresseAssujetti = GeneralConst.EMPTY_STRING;
                        String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                        typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                personne.getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                        assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                personne.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                        if (adresse != null) {

                            adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                            jsonObject.addProperty(PoursuiteConst.ParamName.ADRESSE_NAME, adresse.toString().toUpperCase());

                        } else {
                            jsonObject.addProperty(PoursuiteConst.ParamName.ADRESSE_NAME, GeneralConst.EMPTY_STRING);
                        }

                        assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                assujettiNameComposite);

                        jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());
                    } else {
                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                GeneralConst.EMPTY_STRING);

                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_CODE, GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME, GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(PoursuiteConst.ParamName.ADRESSE_NAME, GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(PoursuiteConst.ParamName.MOTIF_PENALITE, bap.getMotifPenalite());
                    jsonObject.addProperty(PoursuiteConst.ParamName.MONTANT_DU, bap.getMontant());

                    if (bap.getFkCompte() != null) {

                        jsonObject.addProperty(PoursuiteConst.ParamName.COMPTE_BANCAIRE,
                                bap.getFkCompte().getIntitule());

                        jsonObject.addProperty(PoursuiteConst.ParamName.DEVISE,
                                bap.getFkCompte().getDevise().getCode());

                    } else if (bap.getFkAmr() != null) {
                        jsonObject.addProperty(PoursuiteConst.ParamName.COMPTE_BANCAIRE,
                                GeneralConst.EMPTY_STRING);

                        jsonObject.addProperty(PoursuiteConst.ParamName.DEVISE,
                                bap.getFkAmr().getFichePriseCharge().getDevise());
                    } else {
                        jsonObject.addProperty(PoursuiteConst.ParamName.COMPTE_BANCAIRE,
                                GeneralConst.EMPTY_STRING);

                        jsonObject.addProperty(PoursuiteConst.ParamName.DEVISE,
                                GeneralConst.EMPTY_STRING);
                    }

                    int state = bap.getEtat() == true ? 1 : 0;

                    jsonObject.addProperty("etatCode", state + "");

                    switch (state) {
                        case 3:
                            jsonObject.addProperty(PoursuiteConst.ParamName.STATE_NAME, "EN ATTENTE DE VALIDATION");
                            break;
                        case 2:
                            jsonObject.addProperty(PoursuiteConst.ParamName.STATE_NAME, "VALIDER");
                            break;
                        case 0:
                            jsonObject.addProperty(PoursuiteConst.ParamName.STATE_NAME, "REJETTER");
                            break;
                    }

                    if (!bap.getActeGenerateur().isEmpty() && bap.getActeGenerateur() != null) {
                        jsonObject.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                bap.getActeGenerateur());
                    } else {
                        jsonObject.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                bap.getMotifPenalite());
                    }

                    Journal journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(bap.getCode());

                    if (journal != null) {

                        String codeStatePaid = "Payer";
                        String libelleStatePaid = "";
                        String statuPaidComposite = "";

                        switch (journal.getEtat()) {
                            case 2:
                                libelleStatePaid = "<br/>".concat("<span style='font-weight:bold;font-style: italic;color:red;font-size:9px'>".concat("(Valider à la Banque)").concat("</span>"));
                                break;
                            case 3:
                                libelleStatePaid = "<br/>".concat("<span style='font-weight:bold;font-style: italic;color:red;font-size:9px'>".concat("(En attente de validation à la Banque)").concat("</span>"));
                                break;
                            case 5:
                                libelleStatePaid = "<br/>".concat("<span style='font-weight:bold;font-style: italic;color:red;font-size:9px'>".concat("(Apurement administratif)").concat("</span>"));
                                break;
                            case 1:
                                libelleStatePaid = "<br/>".concat("<span style='font-weight:bold;font-style: italic;color:red;font-size:9px'>".concat("(Apuremnt comptable)").concat("</span>"));
                                break;
                        }

                        statuPaidComposite = codeStatePaid.concat(libelleStatePaid);

                        jsonObject.addProperty("estPayer", statuPaidComposite);

                    } else {

                        jsonObject.addProperty("estPayer", GeneralConst.EMPTY_STRING);

                    }

                    Archive archive = NotePerceptionBusiness.getArchiveByRefDocument(bap.getCode());

                    if (archive != null) {

                        jsonObject.addProperty("bapDocument", archive.getDocumentString());

                    } else {

                        jsonObject.addProperty("bapDocument", GeneralConst.EMPTY_STRING);

                    }

                    bonAPayerJsonList.add(jsonObject);

                }

                dataReturn = bonAPayerJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String traiterFichePriseCharge(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String value = GeneralConst.EMPTY_STRING;
        String idFpc = GeneralConst.EMPTY_STRING;
        String typeFpc = GeneralConst.EMPTY_STRING;
        String userId = GeneralConst.EMPTY_STRING;

        Amr amr1 = new Amr();
        Amr amr2 = new Amr();

        PrintDocument printDocument = new PrintDocument();
        try {

            value = request.getParameter("value");
            idFpc = request.getParameter(PoursuiteConst.ParamName.ID_FPC);
            typeFpc = request.getParameter(PoursuiteConst.ParamName.CAS_FPC);
            userId = request.getParameter(PoursuiteConst.ParamName.USER_ID);

            if (PoursuiteBusiness.traiterFichePriseCharge(value, idFpc, typeFpc, userId)) {

                if (value.equals(GeneralConst.Number.TWO)) {

                    switch (typeFpc) {

                        //case "NP":
                        case "1":
                        case "2":

                            amr2 = PoursuiteBusiness.getAmrTwoByFichePriseCharge(idFpc);

                            if (amr2 != null) {

                                dataReturn = printDocument.createAmr(amr2, userId);
                            } else {
                                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                            }

                            break;

                        //case "TO":
                        case "3":

                            amr2 = PoursuiteBusiness.getAmrTwoByFichePriseCharge(idFpc);
                            String amr2Html = GeneralConst.EMPTY_STRING;

                            if (amr2 != null) {

                                amr2Html = printDocument.createAmr(amr2, userId);
                            } else {
                                amr2Html = GeneralConst.ResultCode.FAILED_OPERATION;
                            }

                            amr1 = PoursuiteBusiness.getAmrOneByFichePriseCharge(idFpc);
                            String amr1Html = GeneralConst.EMPTY_STRING;

                            if (amr1 != null) {

                                amr1Html = printDocument.createAmr(amr1, userId);
                            } else {
                                amr1Html = GeneralConst.ResultCode.FAILED_OPERATION;
                            }

                            JsonObject amrDocJson = new JsonObject();

                            amrDocJson.addProperty("amr1Html", amr1Html);
                            amrDocJson.addProperty("amr2Html", amr2Html);

                            List<JsonObject> amrDocJsonList = new ArrayList<>();

                            amrDocJsonList.add(amrDocJson);

                            dataReturn = amrDocJsonList.toString();

                            break;
                        default:

                            break;
                    }
                } else {
                    dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getFichePriseCharge(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;
        String codeTypeFpc = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);
            codeTypeFpc = request.getParameter(PoursuiteConst.ParamName.CODE_TYPE_FPC);
            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);

            List<FichePriseCharge> fichePriseChargeList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    fichePriseChargeList = PoursuiteBusiness.getListFichePriseChargeBySimpleSearch(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    fichePriseChargeList = PoursuiteBusiness.getListFichePriseChargeByAdvancedSearch(codeService,
                            codeTypeFpc, dateDebut, dateFin);

                    break;
            }

            if (!fichePriseChargeList.isEmpty()) {

                List<JsonObject> fichePriseChargeJsonList = new ArrayList<>();

                for (FichePriseCharge fpc : fichePriseChargeList) {

                    JsonObject jsonObject = new JsonObject();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();
                    BigDecimal amountTotalDu = new BigDecimal("0");

                    if (fpc.getFkMed() != null && !fpc.getFkMed().isEmpty()) {

                        Med med = RecouvrementBusiness.getMedById(fpc.getFkMed());

                        if (med != null) {

                            jsonObject.addProperty("codeMed", med.getId());
                            jsonObject.addProperty("typeMed", med.getTypeMed());
                        }

                    } else {
                        jsonObject.addProperty("codeMed", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("typeMed", GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(PoursuiteConst.ParamName.ID_FPC, fpc.getCode());

                    jsonObject.addProperty(PoursuiteConst.ParamName.DATE_CREATE_FPC,
                            ConvertDate.formatDateToString(fpc.getDateCreat()));

                    personne = fpc.getPersonne();

                    if (personne != null) {

                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_CODE, personne.getCode());
                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME, personne.toString().toUpperCase());

                        String typeAssujetti = GeneralConst.EMPTY_STRING;
                        String assujettiName = GeneralConst.EMPTY_STRING;
                        String adresseAssujetti = GeneralConst.EMPTY_STRING;
                        String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                        typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                personne.getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                        assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                personne.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                        if (adresse != null) {

                            adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                            jsonObject.addProperty(PoursuiteConst.ParamName.ADRESSE_NAME, adresse.toString().toUpperCase());

                        } else {
                            jsonObject.addProperty(PoursuiteConst.ParamName.ADRESSE_NAME, GeneralConst.EMPTY_STRING);
                        }

                        assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                assujettiNameComposite);

                        jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());

                    } else {
                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                GeneralConst.EMPTY_STRING);

                        jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                                GeneralConst.EMPTY_STRING);

                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_CODE, GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME, GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(PoursuiteConst.ParamName.ADRESSE_NAME, GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(PoursuiteConst.ParamName.MOTIF_PENALITE, fpc.getDetailMotif());
                    jsonObject.addProperty(PoursuiteConst.ParamName.MONTANT_DU, fpc.getTotalPrincipaldu());

                    amountTotalDu = amountTotalDu.add(fpc.getTotalPrincipaldu());
                    amountTotalDu = amountTotalDu.add(fpc.getTotalPenalitedu());

                    jsonObject.addProperty(PoursuiteConst.ParamName.MONTANT_TOTAL_DU, amountTotalDu);

                    jsonObject.addProperty(PoursuiteConst.ParamName.DEVISE, fpc.getDevise());
                    jsonObject.addProperty(PoursuiteConst.ParamName.STATE, fpc.getEtat().getCode());
                    jsonObject.addProperty(PoursuiteConst.ParamName.CAS_FPC, fpc.getCasFpc());

                    switch (fpc.getEtat().getCode()) {
                        case 3:
                            jsonObject.addProperty(PoursuiteConst.ParamName.STATE_NAME, "EN ATTENTE DE VALIDATION");
                            break;
                        case 2:
                            jsonObject.addProperty(PoursuiteConst.ParamName.STATE_NAME, "VALIDER");
                            break;
                        case 0:
                            jsonObject.addProperty(PoursuiteConst.ParamName.STATE_NAME, "REJETTER");
                            break;
                    }

                    if (fpc.getFkArticleBudgetaire() != null && !fpc.getFkArticleBudgetaire().isEmpty()) {

                        articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(fpc.getFkArticleBudgetaire());

                        if (articleBudgetaire != null) {
                            jsonObject.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                    articleBudgetaire.getIntitule().toUpperCase());
                        } else {
                            jsonObject.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                    GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        jsonObject.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(PoursuiteConst.ParamName.TITRE_PERCEPTION, fpc.getNumeroReference());

                    if (!fpc.getDetailFichePriseChargeList().isEmpty()) {

                        List<JsonObject> detailFichePriseChargeJsonList = new ArrayList<>();

                        for (DetailFichePriseCharge dfpc : fpc.getDetailFichePriseChargeList()) {

                            JsonObject dfpcJson = new JsonObject();
                            Penalite penalite = new Penalite();

                            dfpcJson.addProperty(PoursuiteConst.ParamName.ID_FPC, fpc.getCode());
                            dfpcJson.addProperty(PoursuiteConst.ParamName.ID_DETAIL_FPC, dfpc.getCode());

                            penalite = PoursuiteBusiness.getPenaliteByID(dfpc.getPenalite());

                            if (penalite != null) {
                                dfpcJson.addProperty(PoursuiteConst.ParamName.PENALITE_NAME,
                                        penalite.getIntitule());
                            } else {
                                dfpcJson.addProperty(PoursuiteConst.ParamName.PENALITE_NAME,
                                        GeneralConst.EMPTY_STRING);
                            }

                            dfpcJson.addProperty(PoursuiteConst.ParamName.TAUX_PENALITE, dfpc.getTauxPenalite());
                            dfpcJson.addProperty(PoursuiteConst.ParamName.PRINCIPAL_DU, dfpc.getPrincipaldu());
                            dfpcJson.addProperty(PoursuiteConst.ParamName.PENALITE_DU, dfpc.getPenalitedu());

                            if (dfpc.getNbreRetardMois() != null && !dfpc.getNbreRetardMois().isEmpty()) {
                                dfpcJson.addProperty(PoursuiteConst.ParamName.NBRE_MOIS_RETARD, dfpc.getNbreRetardMois());

                            } else {
                                dfpcJson.addProperty(PoursuiteConst.ParamName.NBRE_MOIS_RETARD, GeneralConst.Numeric.ZERO);
                            }

                            if (dfpc.getPeriodeDeclaration() > 0) {

                                String libellePeriode = GeneralConst.EMPTY_STRING;

                                PeriodeDeclaration periodeDeclaration = DeclarationBusiness.getPeriodeDeclarationById(
                                        dfpc.getPeriodeDeclaration() + "");

                                if (periodeDeclaration != null) {

                                    String periodiciteAb = periodeDeclaration.getAssujetissement().getPeriodicite();
                                    libellePeriode = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(), periodiciteAb);

                                    dfpcJson.addProperty(PoursuiteConst.ParamName.PERIODE_DECLARATION_NAME,
                                            libellePeriode);

                                } else {
                                    dfpcJson.addProperty(PoursuiteConst.ParamName.PERIODE_DECLARATION_NAME,
                                            libellePeriode);
                                }

                            } else {
                                dfpcJson.addProperty(PoursuiteConst.ParamName.PERIODE_DECLARATION_NAME,
                                        dfpc.getExercice());
                            }

                            detailFichePriseChargeJsonList.add(dfpcJson);
                        }

                        jsonObject.addProperty(PoursuiteConst.ParamName.DETAILS_FPC_LIST,
                                detailFichePriseChargeJsonList.toString());

                    } else {
                        jsonObject.addProperty(PoursuiteConst.ParamName.DETAILS_FPC_LIST, GeneralConst.EMPTY_STRING);
                    }

                    fichePriseChargeJsonList.add(jsonObject);

                }

                dataReturn = fichePriseChargeJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String printAtd(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        PrintDocument printDocument = new PrintDocument();
        String userId = GeneralConst.EMPTY_STRING;
        String idAtd = GeneralConst.EMPTY_STRING;

        try {

            idAtd = request.getParameter("atdID");
            userId = request.getParameter("userId");

            Atd atd = PoursuiteBusiness.getAtdByID(idAtd);

            if (atd != null) {

                dataReturn = printDocument.createAtd(atd, userId);

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String activateGenerate(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String cmdId = GeneralConst.EMPTY_STRING;
        String bapId = GeneralConst.EMPTY_STRING;
        String userId = GeneralConst.EMPTY_STRING;

        String htmlCommandement = GeneralConst.EMPTY_STRING;
        String htmlBonAPayer = GeneralConst.EMPTY_STRING;

        PrintDocument printDocument = new PrintDocument();
        boolean result = false;

        try {

            bapId = request.getParameter(PoursuiteConst.ParamName.NUMERO_BON_A_PAYER);
            cmdId = request.getParameter(PoursuiteConst.ParamName.NUMERO_COMMANDEMENT);
            userId = request.getParameter(PoursuiteConst.ParamName.USER_ID);

            HashMap<String, Object[]> bulkUpdateObj = new HashMap<>();

            int count = 0;

            String query1 = ":UPDATE T_COMMANDEMENT SET ETAT = 1 WHERE ID = '%s'";
            query1 = String.format(query1, cmdId.trim());

            bulkUpdateObj.put(count + query1, new Object[]{});

            String query2 = ":UPDATE T_BON_A_PAYER SET ETAT = 1 WHERE CODE = '%s'";
            query2 = String.format(query2, bapId.trim());

            count++;

            bulkUpdateObj.put(count + query2, new Object[]{});

            result = PoursuiteBusiness.executeQueryBulkUpdate(bulkUpdateObj);

            if (result) {

                Commandement commandement = PoursuiteBusiness.getCommandementByCode(cmdId);

                if (commandement != null) {
                    htmlCommandement = printDocument.createCommandement(commandement, userId);
                }

                BonAPayer bonAPayer = PoursuiteBusiness.getBonAPayerByCode(bapId);

                if (bonAPayer != null) {
                    htmlBonAPayer = printDocument.createBonAPayer(bonAPayer);
                }

                List<JsonObject> jsonList = new ArrayList<>();

                JsonObject jsonObject = new JsonObject();

                jsonObject.addProperty("htmlCommandement", htmlCommandement);
                jsonObject.addProperty("htmlBonApayer", htmlBonAPayer);

                jsonList.add(jsonObject);

                dataReturn = jsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String printDocument(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String referenceDocument = GeneralConst.EMPTY_STRING;
        String typeDocument = GeneralConst.EMPTY_STRING;
        String userId = GeneralConst.EMPTY_STRING;

        Archive archive = new Archive();

        try {

            referenceDocument = request.getParameter(PoursuiteConst.ParamName.REFERENCE_DOCUMENT);
            userId = request.getParameter(PoursuiteConst.ParamName.USER_ID);
            typeDocument = request.getParameter(PoursuiteConst.ParamName.TYPE_DOCUMENT);

            archive = NotePerceptionBusiness.getArchiveByRefDocument(referenceDocument);

            if (archive != null) {

                dataReturn = archive.getDocumentString();

            } else {

                PrintDocument printDocument = new PrintDocument();

                switch (typeDocument) {
                    case DocumentConst.DocumentCode.CONTRAINTE:

                        Contrainte contrainte = PoursuiteBusiness.getContrainteByCode(referenceDocument);

                        if (contrainte != null) {

                            dataReturn = printDocument.createContrainte(contrainte, userId);
                        }

                        break;
                    case DocumentConst.DocumentCode.COMMANDEMENT:

                        Commandement commandement = PoursuiteBusiness.getCommandementByCode(referenceDocument);

                        if (commandement != null) {
                            dataReturn = printDocument.createCommandement(commandement, userId);
                        }

                        break;
                    default:

                        BonAPayer bonAPayer = PoursuiteBusiness.getBonAPayerByCode(referenceDocument);

                        if (bonAPayer != null) {
                            dataReturn = printDocument.createBonAPayer(bonAPayer);
                        }
                        break;
                }
            }

        } catch (Exception e) {

            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String generateAtd(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String contrainteID = GeneralConst.EMPTY_STRING;
        String userId = GeneralConst.EMPTY_STRING;
        String tierDetenteur = GeneralConst.EMPTY_STRING;

        PrintDocument printDocument = new PrintDocument();

        String atdId = GeneralConst.EMPTY_STRING;

        try {

            contrainteID = request.getParameter(PoursuiteConst.ParamName.NUMERO_CONTRAINTE);
            userId = request.getParameter(PoursuiteConst.ParamName.USER_ID);
            tierDetenteur = request.getParameter(PoursuiteConst.ParamName.TIER_DETENTEUR);

            atdId = PoursuiteBusiness.saveAtd(contrainteID, userId, tierDetenteur);

            if (!atdId.isEmpty()) {

                Atd atd = PoursuiteBusiness.getAtdByID(atdId.trim());

                if (atd != null) {

                    dataReturn = printDocument.createAtd(atd, userId);

                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String generateContrainte(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String numeroAmr = GeneralConst.EMPTY_STRING;
        String userId = GeneralConst.EMPTY_STRING;
        String codeAgentHuissier = GeneralConst.EMPTY_STRING;
        String inputParquet = GeneralConst.EMPTY_STRING;
        String monthLater = GeneralConst.EMPTY_STRING;
        String numeroMed = GeneralConst.EMPTY_STRING;

        try {

            numeroAmr = request.getParameter(PoursuiteConst.ParamName.NUMERO_AMR);
            userId = request.getParameter(PoursuiteConst.ParamName.USER_ID);
            codeAgentHuissier = request.getParameter("codeAgentHuissier");
            inputParquet = request.getParameter("parquetName");
            monthLater = request.getParameter("monthLater");
            numeroMed = request.getParameter("numeroMed");

            dataReturn = PoursuiteBusiness.saveContrainteAndCommandement(numeroAmr, userId, codeAgentHuissier, inputParquet);

            if (!dataReturn.isEmpty()) {

                Med med = new Med();
                //Amr amr = PoursuiteBusiness.getAmrByNumero(numeroAmr);

                String contrainteHtml = GeneralConst.EMPTY_STRING;
                String commandementHtml = GeneralConst.EMPTY_STRING;

                PrintDocument printDocument;

                Contrainte contrainte = PoursuiteBusiness.getContrainteByCode(dataReturn);

                if (contrainte != null) {

                    med = RecouvrementBusiness.getMedById(numeroMed);

                    printDocument = new PrintDocument();
                    contrainteHtml = printDocument.createContrainteV2(contrainte, userId, inputParquet, med);
                }

                Commandement commandement = PoursuiteBusiness.getCommandementByContrainte(dataReturn);

                if (commandement != null) {
                    printDocument = new PrintDocument();

                    if (monthLater.equals(GeneralConst.Number.ZERO)) {
                        monthLater = GeneralConst.Number.ONE;
                    }

                    commandementHtml = printDocument.createCommandementV2(commandement, userId, inputParquet, Integer.valueOf(monthLater));
                }

                JsonObject documentJson = new JsonObject();

                documentJson.addProperty("contrainteHtml", contrainteHtml);
                documentJson.addProperty("commandementHtml", commandementHtml);

                dataReturn = documentJson.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String printAmr(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String numeroAmr = GeneralConst.EMPTY_STRING;
        String userId = GeneralConst.EMPTY_STRING;

        try {
            numeroAmr = request.getParameter(PoursuiteConst.ParamName.NUMERO_AMR);
            userId = request.getParameter(PoursuiteConst.ParamName.USER_ID);

            Amr amr = PoursuiteBusiness.getAmrByNumero(numeroAmr);

            if (amr != null) {

                PrintDocument printDocument = new PrintDocument();

                dataReturn = printDocument.createAmr(amr, userId);

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getAtds(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;

        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);

            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);

            atdList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    atdList = PoursuiteBusiness.getListAtdBySimpleSearch(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    atdList = PoursuiteBusiness.getListAtdByAdvancedSearch(codeService, dateDebut, dateFin);

                    break;
            }

            if (!atdList.isEmpty()) {

                List<JsonObject> atdJsonList = new ArrayList<>();

                for (Atd atd : atdList) {

                    JsonObject atdJson = new JsonObject();
                    Personne personne = new Personne();
                    Personne detenteur = new Personne();
                    Adresse adresse = new Adresse();

                    Archive archive = NotePerceptionBusiness.getArchiveByRefDocument(atd.getId());

                    String devise = GeneralConst.EMPTY_STRING;

                    Commandement commandement = PoursuiteBusiness.getCommandementByCode(atd.getIdCommandement());

                    if (commandement != null) {

                        devise = commandement.getIdContrainte().getIdAmr().getFichePriseCharge().getDevise();

                    }

                    if (archive != null) {

                        atdJson.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ONE);
                        atdJson.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT, archive.getDocumentString());

                    } else {
                        atdJson.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ZERO);
                        atdJson.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT, GeneralConst.EMPTY_STRING);
                    }

                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                    atdJson.addProperty("atdID", atd.getId());
                    atdJson.addProperty("commandementID", atd.getIdCommandement());
                    atdJson.addProperty("dateEmission", ConvertDate.formatDateToString(atd.getDateCreat()));
                    atdJson.addProperty(PoursuiteConst.ParamName.NUMERO_DOCUMENT,
                            atd.getNumeroDocument() != null ? atd.getNumeroDocument() : GeneralConst.EMPTY_STRING);

                    atdJson.addProperty("amountGlobal", atd.getMontantGlobal());
                    atdJson.addProperty("amountPenalite", atd.getMontantPenalite());
                    atdJson.addProperty("amountPrincipal", atd.getMontantPrincipal());
                    atdJson.addProperty("amountFontionnement", atd.getMontantFonctionnement());

                    atdJson.addProperty("devise", devise);

                    personne = IdentificationBusiness.getPersonneByCode(atd.getIdRedevable());

                    if (personne != null) {

                        atdJson.addProperty(PoursuiteConst.ParamName.ASSUJETTI_CODE, personne.getCode());

                        typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                personne.getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                        assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                personne.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                        if (adresse != null) {

                            adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                        }

                        assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                        atdJson.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                assujettiNameComposite);

                        atdJson.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());

                    } else {
                        atdJson.addProperty(PoursuiteConst.ParamName.ASSUJETTI_CODE,
                                GeneralConst.EMPTY_STRING);
                        atdJson.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                GeneralConst.EMPTY_STRING);
                    }

                    detenteur = IdentificationBusiness.getPersonneByCode(atd.getIdDetenteur());

                    if (detenteur != null) {

                        atdJson.addProperty("detenteurCode", detenteur.getCode());

                        typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                personne.getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                        assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                detenteur.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(detenteur.getCode());

                        if (adresse != null) {

                            adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                        }

                        assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                        atdJson.addProperty("detenteurNameComposite", assujettiNameComposite);

                    } else {
                        atdJson.addProperty("detenteurCode", GeneralConst.EMPTY_STRING);
                        atdJson.addProperty("detenteurNameComposite", GeneralConst.EMPTY_STRING);
                    }

                    atdJsonList.add(atdJson);
                }

                dataReturn = atdJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getCommandements(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;
        String codeTypeContrainte = GeneralConst.EMPTY_STRING;
        String codeStateContrainte = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);
            codeTypeContrainte = request.getParameter(PoursuiteConst.ParamName.CODE_TYPE_CONTRAINTE);
            codeStateContrainte = request.getParameter(PoursuiteConst.ParamName.CODE_STATE_CONTRAINTE);
            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);

            commandementList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    commandementList = PoursuiteBusiness.getListCommandementBySimpleSearch(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    commandementList = PoursuiteBusiness.getListCommandementByAdvancedSearch(codeService,
                            codeTypeContrainte, dateDebut, dateFin);

                    break;
            }

            if (!commandementList.isEmpty()) {

                List<JsonObject> commandementJsonObjList = new ArrayList<>();

                String deviseContrainte = GeneralConst.EMPTY_STRING;

                for (Commandement commandement : commandementList) {

                    JsonObject commandementJsonObj = new JsonObject();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();

                    Contrainte contrainte = new Contrainte();

                    String nextStap = GeneralConst.Number.ZERO;
                    boolean cmdActivate = false;

                    commandementJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_COMMANDEMENT,
                            commandement.getId());

                    commandementJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_CONTRAINTE,
                            commandement.getIdContrainte().getNumeroDocument() == null ? ""
                                    : commandement.getIdContrainte().getNumeroDocument().toUpperCase());

                    commandementJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_DOCUMENT,
                            commandement.getNumeroDocument() != null ? commandement.getNumeroDocument() : GeneralConst.EMPTY_STRING);

                    commandementJsonObj.addProperty("dateEmissionContrainte",
                            ConvertDate.formatDateToString(commandement.getIdContrainte().getDateReception()));

                    Med med = new Med();

                    if (commandement.getIdContrainte() != null) {

                        contrainte = commandement.getIdContrainte();

                        med = RecouvrementBusiness.getMedById(contrainte.getIdAmr().getFkMed());

                        personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());

                    }

                    if (personne != null) {

                        String typeAssujetti = GeneralConst.EMPTY_STRING;
                        String assujettiName = GeneralConst.EMPTY_STRING;
                        String adresseAssujetti = GeneralConst.EMPTY_STRING;
                        String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                        commandementJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_CODE, personne.getCode());

                        typeAssujetti = "NTD : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                personne.getLoginWeb().getUsername().toUpperCase().concat("</span>")));

                        assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                personne.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                        if (adresse != null) {

                            adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                        }

                        assujettiNameComposite = typeAssujetti.concat("<br/><br/>").concat(assujettiName).concat("<br/><br/>").concat(adresseAssujetti);

                        commandementJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                assujettiNameComposite);

                    } else {
                        commandementJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_CODE,
                                GeneralConst.EMPTY_STRING);
                        commandementJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                GeneralConst.EMPTY_STRING);

                    }

                    commandementJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_POURSUIVI,
                            commandement.getMontantPoursuivi());

                    commandementJsonObj.addProperty(PoursuiteConst.ParamName.FRAIS_POURSUITE,
                            commandement.getMontantFraisPoursuite());

                    commandementJsonObj.addProperty(PoursuiteConst.ParamName.DATE_CREAT_COMMANDEMENT,
                            ConvertDate.formatDateToString(commandement.getDateCreat()));

                    if (contrainte.getDateEcheance() != null) {
                        commandementJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_COMMANDEMENT,
                                ConvertDate.formatDateToString(commandement.getDateEcheance()));

                        if (contrainte.getDateEcheance().before(new Date())) {

                            nextStap = cmdActivate == true ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE;

                            commandementJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    nextStap);

                        } else {
                            commandementJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ZERO);
                        }

                    } else {
                        commandementJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_COMMANDEMENT,
                                GeneralConst.EMPTY_STRING);
                        commandementJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                nextStap);
                    }

                    if (contrainte.getDateReception() != null) {
                        commandementJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_COMMANDEMENT,
                                ConvertDate.formatDateToString(commandement.getDateReception()));
                    } else {
                        commandementJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_COMMANDEMENT,
                                GeneralConst.EMPTY_STRING);
                    }

                    ArticleBudgetaire articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(commandement.getFaitGenerateur());

                    commandementJsonObj.addProperty(PoursuiteConst.ParamName.FAIT_GENERATEUR,
                            articleBudgetaire.getIntitule().toUpperCase());

                    String periodeName = GeneralConst.EMPTY_STRING;
                    NotePerception notePerception = new NotePerception();

                    switch (med.getTypeMed()) {

                        case "INVITATION_PAIEMENT":
                        case "RELANCE":

                            RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(med.getPeriodeDeclaration() + "");
                            deviseContrainte = retraitDeclaration.getDevise();
                            PeriodeDeclaration periodeDeclaration = AssujettissementBusiness.getPeriodeDeclarationByCode(med.getPeriodeDeclaration() + "");
                            periodeName = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(), articleBudgetaire.getPeriodicite().getCode());
                            break;

                        case "INVITATION_SERVICE":

                            notePerception = NotePerceptionBusiness.getNotePerceptionByCode(med.getNotePerception());
                            deviseContrainte = notePerception.getDevise();
                            periodeName = notePerception.getNoteCalcul().getExercice();
                            break;
                        default:
                            notePerception = NotePerceptionBusiness.getNotePerceptionByCode(med.getNotePerception());
                            deviseContrainte = notePerception.getDevise();
                            periodeName = notePerception.getNoteCalcul().getExercice();
                            break;
                    }

                    commandementJsonObj.addProperty(PoursuiteConst.ParamName.DEVISE, deviseContrainte);
                    commandementJsonObj.addProperty("periodeDeclaration", periodeName.toUpperCase());

                    commandementJsonObjList.add(commandementJsonObj);
                }

                dataReturn = commandementJsonObjList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getContraintes(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;
        String codeTypeContrainte = GeneralConst.EMPTY_STRING;
        String codeStateContrainte = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);
            codeTypeContrainte = request.getParameter(PoursuiteConst.ParamName.CODE_TYPE_CONTRAINTE);
            codeStateContrainte = request.getParameter(PoursuiteConst.ParamName.CODE_STATE_CONTRAINTE);
            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);

            contrainteList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    contrainteList = PoursuiteBusiness.getListContrainteBySimpleSearch(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    contrainteList = PoursuiteBusiness.getListContrainteByAdvancedSearch(codeService,
                            codeTypeContrainte, dateDebut, dateFin);

                    break;
            }

            if (!contrainteList.isEmpty()) {

                List<JsonObject> contrainteJsonObjList = new ArrayList<>();

                String deviseContrainte = GeneralConst.EMPTY_STRING;
                String codeCommandement = GeneralConst.EMPTY_STRING;

                for (Contrainte contrainte : contrainteList) {

                    JsonObject contrainteJsonObj = new JsonObject();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    Archive archive = new Archive();

                    String nextStap = GeneralConst.Number.ZERO;
                    boolean cmdActivate = false;

                    contrainteJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_CONTRAINTE, contrainte.getId());

                    Med med = RecouvrementBusiness.getMedById(contrainte.getIdAmr().getFkMed());

                    personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());

                    if (personne != null) {

                        String typeAssujetti = GeneralConst.EMPTY_STRING;
                        String assujettiName = GeneralConst.EMPTY_STRING;
                        String adresseAssujetti = GeneralConst.EMPTY_STRING;
                        String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                        typeAssujetti = "NTD : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                personne.getLoginWeb().getUsername().toUpperCase().concat("</span>")));

                        assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                personne.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                        if (adresse != null) {

                            adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                        }

                        assujettiNameComposite = typeAssujetti.concat("<br/><br/>").concat(assujettiName).concat("<br/><br/>").concat(adresseAssujetti);

                        contrainteJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                assujettiNameComposite);

                        contrainteJsonObj.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());

                    } else {
                        contrainteJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                GeneralConst.EMPTY_STRING);
                    }

                    contrainteJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_RECOUVREMENT,
                            contrainte.getMontantRecouvrement());

                    contrainteJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_PENALITE,
                            contrainte.getMontantPenalite());
                    contrainteJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_DOCUMENT,
                            contrainte.getNumeroDocument() != null ? contrainte.getNumeroDocument() : GeneralConst.EMPTY_STRING);

                    BigDecimal amountTotal = new BigDecimal(0);

                    amountTotal = amountTotal.add(contrainte.getMontantRecouvrement());
                    amountTotal = amountTotal.add(contrainte.getMontantPenalite());

                    contrainteJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_POURSUIVI, amountTotal);

                    BigDecimal _3percent = new BigDecimal("0.03");
                    BigDecimal fraisPoursuite = new BigDecimal(0);

                    fraisPoursuite = fraisPoursuite.add(amountTotal.multiply(_3percent));

                    contrainteJsonObj.addProperty(PoursuiteConst.ParamName.FRAIS_POURSUITE,
                            fraisPoursuite);

                    NotePerception notePerception;
                    String periodeName = GeneralConst.EMPTY_STRING;

                    ArticleBudgetaire articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(contrainte.getFaitGenerateur());

                    RetraitDeclaration retraitDeclaration;
                    PeriodeDeclaration periodeDeclaration;

                    switch (med.getTypeMed()) {

                        case "RELANCE":

                            retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(med.getPeriodeDeclaration() + "");
                            deviseContrainte = retraitDeclaration.getDevise();
                            periodeDeclaration = AssujettissementBusiness.getPeriodeDeclarationByCode(med.getPeriodeDeclaration() + "");
                            periodeName = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(), articleBudgetaire.getPeriodicite().getCode());
                            break;
                        case "INVITATION_PAIEMENT":

                            retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(med.getPeriodeDeclaration() + "");
                            deviseContrainte = retraitDeclaration.getDevise();
                            periodeDeclaration = AssujettissementBusiness.getPeriodeDeclarationByCode(med.getPeriodeDeclaration() + "");
                            periodeName = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(), articleBudgetaire.getPeriodicite().getCode());
                            break;

                        case "INVITATION_SERVICE":

                            notePerception = NotePerceptionBusiness.getNotePerceptionByCode(med.getNotePerception());
                            deviseContrainte = notePerception.getDevise();
                            periodeName = notePerception.getNoteCalcul().getExercice();
                            break;
                        default:
                            notePerception = NotePerceptionBusiness.getNotePerceptionByCode(med.getNotePerception());
                            deviseContrainte = notePerception.getDevise();
                            periodeName = notePerception.getNoteCalcul().getExercice();
                            break;
                    }

                    contrainteJsonObj.addProperty(PoursuiteConst.ParamName.DEVISE, deviseContrainte);

                    contrainteJsonObj.addProperty(PoursuiteConst.ParamName.DATE_CREAT_CONTRAINTE,
                            ConvertDate.formatDateToString(contrainte.getDateCreat()));

                    if (contrainte.getDateEcheance() != null) {
                        contrainteJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_CONTRAINTE,
                                ConvertDate.formatDateToString(contrainte.getDateEcheance()));

                        if (contrainte.getDateEcheance().before(new Date())) {

                            nextStap = cmdActivate == true ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE;

                            contrainteJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    nextStap);

                        } else {
                            contrainteJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ZERO);
                        }

                    } else {
                        contrainteJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_CONTRAINTE,
                                GeneralConst.EMPTY_STRING);
                        contrainteJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                nextStap);
                    }

                    if (contrainte.getDateReception() != null) {
                        contrainteJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_CONTRAINTE,
                                ConvertDate.formatDateToString(contrainte.getDateReception()));
                    } else {
                        contrainteJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_CONTRAINTE,
                                GeneralConst.EMPTY_STRING);
                    }

                    contrainteJsonObj.addProperty(PoursuiteConst.ParamName.FAIT_GENERATEUR,
                            articleBudgetaire.getIntitule().toUpperCase());

                    contrainteJsonObj.addProperty("periodeDeclaration", periodeName.toUpperCase());

                    contrainteJsonObjList.add(contrainteJsonObj);
                }

                dataReturn = contrainteJsonObjList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getAvisMiseEnRecouvrement(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;
        String codeTypeAmr = GeneralConst.EMPTY_STRING;
        String codeStateAmr = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        String typeAssujetti = GeneralConst.EMPTY_STRING;
        String assujettiName = GeneralConst.EMPTY_STRING;
        String adresseAssujetti = GeneralConst.EMPTY_STRING;
        String assujettiNameComposite = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);
            //codeTypeAmr = request.getParameter(PoursuiteConst.ParamName.CODE_TYPE_AMR);
            codeStateAmr = request.getParameter(PoursuiteConst.ParamName.CODE_STATE_AMR);
            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);

            amrList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    amrList = PoursuiteBusiness.getListAmrBySimpleSearch(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    amrList = PoursuiteBusiness.getListAmrByAdvancedSearch(codeService, codeStateAmr, dateDebut, dateFin);

                    break;
            }

            if (!amrList.isEmpty()) {

                List<JsonObject> amrJsonObjList = new ArrayList<>();

                for (Amr amr : amrList) {

                    JsonObject amrJsonObj = new JsonObject();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    Archive archive = new Archive();
                    Med med = new Med();
                    RetraitDeclaration retraitDeclaration = new RetraitDeclaration();
                    String deviseAmr = GeneralConst.EMPTY_STRING;
                    ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();
                    PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();

                    boolean paiementExist = false;

                    if (!amr.getContrainteList().isEmpty()) {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.POURSUITE_EXIST, GeneralConst.Number.ONE);
                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.POURSUITE_EXIST, GeneralConst.Number.ZERO);
                    }

                    amrJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_AMR, amr.getNumero());
                    amrJsonObj.addProperty("numeroMed", amr.getFkMed());
                    amrJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_DOCUMENT,
                            amr.getNumeroDocument() != null ? amr.getNumeroDocument() : GeneralConst.EMPTY_STRING);

                    Date dateCreate = Tools.formatStringFullToDate(amr.getDateCreat());

                    amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_CREATE_AMR,
                            Tools.formatDateToStringV2(dateCreate));

                    boolean nextStap = false;
                    if (amr.getDateEcheance() != null) {

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_LIMITE_AMR,
                                ConvertDate.formatDateToString(amr.getDateEcheance()));

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.NOMBRE_MOIS_REATARD,
                                Math.abs(ConvertDate.getMonthsBetween(amr.getDateEcheance(), new Date())));

                        if (amr.getDateEcheance().before(new Date())) {

                            amrJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ONE);

                            nextStap = true;

                        } else {
                            amrJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ZERO);
                            nextStap = true;
                        }

                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_LIMITE_AMR,
                                GeneralConst.EMPTY_STRING);
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.NOMBRE_MOIS_REATARD,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (amr.getDateReceptionAmr() != null) {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_AMR,
                                ConvertDate.formatDateToString(amr.getDateReceptionAmr()));
                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_AMR,
                                GeneralConst.EMPTY_STRING);
                    }

                    med = RecouvrementBusiness.getMedById(amr.getFkMed());

                    if (med != null) {

                        articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(med.getArticleBudgetaire());
                        periodeDeclaration = DeclarationBusiness.getPeriodeDeclarationById(med.getPeriodeDeclaration() + "");

                        amrJsonObj.addProperty("articleBudgetaireName", articleBudgetaire.getIntitule().toUpperCase());

                        if (periodeDeclaration != null) {
                            amrJsonObj.addProperty("periodeDeclarationName", Tools.getPeriodeIntitule(periodeDeclaration.getDebut(),
                                    articleBudgetaire.getPeriodicite().getCode()));
                        } else {
                            amrJsonObj.addProperty("periodeDeclarationName", "--");
                        }

                        retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(med.getPeriodeDeclaration() + "");

                        Bordereau bordereau = GeneralBusiness.getBordereauByDeclaration(retraitDeclaration.getCodeDeclaration());

                        amrJsonObj.addProperty("paiementExist", bordereau == null ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE);

                        if (nextStap) {
                            paiementExist = bordereau == null ? true : true;
                        } else {
                            paiementExist = bordereau == null ? false : true;
                        }

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP, paiementExist == true
                                ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                        deviseAmr = retraitDeclaration == null ? GeneralConst.Devise.DEVISE_USD : retraitDeclaration.getDevise();

                        personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());

                        if (personne != null) {

                            String ntd = personne.getLoginWeb().getUsername() == null ? "" : personne.getLoginWeb().getUsername().toUpperCase();

                            if (personne.getFormeJuridique() != null) {
                                typeAssujetti = "NTD : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                        ntd.concat("</span>")));
                            }

                            assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                    personne.toString().toUpperCase()).concat("</span>"));

                            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                            if (adresse != null) {

                                adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                        "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                                "</span>"));

                            }

                            assujettiNameComposite = typeAssujetti.concat("<br/><br/>").concat(assujettiName).concat("<br/><br/>").concat(adresseAssujetti);

                            amrJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                    assujettiNameComposite);

                            /*amrJsonObj.addProperty(IdentificationConst.ParamName.USER_NAME,
                             personne.getLoginWeb() == null
                             ? GeneralConst.EMPTY_STRING
                             : personne.getLoginWeb().getUsername());*/
                        } else {
                            amrJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                    GeneralConst.EMPTY_STRING);
                            /*amrJsonObj.addProperty(IdentificationConst.ParamName.USER_NAME,
                             GeneralConst.EMPTY_STRING);*/
                        }

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.TYPE_AMR, "AMR");
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_PRINCIPAL_AMR, med.getMontant());
                        amrJsonObj.addProperty("amountPenalite", med.getMontantPenalite());
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_PAID_AMR, 0);
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DEVISE_AMR, deviseAmr);
                    }

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(amr.getNumero());

                    if (archive != null) {

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT, archive.getDocumentString());
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ONE);

                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT, GeneralConst.EMPTY_STRING);
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ZERO);
                    }

                    amrJsonObjList.add(amrJsonObj);

                }

                dataReturn = amrJsonObjList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getAmrTaxationOffice(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;
        String codeTypeAmr = GeneralConst.EMPTY_STRING;
        String codeStateAmr = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        String typeAssujetti = GeneralConst.EMPTY_STRING;
        String assujettiName = GeneralConst.EMPTY_STRING;
        String adresseAssujetti = GeneralConst.EMPTY_STRING;
        String assujettiNameComposite = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);
            //codeTypeAmr = request.getParameter(PoursuiteConst.ParamName.CODE_TYPE_AMR);
            codeStateAmr = request.getParameter(PoursuiteConst.ParamName.CODE_STATE_AMR);
            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);

            amrList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    amrList = PoursuiteBusiness.getListAmrTaxationofficeBySimpleSearch(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    amrList = PoursuiteBusiness.getListAmrTaxationOfficeByAdvancedSearch(codeService, codeStateAmr, dateDebut, dateFin);

                    break;
            }

            if (!amrList.isEmpty()) {

                List<JsonObject> amrJsonObjList = new ArrayList<>();

                for (Amr amr : amrList) {

                    JsonObject amrJsonObj = new JsonObject();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    Archive archive = new Archive();
                    Med med = new Med();
                    RetraitDeclaration retraitDeclaration = new RetraitDeclaration();
                    String deviseAmr = GeneralConst.EMPTY_STRING;
                    ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();
                    PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();

                    boolean paiementExist = false;

                    if (!amr.getContrainteList().isEmpty()) {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.POURSUITE_EXIST, GeneralConst.Number.ONE);
                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.POURSUITE_EXIST, GeneralConst.Number.ZERO);
                    }

                    amrJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_AMR, amr.getNumero());
                    amrJsonObj.addProperty("numeroMed", amr.getFkMed());
                    amrJsonObj.addProperty("numeroDocument", amr.getNumeroDocument());

                    RetraitDeclaration retraitDeclarationNT = null;

                    if (amr.getFkRetraitDeclaration() != null) {

                        retraitDeclarationNT = AssujettissementBusiness.getRetraitDeclarationByID(
                                amr.getFkRetraitDeclaration());
                    }

                    if (retraitDeclarationNT != null) {

                        amrJsonObj.addProperty("noteTaxationPrincipal", retraitDeclarationNT.getCodeDeclaration());
                        amrJsonObj.addProperty("amountNoteTaxationPrincipal", retraitDeclarationNT.getMontant());
                        amrJsonObj.addProperty("deviseNoteTaxationPrincipal", retraitDeclarationNT.getDevise());

                        retraitDeclarationNT = AssujettissementBusiness.getRetraitDeclarationByCodeMere(retraitDeclarationNT.getId());

                        amrJsonObj.addProperty("noteTaxationPenalite", retraitDeclarationNT.getCodeDeclaration());

                        if (retraitDeclarationNT.getPenaliteRemise().floatValue() > 0) {

                            amrJsonObj.addProperty("amountNoteTaxationPenalite", retraitDeclarationNT.getPenaliteRemise());

                        } else {
                            amrJsonObj.addProperty("amountNoteTaxationPenalite", retraitDeclarationNT.getMontant());
                        }

                        amrJsonObj.addProperty("deviseNoteTaxationPrincipal", retraitDeclarationNT.getDevise());

                    } else {

                        amrJsonObj.addProperty("noteTaxationPrincipal", GeneralConst.EMPTY_STRING);
                        amrJsonObj.addProperty("noteTaxationPenalite", GeneralConst.EMPTY_STRING);
                        amrJsonObj.addProperty("amountNoteTaxationPrincipal", GeneralConst.EMPTY_STRING);
                        amrJsonObj.addProperty("amountNoteTaxationPenalite", GeneralConst.EMPTY_STRING);
                    }

                    Date dateCreate = Tools.formatStringFullToDate(amr.getDateCreat());

                    amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_CREATE_AMR,
                            Tools.formatDateToStringV2(dateCreate));

                    boolean nextStap = false;
                    if (amr.getDateEcheance() != null) {

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_LIMITE_AMR,
                                ConvertDate.formatDateToString(amr.getDateEcheance()));

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.NOMBRE_MOIS_REATARD,
                                Math.abs(ConvertDate.getMonthsBetween(amr.getDateEcheance(), new Date())));

                        if (amr.getDateEcheance().before(new Date())) {

                            amrJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ONE);

                            nextStap = true;

                        } else {
                            amrJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ZERO);
                            nextStap = true;
                        }

                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_LIMITE_AMR,
                                GeneralConst.EMPTY_STRING);
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.NOMBRE_MOIS_REATARD,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (amr.getDateReceptionAmr() != null) {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_AMR,
                                ConvertDate.formatDateToString(amr.getDateReceptionAmr()));
                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_AMR,
                                GeneralConst.EMPTY_STRING);
                    }

                    med = RecouvrementBusiness.getMedById(amr.getFkMed());

                    if (med != null) {

                        articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(med.getArticleBudgetaire());
                        periodeDeclaration = DeclarationBusiness.getPeriodeDeclarationById(med.getPeriodeDeclaration() + "");

                        amrJsonObj.addProperty("articleBudgetaireName", articleBudgetaire.getIntitule().toUpperCase());

                        if (periodeDeclaration != null) {
                            amrJsonObj.addProperty("periodeDeclarationName", Tools.getPeriodeIntitule(periodeDeclaration.getDebut(),
                                    articleBudgetaire.getPeriodicite().getCode()));
                        } else {
                            amrJsonObj.addProperty("periodeDeclarationName", "--");
                        }

                        retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(med.getPeriodeDeclaration() + "");

                        Bordereau bordereau = GeneralBusiness.getBordereauByDeclaration(retraitDeclaration.getCodeDeclaration());

                        amrJsonObj.addProperty("paiementExist", bordereau == null ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE);

                        if (nextStap) {
                            paiementExist = bordereau == null ? true : true;
                        } else {
                            paiementExist = bordereau == null ? false : true;
                        }

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP, paiementExist == true
                                ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                        deviseAmr = retraitDeclaration == null ? GeneralConst.Devise.DEVISE_USD : retraitDeclaration.getDevise();

                        personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());

                        if (personne != null) {

                            String ntd = personne.getLoginWeb().getUsername() == null ? "" : personne.getLoginWeb().getUsername().toUpperCase();

                            if (personne.getFormeJuridique() != null) {
                                typeAssujetti = "NTD : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                        ntd.concat("</span>")));
                            }

                            assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                    personne.toString().toUpperCase()).concat("</span>"));

                            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                            if (adresse != null) {

                                adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                        "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                                "</span>"));

                            }

                            assujettiNameComposite = typeAssujetti.concat("<br/><br/>").concat(assujettiName).concat("<br/><br/>").concat(adresseAssujetti);

                            amrJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                    assujettiNameComposite);

                            /*amrJsonObj.addProperty(IdentificationConst.ParamName.USER_NAME,
                             personne.getLoginWeb() == null
                             ? GeneralConst.EMPTY_STRING
                             : personne.getLoginWeb().getUsername());*/
                        } else {
                            amrJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                    GeneralConst.EMPTY_STRING);
                            /*amrJsonObj.addProperty(IdentificationConst.ParamName.USER_NAME,
                             GeneralConst.EMPTY_STRING);*/
                        }

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.TYPE_AMR, "AMR");
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_PRINCIPAL_AMR, med.getMontant());

                        RetraitDeclaration retraitPenalite = AssujettissementBusiness.getRetraitDeclarationByCodeMere(
                                amr.getFkRetraitDeclaration());

                        BigDecimal penaliteDu = new BigDecimal("0");

                        if (retraitPenalite.getPenaliteRemise().floatValue() > 0) {
                            penaliteDu = penaliteDu.add(retraitPenalite.getPenaliteRemise());
                        } else {
                            penaliteDu = penaliteDu.add(retraitPenalite.getMontant());
                        }
                        amrJsonObj.addProperty("amountPenalite", penaliteDu);
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_PAID_AMR, 0);
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DEVISE_AMR, deviseAmr);
                    }

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(amr.getNumero());

                    if (archive != null) {

                        //amrJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT, archive.getDocumentString());
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT, GeneralConst.EMPTY_STRING);
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ONE);

                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT, GeneralConst.EMPTY_STRING);
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ZERO);
                    }

                    amrJsonObjList.add(amrJsonObj);

                }

                dataReturn = amrJsonObjList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getMiseEnDemeure(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;
        String codeTypeMiseEnDemeure = GeneralConst.EMPTY_STRING;
        String codeStateMiseEnDemeure = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);
            codeTypeMiseEnDemeure = request.getParameter(PoursuiteConst.ParamName.CODE_TYPE_MISE_EN_DEMEURE);
            codeStateMiseEnDemeure = request.getParameter(PoursuiteConst.ParamName.CODE_STATE_MISE_EN_DEMEURE);
            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);

            medList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    medList = PoursuiteBusiness.getListMedBySimpleSearch(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    medList = PoursuiteBusiness.getListMedByAdvancedSearch(codeService,
                            codeTypeMiseEnDemeure, codeStateMiseEnDemeure, dateDebut, dateFin);

                    break;
                case GeneralConst.Number.TWO:

                    medList = PoursuiteBusiness.getListMedBySearch(codeService);

                    break;
            }

            if (!medList.isEmpty()) {

                List<JsonObject> medJsonList = new ArrayList<>();

                for (Med med : medList) {

                    JsonObject medJsonObj = new JsonObject();
                    Agent agent = new Agent();
                    Archive archive = new Archive();
                    NoteCalcul noteCalcul = new NoteCalcul();
                    NotePerception notePerception = new NotePerception();
                    Tarif tarif = new Tarif();
                    PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();
                    FichePriseCharge fpc = new FichePriseCharge();

                    String devise = GeneralConst.EMPTY_STRING;

                    medJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_MED, med.getId());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.EXERCICE_MED, med.getExerciceFiscal());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.TYPE_MED, med.getTypeMed());

                    personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());

                    fpc = PoursuiteBusiness.getFichePriseChargeByMed(med.getId());

                    boolean fpcExist = false;

                    if (fpc != null) {
                        medJsonObj.addProperty("fpcExist", GeneralConst.Number.ONE);
                        medJsonObj.addProperty("amountPenaliteMed", fpc.getTotalPenalitedu());

                        fpcExist = true;

                    } else {
                        medJsonObj.addProperty("fpcExist", GeneralConst.Number.ZERO);
                        medJsonObj.addProperty("amountPenaliteMed", 0);
                    }

                    medJsonObj.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS,
                            GeneralConst.Number.ONE);

                    if (med.getDateEcheance() != null) {

                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_MED,
                                ConvertDate.formatDateToString(med.getDateEcheance()));

                        long nombreJour = Math.abs(ConvertDate.getMonthsBetween(med.getDateEcheance(), new Date()));
                        medJsonObj.addProperty("nombreMois", nombreJour);

                        if (new Date().after(med.getDateEcheance())) {

                            if (fpcExist) {
                                medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                        GeneralConst.Number.ZERO);
                            } else {
                                medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                        GeneralConst.Number.ONE);
                            }

                        } else {
                            medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ZERO);
                        }

                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_MED,
                                GeneralConst.EMPTY_STRING);
                        medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                GeneralConst.Number.ZERO);
                        medJsonObj.addProperty("nombreMois", 0);
                    }

                    if (med.getDateReception() != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_MED,
                                ConvertDate.formatDateToString(med.getDateReception()));
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_MED,
                                GeneralConst.EMPTY_STRING);
                    }

                    medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_CREATION_MED,
                            ConvertDate.formatDateToString(med.getDateCreat()));

                    agent = GeneralBusiness.getAgentByCode(med.getAgentCreat());

                    if (agent != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.AGENT_CREATION_MED,
                                agent.toString().toUpperCase());
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.AGENT_CREATION_MED,
                                GeneralConst.EMPTY_STRING);
                    }

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(med.getId());

                    if (archive != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT,
                                archive.getDocumentString());
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT,
                                GeneralConst.EMPTY_STRING);
                    }

                    int resultRecidiviste = 0;

                    switch (med.getTypeMed()) {

                        case DocumentConst.DocumentCode.MEP_2:

                            noteCalcul = TaxationBusiness.getNoteCalculByNumero(med.getNoteCalcul());

                            if (noteCalcul != null) {
                                medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                        Tools.getArticleByNC_V2(noteCalcul));

                                resultRecidiviste = PoursuiteBusiness.checkRecidivistePaiement(
                                        noteCalcul.getDetailsNcList().get(0).getArticleBudgetaire().getCode(),
                                        noteCalcul.getPersonne());

                                medJsonObj.addProperty("isRecidiviste", resultRecidiviste);

                            } else {
                                medJsonObj.addProperty("isRecidiviste", GeneralConst.Numeric.ZERO);
                                medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                        GeneralConst.EMPTY_STRING);
                            }

                            if (med.getNotePerception() != null && !med.getNotePerception().isEmpty()) {

                                notePerception = NotePerceptionBusiness.getNotePerceptionByCode(
                                        med.getNotePerception());

                                if (notePerception != null) {

                                    medJsonObj.addProperty("referenceID", notePerception.getNumero());
                                    medJsonObj.addProperty("referenceName", notePerception.getNumero());

                                    devise = notePerception.getDevise();
                                }
                            }

                            break;
                        case DocumentConst.DocumentCode.MED_2:

                            periodeDeclaration = DeclarationBusiness.getPeriodeDeclarationById(
                                    med.getPeriodeDeclaration() + "");

                            if (periodeDeclaration != null) {

                                resultRecidiviste = PoursuiteBusiness.checkRecidivisteDeclaration(
                                        periodeDeclaration.getAssujetissement().getId(),
                                        periodeDeclaration.getId());

                                medJsonObj.addProperty("isRecidiviste", resultRecidiviste);

                                String periodeName = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(),
                                        periodeDeclaration.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode());

                                medJsonObj.addProperty("referenceID", periodeDeclaration.getId());
                                medJsonObj.addProperty("referenceName", periodeName);

                                if (periodeDeclaration.getAssujetissement() != null) {

                                    articleBudgetaire = periodeDeclaration.getAssujetissement().getArticleBudgetaire();
                                    tarif = periodeDeclaration.getAssujetissement().getTarif();

                                    medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                            articleBudgetaire.getIntitule().concat(GeneralConst.AB_SEPARTOR).concat(
                                                    tarif.getIntitule()));

                                    Palier palier = AssujettissementBusiness.getPalierByAbTarifAndTypePersonne(
                                            articleBudgetaire.getCode(),
                                            tarif.getCode(),
                                            personne.getFormeJuridique().getCode());

                                    if (palier != null) {

                                        devise = palier.getDevise().getCode();
                                    }

                                } else {
                                    medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                            GeneralConst.EMPTY_STRING);
                                }

                            } else {
                                medJsonObj.addProperty("isRecidiviste", GeneralConst.Numeric.ZERO);
                                medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                        GeneralConst.EMPTY_STRING);
                            }
                            break;

                    }

                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                    if (personne != null) {

                        typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                personne.getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                        assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                personne.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                        if (adresse != null) {

                            adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                        }

                        assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                        medJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                assujettiNameComposite);

                        medJsonObj.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());
                    }

                    medJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_MED, med.getMontant());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.DEVISE_MED, devise);

                    medJsonList.add(medJsonObj);
                }

                dataReturn = medJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getInvitationApayer(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;
        String codeStateMiseEnDemeure = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);
            codeStateMiseEnDemeure = request.getParameter(PoursuiteConst.ParamName.CODE_STATE_MISE_EN_DEMEURE);
            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);
            String codeSiteRec = request.getParameter("codeSiteRec");

            medList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    medList = PoursuiteBusiness.getListMedBySimpleSearch(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    medList = PoursuiteBusiness.getListInvitationApayerByAdvancedSearch(codeService,
                            codeStateMiseEnDemeure, dateDebut, dateFin, codeSiteRec);

                    break;
            }

            if (!medList.isEmpty()) {

                List<JsonObject> medJsonList = new ArrayList<>();

                for (Med med : medList) {

                    JsonObject medJsonObj = new JsonObject();
                    Agent agent = new Agent();
                    Archive archive = new Archive();
                    PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();

                    medJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_MED, med.getId());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.EXERCICE_MED, med.getExerciceFiscal());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.TYPE_MED, med.getTypeMed());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_DOCUMENT,
                            med.getNumeroDocument() != null ? med.getNumeroDocument() : GeneralConst.EMPTY_STRING);

                    personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());

                    Amr amr = PoursuiteBusiness.getAmrByMed(med.getId());

                    boolean fpcExist = false;

                    if (amr != null) {
                        medJsonObj.addProperty("amrExist", GeneralConst.Number.ONE);
                        fpcExist = true;
                    } else {
                        medJsonObj.addProperty("amrExist", GeneralConst.Number.ZERO);
                    }

                    RetraitDeclaration declarationPrincipal = AssujettissementBusiness.getRetraitDeclarationPrincipalByPeriode(
                            med.getPeriodeDeclaration() + "");

                    BigDecimal amountPaidPenalite = new BigDecimal(0);
                    BigDecimal amountPaidTotal = new BigDecimal(0);

                    if (declarationPrincipal != null) {

                        medJsonObj.addProperty("noteTaxationIA", declarationPrincipal.getCodeDeclaration());

                        List<RetraitDeclaration> declarationPenaliteList = new ArrayList<>();

                        declarationPenaliteList = AssujettissementBusiness.getListNoteTaxationPenaliteByPrincipal(declarationPrincipal.getId());

                        if (!declarationPenaliteList.isEmpty()) {

                            medJsonObj.addProperty("ntPenaliteExiste", GeneralConst.Number.ONE);

                            for (RetraitDeclaration declarationPenalite : declarationPenaliteList) {

                                Journal journalNTPenalite = AcquitLiberatoireBusiness.getJournalByDocumentApure(declarationPenalite.getCodeDeclaration());

                                if (journalNTPenalite != null) {
                                    amountPaidPenalite = amountPaidPenalite.add(journalNTPenalite.getMontant());
                                }
                            }
                        } else {
                            medJsonObj.addProperty("ntPenaliteExiste", GeneralConst.Number.ZERO);
                        }
                    } else {
                        medJsonObj.addProperty("noteTaxationIA", "");
                    }

                    BigDecimal amountDu = new BigDecimal(0);
                    float resteARecouvre = 0;

                    amountDu = amountDu.add(med.getMontant());
                    amountDu = amountDu.add(med.getMontantPenalite());

                    Journal journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(declarationPrincipal.getCodeDeclaration());

                    if (journal != null) {

                        amountPaidTotal = amountPaidTotal.add(journal.getMontant());
                        amountPaidTotal = amountPaidTotal.add(amountPaidPenalite);

                        if (amountDu.floatValue() > amountPaidTotal.floatValue()) {

                            resteARecouvre = amountDu.floatValue() - amountPaidTotal.floatValue();

                            medJsonObj.addProperty("estPayer", "-1");

                        } else {
                            medJsonObj.addProperty("estPayer", "1");
                        }
                    } else {
                        medJsonObj.addProperty("estPayer", GeneralConst.Number.ZERO);
                        resteARecouvre = amountDu.floatValue();
                    }

                    medJsonObj.addProperty("resteARecouvre", resteARecouvre);

                    medJsonObj.addProperty("amountPenaliteMed", med.getMontantPenalite());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_MED, med.getMontant());

                    RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(
                            med.getPeriodeDeclaration() + "");

                    medJsonObj.addProperty(PoursuiteConst.ParamName.DEVISE_MED, retraitDeclaration == null ? GeneralConst.EMPTY_STRING : retraitDeclaration.getDevise());

                    medJsonObj.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS,
                            GeneralConst.Number.ONE);

                    if (med.getDateEcheance() != null) {

                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_MED,
                                ConvertDate.formatDateToString(med.getDateEcheance()));

                        if (new Date().after(med.getDateEcheance())) {

                            if (fpcExist) {
                                medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                        GeneralConst.Number.ZERO);
                            } else {
                                medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                        GeneralConst.Number.ONE);
                            }

                        } else {
                            medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ZERO);
                        }

                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_MED,
                                GeneralConst.EMPTY_STRING);
                        medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                GeneralConst.Number.ZERO);
                    }

                    if (med.getDateReception() != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_MED,
                                ConvertDate.formatDateToString(med.getDateReception()));
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_MED,
                                GeneralConst.EMPTY_STRING);
                    }

                    medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_CREATION_MED,
                            ConvertDate.formatDateToString(med.getDateCreat()));

                    agent = GeneralBusiness.getAgentByCode(med.getAgentCreat());

                    if (agent != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.AGENT_CREATION_MED,
                                agent.toString().toUpperCase());
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.AGENT_CREATION_MED,
                                GeneralConst.EMPTY_STRING);
                    }

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(med.getId());

                    if (archive != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT,
                                archive.getDocumentString());
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT,
                                GeneralConst.EMPTY_STRING);
                    }

                    periodeDeclaration = DeclarationBusiness.getPeriodeDeclarationById(
                            med.getPeriodeDeclaration() + "");

                    if (periodeDeclaration != null) {

                        String periodeName = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(),
                                periodeDeclaration.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode());

                        medJsonObj.addProperty("referenceID", periodeDeclaration.getId());
                        medJsonObj.addProperty("referenceName", periodeName);

                        if (periodeDeclaration.getAssujetissement() != null) {

                            articleBudgetaire = periodeDeclaration.getAssujetissement().getArticleBudgetaire();

                            medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                    articleBudgetaire.getIntitule().toUpperCase());

                        } else {
                            medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                    GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        medJsonObj.addProperty("isRecidiviste", GeneralConst.Numeric.ZERO);
                        medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                GeneralConst.EMPTY_STRING);
                    }

                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                    if (personne != null) {

                        /*typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                         personne.getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));*/
                        assujettiName = "Noms : ".concat("<span style='font-weight:bold'>".concat(
                                personne.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                        if (adresse != null) {

                            adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                        }

                        //assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);
                        assujettiNameComposite = assujettiName.concat("<br/><br/>").concat(adresseAssujetti);

                        medJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                assujettiNameComposite);

                        medJsonObj.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : personne.getLoginWeb().getUsername());
                    }

                    medJsonList.add(medJsonObj);
                }

                dataReturn = medJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getInvitationServicePaiement(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;
        String codeStateMiseEnDemeure = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);
            codeStateMiseEnDemeure = request.getParameter(PoursuiteConst.ParamName.CODE_STATE_MISE_EN_DEMEURE);
            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);

            medList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    medList = PoursuiteBusiness.getListMedISBySimpleSearch(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    medList = PoursuiteBusiness.getListInvitationServicePaiementByAdvancedSearch(codeService,
                            codeStateMiseEnDemeure, dateDebut, dateFin);

                    break;
            }

            if (!medList.isEmpty()) {

                List<JsonObject> medJsonList = new ArrayList<>();

                for (Med med : medList) {

                    JsonObject medJsonObj = new JsonObject();
                    Agent agent = new Agent();
                    Archive archive = new Archive();
                    PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();

                    medJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_MED, med.getId());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.EXERCICE_MED, med.getExerciceFiscal());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.TYPE_MED, med.getTypeMed());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_DOCUMENT,
                            med.getNumeroDocument() != null ? med.getNumeroDocument() : GeneralConst.EMPTY_STRING);
                    medJsonObj.addProperty(PoursuiteConst.ParamName.REFERENCE_DOCUMENT, med.getNotePerception());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_CODE, med.getPersonne());

                    personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());

                    DetailsRole detailsRole = PoursuiteBusiness.getDetailRoleByMed(med.getId());

                    boolean fpcExist = false;

                    if (detailsRole != null) {
                        medJsonObj.addProperty("roleExist", GeneralConst.Number.ONE);
                        fpcExist = true;
                        medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVE_ROLE, GeneralConst.Number.ZERO);

                    } else {
                        medJsonObj.addProperty("roleExist", GeneralConst.Number.ZERO);

                        if (new Date().after(med.getDateEcheance())) {
                            medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVE_ROLE, GeneralConst.Number.ONE);
                        } else {
                            medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVE_ROLE, GeneralConst.Number.ZERO);
                        }

                    }

                    Journal journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(med.getNotePerception());
                    BigDecimal somMed = med.getMontant().add(med.getMontantPenalite());

                    if (journal != null) {

                        if (somMed.compareTo(journal.getMontantApurre()) > 0) {
                            NotePerception np = NotePerceptionBusiness.getNotePerceptionByCode(journal.getDocumentApure());
                            List<NotePerception> npFilleList = np.getNotePerceptionList();

                            if (!npFilleList.isEmpty()) {
                                BigDecimal totalAmountPenalitePaid = new BigDecimal(0);
                                BigDecimal totalAmountPenaliteDu = new BigDecimal(0);
                                for (NotePerception npFille : npFilleList) {
                                    if (npFille.getEtat() == GeneralConst.Numeric.THREE) {
                                        totalAmountPenaliteDu = totalAmountPenaliteDu.add(npFille.getNetAPayer());
                                        journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(npFille.getNumero());
                                        BigDecimal amountPaid = BigDecimal.valueOf(0);
                                        if (journal != null) {
                                            amountPaid = journal.getMontantApurre();
                                        }
                                        totalAmountPenalitePaid = totalAmountPenalitePaid.add(amountPaid);
                                    }
                                }
                                if (totalAmountPenaliteDu.compareTo(totalAmountPenalitePaid) == 0) {
                                    medJsonObj.addProperty("estPayer", GeneralConst.Number.ONE);
                                } else {
                                    medJsonObj.addProperty("estPayer", GeneralConst.ResultCode.EXCEPTION_OPERATION);
                                }
                            }
                        }

                    } else {
                        medJsonObj.addProperty("estPayer", GeneralConst.Number.ZERO);
                    }

                    medJsonObj.addProperty("amountPenaliteMed", med.getMontantPenalite());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_MED, med.getMontant());

                    NotePerception notePerception = PaiementBusiness.getNotePerceptionByCode(med.getNotePerception());

                    medJsonObj.addProperty(PoursuiteConst.ParamName.DEVISE_MED, notePerception == null ? GeneralConst.EMPTY_STRING : notePerception.getDevise());

                    medJsonObj.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS,
                            GeneralConst.Number.ONE);

                    /*Amr amr = PoursuiteBusiness.getAmrByMedAll(med.getId());
                     if (amr != null) {
                     if (amr.getDateEcheance() != null) {
                     if (new Date().after(amr.getDateEcheance())) {
                     medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVE_ROLE, GeneralConst.Number.ONE);

                     } else {
                     medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVE_ROLE, GeneralConst.Number.ZERO);
                     }
                     } else {
                     medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVE_ROLE, GeneralConst.Number.ZERO);
                     }

                     } else {
                     medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVE_ROLE, GeneralConst.Number.ZERO);
                     }*/
                    if (med.getDateEcheance() != null) {

                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_MED,
                                ConvertDate.formatDateToString(med.getDateEcheance()));

                        if (new Date().after(med.getDateEcheance())) {

                            if (fpcExist) {
                                medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                        GeneralConst.Number.ZERO);
                            } else {
                                medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                        GeneralConst.Number.ONE);
                            }

                        } else {
                            medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ZERO);
                        }

                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_MED,
                                GeneralConst.EMPTY_STRING);
                        medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                GeneralConst.Number.ZERO);
                    }

                    if (med.getDateReception() != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_MED,
                                ConvertDate.formatDateToString(med.getDateReception()));
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_MED,
                                GeneralConst.EMPTY_STRING);
                    }

                    medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_CREATION_MED,
                            ConvertDate.formatDateToString(med.getDateCreat()));

                    agent = GeneralBusiness.getAgentByCode(med.getAgentCreat());

                    if (agent != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.AGENT_CREATION_MED,
                                agent.toString().toUpperCase());
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.AGENT_CREATION_MED,
                                GeneralConst.EMPTY_STRING);
                    }

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(med.getId());

                    if (archive != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT,
                                archive.getDocumentString());
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT,
                                GeneralConst.EMPTY_STRING);
                    }

                    medJsonObj.addProperty("referenceID", med.getNotePerception());
                    medJsonObj.addProperty("referenceName", med.getNotePerception());

                    if (notePerception != null) {

                        medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                Tools.getArticleByNC(notePerception.getNoteCalcul()));

                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                GeneralConst.EMPTY_STRING);
                    }

                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                    if (personne != null) {

                        typeAssujetti = "NTD : ".concat("<span style='font-weight:bold'>".concat(
                                personne.getLoginWeb().getUsername().toUpperCase()).concat("</span>"));

                        assujettiName = "NOMS : ".concat("<span style='font-weight:bold'>".concat(
                                personne.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                        if (adresse != null) {

                            adresseAssujetti = "ADRESSE : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                        }

                        assujettiNameComposite = typeAssujetti.concat("<br/><br/>").concat(adresseAssujetti).concat("<br/><br/>").concat(assujettiName);
                        //assujettiNameComposite = assujettiName.concat("<br/><br/>").concat(adresseAssujetti);

                        medJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                assujettiNameComposite);

                        medJsonObj.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : personne.getLoginWeb().getUsername());
                    }

                    medJsonList.add(medJsonObj);
                }

                dataReturn = medJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getInvitationService(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;
        String codeStateMiseEnDemeure = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);
            codeStateMiseEnDemeure = request.getParameter(PoursuiteConst.ParamName.CODE_STATE_MISE_EN_DEMEURE);
            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);

            medList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    medList = PoursuiteBusiness.getListMedBySimpleSearchService(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    medList = PoursuiteBusiness.getListInvitationApayerByAdvancedSearchService(codeService,
                            codeStateMiseEnDemeure, dateDebut, dateFin);

                    break;
            }

            if (!medList.isEmpty()) {

                List<JsonObject> medJsonList = new ArrayList<>();

                for (Med med : medList) {

                    JsonObject medJsonObj = new JsonObject();
                    Agent agent = new Agent();
                    Archive archive = new Archive();
                    PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();

                    medJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_MED, med.getId());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.EXERCICE_MED, med.getExerciceFiscal());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.TYPE_MED, med.getTypeMed());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_DOCUMENT,
                            med.getNumeroDocument() != null ? med.getNumeroDocument() : GeneralConst.EMPTY_STRING);

                    personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());

                    Amr amr = PoursuiteBusiness.getAmrByMed(med.getId());

                    boolean fpcExist = false;

                    if (amr != null) {
                        medJsonObj.addProperty("amrExist", GeneralConst.Number.ONE);
                        fpcExist = true;
                    } else {
                        medJsonObj.addProperty("amrExist", GeneralConst.Number.ZERO);
                    }

                    Journal journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(med.getNotePerception());
                    BigDecimal somMed = med.getMontant().add(med.getMontantPenalite());

                    if (journal != null) {

                        if (somMed.compareTo(journal.getMontantApurre()) > 0) {
                            NotePerception np = NotePerceptionBusiness.getNotePerceptionByCode(journal.getDocumentApure());
                            List<NotePerception> npFilleList = np.getNotePerceptionList();

                            if (!npFilleList.isEmpty()) {
                                BigDecimal totalAmountPenalitePaid = new BigDecimal(0);
                                BigDecimal totalAmountPenaliteDu = new BigDecimal(0);
                                for (NotePerception npFille : npFilleList) {
                                    if (npFille.getEtat() == GeneralConst.Numeric.THREE) {
                                        totalAmountPenaliteDu = totalAmountPenaliteDu.add(npFille.getNetAPayer());
                                        journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(npFille.getNumero());
                                        BigDecimal amountPaid = BigDecimal.valueOf(0);
                                        if (journal != null) {
                                            amountPaid = journal.getMontantApurre();
                                        }
                                        totalAmountPenalitePaid = totalAmountPenalitePaid.add(amountPaid);
                                    }
                                }
                                if (totalAmountPenaliteDu.compareTo(totalAmountPenalitePaid) == 0) {
                                    medJsonObj.addProperty("estPayer", GeneralConst.Number.ONE);
                                } else {
                                    medJsonObj.addProperty("estPayer", GeneralConst.ResultCode.EXCEPTION_OPERATION);
                                }
                            }
                        }

                    } else {
                        medJsonObj.addProperty("estPayer", GeneralConst.Number.ZERO);
                    }

                    medJsonObj.addProperty("amountPenaliteMed", med.getMontantPenalite());
                    medJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_MED, med.getMontant());

                    RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(
                            med.getPeriodeDeclaration() + "");

                    medJsonObj.addProperty(PoursuiteConst.ParamName.DEVISE_MED, retraitDeclaration == null ? "CDF" : retraitDeclaration.getDevise());

                    medJsonObj.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS,
                            GeneralConst.Number.ONE);

                    if (med.getDateEcheance() != null) {

                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_MED,
                                ConvertDate.formatDateToString(med.getDateEcheance()));

                        if (new Date().after(med.getDateEcheance())) {

                            if (fpcExist) {
                                medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                        GeneralConst.Number.ZERO);
                            } else {
                                medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                        GeneralConst.Number.ONE);
                            }

                        } else {
                            medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ZERO);
                        }

                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_ECHEANCE_MED,
                                GeneralConst.EMPTY_STRING);
                        medJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                GeneralConst.Number.ZERO);
                    }

                    if (med.getDateReception() != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_MED,
                                ConvertDate.formatDateToString(med.getDateReception()));
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_MED,
                                GeneralConst.EMPTY_STRING);
                    }

                    medJsonObj.addProperty(PoursuiteConst.ParamName.DATE_CREATION_MED,
                            ConvertDate.formatDateToString(med.getDateCreat()));

                    agent = GeneralBusiness.getAgentByCode(med.getAgentCreat());

                    if (agent != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.AGENT_CREATION_MED,
                                agent.toString().toUpperCase());
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.AGENT_CREATION_MED,
                                GeneralConst.EMPTY_STRING);
                    }

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(med.getId());

                    if (archive != null) {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT,
                                archive.getDocumentString());
                    } else {
                        medJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT,
                                GeneralConst.EMPTY_STRING);
                    }

                    periodeDeclaration = DeclarationBusiness.getPeriodeDeclarationById(
                            med.getPeriodeDeclaration() + "");

                    if (periodeDeclaration != null) {

                        String periodeName = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(),
                                periodeDeclaration.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode());

                        medJsonObj.addProperty("referenceID", periodeDeclaration.getId());
                        medJsonObj.addProperty("referenceName", periodeName);

                        if (periodeDeclaration.getAssujetissement() != null) {

                            articleBudgetaire = periodeDeclaration.getAssujetissement().getArticleBudgetaire();

                            medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                    articleBudgetaire.getIntitule().toUpperCase());

                        } else {
                            medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                    GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        medJsonObj.addProperty("isRecidiviste", GeneralConst.Numeric.ZERO);
                        medJsonObj.addProperty(PoursuiteConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                GeneralConst.EMPTY_STRING);
                    }

                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                    if (personne != null) {

                        /*typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                         personne.getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));*/
                        assujettiName = "Noms : ".concat("<span style='font-weight:bold'>".concat(
                                personne.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                        if (adresse != null) {

                            adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                        }

                        //assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);
                        assujettiNameComposite = assujettiName.concat("<br/><br/>").concat(adresseAssujetti);

                        medJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                assujettiNameComposite);

                        medJsonObj.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : personne.getLoginWeb().getUsername());
                    }

                    medJsonList.add(medJsonObj);
                }

                dataReturn = medJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
