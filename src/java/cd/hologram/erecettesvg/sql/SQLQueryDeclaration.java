/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.sql;

/**
 *
 * @author WILLY
 */
public class SQLQueryDeclaration {

    public static String SELECT_CHECK_DECLARATION_TWO = "SELECT * FROM T_DEPOT_DECLARATION WITH (READPAST) WHERE  NUMERO_DEPOT = ?1";
    public static String SELECT_DEPOT_DECLARATION_BY_NUMERO_BORDEREAU = "SELECT * FROM T_DEPOT_DECLARATION WITH (READPAST) WHERE BORDEREAU"
            + " =(SELECT CODE FROM T_BORDEREAU WITH (READPAST) WHERE NUMERO_BORDEREAU = ?1)";

    public static String SELECT_BORDEREAU_BY_NUMERO = "SELECT * FROM T_BORDEREAU WITH (READPAST) WHERE  NUMERO_BORDEREAU = ?1";

    public static String SELECT_BORDEREAU_BY_DECLARATION = "SELECT * FROM T_BORDEREAU WITH (READPAST) WHERE  NUMERO_DECLARATION = ?1 OR NUMERO_BORDEREAU = ?1";
    public static String SELECT_BORDEREAU_BY_CODE = "SELECT * FROM T_BORDEREAU WITH (READPAST) WHERE  CODE = ?1";
    public static String SELECT_DETAIL_BORDEREAU_BY_PERIODE = "SELECT * FROM T_DETAIL_BORDEREAU WITH (READPAST) WHERE  PERIODICITE = ?1";

    public static String SELECT_ARTICLE_BUDGETAIRE_BY_CODE = "SELECT * FROM T_ARTICLE_BUDGETAIRE WITH (READPAST) WHERE CODE = ?1";

    public static String SELECT_PESONNE_BY_NUMERO_DECLARATION = "SELECT * FROM T_PERSONNE WITH (READPAST) WHERE CODE"
            + "=(SELECT PERSONNE FROM T_DEPOT_DECLARATION WHERE NUMERO_DEPOT = ?1) AND ETAT = 1";

    public static String SELECT_RETRAIT_DECLARATION_BY_NUMERO = "SELECT D.* FROM T_RETRAIT_DECLARATION D WITH (READPAST)"
            + " WHERE D.CODE_DECLARATION = (SELECT TOP 1 NUMERO_DECLARATION FROM T_BORDEREAU B WITH (READPAST) "
            + " WHERE (B.NUMERO_DECLARATION = ?1 OR B.NUMERO_BORDEREAU = ?1) AND B.ETAT = 2)";

    public static String SELECT_PERSONNE_BY_CODE = "SELECT * FROM T_PERSONNE WITH (READPAST) WHERE CODE = ?1 AND ETAT = 1";

    public static String SELECT_DEPOT_DECLARATION_BY_CODE = "SELECT * FROM T_DEPOT_DECLARATION WITH (READPAST) WHERE BORDEREAU = ?1";

    public static String SELECT_RETRAIT_DECLARATION_BY_ID = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ID = ?1";

    public static String SAVE_DEPOT_DECLARATION = "P_NEW_DEPOT_DECLARATION";

    public static String F_NEW_DETAILS_RECLAMATION = ":EXEC P_NEW_DETAILS_DEPOT_DECLARATION '%s',?1,?2,?3,?4,?5,?6";

    public static String P_NEW_NOTE_CALCUL_TO_ALL = ":EXEC P_NEW_NOTE_CALCUL_TO_ALL '%s',?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14,?15,?16,?17,?18,?19,?20,?21";

    public static String ARCHIVE_ACCUSER_RECEPTION = ":EXEC F_NEW_ARCHIVE_ACCUSE_RECEPTION '%s',?1,?2,?3,?4";

    public static String SELECT_ASSUJETTI_BY_PERIODE = "SELECT * FROM T_ASSUJETI WITH (READPAST) WHERE ID"
            + "=(SELECT ASSUJETISSEMENT FROM T_PERIODE_DECLARATION WHERE ID = ?1) AND ETAT = 1";

    public static String SELECT_DEPOT_DECLARATION_BY_NUM_DEPOT = "SELECT * FROM T_DEPOT_DECLARATION WITH (READPAST) WHERE NUMERO_DEPOT = ?1 AND ETAT = 1";
    public static String SELECT_DEPOT_DECLARATION_BY_ARTICLE = "SELECT * FROM T_DEPOT_DECLARATION WITH (READPAST) WHERE NUMERO_DEPOT = ?1 AND ETAT = 1";

    public static String SELECT_DEPOT_DECLARATION_BY_BORDEREAU = "SELECT * FROM T_DEPOT_DECLARATION WITH (READPAST) WHERE BORDEREAU = ?1 AND ETAT = 1";
    public static String SELECT_LOGINWEB_PERSONNE = "SELECT * FROM T_LOGIN_WEB WITH (READPAST) WHERE FK_PERSONNE = ?1 AND ETAT = 1";

    public static String SELECT_PERIODE_DECLARATION_BY_ID = "SELECT * FROM T_PERIODE_DECLARATION WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";

    public static String UPDATE_DECLARATION_PRINT = ":UPDATE T_DEPOT_DECLARATION SET NBR_IMPRESSION = 1 "
            + " WHERE CODE = ?1  ";

    public static String SELECT_TAUX_APPLIQUER = "SELECT TOP (1) * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 "
            + " AND TYPE_PERSONNE = ?2 AND TARIF = ?3 AND ETAT = 1";

    public static String SELECT_TAUX_APPLIQUER_ALL = "SELECT TOP(1) * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 "
            + "  AND TARIF = ?2 AND ETAT = 1";

    public static String SELECT_ARTICLE_BUDGETAIRE_ASSUJETTISABLE = "SELECT DISTINCT AB.* FROM T_ARTICLE_BUDGETAIRE AB WITH (READPAST) "
            + "INNER JOIN T_DETAIL_DEPOT_DECLARATION DD WITH (READPAST) ON DD.ARTICLE_BUDGETAIRE = AB.CODE "
            + "WHERE  AB.ASSUJETISSABLE = 1 AND AB.ETAT = 1 AND DD.ETAT = 1 %s";

    public static String LIST_ARTICLE_BUDGETAIRE_ASSUJETISSABLE_BY_SERVICE = "SELECT AB.* FROM T_ARTICLE_BUDGETAIRE AB WITH (READPAST)"
            + " WHERE AB.ASSUJETISSABLE = 1 AND AB.ARTICLE_GENERIQUE IN (SELECT CODE FROM T_ARTICLE_GENERIQUE AG WITH (READPAST)"
            + " WHERE AG.SERVICE_ASSIETTE = ?1)";

    public static String SELECT_DEPOT_DECLARATION_BY_ARTICLE_AND_PERIODE = " SELECT DD.* FROM T_DEPOT_DECLARATION DD WITH (READPAST)"
            + " WHERE DD.ETAT = 1 AND DD.CODE IN (SELECT DDT.DEPOT_DECLARATION "
            + " FROM T_DETAIL_DEPOT_DECLARATION DDT WITH (READPAST) "
            + " WHERE DDT.ARTICLE_BUDGETAIRE = ?1 AND DDT.FK_PERIODE IN "
            + " (SELECT PD.ID FROM T_PERIODE_DECLARATION PD WITH (READPAST)"
            + " WHERE PD.ASSUJETISSEMENT IN ( SELECT ASS.ID FROM T_ASSUJETI ASS WITH (READPAST)"
            + " WHERE ASS.PERIODICITE = ?2)))";

    public static class SecondaryQuery {

        public static String SQL_QUERY_SECONDARY_ONE_REGISTER_DEPOT_DECLARATION = " AND DBO.F_GET_PERSONNE_NAME(DDCL.PERSONNE) LIKE ?1";
        public static String SQL_QUERY_SECONDARY_TWO_REGISTER_DEPOT_DECLARATION = " AND DDCL.NUMERO_DEPOT = ?1";
        public static String SQL_QUERY_SECONDARY_FOURT_REGISTER_DEPOT_DECLARATION = " AND DDCL.SERVICE = '%s'";

        public static String SQL_QUERY_SECONDARY_ONE_DEPOT_DECLARATION_ADVANCED = " AND DBO.FDATE(DDCL.DATE_CREAT) BETWEEN '%s' AND '%s'";
        public static String SQL_QUERY_SECONDARY_THREE_DEPOT_DECLARATION_ADVANCED = " AND DDCL.SITE = '%s' AND DBO.FDATE(DDCL.DATE_CREAT) "
                + " BETWEEN '%s' AND '%s'";
        public static String SQL_QUERY_SECONDARY_TWO_DEPOT_DECLARATION_ADVANCED = " AND DDCL.SERVICE = '%s'";

        public static String SQL_QUERY_SECONDARY_ONE_REGISTER_RETRAIT_DECLARATION = " AND DBO.F_GET_PERSONNE_NAME(RDCL.FK_ASSUJETTI) LIKE ?1";
        public static String SQL_QUERY_SECONDARY_TWO_REGISTER_RETRAIT_DECLARATION = " AND RDCL.CODE_DECLARATION = ?1";

        public static String SQL_QUERY_SECONDARY_ONE_RETRAIT_DECLARATION_ADVANCED = " AND DBO.FDATE(RDCL.DATE_CREATE) BETWEEN '%s' AND '%s'";

    }

    public static class MasterQuery {

        public static String SELECT_MASTER_REGISTER_DEPOT_DECLARATION = "SELECT DDCL.* FROM T_DEPOT_DECLARATION DDCL WITH (READPAST) "
                + " WHERE DDCL.ETAT = 1  %s %s %s ORDER BY DDCL.DATE_CREAT DESC,DDCL.CODE DESC";

        public static String SELECT_MASTER_REGISTER_DEPOT_DECLARATION_ADVANCED_2 = "SELECT DDCL.* FROM T_DEPOT_DECLARATION DDCL WITH (READPAST) "
                + " %s %s %s ORDER BY DDCL.NUMERO_DEPOT";

        public static String SELECT_MASTER_REGISTER_RETRAIT_DECLARATION = "SELECT RDCL.* FROM T_RETRAIT_DECLARATION RDCL WITH (READPAST) JOIN T_PERSONNE P WITH (READPAST) ON RDCL.FK_ASSUJETTI = P.CODE"
                + " WHERE RDCL.ETAT IN (1,2) AND CAST(RDCL.ID as varchar(10)) IN (select a.REF_DOCUMENT from T_ARCHIVE a WITH (READPAST)) %s %s %s ORDER BY P.PRENOMS,P.NOM,P.POSTNOM";

        public static String SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_VIGNETTE = "SELECT RDCL.* FROM T_RETRAIT_DECLARATION RDCL WITH (READPAST) JOIN T_PERSONNE P WITH (READPAST) ON RDCL.FK_ASSUJETTI = P.CODE"
                + " WHERE RDCL.ETAT IN (1,2,5,6) AND RDCL.FK_AB = '00000000000002302020'  AND CAST(RDCL.ID as varchar(10)) IN (select a.REF_DOCUMENT from T_ARCHIVE a WITH (READPAST)) %s %s %s ORDER BY P.PRENOMS,P.NOM,P.POSTNOM";

        public static String SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_DEFAILLANT = "SELECT RDCL.* FROM T_RETRAIT_DECLARATION RDCL WITH (READPAST) JOIN T_PERSONNE P WITH (READPAST) ON RDCL.FK_ASSUJETTI = P.CODE"
                + " WHERE RDCL.ETAT IN (1,3,5,6) AND RDCL.CODE_DECLARATION NOT IN (SELECT NUMERO_DECLARATION FROM T_BORDEREAU WITH (READPAST) WHERE  ETAT = 2)"
                + " AND CONVERT(DATE,RDCL.DATE_ECHEANCE_PAIEMENT) < CONVERT(DATE,GETDATE()) AND RDCL.EST_PENALISE = 1 AND CAST(RDCL.ID as varchar(10)) IN (select a.REF_DOCUMENT from T_ARCHIVE a WITH (READPAST)) %s %s %s ORDER BY P.PRENOMS,P.NOM,P.POSTNOM";

        public static String SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_DEFAILLANT_PAID_RETARD = "SELECT RDCL.* FROM T_RETRAIT_DECLARATION RDCL WITH (READPAST) JOIN T_PERSONNE P WITH (READPAST) ON RDCL.FK_ASSUJETTI = P.CODE"
                + " WHERE RDCL.ETAT IN (1,3,5,6) AND RDCL.CODE_DECLARATION IN (SELECT b.NUMERO_DECLARATION FROM T_BORDEREAU b WITH (READPAST) join T_RETRAIT_DECLARATION r on b.NUMERO_DECLARATION = r.CODE_DECLARATION \n"
                + " WHERE  b.ETAT = 2 AND CONVERT(DATE,r.DATE_ECHEANCE_PAIEMENT) < CONVERT(DATE,b.DATE_PAIEMENT)) AND RDCL.EST_PENALISE = 1 %s %s %s ORDER BY P.PRENOMS,P.NOM,P.POSTNOM";

        public static String SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_ADVANCED_2 = "SELECT RDCL.* FROM T_RETRAIT_DECLARATION RDCL WITH (READPAST) JOIN T_PERSONNE P WITH (READPAST) ON RDCL.FK_ASSUJETTI = P.CODE WHERE RDCL.ETAT = 1"
                + " %s %s %s %s ORDER BY P.PRENOMS,P.NOM,P.POSTNOM";

        public static String SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_DEFAILLANT_ADVANCED_2 = "SELECT RDCL.* FROM T_RETRAIT_DECLARATION RDCL WITH (READPAST) JOIN T_PERSONNE P WITH (READPAST) ON RDCL.FK_ASSUJETTI = P.CODE WHERE RDCL.ETAT IN (1,3,5,6)"
                + " AND RDCL.CODE_DECLARATION NOT IN (SELECT NUMERO_DECLARATION FROM T_BORDEREAU WITH (READPAST) WHERE  ETAT = 2)"
                + " AND CONVERT(DATE,RDCL.DATE_ECHEANCE_PAIEMENT) < CONVERT(DATE,GETDATE()) AND RDCL.RETRAIT_DECLARATION_MERE IS NULL AND CAST(RDCL.ID as varchar(10)) IN (select a.REF_DOCUMENT from T_ARCHIVE a WITH (READPAST)) %s %s %s %s ORDER BY P.PRENOMS,P.NOM,P.POSTNOM";

        public static String SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_DEFAILLANT_ADVANCED_3 = "SELECT RDCL.* FROM T_RETRAIT_DECLARATION RDCL WITH (READPAST) JOIN T_PERSONNE P WITH (READPAST) ON RDCL.FK_ASSUJETTI = P.CODE WHERE RDCL.ETAT IN (1,3,5,6)\n"
                + " AND RDCL.CODE_DECLARATION IN (SELECT b.NUMERO_DECLARATION FROM T_BORDEREAU b WITH (READPAST) join T_RETRAIT_DECLARATION r on b.NUMERO_DECLARATION = r.CODE_DECLARATION \n"
                + " WHERE  b.ETAT = 2 AND CONVERT(DATE,r.DATE_ECHEANCE_PAIEMENT) < CONVERT(DATE,b.DATE_PAIEMENT)) AND RDCL.RETRAIT_DECLARATION_MERE IS NULL %s %s %s %s ORDER BY P.PRENOMS,P.NOM,P.POSTNOM";

        public static String SELECT_MASTER_RETRAIT_DECLARATION_FROM_ADVANCED_SEARCH = "SELECT * FROM T_RETRAIT_DECLARATION WHERE RETRAIT_DECLARATION_MERE IS NULL "
                + "AND FK_AB = ?1 AND FK_SITE = ?2 AND FK_PERIODE IN (SELECT ID FROM T_PERIODE_DECLARATION WHERE DATEPART(YEAR,DEBUT) = ?3 %s %s)";

        public static String SELECT_MASTER_RETRAIT_DECLARATION_FROM_ADVANCED_SEARCH_V2 = "SELECT * FROM T_RETRAIT_DECLARATION WHERE FK_AB = ?1 "
                + "AND FK_SITE = ?2 AND FK_PERIODE IN (SELECT ID FROM T_PERIODE_DECLARATION WHERE DATEPART(YEAR,DEBUT) = ?3 %s %s "
                + "AND DBO.FDATE(DATE_CREATE) BETWEEN ?4 AND ?5)";

        public static String SELECT_MASTER_RETRAIT_DECLARATION_FROM_ADVANCED_SEARCH_V3 = "SELECT RD.* FROM T_RETRAIT_DECLARATION RD WITH (READPAST) WHERE RD.ETAT IN (1,2,5,6) AND RD.FK_SITE = ?1 AND CAST(RD.ID as varchar(10)) IN (select a.REF_DOCUMENT from T_ARCHIVE a WITH (READPAST)) %s %s %s %s %s";

        public static String SELECT_RETRAIT_DECLARATION__ADVANCED_SEARCH_PART2 = "AND DATEPART(MONTH,DEBUT) = %s";

        public static String SELECT_MASTER_DEPOT_DECLARATION_FROM_ADVANCED_SEARCH = "SELECT * FROM T_DEPOT_DECLARATION WITH (READPAST) WHERE NUMERO_DEPOT IN("
                + "SELECT CODE_DECLARATION FROM T_RETRAIT_DECLARATION WHERE RETRAIT_DECLARATION_MERE IS NULL AND FK_AB = ?1 AND FK_SITE = ?2 AND FK_PERIODE IN("
                + "SELECT ID FROM T_PERIODE_DECLARATION WHERE DATEPART(YEAR,DEBUT) = ?3 %s %s))";

        public static String SELECT_DEPOT_DECLARATION__ADVANCED_SEARCH_PART2 = "AND DATEPART(MONTH,DEBUT) = ?4";
    }

}
