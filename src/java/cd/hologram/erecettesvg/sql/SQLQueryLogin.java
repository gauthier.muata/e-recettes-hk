/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.sql;

/**
 *
 * @author gauthier.muata
 */
public class SQLQueryLogin {

    public static String LOGIN_AGENT = "SELECT * FROM T_AGENT WITH (READPAST) WHERE LOGIN = ?1 AND ETAT = 1";

    public static String LIST_ACCESS_USER_SELECT_BY_AGENT = "SELECT a.ID,a.DROIT FROM T_ACCES_USER a WITH (READPAST) "
            + " WHERE a.AGENT = ?1 AND a.ETAT = 1";

    public static String LIST_GROUP_USER_SELECT_BY_AGENT = "SELECT a.ID,a.FK_GU FROM T_AGENT_GU a WITH (READPAST) "
            + " WHERE a.FK_AGENT = ?1 AND a.ETAT = 1";

    public static String LIST_RIGHTS_GROUP_USER_SELECT_BY_GROUP = "SELECT a.ID,a.DROITS FROM T_GU_ACCES a "
            + " WHERE a.GROUPE = ?1 AND a.ETAT = 1 AND a.DROITS NOT IN (SELECT b.DROIT FROM T_ACCES_USER b "
            + " WHERE b.AGENT = ?2 AND b.ETAT = 1)";

    public static String LIST_RIGHTS_USER_SELECT_BY_AGENT = "SELECT a.ID,a.DROIT FROM T_ACCES_USER a WITH (READPAST)"
            + " WHERE a.AGENT = ?1 AND a.ETAT = 1";

    public static String UPDATE_AGENT_PASSWORD = "UPDATE T_AGENT SET MDP = HASHBYTES('MD5','%s'), CHANGE_PWD = 0 WHERE CODE = %s";

    public static String GET_AGENT_BY_CODE = "SELECT * FROM T_AGENT WITH (READPAST) WHERE CODE = ?1 AND ETAT = 1";
    
    public static String UPDATE_AGENT_SIGNATURE = "UPDATE T_AGENT SET SIGNATURE = '%s' WHERE CODE = %s";
    
    public static String AGENT_BY_CODE = "SELECT * FROM T_AGENT WITH (READPAST) WHERE CODE = ?1 AND ETAT = 1";
    
    public static String USER_DIVISION_BY_CODE = "SELECT * FROM T_UTILISATEUR_DIVISION WITH (READPAST) WHERE FK_PERSONNE = ?1 AND ETAT = 1";
}
