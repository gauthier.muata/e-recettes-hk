/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.sql;

/**
 *
 * @author gauthier.muata
 */
public class SQLQueryMed {

    public static String SELECT_MASTER_LIST_MED_VALIDATE = "SELECT M.* FROM T_MED M WITH (READPAST) "
            + " WHERE M.ETAT = 1 %s %s AND M.NOTE_PERCEPTION NOT IN (SELECT ISNULL(NP_MERE,'') FROM T_NOTE_PERCEPTION "
            + " WITH (READPAST) WHERE FRACTIONNEE <> 0 )ORDER BY M.ID DESC";

    public static String SQL_PART_3_BY_LETTRE_NOTIFICATION = " AND M.ID = ?1";

    public static String SQL_PART_3_BY_AB = " AND M.NOTE_CALCUL IN (SELECT DT.NOTE_CALCUL FROM T_DETAILS_NC DT WHERE dt.ARTICLE_BUDGETAIRE = '%s')";

    public static String SQL_PART_2_BY_NOTE_PERCEPTION = " AND M.NOTE_PERCEPTION = ?1";

    public static String SQL_PART_1_BY_ASSUJETTI_NAME = " AND DBO.F_GET_PERSONNE_NAME(M.PERSONNE) LIKE ?1";
    public static String SQL_PART_1_BY_ASSUJETTI_NAME1 = " AND M.PERSONNE = '%s' ";

    public static String SELECT_FICHE_PRISE_CHARGE_BY_NP = "SELECT F.* FROM T_FICHE_PRISE_CHARGE F WITH (READPAST) "
            + " WHERE F.NUMERO_REFERENCE = ?1 AND F.ETAT = 1";

    public static String SELECT_LIST_DETAIL_FPC_BY_FPC = "SELECT * FROM T_DETAIL_FICHE_PRISE_CHARGE WITH (READPAST) "
            + " WHERE FICHE_PRISE_CHARGE = ?1 AND ETAT = 1";

    public static String SQL_PART_SELECT_MED_BY_DATES = " AND DBO.FDATE(M.DATE_CREAT) BETWEEN '%s' AND '%s'";

    public static String EXEC_F_NEW_BON_A_PAYER_FRACTION = ":EXEC F_NEW_BON_A_PAYER_FRACTION ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11";

    public static String EXEC_F_NEW_BON_A_PAYER = ":EXEC F_NEW_BON_A_PAYER ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10";
    public static String EXEC_F_NEW_BON_A_PAYER_V2 = ":EXEC F_NEW_BON_A_PAYER ?1,?2,?3,?4,?5,?6,?7,?8,?9,'%s'";

    public static String EXEC_F_NEW_BON_A_PAYER_INTERCALAIRE = ":EXEC F_NEW_BON_A_PAYER_INTERCALAIRE ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10";
    
    public static String EXEC_F_NEW_RETRAIT_DECLARATION_ECHELONNE = ":EXEC F_NEW_RETRAIT_DECLARATION_ECHELONNE ?1,?2,?3,?4";

    public static String SELECT_LAST_BON_A_PAYE_BY_NC = "SELECT BP.* FROM T_BON_A_PAYER BP WITH (READPAST) "
            + " WHERE BP.FK_NOTE_CALCUL = ?1 AND EST_FRACTION = 0 AND BP.ETAT = 1 ORDER BY BP.CODE DESC";

    public static String SELECT_LAST_BON_A_PAYE_BY_NC_NPMERE = "SELECT BP.* FROM T_BON_A_PAYER BP WITH (READPAST) "
            + " WHERE BP.FK_NOTE_CALCUL = ?1 AND TYPE_BP IN (1,3,4) AND (NP_MERE = ?2 OR NP_MERE IS NULL) ORDER BY BP.CODE DESC";

    public static String SELECT_LAST_BON_A_PAYE_BY_NC_CAS_PENALITE = "SELECT BP.* FROM T_BON_A_PAYER BP WITH (READPAST) "
            + " WHERE BP.FK_NOTE_CALCUL = ?1 AND TYPE_BP IN (1,3,4) AND NP_MERE = ?2 ORDER BY BP.CODE DESC";

    public static String SELECT_NP_BY_NP_MERE = "select * from T_NOTE_PERCEPTION with (readpast) where BP_MERE = ?1";

    public static String SELECT_LIST_BON_A_PAYER = "SELECT B.* FROM T_BON_A_PAYER B WITH (READPAST) "
            + " WHERE %s %s ORDER BY B.DATE_CREAT DESC , B.CODE DESC";

    public static String SQL_PART_1_SELECT_BP_BY_CODE = " B.BP_MANUEL = ?1";

    public static String SQL_PART_2_SELECT_BP_BY_ASSUJETTI_NAME = " AND DBO.F_GET_PERSONNE_NAME(B.FK_PERSONNE) LIKE ?1";
    public static String SQL_PART_2_SELECT_BP_BY_ASSUJETTI_CODE = "B.FK_PERSONNE = '%s' ";

    public static String SQL_PART_2_SELECT_BP_BY_AB = " B.FK_NOTE_CALCUL IN (SELECT NC.NUMERO FROM T_NOTE_CALCUL NC "
            + " WHERE NC.PERSONNE = '%s'";

    public static String SQL_PART_3_SELECT_BP_BY_SITE_SERVICE_CAS1 = " AND B.FK_NOTE_CALCUL IN (SELECT NC.NUMERO "
            + " FROM T_NOTE_CALCUL NC WITH (READPAST) WHERE NC.SITE IN (%s) AND NC.SERVICE IN (%s))";

    public static String SQL_PART_3_SELECT_BP_BY_SITE_SERVICE_CAS4 = " AND B.FK_SITE IN (%s) "
            + " AND B.FK_NOTE_CALCUL IN (SELECT NC.NUMERO FROM T_NOTE_CALCUL NC WITH (READPAST) "
            + " WHERE NC.SERVICE IN (%s)) AND B.AGENT_CREAT IN (%s)";
    
    public static String SQL_PART_3_SELECT_BP_BY_SITE_SERVICE_CAS5 = " AND B.FK_SITE IN (%s) "
            + " AND B.FK_SITE IN (SELECT S.CODE FROM T_SITE S WITH (READPAST) "
            + " WHERE S.FK_SERVICE IN (%s)) AND B.AGENT_CREAT IN (%s) AND B.TYPE_BP IN (5,6)";
    
    public static String SQL_PART_3_SELECT_BP_BY_SITE_SERVICE_CAS3 = " AND M.NOTE_CALCUL IN (SELECT NC.NUMERO "
            + " FROM T_NOTE_CALCUL NC WITH (READPAST) WHERE NC.SITE IN (%s) AND NC.SERVICE IN (%s))";

    public static String SQL_PART_3_SELECT_BP_BY_SITE_SERVICE_CAS2 = " B.FK_NOTE_CALCUL IN (SELECT NC.NUMERO "
            + " FROM T_NOTE_CALCUL NC WITH (READPAST) WHERE NC.SITE IN (%s) AND NC.SERVICE IN (%s))";

    public static String SQL_PART_5_SELECT_BP_BY_DATES = " AND DBO.FDATE(B.DATE_CREAT) BETWEEN '%s' AND '%s'";

    public static String SELECT_LIST_BON_A_PAYER_AVANCED = "SELECT B.* FROM T_BON_A_PAYER B WITH (READPAST) "
            + " WHERE B.ETAT = 1 %s %s ORDER BY B.DATE_CREAT DESC , B.CODE DESC";

    public static String SELECT_LIST_BON_A_PAYER_BY_BP_MERE = "SELECT BP.* FROM T_BON_A_PAYER BP WITH (READPAST) "
            + " WHERE BP.BP_MERE IN (%s)";

    public static String SELECT_BON_A_PAYER_BY_BP_MERE = "SELECT BP.* FROM T_BON_A_PAYER BP WITH (READPAST) "
            + " WHERE BP.BP_MERE = ?1 AND TYPE_BP = 4";

    public static String SELECT_BON_A_PAYER_BY_NP_MERE = "SELECT * FROM T_BON_A_PAYER B WITH (READPAST)"
            + " WHERE B.NP_MERE = ?1";
    
    public static String F_GET_LAST_ID_BON_A_PAYER = "SELECT DBO.F_GET_LAST_ID_BON_A_PAYER()";
}
