/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author gauthier.muata
 */
public class GeneralConst {

    public static String UNIQE = "UNIQUE";
    public static String ID_USER = "idUser";
    public static String EMPTY_STRING = "";
    public static String MULTIPLE_POINT = "................................   ";
    public static String SPACE = " ";
    public static String BRAKET_OPEN = "(";
    public static String BRAKET_CLOSE = ")";
    public static String SEPARATOR_SLASH = " / ";
    public static String KEY_SESSION_AGENT = "erecettevg_agent";
    public static String KEY_SESSION_AGENT_RIGHTS = "erecettevg_agent_rights";
    public static String KEY_SESSION_AGENT_SITES = "erecettevg_agent_sites";
    public static String ALL = "*";
    public static String OPERATION = "operation";
    public static String TEXT_SELECTED_VALUE = "-- Sélectionner --";
    public static String PREFIX_IDENTIFIANT_KEY = "IDENT_";
    public static String DASH_SEPARATOR = "-";
    public static String VIRGULE = ",";
    public static String YES_VALUE = "Oui";
    public static String ADRESS_NOTHING = "Aucune adresse";
    public static String TWO_POINTS = " : ";
    public static String AB_SEPARTOR = "; ";
    public static int EMPTY_ZERO = 0;
    public static String AUCUNE_DONNEE = "Aucune donnée";
    public static String MY_SELF = "Moi-même";
    public static String INTITULE_DATA = "<p style=\"font-weight:bold\">%s</p>";
    public static String TEXT_LOCATION = " <p style=\"color:red\">(Acquis par location)</p>";
    public static String TEXT_MISE_LOCATION = " <p style=\"color:blue\">(Loué par %s)</p>";

    public static final String FORMAT_CONVERT = "%.2f";
    public static String DOT = ".";

    public static final String DATA_IMAGE_JPEG_BASE64_TEXT = "data:image/jpeg;base64,";
    public static final String BASE64_TEXT = "base64";

    public static final String POURCENTAGE_PARTIE_NON_CONTESTER = "0.2";

    public static String TOUT = "TOUT";

    public static String SEPARATOR_SLASH_NO_SPACE = "/";

    public static String NON_DEFINI = "non defini";
    public static final String CENT_POURCENT = "100";

    public static final String FIRST_LETTRE_DEVISE = "d";
    public static final String FIRST_LETTRE_UNITE = "u";

    public class Operation {

        public final static String LOAD_ENTITIES = "00025";
        public final static String CREATE_ENTITY = "1101";
        public final static String UPLOADING_TYPE_DOCUMENT = "uploadingTypeDoc";
        public final static String UPDATE_DATE_ECHEANCE = "updateDateEcheance";
        public final static String LOAD_PENALITE_LIST = "getPenalite";

        public final static String GET_ENTITE_CODE = "getEntiteByCode";
        public final static String GET_ENTITE_BY_MERE = "getEntiteByMere";
        public final static String SET_EXONERATION = "setExoneration";
        public final static String GET_BIENS_EXONERES = "getBiensExoneres";
    }

    public class TypeNPFils {

        public final static String NP_I = "Note intercalaire";
        public final static String NP_P = "Note de pénalité";
        public final static String NP = "Note de principale";
        public final static String NP_BP = "Bon à payer";
        public final static String NP_FPC = "Détails pénalités";
        public final static String NP_P_FR = "Note fractionné";

        public final static String NP_INTERCALAIRES = "INTERCALAIRES";
        public final static String TITRE_ENROLEMENT = "ENROLEMENT";
        public final static String TITRE_AMENDE = "50% AMENDES";
        public final static String TITRE_DECLARATIVE = "DECLARATION TARDIVE";
        public final static String TITRE_INTERET = "INTERET ECHELONNEMENT";
        public final static String FRAIS_DE_POURSUITE = "FRAIS DE POURSUITE";
        public final static String TITRE_FRACTION = "FRACTION";
    }

    public class ResultCode {

        public final static String VALUE_EXIST = "3";
        public final static String SUCCES_OPERATION = "1";
        public final static String FAILED_OPERATION = "0";
        public final static String EXCEPTION_OPERATION = "-1";
        public final static String LOGIN_EMAIL_EXIST = "2";
        public final static String INVALID_PASSWORD = "3";
        public final static String NC_CONFORME_SUCCESS_OPERATION = "OK";
        public final static String NC_NON_CONFORME_SUCCESS_OPERATION = "KO";
        public final static String SERIE_OVER = "5";
        public final static String TITRE_EXIST = "4";
        public final static String DATE_RECEPTION_NON_EXIST = "6";
        public final static String NON_ADMIS_CONTENTIEUX = "9";
        public final static String DECLARATION_NON_EXIST = "-2";
        public final static String DECLARATION_EXIST = "-3";
        public final static String PAIEMENT_NON_EXIST = "-4";
        public final static String PAIEMENT_NON_AUTORISE = "3";
        public final static String TITRE_PERCEPTION_FRACTIONNE = "4";
        public final static String TITRE_PERCEPTION_EN_CONTENTIEUX = "5";
        public final static String PHONE_EXIST = "5";
        public final static String MAIL_EXIST = "6";
        public final static String PERSONNE_NON_EXIST = "2";
        public final static String PASSWORD_NON_CHANGE = "-2";
        public final static String NO_FOUND = "400";
    }

    public class Number {

        public static final String ZERO = "0";
        public static final String ONE = "1";
        public static final String TWO = "2";
        public static final String THREE = "3";
        public static final String FOUR = "4";
        public static final String FIVE = "5";
        public static final String SIX = "6";
        public static final String SEVEN = "7";
        public static final String EIGTH = "8";
        public static final String NINE = "9";
        public static final String TEN = "10";

        public static final String ZERO_FORMAT = ",00";
    }

    public class Format {

        public final static String FORMAT_DATE = "dd/MM/yyyy";
        public final static String FORMAT_DATE_2 = "yyy/MM/dd";
        public final static String FORMAT_IMAGE = "image/png";
        public static final String ZERO_FORMAT = ",00";
        public static final String FORMAT_HTML = "HTML";
        public static final String FORMAT_DATE_TEXT_ATTESTATION = "Le %s jour du mois de %s de l'an %s";
        public static final String FORMAT_DATE_TEXT_ACTE = "L'an %s, le %s jour du mois de %s";
        public static final String FORMAT_DATE_TEXT_ACTE_V2 = "L'an %s, le %s jour du mois d'%s";
        public static final String FORMAT_DATE_TEXT_ATTESTATION_V2 = "Le %s jour du mois d'%s de l'an %s";
        public static final String FORMAT_AMOUNT = "###,###.###";
    }

    public class NumeralString {

        public final static String UN = "Premier";
        public final static String DEUX = "Deuxième";
        public final static String TROIS = "Troisième";
        public final static String QUATRE = "Quatrième";
        public final static String CINQ = "Cinquième";
        public final static String SIX = "Sixième";
        public final static String SEPT = "Septième";
        public final static String HUIT = "Huitième";
        public final static String NEUF = "Neuvième";
        public final static String DIX = "Dixième";
        public final static String ONZE = "Onzième";
        public final static String DOUZE = "Douzième";
        public final static String TREIZE = "Treizième";
        public final static String QUATORZE = "Quatorzième";
        public final static String QUIZE = "Quinzième";
        public final static String SIEZE = "Seizième";
        public final static String DIX_SEPT = "Dix-septième";
        public final static String DIX_HUIT = "Dix-huitième";
        public final static String DIX_NEUF = "Dix-neuvième";
        public final static String VINGT = "Vingtième";
        public final static String VINGT_UN = "Vingt et unième";
        public final static String VINGT_DEUXIEME = "Vingt-deuxième";
        public final static String VINGT_TROIS = "Vingt-troisième";
        public final static String VINGT_QUATRE = "Vingt-quatrième";
        public final static String VINGT_CINQ = "Vingt-cinquième";
        public final static String VINGT_SIX = "Vingt-sixième";
        public final static String VINGT_SEPT = "Vingt-septième";
        public final static String VINGT_HUIT = "Vingt-huitième";
        public final static String VINGT_NEUF = "Vingt-neuvième";
        public final static String TRENTE = "Trentième";
        public final static String TRENTE_UN = "Trente et unième";

    }

    public class Module {

        public final static String MOD_RECLAMATION = "RECLAMATION";
        public final static String MOD_TAXATION = "TAXATION";
        public final static String MOD_ORDONNANCEMENT = "ORDONNANCEMENT";
        public final static String MOD_NOTE_PERCEPTION = "NOTE_PERCEPTION";
        public final static String MOD_ECHELONNEMENT = "ECHELONNEMENT";
        public final static String MOD_MISSION_CONTROLE = "MISSION";
        public final static String MOD_EXONERATION = "EXONERATION";
        public final static String MOD_RECOUVREMENT = "RECOUVREMENT";
    }

    public class ParamName {

        //Site param
        public final static String SITE_CODE = "SiteCode";
        public final static String SITE_NAME = "siteName";
        public final static String SITE_USER_LIST = "siteUserList";
        public final static String SITE_ALL_LIST = "allSiteList";
        public final static String AVIS_LIST = "avisList";
        public final static String PEAGE = "peage";

        //Site param
        public final static String SERVICE_CODE = "serviceCode";
        public final static String SERVICE_NAME = "serviceName";
        public final static String SERVICE_ALL_LIST = "allServiceList";
        //Fonction param
        public final static String FONCTION_CODE = "fonctionCode";
        public final static String FONCTION_NAME = "fonctionName";

        //Banque param
        public final static String BANQUE_CODE = "banqueCode";
        public final static String BANQUE_NAME = "banqueName";
        public final static String BANQUE_USER_LIST = "banqueUserList";

        //Compte param
        public final static String ACCOUNT_CODE = "accountCode";
        public final static String ACCOUNT_NAME = "accountName";
        public final static String ACCOUNT_DEVISE = "acountDevise";
        public final static String ACCOUNT_BANQUE_CODE = "acountBanqueCode";
        public final static String ACCOUNT_BANQUE_NAME = "acountBanqueName";
        public final static String ACCOUNT_LIST = "accountList";

        // Dates param
        public final static String DATE_DEBUT = "dateDebut";
        public final static String DATE_FIN = "dateFin";

        // Taux param
        public final static String TAUX_CODE = "tauxCode";
        public final static String TAUX_DEVIDE_SOURCE = "deviseSource";
        public final static String TAUX_DEVIDE_DEST = "deviseDestination";
        public final static String TAUX_VALUE = "tauxValue";
        public final static String TAUX_LIST = "tauxList";

        //Entite Administrative
        public final static String ENTITE_CODE = "code";
        public final static String ENTITE_INTITULE = "intitule";
        public final static String ENTITE_TYPE = "type";
        public final static String ENTITE_MERE = "mere";

        public final static String INTITULE = "INTITULE";
        public final static String TYPE_ENTITE = "TYPE_ENTITE";
        public final static String MERE_ENTITY = "ENTITE_MERE";
        public final static String AGENT_CREAT = "AGENT_CREAT";

        // Type Document param
        public final static String CODE_TYPE_DOCUMENT = "codeTypeDocument";
        public final static String LIBELLE_TYPE_DOCUMENT = "libelleTypeDocument";
        public final static String COMMUNE_USER = "communeUser";
        public final static String DISTRICT_USER = "districtUser";

        //Observation Param
        public final static String OBSERVATION_CREATE_EXISTS = "obsCreateExists";
        public final static String OBSERVATION_CREATE_VALUES = "obsCreateValues";

        public final static String OBSERVATION_VALIDATE_EXISTS_FIRST_LEVEL = "obsValidateExistsFirstLevel";
        public final static String OBSERVATION_VALIDATE_VALUES_FIRST_LEVEL = "obsValidateFirstLevel";

        public final static String OBSERVATION_VALIDATE_EXISTS_SECOND_LEVEL = "obsValidateExistsSecondLevel";
        public final static String OBSERVATION_VALIDATE_VALUES_SECOND_LEVEL = "obsValidateSecondLevel";

        //Utilisateur Pram
        public final static String LISTE_FONCTION = "listeFonction";
        public final static String AJOUT_FONCTION = "ajoutFonction";
        public final static String MODIF_FONCTION = "modifierFonction";
        public final static String DESACTIVER_FONCTION = "desactiverFonction";
        public final static String ACTIVER_FONCTION = "activerFonction";
        public final static String CODE_FONCTION = "code";
        public final static String INTITULE_FONCTION = "intitule";
        public final static String LISTE_GROUPES = "listeGroupe";

        public final static String LISTE_DROITS = "listeDroits";
        public final static String AJOUT_GROUPE = "ajoutGroupe";
        public final static String MODIF_GROUPE = "modifierGroupe";
        public final static String ACTIVER_GROUPE = "activerGroupe";
        public final static String DESACTIVER_GROUPE = "desactiverGroupe";

        public final static String LISTE_AUTRES_DROITS = "listerAutresDroits";
        public final static String AJOUTER_DROIT_AU_GROUPE = "ajouterDroitAuGroupe";

        public final static String INIT_DATA_USER = "initDataUser";
        public final static String DESACTIVER_MODIF_FONCTIONFONCTION = "desactiverFonction";

        public final static String SAVE_USER = "saveUser";
        public final static String CHECK_LOGIN = "checkLogin";
        public final static String SEARCH_GESTIONNAIRE = "searchGestionnaire";
        public final static String AFFECTER_ASSUJETTI = "affecterAssujetti";
        public final static String DESAFFECTER_ASSUJETTI = "desaffecterAssujetti";
        public final static String LOAD_ASSUJETTI = "loadAssujetti";
        public final static String LOAD_ASSUJETTI_AFFECTER = "loadAssujettiAffecter";
        public final static String LOAD_ARTICLE_OF_ASSUJETTI = "loadArtcleOfAssujetti";
        public final static String GET_DETAIL_ASSUJETTISSEMENT = "getDetailsAssujettissement";
        public final static String SAVE_PREVISION_CREDIT = "savePrevisionCredit";

        public final static String USER_ID = "userId";

        public final static String ARCHIVES = "archives";
        public final static String OBSERVATION = "observation";
        public final static String FK_DOCUMENT = "fkDocument";
        public final static String PV_DOCUMENT = "pvDocument";

        public final static String LOAD_USER = "loadUser";
        public final static String LOAD_INFOS_USER = "loadInfosUser";
        public final static String LOAD_INFOS_USER_BY_SITE = "loadInfosUserBySite";
        public final static String DISABLE_USER = "disabledUser";
        public final static String LOAD_DROIT_AFFECTER_USER = "loadDroitAffecterUser";
        public final static String LOAD_DROITS = "loadDroits";
        public final static String AFFECTER_DROITS = "affecterDroit";
        public final static String RETIRER_DROITS = "retirerDroit";
        public final static String GET_DROITS = "getDroits";
        public final static String LOAD_MODULE = "loadModule";
        public final static String SAVE_DROIT = "saveDroit";

        public final static String CODE_ENTITE = "codeEntite";

        public final static String CODE_AGENT = "codeAgent";
        public final static String CODE_SITE = "codeSite";
        public final static String PROVINCE_CODE = "provinceCode";

        // Penalite Param
        public final static String PENALITE_CODE = "CODE";
        public final static String PENALITE_INTITULE = "INTITULE";
        public final static String PENALITE_TYPE_PENALITE = "TYPE_PENALITE";
        public final static String PENALITE_PALIER = "PALIER";
        public final static String PENALITE_APPLIQUER_MOIS_RETARD = "APPLIQUER_MOIS_RETARD";
        public final static String PENALITE_IS_VISIBLE_UTILISATEUR = "IS_VISIBLE_UTILISATEUR";
        public final static String T_PALIER_TAUX_PENALITE_ID = "ID";
        public final static String T_PALIER_TAUX_PENALITE_PENALITE = "PENALITE";
        public final static String T_PALIER_TAUX_PENALITE_BORNE_INFERIEURE = "BORNE_INFERIEURE";
        public final static String T_PALIER_TAUX_PENALITE_BORNE_SUPERIEURE = "BORNE_SUPERIEURE";
        public final static String T_PALIER_TAUX_PENALITE_FORME_JURIDIQUE = "FORME_JURIDIQUE";
        public final static String T_PALIER_TAUX_PENALITE_RECIDIVE = "RECIDIVE";
        public final static String T_PALIER_TAUX_PENALITE_VALEUR = "VALEUR";
        public final static String T_PALIER_TAUX_PENALITE_TYPE_VALEUR = "TYPE_VALEUR";
        public final static String T_PALIER_TAUX_PENALITE_EST_POURCENTAGE = "EST_POURCENTAGE";
        public final static String LIST_TAUX_PANALIE = "LIST_TAUX_PANALIE";
        public final static String IS_ADVANCED = "isAdvanced";
        public final static String IS_PRINTABLE = "IS_PRINTABLE";

        public final static String CODE_SERVICE = "codeService";
        public final static String CODE_PROVINCE = "codeProvince";
    }

    public class SearchCode {

        public final static String SITE = "SiteCode";
        public final static String SERVICE = "serviceCode";
        public final static String AGENT = "codeAgent";
        public final static String ENTITE = "codeEntite";

    }

    public class CalendarMonth {

        public final static String JANVIER_CODE_1 = "1";
        public final static String JANVIER_CODE_2 = "01";
        public final static String JANVIER_NAME = "Janvier";

        public final static String FEVRIER_CODE_1 = "2";
        public final static String FEVRIER_CODE_2 = "02";
        public final static String FEVRIER_NAME = "Février";

        public final static String MARS_CODE_1 = "3";
        public final static String MARS_CODE_2 = "03";
        public final static String MARS_NAME = "Mars";

        public final static String AVRIL_CODE_1 = "4";
        public final static String AVRIL_CODE_2 = "04";
        public final static String AVRIL_NAME = "Avril";

        public final static String MAI_CODE_1 = "5";
        public final static String MAI_CODE_2 = "05";
        public final static String MAI_NAME = "Mai";

        public final static String JUIN_CODE_1 = "6";
        public final static String JUIN_CODE_2 = "06";
        public final static String JUIN_NAME = "Juin";

        public final static String JUILLET_CODE_1 = "7";
        public final static String JUILLET_CODE_2 = "07";
        public final static String JUILLET_NAME = "Juillet";

        public final static String AOUT_CODE_1 = "8";
        public final static String AOUT_CODE_2 = "08";
        public final static String AOUT_NAME = "Août";

        public final static String SEPTEMBRE_CODE_1 = "9";
        public final static String SEPTEMBRE_CODE_2 = "09";
        public final static String SEPTEMBRE_NAME = "Septembre";

        public final static String OCTOBRE_CODE = "10";
        public final static String OCTOBRE_NAME = "Octobre";

        public final static String NOVEMEBRE_CODE = "11";
        public final static String NOVEMEBRE_NAME = "Novembre";

        public final static String DECMEBRE_CODE = "12";
        public final static String DECMEBRE_NAME = "Décembre";
    }

    public class Devise {

        public final static String DEVISE_CDF = "CDF";
        public final static String DEVISE_USD = "USD";
    }

    public class Boolean {

        public final static String TRUE = "true";
        public final static String FALSE = "false";

    }

    public class Numeric {

        public static final int ZERO = 0;
        public static final int ONE = 1;
        public static final int TWO = 2;
        public static final int THREE = 3;
        public static final int FOUR = 4;
        public static final int FIVE = 5;
        public static final int SIX = 6;
        public static final int SEVEN = 7;
        public static final int HEIGHT = 8;
        public static final int NINE = 9;
        public static final int TEN = 10;
        public static final int ELEVEN = 11;
        public static final int DOUZE = 12;
        public static final int TREIZE = 13;
        public static final int CENT = 100;

    }

    public class Periodicite {

//        public final static String PONC = "PONC";
//        public final static String JOUR = "JOUR";
//        public final static String YEAR = "ANNEE";
//        public final static String MONTH = "MENS";
//        public final static String BIMENS = "BIMEN";
//        public final static String TRIME = "TRIME";
//        public final static String SEMS = "SEMS";
        public final static String PONC = "PR0012015";
        public final static String JOUR = "PR0022015";
        public final static String YEAR = "PR0042015";
        public final static String MONTH = "PR0032015";
        public final static String BIMENS = "PR0102015";
        public final static String TRIME = "PR0052015";
        public final static String SEMS = "PR0072015";
    }

    public class AccuserReception {

        public final static String TYPE_DOCUMENT = "typeDocument";
        public final static String ID_DOCUMENT = "idDocument";
        public final static String DATE_ACCUSER_RECEPTION = "dateAccuserReception";
        public final static String NP_FILLE = "npFille";
    }

    public class LogParam {

        public final static String MAC_ADRESSE = "MAC_ADRESSE";
        public final static String IP_ADRESSE = "IP_ADRESSE";
        public final static String HOST_NAME = "HOST_NAME";
        public final static String FK_EVENEMENT = "FK_EVENEMENT";
        public final static String CONTEXT_BROWSER = "CONTEXT_BROWSER";
    }

    public class Mail {

        public final static String CRYPTO_TLSv1 = "TLSv1";
        public final static String STATUS = "Status";
        public final static String MESSAGE = "Messages";
        public final static String SUCCESS = "success";
        public final static String MAIL_PARAM_JSON_ = "{\"Messages\":[{\"From\":{\"Email\":\"%s\",\"Name\":\"ERECETTES-DGRK\"},\"To\":[{\"Email\":\"%s\",\"Name\":\"%s\"}],\"Subject\":\"%s\",\"TextPart\":\"%s\",\"HTMLPart\":\"%s\"}]}";
        public final static String MAIL_PARAM_JSON = "{\"Messages\":[{\"From\":{\"Email\":\"%s\",\"Name\":\"e-Recettes\"},\"To\":[{\"Email\":\"%s\",\"Name\":\"%s\"}],\"Subject\":\"%s\",\"TextPart\":\"%s\",\"HTMLPart\":\"%s\"}]}";
    }

    public class NameTable {

        public final static String T_NOTE_PERCEPTION = "T_NOTE_PERCEPTION";
        public final static String T_BON_A_PAYER = "T_BON_A_PAYER";
        public final static String T_MED = "T_MED";
        public final static String T_AMR = "T_AMR";
        public final static String T_CONTRAINTE = "T_CONTRAINTE";
        public final static String T_COMMANDEMENT = "T_COMMANDEMENT";
    }

    public class Level {

        public final static String LEVEL_ZERO = "N0";
        public final static String LEVEL_ONE = "N1";
        public final static String LEVEL_TWO = "N2";
        public final static String LEVEL_THREE = "N3";
        public final static String LEVEL_FOUR = "N4";
        public final static String LEVEL_FIVE = "N5";
    }

    public class FormatComplement {

        public final static String NOM = "nom";
        public final static String PRENOM = "prenom";
        public final static String LIEU_NAISSANCE = "lieunaissance";
        public final static String DATE_NAISSANCE = "datenaissance";
        public final static String NOM_COMMUNE = "nomcommune";
        public final static String SIGNATURE = "signaturegest";
        public final static String AVATAR = "avatar";
        public final static String LOGO = "logohvk";
        public final static String DISTRICT = "district";
        public final static String NUMERO_CARTE = "numerocarte";
        public final static String DATE_LIVRAISON = "datelivraison";
        public final static String DATE_FIN_VALIDITE = "datefinvalidite";
        public final static String QRCODE = "qrcode";

        public final static String LOGO_HK = "logoKatanga";
        public final static String NOM_SOCIETE = "nomSociete";
        public final static String NUM_CARTE_NFC = "numCarteNFC";

    }

    public class NatureArticleBudgetaire {

        public final static String TAXE = "N001";
        public final static String IMPOT = "N002";
        public final static String REDEVANCE = "N003";
        public final static String DROIT_DOMANIALE = "N004";
        public final static String AMENDE = "N006";

    }

}
