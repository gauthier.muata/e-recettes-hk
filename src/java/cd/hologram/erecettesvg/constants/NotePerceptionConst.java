/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author moussa.toure
 */
public class NotePerceptionConst {

    public class ParamName {

        public static final String RECHERCHE_NP = "recherche";

        public final static String NUMERO = "NUMERO";
        public final static String ASSUJETTI_NAME = "assujettiName";
        public final static String ASSUJETTI_CODE = "assujettiCode";
        public final static String NOTE_CALCUL = "NOTE_CALCUL";
        public final static String ARTICLE_NAME = "articleName";
        public final static String EXERCICE_FISCALE = "NOTE_CALCUL";
        public final static String NET_A_PAYER = "netAPayer";
        public final static String PAYER = "payer";
        public final static String SOLDE = "solde";
        public final static String SITE = "SITE";
        public final static String PAYER_1 = "PAYER1";
        public final static String PAYER_2 = "PAYER2";
        public final static String SOLDE_1 = "SOLDE1";
        public final static String SOLDE_2 = "SOLDE2";
        public final static String DATE_ECHEANCE_PAIEMENT = "DATE_ECHEANCE_PAIEMENT";
        public final static String NP_MERE = "NP_MERE";
        public final static String COMPTE_BANCAIRE_CODE = "CompteBancaireCode";
        public final static String COMPTE_BANCAIRE_LIBELLE = "CompteBancaireLibelle";

        public final static String COMPTE_BANCAIRE_LIST = "CompteBancaireList";
        public final static String BANQUE_LIST = "BanqueList";

        public final static String BANQUE_CODE = "banqueCode";
        public final static String BANQUE_LIBELLE = "banqueLibelle";

        public final static String DEVISE_CODE = "deviseCode";

        public final static String DATE_RECEPTION = "DATE_RECEPTION";
        public final static String RECEPTIONNISTE = "RECEPTIONNISTE";
        public final static String PRINT_COUNT = "PRINT_COUNT";
        public final static String ADD_ECHEANCE = "ADD_ECHEANCE";
        public final static String TIMBRE = "TIMBRE";
        public final static String PAPIER_SECURISE = "PAPIER_SECURISE";
        public final static String ETAT = "ETAT";
        public final static String AGENT_CREAT = "AGENT_CREAT";
        public final static String DATE_CREAT = "DATECREAT";
        public final static String FRACTIONNEE = "FRACTIONNEE";
        public final static String TAXATION_OFFICE = "TAXATION_OFFICE";
        public final static String NBR_IMPRESSION = "NBR_IMPRESSION";
        public final static String AMR = "AMR";
        public final static String QUITTANCE = "QUITTANCE";
        public final static String DEVISE = "DEVISE";
        public final static String INITIAL_DEVISE = "INITIAL_DEVISE";
        public final static String TAUX_APPLIQUE = "TAUX_APPLIQUE";
        public final static String MONTANT_INITIAL = "MONTANT_INITIAL";
        public final static String EXERCICE = "EXERCICE";
        public final static String SERVICE = "SERVICE";
        public final static String ASSUJETTI = "ASSUJETTI";
        public final static String ADRESSE = "ADRESSE";
        public final static String CODE_ASSUJETTI = "CODE";
        public final static String CODE_ASSUJETTI_RECEPTION = "CODE_RECEPTIONNISTE";
        public final static String MONTANT_DU = "MONTANTDU";
        public final static String NBRE_DAY_NP_ECHEANCE = "8";

        public final static String NBRE_DAY_MED_ECHEANCE = "5";
        public final static String NBRE_DAY_MEP_ECHEANCE = "8";

        public final static String NBRE_DAY_AMR1_ECHEANCE = "15";
        public final static String NBRE_DAY_AMR2_ECHEANCE = "15";
        public final static String NBRE_DAY_AMR_ECHEANCE = "15";
        public final static String NBRE_DAY_CONTRAINTE = "8";
        public final static String NBRE_DAY_COMMANDEMENT = "8";
        public final static String NBRE_DAY_ECHEANCE_BP = "5";
        public final static String NBRE_DAY_ECHEANCE_INVITATION_A_PAYER = "8";
        public final static String NBRE_DAY_ECHEANCE_INVITATION_SERVICE = "2";
        public final static String NBRE_DAY_ECHEANCE_RELANCE = "5";
        public final static String NBRE_DAY_ECHEANCE_INVITATION_PAIEMENT= "8";

        public final static String RERERENCE_DOCUMENT = "referenceDocument";
        public final static String PAIEMENT_AUTORISE = "paiementAutorise";

        public final static String TYPE_PERSONNE = "typePersonne";
        public final static String CODE_PERSONNE = "codePersonne";
        public final static String NOMS_PERSONNE = "nomsPersonne";
        public final static String ADRESSE_PERSONNE = "adressePersonne";
        public final static String TYPE_DOCUMENT_CODE = "typeDocumentCode";
        public final static String TYPE_DOCUMENT_LIBELLE = "typeDocumentLibelle";
        public final static String ACCOUNT_BANK_DECLARATION_LIST = "accountBankDeclarationList";
        public final static String DETAIL_DECLARATION_LIST = "detailDeclarationJsonList";
        public final static String PERIODE_DECLARATION = "periodeDeclaration";
        public final static String BIEN_DECLARATION = "bienDeclaration";
        public final static String SITE_CODE = "siteCode";
        public final static String IS_ECHEANCE_ECHUS = "isEchanceEchus";
        public final static String TYPE_DOCUMENT = "TYPE_DOCUMENT";

        public final static String IS_INTERET = "IS_INTERET";
        public final static String NUMERO_MANUEL = "NUMERO_MANUEL";
        public final static String NP_MERE_MANUEL = "NP_MERE_MANUEL";
        public final static String ETAT_ECHELONNEMENT = "ETAT_ECHELONNEMENT";
        public final static String DATE_ECHEANCE_PAIEMENT_V2 = "DATE_ECHEANCE_PAIEMENT_V2";
        public final static String IS_PAID = "isPaid";
        public final static String IS_APURED = "isApured";

        public final static String TYPE = "TYPE";

        public final static String TITRE_PERCEPTION_DEPENDACY = "TITRE_PERCEPTION_DEPENDACY";
        public final static String TOTAL_PENALITE = "TOTAL_PENALITE";
        public final static String TOTAL_PAYEE_PENALITE = "TOTAL_PAYEE_PENALITE";

    }

    public class Operation {

        public final static String LOAD_NOTE_PERCEPTION = "loadNotePerception";
        public final static String PRINT_NOTE_PERCEPTION = "PrintNotePerception";
        public final static String GET_LAST_SERIE = "getLastSerie";
        public final static String UPDATE_DATE_ECHEANCE = "updateDateEcheance";
        public final static String LOAD_DECLARATION = "loadDeclaration";
        public final static String LOAD_AMR = "loadAmr";
        public final static String LOAD_BON_A_PAYER = "loadBonApayer";
    }

    public class TypeDocument {

        public final static String NP_CODE = "NP";
        public final static String NP_LIBELLE = "NOTE PERCEPTION";

        public final static String DECLARATION_CODE = "DEC";
        public final static String DECLARATION_LIBELLE = "NOTE DE TAXATION";

        public final static String AMR_CODE = "AMR";
        public final static String AMR_LIBELLE_1 = "AMR 1";
        public final static String AMR_LIBELLE_2 = "AMR 2";
        public final static String AMR_LIBELLE = "AVIS DE MISE EN DEMEURE";

        public final static String BP_CODE = "BP";
        public final static String BP_LIBELLE = "BON A PAYER";
        
        public final static String NPCV_CODE = "NPCV";
        public final static String NPCV_LIBELLE = "NOTE DE PAIEMENT DE COMMANDE VOUCHER";
    }

}
