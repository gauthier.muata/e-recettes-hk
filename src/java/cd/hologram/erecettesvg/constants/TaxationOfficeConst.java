/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author moussa.toure
 */
public class TaxationOfficeConst {

    public class ParamName {

        public final static String operation = "operation";
        public final static String articleCode = "articleCode";
        public final static String codeServiceAB = "serviceCodeAB";
        public final static String articleLibelle = "articleLibelle";
        public final static String assujetti = "assujetti";
        public final static String codeAssujetti = "codeAssujetti";
        public final static String formeAssujetti = "formeAssujetti";
        public final static String codeAdresse = "codeAdresse";
        public final static String libelleAdresse = "libelleAdresse";
        public final static String noteTaxation = "noteTaxation";
        public final static String notePerception = "notePerception";
        public final static String notePerceptionManuel = "notePerceptionManuel";
        public final static String montantDu = "montantDu";
        public final static String montantDuInitial = "montantDuInitial";
        public final static String penaliteDu = "penaliteDu";
        public final static String montantpayer = "montantPayer";
        public final static String resteApayer = "restePayer";
        public final static String totalDu = "totalDu";
        public final static String codeSite = "codeSite";
        public final static String userId = "userId";
        public final static String newBase = "newBase";
        public final static String idPeriode = "idPeriode";
        public final static String codeService = "codeService";
        public final static String typeTaux = "typeTaux";
        public final static String multiplierParBase = "multiplierParBase";
        public final static String tauxCourant = "tauxCourant";
        public final static String exercice = "exercice";
        public final static String annee = "annee";
        public final static String mois = "mois";
        public final static String periodicite = "periodicite";
        public final static String codeAssujettissement = "codeAssujettissement";
        public final static String codePeriode = "codePeriode";
        public final static String namePeriode = "namePeriode";
        public final static String codeTarif = "codeTarif";
        public final static String valeurBase = "valeurBase";
        public final static String unite = "unite";
        public final static String labelUnite = "labelUnite";
        public final static String estPenalise = "estPenalise";
        public final static String estPenalisePaiement = "estPenalisePaiement";
        public final static String dateLimite = "dateLimite";
        public final static String dateLimitePaiement = "dateLimitePaiement";
        public final static String dateDeclaration = "dateDeclaration";
        public final static String statePeriodeDeclaration = "statePeriodeDeclaration";
        public final static String devise = "devise";
        public final static String typeSearch = "typeSearch";
        public final static String indexSearch = "indexSearch";
        public final static String typeRegistre = "typeRegistre";
        public final static String taux = "taux";
        public final static String baseTaxable = "baseTaxable";
        public final static String quantity = "quantity";
        public final static String deviseInitial = "deviseInitial";
        public final static String estRedresse = "estRedresse";
        public final static String npSpecialeRedressement = "npSpecialeRedresse";
        public final static String tauxPenalite = "tauxPenalite";
        public final static String codeOfficiel = "codeOfficiel";
    }

    public class Operation {

        public final static String LOAD_DEFAILLANT_DECLARATION = "loadDefaillantDeclaration";
        public final static String SAVE_TAXTION_OFFICE = "301";

    }

}
