/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_TAUX_RECIDIVE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TauxRecidive.findAll", query = "SELECT t FROM TauxRecidive t"),
    @NamedQuery(name = "TauxRecidive.findByCode", query = "SELECT t FROM TauxRecidive t WHERE t.code = :code"),
    @NamedQuery(name = "TauxRecidive.findByTauxPenalite", query = "SELECT t FROM TauxRecidive t WHERE t.tauxPenalite = :tauxPenalite"),
    @NamedQuery(name = "TauxRecidive.findByTaux", query = "SELECT t FROM TauxRecidive t WHERE t.taux = :taux"),
    @NamedQuery(name = "TauxRecidive.findByEstPourcentage", query = "SELECT t FROM TauxRecidive t WHERE t.estPourcentage = :estPourcentage"),
    @NamedQuery(name = "TauxRecidive.findByFormeJuridique", query = "SELECT t FROM TauxRecidive t WHERE t.formeJuridique = :formeJuridique"),
    @NamedQuery(name = "TauxRecidive.findByAgentCreat", query = "SELECT t FROM TauxRecidive t WHERE t.agentCreat = :agentCreat"),
    @NamedQuery(name = "TauxRecidive.findByDateCreat", query = "SELECT t FROM TauxRecidive t WHERE t.dateCreat = :dateCreat"),
    @NamedQuery(name = "TauxRecidive.findByAgentMaj", query = "SELECT t FROM TauxRecidive t WHERE t.agentMaj = :agentMaj"),
    @NamedQuery(name = "TauxRecidive.findByDateMaj", query = "SELECT t FROM TauxRecidive t WHERE t.dateMaj = :dateMaj")})
public class TauxRecidive implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODE")
    private String code;
    @Size(max = 20)
    @Column(name = "TAUX_PENALITE")
    private String tauxPenalite;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TAUX")
    private BigDecimal taux;
    @Column(name = "EST_POURCENTAGE")
    private Boolean estPourcentage;
    @Size(max = 10)
    @Column(name = "FORME_JURIDIQUE")
    private String formeJuridique;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    public TauxRecidive() {
    }

    public TauxRecidive(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTauxPenalite() {
        return tauxPenalite;
    }

    public void setTauxPenalite(String tauxPenalite) {
        this.tauxPenalite = tauxPenalite;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public Boolean getEstPourcentage() {
        return estPourcentage;
    }

    public void setEstPourcentage(Boolean estPourcentage) {
        this.estPourcentage = estPourcentage;
    }

    public String getFormeJuridique() {
        return formeJuridique;
    }

    public void setFormeJuridique(String formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TauxRecidive)) {
            return false;
        }
        TauxRecidive other = (TauxRecidive) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.TauxRecidive[ code=" + code + " ]";
    }
    
}
