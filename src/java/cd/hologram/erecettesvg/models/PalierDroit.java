/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PALIER_DROIT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PalierDroit.findAll", query = "SELECT p FROM PalierDroit p"),
    @NamedQuery(name = "PalierDroit.findById", query = "SELECT p FROM PalierDroit p WHERE p.id = :id"),
    @NamedQuery(name = "PalierDroit.findByBorneInf", query = "SELECT p FROM PalierDroit p WHERE p.borneInf = :borneInf"),
    @NamedQuery(name = "PalierDroit.findByBorneSup", query = "SELECT p FROM PalierDroit p WHERE p.borneSup = :borneSup"),
    @NamedQuery(name = "PalierDroit.findByDroit", query = "SELECT p FROM PalierDroit p WHERE p.droit = :droit"),
    @NamedQuery(name = "PalierDroit.findByEtat", query = "SELECT p FROM PalierDroit p WHERE p.etat = :etat"),
    @NamedQuery(name = "PalierDroit.findByAgentCreat", query = "SELECT p FROM PalierDroit p WHERE p.agentCreat = :agentCreat"),
    @NamedQuery(name = "PalierDroit.findByDateCreat", query = "SELECT p FROM PalierDroit p WHERE p.dateCreat = :dateCreat"),
    @NamedQuery(name = "PalierDroit.findByAgentMaj", query = "SELECT p FROM PalierDroit p WHERE p.agentMaj = :agentMaj"),
    @NamedQuery(name = "PalierDroit.findByDateMaj", query = "SELECT p FROM PalierDroit p WHERE p.dateMaj = :dateMaj")})
public class PalierDroit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BORNE_INF")
    private BigDecimal borneInf;
    @Column(name = "BORNE_SUP")
    private BigDecimal borneSup;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "DROIT")
    private String droit;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    public PalierDroit() {
    }

    public PalierDroit(Integer id) {
        this.id = id;
    }

    public PalierDroit(Integer id, String droit) {
        this.id = id;
        this.droit = droit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getBorneInf() {
        return borneInf;
    }

    public void setBorneInf(BigDecimal borneInf) {
        this.borneInf = borneInf;
    }

    public BigDecimal getBorneSup() {
        return borneSup;
    }

    public void setBorneSup(BigDecimal borneSup) {
        this.borneSup = borneSup;
    }

    public String getDroit() {
        return droit;
    }

    public void setDroit(String droit) {
        this.droit = droit;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PalierDroit)) {
            return false;
        }
        PalierDroit other = (PalierDroit) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.PalierDroit[ id=" + id + " ]";
    }
    
}
