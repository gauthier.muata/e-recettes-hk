/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_NOTE_PERCEPTION_VIGNETTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotePerceptionVignette.findAll", query = "SELECT l FROM NotePerceptionVignette l"),
    @NamedQuery(name = "NotePerceptionVignette.findById", query = "SELECT l FROM NotePerceptionVignette l WHERE l.id = :id"),
    @NamedQuery(name = "NotePerceptionVignette.findByFkNotePerceptionBanque", query = "SELECT l FROM NotePerceptionVignette l WHERE l.fkNotePerceptionBanque = :fkNotePerceptionBanque"),
    @NamedQuery(name = "NotePerceptionVignette.findByDateImpression", query = "SELECT l FROM NotePerceptionVignette l WHERE l.dateImpression = :dateImpression"),
    @NamedQuery(name = "NotePerceptionVignette.findByNumeroSerieVignette", query = "SELECT l FROM NotePerceptionVignette l WHERE l.numeroSerieVignette = :numeroSerieVignette"),
    @NamedQuery(name = "NotePerceptionVignette.findByNoteTaxation", query = "SELECT l FROM NotePerceptionVignette l WHERE l.noteTaxation = :noteTaxation"),
    @NamedQuery(name = "NotePerceptionVignette.findByVignetteHtml", query = "SELECT l FROM NotePerceptionVignette l WHERE l.vignetteHtml = :vignetteHtml"),
    @NamedQuery(name = "NotePerceptionVignette.findByAgentImpression", query = "SELECT l FROM NotePerceptionVignette l WHERE l.agentImpression = :agentImpression"),
    @NamedQuery(name = "NotePerceptionVignette.findByNbreImpression", query = "SELECT l FROM NotePerceptionVignette l WHERE l.nbreImpression = :nbreImpression"),
    @NamedQuery(name = "NotePerceptionVignette.findByFkBien", query = "SELECT l FROM NotePerceptionVignette l WHERE l.fkBien = :fkBien"),
    @NamedQuery(name = "NotePerceptionVignette.findByEtat", query = "SELECT l FROM NotePerceptionVignette l WHERE l.etat = :etat")})
public class NotePerceptionVignette implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "FK_NOTE_PERCEPTION_BANQUE")
    private Integer fkNotePerceptionBanque;

    @Column(name = "DATE_IMPRESSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateImpression;

    @Column(name = "NOTE_TAXATION")
    private String noteTaxation;

    @Column(name = "NUMERO_SERIE_VIGNETTE")
    private String numeroSerieVignette;

    @Column(name = "ETAT")
    private Integer etat;

    @Column(name = "VIGNETE_HTML")
    private String vignetteHtml;
    
    @Column(name = "FK_BIEN")
    private String fkBien;

    @Column(name = "NBRE_IMPRESSION")
    private Integer nbreImpression;

    @Column(name = "AGENT_IMPRESSION")
    private Integer agentImpression;

    public NotePerceptionVignette() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNoteTaxation() {
        return noteTaxation;
    }

    public void setNoteTaxation(String noteTaxation) {
        this.noteTaxation = noteTaxation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getNbreImpression() {
        return nbreImpression;
    }

    public void setNbreImpression(Integer nbreImpression) {
        this.nbreImpression = nbreImpression;
    }

    public Integer getFkNotePerceptionBanque() {
        return fkNotePerceptionBanque;
    }

    public void setFkNotePerceptionBanque(Integer fkNotePerceptionBanque) {
        this.fkNotePerceptionBanque = fkNotePerceptionBanque;
    }

    public Date getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(Date dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getNumeroSerieVignette() {
        return numeroSerieVignette;
    }

    public void setNumeroSerieVignette(String numeroSerieVignette) {
        this.numeroSerieVignette = numeroSerieVignette;
    }

    public String getVignetteHtml() {
        return vignetteHtml;
    }

    public void setVignetteHtml(String vignetteHtml) {
        this.vignetteHtml = vignetteHtml;
    }

    public Integer getAgentImpression() {
        return agentImpression;
    }

    public void setAgentImpression(Integer agentImpression) {
        this.agentImpression = agentImpression;
    }

    public String getFkBien() {
        return fkBien;
    }

    public void setFkBien(String fkBien) {
        this.fkBien = fkBien;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotePerceptionVignette)) {
            return false;
        }
        NotePerceptionVignette other = (NotePerceptionVignette) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.NotePerceptionVignette[ id=" + id + " ]";
    }
}
