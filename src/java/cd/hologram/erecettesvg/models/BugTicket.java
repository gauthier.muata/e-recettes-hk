/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_BUG_TICKET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BugTicket.findAll", query = "SELECT b FROM BugTicket b"),
    @NamedQuery(name = "BugTicket.findByCode", query = "SELECT b FROM BugTicket b WHERE b.code = :code"),
    @NamedQuery(name = "BugTicket.findByTitre", query = "SELECT b FROM BugTicket b WHERE b.titre = :titre"),
    @NamedQuery(name = "BugTicket.findByContenu", query = "SELECT b FROM BugTicket b WHERE b.contenu = :contenu"),
    @NamedQuery(name = "BugTicket.findByIp", query = "SELECT b FROM BugTicket b WHERE b.ip = :ip"),
    @NamedQuery(name = "BugTicket.findByMacAdresse", query = "SELECT b FROM BugTicket b WHERE b.macAdresse = :macAdresse"),
    @NamedQuery(name = "BugTicket.findByService", query = "SELECT b FROM BugTicket b WHERE b.service = :service"),
    @NamedQuery(name = "BugTicket.findBySite", query = "SELECT b FROM BugTicket b WHERE b.site = :site"),
    @NamedQuery(name = "BugTicket.findByAgentCreat", query = "SELECT b FROM BugTicket b WHERE b.agentCreat = :agentCreat"),
    @NamedQuery(name = "BugTicket.findByDateCreat", query = "SELECT b FROM BugTicket b WHERE b.dateCreat = :dateCreat"),
    @NamedQuery(name = "BugTicket.findByEtat", query = "SELECT b FROM BugTicket b WHERE b.etat = :etat")})
public class BugTicket implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 50)
    @Column(name = "TITRE")
    private String titre;
    @Size(max = 255)
    @Column(name = "CONTENU")
    private String contenu;
    @Size(max = 15)
    @Column(name = "IP")
    private String ip;
    @Size(max = 25)
    @Column(name = "MAC_ADRESSE")
    private String macAdresse;
    @Size(max = 25)
    @Column(name = "SERVICE")
    private String service;
    @Size(max = 25)
    @Column(name = "SITE")
    private String site;
    @Size(max = 25)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Column(name = "ETAT")
    private Integer etat;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "CONTENU1")
    private String contenu1;

    public BugTicket() {
    }

    public BugTicket(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMacAdresse() {
        return macAdresse;
    }

    public void setMacAdresse(String macAdresse) {
        this.macAdresse = macAdresse;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getContenu1() {
        return contenu1;
    }

    public void setContenu1(String contenu1) {
        this.contenu1 = contenu1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BugTicket)) {
            return false;
        }
        BugTicket other = (BugTicket) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.BugTicket[ code=" + code + " ]";
    }
    
}
