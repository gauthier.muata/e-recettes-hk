/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_ARTICLE_BUDGETAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArticleBudgetaire.findAll", query = "SELECT a FROM ArticleBudgetaire a"),
    @NamedQuery(name = "ArticleBudgetaire.findByCode", query = "SELECT a FROM ArticleBudgetaire a WHERE a.code = :code"),
    @NamedQuery(name = "ArticleBudgetaire.findByIntitule", query = "SELECT a FROM ArticleBudgetaire a WHERE a.intitule = :intitule"),
    @NamedQuery(name = "ArticleBudgetaire.findByPalier", query = "SELECT a FROM ArticleBudgetaire a WHERE a.palier = :palier"),
    @NamedQuery(name = "ArticleBudgetaire.findByAssujetissable", query = "SELECT a FROM ArticleBudgetaire a WHERE a.assujetissable = :assujetissable"),
    @NamedQuery(name = "ArticleBudgetaire.findByArticleMere", query = "SELECT a FROM ArticleBudgetaire a WHERE a.articleMere = :articleMere"),
    @NamedQuery(name = "ArticleBudgetaire.findByTarifVariable", query = "SELECT a FROM ArticleBudgetaire a WHERE a.tarifVariable = :tarifVariable"),
    @NamedQuery(name = "ArticleBudgetaire.findByEtat", query = "SELECT a FROM ArticleBudgetaire a WHERE a.etat = :etat"),
    @NamedQuery(name = "ArticleBudgetaire.findByAgentCreat", query = "SELECT a FROM ArticleBudgetaire a WHERE a.agentCreat = :agentCreat"),
    @NamedQuery(name = "ArticleBudgetaire.findByDateCreat", query = "SELECT a FROM ArticleBudgetaire a WHERE a.dateCreat = :dateCreat"),
    @NamedQuery(name = "ArticleBudgetaire.findByAgentMaj", query = "SELECT a FROM ArticleBudgetaire a WHERE a.agentMaj = :agentMaj"),
    @NamedQuery(name = "ArticleBudgetaire.findByDateMaj", query = "SELECT a FROM ArticleBudgetaire a WHERE a.dateMaj = :dateMaj"),
    @NamedQuery(name = "ArticleBudgetaire.findByProprietaire", query = "SELECT a FROM ArticleBudgetaire a WHERE a.proprietaire = :proprietaire"),
    @NamedQuery(name = "ArticleBudgetaire.findByTransactionnel", query = "SELECT a FROM ArticleBudgetaire a WHERE a.transactionnel = :transactionnel"),
    @NamedQuery(name = "ArticleBudgetaire.findByDebut", query = "SELECT a FROM ArticleBudgetaire a WHERE a.debut = :debut"),
    @NamedQuery(name = "ArticleBudgetaire.findByFin", query = "SELECT a FROM ArticleBudgetaire a WHERE a.fin = :fin"),
    @NamedQuery(name = "ArticleBudgetaire.findByPeriodiciteVariable", query = "SELECT a FROM ArticleBudgetaire a WHERE a.periodiciteVariable = :periodiciteVariable"),
    @NamedQuery(name = "ArticleBudgetaire.findByNbrJourLimite", query = "SELECT a FROM ArticleBudgetaire a WHERE a.nbrJourLimite = :nbrJourLimite"),
    @NamedQuery(name = "ArticleBudgetaire.findByQuantiteVariable", query = "SELECT a FROM ArticleBudgetaire a WHERE a.quantiteVariable = :quantiteVariable"),
    @NamedQuery(name = "ArticleBudgetaire.findByArrete", query = "SELECT a FROM ArticleBudgetaire a WHERE a.arrete = :arrete"),
    @NamedQuery(name = "ArticleBudgetaire.findByCodeOfficiel", query = "SELECT a FROM ArticleBudgetaire a WHERE a.codeOfficiel = :codeOfficiel"),
    @NamedQuery(name = "ArticleBudgetaire.findBySigle", query = "SELECT a FROM ArticleBudgetaire a WHERE a.sigle = :sigle"),
    @NamedQuery(name = "ArticleBudgetaire.findByDateDebutPeriode", query = "SELECT a FROM ArticleBudgetaire a WHERE a.dateDebutPeriode = :dateDebutPeriode"),
    @NamedQuery(name = "ArticleBudgetaire.findByDateDebutPenalite", query = "SELECT a FROM ArticleBudgetaire a WHERE a.dateDebutPenalite = :dateDebutPenalite")})
public class ArticleBudgetaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 255)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "PALIER")
    private Boolean palier;
    @Column(name = "ASSUJETISSABLE")
    private Boolean assujetissable;
    @Size(max = 25)
    @Column(name = "ARTICLE_MERE")
    private String articleMere;
    @Size(max = 20)
    @Column(name = "TARIF_VARIABLE")
    private String tarifVariable;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    
    @Column(name = "SIGLE")
    private String sigle;
    
    @Column(name = "PROPRIETAIRE")
    private Short proprietaire;
    @Column(name = "TRANSACTIONNEL")
    private Short transactionnel;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DEBUT")
    private BigDecimal debut;
    @Column(name = "FIN")
    private BigDecimal fin;
    @Column(name = "PERIODICITE_VARIABLE")
    private Short periodiciteVariable;
    @Column(name = "NBR_JOUR_LIMITE")
    private Integer nbrJourLimite;
    @Column(name = "QUANTITE_VARIABLE")
    private Boolean quantiteVariable;
    @Size(max = 25)
    @Column(name = "ARRETE")
    private String arrete;
    @Size(max = 10)
    @Column(name = "CODE_OFFICIEL")
    private String codeOfficiel;
    @Size(max = 10)
    @Column(name = "DATE_DEBUT_PERIODE")
    private String dateDebutPeriode;
    @Size(max = 10)
    @Column(name = "DATE_DEBUT_PENALITE")
    private String dateDebutPenalite;

    @Size(max = 10)
    @Column(name = "ECHEANCE_LEGALE")
    private String echeanceLegale;
    @Column(name = "PERIODE_ECHEANCE")
    private Boolean periodeEcheance;
    @Size(max = 10)
    @Column(name = "DATE_LIMITE_PAIEMENT")
    private String dateLimitePaiement;

    @OneToMany(mappedBy = "articleBudgetaire")
    private List<Palier> palierList;
    @JoinColumn(name = "ARTICLE_GENERIQUE", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleGenerique articleGenerique;
    @JoinColumn(name = "NATURE", referencedColumnName = "CODE")
    @ManyToOne
    private NatureArticleBudgetaire nature;
    @JoinColumn(name = "PERIODICITE", referencedColumnName = "CODE")
    @ManyToOne
    private Periodicite periodicite;
    @JoinColumn(name = "TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif tarif;
    @JoinColumn(name = "UNITE", referencedColumnName = "CODE")
    @ManyToOne
    private Unite unite;
    @OneToMany(mappedBy = "fkAb")
    private List<BanqueAb> banqueAbList;
    @OneToMany(mappedBy = "articleBudgetaire")
    private List<Assujeti> assujetiList;
    @OneToMany(mappedBy = "fkArticleBudgetaire")
    private List<CleRepartition> cleRepartitionList;
    @OneToMany(mappedBy = "articleBudgetaire")
    private List<DetailFichePriseCharge> detailFichePriseChargeList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "articleBudgetaire")
    private List<DetailDepotDeclaration> detailDepotDeclarationList;
    @OneToMany(mappedBy = "articleBudgetaire")
    private List<AbComplementBien> abComplementBienList;
    @OneToMany(mappedBy = "articleBudgetaire")
    private List<DetailBordereau> detailBordereauList;
    @OneToMany(mappedBy = "articleBudgetaire")
    private List<DetailsNc> detailsNcList;
    @OneToMany(mappedBy = "fkArticleBudgetaire")
    private List<Exoneration> exonerationList;

    @OneToMany(mappedBy = "fkAb")
    private List<Commande> commandeList;

    public ArticleBudgetaire() {
    }

    public ArticleBudgetaire(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Boolean getPalier() {
        return palier;
    }

    public void setPalier(Boolean palier) {
        this.palier = palier;
    }

    public Boolean getAssujetissable() {
        return assujetissable;
    }

    public void setAssujetissable(Boolean assujetissable) {
        this.assujetissable = assujetissable;
    }

    public String getArticleMere() {
        return articleMere;
    }

    public void setArticleMere(String articleMere) {
        this.articleMere = articleMere;
    }

    public String getTarifVariable() {
        return tarifVariable;
    }

    public void setTarifVariable(String tarifVariable) {
        this.tarifVariable = tarifVariable;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Short proprietaire) {
        this.proprietaire = proprietaire;
    }

    public Short getTransactionnel() {
        return transactionnel;
    }

    public void setTransactionnel(Short transactionnel) {
        this.transactionnel = transactionnel;
    }

    public BigDecimal getDebut() {
        return debut;
    }

    public void setDebut(BigDecimal debut) {
        this.debut = debut;
    }

    public BigDecimal getFin() {
        return fin;
    }

    public void setFin(BigDecimal fin) {
        this.fin = fin;
    }

    public Short getPeriodiciteVariable() {
        return periodiciteVariable;
    }

    public void setPeriodiciteVariable(Short periodiciteVariable) {
        this.periodiciteVariable = periodiciteVariable;
    }

    public Integer getNbrJourLimite() {
        return nbrJourLimite;
    }

    public void setNbrJourLimite(Integer nbrJourLimite) {
        this.nbrJourLimite = nbrJourLimite;
    }

    public Boolean getQuantiteVariable() {
        return quantiteVariable;
    }

    public void setQuantiteVariable(Boolean quantiteVariable) {
        this.quantiteVariable = quantiteVariable;
    }

    public String getArrete() {
        return arrete;
    }

    public void setArrete(String arrete) {
        this.arrete = arrete;
    }

    public String getCodeOfficiel() {
        return codeOfficiel;
    }

    public void setCodeOfficiel(String codeOfficiel) {
        this.codeOfficiel = codeOfficiel;
    }

    public String getDateDebutPeriode() {
        return dateDebutPeriode;
    }

    public void setDateDebutPeriode(String dateDebutPeriode) {
        this.dateDebutPeriode = dateDebutPeriode;
    }

    public String getDateDebutPenalite() {
        return dateDebutPenalite;
    }

    public void setDateDebutPenalite(String dateDebutPenalite) {
        this.dateDebutPenalite = dateDebutPenalite;
    }

    @XmlTransient
    public List<Palier> getPalierList() {
        return palierList;
    }

    public void setPalierList(List<Palier> palierList) {
        this.palierList = palierList;
    }

    public ArticleGenerique getArticleGenerique() {
        return articleGenerique;
    }

    public void setArticleGenerique(ArticleGenerique articleGenerique) {
        this.articleGenerique = articleGenerique;
    }

    public NatureArticleBudgetaire getNature() {
        return nature;
    }

    public void setNature(NatureArticleBudgetaire nature) {
        this.nature = nature;
    }

    public Periodicite getPeriodicite() {
        return periodicite;
    }

    public void setPeriodicite(Periodicite periodicite) {
        this.periodicite = periodicite;
    }

    public Tarif getTarif() {
        return tarif;
    }

    public void setTarif(Tarif tarif) {
        this.tarif = tarif;
    }

    public String getSigle() {
        return sigle;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }
    
    
    

    public Unite getUnite() {
        return unite;
    }

    public void setUnite(Unite unite) {
        this.unite = unite;
    }

    public String getEcheanceLegale() {
        return echeanceLegale;
    }

    public void setEcheanceLegale(String echeanceLegale) {
        this.echeanceLegale = echeanceLegale;
    }

    public Boolean getPeriodeEcheance() {
        return periodeEcheance;
    }

    public void setPeriodeEcheance(Boolean periodeEcheance) {
        this.periodeEcheance = periodeEcheance;
    }

    public String getDateLimitePaiement() {
        return dateLimitePaiement;
    }

    public void setDateLimitePaiement(String dateLimitePaiement) {
        this.dateLimitePaiement = dateLimitePaiement;
    }

    @XmlTransient
    public List<BanqueAb> getBanqueAbList() {
        return banqueAbList;
    }

    public void setBanqueAbList(List<BanqueAb> banqueAbList) {
        this.banqueAbList = banqueAbList;
    }

    @XmlTransient
    public List<Assujeti> getAssujetiList() {
        return assujetiList;
    }

    public void setAssujetiList(List<Assujeti> assujetiList) {
        this.assujetiList = assujetiList;
    }

    @XmlTransient
    public List<CleRepartition> getCleRepartitionList() {
        return cleRepartitionList;
    }

    public void setTCleRepartitionList(List<CleRepartition> cleRepartitionList) {
        this.cleRepartitionList = cleRepartitionList;
    }

    @XmlTransient
    public List<DetailFichePriseCharge> getDetailFichePriseChargeList() {
        return detailFichePriseChargeList;
    }

    public void setDetailFichePriseChargeList(List<DetailFichePriseCharge> detailFichePriseChargeList) {
        this.detailFichePriseChargeList = detailFichePriseChargeList;
    }

    @XmlTransient
    public List<DetailDepotDeclaration> getDetailDepotDeclarationList() {
        return detailDepotDeclarationList;
    }

    public void setDetailDepotDeclarationList(List<DetailDepotDeclaration> detailDepotDeclarationList) {
        this.detailDepotDeclarationList = detailDepotDeclarationList;
    }

    @XmlTransient
    public List<AbComplementBien> getAbComplementBienList() {
        return abComplementBienList;
    }

    public void setAbComplementBienList(List<AbComplementBien> abComplementBienList) {
        this.abComplementBienList = abComplementBienList;
    }

    @XmlTransient
    public List<DetailBordereau> getDetailBordereauList() {
        return detailBordereauList;
    }

    public void setDetailBordereauList(List<DetailBordereau> detailBordereauList) {
        this.detailBordereauList = detailBordereauList;
    }

    @XmlTransient
    public List<DetailsNc> getDetailsNcList() {
        return detailsNcList;
    }

    public void setDetailsNcList(List<DetailsNc> detailsNcList) {
        this.detailsNcList = detailsNcList;
    }

    public List<Exoneration> getExonerationList() {
        return exonerationList;
    }

    public void setExonerationList(List<Exoneration> exonerationList) {
        this.exonerationList = exonerationList;
    }

    public List<Commande> getCommandeList() {
        return commandeList;
    }

    public void setCommandeList(List<Commande> commandeList) {
        this.commandeList = commandeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArticleBudgetaire)) {
            return false;
        }
        ArticleBudgetaire other = (ArticleBudgetaire) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.ArticleBudgetaire[ code=" + code + " ]";
    }

}
