/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author WILLY
 */
@Entity
@Table(name = "T_COMMANDE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commande.findAll", query = "SELECT c FROM Commande c"),
    @NamedQuery(name = "Commande.findById", query = "SELECT c FROM Commande c WHERE c.id = :id"),
    @NamedQuery(name = "Commande.findByDateCreat", query = "SELECT c FROM Commande c WHERE c.dateCreat = :dateCreat"),
    @NamedQuery(name = "Commande.findByMontant", query = "SELECT c FROM Commande c WHERE c.montant = :montant"),
    @NamedQuery(name = "Commande.findByReference", query = "SELECT c FROM Commande c WHERE c.reference = :refence"),
    @NamedQuery(name = "Commande.findByNotePaiement", query = "SELECT c FROM Commande c WHERE c.notePaiement = :notePaiement"),

    @NamedQuery(name = "Commande.findByReferencePaiement", query = "SELECT c FROM Commande c WHERE c.referencePaiement = :referencePaiement"),
    @NamedQuery(name = "Commande.findBySitePaiement", query = "SELECT c FROM Commande c WHERE c.sitePaiement = :sitePaiement"),
    @NamedQuery(name = "Commande.findByAgentPaiement", query = "SELECT c FROM Commande c WHERE c.agentPaiement = :agentPaiement"),
    @NamedQuery(name = "Commande.findByDatePaiement", query = "SELECT c FROM Commande c WHERE c.datePaiement = :datePaiement"),
    @NamedQuery(name = "Commande.findByStePaiement", query = "SELECT c FROM Commande c WHERE c.sitePaiement = :sitePaiement"),
    @NamedQuery(name = "Commande.findByAgentCreate", query = "SELECT c FROM Commande c WHERE c.agentCreate = :agentCreate"),
    @NamedQuery(name = "Commande.findByTypeCmd", query = "SELECT c FROM Commande c WHERE c.typeCmd = :typeCmd"),

    @NamedQuery(name = "Commande.findByAgentValidationPaiement", query = "SELECT c FROM Commande c WHERE c.agentValidationPaiement = :agentValidationPaiement"),
    @NamedQuery(name = "Commande.findByDateValidationPaiement", query = "SELECT c FROM Commande c WHERE c.dateValidationPaiement = :dateValidationPaiement"),

    @NamedQuery(name = "Commande.findByPreuvePaiement", query = "SELECT c FROM Commande c WHERE c.preuvePaiement = :preuvePaiement"),
    @NamedQuery(name = "Commande.findByEtat", query = "SELECT c FROM Commande c WHERE c.etat = :etat")})
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 2147483647)
    @Column(name = "NOTE_PAIEMENT")
    private String notePaiement;
    @Size(max = 2147483647)
    @Column(name = "PREUVE_PAIEMENT")
    private String preuvePaiement;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    @JoinColumn(name = "FK_COMPTE_BANCAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private CompteBancaire fkCompteBancaire;
    @Size(max = 25)
    @Column(name = "REFERENCE")
    private String reference;

    @Column(name = "REFERENCE_PAIEMENT")
    private String referencePaiement;
    
    @Column(name = "TYPE_CMD")
    private Integer typeCmd;

    @JoinColumn(name = "SITE_PAIEMENT", referencedColumnName = "CODE")
    @ManyToOne
    private Site sitePaiement;

    @JoinColumn(name = "AGENT_PAIEMENT", referencedColumnName = "CODE")
    private Agent agentPaiement;

    @Column(name = "DATE_PAIEMENT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePaiement;

    @Column(name = "AGENT_VALIDATION_PAIEMENT")
    private Integer agentValidationPaiement;

    @Column(name = "DATE_VALIDATION_PAIEMENT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateValidationPaiement;

    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;

    @JoinColumn(name = "FK_AB", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire fkAb;
    private List<Voucher> detailsVoucherList;

    public Commande() {
    }

    public Commande(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTypeCmd() {
        return typeCmd;
    }

    public void setTypeCmd(Integer typeCmd) {
        this.typeCmd = typeCmd;
    }
    
    
    

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getNotePaiement() {
        return notePaiement;
    }

    public void setNotePaiement(String notePaiement) {
        this.notePaiement = notePaiement;
    }

    public String getPreuvePaiement() {
        return preuvePaiement;
    }

    public void setPreuvePaiement(String preuvePaiement) {
        this.preuvePaiement = preuvePaiement;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public ArticleBudgetaire getFkAb() {
        return fkAb;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setFkAb(ArticleBudgetaire fkAb) {
        this.fkAb = fkAb;
    }

    public String getReferencePaiement() {
        return referencePaiement;
    }

    public void setReferencePaiement(String referencePaiement) {
        this.referencePaiement = referencePaiement;
    }

    public Site getSitePaiement() {
        return sitePaiement;
    }

    public void setSitePaiement(Site sitePaiement) {
        this.sitePaiement = sitePaiement;
    }

    public Agent getAgentPaiement() {
        return agentPaiement;
    }

    public void setAgentPaiement(Agent agentPaiement) {
        this.agentPaiement = agentPaiement;
    }

    public Date getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public CompteBancaire getFkCompteBancaire() {
        return fkCompteBancaire;
    }

    public void setFkCompteBancaire(CompteBancaire fkCompteBancaire) {
        this.fkCompteBancaire = fkCompteBancaire;
    }

    public Integer getAgentValidationPaiement() {
        return agentValidationPaiement;
    }

    public void setAgentValidationPaiement(Integer agentValidationPaiement) {
        this.agentValidationPaiement = agentValidationPaiement;
    }

    public Date getDateValidationPaiement() {
        return dateValidationPaiement;
    }

    public void setDateValidationPaiement(Date dateValidationPaiement) {
        this.dateValidationPaiement = dateValidationPaiement;
    }
    
    
    

    public List<Voucher> getDetailsVoucherList() {
        return detailsVoucherList;
    }

    public void setDetailsVoucherList(List<Voucher> detailsVoucherList) {
        this.detailsVoucherList = detailsVoucherList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Commande other = (Commande) obj;
        return true;
    }

    @Override
    public String toString() {
        return "Commande{" + "id=" + id + ", dateCreat=" + dateCreat + ", montant=" + montant + ", etat=" + etat + ", fkPersonne=" + fkPersonne + ", devise=" + devise + ", fkAb=" + fkAb + '}';
    }

}
