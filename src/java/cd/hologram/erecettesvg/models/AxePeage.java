/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */
@Entity
@Table(name = "T_AXE_PEAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AxePeage.findAll", query = "SELECT a FROM AxePeage a"),
    @NamedQuery(name = "AxePeage.findById", query = "SELECT a FROM AxePeage a WHERE a.id = :id"),
    @NamedQuery(name = "AxePeage.findByFkAxe", query = "SELECT a FROM AxePeage a WHERE a.fkAxe = :fkAxe"),
    @NamedQuery(name = "AxePeage.findByEtat", query = "SELECT a FROM AxePeage a WHERE a.etat = :etat")})
public class AxePeage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "FK_AXE")
    private Integer fkAxe;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "FK_SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site fkSite;

    public AxePeage() {
    }

    public AxePeage(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFkAxe() {
        return fkAxe;
    }

    public void setFkAxe(Integer fkAxe) {
        this.fkAxe = fkAxe;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Site getFkSite() {
        return fkSite;
    }

    public void setFkSite(Site fkSite) {
        this.fkSite = fkSite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AxePeage)) {
            return false;
        }
        AxePeage other = (AxePeage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entites.AxePeage[ id=" + id + " ]";
    }
    
}
