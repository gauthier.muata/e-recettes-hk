/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_NOTE_PERCEPTION_BANQUE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotePerceptionBanque.findAll", query = "SELECT l FROM NotePerceptionBanque l"),
    @NamedQuery(name = "NotePerceptionBanque.findById", query = "SELECT l FROM NotePerceptionBanque l WHERE l.id = :id"),
    @NamedQuery(name = "NotePerceptionBanque.findByNotePerception", query = "SELECT l FROM NotePerceptionBanque l WHERE l.notePerception = :notePerception"),
    @NamedQuery(name = "NotePerceptionBanque.findByDateUtilisation", query = "SELECT l FROM NotePerceptionBanque l WHERE l.dateUtilisation = :dateUtilisation"),
    @NamedQuery(name = "NotePerceptionBanque.findByFkBanque", query = "SELECT l FROM NotePerceptionBanque l WHERE l.fkBanque = :fkBanque"),
    @NamedQuery(name = "NotePerceptionBanque.findByNoteTaxation", query = "SELECT l FROM NotePerceptionBanque l WHERE l.noteTaxation = :noteTaxation"),
    @NamedQuery(name = "NotePerceptionBanque.findByEstUtilise", query = "SELECT l FROM NotePerceptionBanque l WHERE l.estUtilise = :estUtilise"),
    @NamedQuery(name = "NotePerceptionBanque.findByPlageDebut", query = "SELECT l FROM NotePerceptionBanque l WHERE l.plageDebut = :plageDebut"),
    @NamedQuery(name = "NotePerceptionBanque.findByPlageFin", query = "SELECT l FROM NotePerceptionBanque l WHERE l.plageFin = :plageFin"),
    @NamedQuery(name = "NotePerceptionBanque.findByNbreImpression", query = "SELECT l FROM NotePerceptionBanque l WHERE l.nbreImpression = :nbreImpression"),
    @NamedQuery(name = "NotePerceptionBanque.findByDateCreate", query = "SELECT l FROM NotePerceptionBanque l WHERE l.dateCreate = :dateCreate"),
    @NamedQuery(name = "NotePerceptionBanque.findByFkEa", query = "SELECT l FROM NotePerceptionBanque l WHERE l.fkEa = :fkEa"),

    @NamedQuery(name = "NotePerceptionBanque.findByAgentCreate", query = "SELECT l FROM NotePerceptionBanque l WHERE l.agentCreate = :agentCreate"),
    @NamedQuery(name = "NotePerceptionBanque.findByAgentMaj", query = "SELECT l FROM NotePerceptionBanque l WHERE l.agentMaj = :agentMaj"),
    @NamedQuery(name = "NotePerceptionBanque.findByDateMaj", query = "SELECT l FROM NotePerceptionBanque l WHERE l.dateMaj = :dateMaj"),

    @NamedQuery(name = "NotePerceptionBanque.findByEtat", query = "SELECT l FROM NotePerceptionBanque l WHERE l.etat = :etat")})
public class NotePerceptionBanque implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "NOTE_PERCEPTION")
    private String notePerception;

    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;

    @Column(name = "DATE_UTILISATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUtilisation;

    @Column(name = "FK_BANQUE")
    private String fkBanque;

    @Column(name = "NOTE_TAXATION")
    private String noteTaxation;

    @Column(name = "ETAT")
    private Integer etat;

    @Column(name = "EST_UTILISE")
    private Integer estUtilise;

    @Column(name = "PLAGE_DEBUT")
    private String plageDebut;

    @Column(name = "PLAGE_FIN")
    private String plageFin;

    @Column(name = "NBRE_IMPRESSION")
    private Integer nbreImpression;

    @Column(name = "FK_EA")
    private String fkEa;

    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;

    @Column(name = "AGENT_MAJ")
    private Integer agentMaj;

    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    public NotePerceptionBanque() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotePerception() {
        return notePerception;
    }

    public void setNotePerception(String notePerception) {
        this.notePerception = notePerception;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateUtilisation() {
        return dateUtilisation;
    }

    public void setDateUtilisation(Date dateUtilisation) {
        this.dateUtilisation = dateUtilisation;
    }

    public String getFkBanque() {
        return fkBanque;
    }

    public void setFkBanque(String fkBanque) {
        this.fkBanque = fkBanque;
    }

    public String getFkEa() {
        return fkEa;
    }

    public void setFkEa(String fkEa) {
        this.fkEa = fkEa;
    }

    public String getNoteTaxation() {
        return noteTaxation;
    }

    public void setNoteTaxation(String noteTaxation) {
        this.noteTaxation = noteTaxation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getEstUtilise() {
        return estUtilise;
    }

    public void setEstUtilise(Integer estUtilise) {
        this.estUtilise = estUtilise;
    }

    public String getPlageDebut() {
        return plageDebut;
    }

    public void setPlageDebut(String plageDebut) {
        this.plageDebut = plageDebut;
    }

    public String getPlageFin() {
        return plageFin;
    }

    public void setPlageFin(String plageFin) {
        this.plageFin = plageFin;
    }

    public Integer getNbreImpression() {
        return nbreImpression;
    }

    public void setNbreImpression(Integer nbreImpression) {
        this.nbreImpression = nbreImpression;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public Integer getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Integer agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotePerceptionBanque)) {
            return false;
        }
        NotePerceptionBanque other = (NotePerceptionBanque) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.NotePerceptionBanque[ id=" + id + " ]";
    }
}
