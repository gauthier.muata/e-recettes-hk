/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_CARTE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Carte.findAll", query = "SELECT c FROM Carte c"),
    @NamedQuery(name = "Carte.findById", query = "SELECT c FROM Carte c WHERE c.id = :id"),
    @NamedQuery(name = "Carte.findByNumeroAutocollan", query = "SELECT c FROM Carte c WHERE c.numeroAutocollan = :numeroAutocollan"),
    @NamedQuery(name = "Carte.findByNumeroCpi", query = "SELECT c FROM Carte c WHERE c.numeroCpi = :numeroCpi"),
    @NamedQuery(name = "Carte.findByNumeroAutorisation", query = "SELECT c FROM Carte c WHERE c.numeroAutorisation = :numeroAutorisation"),
    @NamedQuery(name = "Carte.findByCommune", query = "SELECT c FROM Carte c WHERE c.commune = :commune"),
    @NamedQuery(name = "Carte.findByDistrict", query = "SELECT c FROM Carte c WHERE c.district = :district"),
    @NamedQuery(name = "Carte.findByDateGeneration", query = "SELECT c FROM Carte c WHERE c.dateGeneration = :dateGeneration"),
    @NamedQuery(name = "Carte.findByNombreImpression", query = "SELECT c FROM Carte c WHERE c.nombreImpression = :nombreImpression"),
    @NamedQuery(name = "Carte.findByEtat", query = "SELECT c FROM Carte c WHERE c.etat = :etat"),
    @NamedQuery(name = "Carte.findByDateFinValidite", query = "SELECT c FROM Carte c WHERE c.dateFinValidite = :dateFinValidite"),
    @NamedQuery(name = "Carte.findByDateLivraison", query = "SELECT c FROM Carte c WHERE c.dateLivraison = :dateLivraison"),
    @NamedQuery(name = "Carte.findByPhotoProfil", query = "SELECT c FROM Carte c WHERE c.photoProfil = :photoProfil")})
public class Carte implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private String id;
    @Size(max = 50)
    @Column(name = "NUMERO_AUTOCOLLAN")
    private String numeroAutocollan;
    @Size(max = 50)
    @Column(name = "NUMERO_CPI")
    private String numeroCpi;
    @Size(max = 50)
    @Column(name = "NUMERO_AUTORISATION")
    private String numeroAutorisation;
    @Size(max = 50)
    @Column(name = "COMMUNE")
    private String commune;
    @Size(max = 50)
    @Column(name = "DISTRICT")
    private String district;
    @Size(max = 10)
    @Column(name = "DATE_GENERATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateGeneration;
    @Column(name = "NOMBRE_IMPRESSION")
    private Integer nombreImpression;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 10)
    @Column(name = "DATE_FIN_VALIDITE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFinValidite;
    @Size(max = 10)
    @Column(name = "DATE_LIVRAISON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLivraison;
    @Column(name = "PHOTO_PROFIL")
    private String photoProfil;
    @JoinColumn(name = "FK_CONDUCTEUR_VEHICULE", referencedColumnName = "ID")
    @ManyToOne
    private ConducteurVehicule fkConducteurVehicule;

    public Carte() {
    }

    public Carte(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumeroAutocollan() {
        return numeroAutocollan;
    }

    public void setNumeroAutocollan(String numeroAutocollan) {
        this.numeroAutocollan = numeroAutocollan;
    }

    public String getNumeroCpi() {
        return numeroCpi;
    }

    public void setNumeroCpi(String numeroCpi) {
        this.numeroCpi = numeroCpi;
    }

    public String getNumeroAutorisation() {
        return numeroAutorisation;
    }

    public void setNumeroAutorisation(String numeroAutorisation) {
        this.numeroAutorisation = numeroAutorisation;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Date getDateGeneration() {
        return dateGeneration;
    }

    public void setDateGeneration(Date dateGeneration) {
        this.dateGeneration = dateGeneration;
    }

    public Integer getNombreImpression() {
        return nombreImpression;
    }

    public void setNombreImpression(Integer nombreImpression) {
        this.nombreImpression = nombreImpression;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Date getDateFinValidite() {
        return dateFinValidite;
    }

    public void setDateFinValidite(Date dateFinValidite) {
        this.dateFinValidite = dateFinValidite;
    }

    public Date getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(Date dateLivraison) {
        this.dateLivraison = dateLivraison;
    }
    
    public String getPhotoProfil() {
        return photoProfil;
    }

    public void setPhotoProfil(String photoProfil) {
        this.photoProfil = photoProfil;
    }

    public ConducteurVehicule getFkConducteurVehicule() {
        return fkConducteurVehicule;
    }

    public void setFkConducteurVehicule(ConducteurVehicule fkConducteurVehicule) {
        this.fkConducteurVehicule = fkConducteurVehicule;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carte)) {
            return false;
        }
        Carte other = (Carte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Carte[ id=" + id + " ]";
    }
    
}
