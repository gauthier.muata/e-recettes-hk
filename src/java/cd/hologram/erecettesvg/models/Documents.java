/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_DOCUMENTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Documents.findAll", query = "SELECT d FROM Documents d"),
    @NamedQuery(name = "Documents.findByCode", query = "SELECT d FROM Documents d WHERE d.code = :code"),
    @NamedQuery(name = "Documents.findByFkTypeDocument", query = "SELECT d FROM Documents d WHERE d.fkTypeDocument = :fkTypeDocument"),
    @NamedQuery(name = "Documents.findByDateCreat", query = "SELECT d FROM Documents d WHERE d.dateCreat = :dateCreat"),
    @NamedQuery(name = "Documents.findByAgentCreat", query = "SELECT d FROM Documents d WHERE d.agentCreat = :agentCreat"),
    @NamedQuery(name = "Documents.findByEtat", query = "SELECT d FROM Documents d WHERE d.etat = :etat"),
    @NamedQuery(name = "Documents.findByPersonne", query = "SELECT d FROM Documents d WHERE d.personne = :personne")})
public class Documents implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "FK_TYPE_DOCUMENT")
    private String fkTypeDocument;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "DATE_CREAT")
    private String dateCreat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "ETAT")
    private Boolean etat;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "HTML")
    private String html;
    @Size(max = 50)
    @Column(name = "PERSONNE")
    private String personne;

    public Documents() {
    }

    public Documents(String code) {
        this.code = code;
    }

    public Documents(String code, String fkTypeDocument, String dateCreat, String agentCreat) {
        this.code = code;
        this.fkTypeDocument = fkTypeDocument;
        this.dateCreat = dateCreat;
        this.agentCreat = agentCreat;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFkTypeDocument() {
        return fkTypeDocument;
    }

    public void setFkTypeDocument(String fkTypeDocument) {
        this.fkTypeDocument = fkTypeDocument;
    }

    public String getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(String dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getPersonne() {
        return personne;
    }

    public void setPersonne(String personne) {
        this.personne = personne;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Documents)) {
            return false;
        }
        Documents other = (Documents) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Documents[ code=" + code + " ]";
    }
    
}
