/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_DOCUMENT_OFFICIEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentOfficiel.findAll", query = "SELECT d FROM DocumentOfficiel d"),
    @NamedQuery(name = "DocumentOfficiel.findByCode", query = "SELECT d FROM DocumentOfficiel d WHERE d.code = :code"),
    @NamedQuery(name = "DocumentOfficiel.findByIntitule", query = "SELECT d FROM DocumentOfficiel d WHERE d.intitule = :intitule"),
    @NamedQuery(name = "DocumentOfficiel.findByDateArrete", query = "SELECT d FROM DocumentOfficiel d WHERE d.dateArrete = :dateArrete"),
    @NamedQuery(name = "DocumentOfficiel.findByEtat", query = "SELECT d FROM DocumentOfficiel d WHERE d.etat = :etat"),
    @NamedQuery(name = "DocumentOfficiel.findByNumero", query = "SELECT d FROM DocumentOfficiel d WHERE d.numero = :numero")})
public class DocumentOfficiel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "CODE")
    private String code;
    @Size(max = 150)
    @Column(name = "INTITULE")
    private String intitule;
    @Size(max = 10)
    @Column(name = "DATE_ARRETE")
    private String dateArrete;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 50)
    @Column(name = "NUMERO")
    private String numero;

    public DocumentOfficiel() {
    }

    public DocumentOfficiel(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getDateArrete() {
        return dateArrete;
    }

    public void setDateArrete(String dateArrete) {
        this.dateArrete = dateArrete;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentOfficiel)) {
            return false;
        }
        DocumentOfficiel other = (DocumentOfficiel) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.DocumentOfficiel[ code=" + code + " ]";
    }
    
}
