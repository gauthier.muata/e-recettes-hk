/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_MED_PERIODE_DECLARATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedPeriodeDeclaration.findAll", query = "SELECT l FROM MedPeriodeDeclaration l"),
    @NamedQuery(name = "MedPeriodeDeclaration.findById", query = "SELECT l FROM MedPeriodeDeclaration l WHERE l.id = :id"),

    @NamedQuery(name = "MedPeriodeDeclaration.findByFkMed", query = "SELECT l FROM MedPeriodeDeclaration l WHERE l.fkMed = :fkMed"),
    
    @NamedQuery(name = "MedPeriodeDeclaration.findByDateTaxationOffice", query = "SELECT l FROM MedPeriodeDeclaration l WHERE l.dateTaxationOffice = :dateTaxationOffice"),
    @NamedQuery(name = "MedPeriodeDeclaration.findByTaxationOffice", query = "SELECT l FROM MedPeriodeDeclaration l WHERE l.taxationOffice = :taxationOffice"),

    @NamedQuery(name = "MedPeriodeDeclaration.findByPrincipalDu", query = "SELECT l FROM MedPeriodeDeclaration l WHERE l.principalDu = :principalDu"),
    @NamedQuery(name = "MedPeriodeDeclaration.findByPenaliteDu", query = "SELECT l FROM MedPeriodeDeclaration l WHERE l.penaliteDu = :penaliteDu"),
    @NamedQuery(name = "MedPeriodeDeclaration.findByEstPaye", query = "SELECT l FROM MedPeriodeDeclaration l WHERE l.estPaye = :estPaye"),

    @NamedQuery(name = "MedPeriodeDeclaration.findByFkPeriodeDeclaration", query = "SELECT l FROM MedPeriodeDeclaration l WHERE l.fkPeriodeDeclaration = :fkPeriodeDeclaration"),
    @NamedQuery(name = "MedPeriodeDeclaration.findByEtat", query = "SELECT l FROM MedPeriodeDeclaration l WHERE l.etat = :etat")})

public class MedPeriodeDeclaration implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "FK_MED")
    private String fkMed;

    @Column(name = "FK_PERIODE_DECLARATION")
    private Integer fkPeriodeDeclaration;

    @Column(name = "ETAT")
    private Integer etat;

    @Column(name = "PRINCIPAL_DU")
    private BigDecimal principalDu;

    @Column(name = "PENALITE_DU")
    private BigDecimal penaliteDu;

    @Column(name = "TAXATION_OFFICE")
    private Integer taxationOffice;
    
    @Column(name = "EST_PAYE")
    private Integer estPaye;

    @Column(name = "DATE_TAXATION_OFFICE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTaxationOffice;

    public MedPeriodeDeclaration() {
    }

    public MedPeriodeDeclaration(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkMed() {
        return fkMed;
    }

    public void setFkMed(String fkMed) {
        this.fkMed = fkMed;
    }

    public Integer getFkPeriodeDeclaration() {
        return fkPeriodeDeclaration;
    }

    public void setFkPeriodeDeclaration(Integer fkPeriodeDeclaration) {
        this.fkPeriodeDeclaration = fkPeriodeDeclaration;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public BigDecimal getPrincipalDu() {
        return principalDu;
    }

    public void setPrincipalDu(BigDecimal principalDu) {
        this.principalDu = principalDu;
    }

    public BigDecimal getPenaliteDu() {
        return penaliteDu;
    }

    public void setPenaliteDu(BigDecimal penaliteDu) {
        this.penaliteDu = penaliteDu;
    }

    public Integer getTaxationOffice() {
        return taxationOffice;
    }

    public void setTaxationOffice(Integer taxationOffice) {
        this.taxationOffice = taxationOffice;
    }

    public Date getDateTaxationOffice() {
        return dateTaxationOffice;
    }

    public void setDateTaxationOffice(Date dateTaxationOffice) {
        this.dateTaxationOffice = dateTaxationOffice;
    }

    public Integer getEstPaye() {
        return estPaye;
    }

    public void setEstPaye(Integer estPaye) {
        this.estPaye = estPaye;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedPeriodeDeclaration)) {
            return false;
        }
        MedPeriodeDeclaration other = (MedPeriodeDeclaration) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.MedPeriodeDeclaration[ id=" + id + " ]";
    }
}
