/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_AMR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Amr.findAll", query = "SELECT a FROM Amr a"),
    @NamedQuery(name = "Amr.findByNumero", query = "SELECT a FROM Amr a WHERE a.numero = :numero"),
    @NamedQuery(name = "Amr.findByNetAPayer", query = "SELECT a FROM Amr a WHERE a.netAPayer = :netAPayer"),
    @NamedQuery(name = "Amr.findByPayer", query = "SELECT a FROM Amr a WHERE a.payer = :payer"),
    @NamedQuery(name = "Amr.findBySolde", query = "SELECT a FROM Amr a WHERE a.solde = :solde"),
    @NamedQuery(name = "Amr.findByAgentCreat", query = "SELECT a FROM Amr a WHERE a.agentCreat = :agentCreat"),
    @NamedQuery(name = "Amr.findByDateCreat", query = "SELECT a FROM Amr a WHERE a.dateCreat = :dateCreat"),
    @NamedQuery(name = "Amr.findByEtat", query = "SELECT a FROM Amr a WHERE a.etat = :etat"),
    @NamedQuery(name = "Amr.findByTypeAmr", query = "SELECT a FROM Amr a WHERE a.typeAmr = :typeAmr"),
    @NamedQuery(name = "Amr.findByDateEcheance", query = "SELECT a FROM Amr a WHERE a.dateEcheance = :dateEcheance"),
    @NamedQuery(name = "Amr.findByNoteCalcul", query = "SELECT a FROM Amr a WHERE a.noteCalcul = :noteCalcul"),
    @NamedQuery(name = "Amr.findByNotePerception", query = "SELECT a FROM Amr a WHERE a.notePerception = :notePerception"),
    @NamedQuery(name = "Amr.findByDateReceptionAmr", query = "SELECT a FROM Amr a WHERE a.dateReceptionAmr = :dateReceptionAmr"),
    @NamedQuery(name = "Amr.findByGenererPenalite", query = "SELECT a FROM Amr a WHERE a.genererPenalite = :genererPenalite"),
    @NamedQuery(name = "Amr.findByAmrMere", query = "SELECT a FROM Amr a WHERE a.amrMere = :amrMere"),
    @NamedQuery(name = "Amr.findByFractionner", query = "SELECT a FROM Amr a WHERE a.fractionner = :fractionner"),
    @NamedQuery(name = "Amr.findBySoldeInitial", query = "SELECT a FROM Amr a WHERE a.soldeInitial = :soldeInitial"),
    @NamedQuery(name = "Amr.findByFkMed", query = "SELECT a FROM Amr a WHERE a.fkMed = :fkMed"),
    @NamedQuery(name = "Amr.findByFkRetraitDeclaration", query = "SELECT a FROM Amr a WHERE a.fkRetraitDeclaration = :fkRetraitDeclaration"),
    @NamedQuery(name = "Amr.findByEnContentieux", query = "SELECT a FROM Amr a WHERE a.enContentieux = :enContentieux"),
    @NamedQuery(name = "Amr.findByNumeroDocument", query = "SELECT c FROM Amr c WHERE c.numeroDocument = :numeroDocument")})
public class Amr implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "NUMERO")
    private String numero;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "NET_A_PAYER")
    private BigDecimal netAPayer;
    @Column(name = "PAYER")
    private BigDecimal payer;
    @Column(name = "SOLDE")
    private BigDecimal solde;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Size(max = 10)
    @Column(name = "DATE_CREAT")
    private String dateCreat;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "TYPE_AMR")
    private String typeAmr;
    @Column(name = "DATE_ECHEANCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheance;
    @Size(max = 25)
    @Column(name = "NOTE_CALCUL")
    private String noteCalcul;
    @Size(max = 25)

    @Column(name = "FK_MED")
    private String fkMed;

    @Column(name = "OBSERVATION")
    private String observation;

    @Column(name = "MOTIF")
    private String motif;

    @Column(name = "FK_RETRAIT_DECLARATION")
    private Integer fkRetraitDeclaration;

    @Column(name = "NOTE_PERCEPTION")
    private String notePerception;
    @Column(name = "DATE_RECEPTION_AMR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReceptionAmr;
    @Column(name = "GENERER_PENALITE")
    private Boolean genererPenalite;
    @Size(max = 25)
    @Column(name = "AMR_MERE")
    private String amrMere;
    @Column(name = "FRACTIONNER")
    private Boolean fractionner;
    @Column(name = "SOLDE_INITIAL")
    private BigDecimal soldeInitial;
    @Column(name = "EN_CONTENTIEUX")
    private Boolean enContentieux;
    @Size(max = 100)
    @Column(name = "NUMERO_DOCUMENT")
    private String numeroDocument;
    @OneToMany(mappedBy = "amr1")
    private List<FraisPoursuite> fraisPoursuiteList;
    @OneToMany(mappedBy = "amr2")
    private List<FraisPoursuite> fraisPoursuiteList1;
    @OneToMany(mappedBy = "amr")
    private List<Journal> journalList;
    @OneToMany(mappedBy = "fkAmr")
    private List<BonAPayer> bonAPayerList;
    @OneToMany(mappedBy = "amr2")
    private List<PenaliteMajoree> penaliteMajoreeList;
    @OneToMany(mappedBy = "amr")
    private List<DetailsAmr> detailsAmrList;
    @OneToMany(mappedBy = "idAmr")
    private List<Contrainte> contrainteList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "amr1")
    private Amr amr;
    @JoinColumn(name = "NUMERO", referencedColumnName = "NUMERO", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Amr amr1;
    @JoinColumn(name = "FICHE_PRISE_CHARGE", referencedColumnName = "CODE")
    @ManyToOne
    private FichePriseCharge fichePriseCharge;

    public Amr() {
    }

    public Amr(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public BigDecimal getNetAPayer() {
        return netAPayer;
    }

    public void setNetAPayer(BigDecimal netAPayer) {
        this.netAPayer = netAPayer;
    }

    public BigDecimal getPayer() {
        return payer;
    }

    public void setPayer(BigDecimal payer) {
        this.payer = payer;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public void setSolde(BigDecimal solde) {
        this.solde = solde;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(String dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getTypeAmr() {
        return typeAmr;
    }

    public void setTypeAmr(String typeAmr) {
        this.typeAmr = typeAmr;
    }

    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getNoteCalcul() {
        return noteCalcul;
    }

    public void setNoteCalcul(String noteCalcul) {
        this.noteCalcul = noteCalcul;
    }

    public String getNotePerception() {
        return notePerception;
    }

    public void setNotePerception(String notePerception) {
        this.notePerception = notePerception;
    }

    public Date getDateReceptionAmr() {
        return dateReceptionAmr;
    }

    public void setDateReceptionAmr(Date dateReceptionAmr) {
        this.dateReceptionAmr = dateReceptionAmr;
    }

    public Boolean getGenererPenalite() {
        return genererPenalite;
    }

    public void setGenererPenalite(Boolean genererPenalite) {
        this.genererPenalite = genererPenalite;
    }

    public String getAmrMere() {
        return amrMere;
    }

    public void setAmrMere(String amrMere) {
        this.amrMere = amrMere;
    }

    public Boolean getFractionner() {
        return fractionner;
    }

    public void setFractionner(Boolean fractionner) {
        this.fractionner = fractionner;
    }

    public BigDecimal getSoldeInitial() {
        return soldeInitial;
    }

    public void setSoldeInitial(BigDecimal soldeInitial) {
        this.soldeInitial = soldeInitial;
    }

    public String getFkMed() {
        return fkMed;
    }

    public void setFkMed(String fkMed) {
        this.fkMed = fkMed;
    }

    public Boolean getEnContentieux() {
        return enContentieux;
    }

    public void setEnContentieux(Boolean enContentieux) {
        this.enContentieux = enContentieux;
    }
    
    
    public String getNumeroDocument() {
        return numeroDocument;
    }

    public void setNumeroDocument(String numeroDocument) {
        this.numeroDocument = numeroDocument;
    }

    public Integer getFkRetraitDeclaration() {
        return fkRetraitDeclaration;
    }

    public void setFkRetraitDeclaration(Integer fkRetraitDeclaration) {
        this.fkRetraitDeclaration = fkRetraitDeclaration;
    }

    @XmlTransient
    public List<FraisPoursuite> getFraisPoursuiteList() {
        return fraisPoursuiteList;
    }

    public void setFraisPoursuiteList(List<FraisPoursuite> fraisPoursuiteList) {
        this.fraisPoursuiteList = fraisPoursuiteList;
    }

    @XmlTransient
    public List<FraisPoursuite> getFraisPoursuiteList1() {
        return fraisPoursuiteList1;
    }

    public void setFraisPoursuiteList1(List<FraisPoursuite> fraisPoursuiteList1) {
        this.fraisPoursuiteList1 = fraisPoursuiteList1;
    }

    @XmlTransient
    public List<Journal> getJournalList() {
        return journalList;
    }

    public void setJournalList(List<Journal> journalList) {
        this.journalList = journalList;
    }

    @XmlTransient
    public List<BonAPayer> getBonAPayerList() {
        return bonAPayerList;
    }

    public void setBonAPayerList(List<BonAPayer> bonAPayerList) {
        this.bonAPayerList = bonAPayerList;
    }

    @XmlTransient
    public List<PenaliteMajoree> getPenaliteMajoreeList() {
        return penaliteMajoreeList;
    }

    public void setPenaliteMajoreeList(List<PenaliteMajoree> penaliteMajoreeList) {
        this.penaliteMajoreeList = penaliteMajoreeList;
    }

    @XmlTransient
    public List<DetailsAmr> getDetailsAmrList() {
        return detailsAmrList;
    }

    public void setDetailsAmrList(List<DetailsAmr> detailsAmrList) {
        this.detailsAmrList = detailsAmrList;
    }

    @XmlTransient
    public List<Contrainte> getContrainteList() {
        return contrainteList;
    }

    public void setContrainteList(List<Contrainte> contrainteList) {
        this.contrainteList = contrainteList;
    }

    public Amr getAmr() {
        return amr;
    }

    public void setAmr(Amr amr) {
        this.amr = amr;
    }

    public Amr getAmr1() {
        return amr1;
    }

    public void setAmr1(Amr amr1) {
        this.amr1 = amr1;
    }

    public FichePriseCharge getFichePriseCharge() {
        return fichePriseCharge;
    }

    public void setFichePriseCharge(FichePriseCharge fichePriseCharge) {
        this.fichePriseCharge = fichePriseCharge;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numero != null ? numero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Amr)) {
            return false;
        }
        Amr other = (Amr) object;
        if ((this.numero == null && other.numero != null) || (this.numero != null && !this.numero.equals(other.numero))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Amr[ numero=" + numero + " ]";
    }

}
