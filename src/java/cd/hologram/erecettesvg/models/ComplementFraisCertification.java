/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_COMPLEMENT_FRAIS_CERTIFICATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComplementFraisCertification.findAll", query = "SELECT l FROM ComplementFraisCertification l"),
    @NamedQuery(name = "ComplementFraisCertification.findById", query = "SELECT l FROM ComplementFraisCertification l WHERE l.id = :id"),
    @NamedQuery(name = "ComplementFraisCertification.findByNoteVersement", query = "SELECT l FROM ComplementFraisCertification l WHERE l.noteVersement = :noteVersement"),
    @NamedQuery(name = "ComplementFraisCertification.findByAgence", query = "SELECT l FROM ComplementFraisCertification l WHERE l.agence = :agence"),
    @NamedQuery(name = "ComplementFraisCertification.findByProduit", query = "SELECT l FROM ComplementFraisCertification l WHERE l.produit = :produit"),
    @NamedQuery(name = "ComplementFraisCertification.findByNumeroLot", query = "SELECT l FROM ComplementFraisCertification l WHERE l.numeroLot = :numeroLot"),
    @NamedQuery(name = "ComplementFraisCertification.findByCotePart", query = "SELECT l FROM ComplementFraisCertification l WHERE l.cotePart = :cotePart"),

    @NamedQuery(name = "ComplementFraisCertification.findByBanque", query = "SELECT l FROM ComplementFraisCertification l WHERE l.banque = :banque"),
    @NamedQuery(name = "ComplementFraisCertification.findByCompteBancaire", query = "SELECT l FROM ComplementFraisCertification l WHERE l.compteBancaire = :compteBancaire"),
    @NamedQuery(name = "ComplementFraisCertification.findByDateTaxation", query = "SELECT l FROM ComplementFraisCertification l WHERE l.dateTaxation = :dateTaxation"),
    @NamedQuery(name = "ComplementFraisCertification.findByFkNoteCalcul", query = "SELECT l FROM ComplementFraisCertification l WHERE l.fkNoteCalcul = :fkNoteCalcul"),
    @NamedQuery(name = "ComplementFraisCertification.findByFkNotePerception", query = "SELECT l FROM ComplementFraisCertification l WHERE l.fkNotePerception = :fkNotePerception"),
    @NamedQuery(name = "ComplementFraisCertification.findByFkAb", query = "SELECT l FROM ComplementFraisCertification l WHERE l.fkAb = :fkAb")})

public class ComplementFraisCertification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "NOTE_VERSEMENT")
    private String noteVersement;

    @Column(name = "AGENCE")
    private String agence;

    @Column(name = "PRODUIT")
    private String produit;

    @Column(name = "NOMBRE_LOT")
    private Integer numeroLot;

    @Column(name = "COTE_PART")
    private BigDecimal cotePart;

    @Column(name = "BANQUE")
    private String banque;

    @Column(name = "COMPTE_BANCAIRE")
    private String compteBancaire;

    @Column(name = "DATE_TAXATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTaxation;

    @Column(name = "FK_NOTE_CALCUL")
    private String fkNoteCalcul;

    @Column(name = "FK_NOTE_PERCEPTION")
    private String fkNotePerception;

    @Column(name = "FK_AB")
    private String fkAb;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNoteVersement() {
        return noteVersement;
    }

    public void setNoteVersement(String noteVersement) {
        this.noteVersement = noteVersement;
    }

    public String getAgence() {
        return agence;
    }

    public void setAgence(String agence) {
        this.agence = agence;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public Integer getNumeroLot() {
        return numeroLot;
    }

    public void setNumeroLot(Integer numeroLot) {
        this.numeroLot = numeroLot;
    }

    public BigDecimal getCotePart() {
        return cotePart;
    }

    public void setCotePart(BigDecimal cotePart) {
        this.cotePart = cotePart;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public Date getDateTaxation() {
        return dateTaxation;
    }

    public void setDateTaxation(Date dateTaxation) {
        this.dateTaxation = dateTaxation;
    }

    public String getFkNoteCalcul() {
        return fkNoteCalcul;
    }

    public void setFkNoteCalcul(String fkNoteCalcul) {
        this.fkNoteCalcul = fkNoteCalcul;
    }

    public String getFkNotePerception() {
        return fkNotePerception;
    }

    public void setFkNotePerception(String fkNotePerception) {
        this.fkNotePerception = fkNotePerception;
    }

    public String getFkAb() {
        return fkAb;
    }

    public void setFkAb(String fkAb) {
        this.fkAb = fkAb;
    }

    public ComplementFraisCertification() {
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComplementFraisCertification)) {
            return false;
        }
        ComplementFraisCertification other = (ComplementFraisCertification) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.ComplementFraisCertification[ id=" + id + " ]";
    }
}
