/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_LOG_CHAMPS_CHANGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LogChampsChange.findAll", query = "SELECT l FROM LogChampsChange l"),
    @NamedQuery(name = "LogChampsChange.findById", query = "SELECT l FROM LogChampsChange l WHERE l.id = :id"),
    @NamedQuery(name = "LogChampsChange.findByChamps", query = "SELECT l FROM LogChampsChange l WHERE l.champs = :champs"),
    @NamedQuery(name = "LogChampsChange.findByBefore", query = "SELECT l FROM LogChampsChange l WHERE l.before = :before"),
    @NamedQuery(name = "LogChampsChange.findByAfter", query = "SELECT l FROM LogChampsChange l WHERE l.after = :after"),
    @NamedQuery(name = "LogChampsChange.findByVisible", query = "SELECT l FROM LogChampsChange l WHERE l.visible = :visible")})
public class LogChampsChange implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "CHAMPS")
    private String champs;
    @Size(max = 500)
    @Column(name = "BEFORE")
    private String before;
    @Size(max = 500)
    @Column(name = "AFTER")
    private String after;
    @Column(name = "VISIBLE")
    private Integer visible;
    @JoinColumn(name = "FK_T_LOG", referencedColumnName = "ID_LOG")
    @ManyToOne
    private Log fkTLog;

    public LogChampsChange() {
    }

    public LogChampsChange(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChamps() {
        return champs;
    }

    public void setChamps(String champs) {
        this.champs = champs;
    }

    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Log getFkTLog() {
        return fkTLog;
    }

    public void setFkTLog(Log fkTLog) {
        this.fkTLog = fkTLog;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogChampsChange)) {
            return false;
        }
        LogChampsChange other = (LogChampsChange) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.LogChampsChange[ id=" + id + " ]";
    }
    
}
