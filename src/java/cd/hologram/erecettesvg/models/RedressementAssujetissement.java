/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_REDRESSEMENT_ASSUJETISSEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RedressementAssujetissement.findAll", query = "SELECT r FROM RedressementAssujetissement r"),
    @NamedQuery(name = "RedressementAssujetissement.findByCode", query = "SELECT r FROM RedressementAssujetissement r WHERE r.code = :code"),
    @NamedQuery(name = "RedressementAssujetissement.findByAncienAssujetissement", query = "SELECT r FROM RedressementAssujetissement r WHERE r.ancienAssujetissement = :ancienAssujetissement"),
    @NamedQuery(name = "RedressementAssujetissement.findByNouvelAssujetissement", query = "SELECT r FROM RedressementAssujetissement r WHERE r.nouvelAssujetissement = :nouvelAssujetissement"),
    @NamedQuery(name = "RedressementAssujetissement.findByAgentCreat", query = "SELECT r FROM RedressementAssujetissement r WHERE r.agentCreat = :agentCreat"),
    @NamedQuery(name = "RedressementAssujetissement.findByEtat", query = "SELECT r FROM RedressementAssujetissement r WHERE r.etat = :etat")})
public class RedressementAssujetissement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Long code;
    @Size(max = 25)
    @Column(name = "ANCIEN_ASSUJETISSEMENT")
    private String ancienAssujetissement;
    @Size(max = 25)
    @Column(name = "NOUVEL_ASSUJETISSEMENT")
    private String nouvelAssujetissement;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "ETAT")
    private Boolean etat;

    public RedressementAssujetissement() {
    }

    public RedressementAssujetissement(Long code) {
        this.code = code;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getAncienAssujetissement() {
        return ancienAssujetissement;
    }

    public void setAncienAssujetissement(String ancienAssujetissement) {
        this.ancienAssujetissement = ancienAssujetissement;
    }

    public String getNouvelAssujetissement() {
        return nouvelAssujetissement;
    }

    public void setNouvelAssujetissement(String nouvelAssujetissement) {
        this.nouvelAssujetissement = nouvelAssujetissement;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RedressementAssujetissement)) {
            return false;
        }
        RedressementAssujetissement other = (RedressementAssujetissement) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.RedressementAssujetissement[ code=" + code + " ]";
    }
    
}
