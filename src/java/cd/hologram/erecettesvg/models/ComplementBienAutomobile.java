/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_COMPLEMENT_BIEN_AUTOMOBILE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComplementBienAutomobile.findAll", query = "SELECT l FROM ComplementBienAutomobile l"),
    @NamedQuery(name = "ComplementBienAutomobile.findById", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.id = :id"),
    @NamedQuery(name = "ComplementBienAutomobile.findByFkBien", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.fkBien = :fkBien"),
    @NamedQuery(name = "ComplementBienAutomobile.findByMarque", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.marque = :marque"),
    @NamedQuery(name = "ComplementBienAutomobile.findByType", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.type = :type"),
    @NamedQuery(name = "ComplementBienAutomobile.findByNumeroPlaque", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.numeroPlaque = :numeroPlaque"),
    @NamedQuery(name = "ComplementBienAutomobile.findByNumeroChassis", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.numeroChassis = :numeroChassis"),
    @NamedQuery(name = "ComplementBienAutomobile.findByAnneeMiseCirculation", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.anneeMiseCirculation = :anneeMiseCirculation"),
    @NamedQuery(name = "ComplementBienAutomobile.findByPuissanceFiscale", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.puissanceFiscale = :puissanceFiscale"),
    @NamedQuery(name = "ComplementBienAutomobile.findByUnitePuissanceFiscale", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.unitePuissanceFiscale = :unitePuissanceFiscale"),
    @NamedQuery(name = "ComplementBienAutomobile.findByDateCreate", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.dateCreate = :dateCreate"),
    @NamedQuery(name = "ComplementBienAutomobile.findByDateMiseCirculation", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.dateMiseCirculation = :dateMiseCirculation"),
    @NamedQuery(name = "ComplementBienAutomobile.findByAgentCreate", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.agentCreate = :agentCreate"),
    
    @NamedQuery(name = "ComplementBienAutomobile.findByFkMarque", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.fkMarque = :fkMarque"),
    @NamedQuery(name = "ComplementBienAutomobile.findByFkModele", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.fkModele = :fkModele"),
    @NamedQuery(name = "ComplementBienAutomobile.findByEstVu", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.estVu = :estVu"),
    
    @NamedQuery(name = "ComplementBienAutomobile.findByEtat", query = "SELECT l FROM ComplementBienAutomobile l WHERE l.etat = :etat")})
public class ComplementBienAutomobile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "FK_BIEN")
    private String fkBien;

    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;

    @Column(name = "DATE_MISE_CIRCULATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMiseCirculation;

    @Column(name = "MARQUE")
    private String marque;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "ETAT")
    private Integer etat;

    @Column(name = "FK_MARQUE")
    private Integer fkMarque;

    @Column(name = "FK_MODELE")
    private Integer fkModele;

    @Column(name = "NUMERO_PLAQUE")
    private String numeroPlaque;

    @Column(name = "NUMERO_CHASSIS")
    private String numeroChassis;

    @Column(name = "ANNEE_MISE_CIRCULATION")
    private String anneeMiseCirculation;

    @Column(name = "PUISSANCE_FISCALE")
    private String puissanceFiscale;

    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;
    
    @Column(name = "EST_VU")
    private Integer estVu;

    @Column(name = "UNITE_PUISSANCE_FISCALE")
    private String unitePuissanceFiscale;

    public ComplementBienAutomobile() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkBien() {
        return fkBien;
    }

    public void setFkBien(String fkBien) {
        this.fkBien = fkBien;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getEstVu() {
        return estVu;
    }

    public void setEstVu(Integer estVu) {
        this.estVu = estVu;
    }
    
    

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getNumeroPlaque() {
        return numeroPlaque;
    }

    public void setNumeroPlaque(String numeroPlaque) {
        this.numeroPlaque = numeroPlaque;
    }

    public String getNumeroChassis() {
        return numeroChassis;
    }

    public void setNumeroChassis(String numeroChassis) {
        this.numeroChassis = numeroChassis;
    }

    public String getAnneeMiseCirculation() {
        return anneeMiseCirculation;
    }

    public void setAnneeMiseCirculation(String anneeMiseCirculation) {
        this.anneeMiseCirculation = anneeMiseCirculation;
    }

    public String getPuissanceFiscale() {
        return puissanceFiscale;
    }

    public void setPuissanceFiscale(String puissanceFiscale) {
        this.puissanceFiscale = puissanceFiscale;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public String getUnitePuissanceFiscale() {
        return unitePuissanceFiscale;
    }

    public void setUnitePuissanceFiscale(String unitePuissanceFiscale) {
        this.unitePuissanceFiscale = unitePuissanceFiscale;
    }

    public Date getDateMiseCirculation() {
        return dateMiseCirculation;
    }

    public void setDateMiseCirculation(Date dateMiseCirculation) {
        this.dateMiseCirculation = dateMiseCirculation;
    }

    public Integer getFkMarque() {
        return fkMarque;
    }

    public void setFkMarque(Integer fkMarque) {
        this.fkMarque = fkMarque;
    }

    public Integer getFkModele() {
        return fkModele;
    }

    public void setFkModele(Integer fkModele) {
        this.fkModele = fkModele;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComplementBienAutomobile)) {
            return false;
        }
        ComplementBienAutomobile other = (ComplementBienAutomobile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.ComplementBienAutomobile[ id=" + id + " ]";
    }
}
