/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_AFFECTATION_AGENT_KIT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AffectationAgentKit.findAll", query = "SELECT a FROM AffectationAgentKit a"),
    @NamedQuery(name = "AffectationAgentKit.findByCode", query = "SELECT a FROM AffectationAgentKit a WHERE a.code = :code"),
    @NamedQuery(name = "AffectationAgentKit.findByKit", query = "SELECT a FROM AffectationAgentKit a WHERE a.kit = :kit"),
    @NamedQuery(name = "AffectationAgentKit.findByAgent", query = "SELECT a FROM AffectationAgentKit a WHERE a.agent = :agent"),
    @NamedQuery(name = "AffectationAgentKit.findByEtat", query = "SELECT a FROM AffectationAgentKit a WHERE a.etat = :etat")})
public class AffectationAgentKit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Long code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "KIT")
    private int kit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "AGENT")
    private String agent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ETAT")
    private boolean etat;

    public AffectationAgentKit() {
    }

    public AffectationAgentKit(Long code) {
        this.code = code;
    }

    public AffectationAgentKit(Long code, int kit, String agent, boolean etat) {
        this.code = code;
        this.kit = kit;
        this.agent = agent;
        this.etat = etat;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public int getKit() {
        return kit;
    }

    public void setKit(int kit) {
        this.kit = kit;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AffectationAgentKit)) {
            return false;
        }
        AffectationAgentKit other = (AffectationAgentKit) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.AffectationAgentKit[ code=" + code + " ]";
    }
    
}
