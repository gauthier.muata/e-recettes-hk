/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_TYPE_ANNUAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeAnnuaire.findAll", query = "SELECT t FROM TypeAnnuaire t"),
    @NamedQuery(name = "TypeAnnuaire.findByCode", query = "SELECT t FROM TypeAnnuaire t WHERE t.code = :code"),
    @NamedQuery(name = "TypeAnnuaire.findByIntitule", query = "SELECT t FROM TypeAnnuaire t WHERE t.intitule = :intitule"),
    @NamedQuery(name = "TypeAnnuaire.findByEtat", query = "SELECT t FROM TypeAnnuaire t WHERE t.etat = :etat"),
    @NamedQuery(name = "TypeAnnuaire.findByAgentCreat", query = "SELECT t FROM TypeAnnuaire t WHERE t.agentCreat = :agentCreat"),
    @NamedQuery(name = "TypeAnnuaire.findByDateCreat", query = "SELECT t FROM TypeAnnuaire t WHERE t.dateCreat = :dateCreat"),
    @NamedQuery(name = "TypeAnnuaire.findByAgentMaj", query = "SELECT t FROM TypeAnnuaire t WHERE t.agentMaj = :agentMaj"),
    @NamedQuery(name = "TypeAnnuaire.findByDateMaj", query = "SELECT t FROM TypeAnnuaire t WHERE t.dateMaj = :dateMaj")})
public class TypeAnnuaire implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Integer code;
    @Size(max = 150)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    public TypeAnnuaire() {
    }

    public TypeAnnuaire(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeAnnuaire)) {
            return false;
        }
        TypeAnnuaire other = (TypeAnnuaire) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.TypeAnnuaire[ code=" + code + " ]";
    }
    
}
