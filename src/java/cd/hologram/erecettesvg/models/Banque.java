/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_BANQUE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Banque.findAll", query = "SELECT b FROM Banque b"),
    @NamedQuery(name = "Banque.findByCode", query = "SELECT b FROM Banque b WHERE b.code = :code"),
    @NamedQuery(name = "Banque.findByIntitule", query = "SELECT b FROM Banque b WHERE b.intitule = :intitule"),
    @NamedQuery(name = "Banque.findBySigle", query = "SELECT b FROM Banque b WHERE b.sigle = :sigle"),
    @NamedQuery(name = "Banque.findByEtat", query = "SELECT b FROM Banque b WHERE b.etat = :etat"),
    @NamedQuery(name = "Banque.findByAgentCreat", query = "SELECT b FROM Banque b WHERE b.agentCreat = :agentCreat"),
    @NamedQuery(name = "Banque.findByDateCreat", query = "SELECT b FROM Banque b WHERE b.dateCreat = :dateCreat"),
    @NamedQuery(name = "Banque.findByAgentMaj", query = "SELECT b FROM Banque b WHERE b.agentMaj = :agentMaj"),
    @NamedQuery(name = "Banque.findByDateMaj", query = "SELECT b FROM Banque b WHERE b.dateMaj = :dateMaj"),
    @NamedQuery(name = "Banque.findByCodeSuift", query = "SELECT b FROM Banque b WHERE b.codeSuift = :codeSuift"),
    @NamedQuery(name = "Banque.findByToken", query = "SELECT b FROM Banque b WHERE b.token = :token"),
    @NamedQuery(name = "Banque.findByEstConnecter", query = "SELECT b FROM Banque b WHERE b.estConnecter = :estConnecter")})
public class Banque implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "CODE")
    private String code;
    @Size(max = 50)
    @Column(name = "INTITULE")
    private String intitule;
    @Size(max = 20)
    @Column(name = "SIGLE")
    private String sigle;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Size(max = 20)
    @Column(name = "CODE_SUIFT")
    private String codeSuift;
    @Size(max = 200)
    @Column(name = "TOKEN")
    private String token;
    @Column(name = "EST_CONNECTER")
    private Boolean estConnecter;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "banque")
    private List<CompteBancaire> compteBancaireList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "banque")
    private List<SiteBanque> siteBanqueList;

    public Banque() {
    }

    public Banque(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getSigle() {
        return sigle;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public String getCodeSuift() {
        return codeSuift;
    }

    public void setCodeSuift(String codeSuift) {
        this.codeSuift = codeSuift;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getEstConnecter() {
        return estConnecter;
    }

    public void setEstConnecter(Boolean estConnecter) {
        this.estConnecter = estConnecter;
    }

    @XmlTransient
    public List<CompteBancaire> getCompteBancaireList() {
        return compteBancaireList;
    }

    public void setCompteBancaireList(List<CompteBancaire> compteBancaireList) {
        this.compteBancaireList = compteBancaireList;
    }

    @XmlTransient
    public List<SiteBanque> getSiteBanqueList() {
        return siteBanqueList;
    }

    public void setSiteBanqueList(List<SiteBanque> siteBanqueList) {
        this.siteBanqueList = siteBanqueList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Banque)) {
            return false;
        }
        Banque other = (Banque) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Banque[ code=" + code + " ]";
    }
    
}
