/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_INTERET_MORATOIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InteretMoratoire.findAll", query = "SELECT i FROM InteretMoratoire i"),
    @NamedQuery(name = "InteretMoratoire.findById", query = "SELECT i FROM InteretMoratoire i WHERE i.id = :id"),
    @NamedQuery(name = "InteretMoratoire.findByInteretGenere", query = "SELECT i FROM InteretMoratoire i WHERE i.interetGenere = :interetGenere"),
    @NamedQuery(name = "InteretMoratoire.findBySoldeTotal", query = "SELECT i FROM InteretMoratoire i WHERE i.soldeTotal = :soldeTotal"),
    @NamedQuery(name = "InteretMoratoire.findByDateCreat", query = "SELECT i FROM InteretMoratoire i WHERE i.dateCreat = :dateCreat"),
    @NamedQuery(name = "InteretMoratoire.findByMoisInteret", query = "SELECT i FROM InteretMoratoire i WHERE i.moisInteret = :moisInteret"),
    @NamedQuery(name = "InteretMoratoire.findByLibelle", query = "SELECT i FROM InteretMoratoire i WHERE i.libelle = :libelle"),
    @NamedQuery(name = "InteretMoratoire.findByFkAmr", query = "SELECT i FROM InteretMoratoire i WHERE i.fkAmr = :fkAmr")})
public class InteretMoratoire implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "INTERET_GENERE")
    private BigDecimal interetGenere;
    @Column(name = "SOLDE_TOTAL")
    private BigDecimal soldeTotal;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 50)
    @Column(name = "MOIS_INTERET")
    private String moisInteret;
    @Size(max = 500)
    @Column(name = "LIBELLE")
    private String libelle;
    @Size(max = 25)
    @Column(name = "FK_AMR")
    private String fkAmr;

    public InteretMoratoire() {
    }

    public InteretMoratoire(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getInteretGenere() {
        return interetGenere;
    }

    public void setInteretGenere(BigDecimal interetGenere) {
        this.interetGenere = interetGenere;
    }

    public BigDecimal getSoldeTotal() {
        return soldeTotal;
    }

    public void setSoldeTotal(BigDecimal soldeTotal) {
        this.soldeTotal = soldeTotal;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getMoisInteret() {
        return moisInteret;
    }

    public void setMoisInteret(String moisInteret) {
        this.moisInteret = moisInteret;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getFkAmr() {
        return fkAmr;
    }

    public void setFkAmr(String fkAmr) {
        this.fkAmr = fkAmr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InteretMoratoire)) {
            return false;
        }
        InteretMoratoire other = (InteretMoratoire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.InteretMoratoire[ id=" + id + " ]";
    }
    
}
