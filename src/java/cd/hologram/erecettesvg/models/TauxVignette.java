/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_TAUX_VIGNETTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TauxVignette.findAll", query = "SELECT l FROM TauxVignette l"),
    @NamedQuery(name = "TauxVignette.findById", query = "SELECT l FROM TauxVignette l WHERE l.id = :id"),
    @NamedQuery(name = "TauxVignette.findByFkFormeJuridique", query = "SELECT l FROM TauxVignette l WHERE l.fkFormeJuridique = :fkFormeJuridique"),
    @NamedQuery(name = "TauxVignette.findByFkTarif", query = "SELECT l FROM TauxVignette l WHERE l.fkTarif = :fkTarif"),
    @NamedQuery(name = "TauxVignette.findByVignette", query = "SELECT l FROM TauxVignette l WHERE l.vignette = :vignette"),
    @NamedQuery(name = "TauxVignette.findByTscr", query = "SELECT l FROM TauxVignette l WHERE l.tscr = :tscr"),
    @NamedQuery(name = "TauxVignette.findByPourcentageVignette", query = "SELECT l FROM TauxVignette l WHERE l.pourcentageVignette = :pourcentageVignette"),
    @NamedQuery(name = "TauxVignette.findByPourcentageTscr", query = "SELECT l FROM TauxVignette l WHERE l.pourcentageTscr = :pourcentageTscr"),
    @NamedQuery(name = "TauxVignette.findByEtat", query = "SELECT l FROM TauxVignette l WHERE l.etat = :etat")})
public class TauxVignette implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "FK_FORME_JURIDIQUE")
    private String fkFormeJuridique;

    @Column(name = "FK_TARIF")
    private String fkTarif;

    @Column(name = "VIGNETTE")
    private BigDecimal vignette;
    
    @Column(name = "TSCR")
    private BigDecimal tscr;

    @Column(name = "POURCENTAGE_VIGNETTE")
    private BigDecimal pourcentageVignette;
    
    @Column(name = "POURCENTAGE_TSCR")
    private BigDecimal pourcentageTscr;

    @Column(name = "ETAT")
    private Integer etat;

    public TauxVignette() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkFormeJuridique() {
        return fkFormeJuridique;
    }

    public void setFkFormeJuridique(String fkFormeJuridique) {
        this.fkFormeJuridique = fkFormeJuridique;
    }

    public String getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(String fkTarif) {
        this.fkTarif = fkTarif;
    }

    public BigDecimal getVignette() {
        return vignette;
    }

    public void setVignette(BigDecimal vignette) {
        this.vignette = vignette;
    }

    public BigDecimal getTscr() {
        return tscr;
    }

    public void setTscr(BigDecimal tscr) {
        this.tscr = tscr;
    }

    public BigDecimal getPourcentageVignette() {
        return pourcentageVignette;
    }

    public void setPourcentageVignette(BigDecimal pourcentageVignette) {
        this.pourcentageVignette = pourcentageVignette;
    }

    public BigDecimal getPourcentageTscr() {
        return pourcentageTscr;
    }

    public void setPourcentageTscr(BigDecimal pourcentageTscr) {
        this.pourcentageTscr = pourcentageTscr;
    }

    

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TauxVignette)) {
            return false;
        }
        TauxVignette other = (TauxVignette) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.TauxVignette[ id=" + id + " ]";
    }
}
