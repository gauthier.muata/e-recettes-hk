/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_MODELE_VEHICULE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ModeleVehicule.findAll", query = "SELECT m FROM ModeleVehicule m"),
    @NamedQuery(name = "ModeleVehicule.findById", query = "SELECT m FROM ModeleVehicule m WHERE m.id = :id"),
    @NamedQuery(name = "ModeleVehicule.findByEtat", query = "SELECT m FROM ModeleVehicule m WHERE m.etat = :etat"),
    @NamedQuery(name = "ModeleVehicule.findByIntitule", query = "SELECT m FROM ModeleVehicule m WHERE m.intitule = :intitule")})
public class ModeleVehicule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    
    @Size(max = 250)
    @Column(name = "INTITULE")
    private String intitule;
    
    @Column(name = "ETAT")
    private Integer etat;
    
    

    public ModeleVehicule() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModeleVehicule)) {
            return false;
        }
        ModeleVehicule other = (ModeleVehicule) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.ModeleVehicule[ code=" + id + " ]";
    }

}
