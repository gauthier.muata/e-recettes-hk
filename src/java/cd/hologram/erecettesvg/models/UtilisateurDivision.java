/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DEMONSTRATION
 */
@Entity
@Cacheable(false)
@Table(name = "T_UTILISATEUR_DIVISION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UtilisateurDivision.findAll", query = "SELECT u FROM UtilisateurDivision u"),
    @NamedQuery(name = "UtilisateurDivision.findByCode", query = "SELECT u FROM UtilisateurDivision u WHERE u.code = :code"),
    @NamedQuery(name = "UtilisateurDivision.findByFkPersonne", query = "SELECT u FROM UtilisateurDivision u WHERE u.fkPersonne = :fkPersonne"),
    @NamedQuery(name = "UtilisateurDivision.findByEtat", query = "SELECT u FROM UtilisateurDivision u WHERE u.etat = :etat")})
public class UtilisateurDivision implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Size(min = 1, max = 20)
    @Column(name = "CODE")
    private String code;
    @Size(max = 75)
    @Column(name = "ETAT")
    private Short etat;
    @JoinColumn(name = "FK_DIVISION", referencedColumnName = "CODE")
    @ManyToOne
    private Division division;
    @JoinColumn(name = "FK_SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site site;
    @Size(max = 25)
    @Column(name = "FK_PERSONNE")
    private String fkPersonne;

    public UtilisateurDivision() {
    }

    public UtilisateurDivision(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public Division getDivision() {
        return division;
    }

    public void setDivision(Division division) {
        this.division = division;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public String getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(String fkPersonne) {
        this.fkPersonne = fkPersonne;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UtilisateurDivision)) {
            return false;
        }
        UtilisateurDivision other = (UtilisateurDivision) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.UtilisateurDivision[ code=" + code + " ]";
    }

}
