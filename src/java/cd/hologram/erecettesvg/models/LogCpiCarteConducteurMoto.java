/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gauthier.muata
 */
@Entity
@Table(name = "T_LOG_CPI_CARTE_CONDUCTEUR_MOTO")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LogCpiCarteConducteurMoto.findAll", query = "SELECT l FROM LogCpiCarteConducteurMoto l"),
    @NamedQuery(name = "LogCpiCarteConducteurMoto.findById", query = "SELECT l FROM LogCpiCarteConducteurMoto l WHERE l.id = :id"),
    @NamedQuery(name = "LogCpiCarteConducteurMoto.findByCpi", query = "SELECT l FROM LogCpiCarteConducteurMoto l WHERE l.cpi = :cpi"),
    @NamedQuery(name = "LogCpiCarteConducteurMoto.findByDateLog", query = "SELECT l FROM LogCpiCarteConducteurMoto l WHERE l.dateLog = :dateLog"),
    @NamedQuery(name = "LogCpiCarteConducteurMoto.findByEtat", query = "SELECT l FROM LogCpiCarteConducteurMoto l WHERE l.etat = :etat")})
public class LogCpiCarteConducteurMoto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "CPI")
    private String cpi;
    @Column(name = "DATE_LOG")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLog;
    @Column(name = "ETAT")
    private Integer etat;

    public LogCpiCarteConducteurMoto() {
    }

    public LogCpiCarteConducteurMoto(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCpi() {
        return cpi;
    }

    public void setCpi(String cpi) {
        this.cpi = cpi;
    }

    public Date getDateLog() {
        return dateLog;
    }

    public void setDateLog(Date dateLog) {
        this.dateLog = dateLog;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogCpiCarteConducteurMoto)) {
            return false;
        }
        LogCpiCarteConducteurMoto other = (LogCpiCarteConducteurMoto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.LogCpiCarteConducteurMoto[ id=" + id + " ]";
    }
    
}
