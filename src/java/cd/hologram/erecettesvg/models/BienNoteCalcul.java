/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_BIEN_NOTE_CALCUL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BienNoteCalcul.findAll", query = "SELECT l FROM BienNoteCalcul l"),
    @NamedQuery(name = "BienNoteCalcul.findById", query = "SELECT l FROM BienNoteCalcul l WHERE l.id = :id"),
    @NamedQuery(name = "BienNoteCalcul.findByFkNc", query = "SELECT l FROM BienNoteCalcul l WHERE l.fkNc = :fkNc"),
    @NamedQuery(name = "BienNoteCalcul.findByFkBien", query = "SELECT l FROM BienNoteCalcul l WHERE l.fkBien = :fkBien"),
    @NamedQuery(name = "BienNoteCalcul.findByMessage", query = "SELECT l FROM BienNoteCalcul l WHERE l.message = :message"),
    @NamedQuery(name = "BienNoteCalcul.findByEtat", query = "SELECT l FROM BienNoteCalcul l WHERE l.etat = :etat")})

public class BienNoteCalcul implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "FK_NC")
    private String fkNc;

    @Column(name = "FK_BIEN")
    private String fkBien;

    @Column(name = "ETAT")
    private Integer etat;
    
    @Column(name = "MESSAGE")
    private String message;
    
    public BienNoteCalcul() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkNc() {
        return fkNc;
    }

    public void setFkNc(String fkNc) {
        this.fkNc = fkNc;
    }

    public String getFkBien() {
        return fkBien;
    }

    public void setFkBien(String fkBien) {
        this.fkBien = fkBien;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BienNoteCalcul)) {
            return false;
        }
        BienNoteCalcul other = (BienNoteCalcul) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.BienNoteCalcul[ id=" + id + " ]";
    }

}
