package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
*
* @author Administrateur
*/
@Entity
@Cacheable(false)
@Table(name = "T_DETAIL_VOUCHER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailVoucher.findAll", query = "SELECT d FROM DetailVoucher d"),
    @NamedQuery(name = "DetailVoucher.findById", query = "SELECT d FROM DetailVoucher d WHERE d.id = :id"),
    @NamedQuery(name = "DetailVoucher.findByCodeVoucher", query = "SELECT d FROM DetailVoucher d WHERE d.codeVoucher = :codeVoucher"),
    
    @NamedQuery(name = "DetailVoucher.findByCodeTicketGenerate", query = "SELECT d FROM DetailVoucher d WHERE d.codeTicketGenerate = :codeTicketGenerate"),
    @NamedQuery(name = "DetailVoucher.findByAgentUtilisation", query = "SELECT d FROM DetailVoucher d WHERE d.agentUtilisation = :agentUtilisation"),
    
    @NamedQuery(name = "DetailVoucher.findByDateGeneration", query = "SELECT d FROM DetailVoucher d WHERE d.dateGeneration = :dateGeneration"),
    @NamedQuery(name = "DetailVoucher.findByDateUtilisation", query = "SELECT d FROM DetailVoucher d WHERE d.dateUtilisation = :dateUtilisation"),
    @NamedQuery(name = "DetailVoucher.findByEtat", query = "SELECT d FROM DetailVoucher d WHERE d.etat = :etat")})
public class DetailVoucher implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "CODE_VOUCHER")
    private String codeVoucher;
    @Column(name = "DATE_GENERATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateGeneration;
    @Column(name = "DATE_UTILISATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUtilisation;
    
    @Column(name = "ETAT")
    private Integer etat;
    
    @Column(name = "CODE_TICKET_GENERATE")
    private String codeTicketGenerate;
    
    @Column(name = "AGENT_UTILISATION")
    private Integer agentUtilisation;
    
    @JoinColumn(name = "FK_VAUCHER", referencedColumnName = "ID")
    @ManyToOne
    private Voucher fkVaucher;
    @JoinColumn(name = "FK_SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site fkSite;

    public DetailVoucher() {
    }

    public DetailVoucher(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodeVoucher() {
        return codeVoucher;
    }

    public void setCodeVoucher(String codeVoucher) {
        this.codeVoucher = codeVoucher;
    }

    public Date getDateGeneration() {
        return dateGeneration;
    }

    public void setDateGeneration(Date dateGeneration) {
        this.dateGeneration = dateGeneration;
    }

    public Date getDateUtilisation() {
        return dateUtilisation;
    }

    public String getCodeTicketGenerate() {
        return codeTicketGenerate;
    }

    public void setCodeTicketGenerate(String codeTicketGenerate) {
        this.codeTicketGenerate = codeTicketGenerate;
    }

    public Integer getAgentUtilisation() {
        return agentUtilisation;
    }

    public void setAgentUtilisation(Integer agentUtilisation) {
        this.agentUtilisation = agentUtilisation;
    }
    
    

    public void setDateUtilisation(Date dateUtilisation) {
        this.dateUtilisation = dateUtilisation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Voucher getFkVaucher() {
        return fkVaucher;
    }

    public void setFkVaucher(Voucher fkVaucher) {
        this.fkVaucher = fkVaucher;
    }

    public Site getFkSite() {
        return fkSite;
    }

    public void setFkSite(Site fkSite) {
        this.fkSite = fkSite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailVoucher)) {
            return false;
        }
        DetailVoucher other = (DetailVoucher) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entites.DetailVoucher[ id=" + id + " ]";
    }
    
}
