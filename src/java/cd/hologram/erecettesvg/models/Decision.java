/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author WILLY
 */
@Entity
@Table(name = "T_DECISION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Decision.findAll", query = "SELECT d FROM Decision d"),
    @NamedQuery(name = "Decision.findById", query = "SELECT d FROM Decision d WHERE d.id = :id"),
    @NamedQuery(name = "Decision.findByLibelleDecision", query = "SELECT d FROM Decision d WHERE d.libelleDecision = :libelleDecision")})
public class Decision implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 150)
    @Column(name = "LIBELLE_DECISION")
    private String libelleDecision;
    @OneToMany(mappedBy = "fkDecision")
    private List<DetailsReclamation> detailsReclamationList;

    public Decision() {
    }

    public Decision(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelleDecision() {
        return libelleDecision;
    }

    public void setLibelleDecision(String libelleDecision) {
        this.libelleDecision = libelleDecision;
    }

    @XmlTransient
    public List<DetailsReclamation> getDetailsReclamationList() {
        return detailsReclamationList;
    }

    public void setDetailsReclamationList(List<DetailsReclamation> detailsReclamationList) {
        this.detailsReclamationList = detailsReclamationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Decision)) {
            return false;
        }
        Decision other = (Decision) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.Decision[ id=" + id + " ]";
    }
    
}
