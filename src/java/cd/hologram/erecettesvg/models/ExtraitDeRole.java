/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_EXTRAIT_DE_ROLE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExtraitDeRole.findAll", query = "SELECT e FROM ExtraitDeRole e"),
    @NamedQuery(name = "ExtraitDeRole.findById", query = "SELECT e FROM ExtraitDeRole e WHERE e.id = :id"),
    @NamedQuery(name = "ExtraitDeRole.findByDateCreat", query = "SELECT e FROM ExtraitDeRole e WHERE e.dateCreat = :dateCreat"),
    @NamedQuery(name = "ExtraitDeRole.findByDateReception", query = "SELECT e FROM ExtraitDeRole e WHERE e.dateReception = :dateReception"),
    @NamedQuery(name = "ExtraitDeRole.findByDateEcheance", query = "SELECT e FROM ExtraitDeRole e WHERE e.dateEcheance = :dateEcheance"),
    @NamedQuery(name = "ExtraitDeRole.findByOrdonnance", query = "SELECT e FROM ExtraitDeRole e WHERE e.ordonnance = :ordonnance"),
    @NamedQuery(name = "ExtraitDeRole.findByEtat", query = "SELECT e FROM ExtraitDeRole e WHERE e.etat = :etat"),
    @NamedQuery(name = "ExtraitDeRole.findByFkRole", query = "SELECT e FROM ExtraitDeRole e WHERE e.fkRole = :fkRole"),
    @NamedQuery(name = "ExtraitDeRole.findByFkSite", query = "SELECT e FROM ExtraitDeRole e WHERE e.fkSite = :fkSite"),
    @NamedQuery(name = "ExtraitDeRole.findByEstEnReclamation", query = "SELECT e FROM ExtraitDeRole e WHERE e.estEnReclamation = :estEnReclamation"),
    @NamedQuery(name = "ExtraitDeRole.findByArticleRole", query = "SELECT e FROM ExtraitDeRole e WHERE e.articleRole = :articleRole"),
    @NamedQuery(name = "ExtraitDeRole.findByFraisPoursuiteCmd", query = "SELECT e FROM ExtraitDeRole e WHERE e.fraisPoursuiteCmd = :fraisPoursuiteCmd"),
    @NamedQuery(name = "ExtraitDeRole.findByFraisPoursuiteAtdPv", query = "SELECT e FROM ExtraitDeRole e WHERE e.fraisPoursuiteAtdPv = :fraisPoursuiteAtdPv")})
public class ExtraitDeRole implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ID")
    private String id;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Column(name = "DATE_RECEPTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReception;
    @Column(name = "DATE_ECHEANCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheance;
    @Column(name = "ORDONNANCE")
    private Integer ordonnance;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 50)
    @Column(name = "FK_ROLE")
    private String fkRole;
    
    @Column(name = "FK_SITE")
    private String fkSite;
    
    
    @Column(name = "EST_EN_RECLAMATION")
    private Boolean estEnReclamation;
    @Size(max = 50)
    @Column(name = "ARTICLE_ROLE")
    private String articleRole;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FRAIS_POURSUITE_CMD")
    private BigDecimal fraisPoursuiteCmd;
    @Column(name = "FRAIS_POURSUITE_ATD_PV")
    private BigDecimal fraisPoursuiteAtdPv;
    @JoinColumn(name = "AGENT_CREAT", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentCreat;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;
    @OneToMany(mappedBy = "fkExtraitRole")
    private List<DetailsRole> detailsRoleList;
    @OneToMany(mappedBy = "fkExtraitRole")
    private List<DernierAvertissement> dernierAvertissementList;

    public ExtraitDeRole() {
    }

    public ExtraitDeRole(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public Integer getOrdonnance() {
        return ordonnance;
    }

    public void setOrdonnance(Integer ordonnance) {
        this.ordonnance = ordonnance;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getFkRole() {
        return fkRole;
    }

    public void setFkRole(String fkRole) {
        this.fkRole = fkRole;
    }

    public Boolean getEstEnReclamation() {
        return estEnReclamation;
    }

    public void setEstEnReclamation(Boolean estEnReclamation) {
        this.estEnReclamation = estEnReclamation;
    }

    public String getArticleRole() {
        return articleRole;
    }

    public void setArticleRole(String articleRole) {
        this.articleRole = articleRole;
    }

    public BigDecimal getFraisPoursuiteCmd() {
        return fraisPoursuiteCmd;
    }

    public void setFraisPoursuiteCmd(BigDecimal fraisPoursuiteCmd) {
        this.fraisPoursuiteCmd = fraisPoursuiteCmd;
    }

    public BigDecimal getFraisPoursuiteAtdPv() {
        return fraisPoursuiteAtdPv;
    }

    public void setFraisPoursuiteAtdPv(BigDecimal fraisPoursuiteAtdPv) {
        this.fraisPoursuiteAtdPv = fraisPoursuiteAtdPv;
    }

    public Agent getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Agent agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    public String getFkSite() {
        return fkSite;
    }

    public void setFkSite(String fkSite) {
        this.fkSite = fkSite;
    }
    
    
    

    @XmlTransient
    public List<DetailsRole> getDetailsRoleList() {
        return detailsRoleList;
    }

    public void setDetailsRoleList(List<DetailsRole> detailsRoleList) {
        this.detailsRoleList = detailsRoleList;
    }

    @XmlTransient
    public List<DernierAvertissement> getDernierAvertissementList() {
        return dernierAvertissementList;
    }

    public void setDernierAvertissementList(List<DernierAvertissement> dernierAvertissementList) {
        this.dernierAvertissementList = dernierAvertissementList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExtraitDeRole)) {
            return false;
        }
        ExtraitDeRole other = (ExtraitDeRole) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ExtraitDeRole[ id=" + id + " ]";
    }
    
}
