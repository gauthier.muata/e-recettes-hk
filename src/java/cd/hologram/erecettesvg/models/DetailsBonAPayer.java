/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_DETAILS_BON_A_PAYER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsBonAPayer.findAll", query = "SELECT d FROM DetailsBonAPayer d"),
    @NamedQuery(name = "DetailsBonAPayer.findByCode", query = "SELECT d FROM DetailsBonAPayer d WHERE d.code = :code"),
    @NamedQuery(name = "DetailsBonAPayer.findByMontant", query = "SELECT d FROM DetailsBonAPayer d WHERE d.montant = :montant"),
    @NamedQuery(name = "DetailsBonAPayer.findByFkArticleBudgetaire", query = "SELECT d FROM DetailsBonAPayer d WHERE d.fkArticleBudgetaire = :fkArticleBudgetaire"),
    @NamedQuery(name = "DetailsBonAPayer.findByFkBonAPayer", query = "SELECT d FROM DetailsBonAPayer d WHERE d.fkBonAPayer = :fkBonAPayer")})
public class DetailsBonAPayer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Integer code;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Size(max = 25)
    @Column(name = "FK_ARTICLE_BUDGETAIRE")
    private String fkArticleBudgetaire;
    @Size(max = 50)
    @Column(name = "FK_BON_A_PAYER")
    private String fkBonAPayer;

    public DetailsBonAPayer() {
    }

    public DetailsBonAPayer(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getFkArticleBudgetaire() {
        return fkArticleBudgetaire;
    }

    public void setFkArticleBudgetaire(String fkArticleBudgetaire) {
        this.fkArticleBudgetaire = fkArticleBudgetaire;
    }

    public String getFkBonAPayer() {
        return fkBonAPayer;
    }

    public void setFkBonAPayer(String fkBonAPayer) {
        this.fkBonAPayer = fkBonAPayer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsBonAPayer)) {
            return false;
        }
        DetailsBonAPayer other = (DetailsBonAPayer) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.DetailsBonAPayer[ code=" + code + " ]";
    }
    
}
