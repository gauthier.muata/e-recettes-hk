/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_TAXATION_BIEN_AUTOMOBILE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TaxationBienAutomobile.findAll", query = "SELECT l FROM TaxationBienAutomobile l"),
    @NamedQuery(name = "TaxationBienAutomobile.findById", query = "SELECT l FROM TaxationBienAutomobile l WHERE l.id = :id"),
    @NamedQuery(name = "TaxationBienAutomobile.findByFkBien", query = "SELECT l FROM TaxationBienAutomobile l WHERE l.fkBien = :fkBien"),
    @NamedQuery(name = "TaxationBienAutomobile.findByFkNoteTaxation", query = "SELECT l FROM TaxationBienAutomobile l WHERE l.fkNoteTaxation = :fkNoteTaxation"),
    @NamedQuery(name = "TaxationBienAutomobile.findByPrincipalDu", query = "SELECT l FROM TaxationBienAutomobile l WHERE l.principalDu = :principalDu"),
    @NamedQuery(name = "TaxationBienAutomobile.findByPenaliteDu", query = "SELECT l FROM TaxationBienAutomobile l WHERE l.penaliteDu = :penaliteDu"),
    @NamedQuery(name = "TaxationBienAutomobile.findByExercice", query = "SELECT l FROM TaxationBienAutomobile l WHERE l.exercice = :exercice"),
    @NamedQuery(name = "TaxationBienAutomobile.findByEtat", query = "SELECT l FROM TaxationBienAutomobile l WHERE l.etat = :etat")})
public class TaxationBienAutomobile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "FK_NOTE_TAXATION")
    private String fkNoteTaxation;

    @Column(name = "FK_BIEN")
    private String fkBien;

    @Column(name = "PRINCIPAL_DU")
    private BigDecimal principalDu;
    
    @Column(name = "PENALITE_DU")
    private BigDecimal penaliteDu;

    @Column(name = "EXERCICE")
    private String exercice;

    @Column(name = "ETAT")
    private Integer etat;

    public TaxationBienAutomobile() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkBien() {
        return fkBien;
    }

    public void setFkBien(String fkBien) {
        this.fkBien = fkBien;
    }

    public String getFkNoteTaxation() {
        return fkNoteTaxation;
    }

    public void setFkNoteTaxation(String fkNoteTaxation) {
        this.fkNoteTaxation = fkNoteTaxation;
    }

    public BigDecimal getPrincipalDu() {
        return principalDu;
    }

    public void setPrincipalDu(BigDecimal principalDu) {
        this.principalDu = principalDu;
    }

    public BigDecimal getPenaliteDu() {
        return penaliteDu;
    }

    public void setPenaliteDu(BigDecimal penaliteDu) {
        this.penaliteDu = penaliteDu;
    }
    
    public String getExercice() {
        return exercice;
    }

    public void setExercice(String exercice) {
        this.exercice = exercice;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TaxationBienAutomobile)) {
            return false;
        }
        TaxationBienAutomobile other = (TaxationBienAutomobile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.TaxationBienAutomobile[ id=" + id + " ]";
    }
}
