/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_AGENT_AUTORITE_BUREAU")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AgentAutoriteBureau.findAll", query = "SELECT l FROM AgentAutoriteBureau l"),
    @NamedQuery(name = "AgentAutoriteBureau.findById", query = "SELECT l FROM AgentAutoriteBureau l WHERE l.id = :id"),
    @NamedQuery(name = "AgentAutoriteBureau.findByFkSite", query = "SELECT l FROM AgentAutoriteBureau l WHERE l.fkSite = :fkSite"),
    @NamedQuery(name = "AgentAutoriteBureau.findByFkAb", query = "SELECT l FROM AgentAutoriteBureau l WHERE l.fkAb = :fkAb"),

    @NamedQuery(name = "AgentAutoriteBureau.findByNomResponsable", query = "SELECT l FROM AgentAutoriteBureau l WHERE l.nomResponsable = :nomResponsable"),
    @NamedQuery(name = "AgentAutoriteBureau.findByQualiteResponsable", query = "SELECT l FROM AgentAutoriteBureau l WHERE l.qualiteResponsable = :qualiteResponsable"),

    @NamedQuery(name = "AgentAutoriteBureau.findByNomCollaborateur", query = "SELECT l FROM AgentAutoriteBureau l WHERE l.nomCollaborateur = :nomCollaborateur"),
    @NamedQuery(name = "AgentAutoriteBureau.findByQualiteCollaborateur", query = "SELECT l FROM AgentAutoriteBureau l WHERE l.qualiteCollaborateur = :qualiteCollaborateur"),
    @NamedQuery(name = "AgentAutoriteBureau.findByAdresseBureau", query = "SELECT l FROM AgentAutoriteBureau l WHERE l.adresseBureau = :adresseBureau"),

    @NamedQuery(name = "AgentAutoriteBureau.findByEtat", query = "SELECT l FROM AgentAutoriteBureau l WHERE l.etat = :etat")})

public class AgentAutoriteBureau implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "FK_SITE")
    private String fkSite;

    @Column(name = "FK_AB")
    private String fkAb;

    @Column(name = "NOM_RESPONSABLE")
    private String nomResponsable;

    @Column(name = "QUALITE_RESPONSABLE")
    private String qualiteResponsable;

    @Column(name = "NOM_COLLABORATEUR")
    private String nomCollaborateur;

    @Column(name = "QUALITE_COLLABORATEUR")
    private String qualiteCollaborateur;

    @Column(name = "ADRESSE_BUREAU")
    private String adresseBureau;

    @Column(name = "ETAT")
    private Integer etat;

    public AgentAutoriteBureau() {
    }

    public AgentAutoriteBureau(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkSite() {
        return fkSite;
    }

    public void setFkSite(String fkSite) {
        this.fkSite = fkSite;
    }

    public String getAdresseBureau() {
        return adresseBureau;
    }

    public void setAdresseBureau(String adresseBureau) {
        this.adresseBureau = adresseBureau;
    }

    public String getFkAb() {
        return fkAb;
    }

    public void setFkAb(String fkAb) {
        this.fkAb = fkAb;
    }

    public String getNomResponsable() {
        return nomResponsable;
    }

    public void setNomResponsable(String nomResponsable) {
        this.nomResponsable = nomResponsable;
    }

    public String getQualiteResponsable() {
        return qualiteResponsable;
    }

    public void setQualiteResponsable(String qualiteResponsable) {
        this.qualiteResponsable = qualiteResponsable;
    }

    public String getNomCollaborateur() {
        return nomCollaborateur;
    }

    public void setNomCollaborateur(String nomCollaborateur) {
        this.nomCollaborateur = nomCollaborateur;
    }

    public String getQualiteCollaborateur() {
        return qualiteCollaborateur;
    }

    public void setQualiteCollaborateur(String qualiteCollaborateur) {
        this.qualiteCollaborateur = qualiteCollaborateur;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgentAutoriteBureau)) {
            return false;
        }
        AgentAutoriteBureau other = (AgentAutoriteBureau) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.AgentAutoriteBureau[ id=" + id + " ]";
    }
}
