/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_TAUX_IF_EXERCICE_ANTERIEUR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TauxIfExerciceAnterieur.findAll", query = "SELECT l FROM TauxIfExerciceAnterieur l"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findById", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.id = :id"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByAnnee", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.annee = :annee"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByFkTarif", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.fkTarif = :fkTarif"),

    @NamedQuery(name = "TauxIfExerciceAnterieur.findByFkTypeBien", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.fkTypeBien= :fkTypeBien"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByFkFormeJuridique", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.fkFormeJuridique = :fkFormeJuridique"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByTaux", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.taux= :taux"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByUnite", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.unite = :unite"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByBase", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.base = :base"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByFkAb", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.fkAb = :fkAb"),
    
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByDateMaj", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.dateMaj = :dateMaj"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByAgentMaj", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.agentMaj = :agentMaj"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByEtat", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.etat = :etat"),
    
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByMultiplierParVb", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.multiplierParVb = :multiplierParVb"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByAgentCreate", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.agentCreate = :agentCreate"),
    @NamedQuery(name = "TauxIfExerciceAnterieur.findByDateCreate", query = "SELECT l FROM TauxIfExerciceAnterieur l WHERE l.dateCreate = :dateCreate")})

public class TauxIfExerciceAnterieur implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "ANNEE")
    private Integer annee;

    @Column(name = "FK_TARIF")
    private String fkTarif;

    @Column(name = "FK_TYPE_BIEN")
    private String fkTypeBien;

    @Column(name = "FK_FORME_JURIDIQUE")
    private String fkFormeJuridique;

    @Column(name = "TAUX")
    private BigDecimal taux;

    @Column(name = "UNITE")
    private String unite;

    @Column(name = "MULTIPLIER_PAR_VB")
    private Integer multiplierParVb;

    @Column(name = "BASE")
    private String base;

    @Column(name = "FK_AB")
    private String fkAb;

    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;

    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    
    @Column(name = "ETAT")
    private Integer etat;
    
    @Column(name = "AGENT_MAJ")
    private Integer agentMaj;

    public TauxIfExerciceAnterieur() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAnnee() {
        return annee;
    }

    public void setAnnee(Integer annee) {
        this.annee = annee;
    }

    public String getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(String fkTarif) {
        this.fkTarif = fkTarif;
    }

    public String getFkTypeBien() {
        return fkTypeBien;
    }

    public void setFkTypeBien(String fkTypeBien) {
        this.fkTypeBien = fkTypeBien;
    }

    public String getFkFormeJuridique() {
        return fkFormeJuridique;
    }

    public void setFkFormeJuridique(String fkFormeJuridique) {
        this.fkFormeJuridique = fkFormeJuridique;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Integer agentMaj) {
        this.agentMaj = agentMaj;
    }
    
    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getFkAb() {
        return fkAb;
    }

    public void setFkAb(String fkAb) {
        this.fkAb = fkAb;
    }
    
    
    

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public Integer getMultiplierParVb() {
        return multiplierParVb;
    }

    public void setMultiplierParVb(Integer multiplierParVb) {
        this.multiplierParVb = multiplierParVb;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TauxIfExerciceAnterieur)) {
            return false;
        }
        TauxIfExerciceAnterieur other = (TauxIfExerciceAnterieur) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.TauxIfExerciceAnterieur[ id=" + id + " ]";
    }
}
