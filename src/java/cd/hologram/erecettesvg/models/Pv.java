///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package cd.hologram.erecettesvg.models;
//
//import java.io.Serializable;
//import java.util.Date;
//import java.util.List;
//import javax.persistence.Basic;
//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.OneToMany;
//import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlTransient;
//
///**
// *
// * @author WILLY
// */
//@Entity
//@Table(name = "T_PV")
//@Cacheable(false)
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Pv.findAll", query = "SELECT p FROM Pv p"),
//    @NamedQuery(name = "Pv.findById", query = "SELECT p FROM Pv p WHERE p.id = :id"),
//    @NamedQuery(name = "Pv.findByNumeroPv", query = "SELECT p FROM Pv p WHERE p.numeroPv = :numeroPv"),
//    @NamedQuery(name = "Pv.findByAgentValider", query = "SELECT p FROM Pv p WHERE p.agentValider = :agentValider"),
//    @NamedQuery(name = "Pv.findByEtat", query = "SELECT p FROM Pv p WHERE p.etat = :etat"),
//    @NamedQuery(name = "Pv.findByAgentCreat", query = "SELECT p FROM Pv p WHERE p.agentCreat = :agentCreat"),
//    @NamedQuery(name = "Pv.findByDateCreat", query = "SELECT n FROM Pv n WHERE n.dateCreat = :dateCreat")})
//public class Pv implements Serializable {
//
//    private static final long serialVersionUID = 1L;
//    @Id
//    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 50)
//    @Column(name = "ID")
//    private String id;
//
//    @Column(name = "NUMERO_PV")
//    private String numeroPv;
//    @Size(max = 50)
//
//    @Column(name = "AGENT_VALIDER")
//    private Integer agentValider;
//    @Size(max = 50)
//
//    @Column(name = "ETAT")
//    private Short etat;
//    
//    @Column(name = "AGENT_CREAT")
//    private int agentCreat;
//
//    @Column(name = "OBSERVATION")
//    private String observation;
//
//    @Column(name = "DATE_CREAT")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date dateCreat;
//
//    @OneToMany(mappedBy = "fkPv")
//    private List<DetailsMission> detailsMissionList;
//    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
//    @ManyToOne
//    private Personne fkPersonne;
//
//    public Pv() {
//    }
//
//    public Pv(String id) {
//        this.id = id;
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getNumeroPv() {
//        return numeroPv;
//    }
//
//    public void setNumeroPv(String numeroPv) {
//        this.numeroPv = numeroPv;
//    }
//
//    public Integer getAgentValider() {
//        return agentValider;
//    }
//
//    public void setAgentValider(Integer agentValider) {
//        this.agentValider = agentValider;
//    }
//
//    public Short getEtat() {
//        return etat;
//    }
//
//    public void setEtat(Short etat) {
//        this.etat = etat;
//    }
//
//    public String getObservation() {
//        return observation;
//    }
//
//    public void setObservation(String observation) {
//        this.observation = observation;
//    }
//
//    public int getAgentCreat() {
//        return agentCreat;
//    }
//
//    public void setAgentCreat(int agentCreat) {
//        this.agentCreat = agentCreat;
//    }
//    
//    
//
//    public Date getDateCreat() {
//        return dateCreat;
//    }
//
//    public void setDateCreat(Date dateCreat) {
//        this.dateCreat = dateCreat;
//    }
//
//    @XmlTransient
//    public List<DetailsMission> getDetailsMissionList() {
//        return detailsMissionList;
//    }
//
//    public void setDetailsMissionList(List<DetailsMission> detailsMissionList) {
//        this.detailsMissionList = detailsMissionList;
//    }
//
//    public Personne getFkPersonne() {
//        return fkPersonne;
//    }
//
//    public void setFkPersonne(Personne fkPersonne) {
//        this.fkPersonne = fkPersonne;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Pv)) {
//            return false;
//        }
//        Pv other = (Pv) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "cd.hologram.erecettesvg.models.Pv[ id=" + id + " ]";
//    }
//
//}
