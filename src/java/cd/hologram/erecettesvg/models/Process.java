/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PROCESS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Process.findAll", query = "SELECT p FROM Process p"),
    @NamedQuery(name = "Process.findById", query = "SELECT p FROM Process p WHERE p.id = :id"),
    @NamedQuery(name = "Process.findByProcess", query = "SELECT p FROM Process p WHERE p.process = :process"),
    @NamedQuery(name = "Process.findByOrdre", query = "SELECT p FROM Process p WHERE p.ordre = :ordre"),
    @NamedQuery(name = "Process.findByWorkFlow", query = "SELECT p FROM Process p WHERE p.workFlow = :workFlow"),
    @NamedQuery(name = "Process.findByUa", query = "SELECT p FROM Process p WHERE p.ua = :ua"),
    @NamedQuery(name = "Process.findByUaOk", query = "SELECT p FROM Process p WHERE p.uaOk = :uaOk"),
    @NamedQuery(name = "Process.findByUaKo", query = "SELECT p FROM Process p WHERE p.uaKo = :uaKo"),
    @NamedQuery(name = "Process.findByDuree", query = "SELECT p FROM Process p WHERE p.duree = :duree"),
    @NamedQuery(name = "Process.findByEcheance", query = "SELECT p FROM Process p WHERE p.echeance = :echeance"),
    @NamedQuery(name = "Process.findByObservation", query = "SELECT p FROM Process p WHERE p.observation = :observation"),
    @NamedQuery(name = "Process.findByControlSolde", query = "SELECT p FROM Process p WHERE p.controlSolde = :controlSolde"),
    @NamedQuery(name = "Process.findByAgentCreat", query = "SELECT p FROM Process p WHERE p.agentCreat = :agentCreat"),
    @NamedQuery(name = "Process.findByDateCreat", query = "SELECT p FROM Process p WHERE p.dateCreat = :dateCreat"),
    @NamedQuery(name = "Process.findByAgentMaj", query = "SELECT p FROM Process p WHERE p.agentMaj = :agentMaj"),
    @NamedQuery(name = "Process.findByDateMaj", query = "SELECT p FROM Process p WHERE p.dateMaj = :dateMaj"),
    @NamedQuery(name = "Process.findByEtat", query = "SELECT p FROM Process p WHERE p.etat = :etat"),
    @NamedQuery(name = "Process.findByDescription", query = "SELECT p FROM Process p WHERE p.description = :description"),
    @NamedQuery(name = "Process.findByMontant", query = "SELECT p FROM Process p WHERE p.montant = :montant"),
    @NamedQuery(name = "Process.findByMetier", query = "SELECT p FROM Process p WHERE p.metier = :metier")})
public class Process implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 20)
    @Column(name = "PROCESS")
    private String process;
    @Size(max = 3)
    @Column(name = "ORDRE")
    private String ordre;
    @Column(name = "WORK_FLOW")
    private BigInteger workFlow;
    @Size(max = 20)
    @Column(name = "UA")
    private String ua;
    @Size(max = 20)
    @Column(name = "UA_OK")
    private String uaOk;
    @Size(max = 20)
    @Column(name = "UA_KO")
    private String uaKo;
    @Column(name = "DUREE")
    private Integer duree;
    @Size(max = 10)
    @Column(name = "ECHEANCE")
    private String echeance;
    @Size(max = 255)
    @Column(name = "OBSERVATION")
    private String observation;
    @Column(name = "CONTROL_SOLDE")
    private Short controlSolde;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Size(max = 50)
    @Column(name = "METIER")
    private String metier;
    @JoinColumn(name = "DOSSIER", referencedColumnName = "NUMERO")
    @ManyToOne
    private Dossier dossier;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "process2")
    private Process process1;
    @JoinColumn(name = "ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Process process2;

    public Process() {
    }

    public Process(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getOrdre() {
        return ordre;
    }

    public void setOrdre(String ordre) {
        this.ordre = ordre;
    }

    public BigInteger getWorkFlow() {
        return workFlow;
    }

    public void setWorkFlow(BigInteger workFlow) {
        this.workFlow = workFlow;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public String getUaOk() {
        return uaOk;
    }

    public void setUaOk(String uaOk) {
        this.uaOk = uaOk;
    }

    public String getUaKo() {
        return uaKo;
    }

    public void setUaKo(String uaKo) {
        this.uaKo = uaKo;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public String getEcheance() {
        return echeance;
    }

    public void setEcheance(String echeance) {
        this.echeance = echeance;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Short getControlSolde() {
        return controlSolde;
    }

    public void setControlSolde(Short controlSolde) {
        this.controlSolde = controlSolde;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getMetier() {
        return metier;
    }

    public void setMetier(String metier) {
        this.metier = metier;
    }

    public Dossier getDossier() {
        return dossier;
    }

    public void setDossier(Dossier dossier) {
        this.dossier = dossier;
    }

    public Process getProcess1() {
        return process1;
    }

    public void setProcess1(Process process1) {
        this.process1 = process1;
    }

    public Process getProcess2() {
        return process2;
    }

    public void setProcess2(Process process2) {
        this.process2 = process2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Process)) {
            return false;
        }
        Process other = (Process) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Process[ id=" + id + " ]";
    }
    
}
