/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PENALITE_MAJOREE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PenaliteMajoree.findAll", query = "SELECT p FROM PenaliteMajoree p"),
    @NamedQuery(name = "PenaliteMajoree.findByCode", query = "SELECT p FROM PenaliteMajoree p WHERE p.code = :code"),
    @NamedQuery(name = "PenaliteMajoree.findByMontant", query = "SELECT p FROM PenaliteMajoree p WHERE p.montant = :montant"),
    @NamedQuery(name = "PenaliteMajoree.findByDateCreat", query = "SELECT p FROM PenaliteMajoree p WHERE p.dateCreat = :dateCreat")})
public class PenaliteMajoree implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CODE")
    private String code;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @JoinColumn(name = "AMR2", referencedColumnName = "NUMERO")
    @ManyToOne
    private Amr amr2;

    public PenaliteMajoree() {
    }

    public PenaliteMajoree(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Amr getAmr2() {
        return amr2;
    }

    public void setAmr2(Amr amr2) {
        this.amr2 = amr2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PenaliteMajoree)) {
            return false;
        }
        PenaliteMajoree other = (PenaliteMajoree) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.PenaliteMajoree[ code=" + code + " ]";
    }
    
}
