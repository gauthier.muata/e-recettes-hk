/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MUATA
 */
@Entity
@Cacheable(false)
@Table(name = "T_LOG_SCAN_TICKET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LogScanTicket.findAll", query = "SELECT l FROM LogScanTicket l"),
    @NamedQuery(name = "LogScanTicket.findById", query = "SELECT l FROM LogScanTicket l WHERE l.id = :id"),
    @NamedQuery(name = "LogScanTicket.findByCodeTicket", query = "SELECT l FROM LogScanTicket l WHERE l.codeTicket = :codeTicket"),
    @NamedQuery(name = "LogScanTicket.findByFkSiteTicket", query = "SELECT l FROM LogScanTicket l WHERE l.fkSiteTicket = :fkSiteTicket"),
    @NamedQuery(name = "LogScanTicket.findByFkSiteScan", query = "SELECT l FROM LogScanTicket l WHERE l.fkSiteScan = :fkSiteScan"),
    @NamedQuery(name = "LogScanTicket.findByFkTarif", query = "SELECT l FROM LogScanTicket l WHERE l.fkTarif = :fkTarif"),

    @NamedQuery(name = "LogScanTicket.findByMontant", query = "SELECT l FROM LogScanTicket l WHERE l.montant = :montant"),
    @NamedQuery(name = "LogScanTicket.findByDevise", query = "SELECT l FROM LogScanTicket l WHERE l.devise = :devise"),
    @NamedQuery(name = "LogScanTicket.findByTypePaiement", query = "SELECT l FROM LogScanTicket l WHERE l.typePaiement = :typePaiement"),
    @NamedQuery(name = "LogScanTicket.findByPlaque", query = "SELECT l FROM LogScanTicket l WHERE l.plaque = :plaque"),
    @NamedQuery(name = "LogScanTicket.findByAgentScan", query = "SELECT l FROM LogScanTicket l WHERE l.agentScan = :agentScan"),

    @NamedQuery(name = "LogScanTicket.findByDateScan", query = "SELECT l FROM LogScanTicket l WHERE l.dateScan = :dateScan")})

public class LogScanTicket implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "CODE_TICKET")
    private String codeTicket;

    @Column(name = "FK_SITE_TICKET")
    private String fkSiteTicket;

    @Column(name = "FK_SITE_SCAN")
    private String fkSiteScan;

    @Column(name = "FK_TARIF")
    private String fkTarif;

    @Column(name = "MONTANT")
    private BigDecimal montant;

    @Column(name = "DEVISE")
    private String devise;

    @Column(name = "TYPE_PAIEMENT")
    private String typePaiement;

    @Column(name = "PLAQUE")
    private String plaque;

    @Column(name = "AGENT_SCAN")
    private Integer agentScan;

    @Column(name = "DATE_SCAN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateScan;

    public LogScanTicket() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodeTicket() {
        return codeTicket;
    }

    public void setCodeTicket(String codeTicket) {
        this.codeTicket = codeTicket;
    }

    public String getFkSiteTicket() {
        return fkSiteTicket;
    }

    public void setFkSiteTicket(String fkSiteTicket) {
        this.fkSiteTicket = fkSiteTicket;
    }

    public String getFkSiteScan() {
        return fkSiteScan;
    }

    public void setFkSiteScan(String fkSiteScan) {
        this.fkSiteScan = fkSiteScan;
    }

    public String getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(String fkTarif) {
        this.fkTarif = fkTarif;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getTypePaiement() {
        return typePaiement;
    }

    public void setTypePaiement(String typePaiement) {
        this.typePaiement = typePaiement;
    }

    public String getPlaque() {
        return plaque;
    }

    public void setPlaque(String plaque) {
        this.plaque = plaque;
    }

    public Integer getAgentScan() {
        return agentScan;
    }

    public void setAgentScan(Integer agentScan) {
        this.agentScan = agentScan;
    }

    public Date getDateScan() {
        return dateScan;
    }

    public void setDateScan(Date dateScan) {
        this.dateScan = dateScan;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogScanTicket)) {
            return false;
        }
        LogScanTicket other = (LogScanTicket) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.LogScanTicket[ id=" + id + " ]";
    }
}
