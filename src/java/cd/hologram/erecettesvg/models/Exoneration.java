/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juslin TSHIAMUA
 */
@Entity
@Table(name = "T_EXONERATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Exoneration.findAll", query = "SELECT e FROM Exoneration e"),
    @NamedQuery(name = "Exoneration.findByCode", query = "SELECT e FROM Exoneration e WHERE e.code = :code"),
    @NamedQuery(name = "Exoneration.findByMontant", query = "SELECT e FROM Exoneration e WHERE e.montant = :montant"),
    @NamedQuery(name = "Exoneration.findByMontantExonere", query = "SELECT e FROM Exoneration e WHERE e.montantExonere = :montantExonere"),
    @NamedQuery(name = "Exoneration.findByObservation", query = "SELECT e FROM Exoneration e WHERE e.observation = :observation"),
    @NamedQuery(name = "Exoneration.findByDateCreation", query = "SELECT e FROM Exoneration e WHERE e.dateCreation = :dateCreation"),
    @NamedQuery(name = "Exoneration.findByAgentMaj", query = "SELECT e FROM Exoneration e WHERE e.agentMaj = :agentMaj"),
    @NamedQuery(name = "Exoneration.findByDateMaj", query = "SELECT e FROM Exoneration e WHERE e.dateMaj = :dateMaj"),
    @NamedQuery(name = "Exoneration.findByEtat", query = "SELECT e FROM Exoneration e WHERE e.etat = :etat"),
    @NamedQuery(name = "Exoneration.findByArchive", query = "SELECT e FROM Exoneration e WHERE e.archive = :archive")})
public class Exoneration implements Serializable {

    @Size(max = 500)
    @Column(name = "OBSERVATION")
    private String observation;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Column(name = "MONTANT_EXONERE")
    private BigDecimal montantExonere;
    @JoinColumn(name = "AGENT_CREAT", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentCreat;
    @JoinColumn(name = "AGENT_MAJ", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentMaj;
    @JoinColumn(name = "FK_ARTICLE_BUDGETAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire fkArticleBudgetaire;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    @JoinColumn(name = "FK_PERIODE_DECLARATION", referencedColumnName = "ID")
    @ManyToOne
    private PeriodeDeclaration fkPeriodeDeclaration;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;
    @JoinColumn(name = "FK_SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site fkSite;
    @JoinColumn(name = "FK_BIEN", referencedColumnName = "ID")
    @ManyToOne
    private Bien fkBien;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Integer etat;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ARCHIVE")
    private String archive;

    public Exoneration() {
    }

    public Exoneration(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    public ArticleBudgetaire getFkArticleBudgetaire() {
        return fkArticleBudgetaire;
    }

    public void setFkArticleBudgetaire(ArticleBudgetaire fkArticleBudgetaire) {
        this.fkArticleBudgetaire = fkArticleBudgetaire;
    }

    public PeriodeDeclaration getFkPeriodeDeclaration() {
        return fkPeriodeDeclaration;
    }

    public void setFkPeriodeDeclaration(PeriodeDeclaration fkPeriodeDeclaration) {
        this.fkPeriodeDeclaration = fkPeriodeDeclaration;
    }

    public Agent getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Agent agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Agent getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Agent agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public Site getFkSite() {
        return fkSite;
    }

    public void setFkSite(Site fkSite) {
        this.fkSite = fkSite;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getArchive() {
        return archive;
    }

    public void setArchive(String archive) {
        this.archive = archive;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public BigDecimal getMontantExonere() {
        return montantExonere;
    }

    public void setMontantExonere(BigDecimal montantExonere) {
        this.montantExonere = montantExonere;
    }

    public Bien getFkBien() {
        return fkBien;
    }

    public void setFkBien(Bien fkBien) {
        this.fkBien = fkBien;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Exoneration)) {
            return false;
        }
        Exoneration other = (Exoneration) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Exoneration[ code=" + code + " ]";
    }

}
