/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_AGENT_SERVICE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AgentService.findAll", query = "SELECT l FROM AgentService l"),
    @NamedQuery(name = "AgentService.findById", query = "SELECT l FROM AgentService l WHERE l.id = :id"),
    @NamedQuery(name = "AgentService.findByDateCreate", query = "SELECT l FROM AgentService l WHERE l.dateCreate = :dateCreate"),
    @NamedQuery(name = "AgentService.findByEtat", query = "SELECT l FROM AgentService l WHERE l.etat = :etat"),
    @NamedQuery(name = "AgentService.findByFkService", query = "SELECT l FROM AgentService l WHERE l.fkService = :fkService"),
    @NamedQuery(name = "AgentService.findByFkAgent", query = "SELECT l FROM AgentService l WHERE l.fkAgent = :fkAgent")})
public class AgentService implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;

    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;

    @Column(name = "FK_AGENT")
    private Integer fkAgent;

    @Column(name = "FK_SERVICE")
    private String fkService;

    @Column(name = "ETAT")
    private Integer etat;

    public AgentService() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getFkAgent() {
        return fkAgent;
    }

    public void setFkAgent(Integer fkAgent) {
        this.fkAgent = fkAgent;
    }

    public String getFkService() {
        return fkService;
    }

    public void setFkService(String fkService) {
        this.fkService = fkService;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgentService)) {
            return false;
        }
        AgentService other = (AgentService) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.AgentService[ id=" + id + " ]";
    }

}
