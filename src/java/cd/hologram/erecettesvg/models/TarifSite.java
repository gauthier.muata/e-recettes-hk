/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_TARIF_SITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TarifSite.findAll", query = "SELECT l FROM TarifSite l"),
    @NamedQuery(name = "TarifSite.findById", query = "SELECT l FROM TarifSite l WHERE l.id = :id"),
    @NamedQuery(name = "TarifSite.findByFkSiteProvenance", query = "SELECT l FROM TarifSite l WHERE l.fkSiteProvenance = :fkSiteProvenance"),
    @NamedQuery(name = "TarifSite.findByFkSiteDestination", query = "SELECT l FROM TarifSite l WHERE l.fkSiteDestination = :fkSiteDestination"),
    @NamedQuery(name = "TarifSite.findByFkTarif", query = "SELECT l FROM TarifSite l WHERE l.fkTarif = :fkTarif"),
    @NamedQuery(name = "TarifSite.findByFkDevise", query = "SELECT l FROM TarifSite l WHERE l.fkDevise = :fkDevise"),

    @NamedQuery(name = "TarifSite.findByAgentCreate", query = "SELECT l FROM TarifSite l WHERE l.agentCreate = :agentCreate"),
    @NamedQuery(name = "TarifSite.findByDateCreate", query = "SELECT l FROM TarifSite l WHERE l.dateCreate = :dateCreate"),
    @NamedQuery(name = "TarifSite.findByAgentMaj", query = "SELECT l FROM TarifSite l WHERE l.agentMaj = :agentMaj"),
    @NamedQuery(name = "TarifSite.findByFkDateMaj", query = "SELECT l FROM TarifSite l WHERE l.dateMaj = :dateMaj"),

    @NamedQuery(name = "TarifSite.findByTaux", query = "SELECT l FROM TarifSite l WHERE l.taux = :taux"),
    @NamedQuery(name = "TarifSite.findByEtat", query = "SELECT l FROM TarifSite l WHERE l.etat = :etat")})

public class TarifSite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;

    @Column(name = "FK_SITE_PROVENANCE")
    private String fkSiteProvenance;

    @Column(name = "FK_SITE_DESTINANTION")
    private String fkSiteDestination;

    @Column(name = "FK_TARIF")
    private String fkTarif;

    @Column(name = "FK_DEVISE")
    private String fkDevise;

    @Column(name = "TAUX")
    private BigDecimal taux;

    @Column(name = "ETAT")
    private Integer etat;

    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;

    @Column(name = "AGENT_MAJ")
    private Integer agentMaj;

    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;

    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    public TarifSite() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(String fkTarif) {
        this.fkTarif = fkTarif;
    }

    public String getFkSiteProvenance() {
        return fkSiteProvenance;
    }

    public void setFkSiteProvenance(String fkSiteProvenance) {
        this.fkSiteProvenance = fkSiteProvenance;
    }

    public String getFkSiteDestination() {
        return fkSiteDestination;
    }

    public void setFkSiteDestination(String fkSiteDestination) {
        this.fkSiteDestination = fkSiteDestination;
    }

    public String getFkDevise() {
        return fkDevise;
    }

    public void setFkDevise(String fkDevise) {
        this.fkDevise = fkDevise;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public Integer getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Integer agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TarifSite)) {
            return false;
        }
        TarifSite other = (TarifSite) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.TarifSite[ id=" + id + " ]";
    }
}
