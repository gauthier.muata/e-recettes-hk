/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PLAGE_CATEGORIE_BIEN_AUTOMOBILE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlageCategorieBienAutomobile.findAll", query = "SELECT m FROM PlageCategorieBienAutomobile m"),
    @NamedQuery(name = "PlageCategorieBienAutomobile.findById", query = "SELECT m FROM PlageCategorieBienAutomobile m WHERE m.id = :id"),

    @NamedQuery(name = "PlageCategorieBienAutomobile.findByEtat", query = "SELECT m FROM PlageCategorieBienAutomobile m WHERE m.etat = :etat"),

    @NamedQuery(name = "PlageCategorieBienAutomobile.findByFkUnite", query = "SELECT m FROM PlageCategorieBienAutomobile m WHERE m.fkUnite = :fkUnite"),
    @NamedQuery(name = "PlageCategorieBienAutomobile.findByPlageMin", query = "SELECT m FROM PlageCategorieBienAutomobile m WHERE m.plageMin = :plageMin"),
    @NamedQuery(name = "PlageCategorieBienAutomobile.findByPlageMax", query = "SELECT m FROM PlageCategorieBienAutomobile m WHERE m.plageMax = :plageMax"),
    @NamedQuery(name = "PlageCategorieBienAutomobile.findByFkTypeBien", query = "SELECT m FROM PlageCategorieBienAutomobile m WHERE m.fkTypeBien = :fkTypeBien"),

    @NamedQuery(name = "PlageCategorieBienAutomobile.findByFkTarif", query = "SELECT m FROM PlageCategorieBienAutomobile m WHERE m.fkTarif = :fkTarif")})
public class PlageCategorieBienAutomobile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;

    @Size(max = 50)
    @Column(name = "FK_TARIF")
    private String fkTarif;

    @Size(max = 50)
    @Column(name = "FK_UNITE")
    private String fkUnite;
    
    @Size(max = 50)
    @Column(name = "FK_TYPE_BIEN")
    private String fkTypeBien;

    @Column(name = "ETAT")
    private Integer etat;

    @Column(name = "PLAGE_MIN")
    private BigDecimal plageMin;

    @Column(name = "PLAGE_MAX")
    private BigDecimal plageMax;

    public PlageCategorieBienAutomobile() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(String fkTarif) {
        this.fkTarif = fkTarif;
    }

    public String getFkUnite() {
        return fkUnite;
    }

    public void setFkUnite(String fkUnite) {
        this.fkUnite = fkUnite;
    }

    public BigDecimal getPlageMin() {
        return plageMin;
    }

    public void setPlageMin(BigDecimal plageMin) {
        this.plageMin = plageMin;
    }

    public BigDecimal getPlageMax() {
        return plageMax;
    }

    public void setPlageMax(BigDecimal plageMax) {
        this.plageMax = plageMax;
    }

    public String getFkTypeBien() {
        return fkTypeBien;
    }

    public void setFkTypeBien(String fkTypeBien) {
        this.fkTypeBien = fkTypeBien;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlageCategorieBienAutomobile)) {
            return false;
        }
        PlageCategorieBienAutomobile other = (PlageCategorieBienAutomobile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.PlageCategorieBienAutomobile[ code=" + id + " ]";
    }

}
