/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author WILLY
 */
@Cacheable(false)
@Entity
@Table(name = "T_ARCHIVE_TAXATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArchiveTaxation.findAll", query = "SELECT a FROM ArchiveTaxation a"),
    @NamedQuery(name = "ArchiveTaxation.findById", query = "SELECT a FROM ArchiveTaxation a WHERE a.id = :id")})
public class ArchiveTaxation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ARCHIVE")
    private String archive;

    @JoinColumn(name = "FK_NC", referencedColumnName = "NUMERO")
    @ManyToOne
    private NoteCalcul fkNc;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "OBSERVATION")
    private String observation;

    @Size(max = 250)
    @Column(name = "DIRECTORY")
    private String directory;

    public ArchiveTaxation() {
    }

    public ArchiveTaxation(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArchive() {
        return archive;
    }

    public void setArchive(String archive) {
        this.archive = archive;
    }

    public NoteCalcul getFkNc() {
        return fkNc;
    }

    public void setFkNc(NoteCalcul fkNc) {
        this.fkNc = fkNc;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArchiveTaxation)) {
            return false;
        }
        ArchiveTaxation other = (ArchiveTaxation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.ArchiveTaxation[ id=" + id + " ]";
    }

}
