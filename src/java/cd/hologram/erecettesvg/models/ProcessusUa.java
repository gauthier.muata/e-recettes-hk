/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PROCESSUS_UA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProcessusUa.findAll", query = "SELECT p FROM ProcessusUa p"),
    @NamedQuery(name = "ProcessusUa.findById", query = "SELECT p FROM ProcessusUa p WHERE p.id = :id"),
    @NamedQuery(name = "ProcessusUa.findByUa", query = "SELECT p FROM ProcessusUa p WHERE p.ua = :ua"),
    @NamedQuery(name = "ProcessusUa.findByProcess", query = "SELECT p FROM ProcessusUa p WHERE p.process = :process"),
    @NamedQuery(name = "ProcessusUa.findByAgentCreat", query = "SELECT p FROM ProcessusUa p WHERE p.agentCreat = :agentCreat"),
    @NamedQuery(name = "ProcessusUa.findByDateCreat", query = "SELECT p FROM ProcessusUa p WHERE p.dateCreat = :dateCreat"),
    @NamedQuery(name = "ProcessusUa.findByAgentMaj", query = "SELECT p FROM ProcessusUa p WHERE p.agentMaj = :agentMaj"),
    @NamedQuery(name = "ProcessusUa.findByDateMaj", query = "SELECT p FROM ProcessusUa p WHERE p.dateMaj = :dateMaj"),
    @NamedQuery(name = "ProcessusUa.findByEtat", query = "SELECT p FROM ProcessusUa p WHERE p.etat = :etat")})
public class ProcessusUa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 20)
    @Column(name = "UA")
    private String ua;
    @Size(max = 20)
    @Column(name = "PROCESS")
    private String process;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;

    public ProcessusUa() {
    }

    public ProcessusUa(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcessusUa)) {
            return false;
        }
        ProcessusUa other = (ProcessusUa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.ProcessusUa[ id=" + id + " ]";
    }
    
}
