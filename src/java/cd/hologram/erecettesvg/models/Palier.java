/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_PALIER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Palier.findAll", query = "SELECT p FROM Palier p"),
    @NamedQuery(name = "Palier.findByCode", query = "SELECT p FROM Palier p WHERE p.code = :code"),
    @NamedQuery(name = "Palier.findByBorneInferieure", query = "SELECT p FROM Palier p WHERE p.borneInferieure = :borneInferieure"),
    @NamedQuery(name = "Palier.findByBorneSuperieure", query = "SELECT p FROM Palier p WHERE p.borneSuperieure = :borneSuperieure"),
    @NamedQuery(name = "Palier.findByTaux", query = "SELECT p FROM Palier p WHERE p.taux = :taux"),
    @NamedQuery(name = "Palier.findByTypeTaux", query = "SELECT p FROM Palier p WHERE p.typeTaux = :typeTaux"),
    @NamedQuery(name = "Palier.findByEtat", query = "SELECT p FROM Palier p WHERE p.etat = :etat"),
    @NamedQuery(name = "Palier.findByAgentCreat", query = "SELECT p FROM Palier p WHERE p.agentCreat = :agentCreat"),
    @NamedQuery(name = "Palier.findByFkEntiteAdministrative", query = "SELECT p FROM Palier p WHERE p.fkEntiteAdministrative = :fkEntiteAdministrative"),
    @NamedQuery(name = "Palier.findByDateCreat", query = "SELECT p FROM Palier p WHERE p.dateCreat = :dateCreat"),
    @NamedQuery(name = "Palier.findByAgentMaj", query = "SELECT p FROM Palier p WHERE p.agentMaj = :agentMaj"),
    @NamedQuery(name = "Palier.findByDateMaj", query = "SELECT p FROM Palier p WHERE p.dateMaj = :dateMaj"),
    @NamedQuery(name = "Palier.findByFkUsage", query = "SELECT p FROM Palier p WHERE p.fkUsage = :fkUsage"),
    @NamedQuery(name = "Palier.findByFkTypeBien", query = "SELECT p FROM Palier p WHERE p.fkTypeBien = :fkTypeBien"),
    @NamedQuery(name = "Palier.findByAnneeFiscale", query = "SELECT p FROM Palier p WHERE p.anneeFiscale = :anneeFiscale"),
    @NamedQuery(name = "Palier.findByMultiplierValeurBase", query = "SELECT p FROM Palier p WHERE p.multiplierValeurBase = :multiplierValeurBase"),
    @NamedQuery(name = "Palier.findByUnite", query = "SELECT p FROM Palier p WHERE p.unite = :unite")})
public class Palier implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Integer code;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BORNE_INFERIEURE")
    private BigDecimal borneInferieure;
    @Column(name = "BORNE_SUPERIEURE")
    private BigDecimal borneSuperieure;
    @Column(name = "TAUX")
    private BigDecimal taux;
    @Size(max = 1)
    @Column(name = "TYPE_TAUX")
    private String typeTaux;
    
    @Column(name = "FK_USAGE")
    private String fkUsage;
    
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "MULTIPLIER_VALEUR_BASE")
    private Short multiplierValeurBase;
    @Size(max = 25)
    @Column(name = "FK_ENTITE_ADMINISTRATIVE")
    private String fkEntiteAdministrative;
    @Size(max = 10)
    @Column(name = "FK_TYPE_BIEN")
    private String fkTypeBien;   
    
    @Column(name = "ANNEE_FISCALE")
    private String anneeFiscale;   
    
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    @JoinColumn(name = "ARTICLE_BUDGETAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire articleBudgetaire;
    @JoinColumn(name = "TYPE_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private FormeJuridique typePersonne;
    @JoinColumn(name = "TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif tarif;

    @JoinColumn(name = "UNITE", referencedColumnName = "CODE")
    @ManyToOne
    private Unite unite;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "palier")
    private List<Retribution> retributionList;

    public Palier() {
    }

    public Palier(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public BigDecimal getBorneInferieure() {
        return borneInferieure;
    }

    public void setBorneInferieure(BigDecimal borneInferieure) {
        this.borneInferieure = borneInferieure;
    }

    public BigDecimal getBorneSuperieure() {
        return borneSuperieure;
    }

    public void setBorneSuperieure(BigDecimal borneSuperieure) {
        this.borneSuperieure = borneSuperieure;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public String getTypeTaux() {
        return typeTaux;
    }

    public void setTypeTaux(String typeTaux) {
        this.typeTaux = typeTaux;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getMultiplierValeurBase() {
        return multiplierValeurBase;
    }

    public void setMultiplierValeurBase(Short multiplierValeurBase) {
        this.multiplierValeurBase = multiplierValeurBase;
    }

    public String getFkEntiteAdministrative() {
        return fkEntiteAdministrative;
    }

    public void setFkEntiteAdministrative(String fkEntiteAdministrative) {
        this.fkEntiteAdministrative = fkEntiteAdministrative;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public String getFkUsage() {
        return fkUsage;
    }

    public void setFkUsage(String fkUsage) {
        this.fkUsage = fkUsage;
    }
    
    
    public Unite getUnite() {
        return unite;
    }

    public void setUnite(Unite unite) {
        this.unite = unite;
    }

    public ArticleBudgetaire getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(ArticleBudgetaire articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public FormeJuridique getTypePersonne() {
        return typePersonne;
    }

    public void setTypePersonne(FormeJuridique typePersonne) {
        this.typePersonne = typePersonne;
    }

    public Tarif getTarif() {
        return tarif;
    }

    public void setTarif(Tarif tarif) {
        this.tarif = tarif;
    }

    public String getFkTypeBien() {
        return fkTypeBien;
    }

    public void setFkTypeBien(String fkTypeBien) {
        this.fkTypeBien = fkTypeBien;
    }

    public String getAnneeFiscale() {
        return anneeFiscale;
    }

    public void setAnneeFiscale(String anneeFiscale) {
        this.anneeFiscale = anneeFiscale;
    }
    
    
    
    
    @XmlTransient
    public List<Retribution> getRetributionList() {
        return retributionList;
    }

    public void setRetributionList(List<Retribution> retributionList) {
        this.retributionList = retributionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Palier)) {
            return false;
        }
        Palier other = (Palier) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Palier[ code=" + code + " ]";
    }

}
