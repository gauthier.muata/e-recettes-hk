/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_LOG_NOTE_TAXATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LogNoteTaxation.findAll", query = "SELECT l FROM LogNoteTaxation l"),
    @NamedQuery(name = "LogNoteTaxation.findById", query = "SELECT l FROM LogNoteTaxation l WHERE l.id = :id"),
    @NamedQuery(name = "LogNoteTaxation.findByAction", query = "SELECT l FROM LogNoteTaxation l WHERE l.action = :action"),
    @NamedQuery(name = "LogNoteTaxation.findByFkCodeAgent", query = "SELECT l FROM LogNoteTaxation l WHERE l.codeAgent = :codeAgent"),
    @NamedQuery(name = "LogNoteTaxation.findByFkNomAgent", query = "SELECT l FROM LogNoteTaxation l WHERE l.nomAgent = :nomAgent"),
    @NamedQuery(name = "LogNoteTaxation.findByFkDateCreate", query = "SELECT l FROM LogNoteTaxation l WHERE l.dateCreate = :dateCreate"),
    @NamedQuery(name = "LogNoteTaxation.findByNoteTaxation", query = "SELECT l FROM LogNoteTaxation l WHERE l.noteTaxation = :noteTaxation"),
    @NamedQuery(name = "LogNoteTaxation.findByObservation", query = "SELECT l FROM LogNoteTaxation l WHERE l.observation = :observation")})

public class LogNoteTaxation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "ACTION")
    private String action;

    @Column(name = "CODE_AGENT")
    private Integer codeAgent;

    @Column(name = "NOM_AGENT")
    private String nomAgent;

    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;

    @Column(name = "NOTE_TAXATION")
    private String noteTaxation;

    @Column(name = "OBSERVATION")
    private String observation;

    public LogNoteTaxation() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getCodeAgent() {
        return codeAgent;
    }

    public void setCodeAgent(Integer codeAgent) {
        this.codeAgent = codeAgent;
    }

    public String getNomAgent() {
        return nomAgent;
    }

    public void setNomAgent(String nomAgent) {
        this.nomAgent = nomAgent;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getNoteTaxation() {
        return noteTaxation;
    }

    public void setNoteTaxation(String noteTaxation) {
        this.noteTaxation = noteTaxation;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
    
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogNoteTaxation)) {
            return false;
        }
        LogNoteTaxation other = (LogNoteTaxation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.LogNoteTaxation[ id=" + id + " ]";
    }
}
