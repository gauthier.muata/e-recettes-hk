///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package cd.hologram.erecettesvg.models;
//
//import java.io.Serializable;
//import java.util.Date;
//import java.util.List;
//import javax.persistence.Basic;
//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.OneToMany;
//import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlTransient;
//
///**
// *
// * @author WILLY
// */
//@Entity
//@Table(name = "T_EQUIPE_MISSION")
//@Cacheable(false)
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "EquipeMission.findAll", query = "SELECT e FROM EquipeMission e"),
//    @NamedQuery(name = "EquipeMission.findById", query = "SELECT e FROM EquipeMission e WHERE e.id = :id"),
//    @NamedQuery(name = "EquipeMission.findByLibelleEquipe", query = "SELECT e FROM EquipeMission e WHERE e.libelleEquipe = :libelleEquipe"),
//    @NamedQuery(name = "EquipeMission.findByDateCreat", query = "SELECT e FROM EquipeMission e WHERE e.dateCreat = :dateCreat"),
//    @NamedQuery(name = "EquipeMission.findByEtat", query = "SELECT e FROM EquipeMission e WHERE e.etat = :etat")})
//public class EquipeMission implements Serializable {
//    private static final long serialVersionUID = 1L;
//    @Id
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "ID")
//    private Integer id;
//    @Size(max = 150)
//    @Column(name = "LIBELLE_EQUIPE")
//    private String libelleEquipe;
//    @Column(name = "DATE_CREAT")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date dateCreat;
//    @Column(name = "ETAT")
//    private Integer etat;
//    @JoinColumn(name = "AGENT_CREAT", referencedColumnName = "CODE")
//    @ManyToOne
//    private Agent agentCreat;
//    @JoinColumn(name = "FK_MISSION", referencedColumnName = "ID")
//    @ManyToOne
//    private Mission fkMission;
//    @OneToMany(mappedBy = "fkEquipeMission")
//    private List<DetailsEquipeMission> detailsEquipeMissionList;
//    @OneToMany(mappedBy = "fkEquipeMission")
//    private List<DetailsMission> detailsMissionList;
//
//    public EquipeMission() {
//    }
//
//    public EquipeMission(Integer id) {
//        this.id = id;
//    }
//
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public String getLibelleEquipe() {
//        return libelleEquipe;
//    }
//
//    public void setLibelleEquipe(String libelleEquipe) {
//        this.libelleEquipe = libelleEquipe;
//    }
//
//    public Date getDateCreat() {
//        return dateCreat;
//    }
//
//    public void setDateCreat(Date dateCreat) {
//        this.dateCreat = dateCreat;
//    }
//
//    public Integer getEtat() {
//        return etat;
//    }
//
//    public void setEtat(Integer etat) {
//        this.etat = etat;
//    }
//
//    public Agent getAgentCreat() {
//        return agentCreat;
//    }
//
//    public void setAgentCreat(Agent agentCreat) {
//        this.agentCreat = agentCreat;
//    }
//
//    public Mission getFkMission() {
//        return fkMission;
//    }
//
//    public void setFkMission(Mission fkMission) {
//        this.fkMission = fkMission;
//    }
//
//    @XmlTransient
//    public List<DetailsEquipeMission> getDetailsEquipeMissionList() {
//        return detailsEquipeMissionList;
//    }
//
//    public void setDetailsEquipeMissionList(List<DetailsEquipeMission> detailsEquipeMissionList) {
//        this.detailsEquipeMissionList = detailsEquipeMissionList;
//    }
//
//    @XmlTransient
//    public List<DetailsMission> getDetailsMissionList() {
//        return detailsMissionList;
//    }
//
//    public void setDetailsMissionList(List<DetailsMission> detailsMissionList) {
//        this.detailsMissionList = detailsMissionList;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof EquipeMission)) {
//            return false;
//        }
//        EquipeMission other = (EquipeMission) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "cd.hologram.erecettesvg.models.EquipeMission[ id=" + id + " ]";
//    }
//    
//}
