/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PARAM_PENALITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParamPenalite.findAll", query = "SELECT p FROM ParamPenalite p"),
    @NamedQuery(name = "ParamPenalite.findByEtape", query = "SELECT p FROM ParamPenalite p WHERE p.etape = :etape"),
    @NamedQuery(name = "ParamPenalite.findByPenalite", query = "SELECT p FROM ParamPenalite p WHERE p.penalite = :penalite")})
public class ParamPenalite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "Etape")
    private String etape;
    @Size(max = 25)
    @Column(name = "PENALITE")
    private String penalite;

    public ParamPenalite() {
    }

    public ParamPenalite(String etape) {
        this.etape = etape;
    }

    public String getEtape() {
        return etape;
    }

    public void setEtape(String etape) {
        this.etape = etape;
    }

    public String getPenalite() {
        return penalite;
    }

    public void setPenalite(String penalite) {
        this.penalite = penalite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (etape != null ? etape.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParamPenalite)) {
            return false;
        }
        ParamPenalite other = (ParamPenalite) object;
        if ((this.etape == null && other.etape != null) || (this.etape != null && !this.etape.equals(other.etape))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.ParamPenalite[ etape=" + etape + " ]";
    }
    
}
