/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_USAGE_BIEN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsageBien.findAll", query = "SELECT l FROM UsageBien l"),
    @NamedQuery(name = "UsageBien.findById", query = "SELECT l FROM UsageBien l WHERE l.id = :id"),
    @NamedQuery(name = "UsageBien.findByIntitule", query = "SELECT l FROM UsageBien l WHERE l.intitule = :intitule"),
    @NamedQuery(name = "UsageBien.findByEtat", query = "SELECT l FROM UsageBien l WHERE l.etat = :etat")})
public class UsageBien implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id")
    private Integer id;

    @Column(name = "Intitule")
    private String intitule;

    @Column(name = "Etat")
    private Integer etat;

    public UsageBien() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsageBien)) {
            return false;
        }
        UsageBien other = (UsageBien) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.UsageBien[ id=" + id + " ]";
    }

}
