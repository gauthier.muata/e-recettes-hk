/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_DETAIL_FICHE_PRISE_CHARGE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailFichePriseCharge.findAll", query = "SELECT d FROM DetailFichePriseCharge d"),
    @NamedQuery(name = "DetailFichePriseCharge.findByCode", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.code = :code"),
    @NamedQuery(name = "DetailFichePriseCharge.findByPeriodeDeclaration", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.periodeDeclaration = :periodeDeclaration"),
    @NamedQuery(name = "DetailFichePriseCharge.findByNc", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.nc = :nc"),
    @NamedQuery(name = "DetailFichePriseCharge.findByBase", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.base = :base"),
    @NamedQuery(name = "DetailFichePriseCharge.findByTaux", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.taux = :taux"),
    @NamedQuery(name = "DetailFichePriseCharge.findByPrincipaldu", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.principaldu = :principaldu"),
    @NamedQuery(name = "DetailFichePriseCharge.findByPenalitedu", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.penalitedu = :penalitedu"),
    @NamedQuery(name = "DetailFichePriseCharge.findByEtat", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.etat = :etat"),
    @NamedQuery(name = "DetailFichePriseCharge.findByTypeTaux", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.typeTaux = :typeTaux"),
    @NamedQuery(name = "DetailFichePriseCharge.findByAmende", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.amende = :amende"),
    @NamedQuery(name = "DetailFichePriseCharge.findByMajoration", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.majoration = :majoration"),
    @NamedQuery(name = "DetailFichePriseCharge.findByExercice", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.exercice = :exercice"),
    @NamedQuery(name = "DetailFichePriseCharge.findByPenalite", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.penalite = :penalite"),
    @NamedQuery(name = "DetailFichePriseCharge.findByNbreRetardMois", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.nbreRetardMois = :nbreRetardMois"),
    @NamedQuery(name = "DetailFichePriseCharge.findByTauxPenalite", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.tauxPenalite = :tauxPenalite"),
    @NamedQuery(name = "DetailFichePriseCharge.findByMontantDu", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.montantDu = :montantDu"),
    @NamedQuery(name = "DetailFichePriseCharge.findByDegrevement", query = "SELECT d FROM DetailFichePriseCharge d WHERE d.degrevement = :degrevement")})
public class DetailFichePriseCharge implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Long code;
    @Column(name = "PERIODE_DECLARATION")
    private Integer periodeDeclaration;
    @Size(max = 25)
    @Column(name = "NC")
    private String nc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BASE")
    private BigDecimal base;
    @Column(name = "TAUX")
    private BigDecimal taux;
    @Column(name = "PRINCIPALDU")
    private BigDecimal principaldu;
    @Column(name = "PENALITEDU")
    private BigDecimal penalitedu;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 2)
    @Column(name = "TYPE_TAUX")
    private String typeTaux;
    @Column(name = "AMENDE")
    private BigDecimal amende;
    @Column(name = "MAJORATION")
    private BigDecimal majoration;
    @Size(max = 10)
    @Column(name = "EXERCICE")
    private String exercice;
    
    @Column(name = "PENALITE")
    private String penalite;
    
    @Column(name = "NBRE_RETARD_MOIS")
    private String nbreRetardMois;
     
    @Column(name = "TAUX_PENALITE")
    private BigDecimal tauxPenalite;
    @Column(name = "MONTANT_DU")
    private BigDecimal montantDu;
    @Column(name = "DEGREVEMENT")
    private BigDecimal degrevement;
    @JoinColumn(name = "ARTICLE_BUDGETAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire articleBudgetaire;
    @JoinColumn(name = "FICHE_PRISE_CHARGE", referencedColumnName = "CODE")
    @ManyToOne
    private FichePriseCharge fichePriseCharge;

    public DetailFichePriseCharge() {
    }

    public DetailFichePriseCharge(Long code) {
        this.code = code;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getPenalite() {
        return penalite;
    }

    public void setPenalite(String penalite) {
        this.penalite = penalite;
    }

    public String getNbreRetardMois() {
        return nbreRetardMois;
    }

    public void setNbreRetardMois(String nbreRetardMois) {
        this.nbreRetardMois = nbreRetardMois;
    }
    
    

    public Integer getPeriodeDeclaration() {
        return periodeDeclaration;
    }

    public void setPeriodeDeclaration(Integer periodeDeclaration) {
        this.periodeDeclaration = periodeDeclaration;
    }

    public String getNc() {
        return nc;
    }

    public void setNc(String nc) {
        this.nc = nc;
    }

    public BigDecimal getBase() {
        return base;
    }

    public void setBase(BigDecimal base) {
        this.base = base;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public BigDecimal getPrincipaldu() {
        return principaldu;
    }

    public void setPrincipaldu(BigDecimal principaldu) {
        this.principaldu = principaldu;
    }

    public BigDecimal getPenalitedu() {
        return penalitedu;
    }

    public void setPenalitedu(BigDecimal penalitedu) {
        this.penalitedu = penalitedu;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getTypeTaux() {
        return typeTaux;
    }

    public void setTypeTaux(String typeTaux) {
        this.typeTaux = typeTaux;
    }

    public BigDecimal getAmende() {
        return amende;
    }

    public void setAmende(BigDecimal amende) {
        this.amende = amende;
    }

    public BigDecimal getMajoration() {
        return majoration;
    }

    public void setMajoration(BigDecimal majoration) {
        this.majoration = majoration;
    }

    public String getExercice() {
        return exercice;
    }

    public void setExercice(String exercice) {
        this.exercice = exercice;
    }

    public BigDecimal getTauxPenalite() {
        return tauxPenalite;
    }

    public void setTauxPenalite(BigDecimal tauxPenalite) {
        this.tauxPenalite = tauxPenalite;
    }

    public BigDecimal getMontantDu() {
        return montantDu;
    }

    public void setMontantDu(BigDecimal montantDu) {
        this.montantDu = montantDu;
    }

    public BigDecimal getDegrevement() {
        return degrevement;
    }

    public void setDegrevement(BigDecimal degrevement) {
        this.degrevement = degrevement;
    }

    public ArticleBudgetaire getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(ArticleBudgetaire articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public FichePriseCharge getFichePriseCharge() {
        return fichePriseCharge;
    }

    public void setFichePriseCharge(FichePriseCharge fichePriseCharge) {
        this.fichePriseCharge = fichePriseCharge;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailFichePriseCharge)) {
            return false;
        }
        DetailFichePriseCharge other = (DetailFichePriseCharge) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.DetailFichePriseCharge[ code=" + code + " ]";
    }
    
}
