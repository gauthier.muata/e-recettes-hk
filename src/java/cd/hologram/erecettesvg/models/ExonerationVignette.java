/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_EXONERATION_VIGNETTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExonerationVignette.findAll", query = "SELECT l FROM ExonerationVignette l"),
    @NamedQuery(name = "ExonerationVignette.findById", query = "SELECT l FROM ExonerationVignette l WHERE l.id = :id"),
    @NamedQuery(name = "ExonerationVignette.findByFkBien", query = "SELECT l FROM ExonerationVignette l WHERE l.fkBien = :fkBien"),
    @NamedQuery(name = "ExonerationVignette.findByTypeExoneration", query = "SELECT l FROM ExonerationVignette l WHERE l.typeExoneration = :typeExoneration"),

    @NamedQuery(name = "ExonerationVignette.findByFkAb", query = "SELECT l FROM ExonerationVignette l WHERE l.fkAb = :fkAb"),

    @NamedQuery(name = "ExonerationVignette.findByDateCreate", query = "SELECT l FROM ExonerationVignette l WHERE l.dateCreate = :dateCreate"),

    @NamedQuery(name = "ExonerationVignette.findByAgentCreate", query = "SELECT l FROM ExonerationVignette l WHERE l.agentCreate = :agentCreate"),
    
    @NamedQuery(name = "ExonerationVignette.findByAgentMaj", query = "SELECT l FROM ExonerationVignette l WHERE l.agentMaj = :agentMaj"),
    @NamedQuery(name = "ExonerationVignette.findByDateMaj", query = "SELECT l FROM ExonerationVignette l WHERE l.dateMaj = :dateMaj"),
    
    @NamedQuery(name = "ExonerationVignette.findByEtat", query = "SELECT l FROM ExonerationVignette l WHERE l.etat = :etat")})
public class ExonerationVignette implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "FK_BIEN")
    private String fkBien;

    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;

    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    @Column(name = "FK_PERSONNE")
    private String fkPersonne;

    @Column(name = "FK_AB")
    private String fkAb;

    @Column(name = "ETAT")
    private Integer etat;
    
    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;
    
    @Column(name = "AGENT_MAJ")
    private Integer agentMaj;

    @Column(name = "TYPE_EXONERATION")
    private Integer typeExoneration;

    public ExonerationVignette() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkBien() {
        return fkBien;
    }

    public void setFkBien(String fkBien) {
        this.fkBien = fkBien;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public String getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(String fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    public String getFkAb() {
        return fkAb;
    }

    public void setFkAb(String fkAb) {
        this.fkAb = fkAb;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public Integer getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Integer agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Integer getTypeExoneration() {
        return typeExoneration;
    }

    public void setTypeExoneration(Integer typeExoneration) {
        this.typeExoneration = typeExoneration;
    }

    

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExonerationVignette)) {
            return false;
        }
        ExonerationVignette other = (ExonerationVignette) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.ExonerationVignette[ id=" + id + " ]";
    }
}
