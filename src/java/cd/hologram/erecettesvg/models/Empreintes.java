/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_EMPREINTES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empreintes.findAll", query = "SELECT e FROM Empreintes e"),
    @NamedQuery(name = "Empreintes.findByPersonne", query = "SELECT e FROM Empreintes e WHERE e.empreintesPK.personne = :personne"),
    @NamedQuery(name = "Empreintes.findByFingerId", query = "SELECT e FROM Empreintes e WHERE e.empreintesPK.fingerId = :fingerId")})
public class Empreintes implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EmpreintesPK empreintesPK;
    @Lob
    @Column(name = "IMAGE_EMPREINTE")
    private byte[] imageEmpreinte;

    public Empreintes() {
    }

    public Empreintes(EmpreintesPK empreintesPK) {
        this.empreintesPK = empreintesPK;
    }

    public Empreintes(String personne, int fingerId) {
        this.empreintesPK = new EmpreintesPK(personne, fingerId);
    }

    public EmpreintesPK getEmpreintesPK() {
        return empreintesPK;
    }

    public void setEmpreintesPK(EmpreintesPK empreintesPK) {
        this.empreintesPK = empreintesPK;
    }

    public byte[] getImageEmpreinte() {
        return imageEmpreinte;
    }

    public void setImageEmpreinte(byte[] imageEmpreinte) {
        this.imageEmpreinte = imageEmpreinte;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (empreintesPK != null ? empreintesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empreintes)) {
            return false;
        }
        Empreintes other = (Empreintes) object;
        if ((this.empreintesPK == null && other.empreintesPK != null) || (this.empreintesPK != null && !this.empreintesPK.equals(other.empreintesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Empreintes[ empreintesPK=" + empreintesPK + " ]";
    }
    
}
