/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_BORDEREAU")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bordereau.findAll", query = "SELECT b FROM Bordereau b"),
    @NamedQuery(name = "Bordereau.findByCode", query = "SELECT b FROM Bordereau b WHERE b.code = :code"),
    @NamedQuery(name = "Bordereau.findByNumeroBordereau", query = "SELECT b FROM Bordereau b WHERE b.numeroBordereau = :numeroBordereau"),
    @NamedQuery(name = "Bordereau.findByNomComplet", query = "SELECT b FROM Bordereau b WHERE b.nomComplet = :nomComplet"),
    @NamedQuery(name = "Bordereau.findByDatePaiement", query = "SELECT b FROM Bordereau b WHERE b.datePaiement = :datePaiement"),
    @NamedQuery(name = "Bordereau.findByTotalMontantPercu", query = "SELECT b FROM Bordereau b WHERE b.totalMontantPercu = :totalMontantPercu"),
    @NamedQuery(name = "Bordereau.findByDateCreate", query = "SELECT b FROM Bordereau b WHERE b.dateCreate = :dateCreate"),
    @NamedQuery(name = "Bordereau.findByDateMaj", query = "SELECT b FROM Bordereau b WHERE b.dateMaj = :dateMaj"),
    @NamedQuery(name = "Bordereau.findByAgentMaj", query = "SELECT b FROM Bordereau b WHERE b.agentMaj = :agentMaj"),
    @NamedQuery(name = "Bordereau.findByTotalMontantPercuToString", query = "SELECT b FROM Bordereau b WHERE b.totalMontantPercuToString = :totalMontantPercuToString"),
    @NamedQuery(name = "Bordereau.findByNumeroDeclaration", query = "SELECT b FROM Bordereau b WHERE b.numeroDeclaration = :numeroDeclaration"),
    @NamedQuery(name = "Bordereau.findByNumeroAttestationPaiement", query = "SELECT b FROM Bordereau b WHERE b.numeroAttestationPaiement = :numeroAttestationPaiement"),
    @NamedQuery(name = "Bordereau.findByImpressionRecu", query = "SELECT b FROM Bordereau b WHERE b.impressionRecu = :impressionRecu"),
    @NamedQuery(name = "Bordereau.findByCentre", query = "SELECT b FROM Bordereau b WHERE b.centre = :centre"),
    @NamedQuery(name = "Bordereau.findByObservationBanque", query = "SELECT b FROM Bordereau b WHERE b.observationBanque = :observationBanque")})
public class Bordereau implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Long code;
    @Size(max = 25)
    @Column(name = "NUMERO_BORDEREAU")
    private String numeroBordereau;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOM_COMPLET")
    private String nomComplet;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATE_PAIEMENT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePaiement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_MONTANT_PERCU")
    //private long totalMontantPercu;
    private BigDecimal totalMontantPercu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    @Column(name = "AGENT_MAJ")
    private int agentMaj;

    @Column(name = "OBSERVATION_BANQUE")
    private String observationBanque;

    @Size(max = 100)
    @Column(name = "TOTAL_MONTANT_PERCU_TO_STRING")
    private String totalMontantPercuToString;
    @Size(max = 50)
    @Column(name = "NUMERO_DECLARATION")
    private String numeroDeclaration;
    @Size(max = 50)
    @Column(name = "NUMERO_ATTESTATION_PAIEMENT")
    private String numeroAttestationPaiement;
    @Column(name = "IMPRESSION_RECU")
    private Boolean impressionRecu;
    @Size(max = 50)
    @Column(name = "CENTRE")
    private String centre;
    //@Size(max = 500)
    //@Column(name = "OBSERVATION")
    //private String observation;
    @JoinColumn(name = "AGENT", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private Agent agent;
    @JoinColumn(name = "COMPTE_BANCAIRE", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private CompteBancaire compteBancaire;
    @JoinColumn(name = "ETAT", referencedColumnName = "CODE")
    @ManyToOne
    private Etat etat;
    @OneToMany(mappedBy = "bordereau")
    private List<DetailBordereau> detailBordereauList;

    public Bordereau() {
    }

    public Bordereau(Long code) {
        this.code = code;
    }

    public Bordereau(Long code, String nomComplet, Date datePaiement, long totalMontantPercu, Date dateCreate) {
        this.code = code;
        this.nomComplet = nomComplet;
        this.datePaiement = datePaiement;
        //this.totalMontantPercu = totalMontantPercu;
        this.dateCreate = dateCreate;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getNumeroBordereau() {
        return numeroBordereau;
    }

    public void setNumeroBordereau(String numeroBordereau) {
        this.numeroBordereau = numeroBordereau;
    }

    public String getNomComplet() {
        return nomComplet;
    }

    public void setNomComplet(String nomComplet) {
        this.nomComplet = nomComplet;
    }

    public Date getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public BigDecimal getTotalMontantPercu() {
        return totalMontantPercu;
    }

    public void setTotalMontantPercu(BigDecimal totalMontantPercu) {
        this.totalMontantPercu = totalMontantPercu;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public String getTotalMontantPercuToString() {
        return totalMontantPercuToString;
    }

    public void setTotalMontantPercuToString(String totalMontantPercuToString) {
        this.totalMontantPercuToString = totalMontantPercuToString;
    }

    public String getNumeroDeclaration() {
        return numeroDeclaration;
    }

    public void setNumeroDeclaration(String numeroDeclaration) {
        this.numeroDeclaration = numeroDeclaration;
    }

    public String getNumeroAttestationPaiement() {
        return numeroAttestationPaiement;
    }

    public void setNumeroAttestationPaiement(String numeroAttestationPaiement) {
        this.numeroAttestationPaiement = numeroAttestationPaiement;
    }

    public Boolean getImpressionRecu() {
        return impressionRecu;
    }

    public void setImpressionRecu(Boolean impressionRecu) {
        this.impressionRecu = impressionRecu;
    }

    public String getCentre() {
        return centre;
    }

    public void setCentre(String centre) {
        this.centre = centre;
    }

    /*public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }*/

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public CompteBancaire getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(CompteBancaire compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public int getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(int agentMaj) {
        this.agentMaj = agentMaj;
    }

    public String getObservationBanque() {
        return observationBanque;
    }

    public void setObservationBanque(String observationBanque) {
        this.observationBanque = observationBanque;
    }
    
    
    

    @XmlTransient
    public List<DetailBordereau> getDetailBordereauList() {
        return detailBordereauList;
    }

    public void setDetailBordereauList(List<DetailBordereau> detailBordereauList) {
        this.detailBordereauList = detailBordereauList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bordereau)) {
            return false;
        }
        Bordereau other = (Bordereau) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Bordereau[ code=" + code + " ]";
    }

}
