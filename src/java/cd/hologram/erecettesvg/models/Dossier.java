/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_DOSSIER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dossier.findAll", query = "SELECT d FROM Dossier d"),
    @NamedQuery(name = "Dossier.findByNumero", query = "SELECT d FROM Dossier d WHERE d.numero = :numero"),
    @NamedQuery(name = "Dossier.findByAutreReference", query = "SELECT d FROM Dossier d WHERE d.autreReference = :autreReference"),
    @NamedQuery(name = "Dossier.findByIdentite", query = "SELECT d FROM Dossier d WHERE d.identite = :identite"),
    @NamedQuery(name = "Dossier.findByIntitule", query = "SELECT d FROM Dossier d WHERE d.intitule = :intitule"),
    @NamedQuery(name = "Dossier.findByObservation", query = "SELECT d FROM Dossier d WHERE d.observation = :observation"),
    @NamedQuery(name = "Dossier.findByAgentCreat", query = "SELECT d FROM Dossier d WHERE d.agentCreat = :agentCreat"),
    @NamedQuery(name = "Dossier.findByDateCreat", query = "SELECT d FROM Dossier d WHERE d.dateCreat = :dateCreat"),
    @NamedQuery(name = "Dossier.findByAgentMaj", query = "SELECT d FROM Dossier d WHERE d.agentMaj = :agentMaj"),
    @NamedQuery(name = "Dossier.findByDateMaj", query = "SELECT d FROM Dossier d WHERE d.dateMaj = :dateMaj"),
    @NamedQuery(name = "Dossier.findByEtat", query = "SELECT d FROM Dossier d WHERE d.etat = :etat"),
    @NamedQuery(name = "Dossier.findByAdresse", query = "SELECT d FROM Dossier d WHERE d.adresse = :adresse"),
    @NamedQuery(name = "Dossier.findByPartieAdverse", query = "SELECT d FROM Dossier d WHERE d.partieAdverse = :partieAdverse"),
    @NamedQuery(name = "Dossier.findByDateDeliberation", query = "SELECT d FROM Dossier d WHERE d.dateDeliberation = :dateDeliberation"),
    @NamedQuery(name = "Dossier.findByDateDecision", query = "SELECT d FROM Dossier d WHERE d.dateDecision = :dateDecision"),
    @NamedQuery(name = "Dossier.findByAmr", query = "SELECT d FROM Dossier d WHERE d.amr = :amr"),
    @NamedQuery(name = "Dossier.findByDateReceptionNotification", query = "SELECT d FROM Dossier d WHERE d.dateReceptionNotification = :dateReceptionNotification"),
    @NamedQuery(name = "Dossier.findByDateEcheanceRejet", query = "SELECT d FROM Dossier d WHERE d.dateEcheanceRejet = :dateEcheanceRejet"),
    @NamedQuery(name = "Dossier.findByDateImpressionNotification", query = "SELECT d FROM Dossier d WHERE d.dateImpressionNotification = :dateImpressionNotification"),
    @NamedQuery(name = "Dossier.findByEtatRejet", query = "SELECT d FROM Dossier d WHERE d.etatRejet = :etatRejet"),
    @NamedQuery(name = "Dossier.findByNatureAb", query = "SELECT d FROM Dossier d WHERE d.natureAb = :natureAb"),
    @NamedQuery(name = "Dossier.findByContentieuxJudiciaire", query = "SELECT d FROM Dossier d WHERE d.contentieuxJudiciaire = :contentieuxJudiciaire")})
public class Dossier implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "NUMERO")
    private String numero;
    @Size(max = 20)
    @Column(name = "AUTRE_REFERENCE")
    private String autreReference;
    @Size(max = 25)
    @Column(name = "IDENTITE")
    private String identite;
    @Size(max = 50)
    @Column(name = "INTITULE")
    private String intitule;
    @Size(max = 2000)
    @Column(name = "OBSERVATION")
    private String observation;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "ADRESSE")
    private String adresse;
    @Size(max = 25)
    @Column(name = "PARTIE_ADVERSE")
    private String partieAdverse;
    @Size(max = 10)
    @Column(name = "DATE_DELIBERATION")
    private String dateDeliberation;
    @Size(max = 10)
    @Column(name = "DATE_DECISION")
    private String dateDecision;
    @Size(max = 25)
    @Column(name = "AMR")
    private String amr;
    @Column(name = "DATE_RECEPTION_NOTIFICATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReceptionNotification;
    @Column(name = "DATE_ECHEANCE_REJET")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheanceRejet;
    @Column(name = "DATE_IMPRESSION_NOTIFICATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateImpressionNotification;
    @Column(name = "ETAT_REJET")
    private Boolean etatRejet;
    @Size(max = 25)
    @Column(name = "NATURE_AB")
    private String natureAb;
    @Column(name = "CONTENTIEUX_JUDICIAIRE")
    private Boolean contentieuxJudiciaire;
    @OneToMany(mappedBy = "dossier")
    private List<Process> processList;

    public Dossier() {
    }

    public Dossier(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getAutreReference() {
        return autreReference;
    }

    public void setAutreReference(String autreReference) {
        this.autreReference = autreReference;
    }

    public String getIdentite() {
        return identite;
    }

    public void setIdentite(String identite) {
        this.identite = identite;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getPartieAdverse() {
        return partieAdverse;
    }

    public void setPartieAdverse(String partieAdverse) {
        this.partieAdverse = partieAdverse;
    }

    public String getDateDeliberation() {
        return dateDeliberation;
    }

    public void setDateDeliberation(String dateDeliberation) {
        this.dateDeliberation = dateDeliberation;
    }

    public String getDateDecision() {
        return dateDecision;
    }

    public void setDateDecision(String dateDecision) {
        this.dateDecision = dateDecision;
    }

    public String getAmr() {
        return amr;
    }

    public void setAmr(String amr) {
        this.amr = amr;
    }

    public Date getDateReceptionNotification() {
        return dateReceptionNotification;
    }

    public void setDateReceptionNotification(Date dateReceptionNotification) {
        this.dateReceptionNotification = dateReceptionNotification;
    }

    public Date getDateEcheanceRejet() {
        return dateEcheanceRejet;
    }

    public void setDateEcheanceRejet(Date dateEcheanceRejet) {
        this.dateEcheanceRejet = dateEcheanceRejet;
    }

    public Date getDateImpressionNotification() {
        return dateImpressionNotification;
    }

    public void setDateImpressionNotification(Date dateImpressionNotification) {
        this.dateImpressionNotification = dateImpressionNotification;
    }

    public Boolean getEtatRejet() {
        return etatRejet;
    }

    public void setEtatRejet(Boolean etatRejet) {
        this.etatRejet = etatRejet;
    }

    public String getNatureAb() {
        return natureAb;
    }

    public void setNatureAb(String natureAb) {
        this.natureAb = natureAb;
    }

    public Boolean getContentieuxJudiciaire() {
        return contentieuxJudiciaire;
    }

    public void setContentieuxJudiciaire(Boolean contentieuxJudiciaire) {
        this.contentieuxJudiciaire = contentieuxJudiciaire;
    }

    @XmlTransient
    public List<Process> getProcessList() {
        return processList;
    }

    public void setProcessList(List<Process> processList) {
        this.processList = processList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numero != null ? numero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dossier)) {
            return false;
        }
        Dossier other = (Dossier) object;
        if ((this.numero == null && other.numero != null) || (this.numero != null && !this.numero.equals(other.numero))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Dossier[ numero=" + numero + " ]";
    }
    
}
