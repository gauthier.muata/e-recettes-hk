/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_SERIE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Serie.findAll", query = "SELECT s FROM Serie s"),
    @NamedQuery(name = "Serie.findById", query = "SELECT s FROM Serie s WHERE s.id = :id"),
    @NamedQuery(name = "Serie.findByAnnee", query = "SELECT s FROM Serie s WHERE s.annee = :annee"),
    @NamedQuery(name = "Serie.findByPrefix", query = "SELECT s FROM Serie s WHERE s.prefix = :prefix"),
    @NamedQuery(name = "Serie.findByDebut", query = "SELECT s FROM Serie s WHERE s.debut = :debut"),
    @NamedQuery(name = "Serie.findByFin", query = "SELECT s FROM Serie s WHERE s.fin = :fin"),
    @NamedQuery(name = "Serie.findByTypeDocument", query = "SELECT s FROM Serie s WHERE s.typeDocument = :typeDocument"),
    @NamedQuery(name = "Serie.findBySite", query = "SELECT s FROM Serie s WHERE s.site = :site"),
    @NamedQuery(name = "Serie.findByDateCreat", query = "SELECT s FROM Serie s WHERE s.dateCreat = :dateCreat"),
    @NamedQuery(name = "Serie.findByAgentCreat", query = "SELECT s FROM Serie s WHERE s.agentCreat = :agentCreat"),
    @NamedQuery(name = "Serie.findByDernierNumeroImprime", query = "SELECT s FROM Serie s WHERE s.dernierNumeroImprime = :dernierNumeroImprime"),
    @NamedQuery(name = "Serie.findByEtat", query = "SELECT s FROM Serie s WHERE s.etat = :etat"),
    @NamedQuery(name = "Serie.findByNombreNoteImprimee", query = "SELECT s FROM Serie s WHERE s.nombreNoteImprimee = :nombreNoteImprimee"),
    @NamedQuery(name = "Serie.findBySerieFinie", query = "SELECT s FROM Serie s WHERE s.serieFinie = :serieFinie")})
public class Serie implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ANNEE")
    private Integer annee;
    @Size(max = 2)
    @Column(name = "PREFIX")
    private String prefix;
    @Column(name = "DEBUT")
    private Integer debut;
    @Column(name = "FIN")
    private Integer fin;
    @Size(max = 5)
    @Column(name = "TYPE_DOCUMENT")
    private String typeDocument;
    @Size(max = 25)
    @Column(name = "SITE")
    private String site;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 25)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Size(max = 20)
    @Column(name = "DERNIER_NUMERO_IMPRIME")
    private String dernierNumeroImprime;
    @Column(name = "ETAT")
    private Boolean etat;
    @Column(name = "NOMBRE_NOTE_IMPRIMEE")
    private Integer nombreNoteImprimee;
    @Column(name = "SERIE_FINIE")
    private Boolean serieFinie;

    public Serie() {
    }

    public Serie(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAnnee() {
        return annee;
    }

    public void setAnnee(Integer annee) {
        this.annee = annee;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Integer getDebut() {
        return debut;
    }

    public void setDebut(Integer debut) {
        this.debut = debut;
    }

    public Integer getFin() {
        return fin;
    }

    public void setFin(Integer fin) {
        this.fin = fin;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getDernierNumeroImprime() {
        return dernierNumeroImprime;
    }

    public void setDernierNumeroImprime(String dernierNumeroImprime) {
        this.dernierNumeroImprime = dernierNumeroImprime;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Integer getNombreNoteImprimee() {
        return nombreNoteImprimee;
    }

    public void setNombreNoteImprimee(Integer nombreNoteImprimee) {
        this.nombreNoteImprimee = nombreNoteImprimee;
    }

    public Boolean getSerieFinie() {
        return serieFinie;
    }

    public void setSerieFinie(Boolean serieFinie) {
        this.serieFinie = serieFinie;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Serie)) {
            return false;
        }
        Serie other = (Serie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Serie[ id=" + id + " ]";
    }
    
}
