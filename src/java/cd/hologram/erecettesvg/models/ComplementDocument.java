/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_COMPLEMENT_DOCUMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComplementDocument.findAll", query = "SELECT c FROM ComplementDocument c"),
    @NamedQuery(name = "ComplementDocument.findById", query = "SELECT c FROM ComplementDocument c WHERE c.id = :id"),
    @NamedQuery(name = "ComplementDocument.findByFkTypeComplementDocument", query = "SELECT c FROM ComplementDocument c WHERE c.fkTypeComplementDocument = :fkTypeComplementDocument"),
    @NamedQuery(name = "ComplementDocument.findByDocument", query = "SELECT c FROM ComplementDocument c WHERE c.document = :document"),
    @NamedQuery(name = "ComplementDocument.findByValeur", query = "SELECT c FROM ComplementDocument c WHERE c.valeur = :valeur"),
    @NamedQuery(name = "ComplementDocument.findByEtat", query = "SELECT c FROM ComplementDocument c WHERE c.etat = :etat")})
public class ComplementDocument implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "FK_TYPE_COMPLEMENT_DOCUMENT")
    private String fkTypeComplementDocument;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "DOCUMENT")
    private String document;
    @Size(max = 75)
    @Column(name = "VALEUR")
    private String valeur;
    @Column(name = "ETAT")
    private Boolean etat;

    public ComplementDocument() {
    }

    public ComplementDocument(Integer id) {
        this.id = id;
    }

    public ComplementDocument(Integer id, String fkTypeComplementDocument, String document) {
        this.id = id;
        this.fkTypeComplementDocument = fkTypeComplementDocument;
        this.document = document;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkTypeComplementDocument() {
        return fkTypeComplementDocument;
    }

    public void setFkTypeComplementDocument(String fkTypeComplementDocument) {
        this.fkTypeComplementDocument = fkTypeComplementDocument;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComplementDocument)) {
            return false;
        }
        ComplementDocument other = (ComplementDocument) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.ComplementDocument[ id=" + id + " ]";
    }
    
}
