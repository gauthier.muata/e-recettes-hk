/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_ETAT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Etat.findAll", query = "SELECT e FROM Etat e"),
    @NamedQuery(name = "Etat.findByCode", query = "SELECT e FROM Etat e WHERE e.code = :code"),
    @NamedQuery(name = "Etat.findByLibelleEtat", query = "SELECT e FROM Etat e WHERE e.libelleEtat = :libelleEtat")})
public class Etat implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Integer code;
    @Size(max = 25)
    @Column(name = "LIBELLE_ETAT")
    private String libelleEtat;
    @OneToMany(mappedBy = "etat")
    private List<Bordereau> bordereauList;
    @OneToMany(mappedBy = "etat")
    private List<FichePriseCharge> fichePriseChargeList;

    public Etat() {
    }

    public Etat(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getLibelleEtat() {
        return libelleEtat;
    }

    public void setLibelleEtat(String libelleEtat) {
        this.libelleEtat = libelleEtat;
    }

    @XmlTransient
    public List<Bordereau> getBordereauList() {
        return bordereauList;
    }

    public void setBordereauList(List<Bordereau> bordereauList) {
        this.bordereauList = bordereauList;
    }

    @XmlTransient
    public List<FichePriseCharge> getFichePriseChargeList() {
        return fichePriseChargeList;
    }

    public void setFichePriseChargeList(List<FichePriseCharge> fichePriseChargeList) {
        this.fichePriseChargeList = fichePriseChargeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Etat)) {
            return false;
        }
        Etat other = (Etat) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Etat[ code=" + code + " ]";
    }
    
}
