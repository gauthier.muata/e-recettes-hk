/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_BUG_RESPONSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BugResponse.findAll", query = "SELECT b FROM BugResponse b"),
    @NamedQuery(name = "BugResponse.findById", query = "SELECT b FROM BugResponse b WHERE b.id = :id"),
    @NamedQuery(name = "BugResponse.findByFkBugTicket", query = "SELECT b FROM BugResponse b WHERE b.fkBugTicket = :fkBugTicket"),
    @NamedQuery(name = "BugResponse.findByAgentCreat", query = "SELECT b FROM BugResponse b WHERE b.agentCreat = :agentCreat"),
    @NamedQuery(name = "BugResponse.findByDateCreat", query = "SELECT b FROM BugResponse b WHERE b.dateCreat = :dateCreat"),
    @NamedQuery(name = "BugResponse.findByEtat", query = "SELECT b FROM BugResponse b WHERE b.etat = :etat")})
public class BugResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "FK_BUG_TICKET")
    private String fkBugTicket;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "CONTENU")
    private String contenu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Column(name = "ETAT")
    private Boolean etat;

    public BugResponse() {
    }

    public BugResponse(Integer id) {
        this.id = id;
    }

    public BugResponse(Integer id, String fkBugTicket, String contenu, String agentCreat) {
        this.id = id;
        this.fkBugTicket = fkBugTicket;
        this.contenu = contenu;
        this.agentCreat = agentCreat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkBugTicket() {
        return fkBugTicket;
    }

    public void setFkBugTicket(String fkBugTicket) {
        this.fkBugTicket = fkBugTicket;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BugResponse)) {
            return false;
        }
        BugResponse other = (BugResponse) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.BugResponse[ id=" + id + " ]";
    }
    
}
