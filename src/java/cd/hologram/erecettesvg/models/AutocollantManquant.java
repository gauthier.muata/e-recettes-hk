/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_AUTOCOLLANT_MANQUANT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AutocollantManquant.findAll", query = "SELECT a FROM AutocollantManquant a"),
    @NamedQuery(name = "AutocollantManquant.findById", query = "SELECT a FROM AutocollantManquant a WHERE a.id = :id"),
    @NamedQuery(name = "AutocollantManquant.findByIdSerie", query = "SELECT a FROM AutocollantManquant a WHERE a.idSerie = :idSerie"),
    @NamedQuery(name = "AutocollantManquant.findByNumeroAutocollant", query = "SELECT a FROM AutocollantManquant a WHERE a.numeroAutocollant = :numeroAutocollant"),
    @NamedQuery(name = "AutocollantManquant.findByDateRapport", query = "SELECT a FROM AutocollantManquant a WHERE a.dateRapport = :dateRapport"),
    @NamedQuery(name = "AutocollantManquant.findByAgent", query = "SELECT a FROM AutocollantManquant a WHERE a.agent = :agent"),
    @NamedQuery(name = "AutocollantManquant.findByBorneInferieure", query = "SELECT a FROM AutocollantManquant a WHERE a.borneInferieure = :borneInferieure"),
    @NamedQuery(name = "AutocollantManquant.findByBorneSuperieure", query = "SELECT a FROM AutocollantManquant a WHERE a.borneSuperieure = :borneSuperieure"),
    @NamedQuery(name = "AutocollantManquant.findByQte", query = "SELECT a FROM AutocollantManquant a WHERE a.qte = :qte"),
    @NamedQuery(name = "AutocollantManquant.findByRecuperable", query = "SELECT a FROM AutocollantManquant a WHERE a.recuperable = :recuperable"),
    @NamedQuery(name = "AutocollantManquant.findByRecupere", query = "SELECT a FROM AutocollantManquant a WHERE a.recupere = :recupere"),
    @NamedQuery(name = "AutocollantManquant.findByEtat", query = "SELECT a FROM AutocollantManquant a WHERE a.etat = :etat"),
    @NamedQuery(name = "AutocollantManquant.findByMotif", query = "SELECT a FROM AutocollantManquant a WHERE a.motif = :motif"),
    @NamedQuery(name = "AutocollantManquant.findByObservation", query = "SELECT a FROM AutocollantManquant a WHERE a.observation = :observation")})
public class AutocollantManquant implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_SERIE")
    private Integer idSerie;
    @Column(name = "NUMERO_AUTOCOLLANT")
    private Integer numeroAutocollant;
    @Column(name = "DATE_RAPPORT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateRapport;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "AGENT")
    private String agent;
    @Column(name = "BORNE_INFERIEURE")
    private Integer borneInferieure;
    @Column(name = "BORNE_SUPERIEURE")
    private Integer borneSuperieure;
    @Column(name = "QTE")
    private Integer qte;
    @Column(name = "RECUPERABLE")
    private Boolean recuperable;
    @Column(name = "RECUPERE")
    private Boolean recupere;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 50)
    @Column(name = "MOTIF")
    private String motif;
    @Size(max = 250)
    @Column(name = "OBSERVATION")
    private String observation;

    public AutocollantManquant() {
    }

    public AutocollantManquant(Integer id) {
        this.id = id;
    }

    public AutocollantManquant(Integer id, String agent) {
        this.id = id;
        this.agent = agent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdSerie() {
        return idSerie;
    }

    public void setIdSerie(Integer idSerie) {
        this.idSerie = idSerie;
    }

    public Integer getNumeroAutocollant() {
        return numeroAutocollant;
    }

    public void setNumeroAutocollant(Integer numeroAutocollant) {
        this.numeroAutocollant = numeroAutocollant;
    }

    public Date getDateRapport() {
        return dateRapport;
    }

    public void setDateRapport(Date dateRapport) {
        this.dateRapport = dateRapport;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public Integer getBorneInferieure() {
        return borneInferieure;
    }

    public void setBorneInferieure(Integer borneInferieure) {
        this.borneInferieure = borneInferieure;
    }

    public Integer getBorneSuperieure() {
        return borneSuperieure;
    }

    public void setBorneSuperieure(Integer borneSuperieure) {
        this.borneSuperieure = borneSuperieure;
    }

    public Integer getQte() {
        return qte;
    }

    public void setQte(Integer qte) {
        this.qte = qte;
    }

    public Boolean getRecuperable() {
        return recuperable;
    }

    public void setRecuperable(Boolean recuperable) {
        this.recuperable = recuperable;
    }

    public Boolean getRecupere() {
        return recupere;
    }

    public void setRecupere(Boolean recupere) {
        this.recupere = recupere;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutocollantManquant)) {
            return false;
        }
        AutocollantManquant other = (AutocollantManquant) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.AutocollantManquant[ id=" + id + " ]";
    }
    
}
