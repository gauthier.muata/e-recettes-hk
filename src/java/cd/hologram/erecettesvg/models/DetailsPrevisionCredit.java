/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_DETAILS_PREVISION_CREDIT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsPrevisionCredit.findAll", query = "SELECT l FROM DetailsPrevisionCredit l"),
    @NamedQuery(name = "DetailsPrevisionCredit.findById", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.id = :id"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByFkPrevisionCredit", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.fkPrevisionCredit = :fkPrevisionCredit"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByFkBien", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.fkBien = :fkBien"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByTaux", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.taux = :taux"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByType", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.type = :type"),
    
    @NamedQuery(name = "DetailsPrevisionCredit.findByDateCreate", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.dateCreate = :dateCreate"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByAgentCreate", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.agentCreate = :agentCreate"),
    
    @NamedQuery(name = "DetailsPrevisionCredit.findByNbreTourSouscrit", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.nbreTourSouscrit = :nbreTourSouscrit"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByTotalTaux", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.totalTaux = :totalTaux"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByFkDevise", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.fkDevise = :fkDevise"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByNbreTourUtilise", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.nbreTourUtilise = :nbreTourUtilise"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByEtat", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.etat = :etat"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByFkDetailAssujettissement", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.fkDetailAssujettissement = :fkDetailAssujettissement"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByDateMaj", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.dateMaj = :dateMaj"),
    @NamedQuery(name = "DetailsPrevisionCredit.findByAgentMaj", query = "SELECT l FROM DetailsPrevisionCredit l WHERE l.agentMaj = :agentMaj")})

public class DetailsPrevisionCredit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;

    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    @Column(name = "FK_PREVISION_CREDIT")
    private String fkPrevisionCredit;
    
    @Column(name = "FK_BIEN")
    private String fkBien;
    
    @Column(name = "TAUX")
    private BigDecimal taux;
    
    @Column(name = "TYPE")
    private Integer type;
    
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    
    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;
    
    @Column(name = "NBRE_TOUR_SOUSCRIT")
    private Integer nbreTourSouscrit;
    
    @Column(name = "TOTAL_TAUX")
    private BigDecimal totalTaux;
    
    @Column(name = "FK_DEVISE")
    private String fkDevise;
    
    @Column(name = "NBRE_TOUR_UTILISE")
    private Integer nbreTourUtilise;
    
    @Column(name = "ETAT")
    private Integer etat;
    
    @Column(name = "FK_DETAIL_ASSUJETTISSEMENT")
    private Integer fkDetailAssujettissement;
    
    @Column(name = "AGENT_MAJ")
    private Integer agentMaj;
    
    public DetailsPrevisionCredit() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public String getFkPrevisionCredit() {
        return fkPrevisionCredit;
    }

    public void setFkPrevisionCredit(String fkPrevisionCredit) {
        this.fkPrevisionCredit = fkPrevisionCredit;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }
    
    
    public String getFkBien() {
        return fkBien;
    }

    public void setFkBien(String fkBien) {
        this.fkBien = fkBien;
    }

    public Integer getFkDetailAssujettissement() {
        return fkDetailAssujettissement;
    }

    public void setFkDetailAssujettissement(Integer fkDetailAssujettissement) {
        this.fkDetailAssujettissement = fkDetailAssujettissement;
    }
    
    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getNbreTourSouscrit() {
        return nbreTourSouscrit;
    }

    public void setNbreTourSouscrit(Integer nbreTourSouscrit) {
        this.nbreTourSouscrit = nbreTourSouscrit;
    }

    public BigDecimal getTotalTaux() {
        return totalTaux;
    }

    public void setTotalTaux(BigDecimal totalTaux) {
        this.totalTaux = totalTaux;
    }

    public String getFkDevise() {
        return fkDevise;
    }

    public void setFkDevise(String fkDevise) {
        this.fkDevise = fkDevise;
    }

    public Integer getNbreTourUtilise() {
        return nbreTourUtilise;
    }

    public void setNbreTourUtilise(Integer nbreTourUtilise) {
        this.nbreTourUtilise = nbreTourUtilise;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Integer agentMaj) {
        this.agentMaj = agentMaj;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Log)) {
            return false;
        }
        DetailsPrevisionCredit other = (DetailsPrevisionCredit) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.DetailsPrevisionCredit[ code=" + id + " ]";
    }
    
    

}
