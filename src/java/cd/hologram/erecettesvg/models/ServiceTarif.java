/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_SERVICE_TARIF")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServiceTarif.findAll", query = "SELECT s FROM ServiceTarif s"),
    @NamedQuery(name = "ServiceTarif.findById", query = "SELECT s FROM ServiceTarif s WHERE s.id = :id"),
    @NamedQuery(name = "ServiceTarif.findByAgentCreat", query = "SELECT s FROM ServiceTarif s WHERE s.agentCreat = :agentCreat"),
    @NamedQuery(name = "ServiceTarif.findByDateCreat", query = "SELECT s FROM ServiceTarif s WHERE s.dateCreat = :dateCreat"),
    @NamedQuery(name = "ServiceTarif.findByAgentMaj", query = "SELECT s FROM ServiceTarif s WHERE s.agentMaj = :agentMaj"),
    @NamedQuery(name = "ServiceTarif.findByDateMaj", query = "SELECT s FROM ServiceTarif s WHERE s.dateMaj = :dateMaj"),
    @NamedQuery(name = "ServiceTarif.findByEtat", query = "SELECT s FROM ServiceTarif s WHERE s.etat = :etat")})
public class ServiceTarif implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "ID")
    private String id;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    @JoinColumn(name = "SERVICE", referencedColumnName = "CODE")
    @ManyToOne
    private Service service;
    @JoinColumn(name = "TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif tarif;

    public ServiceTarif() {
    }

    public ServiceTarif(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Tarif getTarif() {
        return tarif;
    }

    public void setTarif(Tarif tarif) {
        this.tarif = tarif;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServiceTarif)) {
            return false;
        }
        ServiceTarif other = (ServiceTarif) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.ServiceTarif[ id=" + id + " ]";
    }
    
}
