/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.util;

import cd.hologram.erecettesvg.business.ContentieuxBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.NotePerceptionBusiness;
import cd.hologram.erecettesvg.business.RecouvrementBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import static cd.hologram.erecettesvg.business.TaxationBusiness.getSiteByCode;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.constants.PropertiesConst;
import cd.hologram.erecettesvg.models.AdressePersonne;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.DetailVoucher;
import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.JourFerie;
import cd.hologram.erecettesvg.models.LoginWeb;
import cd.hologram.erecettesvg.models.NoteCalcul;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Reclamation;
import cd.hologram.erecettesvg.models.Role;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.models.Voucher;
import cd.hologram.erecettesvg.pojo.AccountBankAmr;
import cd.hologram.erecettesvg.pojo.AssujettiListPrint;
import cd.hologram.erecettesvg.pojo.BienTaxationPrint;
import cd.hologram.erecettesvg.pojo.DetailRolePrint;
import cd.hologram.erecettesvg.pojo.TMBPayBillRequest;
import cd.hologram.erecettesvg.properties.PropertiesConfig;
import com.google.gson.JsonObject;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author moussa.toure
 */
public class Tools {

    static final String DATA_IMAGE_JPEG_BASE64_TEXT = "data:image/jpeg;base64,";
    static final String BASE64_TEXT = "base64";

    public static String base64Encode(String token) {

        byte[] utf8 = token.getBytes();
        String result = Base64.encodeBase64String(utf8);
        return result;
    }

    public static String base64Decode(String result) {
        byte[] resultAfterDecode = Base64.decodeBase64(result);
        String resultStr = new String(resultAfterDecode);
        return resultStr;
    }

    public static String base64UrlEncode(String token) {

        byte[] utf8 = token.getBytes();
        String result = Base64.encodeBase64URLSafeString(utf8);
        return result;
    }

    public static String base64UrlDecode(String result) {
        String resultStr = GeneralConst.EMPTY_STRING;

        if (result != null && !result.isEmpty()) {
            byte[] resultAfterDecode = org.apache.commons.codec.binary.Base64.decodeBase64(result);
            resultStr = new String(resultAfterDecode);
        }

        return resultStr;
    }

    public static String formatDateToString(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatDateToStringV2(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatDateWithTimeToString(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy à HH:mm:ss");
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static Date formatStringFullToDate(String date) {

        DateFormat formatter;
        Date formattedDate = null;
        formatter = new SimpleDateFormat("yyy-MM-dd", Locale.US);
        try {
            formattedDate = formatter.parse(date);
        } catch (ParseException e) {
        }
        return formattedDate;
    }

    public static String lastConnexion(long diff) {

        long days = TimeUnit.MILLISECONDS.toDays(diff);
        long remainingHoursInMillis = diff - TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(remainingHoursInMillis);
        long remainingMinutesInMillis = remainingHoursInMillis - TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(remainingMinutesInMillis);
        long remainingSecondsInMillis = remainingMinutesInMillis - TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(remainingSecondsInMillis);

        String lastConnexion = GeneralConst.EMPTY_STRING;

        if (days != 0) {
            lastConnexion += days + " jr(s) " + hours + " hr(s) " + minutes + " min " + seconds + " sec";
        } else {
            if (hours != 0) {
                lastConnexion += hours + " hr(s)  " + minutes + " min " + seconds + " sec";
            } else {
                lastConnexion += minutes + " min " + seconds + " sec";
            }
        }

        return lastConnexion;
    }

    public static String getColor(int position) {

        String[] colors = new String[]{
            "#4dc9f6",
            "#f67019",
            "#f53794",
            "#537bc4",
            "#acc236",
            "#166a8f",
            "#00a950",
            "#58595b",
            "#8549ba",
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850"
        };

        return colors[position];
    }

    public static String envoieMail(
            String[] params,
            String receiver,
            String subject,
            String message,
            Date dateMail) {
        try {

            MailSender mailSender = new MailSender();
            mailSender.setSender(params[0]);
            mailSender.setHost(params[1]);
            mailSender.setPassword(params[2]);
            mailSender.setReciever(receiver.trim());
            mailSender.setSubject(subject);
            mailSender.setMessage(message);
            mailSender.setDate(dateMail);
            mailSender.sendMail();

            return GeneralConst.Number.ONE;

        } catch (Exception e) {

            return GeneralConst.Number.ZERO;
        }
    }

    public static long convertDateToTimestamp(Date date) {
        long output = date.getTime() / 1000L;
        String str = Long.toString(output);
        long timestamp = Long.parseLong(str) * 1000;
        return timestamp;
    }

    public static String getValidFormat(String date) {
        String result;
        String[] parts = date.split("-");
        String part1 = parts[0];
        String part2 = parts[1];
        String part3 = parts[2];
        result = part3 + part2 + part1;
        return result;
    }

    static public String formatNombreToString(String pattern, BigDecimal value) {

        NumberFormat nf = NumberFormat.getNumberInstance(new Locale("fr", "FR"));
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern(pattern);
        String output = df.format(value);
        return output;

    }

    public static String getDataTableToHtml(List<?> currentObject) throws InvocationTargetException {
        StringBuilder lbl = null;
        lbl = new StringBuilder();
        lbl.append("<table style=\"border-collapse: collapse;width:100%;font-size:12px;\">");
        for (int j = 0; j < currentObject.size(); j++) {
            lbl.append("<tr>");
            List<Field> currentField = getPublicFields(currentObject.get(j).getClass());
            for (int i = 0; i < currentField.size(); i++) {
                String fieldStr = currentField.get(i).getName();
                String formatedFieldName = String.valueOf(fieldStr.charAt(0)).toUpperCase() + fieldStr.substring(1);
                if (j == 0) {
                    String replaceFormatedFieldName = formatedFieldName.replace("_", " ").toUpperCase();
                    lbl.append("<th style=\"border:1px solid black;text-align:left\">").append(replaceFormatedFieldName).append("</th>");
                }
            }
            lbl.append("</tr>");
        }
        for (int j = 0; j < currentObject.size(); j++) {
            List<Field> currentField = getPublicFields(currentObject.get(j).getClass());
            lbl.append("<tr>");
            for (int i = 0; i < currentField.size(); i++) {
                String fieldStr = currentField.get(i).getName();
                String formatedFieldName = String.valueOf(fieldStr.charAt(0)).toUpperCase() + fieldStr.substring(1);
                lbl.append("<td style=\"border:0px solid black;padding: 3px;text-align:left\">").append(getValue(currentObject.get(j), formatedFieldName)).append("</td>");
            }
            lbl.append("</tr>");
        }
        lbl.append("</table>");
        return lbl.toString();
    }

    public static List<Field> getPublicFields(Class<?> theClass) {
        List<Field> publicFields = new ArrayList<>();
        Field[] fields = theClass.getDeclaredFields();

        for (Field field : fields) {
            if (Modifier.isPublic(field.getModifiers())) {
                publicFields.add(field);
            }
        }
        return publicFields;
    }

    private static String getValue(Object currentObject, String fieldName) throws InvocationTargetException {
        String valueStr = GeneralConst.EMPTY_STRING;
        try {

            Method m = currentObject.getClass().getMethod(DocumentConst.DocumentParamName.GET.concat(fieldName.trim()));
            valueStr = String.valueOf(m.invoke(currentObject));
        } catch (NoSuchMethodException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
            valueStr = GeneralConst.AUCUNE_DONNEE;
        }
        return valueStr;
    }

    public static String getByteaToBase64String(byte[] bytea) {
        String photoStr = GeneralConst.EMPTY_STRING;
        try {
            if (bytea != null) {
                StringBuilder sb = new StringBuilder();
                sb.append(GeneralConst.DATA_IMAGE_JPEG_BASE64_TEXT);
                sb.append(StringUtils.newStringUtf8(Base64.encodeBase64(bytea, false)));
                photoStr = sb.toString();
            }
        } catch (Exception e) {
            return GeneralConst.EMPTY_STRING;
        }
        return photoStr;
    }

    public static String getMoisByCode(String code) {

        String mois = GeneralConst.EMPTY_STRING;
        try {

            switch (code.trim()) {
                case GeneralConst.CalendarMonth.JANVIER_CODE_1:
                case GeneralConst.CalendarMonth.JANVIER_CODE_2:
                    mois = GeneralConst.CalendarMonth.JANVIER_NAME;
                    break;
                case GeneralConst.CalendarMonth.FEVRIER_CODE_1:
                case GeneralConst.CalendarMonth.FEVRIER_CODE_2:
                    mois = GeneralConst.CalendarMonth.FEVRIER_NAME;
                    break;
                case GeneralConst.CalendarMonth.MARS_CODE_1:
                case GeneralConst.CalendarMonth.MARS_CODE_2:
                    mois = GeneralConst.CalendarMonth.MARS_NAME;
                    break;
                case GeneralConst.CalendarMonth.AVRIL_CODE_1:
                case GeneralConst.CalendarMonth.AVRIL_CODE_2:
                    mois = GeneralConst.CalendarMonth.AVRIL_NAME;
                    break;
                case GeneralConst.CalendarMonth.MAI_CODE_1:
                case GeneralConst.CalendarMonth.MAI_CODE_2:
                    mois = GeneralConst.CalendarMonth.MAI_NAME;
                    break;
                case GeneralConst.CalendarMonth.JUIN_CODE_1:
                case GeneralConst.CalendarMonth.JUIN_CODE_2:
                    mois = GeneralConst.CalendarMonth.JUIN_NAME;
                    break;
                case GeneralConst.CalendarMonth.JUILLET_CODE_1:
                case GeneralConst.CalendarMonth.JUILLET_CODE_2:
                    mois = GeneralConst.CalendarMonth.JUILLET_NAME;
                    break;
                case GeneralConst.CalendarMonth.AOUT_CODE_1:
                case GeneralConst.CalendarMonth.AOUT_CODE_2:
                    mois = GeneralConst.CalendarMonth.AOUT_NAME;
                    break;
                case GeneralConst.CalendarMonth.SEPTEMBRE_CODE_1:
                case GeneralConst.CalendarMonth.SEPTEMBRE_CODE_2:
                    mois = GeneralConst.CalendarMonth.SEPTEMBRE_NAME;
                    break;
                case GeneralConst.CalendarMonth.OCTOBRE_CODE:
                    mois = GeneralConst.CalendarMonth.OCTOBRE_NAME;
                    break;
                case GeneralConst.CalendarMonth.NOVEMEBRE_CODE:
                    mois = GeneralConst.CalendarMonth.NOVEMEBRE_NAME;
                    break;
                case GeneralConst.CalendarMonth.DECMEBRE_CODE:
                    mois = GeneralConst.CalendarMonth.DECMEBRE_NAME;
                    break;
            }

        } catch (Exception e) {
            //HandleException.getException(e, false);
        }
        return mois;
    }

    public static String getTableHtmlV(List<?> currentObject) {
        StringBuilder lbl = new StringBuilder();
        try {
            lbl = new StringBuilder();
            lbl.append("<table style=\"border-collapse: collapse;width:100%;font-size:12px\">");
            for (int j = 0; j < currentObject.size(); j++) {
                lbl.append("<tr>");
                List<Field> currentField = getPublicFields(currentObject.get(j).getClass());
                for (int i = 0; i < currentField.size(); i++) {
                    String fieldStr = currentField.get(i).getName();
                    String formatedFieldName = String.valueOf(fieldStr.charAt(0)).toUpperCase() + fieldStr.substring(1);
                    if (j == 0) {
                        String replaceFormatedFieldName = formatedFieldName.replace("_", " ");
                        switch (replaceFormatedFieldName) {
                            case "Reference":
                                replaceFormatedFieldName = "Référence";
                                break;
                            case "CompteBancaire":
                                replaceFormatedFieldName = "N°Compte";
                                break;
                            //case "NumeroPiece":
                            //replaceFormatedFieldName = "N°de pièce";
                            //break;
                            case "DatePaiement":
                                replaceFormatedFieldName = "Date";
                                break;
                            case "MontantPayer":
                                replaceFormatedFieldName = "Montant Payé";
                                break;
                        }
                        lbl.append("<th style=\"border:1px solid black;text-align:center\">").append(replaceFormatedFieldName).append("</th>");
                    }
                }
                lbl.append("</tr>");
            }
            for (int j = 0; j < currentObject.size(); j++) {
                List<Field> currentField = getPublicFields(currentObject.get(j).getClass());
                lbl.append("<tr>");
                for (int i = 0; i < currentField.size(); i++) {
                    String fieldStr = currentField.get(i).getName();
                    String formatedFieldName = String.valueOf(fieldStr.charAt(0)).toUpperCase() + fieldStr.substring(1);
                    lbl.append("<td style=\"border:1px solid black;padding: 3px;text-align:left\">").append(getValue(currentObject.get(j), formatedFieldName)).append("</td>");
                }
                lbl.append("</tr>");
            }
            lbl.append("</table>");
        } catch (Exception e) {
        }
        return lbl.toString();
    }

    public static String generateQRCode(String contenu) {
        String qrBase64 = GeneralConst.EMPTY_STRING;
        try {
            ByteArrayOutputStream out = QRCode.from(contenu).to(ImageType.PNG).stream();
            ByteArrayInputStream qrIn = new ByteArrayInputStream(out.toByteArray());
            qrBase64 = getByteaToBase64String(out.toByteArray());
        } catch (Exception e) {
        }
        return qrBase64;
    }

    public static Date addMonthTodate(Date date, Integer month) {

        Date newDate = null;

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, month);

            newDate = calendar.getTime();

        } catch (Exception e) {
            throw e;
        }

        return newDate;
    }

    public static String getExerciceFiscal(NoteCalcul noteCalcul) {

        String exerciceFiscal = GeneralConst.EMPTY_STRING;

        try {
            if (!noteCalcul.getDetailsNcList().isEmpty()) {

                if (noteCalcul.getDetailsNcList().get(
                        GeneralConst.Numeric.ZERO).getPeriodeDeclaration() != null) {

                    PeriodeDeclaration periodeDeclaration = noteCalcul.getDetailsNcList().get(
                            GeneralConst.Numeric.ZERO).getPeriodeDeclaration();

                    String periodeAB = periodeDeclaration.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode();

                    exerciceFiscal = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(), periodeAB);

                } else {

                    exerciceFiscal = noteCalcul.getExercice().trim();
                }

            }
        } catch (Exception e) {
            throw e;
        }

        return exerciceFiscal;
    }

    public static String getPeriodeIntitule(Date date, String periodeCode) {

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            int day, year, month;

            String periode = GeneralConst.EMPTY_STRING;

            day = calendar.get(Calendar.DATE);
            month = calendar.get(Calendar.MONTH);
            year = calendar.get(Calendar.YEAR);
            month++;
            String monthName = Tools.getMoisByCode(month + GeneralConst.EMPTY_STRING);

            switch (periodeCode) {

                case GeneralConst.Periodicite.JOUR:
                case GeneralConst.Periodicite.PONC:
                    periode = String.format("%s/%s/%s", day, month, year);
                    break;
                case GeneralConst.Periodicite.MONTH:
                    periode = String.format("%s %s", monthName, year);
                    break;
                case GeneralConst.Periodicite.TRIME:
                    periode = String.format("%s %s", monthName, year);
                    break;
                case GeneralConst.Periodicite.BIMENS:
                    month += GeneralConst.Numeric.ONE;
                    periode = String.format("%s %s <br/> %s %s",
                            monthName, year,
                            Tools.getMoisByCode(month + GeneralConst.EMPTY_STRING), year);
                    break;
                case GeneralConst.Periodicite.SEMS:
                    periode = String.format("%s %s", monthName, year);
                    break;
                case GeneralConst.Periodicite.YEAR:
                    periode = String.format("%s", year);
                    break;
                default:

            }

            return periode.toUpperCase();
        } catch (Exception e) {
            throw e;
        }
    }

    public static String getArticleByNC(NoteCalcul noteCalcul) {

        String article = GeneralConst.EMPTY_STRING;

        try {
            if (!noteCalcul.getDetailsNcList().isEmpty()) {

                Integer size = noteCalcul.getDetailsNcList().size();

                for (int i = GeneralConst.Numeric.ZERO; i < size; i++) {

                    String tempAb;
                    String libelleTarif = GeneralConst.EMPTY_STRING;

                    String codeTarif = noteCalcul.getDetailsNcList().get(i).getTarif();
                    Tarif tarif = TaxationBusiness.getTarifByCode(codeTarif);

                    if (tarif != null) {
                        if (!GeneralConst.TOUT.equalsIgnoreCase(tarif.getIntitule())) {
                            libelleTarif = GeneralConst.TWO_POINTS.concat(tarif.getIntitule());
                        }
                    }

                    ArticleBudgetaire ab = noteCalcul.getDetailsNcList().get(i).getArticleBudgetaire();

                    tempAb = ab.getIntitule().concat(libelleTarif);

                    if (i == GeneralConst.Numeric.ZERO) {
                        article = tempAb;
                    } else {
                        article += GeneralConst.AB_SEPARTOR.concat(tempAb);
                    }
                }

            }
        } catch (Exception e) {
            throw e;
        }

        return article;
    }

    public static String getArticleByNC_V2(NoteCalcul noteCalcul) {

        String articleComposite = GeneralConst.EMPTY_STRING;
        int i = 0;
        int nbreLine = 0;

        try {
            if (!noteCalcul.getDetailsNcList().isEmpty()) {
                nbreLine = noteCalcul.getDetailsNcList().size();

                ArticleBudgetaire ab;
                Tarif tarif;

                String articleBudg = GeneralConst.EMPTY_STRING;
                String libelleTarif = GeneralConst.EMPTY_STRING;

                switch (noteCalcul.getDetailsNcList().size()) {

                    case GeneralConst.Numeric.ONE:

                        ab = new ArticleBudgetaire();
                        ab = noteCalcul.getDetailsNcList().get(0).getArticleBudgetaire();

                        tarif = new Tarif();
                        tarif = NotePerceptionBusiness.getTarifByCode(noteCalcul.getDetailsNcList().get(0).getTarif());

                        if (tarif != null) {

                            if (tarif.getCode().equals(GeneralConst.ALL)) {
                                libelleTarif = GeneralConst.EMPTY_STRING;
                            } else {
                                libelleTarif = GeneralConst.TWO_POINTS.concat(tarif.getIntitule());
                            }

                        } else {
                            libelleTarif = GeneralConst.EMPTY_STRING;
                        }

                        articleBudg = ab.getIntitule().concat(libelleTarif);

                        articleComposite = articleBudg;

                        break;
                    default:

                        for (DetailsNc dnc : noteCalcul.getDetailsNcList()) {

                            i++;

                            ab = new ArticleBudgetaire();
                            ab = dnc.getArticleBudgetaire();

                            tarif = new Tarif();
                            tarif = NotePerceptionBusiness.getTarifByCode(dnc.getTarif().trim());

                            if (tarif != null) {

                                if (tarif.getCode().equals(GeneralConst.ALL)) {
                                    libelleTarif = GeneralConst.EMPTY_STRING;
                                } else {
                                    libelleTarif = GeneralConst.TWO_POINTS.concat(tarif.getIntitule().toUpperCase());
                                }

                            } else {
                                libelleTarif = GeneralConst.EMPTY_STRING;
                            }

                            articleBudg = ab.getIntitule().toUpperCase().concat(libelleTarif).concat("<br/>");

                            if (articleComposite.isEmpty()) {
                                articleComposite = i + ") " + articleBudg.concat("<br/>");
                            } else {
                                if (i == nbreLine) {
                                    articleComposite += i + ") " + articleBudg;
                                } else {
                                    articleComposite += i + ") " + articleBudg.concat("<br/>");
                                }

                            }
                        }
                        break;
                }

            }
        } catch (Exception e) {
            throw e;
        }

        return articleComposite;
    }

    public static String getCodeBudgetaireByNC(NoteCalcul noteCalcul) {

        String codeBudgetaire = GeneralConst.EMPTY_STRING;

        try {
            if (!noteCalcul.getDetailsNcList().isEmpty()) {

                Integer size = noteCalcul.getDetailsNcList().size();

                for (int i = GeneralConst.Numeric.ZERO; i < size; i++) {

                    String codeB = GeneralConst.EMPTY_STRING;

                    ArticleBudgetaire ab = new ArticleBudgetaire();

                    ab = noteCalcul.getDetailsNcList().get(i).getArticleBudgetaire();

                    if (ab.getCodeOfficiel() != null && !ab.getCodeOfficiel().isEmpty()) {
                        codeB = ab.getCodeOfficiel().toUpperCase();
                    }

                    if (i == GeneralConst.Numeric.ZERO) {
                        codeBudgetaire = codeB;
                    } else {
                        if (!codeB.isEmpty()) {
                            codeBudgetaire += GeneralConst.AB_SEPARTOR.concat(codeB);
                        }

                    }
                }

            }
        } catch (Exception e) {
            throw e;
        }

        return codeBudgetaire;
    }

    public static Date formatStringFullToDateV2(String date) {

        DateFormat formatter;
        Date formattedDate = null;
        formatter = new SimpleDateFormat(GeneralConst.Format.FORMAT_DATE, Locale.FRENCH);
        try {
            formattedDate = formatter.parse(date);
        } catch (ParseException e) {
        }
        return formattedDate;
    }

    public static Date getEcheanceDeclarationByYear(int year) {

        Date dateEchance = null;

        switch (year) {
            case 2017:
                dateEchance = ConvertDate.formatDate("28/02/2021");
                break;
            case 2018:
                dateEchance = ConvertDate.formatDate("28/02/2021");
                break;
            case 2019:
                dateEchance = ConvertDate.formatDate("28/02/2021");
                break;
            case 2020:
                dateEchance = ConvertDate.formatDate("28/02/2021");
                break;
            case 2021:
                dateEchance = ConvertDate.formatDate("28/02/2021");
                break;
        }

        return dateEchance;
    }

    public static Date getEcheanceDate(Date date, int nbreJour) {

        Date dateEchance = null;

        try {

            dateEchance = addDayTodate(date, nbreJour, false);

        } catch (Exception e) {
            throw e;
        }

        return dateEchance;
    }

    public static Date addDayTodate(Date date, Integer day, boolean islegale) {

        Date newDate = null;

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            if (islegale) {
                calendar.add(Calendar.DATE, -day);
            } else {
                calendar.add(Calendar.DATE, day);
            }
            newDate = calendar.getTime();

        } catch (Exception e) {
            throw e;
        }

        return newDate;
    }

    public static int getDayOfMonthByYear(int annee, int mois) {

        YearMonth yearMonthObject = YearMonth.of(annee, mois);
        int daysInMonth = yearMonthObject.lengthOfMonth();
        return daysInMonth;
    }

    public static Date getEcheanceDate(Date dateEcheance, int NbrJour, boolean isLegal) {

        Date dateUser = dateEcheance;

        boolean canGo = true, isDayOff, isSunday;

        while (canGo) {

            isDayOff = checkDayOff(dateUser);
            isSunday = checkSunday(dateUser);

            canGo = isDayOff || isSunday;

            if (canGo) {

                dateUser = addDayTodate(dateUser, NbrJour, isLegal);

            } else {

                canGo = false;

            }

        }

        return dateUser;
    }

    static boolean checkSunday(Date date) {

        boolean isSundy = false;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            isSundy = true;
        }

        return isSundy;
    }

    static boolean checkDayOff(Date date) {

        boolean isDayOff = false;

        List<JourFerie> dates = GeneralBusiness.getDaysOff();

        for (JourFerie jourFerie : dates) {

            if (jourFerie.getType().equals(GeneralConst.Number.ONE)) {

                String userDate = getDatepartv2(date);
                String dayOff = getDatepartv2(jourFerie.getDateJourFerie());

                isDayOff = userDate.equals(dayOff);
                if (isDayOff) {
                    break;
                }

            } else {
                isDayOff = jourFerie.getDateJourFerie().compareTo(date) == GeneralConst.Numeric.ZERO;
                if (isDayOff) {
                    break;
                }
            }
        }
        return isDayOff;
    }

    public static String getDatepartv2(Date date) {

        try {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            int day, month;

            day = calendar.get(Calendar.DATE);
            month = calendar.get(Calendar.MONTH);
            month++;

            return day + GeneralConst.EMPTY_STRING + month;

        } catch (Exception e) {
            throw e;
        }

    }

    public static String getDeviseByNC(NoteCalcul noteCalcul) {

        String devise = GeneralConst.EMPTY_STRING;

        try {
            List<DetailsNc> detailsNcList = noteCalcul.getDetailsNcList();
            if (detailsNcList != null && !detailsNcList.isEmpty()) {
                devise = detailsNcList.get(GeneralConst.Numeric.ZERO).getDevise();
                if (devise == null) {
                    devise = GeneralConst.EMPTY_STRING;
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return devise;
    }

    public static String getStringCode(List<String> listCode) {

        String codeList = GeneralConst.EMPTY_STRING;

        try {
            if (listCode.size() > 0) {

                int i = 0;
                int max = listCode.size();

                for (String code : listCode) {

                    if (listCode.size() == 1) {
                        codeList = "'" + code + "'";
                    } else if (i == (max - 1)) {
                        codeList += "'" + code + "'";
                    } else {
                        codeList += "'" + code + "'" + ",";
                    }
                    i++;
                }
            }

        } catch (Exception e) {
            throw e;
        }
        return codeList;
    }

    public static String generateRandomValue(int size) {
        String generatedString = RandomStringUtils.randomAlphanumeric(size);
        return generatedString;
    }

    public static String getFormatTabAccountBankAmr(List<AccountBankAmr> accountBankAmrs) throws InvocationTargetException {

        StringBuilder lbl = null;
        lbl = new StringBuilder();

        lbl.append("<table style=\"border-collapse: collapse;width:100%;font-size:14px;\">");
        lbl.append("<tbody>");
        lbl.append("<tr>");

        lbl.append("<th style=\"border:1px solid black;text-align:left;background-color:#CCC\">");
        lbl.append("BANQUE");
        lbl.append("</th>");

        lbl.append("<th style=\"border:1px solid black;text-align:left;background-color:#CCC\">");
        lbl.append("COMPTE BANCAIRE");
        lbl.append("</th>");

        lbl.append("<th style=\"border:1px solid black;text-align:left;background-color:#CCC\">");
        lbl.append("DEVISE");
        lbl.append("</th>");

        lbl.append("</tr>");

        for (AccountBankAmr aba : accountBankAmrs) {

            lbl.append("<tr>");
            lbl.append("<td style=\"border:1px solid black;vertical-align:middle;text-align:left\">");
            lbl.append(aba.banqueName != null ? aba.banqueName : GeneralConst.EMPTY_STRING);
            lbl.append("</td>");

            lbl.append("<td style=\"border:1px solid black;vertical-align:middle;text-align:left\">");
            lbl.append(aba.compteCode != null ? aba.compteCode : GeneralConst.EMPTY_STRING);
            lbl.append("</td>");

            lbl.append("<td style=\"border:1px solid black;vertical-align:middle;text-align:left\">");
            lbl.append(aba.deviseName != null ? aba.deviseName : GeneralConst.EMPTY_STRING);
            lbl.append("</td>");

            lbl.append("</tr>");
        }

        lbl.append("</tbody>");
        lbl.append("</table>");
        return lbl.toString();
    }

    public static String getDateString(Date date, boolean isActe) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int day, year, month;

        day = calendar.get(Calendar.DATE);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);
        month++;

        String mois = getMoisByCode(String.valueOf(month)).toLowerCase();
        String dateFormat;

        if (mois.startsWith("a") || mois.startsWith("o")) {
            dateFormat = isActe ? GeneralConst.Format.FORMAT_DATE_TEXT_ACTE_V2
                    : GeneralConst.Format.FORMAT_DATE_TEXT_ATTESTATION_V2;
        } else {
            dateFormat = isActe ? GeneralConst.Format.FORMAT_DATE_TEXT_ACTE
                    : GeneralConst.Format.FORMAT_DATE_TEXT_ATTESTATION;
        }

        String textDate;

        if (isActe) {

            textDate = String.format(dateFormat, FrenchNumberToWords.convert(BigDecimal.valueOf(year)),
                    getNumeralByNumber(day).toLowerCase(), getMoisByCode(String.valueOf(month)).toLowerCase());

        } else {

            textDate = String.format(dateFormat, getNumeralByNumber(day).toLowerCase(),
                    getMoisByCode(String.valueOf(month)).toLowerCase(), FrenchNumberToWords.convert(BigDecimal.valueOf(year)));

        }

        return textDate;
    }

    public static String getNumeralByNumber(int number) {

        String numeral = GeneralConst.EMPTY_STRING;

        switch (number) {

            case 1:
                numeral = GeneralConst.NumeralString.UN;
                break;
            case 2:
                numeral = GeneralConst.NumeralString.DEUX;
                break;
            case 3:
                numeral = GeneralConst.NumeralString.TROIS;
                break;
            case 4:
                numeral = GeneralConst.NumeralString.QUATRE;
                break;
            case 5:
                numeral = GeneralConst.NumeralString.CINQ;
                break;
            case 6:
                numeral = GeneralConst.NumeralString.SIX;
                break;
            case 7:
                numeral = GeneralConst.NumeralString.SEPT;
                break;
            case 8:
                numeral = GeneralConst.NumeralString.HUIT;
                break;
            case 9:
                numeral = GeneralConst.NumeralString.NEUF;
                break;
            case 10:
                numeral = GeneralConst.NumeralString.DIX;
                break;
            case 11:
                numeral = GeneralConst.NumeralString.ONZE;
                break;
            case 12:
                numeral = GeneralConst.NumeralString.DOUZE;
                break;
            case 13:
                numeral = GeneralConst.NumeralString.TREIZE;
                break;
            case 14:
                numeral = GeneralConst.NumeralString.QUATORZE;
                break;
            case 15:
                numeral = GeneralConst.NumeralString.QUIZE;
                break;
            case 16:
                numeral = GeneralConst.NumeralString.SIEZE;
                break;
            case 17:
                numeral = GeneralConst.NumeralString.DIX_SEPT;
                break;
            case 18:
                numeral = GeneralConst.NumeralString.DIX_HUIT;
                break;
            case 19:
                numeral = GeneralConst.NumeralString.DIX_NEUF;
                break;
            case 20:
                numeral = GeneralConst.NumeralString.VINGT;
                break;
            case 21:
                numeral = GeneralConst.NumeralString.VINGT_UN;
                break;
            case 22:
                numeral = GeneralConst.NumeralString.VINGT_DEUXIEME;
                break;
            case 23:
                numeral = GeneralConst.NumeralString.VINGT_TROIS;
                break;
            case 24:
                numeral = GeneralConst.NumeralString.VINGT_QUATRE;
                break;
            case 25:
                numeral = GeneralConst.NumeralString.VINGT_CINQ;
                break;
            case 26:
                numeral = GeneralConst.NumeralString.VINGT_SIX;
                break;
            case 27:
                numeral = GeneralConst.NumeralString.VINGT_SEPT;
                break;
            case 28:
                numeral = GeneralConst.NumeralString.VINGT_HUIT;
                break;
            case 29:
                numeral = GeneralConst.NumeralString.VINGT_NEUF;
                break;
            case 30:
                numeral = GeneralConst.NumeralString.TRENTE;
                break;
            case 31:
                numeral = GeneralConst.NumeralString.TRENTE_UN;
                break;

        }

        return numeral;
    }

    public static String getAdressesByPersonne(Personne personne) {

        List<JSONObject> adresses = new ArrayList<>();

        try {
            if (personne != null) {
                if (personne.getAdressePersonneList() != null) {
                    for (AdressePersonne adressePersonne : personne.getAdressePersonneList()) {

                        if (adressePersonne.getEtat() == true) {

                            JSONObject adresse = new JSONObject();
                            adresse.put(IdentificationConst.ParamName.CODE_ENTITE_ADMINISTRATIVE,
                                    adressePersonne.getAdresse().getAvenue().getCode());

                            adresse.put(IdentificationConst.ParamName.DEFAUT,
                                    adressePersonne.getParDefaut()
                                            ? GeneralConst.YES_VALUE
                                            : GeneralConst.EMPTY_STRING);

                            adresse.put(IdentificationConst.ParamName.CHAINE_ADRESSE,
                                    adressePersonne.getAdresse().toString());

                            adresse.put(IdentificationConst.ParamName.NUMERO,
                                    adressePersonne.getAdresse().getNumero());

                            adresse.put(IdentificationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                    adressePersonne.getCode());

                            adresses.add(adresse);
                        }

                    }
                }
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return adresses.toString();
    }

    public static String getJsonListToString(String listJsonValue, String KeyValue) throws JSONException {

        String result = GeneralConst.EMPTY_STRING;
        try {
            JSONArray jSONArray = new JSONArray(listJsonValue);
            List<String> codeList = new ArrayList<>();
            String listCode;

            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject object = jSONArray.getJSONObject(i);
                String code = object.getString(KeyValue);
                codeList.add(code);
            }
            if (!codeList.isEmpty()) {
                for (int i = 0; i < jSONArray.length(); i++) {
                    if (i < jSONArray.length() - 1) {
                        listCode = "'" + codeList.get(i) + "'" + GeneralConst.VIRGULE;
                    } else {
                        listCode = "'" + codeList.get(i) + "'";
                    }
                    result = result + listCode;
                }
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.FAILED_OPERATION;
        }

        return result;
    }

    public static String getListToString(String siteValueList) {

        String result = GeneralConst.EMPTY_STRING;
        result = siteValueList.replace("\"", "'")
                .replace("[", GeneralConst.EMPTY_STRING)
                .replace("]", GeneralConst.EMPTY_STRING);

        return result;
    }

    public static String getListToStringV2(String siteValueList) {

        String result = GeneralConst.EMPTY_STRING;
        result = siteValueList.replace("\"", "")
                .replace("[", GeneralConst.EMPTY_STRING)
                .replace("]", GeneralConst.EMPTY_STRING);

        return result;
    }

    public static ArticleBudgetaire getAmendeBySecteur(String codeSecteur, String natureTaxe) {
        ArticleBudgetaire ab;
        try {
            ab = TaxationBusiness.getAmendeTransactionnelBySecteur(
                    natureTaxe, codeSecteur);
        } catch (Exception e) {
            throw e;
        }

        return ab;

    }

    public static void main(String[] args) {

        //String toDays = Tools.getDateString(new Date(), true);
        //String toDays = ConvertDate.getYearPartDateFromDateParam("04/03/2021");
        //Double double1 = Double.valueOf("4524545.58");
        //Double double1 = Double.valueOf("4524545.58");
//        String x = "002152/GUT/N°024555";
//        String noteDebit = x.substring(0, 6);
        /* String dateToDay = ConvertDate.formatDateToStringOfFormat(new Date(), GeneralConst.Format.FORMAT_DATE);
         String partMonth = dateToDay.substring(3, dateToDay.length());
         partMonth = partMonth.substring(0, dateToDay.length() - 3);
         partMonth = partMonth.replace("/", "-");
         System.out.println("Result : " + partMonth);*/
        String dateToDay = ConvertDate.formatDateToStringOfFormat(new Date(), GeneralConst.Format.FORMAT_DATE);
        String partMonth = dateToDay.substring(3, dateToDay.length());
        partMonth = partMonth.substring(0, 2);
        //partMonth = partMonth.replace("/", "-");
        
        String val = "08";
        int val2 = Integer.valueOf(val);
        
        System.out.println("Result : " + val2);

    }

    public static String getPartMonthDate(Date date) {

        String partMonth = "";

        String dateToDay2 = ConvertDate.formatDateToStringOfFormat(date, GeneralConst.Format.FORMAT_DATE);
        partMonth = dateToDay2.substring(3, dateToDay2.length());
        return partMonth = partMonth.substring(0, 2);
    }

    public static BigDecimal convertAmountToCDF(BigDecimal amount, BigDecimal taux) {
        BigDecimal amountConvert = new BigDecimal(GeneralConst.Number.ZERO);
        try {
            amountConvert = amount.multiply(taux);
        } catch (Exception e) {

        }
        return amountConvert;
    }

    public static Integer getCodeAgentFromSession(HttpServletRequest request) {
        Integer valueInterge = GeneralConst.EMPTY_ZERO;
        try {
            HttpSession session = request.getSession();
            Agent agent = (Agent) session.getAttribute(GeneralConst.KEY_SESSION_AGENT);
            if (agent != null) {
                valueInterge = agent.getCode();
            }
        } catch (Exception e) {
            valueInterge = GeneralConst.EMPTY_ZERO;
        }
        return valueInterge;
    }

    public static String getTableHtmlV(
            List<?> currentObject,
            int[] tableau,
            boolean boldLastRow,
            boolean fusion) {
        StringBuilder lbl = new StringBuilder();
        try {
            lbl = new StringBuilder();
            lbl.append("<table style=\"border-collapse: collapse;width:100%;font-size:12px\">");
            for (int j = GeneralConst.Numeric.ZERO; j < currentObject.size(); j++) {
                lbl.append("<tr>");
                List<Field> currentField = getPublicFields(currentObject.get(j).getClass());
                for (int i = GeneralConst.Numeric.ZERO; i < currentField.size(); i++) {
                    String fieldStr = currentField.get(i).getName();
                    String formatedFieldName = String.valueOf(fieldStr.charAt(
                            GeneralConst.Numeric.ZERO)).toUpperCase() + fieldStr.substring(1);
                    if (j == GeneralConst.Numeric.ZERO) {
                        String replaceFormatedFieldName = formatedFieldName.replace("_", GeneralConst.SPACE).toUpperCase();
                        lbl.append("<th style=\"border:1px solid black;text-align:center\">").append(replaceFormatedFieldName).append("</th>");
                    }
                }
                lbl.append("</tr>");
            }
            for (int j = GeneralConst.Numeric.ZERO; j < currentObject.size(); j++) {
                List<Field> currentField = getPublicFields(currentObject.get(j).getClass());
                String rowStyle = "<tr style=\"border-collapse: collapse;width:100%;font-size:12px\">";

                if (boldLastRow) {

                    if (j == (currentObject.size() - GeneralConst.Numeric.ONE)
                            || j == (currentObject.size() - GeneralConst.Numeric.TWO)) {
                        rowStyle = "<tr style=\"border-collapse: collapse;width:100%;font-size:12px;font-weight:bold\">";

                    }
                }

                lbl.append(rowStyle);

                for (int i = GeneralConst.Numeric.ZERO; i < currentField.size(); i++) {

                    //System.out.println("i :");
                    String contentTD = "<td style=\"border:1px solid black;padding: 3px;text-align:left\">";

                    for (int k = GeneralConst.Numeric.ZERO; k < tableau.length; k++) {

                        if (i == 2) {
                            contentTD = "<td style=\"border:1px solid black;padding: 3px;text-align:left;vertical-align:middle;width:200px\">";
                        } else if (i == 3) {
                            contentTD = "<td style=\"border:1px solid black;padding: 3px;text-align:left;vertical-align:middle;width:300px\">";
                        } else if (i == 6) {
                            contentTD = "<td style=\"border:1px solid black;padding: 3px;text-align:center;vertical-align:middle;width:100px\">";
                        } else if (i == tableau[k]) {
                            contentTD = "<td style=\"border:1px solid black;padding: 3px;text-align:right;vertical-align:middle;width:150px\">";
                            break;
                        }
                    }

                    if (j == (currentObject.size() - GeneralConst.Numeric.ONE)
                            || j == (currentObject.size() - GeneralConst.Numeric.TWO)) {

                        if (fusion) {
                            if (i < GeneralConst.Numeric.THREE) {
                                continue;
                            }
                            if (i == GeneralConst.Numeric.THREE) {
                                contentTD = "<td colspan=\"4\" style=\"border:1px solid black;padding: 3px;text-align:left\">";
                            }
                        }
                    }

                    String fieldStr = currentField.get(i).getName();
                    String formatedFieldName = String.valueOf(fieldStr.charAt(
                            GeneralConst.Numeric.ZERO)).toUpperCase() + fieldStr.substring(GeneralConst.Numeric.ONE);
                    lbl.append(contentTD).append(getValue(currentObject.get(j), formatedFieldName)).append("</td>");
                }
                lbl.append("</tr>");
            }

            lbl.append("</table>");

        } catch (Exception e) {
        }
        return lbl.toString();
    }

    public static String getListObservations(String numeroDoc, String module) {

        String result = GeneralConst.EMPTY_STRING;
        List<JsonObject> listJsonObject = new ArrayList<>();
        JsonObject jsonObject = new JsonObject();

        try {

            switch (module.trim()) {

                case GeneralConst.Module.MOD_RECOUVREMENT:

                    Role role = new Role();

                    role = RecouvrementBusiness.getRoleById(numeroDoc.trim());

                    if (role != null) {

                        jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_CREATE_EXISTS,
                                GeneralConst.Number.ZERO);
                        jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_CREATE_VALUES,
                                GeneralConst.EMPTY_STRING);

                        if (role.getObservationReceveur() != null && !role.getObservationReceveur().isEmpty()) {

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_EXISTS_FIRST_LEVEL,
                                    GeneralConst.Number.ONE);

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_VALUES_FIRST_LEVEL,
                                    role.getObservationReceveur());

                        } else {
                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_EXISTS_FIRST_LEVEL,
                                    GeneralConst.Number.ZERO);

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_VALUES_FIRST_LEVEL,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (role.getObservationDirection() != null && !role.getObservationDirection().isEmpty()) {

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_EXISTS_SECOND_LEVEL,
                                    GeneralConst.Number.ONE);

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_VALUES_SECOND_LEVEL,
                                    role.getObservationDirection());

                        } else {

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_EXISTS_SECOND_LEVEL,
                                    GeneralConst.Number.ZERO);

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_VALUES_SECOND_LEVEL,
                                    GeneralConst.EMPTY_STRING);

                        }

                        listJsonObject.add(jsonObject);

                        result = listJsonObject.toString();

                    } else {
                        result = GeneralConst.ResultCode.FAILED_OPERATION;
                    }

                    break;

                case GeneralConst.Module.MOD_TAXATION:
                case GeneralConst.Module.MOD_ORDONNANCEMENT:

                    NoteCalcul noteCalcul = new NoteCalcul();

                    noteCalcul = TaxationBusiness.getNoteCalculByNumero(numeroDoc.trim());

                    if (noteCalcul != null) {

                        if (noteCalcul.getObservation() != null
                                && !noteCalcul.getObservation().isEmpty()) {

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_CREATE_EXISTS,
                                    GeneralConst.Number.ONE);

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_CREATE_VALUES,
                                    noteCalcul.getObservation());

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_EXISTS_FIRST_LEVEL,
                                    GeneralConst.Number.ZERO);

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_EXISTS_SECOND_LEVEL,
                                    GeneralConst.Number.ZERO);

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_VALUES_FIRST_LEVEL,
                                    GeneralConst.EMPTY_STRING);

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_VALUES_SECOND_LEVEL,
                                    GeneralConst.EMPTY_STRING);

                            listJsonObject.add(jsonObject);

                            result = listJsonObject.toString();

                        } else {
                            result = GeneralConst.ResultCode.FAILED_OPERATION;
                        }

                    } else {
                        result = GeneralConst.ResultCode.FAILED_OPERATION;
                    }

                    break;

                case GeneralConst.Module.MOD_NOTE_PERCEPTION:

                    NotePerception notePerception = new NotePerception();
                    notePerception = NotePerceptionBusiness.getNotePerceptionByCode(numeroDoc.trim());

                    break;

                case GeneralConst.Module.MOD_RECLAMATION:

                    Reclamation reclamation = new Reclamation();
                    reclamation = ContentieuxBusiness.getReclamationById(numeroDoc.trim());

                    if (reclamation != null) {

                        if (reclamation.getObservation() != null
                                && !reclamation.getObservation().isEmpty()) {

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_CREATE_EXISTS,
                                    GeneralConst.Number.ONE);

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_CREATE_VALUES,
                                    reclamation.getObservation());

                        } else {

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_CREATE_EXISTS,
                                    GeneralConst.Number.ZERO);

                            jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_CREATE_VALUES,
                                    GeneralConst.EMPTY_STRING);
                        }

                        jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_EXISTS_FIRST_LEVEL,
                                GeneralConst.Number.ZERO);

                        jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_VALUES_FIRST_LEVEL,
                                GeneralConst.EMPTY_STRING);

                        jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_EXISTS_SECOND_LEVEL,
                                GeneralConst.Number.ZERO);

                        jsonObject.addProperty(GeneralConst.ParamName.OBSERVATION_VALIDATE_VALUES_SECOND_LEVEL,
                                GeneralConst.EMPTY_STRING);

                        listJsonObject.add(jsonObject);
                        result = listJsonObject.toString();

                    } else {
                        result = GeneralConst.ResultCode.FAILED_OPERATION;
                    }

                    break;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.FAILED_OPERATION;
        }

        return result;
    }

    public static String getDataTableToHtmlV3(
            List<AssujettiListPrint> assujettiListPrints,
            List<DetailRolePrint> listDetailRolePrint) throws InvocationTargetException {

        StringBuilder lbl;
        lbl = new StringBuilder();

        for (AssujettiListPrint alp : assujettiListPrints) {

            lbl.append("<table style=\"border-collapse: collapse;width:100%;font-size:14px;\">");
            lbl.append("<thead style=\"border:0px solid black;text-align:left\">");
            lbl.append("<tr style=\"background-color:#CCC\">");
            lbl.append("<td colspan = \"6\" style=\"font-size:14;font-weight:bold;vertical-align:middle\">");
            lbl.append("Nif : ".concat(alp.getNif()).concat(" -- ")
                    .concat("Noms assujetti : ".concat(alp.getRaison_Sociale())).concat(" -- ")
                    .concat("Téléphone : ".concat(alp.getTelephone()))
                    .concat(" -- ").concat("Adresse : ".concat(alp.getSiege_Social())));
            lbl.append("</td>");
            lbl.append("</tr>");
            lbl.append("</thead>");
            lbl.append("<tbody>");
            lbl.append("</tbody>");
            lbl.append("</table> <br/><br/>");

            List<DetailRolePrint> detailRolePrints = new ArrayList<>();
            PrintDocument printDocument = new PrintDocument();

            for (DetailRolePrint detailRolePrint : listDetailRolePrint) {

                if (alp.getCodeAssujetti().trim().equals(detailRolePrint.getAssujettiCode().trim())) {
                    detailRolePrints.add(detailRolePrint);
                    break;
                }
            }

            lbl.append(GeneralConst.EMPTY_STRING);
            lbl.append(printDocument.preparedDetailProjectRole(detailRolePrints));
            lbl.append("<br/><br/>");

        }

        BigDecimal sumAmountPrincipal = new BigDecimal(GeneralConst.Number.ZERO);
        BigDecimal sumAmountPenalite = new BigDecimal(GeneralConst.Number.ZERO);
        BigDecimal sumAmountTotal = new BigDecimal(GeneralConst.Number.ZERO);
        BigDecimal sumAmountTotalTresor = new BigDecimal(GeneralConst.Number.ZERO);
        BigDecimal sumAmountTotalBap = new BigDecimal(GeneralConst.Number.ZERO);
        BigDecimal sumAmountTotalGeneral = new BigDecimal(GeneralConst.Number.ZERO);

        for (DetailRolePrint detailRolePrint : listDetailRolePrint) {

            BigDecimal amountPrincipal;
            BigDecimal amountPenalite;
            BigDecimal amountTotal;
            BigDecimal amountTotalTresor;
            BigDecimal amountTotalBap;

            double diviseur = GeneralConst.Numeric.TWO;

            amountPrincipal = BigDecimal.valueOf(Double.valueOf(detailRolePrint.getAmountPrincipal()));
            amountPenalite = BigDecimal.valueOf(Double.valueOf(detailRolePrint.getAmountPenalite()));
            amountTotalTresor = amountPenalite.divide(BigDecimal.valueOf(diviseur));
            amountTotalTresor = amountTotalTresor.add(amountPrincipal);
            amountTotalBap = amountPenalite.divide(BigDecimal.valueOf(diviseur));
            amountTotal = amountPrincipal;
            amountTotal = amountTotal.add(amountPenalite);

            sumAmountPrincipal = sumAmountPrincipal.add(amountPrincipal);
            sumAmountPenalite = sumAmountPenalite.add(amountPenalite);
            sumAmountTotal = sumAmountTotal.add(amountTotal);
            sumAmountTotalTresor = sumAmountTotalTresor.add(amountTotalTresor);
            sumAmountTotalBap = sumAmountTotalBap.add(amountTotalBap);
            sumAmountTotalGeneral = sumAmountTotalGeneral.add(amountTotal);

        }

        lbl.append("<tr style=\"background-color:#FFF\">");
        lbl.append("<td colspan = \"6\" style=\"font-size:15;font-weight:bold;border:0px solid black;text-align:left;vertical-align:middle\">");
        lbl.append("TOTAL GENERAL PRINCIPAL : ".concat(Tools.formatNombreToString(
                GeneralConst.Format.FORMAT_AMOUNT,
                sumAmountPrincipal)).concat(GeneralConst.Devise.DEVISE_CDF));
        lbl.append("</td>");
        lbl.append("</tr>");

        lbl.append("<tr style=\"background-color:#FFF\">");
        lbl.append("<td colspan = \"6\" style=\"font-size:15;font-weight:bold;border:0px solid black;text-align:left;vertical-align:middle\">");
        lbl.append("TOTAL GENERAL PENALITE : ".concat(Tools.formatNombreToString(
                GeneralConst.Format.FORMAT_AMOUNT,
                sumAmountPenalite)).concat(GeneralConst.Devise.DEVISE_CDF));
        lbl.append("</td>");
        lbl.append("</tr>");

        return lbl.toString();
    }

    public static String generateDocumentNumber(String codeAgent, String typeDocument, String codeDocument) {

        String document_Incr = codeDocument;
        String requestIncr = GeneralConst.EMPTY_STRING;

        String sqlQuery = GeneralConst.EMPTY_STRING;

        String documentNumero = GeneralConst.EMPTY_STRING;
        Agent agent = GeneralBusiness.getAgentByCode(codeAgent);
        String codeSite = agent.getSite().getCode();
        String documentPrefix = GeneralConst.EMPTY_STRING;

        switch (typeDocument.trim()) {
            case "AMR":
            case "AMR_OF_INVIT_PAYER":
                documentPrefix = "AMR";
                sqlQuery = "UPDATE T_AMR SET NUMERO_DOCUMENT = '%s' WHERE NUMERO = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_AMR('" + codeSite + "')";
                break;
            case "COMMANDEMENT":
                documentPrefix = "CMD";
                sqlQuery = "UPDATE T_COMMANDEMENT SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_COMMANDEMENT('" + codeSite + "')";
                break;
            case "CONTRAINTE":
                documentPrefix = "CNT";
                sqlQuery = "UPDATE T_CONTRAINTE SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_CONTRAINTE('" + codeSite + "')";
                break;
            case "MED":
            case "INVITATION_SERVICE":
                //documentPrefix = "ISA";
                documentPrefix = "";
                sqlQuery = "UPDATE T_MED SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_MED('INVITATION_SERVICE','" + codeSite + "')";
                break;
            case "INVITATION_PAIEMENT":
                //documentPrefix = "IAP";
                documentPrefix = "";
                sqlQuery = "UPDATE T_MED SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_MED('INVITATION_PAIEMENT','" + codeSite + "')";
                break;
            case "SERVICE":
                documentPrefix = "ISB";
                sqlQuery = "UPDATE T_MED SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_MED('SERVICE','" + codeSite + "')";
                break;
            case "RELANCE":
                //documentPrefix = "RL";
                documentPrefix = "";
                sqlQuery = "UPDATE T_MED SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_MED('RELANCE','" + codeSite + "')";
                break;
            case "PAIEMENT":
                sqlQuery = "UPDATE T_MED SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_MED('PAIEMENT','" + codeSite + "')";
                break;
            case "ATD":
                documentPrefix = "ATD";
                sqlQuery = "UPDATE T_ATD SET NUMERO_DOCUMENT = '%s' WHERE ID = '%s'";
                requestIncr = "SELECT dbo.F_GET_LAST_INCREMENT_ATD('" + codeSite + "')";
                break;
        }

        document_Incr = incr(GeneralBusiness.getLastIncrement(requestIncr), 4);

        String dateToDay = ConvertDate.formatDateToStringOfFormat(new Date(), GeneralConst.Format.FORMAT_DATE);
        String partMonth = dateToDay.substring(3, dateToDay.length());
        //partMonth = partMonth.substring(0, dateToDay.length() - 8);
        partMonth = partMonth.substring(0, dateToDay.length() - 3);
        //partMonth = partMonth.replace("/", "-");

        Site site = getSiteByCode(codeSite);

        //documentNumero = "DRHKAT" + "/" + site.getDivision().getSigle().trim() + "/" + site.getSigle() + "/" + partMonth + "/" + documentPrefix.concat(document_Incr);
        documentNumero = documentPrefix.concat(document_Incr) + "/DRHKAT" + "/" + site.getDivision().getSigle().trim() + "/" + site.getSigle() + "/" + partMonth;

        sqlQuery = String.format(sqlQuery, documentNumero, codeDocument);

        if (GeneralBusiness.updateNumeroDoc(sqlQuery)) {
            return documentNumero;
        } else {
            return codeDocument;
        }
    }

    public static String incr(int intCode, int nbrOrdrSize) {

        String key = GeneralConst.EMPTY_STRING;
        String zero = GeneralConst.EMPTY_STRING;;
        try {
            intCode += 1;
            int taille = String.valueOf(intCode).length();

            for (int i = 1; i < nbrOrdrSize - (taille - 1); i++) {
                zero += "0";
            }
            key = zero + intCode;

        } catch (Exception e) {

            for (int i = 1; i < nbrOrdrSize; i++) {
                zero += "0";
            }
            key = zero + 1;
        }
        return key;
    }

    public static String encoder(String filePath) {
        String base64File = "data:image/png;base64,";
        File file = new File(filePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            byte fileData[] = new byte[(int) file.length()];
            imageInFile.read(fileData);
            base64File += Base64.encodeBase64String(fileData);
        } catch (FileNotFoundException e) {
            return "";
        } catch (IOException ioe) {
            return "";
        }
        return base64File;
    }

    public static int getNumberMonth(String codePeriode) {

        int nbre = 1;

        try {

            switch (codePeriode) {
                case "PR0032015": //MENSUELLE
                    nbre = 1;
                    break;
                case "PR0072015": //SEMESTRIELLE
                    nbre = 6;
                    break;
                case "PR0052015": // TRIMESTRIEL
                    nbre = 3;
                    break;
                case "PR0042015": // ANNUELLE
                    nbre = 12;
                    break;
            }

        } catch (Exception e) {
        }

        return nbre = 1;
    }

    public static BigDecimal getSumPenaliteNotePerception(NotePerception npEnrolee) {

        BigDecimal amountPenalite = new BigDecimal("0");

        try {

            for (NotePerception np : npEnrolee.getNotePerceptionList()) {

                amountPenalite = amountPenalite.add(np.getNetAPayer());
            }

        } catch (Exception e) {
        }

        return amountPenalite;
    }

    public static String getTauxPalierATCommercial(String typePersonne, String codeTarif) {

        String taux = "";

        try {

            switch (codeTarif) {
                case "0000070120": //1 RANG
                case "0000070220": //1 RANG

                    switch (typePersonne) {
                        case "03": //PM
                            taux = "25";
                            break;
                        case "04": // PP
                            taux = "15";
                            break;
                    }
                    break;
                case "0000070320": //2 RANG
                case "0000070420": //2 RANG
                    switch (typePersonne) {
                        case "03": //PM
                            taux = "20";
                            break;
                        case "04": // PP
                            taux = "10";
                            break;
                    }
                    break;
                case "0000070520": //3 RANG
                case "0000070620": //3 RANG
                    switch (typePersonne) {
                        case "03": //PM
                            taux = "15";
                            break;
                        case "04": // PP
                            taux = "7.5";
                            break;
                    }
                    break;
                case "0000070720": //4 RANG BATIS
                case "0000070820": //4 RANG NON BATIS
                    switch (typePersonne) {
                        case "03": //PM
                            taux = "10";
                            break;
                        case "04": // PP
                            taux = "5";
                            break;
                    }
                    break;
            }

        } catch (Exception e) {
        }

        return taux;
    }

    public static String sendNotification(Personne personne, boolean isValidate) throws IOException {

        PropertiesConfig propertiesConfig = new PropertiesConfig();

        String mailjetUser = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_USERNAME2);
        String mailjetPassword = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_PASSWORD2);
        String mailjetURL = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_URL2);
        String mailSmtp = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SMTP);

        String mailSubscribeContent = "";
        if (isValidate) {
            mailSubscribeContent = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SUBSCRIBE_CLIENT_CONTENT_VALIDATE);
        } else {
            mailSubscribeContent = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SUBSCRIBE_CLIENT_CONTENT_REJECTED);
        }

        String mailSubscribeSubjet = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SUBSCRIBE_CLIENT_SUBJECT_VALIDATE);
        String urlErecettes = propertiesConfig.getContent(PropertiesConst.Config.ERECETTES_LINK2);

        LoginWeb login = IdentificationBusiness.getLoginWebByPersonneV2(personne.getLoginWeb().getFkPersonne());

        //String passWordNTD = personne.getLoginWeb().getPassword().trim();
        String passWordNTD = "123456";
        String mailContent = String.format(mailSubscribeContent, personne.toString(), login.getUsername(), passWordNTD, urlErecettes.trim());

        MailSender mailSender = new MailSender();
        mailSender.setDate(new Date());
        mailSender.setHost(mailjetURL);
        mailSender.setSubject(mailSubscribeSubjet);
        mailSender.setMessage(mailContent);
        mailSender.setReciever(personne.getLoginWeb().getMail().trim());
        mailSender.setSender(mailSmtp);

        mailSender.sendMailWithAPIV2(mailjetUser, mailjetPassword);

        if (IdentificationBusiness.updateStateSendMail(personne.getCode())) {
            return "1";
        } else {
            return "0";
        }

    }

    public static String sendMailNotificationAllPerson(List<Personne> personnes, boolean isValidate) throws IOException {

        String result = "0";

        PropertiesConfig propertiesConfig = new PropertiesConfig();

        String mailjetUser = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_USERNAME2);
        String mailjetPassword = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_PASSWORD2);
        String mailjetURL = propertiesConfig.getContent(PropertiesConst.Config.MAILJET_URL2);
        String mailSmtp = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SMTP);

        String mailSubscribeContent = "";
        if (isValidate) {
            mailSubscribeContent = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SUBSCRIBE_CLIENT_CONTENT_VALIDATE);
        } else {
            mailSubscribeContent = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SUBSCRIBE_CLIENT_CONTENT_REJECTED);
        }

        String mailSubscribeSubjet = propertiesConfig.getContent(PropertiesConst.Config.MAIL_SUBSCRIBE_CLIENT_SUBJECT_VALIDATE);
        String urlErecettes = propertiesConfig.getContent(PropertiesConst.Config.ERECETTES_LINK2);

        for (Personne personne : personnes) {

            LoginWeb login = IdentificationBusiness.getLoginWebByPersonneV2(personne.getLoginWeb().getFkPersonne());

            String passWordNTD = "123456";
            String mailContent = String.format(mailSubscribeContent, personne.toString(), login.getUsername(), passWordNTD, urlErecettes.trim());

            MailSender mailSender = new MailSender();
            mailSender.setDate(new Date());
            mailSender.setHost(mailjetURL);
            mailSender.setSubject(mailSubscribeSubjet);
            mailSender.setMessage(mailContent);
            mailSender.setReciever(personne.getLoginWeb().getMail().trim());
            mailSender.setSender(mailSmtp);

            mailSender.sendMailWithAPIV2(mailjetUser, mailjetPassword);

            if (IdentificationBusiness.updateStateSendMail(personne.getCode())) {
                result = "1";
            } else {
                result = "0";
            }
        }

        return result;
    }

    public static TMBPayBillRequest getPayBillRequestFromXML(String paymentInfo, boolean defaultBankAccount) {

        TMBPayBillRequest payBillRequest = null;

        try {

            NodeList nodeList;
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource source = new InputSource();
            source.setCharacterStream(new StringReader(paymentInfo));
            Document doc = dBuilder.parse(source);
            doc.getDocumentElement().normalize();

            nodeList = doc.getElementsByTagName("BillIssuerAPI");
            if (nodeList.getLength() == 0) {
                nodeList = doc.getElementsByTagName("billIssuerAPI");
            }
            for (int temp = 0; temp < nodeList.getLength(); temp++) {
                payBillRequest = new TMBPayBillRequest();
                Node nNode = nodeList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    org.w3c.dom.Element eElement;
                    eElement = (org.w3c.dom.Element) nNode;

                    payBillRequest.setType(eElement.getElementsByTagName("type").item(0) != null ? eElement.getElementsByTagName("type").item(0).getTextContent() : "");
                    payBillRequest.setUserFirstName(eElement.getElementsByTagName("user_first_name").item(0) != null ? eElement.getElementsByTagName("user_first_name").item(0).getTextContent() : "");
                    payBillRequest.setUserLastName(eElement.getElementsByTagName("user_last_name").item(0) != null ? eElement.getElementsByTagName("user_last_name").item(0).getTextContent() : "");
                    payBillRequest.setUserLang(eElement.getElementsByTagName("user_lang").item(0) != null ? eElement.getElementsByTagName("user_lang").item(0).getTextContent() : "");
                    payBillRequest.setUserMobile(eElement.getElementsByTagName("user_mobile").item(0) != null ? eElement.getElementsByTagName("user_mobile").item(0).getTextContent() : "");
                    payBillRequest.setPayMode(eElement.getElementsByTagName("pay_mode").item(0) != null ? eElement.getElementsByTagName("pay_mode").item(0).getTextContent() : "");
                    payBillRequest.setBillUserId(eElement.getElementsByTagName("billissuer_id").item(0) != null ? eElement.getElementsByTagName("billissuer_id").item(0).getTextContent() : "");
                    payBillRequest.setBillIssuerName(eElement.getElementsByTagName("billissuer_name").item(0) != null ? eElement.getElementsByTagName("billissuer_name").item(0).getTextContent() : "");
                    payBillRequest.setExternalReference(eElement.getElementsByTagName("user_external_reference").item(0) != null ? eElement.getElementsByTagName("user_external_reference").item(0).getTextContent() : "");
                    payBillRequest.setBillRef(eElement.getElementsByTagName("bill_ref").item(0) != null ? eElement.getElementsByTagName("bill_ref").item(0).getTextContent() : "");
                    payBillRequest.setOption(eElement.getElementsByTagName("option").item(0) != null ? eElement.getElementsByTagName("option").item(0).getTextContent() : "");
                    payBillRequest.setAgentShortCode(eElement.getElementsByTagName("agent_short_code").item(0) != null ? eElement.getElementsByTagName("agent_short_code").item(0).getTextContent() : "");
                    payBillRequest.setMedia(eElement.getElementsByTagName("media").item(0) != null ? eElement.getElementsByTagName("media").item(0).getTextContent() : "");
                    payBillRequest.setSessionId(eElement.getElementsByTagName("sessionId").item(0) != null ? eElement.getElementsByTagName("sessionId").item(0).getTextContent() : "");
                    payBillRequest.setAmount(eElement.getElementsByTagName("amount").item(0) != null ? eElement.getElementsByTagName("amount").item(0).getTextContent() : "");
                    payBillRequest.setCurrency(eElement.getElementsByTagName("currency").item(0) != null ? eElement.getElementsByTagName("currency").item(0).getTextContent() : "");
                    if (!defaultBankAccount) {
                        payBillRequest.setBankAccount(eElement.getElementsByTagName("bank_account").item(0) != null ? eElement.getElementsByTagName("bank_account").item(0).getTextContent() : "");
                    }

                }

            }
        } catch (Exception ex) {
            Logger.getLogger(TMBPayBillRequest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return payBillRequest;
    }

    public static List<String> getCodeCmdCarte(int size, int sizeCodeCmd) {
        List<String> codeCmdCarteList = new ArrayList<>();
        String generatedString;
        for (int i = 0; i < size; i++) {
            generatedString = RandomStringUtils.randomAlphanumeric(sizeCodeCmd);
            codeCmdCarteList.add(generatedString);
        }

        return codeCmdCarteList;
    }

    public static String getDateLivraisonVoucher(List<Voucher> voucherList) {

        String dateLivraison = "";

        for (Voucher voucher : voucherList) {

            for (DetailVoucher detailVoucher : voucher.getDetailVoucherList()) {

                if (detailVoucher.getDateGeneration() != null) {

                    dateLivraison = ConvertDate.formatDateHeureToStringV2(detailVoucher.getDateGeneration());
                }
            }
        }

        return dateLivraison;
    }

    public static String getInfoConsommationVoucher(Voucher voucher) {

        String result = "";
        int count = 0;

        if (!voucher.getDetailVoucherList().isEmpty()) {

            for (DetailVoucher detailVoucher : voucher.getDetailVoucherList()) {

                if (detailVoucher.getDateUtilisation() != null) {
                    count++;
                }
            }

            int reste = voucher.getQte() - count;

            String resteInfo = "";

            if (reste == 0) {
                resteInfo = " | Reste : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold;color:red'>".concat(
                        reste + "").concat("</span>"));

            } else {
                resteInfo = " | Reste : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold;color:green'>".concat(
                        reste + "").concat("</span>"));

            }

            result = count + " sur " + 100 + resteInfo;

        } else {
            result = count + " sur " + voucher.getQte();
        }

        return result;
    }

    public static String checkVoucherUseExisting(List<DetailVoucher> listDetailVoucher) {

        String result = "0";

        for (DetailVoucher detailVoucher : listDetailVoucher) {

            if (detailVoucher.getDateUtilisation() != null) {
                return result = "1";

            }
        }

        return result;
    }

    public static List<String> getCodeList(int numberZero, int numberCode, String sigle) {

        List<String> codeList = new ArrayList<>();

        String codeCmdCrt = "";
        String dt = "";

        try {
            Long lastIncret = GeneralBusiness.getLastNumberOfCode();

            for (int j = 1; j < numberCode + 1; j++) {
                String zero = "";
                lastIncret += 1;

                int taille = String.valueOf(lastIncret).length();

                for (int i = 0; i < (numberZero - taille); i++) {
                    zero += "0";
                }

                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy");
                dt = dateFormat.format(date);

                codeCmdCrt += sigle + "" + zero + "" + lastIncret + "" + dt;
                codeList.add(codeCmdCrt);
                codeCmdCrt = "";
            }
        } catch (Exception e) {
            Object ex = null;
            Logger.getLogger(TMBPayBillRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return codeList;
    }

    public static String generateQRCodeV2(String contenu) {
        String qrBase64 = GeneralConst.EMPTY_STRING;
        StreamedContent file = null;
        try {
            ByteArrayOutputStream out = QRCode.from(contenu).to(ImageType.PNG).stream();
            ByteArrayInputStream qrIn = new ByteArrayInputStream(out.toByteArray());
            file = new DefaultStreamedContent(qrIn, GeneralConst.Format.FORMAT_IMAGE);
            qrBase64 = Tools.getByteaToBase64String(out.toByteArray());
        } catch (Exception e) {
            throw e;
        }
        return qrBase64;
    }

    public static String getFormatTableBienTaxation(List<BienTaxationPrint> listBienTaxationPrint, String devise) throws InvocationTargetException {

        StringBuilder lbl = null;
        lbl = new StringBuilder();

        lbl.append("<table style=\"border-collapse: collapse;width:100%;font-size:14px;\">");
        lbl.append("<tbody>");
        lbl.append("<tr>");

        lbl.append("<th style=\"border:1px solid black;text-align:left;background-color:#CCC\">");
        lbl.append("CATEGORIE");
        lbl.append("</th>");

        lbl.append("<th style=\"border:1px solid black;text-align:center;background-color:#CCC\">");
        lbl.append("QUANTITE");
        lbl.append("</th>");

        lbl.append("<th style=\"border:1px solid black;text-align:center;background-color:#CCC\">");
        lbl.append("ANNEE");
        lbl.append("</th>");

        lbl.append("<th style=\"border:1px solid black;text-align:right;background-color:#CCC\">");
        lbl.append("MONTANT");
        lbl.append("</th>");

        lbl.append("<th style=\"border:1px solid black;text-align:right;background-color:#CCC\">");
        lbl.append("PENALITE");
        lbl.append("</th>");

        lbl.append("<th style=\"border:1px solid black;text-align:right;background-color:#CCC\">");
        lbl.append("TOTAL");
        lbl.append("</th>");

        lbl.append("</tr>");

        for (BienTaxationPrint bienTaxationPrint : listBienTaxationPrint) {

            lbl.append("<tr>");
            lbl.append("<td style=\"border:1px solid black;vertical-align:middle;text-align:left\">");
            lbl.append(bienTaxationPrint.getNature_De_Droit());
            lbl.append("</td>");

            lbl.append("<td style=\"border:1px solid black;vertical-align:middle;text-align:center\">");
            lbl.append(bienTaxationPrint.getQuantite());
            lbl.append("</td>");

            lbl.append("<td style=\"border:1px solid black;vertical-align:middle;text-align:center\">");
            lbl.append(bienTaxationPrint.getAnnee());
            lbl.append("</td>");

            lbl.append("<td style=\"border:1px solid black;vertical-align:middle;text-align:right\">");
            lbl.append(formatNombreToString("###,###.###",
                    BigDecimal.valueOf(Double.valueOf(bienTaxationPrint.Montant))) + GeneralConst.SPACE + devise);
            lbl.append("</td>");

            lbl.append("<td style=\"border:1px solid black;vertical-align:middle;text-align:right\">");
            lbl.append(formatNombreToString("###,###.###",
                    BigDecimal.valueOf(Double.valueOf(bienTaxationPrint.Penalite))) + GeneralConst.SPACE + devise);
            lbl.append("</td>");

            lbl.append("<td style=\"border:1px solid black;vertical-align:middle;text-align:right\">");
            lbl.append(formatNombreToString("###,###.###",
                    BigDecimal.valueOf(Double.valueOf(bienTaxationPrint.Montant_Total))) + GeneralConst.SPACE + devise);
            lbl.append("</td>");

            lbl.append("</tr>");
        }

        lbl.append("</tbody>");
        lbl.append("</table>");
        return lbl.toString();
    }
}
