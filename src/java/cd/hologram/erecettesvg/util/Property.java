/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.util;

import java.util.Properties;

/**
 *
 * @author emmanuel.tsasa
 */
public class Property {

    public static Properties getProperties(FileData fileData) {

        Properties prop = new java.util.Properties();
        try {
            String fileName = getFileName(fileData);
            prop.load(Property.class.getResourceAsStream("/cd/hologram/erecettesvg/properties/".concat(fileName)));
        } catch (Exception e) {
            CustumException.LogException(e);
        }
        return prop;
    }

    public static String getFileName(FileData fileData) {
        
        switch (fileData) {
            case APP_CONFIG:
                return "APP_CONFIG.properties";
            case FR_MESSAGE:
                return "FR_Message.properties";
            case FR_MAIL:
                return "FR_MAIL.properties";
        }
        return "";
    }

    public enum FileData {

        APP_CONFIG,
        FR_MESSAGE,
        FR_MAIL
    }

}
