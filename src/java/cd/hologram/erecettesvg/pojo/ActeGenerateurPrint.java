/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author moussa.toure
 */
public class ActeGenerateurPrint implements Serializable {

    public int numero;
    public String articlesBudgetaires;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getArticlesBudgetaires() {
        return articlesBudgetaires;
    }

    public void setArticlesBudgetaires(String articlesBudgetaires) {
        this.articlesBudgetaires = articlesBudgetaires;
    }

}
