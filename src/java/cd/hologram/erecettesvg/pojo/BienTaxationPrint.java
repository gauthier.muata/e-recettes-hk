/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author hp
 */
public class BienTaxationPrint implements Serializable{
    
    public String Nature_De_Droit;
    public String Quantite;
    public String Annee;
    public String Montant;
    public String Penalite;
    public String Montant_Total;
    
    public String getNature_De_Droit() {
        return Nature_De_Droit;
    }

    public void setNature_De_Droit(String Nature_De_Droit) {
        this.Nature_De_Droit = Nature_De_Droit;
    }

    public String getQuantite() {
        return Quantite;
    }

    public void setQuantite(String Quantite) {
        this.Quantite = Quantite;
    }

    public String getAnnee() {
        return Annee;
    }

    public void setAnnee(String Annee) {
        this.Annee = Annee;
    }

    public String getMontant() {
        return Montant;
    }

    public void setMontant(String Montant) {
        this.Montant = Montant;
    }

    public String getPenalite() {
        return Penalite;
    }

    public void setPenalite(String Penalite) {
        this.Penalite = Penalite;
    }

    public String getMontant_Total() {
        return Montant_Total;
    }

    public void setMontant_Total(String Montant_Total) {
        this.Montant_Total = Montant_Total;
    }
    
    
}
