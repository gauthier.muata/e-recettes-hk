/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.math.BigDecimal;

/**
 *
 * @author WILLY KASHALA
 */
public class DetailsReclamationPrint {
    
    String motif;
    String referenceDocument;
    String typeDocument;
    String devise;
    String ncDocumentReference;
    String documentId;
    String etataReclamation;
    BigDecimal montantContester;
    BigDecimal pourcentageMontantNonContster;

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getReferenceDocument() {
        return referenceDocument;
    }

    public void setReferenceDocument(String referenceDocument) {
        this.referenceDocument = referenceDocument;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getNcDocumentReference() {
        return ncDocumentReference;
    }

    public void setNcDocumentReference(String ncDocumentReference) {
        this.ncDocumentReference = ncDocumentReference;
    }

    public BigDecimal getMontantContester() {
        return montantContester;
    }

    public void setMontantContester(BigDecimal montantContester) {
        this.montantContester = montantContester;
    }

    public BigDecimal getPourcentageMontantNonContster() {
        return pourcentageMontantNonContster;
    }

    public void setPourcentageMontantNonContster(BigDecimal pourcentageMontantNonContster) {
        this.pourcentageMontantNonContster = pourcentageMontantNonContster;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    } 

    public String getEtataReclamation() {
        return etataReclamation;
    }

    public void setEtataReclamation(String etataReclamation) {
        this.etataReclamation = etataReclamation;
    }
    
}
