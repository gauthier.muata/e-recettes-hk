/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author WILLY KASHALA
 */
public class BonAPayerPrint implements Serializable{
    
    String acteGenerateur;
    String articleBudgetaire;
    String motifPenalite;
    String tauxPenalite;
    String montantChiffre;
    String montantLettre;
    String assujettiName;
    String rue;
    String commune;
    String phone;
    String numeroBp;
    String codeQR;
    String ordonnanteurName;
    String ordonnancementDate;
    String adresse;
    String numeroCheque;
    String dateCheque;
    String banque;
    String bordereau;
    String dateBordereau;
    String quittance;
    String percepteurName;
    String fonctionPercepteur;
    String datePerception;
    String chequePostal;
    String dateDebutChequePostal;
    String dateFinChequePostal;
    String accountBank;
    String especes;
    String codeBudgetaire;

    String nameMinistrie;
    String nameSector;
    String nameSite;
    String logo;
    
    String email;
    String nif;
    String dateEcheance;
    String arrete;

    public String getActeGenerateur() {
        return acteGenerateur;
    }

    public void setActeGenerateur(String acteGenerateur) {
        this.acteGenerateur = acteGenerateur;
    }

    public String getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(String articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public String getMotifPenalite() {
        return motifPenalite;
    }

    public void setMotifPenalite(String motifPenalite) {
        this.motifPenalite = motifPenalite;
    }

    public String getTauxPenalite() {
        return tauxPenalite;
    }

    public void setTauxPenalite(String tauxPenalite) {
        this.tauxPenalite = tauxPenalite;
    }

    public String getMontantChiffre() {
        return montantChiffre;
    }

    public void setMontantChiffre(String montantChiffre) {
        this.montantChiffre = montantChiffre;
    }

    public String getMontantLettre() {
        return montantLettre;
    }

    public void setMontantLettre(String montantLettre) {
        this.montantLettre = montantLettre;
    }

    public String getAssujettiName() {
        return assujettiName;
    }

    public void setAssujettiName(String assujettiName) {
        this.assujettiName = assujettiName;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNumeroBp() {
        return numeroBp;
    }

    public void setNumeroBp(String numeroBp) {
        this.numeroBp = numeroBp;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getOrdonnanteurName() {
        return ordonnanteurName;
    }

    public void setOrdonnanteurName(String ordonnanteurName) {
        this.ordonnanteurName = ordonnanteurName;
    }

    public String getOrdonnancementDate() {
        return ordonnancementDate;
    }

    public void setOrdonnancementDate(String ordonnancementDate) {
        this.ordonnancementDate = ordonnancementDate;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    public String getDateCheque() {
        return dateCheque;
    }

    public void setDateCheque(String dateCheque) {
        this.dateCheque = dateCheque;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    public String getBordereau() {
        return bordereau;
    }

    public void setBordereau(String bordereau) {
        this.bordereau = bordereau;
    }

    public String getDateBordereau() {
        return dateBordereau;
    }

    public void setDateBordereau(String dateBordereau) {
        this.dateBordereau = dateBordereau;
    }

    public String getQuittance() {
        return quittance;
    }

    public void setQuittance(String quittance) {
        this.quittance = quittance;
    }

    public String getPercepteurName() {
        return percepteurName;
    }

    public void setPercepteurName(String percepteurName) {
        this.percepteurName = percepteurName;
    }

    public String getFonctionPercepteur() {
        return fonctionPercepteur;
    }

    public void setFonctionPercepteur(String fonctionPercepteur) {
        this.fonctionPercepteur = fonctionPercepteur;
    }

    public String getDatePerception() {
        return datePerception;
    }

    public void setDatePerception(String datePerception) {
        this.datePerception = datePerception;
    }

    public String getChequePostal() {
        return chequePostal;
    }

    public void setChequePostal(String chequePostal) {
        this.chequePostal = chequePostal;
    }

    public String getDateDebutChequePostal() {
        return dateDebutChequePostal;
    }

    public void setDateDebutChequePostal(String dateDebutChequePostal) {
        this.dateDebutChequePostal = dateDebutChequePostal;
    }

    public String getDateFinChequePostal() {
        return dateFinChequePostal;
    }

    public void setDateFinChequePostal(String dateFinChequePostal) {
        this.dateFinChequePostal = dateFinChequePostal;
    }

    public String getAccountBank() {
        return accountBank;
    }

    public void setAccountBank(String accountBank) {
        this.accountBank = accountBank;
    }

    public String getEspeces() {
        return especes;
    }

    public void setEspeces(String especes) {
        this.especes = especes;
    }

    public String getCodeBudgetaire() {
        return codeBudgetaire;
    }

    public void setCodeBudgetaire(String codeBudgetaire) {
        this.codeBudgetaire = codeBudgetaire;
    }

    public String getNameMinistrie() {
        return nameMinistrie;
    }

    public void setNameMinistrie(String nameMinistrie) {
        this.nameMinistrie = nameMinistrie;
    }

    public String getNameSector() {
        return nameSector;
    }

    public void setNameSector(String nameSector) {
        this.nameSector = nameSector;
    }

    public String getNameSite() {
        return nameSite;
    }

    public void setNameSite(String nameSite) {
        this.nameSite = nameSite;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(String dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getArrete() {
        return arrete;
    }

    public void setArrete(String arrete) {
        this.arrete = arrete;
    }
    
    
    
}
