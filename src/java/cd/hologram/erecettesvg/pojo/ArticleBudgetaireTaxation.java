/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import cd.hologram.erecettesvg.models.*;

/**
 *
 * @author gauthier.muata
 */
public class ArticleBudgetaireTaxation {

    ArticleBudgetaire articleBudgetaire;
    Unite unite;
    String typeTaux;
    float baseCalcul;
    float taux;
    float montantDu;
    int qte;
    boolean penaliser;
    String tauxToString;
    String baseCalculToString;
    String montantDuToString;
    String periodeDeclaration;
    String devise;

    String codeTarif;
    String intituleTarif;
    String intituleArticle;

    public String getIntituleArticle() {
        return intituleArticle;
    }

    public void setIntituleArticle(String intituleArticle) {
        this.intituleArticle = intituleArticle;
    }

    public String getCodeTarif() {
        return codeTarif;
    }

    public void setCodeTarif(String codeTarif) {
        this.codeTarif = codeTarif;
    }

    public String getIntituleTarif() {
        return intituleTarif;
    }

    public void setIntituleTarif(String intituleTarif) {
        this.intituleTarif = intituleTarif;
    }

    public ArticleBudgetaire getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(ArticleBudgetaire articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public Unite getUnite() {
        return unite;
    }

    public void setUnite(Unite unite) {
        this.unite = unite;
    }

    public float getBaseCalcul() {
        return baseCalcul;
    }

    public void setBaseCalcul(float baseCalcul) {
        this.baseCalcul = baseCalcul;
    }

    public float getTaux() {
        return taux;
    }

    public void setTaux(float taux) {
        this.taux = taux;
    }

    public String getTypeTaux() {
        return typeTaux;
    }

    public void setTypeTaux(String typeTaux) {
        this.typeTaux = typeTaux;
    }

    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    public float getMontantDu() {
        return montantDu;
    }

    public void setMontantDu(float montantDu) {
        this.montantDu = montantDu;
    }

    public String getTauxToString() {
        return tauxToString;
    }

    public void setTauxToString(String tauxToString) {
        this.tauxToString = tauxToString;
    }

    public String getBaseCalculToString() {
        return baseCalculToString;
    }

    public void setBaseCalculToString(String baseCalculToString) {
        this.baseCalculToString = baseCalculToString;
    }

    public String getMontantDuToString() {
        return montantDuToString;
    }

    public void setMontantDuToString(String montantDuToString) {
        this.montantDuToString = montantDuToString;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getPeriodeDeclaration() {
        return periodeDeclaration;
    }

    public void setPeriodeDeclaration(String periodeDeclaration) {
        this.periodeDeclaration = periodeDeclaration;
    }

    public boolean isPenaliser() {
        return penaliser;
    }

    public void setPenaliser(boolean penaliser) {
        this.penaliser = penaliser;
    }
    
    

}
