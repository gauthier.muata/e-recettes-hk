/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author WILLY KASHALA
 */
public class RecepissePrint implements Serializable{
    
   String nomAssujetti;
   String numImpot;
   String sigle;
   String adresseAssujetti;
   String telephoneAssujetti;
   String boitePostal;
   String faxAssujetti;
   String emailAssujetti;
   String dateImpression;
   String dateDepot;
   String referenceDocument;
   String datePaiement;
   String montantPayer;
   String banque;
   String compteBancaire;
   String typeTitre;
   String nomValidateur;
   String numPage;
   String numDepotDeclaration;
   String numBordereau;
   String headTitle;
   String flag;
   String codeQR;
   String logo;
   String numRecepisse;
   String codeDeclaration;
   String signature;
   
   
   String lieuImpression;
   String noteTaxation;
   String periodeDeclaration;
   String fonctionValidateur;
   String adresseSite;
   String impotName;
   String codeDepotDeclaration;
   
    public String getNomAssujetti() {
        return nomAssujetti;
    }

    public void setNomAssujetti(String nomAssujetti) {
        this.nomAssujetti = nomAssujetti;
    }

    public String getNumImpot() {
        return numImpot;
    }

    public void setNumImpot(String numImpot) {
        this.numImpot = numImpot;
    }

    public String getSigle() {
        return sigle;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    public String getAdresseAssujetti() {
        return adresseAssujetti;
    }

    public void setAdresseAssujetti(String adresseAssujetti) {
        this.adresseAssujetti = adresseAssujetti;
    }

    public String getTelephoneAssujetti() {
        return telephoneAssujetti;
    }

    public void setTelephoneAssujetti(String telephoneAssujetti) {
        this.telephoneAssujetti = telephoneAssujetti;
    }

    public String getBoitePostal() {
        return boitePostal;
    }

    public void setBoitePostal(String boitePostal) {
        this.boitePostal = boitePostal;
    }

    public String getFaxAssujetti() {
        return faxAssujetti;
    }

    public void setFaxAssujetti(String faxAssujetti) {
        this.faxAssujetti = faxAssujetti;
    }

    public String getEmailAssujetti() {
        return emailAssujetti;
    }

    public void setEmailAssujetti(String emailAssujetti) {
        this.emailAssujetti = emailAssujetti;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getDateDepot() {
        return dateDepot;
    }

    public void setDateDepot(String dateDepot) {
        this.dateDepot = dateDepot;
    }

    public String getReferenceDocument() {
        return referenceDocument;
    }

    public void setReferenceDocument(String referenceDocument) {
        this.referenceDocument = referenceDocument;
    }

    public String getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(String datePaiement) {
        this.datePaiement = datePaiement;
    }

    public String getMontantPayer() {
        return montantPayer;
    }

    public void setMontantPayer(String montantPayer) {
        this.montantPayer = montantPayer;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getTypeTitre() {
        return typeTitre;
    }

    public void setTypeTitre(String typeTitre) {
        this.typeTitre = typeTitre;
    }

    public String getNomValidateur() {
        return nomValidateur;
    }

    public void setNomValidateur(String nomValidateur) {
        this.nomValidateur = nomValidateur;
    }

    public String getNumPage() {
        return numPage;
    }

    public void setNumPage(String numPage) {
        this.numPage = numPage;
    }

    public String getNumDepotDeclaration() {
        return numDepotDeclaration;
    }

    public void setNumDepotDeclaration(String numDepotDeclaration) {
        this.numDepotDeclaration = numDepotDeclaration;
    }

    public String getNumBordereau() {
        return numBordereau;
    }

    public void setNumBordereau(String numBordereau) {
        this.numBordereau = numBordereau;
    }

    public String getHeadTitle() {
        return headTitle;
    }

    public void setHeadTitle(String headTitle) {
        this.headTitle = headTitle;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    } 

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getNumRecepisse() {
        return numRecepisse;
    }

    public void setNumRecepisse(String numRecepisse) {
        this.numRecepisse = numRecepisse;
    } 

    public String getCodeDeclaration() {
        return codeDeclaration;
    }

    public void setCodeDeclaration(String codeDeclaration) {
        this.codeDeclaration = codeDeclaration;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getLieuImpression() {
        return lieuImpression;
    }

    public void setLieuImpression(String lieuImpression) {
        this.lieuImpression = lieuImpression;
    }

    public String getNoteTaxation() {
        return noteTaxation;
    }

    public void setNoteTaxation(String noteTaxation) {
        this.noteTaxation = noteTaxation;
    }

    public String getPeriodeDeclaration() {
        return periodeDeclaration;
    }

    public void setPeriodeDeclaration(String periodeDeclaration) {
        this.periodeDeclaration = periodeDeclaration;
    }

    public String getFonctionValidateur() {
        return fonctionValidateur;
    }

    public void setFonctionValidateur(String fonctionValidateur) {
        this.fonctionValidateur = fonctionValidateur;
    }

    public String getAdresseSite() {
        return adresseSite;
    }

    public void setAdresseSite(String adresseSite) {
        this.adresseSite = adresseSite;
    }

    public String getImpotName() {
        return impotName;
    }

    public void setImpotName(String impotName) {
        this.impotName = impotName;
    }

    public String getCodeDepotDeclaration() {
        return codeDepotDeclaration;
    }

    public void setCodeDepotDeclaration(String codeDepotDeclaration) {
        this.codeDepotDeclaration = codeDepotDeclaration;
    }
    
    
 
}
