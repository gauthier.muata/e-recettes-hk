/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author gauthier.muata
 */
public class MedPrint implements Serializable{
    
    String id;
    String logo;
    String numeroInvitationService;
    String codeQR;
    String numImpot;
    String nomAssujetti;
    String adresseAssujetti;
    String telephoneAssujetti;
    String emailAssujetti;
    String dateImpression;
    String numeroMed;
    String articleBudgetaire;
    String exercice;
    String echeance;
    String dateLimiteAccordee;
    String dateHeureInvitation;
    String agentSignateur;
    String periodeDeclaration;
    String impot;
    String lieuImpression;
    String impotObjet;
    String adresseBureau;
    
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getNumImpot() {
        return numImpot;
    }

    public void setNumImpot(String numImpot) {
        this.numImpot = numImpot;
    }

    public String getImpotObjet() {
        return impotObjet;
    }

    public void setImpotObjet(String impotObjet) {
        this.impotObjet = impotObjet;
    }
    
    public String getNomAssujetti() {
        return nomAssujetti;
    }

    public void setNomAssujetti(String nomAssujetti) {
        this.nomAssujetti = nomAssujetti;
    }

    public String getAdresseAssujetti() {
        return adresseAssujetti;
    }

    public void setAdresseAssujetti(String adresseAssujetti) {
        this.adresseAssujetti = adresseAssujetti;
    }

    public String getTelephoneAssujetti() {
        return telephoneAssujetti;
    }

    public void setTelephoneAssujetti(String telephoneAssujetti) {
        this.telephoneAssujetti = telephoneAssujetti;
    }

    public String getEmailAssujetti() {
        return emailAssujetti;
    }

    public void setEmailAssujetti(String emailAssujetti) {
        this.emailAssujetti = emailAssujetti;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getNumeroMed() {
        return numeroMed;
    }

    public void setNumeroMed(String numeroMed) {
        this.numeroMed = numeroMed;
    }

    public String getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(String articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public String getExercice() {
        return exercice;
    }

    public void setExercice(String exercice) {
        this.exercice = exercice;
    }

    public String getEcheance() {
        return echeance;
    }

    public void setEcheance(String echeance) {
        this.echeance = echeance;
    }

    public String getDateLimiteAccordee() {
        return dateLimiteAccordee;
    }

    public void setDateLimiteAccordee(String dateLimiteAccordee) {
        this.dateLimiteAccordee = dateLimiteAccordee;
    }

    public String getDateHeureInvitation() {
        return dateHeureInvitation;
    }

    public void setDateHeureInvitation(String dateHeureInvitation) {
        this.dateHeureInvitation = dateHeureInvitation;
    }

    public String getAgentSignateur() {
        return agentSignateur;
    }

    public void setAgentSignateur(String agentSignateur) {
        this.agentSignateur = agentSignateur;
    }

    public String getPeriodeDeclaration() {
        return periodeDeclaration;
    }

    public void setPeriodeDeclaration(String periodeDeclaration) {
        this.periodeDeclaration = periodeDeclaration;
    }

    public String getImpot() {
        return impot;
    }

    public void setImpot(String impot) {
        this.impot = impot;
    }

    public String getLieuImpression() {
        return lieuImpression;
    }

    public void setLieuImpression(String lieuImpression) {
        this.lieuImpression = lieuImpression;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumeroInvitationService() {
        return numeroInvitationService;
    }

    public void setNumeroInvitationService(String numeroInvitationService) {
        this.numeroInvitationService = numeroInvitationService;
    }

    public String getAdresseBureau() {
        return adresseBureau;
    }

    public void setAdresseBureau(String adresseBureau) {
        this.adresseBureau = adresseBureau;
    }
    
    
    
}
