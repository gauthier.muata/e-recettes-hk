/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

/**
 *
 * @author emmanuel.tsasa
 */
public class CpiPrint {

    String numeroCpi;
    String codeComptablePublic;
    String nomsContribuable;
    String adresseContribuable;
    String montantEnLettre;
    String montantEnChiffre;
    String articleTaxes;
    String principal;
    String reference;
    String compteBancaire;
    String numeroPiece;
    String datePaiement;
    String dateImpression;
    String nomAgent;
    String codeQr;
    String url;
    String penalite;
    String detailPaiementCpi;
    String lieuImpression;
    String logo;
    String rubPapier;
    String rubTimbre;

    public String getNumeroCpi() {
        return numeroCpi;
    }

    public void setNumeroCpi(String numeroCpi) {
        this.numeroCpi = numeroCpi;
    }

    public String getCodeComptablePublic() {
        return codeComptablePublic;
    }

    public void setCodeComptablePublic(String codeComptablePublic) {
        this.codeComptablePublic = codeComptablePublic;
    }

    public String getNomsContribuable() {
        return nomsContribuable;
    }

    public void setNomsContribuable(String nomsContribuable) {
        this.nomsContribuable = nomsContribuable;
    }

    public String getLieuImpression() {
        return lieuImpression;
    }

    public void setLieuImpression(String lieuImpression) {
        this.lieuImpression = lieuImpression;
    }

    public String getAdresseContribuable() {
        return adresseContribuable;
    }

    public void setAdresseContribuable(String adresseContribuable) {
        this.adresseContribuable = adresseContribuable;
    }

    public String getMontantEnLettre() {
        return montantEnLettre;
    }

    public void setMontantEnLettre(String montantEnLettre) {
        this.montantEnLettre = montantEnLettre;
    }

    public String getMontantEnChiffre() {
        return montantEnChiffre;
    }

    public void setMontantEnChiffre(String montantEnChiffre) {
        this.montantEnChiffre = montantEnChiffre;
    }

    public String getArticleTaxes() {
        return articleTaxes;
    }

    public void setArticleTaxes(String articleTaxes) {
        this.articleTaxes = articleTaxes;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getNumeroPiece() {
        return numeroPiece;
    }

    public void setNumeroPiece(String numeroPiece) {
        this.numeroPiece = numeroPiece;
    }

    public String getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(String datePaiement) {
        this.datePaiement = datePaiement;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getNomAgent() {
        return nomAgent;
    }

    public void setNomAgent(String nomAgent) {
        this.nomAgent = nomAgent;
    }

    public String getCodeQr() {
        return codeQr;
    }

    public void setCodeQr(String codeQr) {
        this.codeQr = codeQr;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPenalite() {
        return penalite;
    }

    public void setPenalite(String penalite) {
        this.penalite = penalite;
    }

    public String getDetailPaiementCpi() {
        return detailPaiementCpi;
    }

    public void setDetailPaiementCpi(String detailPaiementCpi) {
        this.detailPaiementCpi = detailPaiementCpi;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getRubPapier() {
        return rubPapier;
    }

    public void setRubPapier(String rubPapier) {
        this.rubPapier = rubPapier;
    }

    public String getRubTimbre() {
        return rubTimbre;
    }

    public void setRubTimbre(String rubTimbre) {
        this.rubTimbre = rubTimbre;
    }

}
