/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author gauthier.muata
 */
public class DernierAvertissementPrint implements Serializable {

    String transmisCopie;
    String assujettiName;
    String articleExtraitRole;
    String dateExtraitRole;
    String amountExtraitRole;
    String amountPrincipal;
    String amountPenalite;
    String accountBankPrincipal;
    String accountBankPenalite;
    String dateImpression;
    String userPrint;
    String fonctionUserPrint;
    String codeQr;
    String numeroDernierAvertissement;
    String logo;
    String lieuSite;

    String libelleArticle2;
    String libelleArticle1;
    String codeArticle1;
    String nameMinistrie;
    String nameSector;
    String nameSite;

    public String getTransmisCopie() {
        return transmisCopie;
    }

    public void setTransmisCopie(String transmisCopie) {
        this.transmisCopie = transmisCopie;
    }

    public String getAssujettiName() {
        return assujettiName;
    }

    public void setAssujettiName(String assujettiName) {
        this.assujettiName = assujettiName;
    }

    public String getArticleExtraitRole() {
        return articleExtraitRole;
    }

    public void setArticleExtraitRole(String articleExtraitRole) {
        this.articleExtraitRole = articleExtraitRole;
    }

    public String getDateExtraitRole() {
        return dateExtraitRole;
    }

    public void setDateExtraitRole(String dateExtraitRole) {
        this.dateExtraitRole = dateExtraitRole;
    }

    public String getAmountExtraitRole() {
        return amountExtraitRole;
    }

    public void setAmountExtraitRole(String amountExtraitRole) {
        this.amountExtraitRole = amountExtraitRole;
    }

    public String getAmountPrincipal() {
        return amountPrincipal;
    }

    public void setAmountPrincipal(String amountPrincipal) {
        this.amountPrincipal = amountPrincipal;
    }

    public String getAmountPenalite() {
        return amountPenalite;
    }

    public void setAmountPenalite(String amountPenalite) {
        this.amountPenalite = amountPenalite;
    }

    public String getAccountBankPrincipal() {
        return accountBankPrincipal;
    }

    public void setAccountBankPrincipal(String accountBankPrincipal) {
        this.accountBankPrincipal = accountBankPrincipal;
    }

    public String getAccountBankPenalite() {
        return accountBankPenalite;
    }

    public void setAccountBankPenalite(String accountBankPenalite) {
        this.accountBankPenalite = accountBankPenalite;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getUserPrint() {
        return userPrint;
    }

    public void setUserPrint(String userPrint) {
        this.userPrint = userPrint;
    }

    public String getFonctionUserPrint() {
        return fonctionUserPrint;
    }

    public void setFonctionUserPrint(String fonctionUserPrint) {
        this.fonctionUserPrint = fonctionUserPrint;
    }

    public String getCodeQr() {
        return codeQr;
    }

    public void setCodeQr(String codeQr) {
        this.codeQr = codeQr;
    }

    public String getNumeroDernierAvertissement() {
        return numeroDernierAvertissement;
    }

    public void setNumeroDernierAvertissement(String numeroDernierAvertissement) {
        this.numeroDernierAvertissement = numeroDernierAvertissement;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLieuSite() {
        return lieuSite;
    }

    public void setLieuSite(String lieuSite) {
        this.lieuSite = lieuSite;
    }

    public String getLibelleArticle2() {
        return libelleArticle2;
    }

    public void setLibelleArticle2(String libelleArticle2) {
        this.libelleArticle2 = libelleArticle2;
    }

    public String getLibelleArticle1() {
        return libelleArticle1;
    }

    public void setLibelleArticle1(String libelleArticle1) {
        this.libelleArticle1 = libelleArticle1;
    }

    public String getCodeArticle1() {
        return codeArticle1;
    }

    public void setCodeArticle1(String codeArticle1) {
        this.codeArticle1 = codeArticle1;
    }

    public String getNameMinistrie() {
        return nameMinistrie;
    }

    public void setNameMinistrie(String nameMinistrie) {
        this.nameMinistrie = nameMinistrie;
    }

    public String getNameSector() {
        return nameSector;
    }

    public void setNameSector(String nameSector) {
        this.nameSector = nameSector;
    }

    public String getNameSite() {
        return nameSite;
    }

    public void setNameSite(String nameSite) {
        this.nameSite = nameSite;
    }
    
    

}
