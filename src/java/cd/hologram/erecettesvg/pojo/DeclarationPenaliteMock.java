/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author PC
 */
public class DeclarationPenaliteMock implements Serializable {
    
    String assujCode;
    BigDecimal amountPenalite;
    int tauxRemise;
    String observationRemise;

    public String getAssujCode() {
        return assujCode;
    }

    public void setAssujCode(String assujCode) {
        this.assujCode = assujCode;
    }

    public BigDecimal getAmountPenalite() {
        return amountPenalite;
    }

    public void setAmountPenalite(BigDecimal amountPenalite) {
        this.amountPenalite = amountPenalite;
    }

    public int getTauxRemise() {
        return tauxRemise;
    }

    public void setTauxRemise(int tauxRemise) {
        this.tauxRemise = tauxRemise;
    }

    public String getObservationRemise() {
        return observationRemise;
    }

    public void setObservationRemise(String observationRemise) {
        this.observationRemise = observationRemise;
    }
    
    
    
    
}
