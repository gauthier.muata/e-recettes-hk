/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author PC
 */
public class TicketPeagePrint implements Serializable{
    
    String tarifCode;
    String tarifName;
    int tourUnit;
    int tourTotal;
    BigDecimal amountUnit;
    BigDecimal amountTot;

    public String getTarifCode() {
        return tarifCode;
    }

    public void setTarifCode(String tarifCode) {
        this.tarifCode = tarifCode;
    }

    public String getTarifName() {
        return tarifName;
    }

    public void setTarifName(String tarifName) {
        this.tarifName = tarifName;
    }

    public int getTourUnit() {
        return tourUnit;
    }

    public void setTourUnit(int tourUnit) {
        this.tourUnit = tourUnit;
    }

    public int getTourTotal() {
        return tourTotal;
    }

    public void setTourTotal(int tourTotal) {
        this.tourTotal = tourTotal;
    }

    public BigDecimal getAmountUnit() {
        return amountUnit;
    }

    public void setAmountUnit(BigDecimal amountUnit) {
        this.amountUnit = amountUnit;
    }

    public BigDecimal getAmountTot() {
        return amountTot;
    }

    public void setAmountTot(BigDecimal amountTot) {
        this.amountTot = amountTot;
    }
    
    
    
}
