/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author gauthier.muata
 */
public class DetailPernalite implements Serializable {
    
    BigDecimal interetMoratoire;
    float tauxPenalite;
    String libellePenalite;
    String codePenalite;
    BigDecimal amountPenalite;
    String devisePenalite;
    String typeTauxPenalite;
    String referenceDocument;
    int nbreMoisRetard;

    public BigDecimal getInteretMoratoire() {
        return interetMoratoire;
    }

    public void setInteretMoratoire(BigDecimal interetMoratoire) {
        this.interetMoratoire = interetMoratoire;
    }

    public float getTauxPenalite() {
        return tauxPenalite;
    }

    public void setTauxPenalite(float tauxPenalite) {
        this.tauxPenalite = tauxPenalite;
    }

    public String getLibellePenalite() {
        return libellePenalite;
    }

    public void setLibellePenalite(String libellePenalite) {
        this.libellePenalite = libellePenalite;
    }

    public String getCodePenalite() {
        return codePenalite;
    }

    public void setCodePenalite(String codePenalite) {
        this.codePenalite = codePenalite;
    }

    public BigDecimal getAmountPenalite() {
        return amountPenalite;
    }

    public void setAmountPenalite(BigDecimal amountPenalite) {
        this.amountPenalite = amountPenalite;
    }

    public String getDevisePenalite() {
        return devisePenalite;
    }

    public void setDevisePenalite(String devisePenalite) {
        this.devisePenalite = devisePenalite;
    }

    public String getTypeTauxPenalite() {
        return typeTauxPenalite;
    }

    public void setTypeTauxPenalite(String typeTauxPenalite) {
        this.typeTauxPenalite = typeTauxPenalite;
    }

    public String getReferenceDocument() {
        return referenceDocument;
    }

    public void setReferenceDocument(String referenceDocument) {
        this.referenceDocument = referenceDocument;
    }

    public int getNbreMoisRetard() {
        return nbreMoisRetard;
    }

    public void setNbreMoisRetard(int nbreMoisRetard) {
        this.nbreMoisRetard = nbreMoisRetard;
    }
    
}
