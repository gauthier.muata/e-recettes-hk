/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author gauthier.muata
 */
public class DetailPenaliteMock implements Serializable {

    BigDecimal taux;
    String typeTaux;
    BigDecimal amountPenalite;
    BigDecimal amountPrincipal;
    String codePenalite;
    int numberMonth;
    String reference;
    String exercice;

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public String getTypeTaux() {
        return typeTaux;
    }

    public void setTypeTaux(String typeTaux) {
        this.typeTaux = typeTaux;
    }

    public BigDecimal getAmountPenalite() {
        return amountPenalite;
    }

    public void setAmountPenalite(BigDecimal amountPenalite) {
        this.amountPenalite = amountPenalite;
    }

    public BigDecimal getAmountPrincipal() {
        return amountPrincipal;
    }

    public void setAmountPrincipal(BigDecimal amountPrincipal) {
        this.amountPrincipal = amountPrincipal;
    }

    public String getCodePenalite() {
        return codePenalite;
    }

    public void setCodePenalite(String codePenalite) {
        this.codePenalite = codePenalite;
    }

    public int getNumberMonth() {
        return numberMonth;
    }

    public void setNumberMonth(int numberMonth) {
        this.numberMonth = numberMonth;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getExercice() {
        return exercice;
    }

    public void setExercice(String exercice) {
        this.exercice = exercice;
    }
    
    
}
