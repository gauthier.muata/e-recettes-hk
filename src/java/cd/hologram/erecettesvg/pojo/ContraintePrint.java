/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author gauthier.muata
 */
public class ContraintePrint implements Serializable {

    String logo;
    String codeQR;
    String numImpot;
    String nomAssujetti;
    String adresseAssujetti;
    String telephoneAssujetti;
    String emailAssujetti;
    String dateImpression;

    String datePrintDocument;
    String userPrintDocument;
    String functionUserDocument;

    String numeroContrainte;
    String numeroAmr;
    String dateReceptionAmr;
    String amendeContrainte;
    String agentHuissier;

    String faitGenerateur;
    String montantAmr;
    String taux;
    String penalite;
    String cotePartTresorUrbain;
    String codePartDgrk;
    String fraisPursuite;

    String compteBancaireTresorUrbain;
    String compteBancaireDgrk;
    String compteBancaireDgrk_Bap;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getNumImpot() {
        return numImpot;
    }

    public void setNumImpot(String numImpot) {
        this.numImpot = numImpot;
    }

    public String getNomAssujetti() {
        return nomAssujetti;
    }

    public void setNomAssujetti(String nomAssujetti) {
        this.nomAssujetti = nomAssujetti;
    }

    public String getAdresseAssujetti() {
        return adresseAssujetti;
    }

    public void setAdresseAssujetti(String adresseAssujetti) {
        this.adresseAssujetti = adresseAssujetti;
    }

    public String getTelephoneAssujetti() {
        return telephoneAssujetti;
    }

    public void setTelephoneAssujetti(String telephoneAssujetti) {
        this.telephoneAssujetti = telephoneAssujetti;
    }

    public String getEmailAssujetti() {
        return emailAssujetti;
    }

    public void setEmailAssujetti(String emailAssujetti) {
        this.emailAssujetti = emailAssujetti;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getNumeroContrainte() {
        return numeroContrainte;
    }

    public void setNumeroContrainte(String numeroContrainte) {
        this.numeroContrainte = numeroContrainte;
    }

    public String getNumeroAmr() {
        return numeroAmr;
    }

    public void setNumeroAmr(String numeroAmr) {
        this.numeroAmr = numeroAmr;
    }

    public String getDateReceptionAmr() {
        return dateReceptionAmr;
    }

    public void setDateReceptionAmr(String dateReceptionAmr) {
        this.dateReceptionAmr = dateReceptionAmr;
    }

    public String getAmendeContrainte() {
        return amendeContrainte;
    }

    public void setAmendeContrainte(String amendeContrainte) {
        this.amendeContrainte = amendeContrainte;
    }

    

    public String getAgentHuissier() {
        return agentHuissier;
    }

    public void setAgentHuissier(String agentHuissier) {
        this.agentHuissier = agentHuissier;
    }

    public String getFaitGenerateur() {
        return faitGenerateur;
    }

    public void setFaitGenerateur(String faitGenerateur) {
        this.faitGenerateur = faitGenerateur;
    }

    public String getMontantAmr() {
        return montantAmr;
    }

    public void setMontantAmr(String montantAmr) {
        this.montantAmr = montantAmr;
    }

    public String getTaux() {
        return taux;
    }

    public void setTaux(String taux) {
        this.taux = taux;
    }

    public String getPenalite() {
        return penalite;
    }

    public void setPenalite(String penalite) {
        this.penalite = penalite;
    }

    public String getCotePartTresorUrbain() {
        return cotePartTresorUrbain;
    }

    public void setCotePartTresorUrbain(String cotePartTresorUrbain) {
        this.cotePartTresorUrbain = cotePartTresorUrbain;
    }

    public String getCodePartDgrk() {
        return codePartDgrk;
    }

    public void setCodePartDgrk(String codePartDgrk) {
        this.codePartDgrk = codePartDgrk;
    }

    public String getFraisPursuite() {
        return fraisPursuite;
    }

    public void setFraisPursuite(String fraisPursuite) {
        this.fraisPursuite = fraisPursuite;
    }

    public String getCompteBancaireTresorUrbain() {
        return compteBancaireTresorUrbain;
    }

    public void setCompteBancaireTresorUrbain(String compteBancaireTresorUrbain) {
        this.compteBancaireTresorUrbain = compteBancaireTresorUrbain;
    }

    public String getCompteBancaireDgrk() {
        return compteBancaireDgrk;
    }

    public void setCompteBancaireDgrk(String compteBancaireDgrk) {
        this.compteBancaireDgrk = compteBancaireDgrk;
    }

    public String getDatePrintDocument() {
        return datePrintDocument;
    }

    public void setDatePrintDocument(String datePrintDocument) {
        this.datePrintDocument = datePrintDocument;
    }

    public String getUserPrintDocument() {
        return userPrintDocument;
    }

    public void setUserPrintDocument(String userPrintDocument) {
        this.userPrintDocument = userPrintDocument;
    }

    public String getFunctionUserDocument() {
        return functionUserDocument;
    }

    public void setFunctionUserDocument(String functionUserDocument) {
        this.functionUserDocument = functionUserDocument;
    }

    public String getCompteBancaireDgrk_Bap() {
        return compteBancaireDgrk_Bap;
    }

    public void setCompteBancaireDgrk_Bap(String compteBancaireDgrk_Bap) {
        this.compteBancaireDgrk_Bap = compteBancaireDgrk_Bap;
    }

    

}
