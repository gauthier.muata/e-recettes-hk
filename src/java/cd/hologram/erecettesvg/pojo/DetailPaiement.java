/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

/**
 *
 * @author emmanuel.tsasa
 */
public class DetailPaiement {

    public String rubrique;
    public String reference;
    public String compteBancaire;
    public String numeroPiece;
    public String datePaiement;
    public String montantPayer;
    float montantTotaux;

    public String getRubrique() {
        return rubrique;
    }

    public void setRubrique(String rubrique) {
        this.rubrique = rubrique;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getNumeroPiece() {
        return numeroPiece;
    }

    public void setNumeroPiece(String numeroPiece) {
        this.numeroPiece = numeroPiece;
    }

    public String getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(String datePaiement) {
        this.datePaiement = datePaiement;
    }

    public String getMontantPayer() {
        return montantPayer;
    }

    public void setMontantPayer(String montantPayer) {
        this.montantPayer = montantPayer;
    }

    public float getMontantTotaux() {
        return montantTotaux;
    }

    public void setMontantTotaux(float montantTotaux) {
        this.montantTotaux = montantTotaux;
    }
}
