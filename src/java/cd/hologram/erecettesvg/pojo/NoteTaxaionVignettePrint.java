/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author hp
 */
public class NoteTaxaionVignettePrint implements Serializable {

    String logo;
    String codeQr;
    String numeroNote;
    String infoBanque;
    String nomAgentOrdonnacement;
    String bureauOrdonnacement;
    String dateOrdonnacement;
    String montantEnLettre;
    String montantEnChiffre;
    String bienTaxes;
    String adresseContribuable;
    String nomContribuable;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQr() {
        return codeQr;
    }

    public void setCodeQr(String codeQr) {
        this.codeQr = codeQr;
    }

    public String getNumeroNote() {
        return numeroNote;
    }

    public void setNumeroNote(String numeroNote) {
        this.numeroNote = numeroNote;
    }

    public String getInfoBanque() {
        return infoBanque;
    }

    public void setInfoBanque(String infoBanque) {
        this.infoBanque = infoBanque;
    }

    public String getNomAgentOrdonnacement() {
        return nomAgentOrdonnacement;
    }

    public void setNomAgentOrdonnacement(String nomAgentOrdonnacement) {
        this.nomAgentOrdonnacement = nomAgentOrdonnacement;
    }

    public String getBureauOrdonnacement() {
        return bureauOrdonnacement;
    }

    public void setBureauOrdonnacement(String bureauOrdonnacement) {
        this.bureauOrdonnacement = bureauOrdonnacement;
    }

    public String getDateOrdonnacement() {
        return dateOrdonnacement;
    }

    public void setDateOrdonnacement(String dateOrdonnacement) {
        this.dateOrdonnacement = dateOrdonnacement;
    }

    public String getMontantEnLettre() {
        return montantEnLettre;
    }

    public void setMontantEnLettre(String montantEnLettre) {
        this.montantEnLettre = montantEnLettre;
    }

    public String getMontantEnChiffre() {
        return montantEnChiffre;
    }

    public void setMontantEnChiffre(String montantEnChiffre) {
        this.montantEnChiffre = montantEnChiffre;
    }

    public String getBienTaxes() {
        return bienTaxes;
    }

    public void setBienTaxes(String bienTaxes) {
        this.bienTaxes = bienTaxes;
    }

    public String getAdresseContribuable() {
        return adresseContribuable;
    }

    public void setAdresseContribuable(String adresseContribuable) {
        this.adresseContribuable = adresseContribuable;
    }

    public String getNomContribuable() {
        return nomContribuable;
    }

    public void setNomContribuable(String nomContribuable) {
        this.nomContribuable = nomContribuable;
    }
    
    
    
}
