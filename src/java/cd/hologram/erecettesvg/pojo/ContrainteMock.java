/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class ContrainteMock implements Serializable{
    
    String id;
    String logo;
    String codeQr;
    String lieuPrint;
    String dateImpression;
    String numeroContrainte;
    String contribuable;
    String adresseContribuable;
    String nomHuissier;
    String numeroAmr;
    String acteGenerateur;
    String principal;
    String accompte;
    String solde;
    String periodeDeclaration;
    String dateEcheance;
    String tauxPenalite;
    String cumulPenalite;
    String fraisPoursuite;
    String totalCreanceFiscal;
    String nameReceveur;
    String parquet;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQr() {
        return codeQr;
    }

    public void setCodeQr(String codeQr) {
        this.codeQr = codeQr;
    }

    public String getLieuPrint() {
        return lieuPrint;
    }

    public void setLieuPrint(String lieuPrint) {
        this.lieuPrint = lieuPrint;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getNumeroContrainte() {
        return numeroContrainte;
    }

    public void setNumeroContrainte(String numeroContrainte) {
        this.numeroContrainte = numeroContrainte;
    }

    public String getContribuable() {
        return contribuable;
    }

    public void setContribuable(String contribuable) {
        this.contribuable = contribuable;
    }

    public String getAdresseContribuable() {
        return adresseContribuable;
    }

    public void setAdresseContribuable(String adresseContribuable) {
        this.adresseContribuable = adresseContribuable;
    }

    public String getNomHuissier() {
        return nomHuissier;
    }

    public void setNomHuissier(String nomHuissier) {
        this.nomHuissier = nomHuissier;
    }

    public String getNumeroAmr() {
        return numeroAmr;
    }

    public void setNumeroAmr(String numeroAmr) {
        this.numeroAmr = numeroAmr;
    }

    public String getActeGenerateur() {
        return acteGenerateur;
    }

    public void setActeGenerateur(String acteGenerateur) {
        this.acteGenerateur = acteGenerateur;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getAccompte() {
        return accompte;
    }

    public void setAccompte(String accompte) {
        this.accompte = accompte;
    }

    public String getSolde() {
        return solde;
    }

    public void setSolde(String solde) {
        this.solde = solde;
    }

    public String getPeriodeDeclaration() {
        return periodeDeclaration;
    }

    public void setPeriodeDeclaration(String periodeDeclaration) {
        this.periodeDeclaration = periodeDeclaration;
    }

    public String getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(String dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getTauxPenalite() {
        return tauxPenalite;
    }

    public void setTauxPenalite(String tauxPenalite) {
        this.tauxPenalite = tauxPenalite;
    }

    public String getCumulPenalite() {
        return cumulPenalite;
    }

    public void setCumulPenalite(String cumulPenalite) {
        this.cumulPenalite = cumulPenalite;
    }

    public String getFraisPoursuite() {
        return fraisPoursuite;
    }

    public void setFraisPoursuite(String fraisPoursuite) {
        this.fraisPoursuite = fraisPoursuite;
    }

    public String getTotalCreanceFiscal() {
        return totalCreanceFiscal;
    }

    public void setTotalCreanceFiscal(String totalCreanceFiscal) {
        this.totalCreanceFiscal = totalCreanceFiscal;
    }

    public String getNameReceveur() {
        return nameReceveur;
    }

    public void setNameReceveur(String nameReceveur) {
        this.nameReceveur = nameReceveur;
    }

    public String getParquet() {
        return parquet;
    }

    public void setParquet(String parquet) {
        this.parquet = parquet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
}
