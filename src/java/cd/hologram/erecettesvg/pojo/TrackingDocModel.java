/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author WILLY KASHALA
 */
public class TrackingDocModel implements Serializable{
    
    String code;
    String date_production;
    String type;
    String format;
    String application;
    String col_1;
    String col_2;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDate_production() {
        return date_production;
    }

    public void setDate_production(String date_production) {
        this.date_production = date_production;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getCol_1() {
        return col_1;
    }

    public void setCol_1(String col_1) {
        this.col_1 = col_1;
    }

    public String getCol_2() {
        return col_2;
    }

    public void setCol_2(String col_2) {
        this.col_2 = col_2;
    }
    
}
