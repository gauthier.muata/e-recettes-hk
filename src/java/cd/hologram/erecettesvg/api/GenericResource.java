/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.api;

import cd.hologram.erecettesvg.business.*;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.pojo.BillIssuerAPI;
import cd.hologram.erecettesvg.pojo.TMBPayBillRequest;
import cd.hologram.erecettesvg.util.*;
import com.google.gson.JsonObject;
import entites.RemiseSousProvisionPeage;
import java.math.BigDecimal;

import java.util.*;
import java.util.logging.*;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.xml.bind.annotation.XmlElementRef;

import org.json.*;

/**
 * REST Web Service
 *
 * @author emmanuel.tsasa
 */
@Path("")
public class GenericResource {

    @Context
    private UriInfo context;

    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
    }

    /**
     * Retrieves representation of an instance of
     * cd.hologram.erecettesvg.api.GenericResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes("application/xml")
    public void putXml(String content) {
    }

    @POST
    @XmlElementRef
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/xml;chartset=UTF-8")
    @Path("v1/pepele/payBill")
    @SuppressWarnings("CallToThreadDumpStack")
    public Object checkAndPay(@FormParam("data") String data, @Context HttpHeaders headers) {

        BillIssuerAPI BillIssuerAPI = new BillIssuerAPI();

        if (data == null || data.isEmpty()) {
            System.out.println("CheckAndPay: Aucune information fournie");
            BillIssuerAPI.setStatus("KO");
            BillIssuerAPI.setInfo("Aucune information de paiement fournie");
            return BillIssuerAPI;
        }

        try {

            TMBPayBillRequest payBillRequest = Tools.getPayBillRequestFromXML(data, false);

            String operation = payBillRequest.getType();

            TicketPeage ticketPeage = AssujettissementBusiness.getTicketByCode(payBillRequest.getBillRef());

            Commande commande = AssujettissementBusiness.getCommandeByRef(payBillRequest.getBillRef());

            if (ticketPeage == null && commande == null) {

                BillIssuerAPI.setStatus("KO");
                BillIssuerAPI.setInfo("La numéro de la facture est introuvable");
                return BillIssuerAPI;
            }

            boolean isTicket;

            isTicket = ticketPeage != null;

            if (isTicket) {

                switch (operation) {
                    case "PREAUTH":
                        BillIssuerAPI.setStatus("OK");
                        BigDecimal amount = ticketPeage.getMontant();
                        amount = ticketPeage.getDevise().getCode().equals("USD") ? amount.multiply(BigDecimal.valueOf(100)) : amount;
                        String currency = ticketPeage.getDevise().getCode().equals("USD") ? "840" : "976";
                        BillIssuerAPI.setAmount(amount);
                        BillIssuerAPI.setCurrency(currency);
                        if (ticketPeage.getEtat().equals(1)) {
                            BillIssuerAPI.setInfo("Ce numéro du ticket est déjà payé");
                        }
                        break;
                    case "TRANSACTION":
                        if (ticketPeage.getEtat().equals(2)) {

                            boolean isUpdated = AssujettissementBusiness.updateTicketByCode(ticketPeage.getCode());

                            if (isUpdated) {

                                BillIssuerAPI.setStatus("OK");
                                BillIssuerAPI.setInfo("Le paiement du ticket " + ticketPeage.getCode() + " a été payé avec succès");
                                BillIssuerAPI.setTransactionid(ticketPeage.getCode());

                            } else {

                                BillIssuerAPI.setStatus("KO");
                                BillIssuerAPI.setInfo("L'opération n'a pas abouti, veuillez contacter l'administrateur");
                            }

                        } else {

                            BillIssuerAPI.setStatus("KO");
                            BillIssuerAPI.setInfo("Ce numéro du ticket est déjà payé");
                        }
                        break;

                    default:
                        BillIssuerAPI.setStatus("KO");
                        BillIssuerAPI.setInfo("Opération non reconnu");
                        break;
                }

            } else {

                switch (operation) {
                    case "PREAUTH":
                        BillIssuerAPI.setStatus("OK");
                        BigDecimal amount = commande.getMontant();
                        amount = commande.getDevise().getCode().equals("USD") ? amount.multiply(BigDecimal.valueOf(100)) : amount;
                        String currency = commande.getDevise().getCode().equals("USD") ? "840" : "976";
                        BillIssuerAPI.setAmount(amount);
                        BillIssuerAPI.setCurrency(currency);
                        if (commande.getEtat().equals(1)) {
                            BillIssuerAPI.setInfo("Cette commande est déjà payé");
                        }
                        break;
                    case "TRANSACTION":
                        if (commande.getEtat().equals(2) || commande.getEtat().equals(3)) {

                            boolean isUpdated = AssujettissementBusiness.updateCommandeCode(commande.getId());

                            if (isUpdated) {

                                BillIssuerAPI.setStatus("OK");
                                BillIssuerAPI.setInfo("Le paiement de la commande " + commande.getReference() + " a été payé avec succès");
                                BillIssuerAPI.setTransactionid(commande.getReference());

                            } else {

                                BillIssuerAPI.setStatus("KO");
                                BillIssuerAPI.setInfo("L'opération n'a pas abouti, veuillez contacter l'administrateur");
                            }

                        } else {

                            BillIssuerAPI.setStatus("KO");
                            BillIssuerAPI.setInfo("Ce numéro de commande est déjà payé");
                        }
                        break;

                    default:
                        BillIssuerAPI.setStatus("KO");
                        BillIssuerAPI.setInfo("Opération non reconnu");
                        break;
                }
            }

        } catch (Exception e) {
            System.out.println("execption : " + e.getMessage());
        }

        return BillIssuerAPI;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("verifyvoucher/{code}/{site}")
    public Object verifyvoucher(@PathParam("code") String code, @PathParam("site") String site, @PathParam("tarif") String tarif) throws JSONException {

        JsonObject response = new JsonObject();

        try {

            DetailVoucher detailVoucher = AssujettissementBusiness.getVoucherByCode(code);

            Voucher voucher = null;

            if (detailVoucher != null) {

                if (detailVoucher.getDateUtilisation() != null) {

                    response.addProperty("status", "200");
                    response.addProperty("message", "Voucher utilisé");

                } else {

                    voucher = detailVoucher.getFkVaucher();

                    AxePeage axePeage = PeageBusiness.getAxeByPeage(site.trim());

                    if (axePeage != null) {

                        response.addProperty("status", "100");
                        response.addProperty("message", "Succes");

                        response.addProperty("codePersonne", voucher.getFkCommande().getFkPersonne().getCode());
                        response.addProperty("namePersonne", voucher.getFkCommande().getFkPersonne().toString().toUpperCase());

                        RemiseSousProvisionPeage remiseSousProvisionPeage = PeageBusiness.getRemiseSousProvisionPeage(
                                voucher.getFkCommande().getFkPersonne().getCode(),
                                voucher.getFkTarif().getCode(), voucher.getFkAxe().getId());

                        if (remiseSousProvisionPeage != null) {
                            response.addProperty("montant", remiseSousProvisionPeage.getTaux());
                            response.addProperty("typePaiement", "SOUS-PROVISION");

                        } else {

                            TarifSite tarifSite = PeageBusiness.getTarifSiteBySiteAndTarif(
                                    voucher.getFkSite().getCode(), voucher.getFkTarif().getCode());

                            if (tarifSite != null) {
                                response.addProperty("montant", tarifSite.getTaux());
                            } else {
                                response.addProperty("montant", 1);
                            }

                            response.addProperty("typePaiement", "CREDIT");
                        }

                        response.addProperty("devise", voucher.getDevise().getCode());
                        response.addProperty("codeTarif", voucher.getFkTarif().getCode());
                        response.addProperty("nameTarif", voucher.getFkTarif().getIntitule().toUpperCase());
                        response.addProperty("detailVoucherId", detailVoucher.getId());
                        response.addProperty("trajet", voucher.getFkAxe().getIntitule().toUpperCase());
                        response.addProperty("trajetId", voucher.getFkAxe().getId());

                    } else {
                        response.addProperty("status", "300");
                        response.addProperty("message", "Voucher invalid");
                    }
                }

            } else {

                response.addProperty("status", "400");
                response.addProperty("message", "Voucher invalid");
            }

        } catch (Exception e) {
            response.addProperty("status", "500");
            response.addProperty("message", "Une erreur est survenue!");
        }

        return response.toString();
    }

    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("verifyvouchercard/{cardnumber}/{site}/{tarif}")
    public Object verifyvouchercard(@PathParam("cardnumber") String cardnumber, @PathParam("site") String site, @PathParam("tarif") String tarif) throws JSONException {

        JsonObject response = new JsonObject();

        try {

            CarteDetailVoucher carteDetailVoucher = AssujettissementBusiness.getCarteDetailVoucherByNumberCard(cardnumber);

            if (carteDetailVoucher != null) {

                Voucher voucher = carteDetailVoucher.getFkDetailVoucher().getFkVaucher();

                if (!voucher.getFkSite().getCode().equals(site)) {

                    response.addProperty("status", "200");
                    response.addProperty("message", "Site invalid");

                } else if (!voucher.getFkTarif().getCode().equals(tarif)) {

                    response.addProperty("status", "300");
                    response.addProperty("message", "Tarif invalid");

                } else {

                    AxePeage axePeage = PeageBusiness.getAxeByPeage(site.trim());

                    if (axePeage != null) {

                        response.addProperty("status", "100");
                        response.addProperty("message", "succes");

                        response.addProperty("codePersonne", voucher.getFkCommande().getFkPersonne().getCode());
                        response.addProperty("namePersonne", voucher.getFkCommande().getFkPersonne().toString().toUpperCase());

                        RemiseSousProvisionPeage remiseSousProvisionPeage = PeageBusiness.getRemiseSousProvisionPeage(
                                voucher.getFkCommande().getFkPersonne().getCode(),
                                voucher.getFkTarif().getCode(), voucher.getFkAxe().getId());

                        if (remiseSousProvisionPeage != null) {
                            response.addProperty("montant", remiseSousProvisionPeage.getTaux());
                            response.addProperty("typePaiement", "SOUS-PROVISION");

                        } else {

                            TarifSite tarifSite = PeageBusiness.getTarifSiteBySiteAndTarif(
                                    voucher.getFkSite().getCode(), voucher.getFkTarif().getCode());

                            if (tarifSite != null) {
                                response.addProperty("montant", tarifSite.getTaux());
                            } else {
                                response.addProperty("montant", 1);
                            }

                            response.addProperty("typePaiement", "CREDIT");
                        }

                        response.addProperty("devise", voucher.getDevise().getCode());
                        response.addProperty("codeTarif", voucher.getFkTarif().getCode());
                        response.addProperty("nameTarif", voucher.getFkTarif().getIntitule().toUpperCase());
                        response.addProperty("detailVoucherId", carteDetailVoucher.getFkDetailVoucher().getId());
                        response.addProperty("trajet", voucher.getFkAxe().getIntitule().toUpperCase());
                        response.addProperty("trajetId", voucher.getFkAxe().getId());

                    } else {
                        response.addProperty("status", "400");
                        response.addProperty("message", "Axe invalid");
                    }
                }

            } else {

                response.addProperty("status", "500");
                response.addProperty("message", "Number card invalid");
            }

        } catch (Exception e) {
            response.addProperty("status", "600");
            response.addProperty("message", "Une erreur est survenue!");
        }

        return response.toString();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("login/{login}/{password}")
    public Object login(@PathParam("login") String login, @PathParam("password") String password) throws JSONException {

        System.out.println("Tentative de connexion de l'utilisateur " + login + "...");
        JSONObject data = new JSONObject();
        try {

            Agent agent = ConnexionBusiness.authentification(login, password);

            if (agent == null) {

                data.put("status", "210");
                data.put("message", "Utilisateur ou mot de passe incorrect");
                data.put("data", "");

            } else {

                List<Devise> deviseList = TaxationBusiness.getListAllDevises();

                if (!deviseList.isEmpty()) {
                    List<JSONObject> dataDeviseList = new ArrayList<>();
                    for (Devise devise : deviseList) {
                        JSONObject dataDevise = new JSONObject();
                        dataDevise.put("code", devise.getCode());
                        dataDevise.put("intitule", devise.getIntitule());
                        dataDevise.put("parDefaut", devise.getParDefaut().intValue());

                        List<JSONObject> dataTauxList = new ArrayList<>();
                        for (Devise deviseDest : deviseList) {
                            Taux taux = TaxationBusiness.getTauxByDevise(devise.getCode(), deviseDest.getCode());
                            if (taux != null) {
                                JSONObject dataTaux = new JSONObject();
                                dataTaux.put("devDest", taux.getDeviseDest());
                                dataTaux.put("taux", taux.getTaux());
                                dataTaux.put("date", ConvertDate.formatDateToString(taux.getDateCreat()));
                                dataTauxList.add(dataTaux);
                            }
                        }
                        dataDevise.put("tauxList", dataTauxList);
                        dataDeviseList.add(dataDevise);
                    }

                    String siteCode = agent.getSite() == null ? "" : agent.getSite().getCode();

                    SitePeageUser sitePeageUser = GestionUtilisateurBusiness.getSitePeageUserByUser(agent.getCode());

                    List<JSONObject> dataAccesUserList = new ArrayList<>();
                    List<AccesUser> accesUsers = GestionUtilisateurBusiness.getAccesUserPeageByUser(agent.getCode());
                    if (!accesUsers.isEmpty()) {
                        for (AccesUser accesUser : accesUsers) {
                            JSONObject dataAU = new JSONObject();
                            dataAU.put("code", accesUser.getDroit());
                            dataAccesUserList.add(dataAU);
                        }
                    }

                    String codeSitePeage = GeneralConst.EMPTY_STRING;

                    if (sitePeageUser != null) {
                        codeSitePeage = sitePeageUser.getFkSitePeage().trim();
                    } else {
                        codeSitePeage = siteCode;
                    }

                    List<JSONObject> dataTarifList = getSpecifiquesTarifs(siteCode, codeSitePeage);

                    JSONObject dataAgent = new JSONObject();
                    dataAgent.put("code", agent.getCode());
                    dataAgent.put("nom", agent.toString());
                    dataAgent.put("fonction", agent.getFonction() == null ? "" : agent.getFonction().getIntitule());
                    dataAgent.put("grade", agent.getGrade());
                    dataAgent.put("service", agent.getService() == null ? "" : agent.getService().getIntitule());
                    dataAgent.put("siteCode", siteCode);
                    dataAgent.put("site", agent.getSite() == null ? "" : agent.getSite().getIntitule());
                    dataAgent.put("tarifs", dataTarifList);
                    dataAgent.put("devises", dataDeviseList);
                    dataAgent.put("droits", dataAccesUserList.isEmpty() ? "" : dataAccesUserList);

                    data.put("status", "100");
                    data.put("message", "Authentification réussie.");
                    data.put("dataAgent", dataAgent);

                    String sqlQuery = "UPDATE T_AGENT SET CONNECTE = 1, DATE_DERNIERE_CONNEXION = GETDATE() WHERE CODE = %s";
                    sqlQuery = String.format(sqlQuery, agent.getCode());

                    TaxationBusiness.updateEcheanePayment(sqlQuery);

                } else {

                    data.put("status", "220");
                    data.put("message", "Aucune configuration de taux");
                    data.put("data", "");

                }
            }

        } catch (JSONException ex) {
            data.put("status", "200");
            data.put("message", "Une erreur est survenue");
            data.put("data", "");
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data.toString();

    }

    @POST
    @Path("changePassword")
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Produces({MediaType.APPLICATION_JSON + ";charset=UTF-8"})
    public Object updatePassword(@FormParam("userId") String userId, @FormParam("newPassword") String password) {
        JSONObject response = new JSONObject();
        try {
            boolean passwordChanged = ConnexionBusiness.updatePassword(userId, password);
            if (passwordChanged) {
                response.put("status", "100");
                response.put("message", "Le mot de passe a été modifié avec succès!");
            } else {
                response.put("status", "210");
                response.put("message", "L'opération a échoué!");
            }
        } catch (JSONException e) {
        }
        return response.toString();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("bienbyimm/{imm}")
    public Object getBienByImm(@PathParam("imm") String imm) throws JSONException {

        JSONObject data = new JSONObject();

        try {

            Bien bien = AssujettissementBusiness.getBienByImmatriculation(imm);

            if (bien == null) {

                data.put("status", "210");
                data.put("message", "Il n'y a aucun bien avec cette plaque ou numéro chassis.");
                data.put("data", "");

            } else {

                DetailAssujettissement da = AssujettissementBusiness.getDetailAssujettissementByIdBien(bien.getId());

                if (da == null) {

                    data.put("status", "220");
                    data.put("message", "Aucun tarif n'est configuré pour ce bien.");
                    data.put("data", "");

                } else {

                    Personne personne = IdentificationBusiness.getPersonneByCode(da.getFkPersonne().trim());

                    Personne otherPerson;

                    DetailsPrevisionCredit detailsPrevisionCredit = AssujettissementBusiness.getDetailsPrevisionCreditByAssujettissementID(
                            da.getId());

                    JSONObject detailPersonne = new JSONObject();
                    JSONObject detailPersonneWithCashMode = null;
                    JSONObject detailTaxe = new JSONObject();
                    JSONObject detailData = new JSONObject();
                    JSONObject detailBien = new JSONObject();
                    JSONObject detailTarif = new JSONObject();

                    if (detailsPrevisionCredit == null) {

                        detailsPrevisionCredit = AssujettissementBusiness.getDetailsPrevisionCreditByBien(
                                da.getBien().getId());

                        if (detailsPrevisionCredit != null) {

                            PrevisionCredit pc = AssujettissementBusiness.getAPrevisionCreditByID(detailsPrevisionCredit.getFkPrevisionCredit());

                            if (pc != null) {

                                if (!personne.getCode().equals(pc.getFkPersonne())) {

                                    detailPersonneWithCashMode = new JSONObject();

                                    detailPersonneWithCashMode.put("codePersonne", personne == null ? "" : personne.getCode());
                                    detailPersonneWithCashMode.put("nomsPersonne", personne == null ? "" : personne.toString().toUpperCase().trim().concat(GeneralConst.TWO_POINTS).concat("(CASH)"));
                                    detailPersonneWithCashMode.put("type", 3);
                                    detailPersonneWithCashMode.put("bienId", bien.getId());

                                }
                            }
                        }
                    }

                    if (detailsPrevisionCredit != null) {

                        List<DetailsPrevisionCredit> listDetailsPrevisionCredits = AssujettissementBusiness.getDetailsPrevisionCreditActifByBien(
                                bien.getId().trim());

                        if (!listDetailsPrevisionCredits.isEmpty()) {

                            List<JSONObject> detailPersonneList = new ArrayList<>();

                            for (DetailsPrevisionCredit dpc : listDetailsPrevisionCredits) {

                                detailPersonne = new JSONObject();

                                personne = AssujettissementBusiness.getPersonneByProvisionCredit(dpc.getFkPrevisionCredit().trim());

                                String type = GeneralConst.EMPTY_STRING;

                                if (dpc.getType() == GeneralConst.Numeric.ONE) {
                                    type = "(".concat("PRE-PAIEMENT").concat(")");
                                } else if (dpc.getType() == GeneralConst.Numeric.TWO) {
                                    type = "(".concat("CREDIT").concat(")");
                                }

                                detailPersonne.put("codePersonne", personne == null ? "" : personne.getCode());
                                detailPersonne.put("nomsPersonne", personne == null ? "" : personne.toString().toUpperCase().trim().concat(GeneralConst.TWO_POINTS).concat(type));
                                detailPersonne.put("type", dpc.getType());
                                detailPersonne.put("bienId", dpc.getFkBien());

                                detailPersonneList.add(detailPersonne);
                            }

                            if (detailPersonneWithCashMode != null) {
                                detailPersonneList.add(detailPersonneWithCashMode);
                            }

                            detailData.put("personne", detailPersonneList);
                            detailData.put("taxe", "");
                            detailData.put("case", 2);
                            detailData.put("dataPC", "");
                            detailData.put("bien", "");
                            detailData.put("tarif", "");

                            data.put("status", "100");
                            data.put("message", "success");
                            data.put("data", detailData);
                        }

                    } else {

                        switch (da.getEtat()) {

                            case GeneralConst.Numeric.ONE:

                                detailPersonne.put("codePersonne", personne == null ? "" : personne.getCode());
                                detailPersonne.put("nomPersonne", personne == null ? "" : personne.getNom() == null ? "" : personne.getNom());
                                detailPersonne.put("postnomPersonne", personne == null ? "" : personne.getPostnom() == null ? "" : personne.getPostnom());
                                detailPersonne.put("prenomPersonne", personne == null ? "" : personne.getPrenoms() == null ? "" : personne.getPrenoms());
                                detailPersonne.put("exempte", 0);

                                break;

                            case GeneralConst.Numeric.TWO:

                                detailPersonne.put("codePersonne", personne == null ? "" : personne.getCode());
                                detailPersonne.put("nomPersonne", personne == null ? "" : personne.getNom() == null ? "" : personne.getNom());
                                detailPersonne.put("postnomPersonne", personne == null ? "" : personne.getPostnom() == null ? "" : personne.getPostnom());
                                detailPersonne.put("prenomPersonne", personne == null ? "" : personne.getPrenoms() == null ? "" : personne.getPrenoms());
                                detailPersonne.put("exempte", 1);
                                break;
                        }

                        Assujettissement assuj = da.getAssujettissement();
                        ArticleBudgetaire ab = TaxationBusiness.getArticleBudgetaireByCode(assuj.getArticleBudgetaire());

                        detailTaxe.put("codeTaxe", ab.getCode());
                        detailTaxe.put("intituleTaxe", ab.getIntitule());

                        detailData.put("personne", detailPersonne);
                        detailData.put("taxe", detailTaxe);
                        detailData.put("case", 1);

                        detailData.put("dataPC", "");

                        bien = da.getBien();
                        String intitule = bien == null ? "" : bien.getIntitule();
                        String description = bien == null ? "" : bien.getDescription();

                        detailBien.put("idBien", bien == null ? "" : bien.getId());
                        detailBien.put("intituleBien", intitule);
                        detailBien.put("descriptionBien", description);
                        detailBien.put("inPC", 0);
                        detailBien.put("hasPC", 0);

                        detailTarif.put("taux", da.getTaux());
                        detailTarif.put("devise", da.getDevise() == null ? "" : da.getDevise().getCode());
                        detailTarif.put("codeTarif", da.getTarif().getCode());
                        detailTarif.put("intituleTarif", da.getTarif().getIntitule());

                        detailData.put("bien", detailBien);
                        detailData.put("tarif", detailTarif);

                        data.put("status", "100");
                        data.put("message", (description == null) ? intitule : intitule + " (" + description + ")");
                        data.put("data", detailData);
                    }
                }
            }

        } catch (JSONException ex) {
            data.put("status", "200");
            data.put("message", "Une erreur est survenue.");
            data.put("data", "");
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return data.toString();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("bienbyimmV2/{personneCode}/{bienCode}/{type}")
    public Object getBienByImmV2(
            @PathParam("personneCode") String personneCode,
            @PathParam("bienCode") String bienCode,
            @PathParam("type") String type) throws JSONException {

        JSONObject data = new JSONObject();

        String intitule = GeneralConst.EMPTY_STRING;
        String description = GeneralConst.EMPTY_STRING;

        try {

            Bien bien = AssujettissementBusiness.getBienByCode(bienCode);

            if (bien == null) {

                data.put("status", "210");
                data.put("message", "Il n'y a aucun bien avec code.");
                data.put("data", "");

            } else {

                Personne personne = IdentificationBusiness.getPersonneByCode(personneCode.trim());

                Assujettissement assuj;
                ArticleBudgetaire ab;

                JSONObject detailPersonne = new JSONObject();
                JSONObject detailTaxe = new JSONObject();
                JSONObject detailData = new JSONObject();
                JSONObject detailBien = new JSONObject();
                JSONObject detailTarif = new JSONObject();

                switch (type) {

                    case "3":

                        detailPersonne.put("codePersonne", personne == null ? "" : personne.getCode());
                        detailPersonne.put("nomPersonne", personne == null ? "" : personne.getNom() == null ? "" : personne.getNom());
                        detailPersonne.put("postnomPersonne", personne == null ? "" : personne.getPostnom() == null ? "" : personne.getPostnom());
                        detailPersonne.put("prenomPersonne", personne == null ? "" : personne.getPrenoms() == null ? "" : personne.getPrenoms());
                        detailPersonne.put("exempte", 0);

                        DetailAssujettissement detAssuj = AssujettissementBusiness.getDetailAssujettissementByPersonneAndBien(
                                personneCode, bienCode);

                        assuj = detAssuj.getAssujettissement();
                        ab = TaxationBusiness.getArticleBudgetaireByCode(assuj.getArticleBudgetaire());

                        detailTaxe.put("codeTaxe", ab.getCode());
                        detailTaxe.put("intituleTaxe", ab.getIntitule());

                        detailData.put("personne", detailPersonne);
                        detailData.put("taxe", detailTaxe);
                        detailData.put("case", 1);

                        detailData.put("dataPC", "");

                        intitule = bien == null ? "" : bien.getIntitule();
                        description = bien == null ? "" : bien.getDescription();

                        detailBien.put("idBien", bien == null ? "" : bien.getId());
                        detailBien.put("intituleBien", intitule);
                        detailBien.put("descriptionBien", description);
                        detailBien.put("inPC", 0);
                        detailBien.put("hasPC", 0);

                        detailTarif.put("taux", detAssuj.getTaux());
                        detailTarif.put("devise", detAssuj.getDevise() == null ? "" : detAssuj.getDevise().getCode());
                        detailTarif.put("codeTarif", detAssuj.getTarif().getCode());
                        detailTarif.put("intituleTarif", detAssuj.getTarif().getIntitule());

                        detailData.put("bien", detailBien);
                        detailData.put("tarif", detailTarif);

                        data.put("status", "100");
                        data.put("message", (description == null) ? intitule : intitule + " (" + description + ")");
                        data.put("data", detailData);

                        break;
                    default:
                        DetailsPrevisionCredit dpc = AssujettissementBusiness.getDetailsPrevisionCreditByPersonneAndBienAndType(
                                personneCode, bienCode, Integer.valueOf(type));

                        if (dpc != null) {

                            detailPersonne.put("codePersonne", personne == null ? "" : personne.getCode());
                            detailPersonne.put("nomPersonne", personne == null ? "" : personne.getNom() == null ? "" : personne.getNom());
                            detailPersonne.put("postnomPersonne", personne == null ? "" : personne.getPostnom() == null ? "" : personne.getPostnom());
                            detailPersonne.put("prenomPersonne", personne == null ? "" : personne.getPrenoms() == null ? "" : personne.getPrenoms());
                            detailPersonne.put("exempte", 2);

                            assuj = AssujettissementBusiness.getAssujettissementByID(dpc.getFkDetailAssujettissement());
                            ab = TaxationBusiness.getArticleBudgetaireByCode(assuj.getArticleBudgetaire());

                            DetailAssujettissement da = AssujettissementBusiness.getDetailAssujettissementByID(dpc.getFkDetailAssujettissement());

                            detailTaxe.put("codeTaxe", ab.getCode());
                            detailTaxe.put("intituleTaxe", ab.getIntitule());

                            detailData.put("personne", detailPersonne);
                            detailData.put("taxe", detailTaxe);

                            JSONObject dataPV = new JSONObject();

                            dataPV.put("code", dpc.getId());
                            dataPV.put("type", dpc.getType() == 1 ? "PRE-PAIEMENT" : "CREDIT");
                            dataPV.put("montantTotal", dpc.getTotalTaux() == null ? 0 : dpc.getTotalTaux());
                            dataPV.put("devise", dpc.getFkDevise() == null ? "" : dpc.getFkDevise());
                            dataPV.put("nombreTourInitial", dpc.getNbreTourSouscrit() == null ? 0 : dpc.getNbreTourSouscrit());
                            dataPV.put("nombreTourReel", dpc.getNbreTourUtilise() == null ? 0 : dpc.getNbreTourUtilise());
                            detailData.put("dataPC", dataPV);

                            intitule = bien == null ? "" : bien.getIntitule();
                            description = bien == null ? "" : bien.getDescription();

                            detailBien.put("idBien", bien == null ? "" : bien.getId());
                            detailBien.put("intituleBien", intitule);
                            detailBien.put("descriptionBien", description);
                            detailBien.put("inPC", 0);
                            detailBien.put("hasPC", 0);

                            detailTarif.put("taux", dpc.getTaux());
                            detailTarif.put("devise", dpc.getFkDevise() == null ? "" : dpc.getFkDevise());
                            detailTarif.put("codeTarif", da.getTarif().getCode());
                            detailTarif.put("intituleTarif", da.getTarif().getIntitule());

                            detailData.put("bien", detailBien);
                            detailData.put("tarif", detailTarif);

                            data.put("status", "100");
                            data.put("message", (description == null) ? intitule : intitule + " (" + description + ")");
                            data.put("data", detailData);

                        }
                        break;
                }

            }

        } catch (JSONException ex) {
            data.put("status", "200");
            data.put("message", "Une erreur est survenue.");
            data.put("data", "");
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return data.toString();
    }

    @POST
    @Path("synctickets")
    @Produces({MediaType.APPLICATION_FORM_URLENCODED})
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    public String syncTickets(@FormParam("ticketsData") String ticketsData) {

        boolean result = false;

        JsonObject response = new JsonObject();

        try {

            result = AssujettissementBusiness.saveTicketsPeage(ticketsData);

            if (result) {
                response.addProperty("status", "100");
                response.addProperty("message", "Synchronisation effectuée avec succès!");
            } else {
                response.addProperty("status", "200");
                response.addProperty("message", "Synchronisation echouée!");
            }

        } catch (Exception e) {
            response.addProperty("status", "210");
            response.addProperty("message", "Une erreur est survenue!");
        }

        return response.toString();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("refreshparam/{code}")
    public Object refreshParam(@PathParam("code") String code) throws JSONException {

        JSONObject data = new JSONObject();
        try {

            Agent agent = ConnexionBusiness.getAgentByCode(code);

            if (agent == null) {

                data.put("status", "210");
                data.put("message", "Problème d'authentification");
                data.put("data", "");

            } else {

                List<Devise> deviseList = TaxationBusiness.getListAllDevises();

                if (!deviseList.isEmpty()) {

                    List<JSONObject> dataDeviseList = new ArrayList<>();

                    for (Devise devise : deviseList) {
                        JSONObject dataDevise = new JSONObject();
                        dataDevise.put("code", devise.getCode());
                        dataDevise.put("intitule", devise.getIntitule());
                        dataDevise.put("parDefaut", devise.getParDefaut().intValue());

                        List<JSONObject> dataTauxList = new ArrayList<>();
                        for (Devise deviseDest : deviseList) {
                            Taux taux = TaxationBusiness.getTauxByDevise(devise.getCode(), deviseDest.getCode());
                            if (taux != null) {
                                JSONObject dataTaux = new JSONObject();
                                dataTaux.put("devDest", taux.getDeviseDest());
                                dataTaux.put("taux", taux.getTaux());
                                dataTaux.put("date", ConvertDate.formatDateToString(taux.getDateCreat()));
                                dataTauxList.add(dataTaux);
                            }
                        }
                        dataDevise.put("tauxList", dataTauxList);
                        dataDeviseList.add(dataDevise);
                    }

                    String siteCode = agent.getSite() == null ? "" : agent.getSite().getCode();

                    SitePeageUser sitePeageUser = GestionUtilisateurBusiness.getSitePeageUserByUser(agent.getCode());

                    List<JSONObject> dataAccesUserList = new ArrayList<>();
                    List<AccesUser> accesUsers = GestionUtilisateurBusiness.getAccesUserPeageByUser(agent.getCode());
                    if (!accesUsers.isEmpty()) {
                        for (AccesUser accesUser : accesUsers) {
                            JSONObject dataAU = new JSONObject();
                            dataAU.put("code", accesUser.getDroit());
                            dataAccesUserList.add(dataAU);
                        }
                    }

                    String codeSitePeage = GeneralConst.EMPTY_STRING;

                    if (sitePeageUser != null) {
                        codeSitePeage = sitePeageUser.getFkSitePeage().trim();

                    }

                    List<JSONObject> dataTarifList = getSpecifiquesTarifs(siteCode, codeSitePeage);

                    JSONObject dataAgent = new JSONObject();
                    dataAgent.put("code", agent.getCode());
                    dataAgent.put("nom", agent.toString());
                    dataAgent.put("fonction", agent.getFonction() == null ? "" : agent.getFonction().getIntitule());
                    dataAgent.put("grade", agent.getGrade());
                    dataAgent.put("service", agent.getService() == null ? "" : agent.getService().getIntitule());
                    dataAgent.put("siteCode", siteCode);
                    dataAgent.put("site", agent.getSite() == null ? "" : agent.getSite().getIntitule());
                    dataAgent.put("tarifs", dataTarifList);
                    dataAgent.put("devises", dataDeviseList);
                    dataAgent.put("droits", dataAccesUserList);

                    data.put("status", "100");
                    data.put("message", "Authentification réussie.");
                    data.put("dataAgent", dataAgent);

                } else {

                    data.put("status", "220");
                    data.put("message", "Aucune configuration de taux");
                    data.put("data", "");

                }
            }

        } catch (JSONException ex) {
            data.put("status", "200");
            data.put("message", "Une erreur est survenue");
            data.put("data", "");
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data.toString();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("ticket/find/{code}/{site}/{agent}")
    public Object getTicketByCodeAndSite(@PathParam("code") String code, @PathParam("site") String site, @PathParam("agent") String agent) throws JSONException {

        JsonObject response = new JsonObject();

        try {

            TicketPeage ticketPeage = AssujettissementBusiness.getTicketPeageByCodeAndSite(code, site);

            if (ticketPeage != null) {

                Tarif tarif = TaxationBusiness.getTarifByCode(ticketPeage.getFkTarif());

                response.addProperty("status", "100");
                response.addProperty("message", "Succes");

                response.addProperty("code", ticketPeage.getCode().toUpperCase());
                response.addProperty("namePersonne", "");
                response.addProperty("montant", ticketPeage.getMontant());
                response.addProperty("devise", ticketPeage.getDevise().getCode().toUpperCase());

                response.addProperty("codeTarif", ticketPeage.getFkTarif());
                response.addProperty("nameTarif", tarif.getIntitule().toUpperCase());

                response.addProperty("codeSite", ticketPeage.getSite().getCode());
                response.addProperty("nameSite", ticketPeage.getSite().getIntitule().toUpperCase());

                response.addProperty("etat", ticketPeage.getEtat());
                response.addProperty("dateTicket", ConvertDate.formatDateHeureToString(ticketPeage.getDateProd()));

                response.addProperty("immatriculation", ticketPeage.getPlaqueChassis().toUpperCase());
                response.addProperty("typePaiement", ticketPeage.getTypePaiement().toUpperCase());

                AssujettissementBusiness.saveLogScanTicketV2(response.toString(), site, Integer.valueOf(agent));

            } else {

                response.addProperty("status", "400");
                response.addProperty("message", "Ticket invalid");
            }

        } catch (Exception e) {
            response.addProperty("status", "500");
            response.addProperty("message", "Une erreur est survenue!");
        }

        return response.toString();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("ticket/find/{code}/{site}")
    public Object getTicketByCodeAndSite(@PathParam("code") String code, @PathParam("site") String site) throws JSONException {

        JsonObject response = new JsonObject();

        try {

            TicketPeage ticketPeage = AssujettissementBusiness.getTicketPeageByCodeAndSite(code, site);

            if (ticketPeage != null) {

                Tarif tarif = TaxationBusiness.getTarifByCode(ticketPeage.getFkTarif());

                response.addProperty("status", "100");
                response.addProperty("message", "Succes");

                response.addProperty("code", ticketPeage.getCode().toUpperCase());
                response.addProperty("namePersonne", "");
                response.addProperty("montant", ticketPeage.getMontant());
                response.addProperty("devise", ticketPeage.getDevise().getCode().toUpperCase());

                response.addProperty("codeTarif", ticketPeage.getFkTarif());
                response.addProperty("nameTarif", tarif.getIntitule().toUpperCase());

                response.addProperty("codeSite", ticketPeage.getSite().getCode());
                response.addProperty("nameSite", ticketPeage.getSite().getIntitule().toUpperCase());

                response.addProperty("etat", ticketPeage.getEtat());
                response.addProperty("dateTicket", ConvertDate.formatDateHeureToString(ticketPeage.getDateProd()));

                response.addProperty("immatriculation", ticketPeage.getPlaqueChassis().toUpperCase());
                response.addProperty("typePaiement", ticketPeage.getTypePaiement().toUpperCase());

            } else {

                response.addProperty("status", "400");
                response.addProperty("message", "Ticket invalid");
            }

        } catch (Exception e) {
            response.addProperty("status", "500");
            response.addProperty("message", "Une erreur est survenue!");
        }

        return response.toString();
    }

    public List<JSONObject> getSpecifiquesTarifs(String site, String siteAccorder) {

        List<JSONObject> tarifJsonList = new ArrayList<>();

        try {
            List<TarifSite> tarifSites = AssujettissementBusiness.getAllTaxesPeagesBySiteUserV2(site.trim(), siteAccorder);
            if (!tarifSites.isEmpty()) {
                for (TarifSite ts : tarifSites) {
                    JSONObject dataTarif = new JSONObject();
                    Tarif tarif = TaxationBusiness.getTarifByCode(ts.getFkTarif());
                    dataTarif.put("code", tarif.getCode());
                    dataTarif.put("intitule", tarif.getIntitule().toUpperCase());
                    //dataTarif.put("valeur", tarif.getValeur());
                    //dataTarif.put("typeValeur", tarif.getTypeValeur());
                    dataTarif.put("montant", ts.getTaux());
                    dataTarif.put("devise", ts.getFkDevise());
                    tarifJsonList.add(dataTarif);
                }
            }
        } catch (Exception e) {
        }

        return tarifJsonList;
    }

    @POST
    @Path("savelogscanticket")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public String savelogscanticket(String logScanTicketData) {

        boolean result = false;

        JsonObject response = new JsonObject();

        try {

            result = AssujettissementBusiness.saveLogScanTicket(logScanTicketData);

            if (result) {
                response.addProperty("status", "100");
                response.addProperty("message", "Logs scan ticket effectué avec succès!");
            } else {
                response.addProperty("status", "200");
                response.addProperty("message", "Logs scan ticket echoué !");
            }

        } catch (Exception e) {
            response.addProperty("status", "210");
            response.addProperty("message", "Une erreur est survenue!");
        }

        return response.toString();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("logscanticketOld/{agent}/{site}")
    public Object getLogScanTicketOld(@PathParam("agent") String agent, @PathParam("site") String site) throws JSONException {

        List<JsonObject> responses = new ArrayList<>();
        JsonObject response = new JsonObject();

        try {

            List<LogScanTicket> listLogScanTicket = AssujettissementBusiness.getListLogScanTicketByAgentAndSite(
                    Integer.valueOf(agent), site);

            if (listLogScanTicket.size() > 0) {

                for (LogScanTicket logScanTicket : listLogScanTicket) {

                    response = new JsonObject();

                    Site siteTicket = TaxationBusiness.getSiteByCode(logScanTicket.getFkSiteTicket());
                    Site siteScan = TaxationBusiness.getSiteByCode(logScanTicket.getFkSiteScan());
                    Tarif tarif = TaxationBusiness.getTarifByCode(logScanTicket.getFkTarif());
                    Agent agentScan = PaiementBusiness.getAgentByCode(Integer.valueOf(agent));
                    TicketPeage ticketPeage = PeageBusiness.getTicketPeageByCode(logScanTicket.getCodeTicket());

                    response.addProperty("status", "100");
                    response.addProperty("message", "Succes");

                    response.addProperty("id", logScanTicket.getId());
                    response.addProperty("ticket", logScanTicket.getCodeTicket() == null ? "" : logScanTicket.getCodeTicket().toUpperCase());
                    response.addProperty("stationTicket", siteTicket == null ? "" : siteTicket.getIntitule().toUpperCase());
                    response.addProperty("montant", logScanTicket.getMontant() == null ? 0 : logScanTicket.getMontant());
                    response.addProperty("devise", logScanTicket.getDevise() == null ? "" : logScanTicket.getDevise().toUpperCase());
                    response.addProperty("plaque", logScanTicket.getPlaque() == null ? "" : logScanTicket.getPlaque().toUpperCase());
                    response.addProperty("dateTicket", ticketPeage == null ? "" : Tools.formatDateWithTimeToString(ticketPeage.getDateSync()));
                    response.addProperty("typePaiement", logScanTicket.getTypePaiement() == null ? "" : logScanTicket.getTypePaiement().toUpperCase());
                    response.addProperty("categorie", tarif == null ? "" : tarif.getIntitule().toUpperCase());
                    response.addProperty("stationScan", siteScan == null ? "" : siteScan.getIntitule().toUpperCase());
                    response.addProperty("dateScan", logScanTicket.getDateScan() == null ? "" : Tools.formatDateWithTimeToString(logScanTicket.getDateScan()));
                    response.addProperty("agentScan", agentScan == null ? "" : agentScan.toString().toUpperCase());

                    responses.add(response);
                }

                return responses.toString();

            } else {

                response.addProperty("status", "200");
                response.addProperty("message", "Log scan empty");
                return response.toString();
            }

        } catch (Exception e) {
            response.addProperty("status", "300");
            response.addProperty("message", "Une erreur est survenue!");
            return response.toString();
        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("logscanticket/{agent}/{site}")
    public Object getLogScanTicket(@PathParam("agent") String agent, @PathParam("site") String site) throws JSONException {

        List<JsonObject> responses = new ArrayList<>();
        JsonObject response = new JsonObject();
        JsonObject myResponse = new JsonObject();

        try {

            List<LogScanTicket> listLogScanTicket = AssujettissementBusiness.getListLogScanTicketByAgentAndSite(
                    Integer.valueOf(agent), site);

            if (listLogScanTicket.size() > 0) {

                myResponse.addProperty("status", "100");
                myResponse.addProperty("message", "Succes");

                for (LogScanTicket logScanTicket : listLogScanTicket) {

                    response = new JsonObject();

                    Site siteTicket = TaxationBusiness.getSiteByCode(logScanTicket.getFkSiteTicket());
                    Site siteScan = TaxationBusiness.getSiteByCode(logScanTicket.getFkSiteScan());
                    Tarif tarif = TaxationBusiness.getTarifByCode(logScanTicket.getFkTarif());
                    Agent agentScan = PaiementBusiness.getAgentByCode(Integer.valueOf(agent));
                    TicketPeage ticketPeage = PeageBusiness.getTicketPeageByCode(logScanTicket.getCodeTicket());

                    response.addProperty("id", logScanTicket.getId());
                    response.addProperty("ticket", logScanTicket.getCodeTicket() == null ? "" : logScanTicket.getCodeTicket().toUpperCase());
                    response.addProperty("stationTicket", siteTicket == null ? "" : siteTicket.getIntitule().toUpperCase());
                    response.addProperty("montant", logScanTicket.getMontant() == null ? 0 : logScanTicket.getMontant());
                    response.addProperty("devise", logScanTicket.getDevise() == null ? "" : logScanTicket.getDevise().toUpperCase());
                    response.addProperty("plaque", logScanTicket.getPlaque() == null ? "" : logScanTicket.getPlaque().toUpperCase());
                    response.addProperty("dateTicket", ticketPeage == null ? "" : Tools.formatDateWithTimeToString(ticketPeage.getDateSync()));
                    response.addProperty("typePaiement", logScanTicket.getTypePaiement() == null ? "" : logScanTicket.getTypePaiement().toUpperCase());
                    response.addProperty("categorie", tarif == null ? "" : tarif.getIntitule().toUpperCase());
                    response.addProperty("stationScan", siteScan == null ? "" : siteScan.getIntitule().toUpperCase());
                    response.addProperty("dateScan", logScanTicket.getDateScan() == null ? "" : Tools.formatDateWithTimeToString(logScanTicket.getDateScan()));
                    response.addProperty("agentScan", agentScan == null ? "" : agentScan.toString().toUpperCase());

                    responses.add(response);
                }

                myResponse.addProperty("data", responses.toString());

                return myResponse.toString();

            } else {

                response.addProperty("status", "200");
                response.addProperty("message", "Log scan empty");
                return response.toString();
            }

        } catch (Exception e) {
            response.addProperty("status", "300");
            response.addProperty("message", "Une erreur est survenue!");
            return response.toString();
        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("dashboard/{agent}/{site}/{day}")
    public Object getDataDashboard(@PathParam("agent") String agent, @PathParam("site") String site, @PathParam("day") String day) throws JSONException {

        JsonObject response = new JsonObject();

        try {

            day = ConvertDate.getValidFormatDate(day);

            int numberTicketGlobal = PeageBusiness.getNumberTicketGenerate(Integer.valueOf(agent), site);
            int numberTicketDay = PeageBusiness.getNumberTicketGenerate(Integer.valueOf(agent), site, day);

            BigDecimal sumTicketGlobal_Cdf = new BigDecimal("0");
            BigDecimal sumTicketGlobal_Usd = new BigDecimal("0");

            BigDecimal sumTicketDay_Cdf = new BigDecimal("0");
            BigDecimal sumTicketDay_Usd = new BigDecimal("0");

            sumTicketDay_Cdf = sumTicketDay_Cdf.add(PeageBusiness.getSumTicketGenerate(Integer.valueOf(agent), site, day, "CDF"));
            sumTicketGlobal_Cdf = sumTicketGlobal_Cdf.add(PeageBusiness.getSumTicketGenerate(Integer.valueOf(agent), site, "CDF"));

            sumTicketDay_Usd = sumTicketDay_Usd.add(PeageBusiness.getSumTicketGenerate(Integer.valueOf(agent), site, day, "USD"));
            sumTicketGlobal_Usd = sumTicketGlobal_Usd.add(PeageBusiness.getSumTicketGenerate(Integer.valueOf(agent), site, "USD"));

            response.addProperty("status", "100");
            response.addProperty("message", "Succes");

            response.addProperty("total_ticket_produit_day", numberTicketDay);
            response.addProperty("total_ticket_produit_global", numberTicketGlobal);

            response.addProperty("montant_ticket_produit_cdf_day", sumTicketDay_Cdf);
            response.addProperty("montant_ticket_produit_cdf_global", sumTicketGlobal_Cdf);

            response.addProperty("montant_ticket_produit_usd_day", sumTicketDay_Usd);
            response.addProperty("montant_ticket_produit_usd_global", sumTicketGlobal_Usd);

            return response.toString();

        } catch (Exception e) {
            response.addProperty("status", "200");
            response.addProperty("message", "Une erreur est survenue!");
            return response.toString();
        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("swclosed/{value}")
    public Object swclosed(@PathParam("value") String value) throws JSONException, Exception {

        JsonObject response = new JsonObject();

        boolean result = AssujettissementBusiness.swClosed(value);

        if (result) {

            response.addProperty("status", "100");
            response.addProperty("message", "Succes");

        } else {
            response.addProperty("status", "100");
            response.addProperty("message", "Failed");
        }

        return response.toString();
    }
}
