<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion de remise de pré-paiement péage</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">Informations des allegements sur la taxe de p&eacute;age</h4> 

                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-10">
                                    <label class="control-label" style="font-weight: normal">Soci&eacute;t&eacute; s&eacute;lectionn&eacute;e :</label>
                                    <input type="text" class="form-control" id="societeNameEdit" readonly="true"
                                           placeholder="Soci&eacute;t&eacute; s&eacute;lectionn&eacute;e">
                                </div>
                                <div class="col-lg-2" style="margin-top: 23px">
                                    <button type="submit" class="btn btn-success" id="btnSearchSociete">Rechercher une Soci&eacute;t&eacute;</button>
                                </div>
                            </div><br/>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Axe p&eacute;age </label>
                                <select class="form-control" id="selectSiteEdit" >
                                    <option value="0">--</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Cat&eacute;gorie</label>
                                <select class="form-control" id="selectCategorieEdit">
                                    <option value="0">--</option>
                                </select>

                            </div>

                            <div class="form-group">
                                <!--label class="control-label" style="font-weight: normal">Tarif (En USD)</label-->
                                <label class="control-label" style="font-weight: normal">Tarif <span id="spnDeviseTarif"></span></label>
                                <input type="number" class="form-control" min="1"
                                       id="tarifEdit" placeholder="Saisir le tarif de la cat&aacute;gorie">
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Devise</label>
                                <select class="form-control" id="selectDeviseEdit" >
                                    <option value="0">--</option>
                                    <option value="USD">DOLLARS AM&Eacute;RICAINS</option>
                                    <option value="CDF">FRANC CONGOLAIS</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Mode de perception</label>
                                <select class="form-control" id="selectModePaiementEdit" >
                                    <option value="0">--</option>
                                    <option value="1">PRE-PAIEMENT</option>
                                    <option value="2">CREDIT</option>
                                    <option value="3">EXON&Eacute;RATION</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" id="btnSaveRemiseEdit">
                                    <i class="fa fa-save"></i> Enregistrer
                                </button>
                            </div><hr/>

                            <div class="fonctions">
                                <table id="tableRemise" class="table">

                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="modal fade" id="newCategorieRemiseModal" role="dialog" data-backdrop="static" data-keyboard="false" >

            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" id="exampleModalLongTitle">Ajout de la nouvelle cat&eacute;gorie de remise</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="control-label col-sm-12" >Veuillez s&eacute;lectionner une cat&eacute;gorie &nbsp;</label>
                            <br/><br/>
                            <select class="form-control" id="selectNewCategorie" style="margin-left: 15px;width: 800px">
                            </select>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnValidateINewCategorieRemise">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalMiseAJourInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form class="form formulaire-edition-fonction">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Mise à jour des informations</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <input type="text" class="form-control" id="societeNameSelectd" readonly="true"
                                       placeholder="Soci&eacute;t&eacute; s&eacute;lectionn&eacute;e">
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Poste p&eacute;age </label>
                                <select class="form-control" id="selectSiteSelected" >
                                    <option value="0">--</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Cat&eacute;gorie</label>
                                <select class="form-control" id="selectCategorieSelected" >
                                    <option value="0">--</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Mode de perception</label>
                                <select class="form-control" id="selectModePaiementSelected" >
                                    <option value="0">--</option>
                                    <option value="1">PRE-PAIEMENT</option>
                                    <option value="2">CREDIT</option>
                                    <option value="3">EXON&Eacute;RATION</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Taux</label>
                                <input type="number" class="form-control" min="1"
                                       id="tarifSelected" placeholder="Saisir le tarif">
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Devise</label>
                                <select class="form-control" id="selectDeviseSelected" >
                                    <option value="0">--</option>
                                    <option value="USD">DOLLARS AM&Eacute;RICAINS</option>
                                    <option value="CDF">FRANC CONGOLAIS</option>
                                </select>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-dismiss" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-danger" id="btnDeleteRemise">Supprimer</button>
                            <button type="submit" class="btn btn-success" id="btnSaveRemiseSelected">Modifier</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/rechercheAssujetti.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script type="text/javascript" src="assets/js/tarifSite/gestionRemisePeage.js"></script>

    </body>
</html>
