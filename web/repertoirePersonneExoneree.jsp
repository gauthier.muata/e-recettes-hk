<%-- 
    Document   : repertoirePersonneExoneree
    Created on : 6 mai 2021, 17:54:36
    Author     : Juslin TSHIAMUA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title> R&eacute;pertoire des exon&eacute;r&eacute;es</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .lblTxtAssuj{text-align: justify;margin-top: 3px}
                .divTableBien,.divTableArticle{overflow: auto}
            }
        </style>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <h4 class="panel-title">FILTRE DE RECHERCHE</h4>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <form class="form-inline" role="form">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control inputAssujettisExoneres" style="width: 100%" 
                                                           placeholder="Nom de l'assujetti">
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-primary btnSearchAssujettisExoneres"> 
                                                            <i class="fa fa-search"></i> 
                                                            <span class="spanSearchLabel">Rechercher</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <button class="btn btn-warning pull-right btnAdvancedSearch"> 
                                                    <i class="fa fa-search"></i> 
                                                    <span class="spanSearchLabel">Recherche avanc&eacute;e</span>
                                                </button>
                                            </div>
                                            <div class="col-lg-1">
                                                <button class="btn btn-warning btnRapport"> 
                                                    <i class="fa fa-file-pdf-o"></i> 
                                                    <span class="spanSearchLabel">PDF</span>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">REPERTOIRE DES EXONERES </h4>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="tableExonerees" class="table table-bordered"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/rechercheAvanceeExoneres.html" %>
        <%@include file="assets/include/modal/annulationExoneration.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/lib/js/dknotus-tour.min.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/export/jspdf.min.js"></script>
        <script type="text/javascript" src="assets/js/export/jspdf.plugin.autotable.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script src="assets/js/repertoirePersonnesExonerees.js" type="text/javascript"></script>
    </body>
</html>
