<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des biens automobile</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row" style="margin-left: 5px">
                    <div class="col-lg-10">
                        <label class="control-label" style="font-weight: normal">Contribuable s&eacute;lectionn&eacute; :</label>
                        <input type="text" class="form-control" id="contribuableNameEdit" readonly="true"
                               placeholder="Contribuable s&eacute;lectionn&eacute;">
                    </div>
                    <div class="col-lg-2" style="margin-top: 23px">
                        <button type="submit" class="btn btn-success" id="btnSearchContribuable">
                            Rechercher &nbsp;
                            <i class="fa fa-user"></i>
                        </button>
                    </div>
                </div><hr style="margin-left: 15px"/>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">Informations du bien automobile</h4> 

                        </div>
                        <div class="panel-body">

                            <div class="row">

                                <div class="col-lg-4">
                                    <label class="control-label" style="font-weight: normal">Genre :</label>
                                    <select class="form-control" id="selectGenreEdit" style="font-weight: bold">
                                        <option value="0">--</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="control-label" style="font-weight: normal">Cat&eacute;gorie :</label>
                                    <select class="form-control " id="selectCategorieEdit" style="font-weight: bold" disabled="true">
                                        <option value="0" style="font-weight: bold">--</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="control-label" style="font-weight: normal">D&eacute;nomination :</label>
                                    <input type="text" class="form-control" id="denominationEdit"  readonly="true"
                                           placeholder="D&eacute;nomination">
                                </div>

                            </div><br/>

                            <div class="row">

                                <div class="col-lg-4">
                                    <label class="control-label" style="font-weight: normal">Marque :</label>
                                    <select class="form-control " id="MarqueEdit" data-live-search="true" style="font-weight: bold">
                                        <option value="0" style="font-weight: bold">--</option>
                                    </select>

                                </div>
                                <div class="col-lg-4">
                                    <label class="control-label" style="font-weight: normal">Mod&egrave;le :</label>

                                    <select class="form-control" id="TypeEdit" data-live-search="true" style="font-weight: bold">
                                        <option value="0">--</option>
                                    </select>

                                </div>
                                <div class="col-lg-4">
                                    <label class="control-label" style="font-weight: normal">&numero; plaque :</label>
                                    <input type="text" class="form-control" id="NumeroPlaqueEdit"  
                                           placeholder="&numero; plaque">
                                </div>

                            </div><br/>

                            <div class="row">

                                <div class="col-lg-2">
                                    <label class="control-label" style="font-weight: normal">&numero; Chassis :</label>
                                    <input type="text" class="form-control" id="NumeroChassisEdit" 
                                           placeholder="&numero; Chassis">
                                </div>

                                <div class="col-lg-2">
                                    <label class="control-label" style="font-weight: normal;color:red">Est un V&eacute;hicule Utilitaire ?</label>
                                    <select class="form-control" id="typeVehicule" style="font-weight: bold">
                                        <option value="0">--</option>
                                        <option value="NON">NON</option>
                                        <option value="OUI">OUI</option>

                                    </select>


                                </div>

                                <div class="col-lg-4">
                                    <label class="control-label" style="font-weight: normal">Unit&eacute; Puissance fiscale :</label>
                                    <select class="form-control" id="UnitePuissanceFiscalEdit" style="font-weight: bold">
                                        <option value="0">--</option>
                                        <option value="U00142015">CV</option>
                                        <option value="U00082015">TONNES</option>
                                        <option value="U00192015">M3</option>
                                        <option value="U00822015">CC</option>
                                    </select>


                                </div>

                                <div class="col-lg-2">
                                    <label class="control-label" style="font-weight: normal">Puissance fiscale :</label>
                                    <input type="number"  min="1" class="form-control" id="PuissanceFiscalEdit" disabled="true"
                                           placeholder="Puissance fiscale" style="font-weight: bold">
                                </div>

                                <div class="col-lg-2">
                                    <label class="control-label" style="font-weight: normal">Exercice :</label>
                                    <select class="form-control" id="ExerciceEdit" style="font-weight: bold">
                                        <option value="2021" style="font-weight: bold">2021</option>
                                    </select>
                                </div>

                            </div><br/>

                            <div class="row">

                                <!--div class="col-lg-4">
                                    <label class="control-label" style="font-weight: normal">Ann&eacute;e de mise en circulation :</label>
                                    <input type="number"  min="1" class="form-control" id="AnneeCirculationEdit" 
                                           placeholder="Ann&eacute;e de mise en circulation">
                                </div-->

                                <div class="col-lg-4">
                                    <label class="control-label" style="font-weight: normal">Date mise en circulation :</label>
                                    <input type="date"  class="form-control" id="DateMiseCirculation" >
                                </div>


                                <div class="col-lg-7">
                                    <label class="control-label" style="font-weight: normal">Adresse du bien :</label>
                                    <input type="text" class="form-control" id="AdresseEdit" readonly="true"
                                           placeholder="Adresse du bien">


                                </div>
                                <div class="col-lg-1" style="margin-top: 25px">
                                    <button type="submit" class="btn btn-warning" id="btnAjouterAdresseEdit" disabled="true">
                                        <i class="fa fa-home"></i> 
                                    </button>
                                </div>
                            </div><br/>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" id="btnSaveBien" disabled="true" >
                                    <i class="fa fa-save"></i> Enregistrer
                                </button>
                                <button type="submit" class="btn btn-danger" id="btnNettoyer">
                                    <i class="fa fa-trash-o"></i> Nettoyer
                                </button>
                                <button type="submit" class="btn btn-warning" id="btnAfficherAllBien" disabled="true">
                                    <i class="fa fa-list"></i> Afficher tous les biens
                                </button>
                                <button type="submit" class="btn btn-danger pull-right" id="btnDeleteBien" style="display: none">
                                    <i class="fa fa-trash-o"></i> Supprimer
                                </button>
                                <button type="submit" class="btn btn-danger" id="btnDeleteAllBienNotTaxed" style="display: none; margin-left: 15px">
                                    <i class="fa fa-trash-o"></i> D&eacute;sactiver tous les biens non-tax&eacute;s
                                </button>
                            </div><hr/>
                            <div class="row">
                                <div class="col-lg-6" style="display: none" id="idDivCheck">
                                    <label class="control-label" style="font-weight: normal;color:red">
                                        <input type="checkbox" id="checkBoxSelectAllBien" name="checkBoxSelectAllBien"/>
                                        Cochez pour taxer tous les biens automobile du contribuable
                                    </label>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-success" id="btnTaxationAllBien" style="display: none">
                                        <i class="fa fa-calculator"></i> Taxer les biens sélectionner
                                    </button>
                                </div>
                            </div><hr/>
                            <div class="fonctions">
                                <table id="tableBienAutomobile" class="table">

                                </table>
                            </div>

                            <br/>

                            <button class="btn btn-warning" id="btnDisplayCotation" style="display: none">
                                <i class="fa fa-calculator"></i>
                                Voir la cotation bien(s) automobile(s)
                            </button>

                        </div>
                    </div>
                </div>

                <div class="modal fade" id="newGenreModal" role="dialog" data-backdrop="static" data-keyboard="false" >

                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title" id="exampleModalLongTitle">Ajout du nouveau Genre de v&eacute;hicule</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row" style="margin-left: 5px">
                                    <div class="col-lg-12">
                                        <label class="control-label" style="font-weight: normal">Nouveau Genre :</label>
                                        <input type="text" class="form-control" id="inputNewGenre"  
                                               placeholder="Genre">
                                    </div>

                                </div> 
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success" id="btnNewGenre">
                                    <i class="fa fa-plus-circle"></i>
                                    Ajouter
                                </button>
                                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="newMarqueModal" role="dialog" data-backdrop="static" data-keyboard="false" >

                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title" id="exampleModalLongTitle">Ajout de la nouvelle Marque de v&eacute;hicule</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row" style="margin-left: 5px">
                                    <div class="col-lg-12">
                                        <label class="control-label" style="font-weight: normal">Nouvelle Marque :</label>
                                        <input type="text" class="form-control" id="inputNewMarque"  
                                               placeholder="Marque">
                                    </div>

                                </div> 
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success" id="btnNewMarque">
                                    <i class="fa fa-plus-circle"></i>
                                    Ajouter
                                </button>
                                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="newModeleModal" role="dialog" data-backdrop="static" data-keyboard="false" >

                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title" id="exampleModalLongTitle">Ajout du nouveau Mod&egrave;le de v&eacute;hicule</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row" style="margin-left: 5px">
                                    <div class="col-lg-12">
                                        <label class="control-label" style="font-weight: normal">Nouveau Mod&egrave;le :</label>
                                        <input type="text" class="form-control" id="inputNewModele"  
                                               placeholder="Mod&egrave;le">
                                    </div>

                                </div> 
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success" id="btnNewModele">
                                    <i class="fa fa-plus-circle"></i>
                                    Ajouter
                                </button>
                                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modalDisplayCotation" role="dialog" data-backdrop="static" data-keyboard="false" >

                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title" id="exampleModalLongTitle">La cotation des biens automobiles du contribuale d&eacute;nomm&eacute;(e) : <span style="font-weight: bold;font-size: 18px" id="spnNameContribuable"></span></h5>
                            </div>
                            <div class="modal-body">

                                <div class="fonctions">
                                    <table id="tableCotation" class="table">

                                    </table>
                                </div>

                                <br/>

                                <label class="control-label" style="font-weight: normal">TOTAL GENERAL : <span id="spnTotalGen" style="font-weight: bold;color: red;font-size: 18px"></span></label>

                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="periodeDeclarationModal" role="dialog" data-backdrop="static" data-keyboard="false" >

                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title" id="exampleModalLongTitle">S&eacute;lectionner la p&eacute;riode de d&eacute;claration</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row" style="margin-left: 5px">
                                    <div class="col-lg-12">
                                        <label class="control-label" style="font-weight: normal">Devise :</label>
                                        <select class="form-control" id="selectDevise" style="font-weight: bold">
                                            <option value="0">--</option>
                                            <option value="USD">DOLLARS AM&Eacute;RICAINS</option>
                                            <option value="CDF">FRANC CONGOLAIS</option>
                                        </select>
                                        <br/>
                                    </div>
                                    <br/><br/>
                                    <div class="col-lg-12">
                                        <label class="control-label" style="font-weight: normal">Banque :</label>
                                        <select class="form-control" id="selectBanque" style="font-weight: bold">
                                            <option value="0">--</option>
                                        </select>
                                        <br/>
                                    </div>
                                    <br/><br/>
                                    <div class="col-lg-12">
                                        <label class="control-label" style="font-weight: normal">Compte bancaire :</label>
                                        <select class="form-control" id="selectCompteBancaire" style="font-weight: bold">
                                            <option value="0">--</option>
                                        </select>
                                    </div>

                                    <br/><br/><br/>
                                    <div class="col-lg-12">
                                        <label class="control-label" style="font-weight: normal">P&eacute;riode de d&eacute;claration :</label>
                                        <select class="form-control" id="selectPeridoeDecl" style="font-weight: bold">

                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value=2020>2020</option>
                                            <option value="2021" selected>2021</option>
                                            <option value="2022">2022</option>
                                        </select>
                                    </div>

                                </div> 
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success" id="btnValidateTaxation">
                                    <i class="fa fa-check-circle"></i>
                                    Valider taxation
                                </button>
                                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


    </div>

    <%@include file="assets/include/modal/rechercheAssujetti.html" %>
    <%@include file="assets/include/modal/rechercheAdressePersonne.html" %>
    <%@include file="assets/include/modal/modalAddPeriodeDeclarationAssujettissement.html" %>
    <%@include file="assets/include/modal/modalDisplayResumeTotalTaxationBatch.html" %>

    <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/select2.full.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script> 
    <script type="text/javascript" src="assets/js/utils.js"></script>
    <script type="text/javascript" src="assets/lib/choosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
    <script type="text/javascript" src="assets/js/gestionBienAutomobile.js"></script>
    <script type="text/javascript" src="assets/js/rechercheAdressePersonne.js"></script>

    <!--script>
        $(function () {
            $('.select2').select2()
        })
    </script-->


</body>
</html>
