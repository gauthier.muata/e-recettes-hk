<%-- 
    Document   : validerAssujettisEnLigne
    Created on : 17 avr. 2020, 08:47:05
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Registre des demandes d'inscriptions &abreve; la t&eacute;l&eacute;-proc&eacute;dure</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .journal{overflow: auto}
                .spanSearchLabel{display: none}
                .inputGroupAssuj{margin-top: 3px}
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="cmbSearchTypeAssujettisValider">
                                <option value ="0">Assujetti</option>
                                <option value ="1">NTD</option>
                            </select>
                            <div class="input-group inputGroupAssuj">
                                <input type="text" class="form-control" style="width: 100%" id="inputSearchAssujettisValider" 
                                       placeholder="Nom de l'assujetti">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" id="btnSearchAssujettisValider"> 
                                        <i class="fa fa-search"></i> 
                                        <span class="spanSearchLabel">Rechercher</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <button id="btnShowAdvancedSearchModal" style="margin-right: 20px" class="btn btn-warning pull-right btnPerso">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée</button>

                </div>

                <hr/>

                <div class="journal">

                    <button class="btn btn-sm btn-primary" id="btnSendMailNotification" style="display: none"><i class="fa fa-mail-reply-all"></i>&nbsp;&nbsp;Renvoyer les mails de notification à tous les assujettis</button>
                    <table id="tableAssujettiValider" class="table table-bordered">

                    </table>
                </div>

            </div>

            <div class="modal fade" id="typeSearchModal" role="dialog" data-backdrop="static" data-keyboard="false" >

                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title" id="exampleModalLongTitle">Filtrage de type de demande d'inscription</h5>
                        </div>
                        <center>
                            <div class="modal-body">
                                <div class="row">
                                    <label class="control-label col-sm-12" >Veuillez s&eacute;lectionner un type</label>
                                    <br/><br/>
                                    <select class="form-control" id="selectTypeTraitement" style="margin-left: 15px;width: 800px">
                                        <option value="3">EN ATTENTE DU TRAITEMENT</option>
                                        <option value="4">TRAITER MAIS NON NOTIFI&Eacute; PAR MAIL</option>
                                    </select>
                                </div> 
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success" id="btnSearchAdvanced2">
                                    <i class="fa fa-search"></i>
                                    Lancer la recherche
                                </button>
                                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>


        <%@include file="assets/include/modal/header.html" %>
        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script  type="text/javascript" src="assets/js/repertoire/validerAssujettis.js"></script>
    </body>
</html>
