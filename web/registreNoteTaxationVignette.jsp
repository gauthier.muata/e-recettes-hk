<%-- 
    Document   : registreRetraitDeclaration
    Created on : 13 févr. 2020, 08:29:54
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des notes de taxation vignettes</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css" />
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            .details-control {
                background: url('assets/images/details_open.png') no-repeat center center;
                cursor: pointer;
            }
            tr.shown td.details-control {
                background: url('assets/images/details_close.png') no-repeat center center;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">

                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="typeResearchRegistreRetraitDeclaration">
                                <option selected value="1">Contribuable</option>
                                <option value="2">Numéro de la note</option>
                            </select>
                            <div class="input-group" >
                                <input 
                                    type="text" 
                                    class="form-control" 
                                    readonly="true"
                                    style="width: 380px" 
                                    placeholder="Veuillez saisir le nom du contribuable"
                                    id="inputResearchRetraitDeclar">

                                <div class="input-group-btn">
                                    <button 
                                        class="btn btn-primary" 
                                        type="submit"id="btnResearchRetraitDeclaration">
                                        <i class="fa fa-search"></i> 
                                        Rechercher
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <button 
                        id="btnCallModalSearchAvandedRetraitDeclaration"
                        style="margin-right: 20px" 
                        class="btn btn-warning pull-right">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche par nature impôt
                    </button>

                </div>

                <hr/>

                <div class="row" id="isAdvance" style="display: none">
                    <div  class="col-lg-12">
                        <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour :</label>
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <div class="row" style="margin-left: 5px;">
                                    <div class="journal">

                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Bureau : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblSite" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Impôt : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblService" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <!--span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Type d&eacute;claration : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="labelType" >&nbsp;&nbsp;&nbsp;</b>
                                        </span-->


                                        <span style="font-size: 16px; color: black" >
                                            &nbsp;&nbsp;&nbsp;&nbsp;Du : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="lblDateDebut" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black" >
                                            &nbsp;&nbsp;&nbsp;&nbsp;Au : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="lblDateFin" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                    </div>
                                </div>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="journal" >
                    <!--button class="btn btn-sm btn-primary pull-right impression">
                        <i class="fa fa-print"></i>&nbsp;&nbsp;Imprimer rapport
                    </button-->
                    <table id="tableRegistreRetraitDeclaration" class="table table-bordered">
                    </table>
                </div>

                <div class="modal fade" id="modalTaxationDupplicaVignette" data-backdrop="static" data-keyboard="false"
                     tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="width: 50%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Taxation Duplicata Vignette</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-body">

                                <div class="modal-body" style="width: 100%">
                                    <div class="form-group">
                                        <span>VALEUR DE BASE (ACTUELLE) :&nbsp;&nbsp;<span style="color:red;font-size: 18px;font-weight: bold" id="spnBaseActuelle"> </span>
                                    </div>
                                </div>

                                <div class="modal-body" style="width: 100%">
                                    <div class="form-group">
                                        <span>NOUVELLE BASE :</span><br/><br/>

                                        <input class="form-control" id="newBase" type="number" min="1">
                                    </div>
                                    <div class="form-group">
                                        <span>UNITE : </span><br/><br/>
                                        <select class="form-control" id="cmbUnite" >

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <span>OBSERVATION : </span><br/><br/>
                                        <textarea rows="5" id="inputObservationRedressement" class="form-control" placeholder="Veuillez motiver la raison de la modification ici" >
                                        </textarea>
                                    </div>

                                </div>

                                <div class="modal-footer">

                                    <button                    
                                        type="button" 
                                        class="btn btn-danger pull-left" 
                                        id="btnConfirmeRedressement">
                                        <i class="fa fa-check-circle"></i>  
                                        Valider la modification
                                    </button>

                                    <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modifyAmountBienModal" role="dialog" data-backdrop="static" data-keyboard="false" >

                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title" id="exampleModalLongTitle">Modification du montant</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <label class="control-label col-sm-12" >Veuillez fournir le nouveau montant en &nbsp;
                                        <span style="font-weight: lighter;color: #08C" id="lblDeviseNewAmount"></span> : 
                                    </label>
                                    <div class="col-sm-12">
                                        <input class="form-control" id="inputNewAmountBien" placeholder="Base de calcul" type="number" min="1" value="1">
                                    </div>

                                </div> 
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success" id="btnValidateNewAmount">
                                    <i class="fa fa-check-circle"></i>
                                    Valider
                                </button>
                                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAvanceeNC.html" %>
        <%@include file="assets/include/modal/modalSearchArticleBudgetaire.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>
        <%@include file="assets/include/modal/modalReviewAmountPenalite.html" %>
        <%@include file="assets/include/modal/modalOrdonnancementControle.html" %>
        <%@include file="assets/include/modal/rechercheAvancee.html" %>
        <%@include file="assets/include/modal/modalDisplayDetailTaxation.html" %>
        <%@include file="assets/include/modal/modalPrintVignette.html" %>

        <script src="assets/lib/js/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/uploading.js"></script>

        <script type="text/javascript" src="assets/js/export/jspdf.min.js"></script>
        <script type="text/javascript" src="assets/js/export/jspdf.plugin.autotable.js"></script>
        <script  type="text/javascript" src="assets/js/declaration/registreRetraitDeclarationVignette.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script src="assets/js/modalStandardAdvancedSearch.js" type="text/javascript"></script>

    </body>
</html>
