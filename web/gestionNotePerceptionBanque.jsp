<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des notes de perception banque</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">Informations des notes de perception banque</h4> 

                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-6">
                                    <label class="control-label" style="font-weight: normal">Note de perception :</label>
                                    <input type="text" class="form-control" id="notePerceptionEdit" 
                                           placeholder="Note de perception">
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label" style="font-weight: normal">Banque :</label>
                                    <select class="form-control" id="selectBanqueEdit" >
                                        <option value="0">--</option>
                                    </select>
                                </div>

                                <div class="col-lg-3">
                                    <label class="control-label" style="font-weight: normal">Ville :</label>
                                    <select class="form-control" id="selectVilleNPEdit" >
                                        <option value="0">--</option>
                                        <option value="00000000000428392016">KASUMBALESA</option>
                                        <option value="00000000000428382016">KIPUSHI</option>
                                        <!--option value="00000000000429092016">LIKASI</option-->
                                        <option value="00000000000428162016">LIKASI</option>
                                        <option value="00000000000429052016">LUBUMBASHI</option>
                                    </select>
                                </div>
                            </div><br/>

                            <div class="row">
                                <div class="col-lg-6">
                                    <label class="control-label" style="font-weight: normal">Plage d&eacute;but Note de perception :</label>
                                    <input type="text" class="form-control" id="PlageDebutNotePerceptionEdit" 
                                           placeholder="Plage d&eacute;but note de perception">
                                </div>
                                <div class="col-lg-6">
                                    <label class="control-label" style="font-weight: normal">Plage fin Note de perception :</label>
                                    <input type="text" class="form-control" id="PlageFinNotePerceptionEdit" 
                                           placeholder="Plage fin note de perception">
                                </div>
                            </div><br/>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" id="btnSaveEdit">
                                    <i class="fa fa-save"></i> Enregistrer
                                </button>
                                <button type="submit" class="btn btn-danger" id="btnDisplayAllNote">
                                    <i class="fa fa-list"></i> Afficher toutes les notes utilis&eacute;es
                                </button>
                                <button type="submit" class="btn btn-warning" id="btnDisplayAllNoteNotUse">
                                    <i class="fa fa-list"></i> Afficher toutes les notes non-utilis&eacute;es
                                </button>
                                
                                <button type="submit" class="btn btn-success" id="btnSaveNpBankBatch">
                                    <i class="fa fa-save"></i> Enregistrer la plage
                                </button>
                            </div><hr/>

                            <div class="fonctions">
                                <table id="tableNotePerceptionBanque" class="table">

                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="modal fade" id="modalMiseAJourNotePerceptionBanque" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form class="form formulaire-edition-fonction">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Mise à jour des informations</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" >
                                <div class="col-lg-12">
                                    <label class="control-label" style="font-weight: normal">Action &amacr; faire :</label>
                                    <select class="form-control" id="selectAction" >
                                        <option value="0">--</option>
                                        <option value="1">Modifier la note de perception</option>
                                        <option value="2">Modifier la banque de la perception</option>
                                        <option value="3">Modifier la plage de la note de perception</option>
                                        <option value="5">Modifier la ville de la note de perception</option>
                                        <option value="4">Supprimer la note de perception</option>
                                    </select>
                                </div>

                            </div><br/>

                            <div class="row" id="divNpID" style="display: none">
                                <div class="col-lg-12">
                                    <label class="control-label" style="font-weight: normal">Note de perception :</label>
                                    <input type="text" class="form-control" id="notePerceptionSelected" 
                                           placeholder="Note de perception">
                                </div>

                            </div>

                            <div class="row" id="divVilleNpID" style="display: none">

                                <div class="col-lg-12">
                                    <label class="control-label" style="font-weight: normal;margin-left: 5px">Ville :</label>
                                    <select class="form-control" id="selectVilleNPSelected" >
                                        <option value="0">--</option>
                                        <option value="00000000000428392016">KASUMBALESA</option>
                                        <option value="00000000000428382016">KIPUSHI</option>
                                        <option value="00000000000428162016">LIKASI</option>
                                        <option value="00000000000429052016">LUBUMBASHI</option>
                                    </select>
                                </div>
                            </div>


                            <div class="row" id="divBanqueNpID" style="display: none">

                                <div class="col-lg-12">
                                    <label class="control-label" style="font-weight: normal">Banque :</label>
                                    <select class="form-control" id="selectBanqueSelected" >
                                        <option value="0">--</option>
                                    </select>
                                </div>

                            </div>

                            <div class="row" id="divPlageNpID" style="display: none">
                                <div class="col-lg-6">
                                    <label class="control-label" style="font-weight: normal">Plage d&eacute;but Note de perception :</label>
                                    <input type="text" class="form-control" id="PlageDebutNotePerceptionSelected" 
                                           placeholder="Plage d&eacute;but note de perception">
                                </div>
                                <div class="col-lg-6">
                                    <label class="control-label" style="font-weight: normal">Plage fin Note de perception :</label>
                                    <input type="text" class="form-control" id="PlageFinNotePerceptionSelected" 
                                           placeholder="Plage fin note de perception">
                                </div>
                            </div><br/>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-dismiss" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-danger" style="display: none" id="btnDeleteNoteSelected">Supprimer</button>
                            <button type="submit" class="btn btn-success" style="display: none" id="btnSaveSelected">Modifier</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/rechercheAssujetti.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script type="text/javascript" src="assets/js/gestionNotePerceptionBanque.js"></script>

    </body>
</html>
