<%-- 
    Document   : registreDepotDeclaration
    Created on : 11 févr. 2020, 08:37:36
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des dépôts déclaration</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>
    </head>
</head>
<body>
    <div class="wrapper">
        <%@include file="assets/include/menu.html" %>
        <div id="content" style="width: 100%">
            <%@include file="assets/include/header.html" %>

            <div class="row">

                <div class="col-lg-8">
                    <form class="form-inline" role="form">
                        Filtrer par 
                        <select  class="form-control" id="typeResearchRegistredepotDeclaration">
                            <option selected value="1">Assujetti</option>
                            <option value="2">Note de taxation</option>
                        </select>
                        <div class="input-group" >
                            <input 
                                type="text" 
                                class="form-control" 
                                style="width: 340px" 
                                id="inputResearchDepotDeclar">
                            <div class="input-group-btn">
                                <button 
                                    class="btn btn-primary" 
                                    type="submit"id="btnResearchDepotDeclaration">
                                    <i class="fa fa-search"></i> 
                                    Rechercher
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <button 
                    id="btnCallModalSearchAvandedDepotdeclaration"
                    style="margin-right: 20px" 
                    class="btn btn-warning pull-right">
                    <i class="fa fa-filter"></i> &nbsp;
                    Effectuer une recherche par nature impôt
                </button>
                <button 
                    type="button" 
                    class="btn btn-success" 
                    id="btnPrintAcquitLiberatoire"
                    style="display: none">
                    <i class="fa fa-print"></i>  
                    Imprimer 
                </button>

            </div>

            <hr/>
            <div class="journal" >
                <button class="btn btn-sm btn-primary pull-right impression">
                    <i class="fa fa-print"></i>&nbsp;&nbsp;Imprimer le rapport des d&eacute;p&ocirc;ts d&eacute;claration
                </button>
                <table id="tableRegistreDepotDeclaration" class="table table-bordered">
                </table>
            </div>

        </div>
    </div>

    <%@include file="assets/include/modal/header.html" %>
    <%@include file="assets/include/modal/rechercheAvanceeNC.html" %>
    <%@include file="assets/include/modal/modalDetailDepotDeclaration.html" %>
    <%@include file="assets/include/modal/rechercheAvanceeByArticle.html" %>
    <%@include file="assets/include/modal/rechercheAvancee.html" %>

    <script src="assets/lib/js/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="assets/js/utils.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
    <script type="text/javascript" src="assets/lib/js/dknotus-tour.min.js"></script>
    <script src="assets/lib/choosen/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript" src="assets/js/utils.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>

    <script type="text/javascript" src="assets/js/export/jspdf.min.js"></script>
    <script type="text/javascript" src="assets/js/export/jspdf.plugin.autotable.js"></script>

    <script type="text/javascript" src="assets/js/rechercheAvancee.js"></script>
    <script type="text/javascript" src="assets/js/declaration/depotDeclaration.js"></script>
    <script type="text/javascript" src="assets/js/declaration/registreDepotDeclaration.js"></script>
    <script type="text/javascript" src="assets/js/articleBudgetaire/rechercheAvanceeByArticle.js" ></script>
    <script src="assets/js/modalStandardAdvancedSearch.js" type="text/javascript"></script>

</body>
</html>
