<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des p&eacute;ages</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <!--link rel="stylesheet" href="assets/lib/css/font-awesome.css"--> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .lblTxtAssuj{text-align: justify;margin-top: 3px}
                .divTableBien{overflow: auto}
                #btnApplyTarication{width:100%;margin-top: 3px}
            }
        </style>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DE L'ASSUJETTI </h4>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body"> 
                                        <span id="lbl1" style="font-size: 16px;display: none">Type assujetti :</span>
                                        <span  >
                                            <p id="lblLegalForm"
                                               style="font-weight: bold;color: black"></p>
                                        </span><br/>
                                        <span id="lbl2" style="font-size: 16px;display: none">Noms :</span>
                                        <span  >
                                            <b id="lblNameResponsible" ></b>
                                        </span><br/><br/>
                                        <span id="lbl3" style="font-size: 16px;display: none">Adresse :</span>
                                        <span  >
                                            <p id="lblAddress"
                                               style="font-size: 14px;
                                               font-weight: bold;color: black"></p>
                                        </span>
                                    </div>
                                    <div class="panel-footer">
                                        <button  
                                            id="btnCallModalSearchResponsable"
                                            class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                            Rechercher un assujetti
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> LISTE DES BIENS AUTOMOBILE DE L'ASSUJETTI 
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in">

                                    <div class="panel-body">

                                        <div class="panel-body">

                                            <div class="row" id="divChooseTaxeTaxe" style="display:none">
                                                <div class="col-lg-1">
                                                    <label style="font-weight: normal;font-size: 14px">
                                                        Choix taxe.
                                                    </label> 
                                                </div>
                                                <div class="col-lg-11">
                                                    <select class="form-control" id="cmbTaxesPeages">
                                                    </select>
                                                </div>
                                            </div>

                                            <hr id="hrSeparator" style="display:none"/>

                                            <div class="row pull-left">

                                                <div class="col-lg-2">
                                                    <label class="pull-left" id="idLabelCheckedNumber" style="font-weight: normal;font-size: 14px">
                                                        0 bien sélectionné.
                                                    </label> 
                                                </div>

                                                <div class="col-lg-8" id="divTarification" style="visibility: hidden">
                                                    <select class="form-control" id="cmbTarification">
                                                    </select>
                                                </div>

                                                <div class="col-lg-2" id="divButtonAppliquer" style="visibility: hidden">
                                                    <button class="btn btn-success" id="btnApplyTarication">
                                                        <i class="fa fa-check-circle-o"></i> Appliquer le tarif
                                                    </button>
                                                </div>

                                            </div>

                                            <!--button class="btn btn-default" id="btnLocateBien"
                                                    style="float:right;margin-right: 5px;margin-top: 5px;display: none">
                                                <i class="fa fa-plus-circle"></i> Louer un bien
                                            </button>

                                            <button class="btn btn-default" id="btnNewBien"
                                                    style="float:right;margin-right: 5px;margin-top: 5px;display: none">
                                                <i class="fa fa-plus-circle"></i> Créer un bien
                                            </button-->

                                        </div>       

                                        <div class="divTableBien">
                                            <table id="tableBiens" class="table table-bordered"></table>  
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="modal fade" id="modalPeriodeDeclaration" data-backdrop="static" data-keyboard="false"
                         tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Périodes des déclarations</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="responsive">
                                                <div class="table-responsive">
                                                    <table
                                                        class="table table-bordered table-hover" 
                                                        id="tablePeriodeDeclaration">                                                       
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--div class="modal fade" id="modalAssujMultipleBien" data-backdrop="static" data-keyboard="false"
                         tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Lier les biens à une taxe</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-lg-2">
                                                <label style="font-weight: normal"> Tarif : </label>
                                            </div>
                                            <div class="col-lg-5" id="divCmbTarif">
                                                <select class="form-control" id="cmbTarif" placeholder="Tarif"></select>
                                            </div>
                                            <div class="col-lg-5" id="divInputTaux">
                                                <input type="text" style="color:red" class="form-control" id="inputTaux"/>
                                            </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                                </div>
                            </div>
                        </div>
                    </div-->

                </form> 

            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/locationBien.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/lib/js/dknotus-tour.min.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/gestionPeage.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>

    </body>

</html>