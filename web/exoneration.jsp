<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title> Exoneration</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <!--link rel="stylesheet" href="assets/lib/css/font-awesome.css"--> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .lblTxtAssuj{text-align: justify;margin-top: 3px}
                .divTableBien,.divTableArticle{overflow: auto}
            }
        </style>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DU CONTRIBUABLE </h4>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body"> 
                                        <span id="lbl1" style="font-size: 16px;display: none">Type contribuable :</span>
                                        <span>
                                            <p id="lblLegalForm"
                                               style="font-weight: bold;color: black"></p>
                                        </span><br/>
                                        <span id="lbl2" style="font-size: 16px;display: none">Noms :</span>
                                        <span>
                                            <b id="lblNameResponsible" ></b>
                                        </span><br/><br/>
                                        <span id="lbl3" style="font-size: 16px;display: none">Adresse :</span>
                                        <span>
                                            <p id="lblAddress"
                                               style="font-size: 14px;
                                               font-weight: bold;color: black"></p>
                                        </span>
                                    </div>
                                    <div class="panel-footer">
                                        <button  
                                            id="btnCallModalSearchResponsable"
                                            class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                            Rechercher un contribuable
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DES BIENS 
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in">

                                    <div class="panel-body">

                                        <ul class="nav nav-tabs">
                                            <li id="tabBiens" class="active"><a data-toggle="tab" href="#idBien">LISTE DES BIENS</a></li>
                                            <li id="tabArticles"><a data-toggle="tab" href="#idArticle">LES ACTES GENERATEURS</a></li>
                                        </ul>

                                        <div class="tab-content">

                                            <div id="idBien" class="tab-pane fade in active">

                                                <div class="panel-body">  </div>  

                                                <div class="divTableBien">
                                                    <table id="tableBiens" class="table table-bordered" ></table>
                                                </div>

                                            </div>

                                            <div id="idArticle" class="tab-pane fade">
                                                <div class="panel-body">  </div>
                                                <!--Liste des articles budgetaires-->
                                                <div class="divTableArticle">
                                                    <table id="tableArticles" class="table table-bordered"></table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="modal fade" id="modalPeriodeDeclaration" data-backdrop="static" data-keyboard="false"
                         tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="width: 85%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"><i class="fa fa-archive"></i>&nbsp;Périodes des déclarations</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">
                                    <div class="panel panel-default">

                                        <div class="panel-heading">
                                            <h4 class="panel-title">EXONERER DES BIENS SELON LES PERIODES DE DECLARATION</h4>
                                        </div>
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">
                                                <div class="col-lg-12">
                                                    <div class="responsive">
                                                        <div class="table-responsive">
                                                            <table
                                                                class="table table-bordered table-hover" 
                                                                id="tablePeriodeDeclaration">                                                       
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="modalRedressement" data-backdrop="static" data-keyboard="false"
                         tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="width: 50%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Redressement de la valeur de base</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">

                                    <div class="modal-body" style="width: 100%">
                                        <div class="form-group">
                                            <span>VALEUR DE BASE (ACTUELLE) :&nbsp;&nbsp;<span style="color:red;font-size: 18px;font-weight: bold" id="spnBaseActuelle"> </span>
                                        </div>
                                    </div>

                                    <div class="modal-body" style="width: 100%">
                                        <div class="form-group">
                                            <span>NOUVELLE BASE :</span><br/><br/>

                                            <input class="form-control" id="newBase" type="number" min="1">
                                        </div>
                                        <div class="form-group">
                                            <span>UNITE : </span><br/><br/>
                                            <select class="form-control" id="cmbUnite" >

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <span>OBSERVATION : </span><br/><br/>
                                            <textarea rows="5" id="inputObservationRedressement" class="form-control" placeholder="Veuillez motiver la raison du redressement ici" >
                                            </textarea>
                                        </div>

                                    </div>

                                    <div class="modal-footer">

                                        <button                    
                                            type="button" 
                                            class="btn btn-danger pull-left" 
                                            id="btnConfirmeRedressement">
                                            <i class="fa fa-check-circle"></i>  
                                            Confirmer redressement;
                                        </button>

                                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </form> 

            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/chooseAB.html" %>
        <%@include file="assets/include/modal/locationBien.html" %>
        <%@include file="assets/include/modal/modalPermutationBien.html" %>
        <%@include file="assets/include/modal/modalAddPeriodeDeclarationAssujettissement.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/lib/js/dknotus-tour.min.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/gestionBien3.js"></script>
        <script type="text/javascript" src="assets/js/exoneration.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script type="text/javascript" src="assets/js/locationBien.js"></script>
        <script type="text/javascript" src="assets/js/gestionBien/permutationBien.js"></script>
    </body>

</html>
