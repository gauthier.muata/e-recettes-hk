<%-- 
    Document   : gestionArticleBudgetaire
    Created on : 31 janv. 2020, 08:50:27
    Author     : juslin.tshiamua
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Edition des articles budgétaires</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="row" style="margin-bottom: 5px">
                                <div class="col-lg-12">                         
                                    <label style="font-weight: normal">Champs obligatoire <span style="color:red">(*)</span></label>

                                </div>
                            </div>

                            <div class="form-group">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"> INFORMATIONS DE L'ARTICLE BUDGETAIRE</h4>
                                    </div>
                                </div> 
                                <div class="col-lg-6">

                                    <div class="panel panel-primary">
                                        <div class="panel-wrapper collapse in">

                                            <div class="panel-body" style="height: 460px;">

                                                <div class="form-group">
                                                    <p class="control-label col-sm-5" for="inputCategorieArticle">
                                                        Fait g&eacute;n&eacute;rateur <span style="color:red"> *</span></p>
                                                    <div class="col-sm-6">
                                                        <input  class="form-control" id="inputCategorieArticle" placeholder="Fait g&eacute;n&eacute;rateur"/>
                                                    </div>
                                                    <a class="btn btn-primary" 
                                                       id="BtnCategorieArticle"
                                                       style="width: 5%">
                                                        <i class="fa fa-search"></i> 
                                                    </a>

                                                    <p class="control-label col-sm-6" for="lblService"></p>
                                                    <div class="col-sm-6">
                                                        <p id="lblService" style="font-weight: bold;color: black;font-size: 10px">                                                        
                                                    </div>

                                                </div>

                                                <div class="form-group" style="margin-top: -15px;">
                                                    <p class="control-label col-sm-5" for="inputDocumentOfficiel">
                                                        Document officiel <span style="color:red"> *</span></p>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control" id="inputDocumentOfficiel" placeholder="Document officiel"/>
                                                    </div>
                                                    <a class="btn btn-primary" 
                                                       id="BtnDocumentOfficiel"
                                                       style="width: 5%">
                                                        <i class="fa fa-search"></i> 
                                                    </a>
                                                </div>                                

                                                <div class="form-group">
                                                    <p class="control-label col-sm-5" for="cmbNatureArticleBudgetaire">
                                                        Nature de l'article
                                                        <span style="color:red"> *</span>
                                                    <div class="col-sm-7">
                                                        <select class="form-control" id="cmbNatureArticleBudgetaire"></select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p class="control-label col-sm-5" for="inputIntituleArticle">Intitul&eacute; article
                                                        <span style="color:red"> *</span></p>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="inputIntituleArticle" placeholder="Libell&eacute; article budg&eacute;taire"/>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p class="control-label col-sm-5" for="inputCodeOfficiel">
                                                        Code officiel <span style="color:red"> *</span></p>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="inputCodeOfficiel" placeholder="Code officiel"/>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p class="control-label col-sm-5" for="inputArticleMere">
                                                        Article m&egrave;re</p>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control" id="inputArticleMere" placeholder="Article m&egrave;re"/>
                                                    </div>
                                                    <a class="btn btn-primary" 
                                                       id="BtnArticleMere"
                                                       style="width: 5%">
                                                        <i class="fa fa-search"></i> 
                                                    </a>
                                                </div>

                                                <div class="form-group">
                                                    <p class="control-label col-sm-5" for="inputTarifArticle">
                                                        Tarif article <span style="color:red"> *</span></p>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control" id="inputTarifArticle" placeholder="Tarif article"/>
                                                    </div>
                                                    <a class="btn btn-primary" 
                                                       id="BtnTarifArticle"
                                                       style="width: 5%">
                                                        <i class="fa fa-search"></i> 
                                                    </a>
                                                </div>

                                                <div class="form-group">
                                                    <p class="control-label col-sm-5" for="cmbUniteArticle">
                                                        Unité article <span style="color:red"> *</span>
                                                    <div class="col-sm-6">
                                                        <select class="form-control" id="cmbUniteArticle"></select>
                                                    </div>
                                                    <a class="btn btn-primary" 
                                                       id="BtnUniteArticle"
                                                       style="width: 5%">
                                                        <i class="fa fa-plus"></i> 
                                                    </a>
                                                </div>

                                            </div> 
                                        </div>
                                    </div> 
                                </div> 

                                <div class="col-lg-6">

                                    <div class="panel panel-primary">

                                        <div class="panel-wrapper collapse in">

                                            <div class="panel-body">

                                                <div class="row">

                                                    <div class="col-lg-6">
                                                        <label  for="inputPriseChargeDeclaration" style="font-weight: normal">Date &eacute;ch&eacute;ance l&eacute;gale de d&eacute;claration</label>
                                                        <input type="date" class="form-control" id="inputPriseChargeDeclaration" placeholder="Date &eacute;ch&eacute;ance l&eacute;gale de d&eacute;claration"/>
                                                        <label style="font-size: 10px;font-weight: normal;font-style: italic">Cette date d&eacute;finie &aacute; partir de quelle date, le syst&egrave;me doit pouvoir prendre en compte la date limite de d&eacute;claration.</label>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <label  for="inputPriseChargePenalite" style="font-weight: normal">Date limite paiement</label>
                                                        <input type="date" class="form-control" id="inputPriseChargePenalite" placeholder="P&eacute;riode p&eacute;nalitp&eacute;"/>
                                                        <label style="font-size: 10px;font-weight: normal;font-style: italic">Cette date d&eacute;finie &aacute; partir de quelle date, le syst&egrave;me doit pouvoir prendre en compte l'application la fin de paiement.</label>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-lg-6" style="margin-top: 10px">
                                                        <label  for="inputNbrJourLimite" style="font-weight: normal">Nombre de jour limite</label>
                                                        <input type="text" class="form-control" id="inputNbrJourLimite" placeholder="Nbr jour limite."/>
                                                        <label style="font-size: 10px;font-weight: normal;font-style: italic">D&eacute;termine le nombre de jour pour le contr&ocirc;le des d&eacute;clarations. Ex: D&eacute;claration ne doit pas d&eacute;passer chaque 10 jours apr&egrave;s le d&eacute;but d'une p&eacute;riode.</label>
                                                    </div>

                                                    <div class="col-lg-6" style="margin-top: 10px;">
                                                        <label  for="cmbPeriodicite" >P&eacute;riodicit&eacute;
                                                            <span style="color:red"> *</span></label>
                                                        <select class="form-control" id="cmbPeriodicite"></select>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-lg-6" style="margin-top: 10px;">
                                                        <label  for="checkBoxPeriodeVariable"></label>
                                                        <input type="checkbox" id="checkBoxPeriodeVariable" name="checkBoxPeriodeVariable"/>
                                                        <label>P&eacute;riode variable</label><b/>
                                                        <label style="font-size: 10px;font-weight: normal;font-style: italic">Cette option permet de donner la possibilt&eacute; au moment de l'assujettissement de pouvoir choisir une p&eacute;riodicit&eacute; qui sera appliqu&eacute;e &aacute; l'article budg&eacute;taire.</label>
                                                    </div>

                                                    <div class="col-lg-6" style="margin-top: 10px;">
                                                        <label  for="checkBoxProprietaire"></label>
                                                        <input type="checkbox" id="checkBoxProprietaire" name="checkBoxProprietaire"/>
                                                        <label>Propri&eacute;taire</label><b/>
                                                        <label style="font-size: 10px;;font-weight: normal;font-style: italic">L'article est visible &aacute; l'assujettissement, seulement pour le propri&eacute;taire de bien</label>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-lg-6" style="margin-top: 10px;">
                                                        <label  for="checkBoxTauxtransactionnel"></label>
                                                        <input type="checkbox" id="checkBoxTauxtransactionnel" name="checkBoxTauxtransactionnel"/>
                                                        <label>Taux transactionnel</label> <b/>
                                                        <label style="font-size: 10px;font-weight: normal;font-style: italic">En cochant cette option, l'article est d&eacute;clar&eacute; comme transactionnel, c'est-&aacute;-dire que le taux est n&eacute;gociable en respectant les bornes maximale et minimale. </label>
                                                    </div>

                                                    <div class="col-lg-6" style="margin-top: 10px;">
                                                        <label  for="checkBoxQteVariable"></label>
                                                        <input type="checkbox" id="checkBoxQteVariable" name="checkBoxQteVariable"/>
                                                        <label>Quantit&eacute; variable</label><b/>
                                                        <label style="font-size: 10px;font-weight: normal;font-style: italic">Donne la possibilit&eacute; de modifier la quantit&eacute; au moment de l'assujettissement.</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6" style="margin-top: 10px;">
                                                        <label  for="checkBoxAssujettissable"></label>
                                                        <input type="checkbox" id="checkBoxAssujettissable" name="checkBoxAssujettissable"/>
                                                        <label>L'article est assujettissable.</label>
                                                        <label style="font-size: 10px;font-weight: normal;font-style: italic">En cochant cette option, l'article est d&eacute;clar&eacute; sera conséderer un imp&ocirc;t. </label>
                                                    </div>
                                                    
                                                </div> 
                                            </div> 
                                        </div> 
                                    </div> 
                                </div> 

                                <div class="col-lg-12"> 
                                    <div class="panel panel-primary">
                                        <div class="panel-wrapper collapse in">

                                            <div class="panel-body">
                                                <div class="row" style="padding: 5px 0;">

                                                    <div class="col-lg-3">
                                                        <label  for="cmbFormeJuridique" style="font-weight: normal">Type personne <span style="color:red"> *</span></label>
                                                        <select class="form-control" id="cmbFormeJuridique"></select>
                                                    </div>

                                                    <div class="col-lg-2">
                                                        <label  for="inputTaux" style="font-weight: normal">Taux <span style="color:red"> *</span></label>
                                                        <input type="number" class="form-control" id="inputTaux" placeholder="Taux"/>
                                                    </div>

                                                    <div class="col-lg-1">
                                                        <label  for="cmbTypeTaux" style="font-weight: normal">Type taux</label>                                                    
                                                        <select class="form-control" id="cmbTypeTaux">
                                                            <option value ="1">F</option>
                                                            <option value ="2">%</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <label  for="cmbDeviseDefaut" style="font-weight: normal">Devise par d&eacute;faut 
                                                            <span style="color:red"> *</span></label>
                                                        <select class="form-control" id="cmbDeviseDefaut"></select>
                                                    </div>

                                                    <div class="col-lg-2">
                                                        <label  for="inputBorneMinimale" style="font-weight: normal">Borne minimale</label>
                                                        <input type="number" class="form-control" 
                                                               id="inputBorneMinimale" placeholder="Borne minimale"/>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <label  for="inputBorneMaximale" style="font-weight: normal">Borne maximale</label>
                                                        <input type="number" class="form-control" 
                                                               id="inputBorneMaximale" placeholder="Borne maximale"/>
                                                    </div>

                                                </div>
                                                <div class="row" style="padding: 5px 0;">

                                                    <div class="col-lg-3">
                                                        <label  for="cmbVille" style="font-weight: normal">Ville </label>
                                                        <select class="form-control" id="cmbVille"></select>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label  for="cmbCommune" style="font-weight: normal">Commune </label>
                                                        <select class="form-control" id="cmbCommune"></select>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label style="font-weight: normal">Quartier </label>
                                                        <select class="form-control" id="cmbQuartier"></select>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <label  for="inputTarif" style="font-weight: normal">Tarif <span style="color:red"> *</span></label>

                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="inputTarif" placeholder="Tarif">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-primary" type="button" id="btnTarif"> <i class="fa fa-search"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>                                               



                                                </div>


                                                <div class="row" style="padding: 2px 0;">

                                                    <div class="col-lg-3">
                                                        <label  for="cmbUnite" style="font-weight: normal">Unit&eacute; <span style="color:red"> *</span></label>
                                                        <select class="form-control" id="cmbUnite"></select>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <label style="font-weight: normal">Nature/Type bien </label>
                                                        <select class="form-control" id="cmbNatureBien"></select>
                                                    </div>

                                                    <div class="col-lg-2" style="margin-top: 20px;">
                                                        <label  for="checkBoxPalier"></label>
                                                        <input type="checkbox" id="checkBoxPalier" name="checkBoxPalier"/>
                                                        <label style="font-weight: normal">Palier</label> <br/>
                                                        <label style="font-size: 10px;font-weight: normal;font-style: italic">L'article budg&eacute;taire a un palier des valeurs sur lequel le taux doit &ecirc;tre puis&eacute; selon sa valeur de base.</label>
                                                    </div>
                                                    <div class="col-lg-2" style="margin-top: 20px;">
                                                        <label  for="checkBoxMultiplierBase"></label>
                                                        <input type="checkbox" id="checkBoxMultiplierBase" name="checkBoxMultiplierBase"/>
                                                        <label style="font-weight: normal">Multiplier par valeur de base</label> <br/>
                                                    </div>

                                                    <div class="col-lg-2" style="margin-top: 20px;">
                                                        <label  for="checkBoxBorneSuperInfinie"></label>
                                                        <input type="checkbox" id="checkBoxBorneSuperInfinie" name="checkBoxBorneSuperInfinie"/>
                                                        <label style="font-weight: normal">Borne sup&eacute;r infinie</label>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>                                  
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 " style="margin-top: -0.5%; margin-left:  15px">
                                        <button  
                                            id="btnAjouterPalier"
                                            class="btn btn-success pull-left">
                                            <i class="fa fa-plus-circle"></i>  
                                            Ajouter palier dans le panier
                                        </button>
                                    </div>                      
                                </div>  

                                <div class="row">
                                    <div  class="col-lg-12" style="margin-top: -1%">

                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body" >
                                                <table width="100%" 
                                                       class="table table-responsive table-bordered table-hover" 
                                                       id="tablePalier" style="font-weight: normal">
                                                </table>                             
                                            </div>

                                            <div class="panel-footer" style="margin-top: 15px">
                                                <div class="row">
                                                    <div class="col-lg-6 " style="margin-top: 1%">
                                                        <button  
                                                            id="btnEnregistrerArticle"
                                                            class="btn btn-success pull-right">
                                                            <i class="fa fa-save"></i>  
                                                            Enregistrer article budgétaire
                                                        </button>
                                                    </div>                      
                                                </div>                      
                                            </div>                      
                                        </div>                      
                                    </div>  

                                </div>                      
                            </div>

                        </div>
                    </div>

                </form> 

            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/modalCategorieBudgetaire.html" %>
        <%@include file="assets/include/modal/modalDocumentOfficiel.html" %>
        <%@include file="assets/include/modal/modalTarif.html" %>
        <%@include file="assets/include/modal/modalEditerUnite.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script src="assets/js/articleBudgetaire/EditerArticleBudgetaire.js"></script>
        <script type="text/javascript" src="assets/js/articleBudgetaire/gestionArticleBudgetaire.js"></script>
        <script type="text/javascript" src="assets/js/articleBudgetaire/modalSearchArticleGenerique.js"></script>
        <script src="assets/js/articleBudgetaire/modalSearchDocumentOfficiel.js"></script>
        <script src="assets/js/articleBudgetaire/modalTarif.js"></script>
        <script src="assets/js/articleBudgetaire/gestionUnite.js"></script>
    </body>
</html>
