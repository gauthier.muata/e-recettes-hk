<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion exon&eacute;ration vignette</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row" style="margin-left: 5px">
                    <div class="col-lg-10">
                        <label class="control-label" style="font-weight: normal">Contribuable s&eacute;lectionn&eacute; :</label>
                        <input type="text" class="form-control" id="contribuableNameEdit" readonly="true"
                               placeholder="Contribuable s&eacute;lectionn&eacute;">
                    </div>
                    <div class="col-lg-2" style="margin-top: 23px">
                        <button type="submit" class="btn btn-success" id="btnSearchContribuable">
                            Rechercher &nbsp;
                            <i class="fa fa-user"></i>
                        </button>
                    </div>
                </div><hr style="margin-left: 15px"/>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">Informations Exon&eacute;ration</h4> 

                        </div>
                        <div class="panel-body">

                            <div class="row">

                                <div class="col-lg-12">
                                    <label class="control-label" style="font-weight: normal">Type Exon&eacute;ration:</label>
                                    <select class="form-control" id="selectTypExoneration" style="font-weight: bold">
                                        <option value="0">--</option>
                                        <option value="1">EXON&Eacute;RATION PARTIELLE (SEULEMENT POUR LA VIGNETTE)</option>
                                        <option value="2">EXON&Eacute;RATION TOTALE (VIGNETTE & TSCR)</option>
                                    </select>
                                </div>
                            </div><br/>

                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" id="inputExonerationAccorder"  readonly="true"
                                           placeholder="Exon&eacute;ration en cours">
                                </div>
                            </div><br/>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" id="btnSaveExoneration" disabled="true">
                                    <i class="fa fa-check-circle"></i> Accorder Exon&eacute;ration
                                </button>
                                <button type="submit" class="btn btn-danger pull-right" id="btnDeleteExoneration" disabled="true">
                                    <i class="fa fa-trash-o"></i> Supprimer Exon&eacute;ration
                                </button>

                                <button type="submit" class="btn btn-warning pull-left" id="btnTaxerExoneration" disabled="true" style="margin-right: 10px">
                                    <i class="fa fa-calculator"></i> Taxer Exon&eacute;ration
                                </button>
                            </div><hr/>
                            <div class="fonctions">
                                <table id="tableBienExonerationVignette" class="table">

                                </table>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal fade" id="periodeDeclarationModal" role="dialog" data-backdrop="static" data-keyboard="false" >

                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title" id="exampleModalLongTitle">S&eacute;lectionner la p&eacute;riode de d&eacute;claration</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row" style="margin-left: 5px">
                                    <div class="col-lg-12">
                                        <label class="control-label" style="font-weight: normal">Banque :</label>
                                        <select class="form-control" id="selectBanqueExo" style="font-weight: bold">
                                            <option value="0">--</option>
                                        </select>
                                        <br/>
                                    </div>
                                    <br/><br/>
                                    <div class="col-lg-12">
                                         <label class="control-label" style="font-weight: normal">Compte bancaire :</label>
                                        <select class="form-control" id="selectCompteBancaireExo" style="font-weight: bold">
                                            <option value="0">--</option>
                                        </select>
                                    </div>

                                </div> 
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success" id="btnValidateTaxationExo">
                                    <i class="fa fa-check-circle"></i>
                                    Valider taxation Exon&eacute;ration
                                </button>
                                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <%@include file="assets/include/modal/rechercheAssujetti.html" %>
    <%@include file="assets/include/modal/rechercheAdressePersonne.html" %>

    <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/select2.full.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script> 
    <script type="text/javascript" src="assets/js/utils.js"></script>
    <script type="text/javascript" src="assets/lib/choosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
    <script type="text/javascript" src="assets/js/gestionExonerationVignette.js"></script>
    <script type="text/javascript" src="assets/js/rechercheAdressePersonne.js"></script>

    <!--script>
        $(function () {
            $('.select2').select2()
        })
    </script-->


</body>
</html>
