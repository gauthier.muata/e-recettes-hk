<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Retrait de déclaration</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DU CONTRIBUABLE</h4>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body"> 
                                        <span id="lbl1" style="font-size: 16px;">TYPE :</span>
                                        <span  >
                                            <p id="lblLegalForm"
                                               style="font-weight: bold;color: black"></p>
                                        </span><br/>
                                        <span id="lbl2" style="font-size: 16px;">NOMS :</span>
                                        <span  >
                                            <b id="lblNameResponsible" ></b>
                                        </span><br/><br/>
                                        <span id="lbl3" style="font-size: 16px;">ADRESSE :</span>
                                        <span  >
                                            <p id="lblAddress"
                                               style="font-size: 14px;
                                               font-weight: bold;color: black"></p>
                                        </span>
                                    </div>
                                    <div class="panel-footer">
                                        <button  
                                            id="btnCallModalSearchResponsable"
                                            class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                            Rechercher un contribuable
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DE LA DECLARATION
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in">

                                    <div class="panel-body">

                                        <!--div class="row">
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal">Numéro déclaration : </label>
                                            </div>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="inputNumeroDeclaration"/>
                                            </div>
                                            <div class="col-lg-1">
                                                <label style="font-weight: normal">Réquerant : </label>
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control" id="inputRequerant" readonly="true" />
                                            </div>
                                        </div-->                                        
                                        <!--hr/-->
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal"> Imp&oacute;t : </label>
                                            </div>
                                            <div class="col-lg-4">
                                                <select class="form-control" id="cmbArticleBudgetaire" ></select>
                                            </div>
                                            <div class="col-lg-1">
                                                <label style="font-weight: normal"> Devise : </label>
                                            </div>
                                            <div class="col-lg-5">
                                                <select class="form-control" id="cmbDevise" >
                                                    <option value="0">--</option>
                                                    <option value="USD">DOLLARS AMERICAIN</option>
                                                    <option value="CDF">FRANC CONGOLAIS</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal"> Banque : </label>
                                            </div>
                                            <div class="col-lg-4">
                                                <select class="form-control" id="cmbBanque" >
                                                    <option value="0">--</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-1">
                                                <label style="font-weight: normal"> Compte bancaire : </label>
                                            </div>
                                            <div class="col-lg-5">
                                                <select class="form-control" id="cmbCompteBancaire" disabled="true" >
                                                </select>
                                            </div>
                                        </div>   
                                        <hr/>
                                        <table width="100%" 
                                               class="table table-bordered table-hover" 
                                               id="tableBiensAB">
                                        </table>
                                        <hr>
                                        <!--span id="span1" >Date (manuelle) d&eacute;claration</span-->
                                        <!--div class="input-group date" style="width: 20%" id="datePicker1">

                                            <input type="text" class="form-control" id="dateManuelleDeclaration">
                                            <div class="input-group-addon">
                                                <span class="fa fa-th"></span>

                                            </div>
                                        </div-->
                                        <!--label style="font-weight: normal;color:red">
                                            <input type="checkbox"  id="checkApplyDateManuel"/>
                                            Côcher pour appliquer manuellement la date de la déclatation
                                        </label-->

                                        <button  class="btn btn-success" id="btnEnregistrer" style="float: right;margin-top: -35px">
                                            <i class="fa fa-save"></i>  Enregistrer la d&eacute;claration
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
            </div>

        </div>

    </form> 

</div>

</div>

<%@include file="assets/include/modal/header.html" %>
<%@include file="assets/include/modal/rechercheAssujetti.html" %>
<%@include file="assets/include/modal/modalFichePriseEnCharge.html" %>
<%@include file="assets/include/modal/modalReviewAmountPenalite.html" %>
<%@include file="assets/include/modal/modalPeriodeDeclaration.html" %>


<script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
<script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
<script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="assets/lib/choosen/chosen.jquery.js"></script>

<script type="text/javascript" src="assets/js/main.js"></script> 
<script type="text/javascript" src="assets/js/utils.js"></script>
<script type="text/javascript" src="assets/js/retraitDeclaration.js"></script>
<script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
<script type="text/javascript" src="assets/js/ficheEnPriseCharge.js"></script>

</body>

</html>