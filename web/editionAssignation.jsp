<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Edition assignation budgétaire</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> Ministère / Secteur d'activit&eacute; / Fait g&eacute;n&eacute;rateur</h4>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body"> 

                                        <div>
                                            <span id="lbl1" style="font-size: 16px">Minist&egrave;re > </span><br/>
                                            <span id="lblMinistere" style="font-weight: bold"></span><hr style="margin-top: 0px"/>                                          
                                        </div>

                                        <div>
                                            <span id="lbl2" style="font-size: 16px">Secteur d'activit&eacute; > </span><br/>
                                            <span id="lblSecteurDactivite" style="font-weight: bold"></span><hr style="margin-top: 0px"/>  
                                        </div>

                                        <div>
                                            <span id="lbl3" style="font-size: 16px">Fait g&eacute;n&eacute;rateur ></span><br/>
                                            <span id="lblFaitGenerateur" style="font-weight: bold"></span><hr style="margin-top: 0px"/>
                                        </div>

                                    </div>
                                    <div class="panel-footer">
                                        <button class="btn btn-warning" style="visibility: hidden">
                                            <i class="fa fa-search"></i>
                                            Rechercher un service
                                        </button>

                                        <button class="btn btn-primary pull-right" id="btnChargeDonnees">
                                            <i class="fa fa-search"></i>
                                            Charger les données
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> Assignation des montants aux articles budg&eacute;taires
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in">

                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label style="font-weight: normal;font-size: 14px">
                                                            Exercice fiscal
                                                        </label> 
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input class="form-control" id="inputFiscal" disabled="true"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label style="font-weight: normal;font-size: 14px">
                                                            Devise (monnaie)
                                                        </label> 
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input class="form-control" id="inputDevise" disabled="true"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr/>     

                                        <button class="btn btn-warning" id="btnSaveAssignation" style="float: right">
                                            <i class="fa fa-save"></i>  Enregistrer
                                        </button>

                                        <table id="tableArticleBudgetaire" class="table table-bordered" ></table>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </form> 


                <div class="modal fade" id="modalAssignation" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Assignation Budgétaire</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body" style="width: 100%">
                                <form >
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-weight: normal;font-size: 14px">
                                                            Exercice fiscal
                                                        </label> 
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="cmbFiscal"></select>
                                                    </div>                                                                                                   
                                                </div>
                                            </div>                                          
                                            <div class="col-lg-12" style="display: none" id="divMontantGlobal">
                                                <hr/>                                                
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-weight: normal;font-size: 14px">
                                                            Montant total
                                                        </label> 
                                                        <i id="iEdit" class="fa fa-edit" style="float: right;position: fixed;margin-left: 10px;display: none"></i>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input class="form-control" id="inputMontantTotal"/>
                                                    </div>                                                  
                                                </div>
                                            </div>
                                            <div class="col-lg-12" style="display: none" id="divDeviseMonnaie">
                                                <hr/>  
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-weight: normal;font-size: 14px">
                                                            Devise (monnaie)
                                                        </label> 
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="cmbDevise"></select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                           
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <div>
                                    <a id="btnJoindreDocument" class="btn btn-default pull-left" style="color:blue;cursor: pointer">
                                        <i class="fa fa-upload"></i> Joindre le document
                                    </a>
                                </div>
                                <div style="margin-right: 15px">
                                    <button type="button" class="btn btn-primary" id="btnActionDialAssign" style="display: none"> Enregistrer </button> 
                                </div>                              
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAdressePersonne.html" %>
        <%@include file="assets/include/modal/modalMinistereService.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/editionAssignation.js"></script>
        <script type="text/javascript" src="assets/js/rechercheAdressePersonne.js"></script>
        <script type="text/javascript" src="assets/js/uploading.js"></script>   

    </body>
</html>
