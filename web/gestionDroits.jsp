<%-- 
    Document   : gestionDroits
    Created on : 29 janv. 2020, 13:34:39
    Author     : joelkhang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des Droits</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <p class="control-label col-sm-1" for="cmbModule">
                                                Module
                                            <div class="col-sm-2">
                                                <select class="form-control" id="cmbModule"></select>
                                            </div>

                                            <p class="control-label col-sm-1" for="inputCodeDroit">
                                                ID</p>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" id="inputCodeDroit" placeholder="ID"/>
                                            </div>
                                            
                                            <p class="control-label col-sm-1" for="inputIntituleDroit">
                                                Libellé</p>
                                            <div class="col-sm-4">
                                                <textarea rows="3" class="form-control" id="inputIntituleDroit" placeholder="Libellé"></textarea>
                                            </div>


                                            <button class="btn btn-primary" 
                                                    id="btnEnregistrerDroit"
                                                    style="float:right;margin-right: 5px;margin-top: 5px">
                                                <i class="fa fa-save"></i> 
                                                Enregistrer
                                            </button>
                                            <button class="btn btn-warning" 
                                                    id="btnInitDroit"
                                                    style="float:right;margin-right: 5px;margin-top: 5px">
                                                <i class="fa fa-close"></i> 
                                                Nettoyer
                                            </button>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>                                 

                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> Registre des droits utilisateur
                                    </h4>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">

                                        <%--  <div class="form-group">
                                             <p class="control-label col-sm-5" for="inputSearchDroit"></p>
                                             <div class="col-sm-5">
                                                 <input type="text" class="form-control" 
                                                        style="float:right;margin-right: 15px;margin-top: 5px"
                                                        id="inputSearchDroit" placeholder="critère de recherche"/>
                                             </div>


                                                <button class="btn btn-default" 
                                                        id="btnSearchDroit"
                                                        style="float:right;margin-right: 25px;margin-top: 5px">
                                                    <i class="fa fa-search"></i> 
                                                    Recherche
                                                </button>

                                            </div> --%>

                                        <div class="journal" >
                                            <table id="tableEditerDroit" class="table table-bordered">

                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>       
                </form>

            </div>
            <%@include file="assets/include/modal/header.html" %>

            <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
            <script type="text/javascript" src="assets/js/main.js"></script> 
            <script type="text/javascript" src="assets/js/utils.js"></script>
            <script src="assets/js/utilisateur/editerDroit.js"></script>

    </body>
</html>
