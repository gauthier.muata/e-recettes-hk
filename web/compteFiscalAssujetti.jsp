<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Compte fiscal courant assujetti</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            .details-control {
                background: url('assets/images/details_open.png') no-repeat center center;
                cursor: pointer;
            }
            tr.shown td.details-control {
                background: url('assets/images/details_close.png') no-repeat center center;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>
                <div class="row">
                    <div class="col-lg-8">
                        <label style="font-weight: normal"> Nif&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                            <span id="spnNifID" style="font-weight: bold;font-size: 15px">
                            </span>
                        </label><br/>
                        <label style="font-weight: normal"> Noms&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                            <span id="spnNameID" style="font-weight: bold;font-size: 15px">
                            </span>
                        </label><br/>
                        <label style="font-weight: normal"> Adresse&nbsp;&nbsp;:
                            <span id="spnAddressID" style="font-weight: bold;font-size: 15px">
                            </span>
                        </label>
                    </div>

                    <div class="col-lg-4">
                        <label style="font-weight: normal" id="lblModule">
                            <span id="spnValueNumberID" style="font-weight: bold;font-size: 18px">
                            </span>
                        </label><br/>
                        <label style="font-weight: normal"> Crédit impôt en USD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                            <span id="spnCreditUsdID" style="font-weight: bold;font-size: 18px">
                            </span>
                        </label><br/>
                        <label style="font-weight: normal"> Crédit impôt en CDF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                            <span id="spnCreditCdfID" style="font-weight: bold;font-size: 18px">
                            </span>
                        </label>
                    </div>
                </div><hr/>

                <div class="journal" >
                    <table id="tableData" class="table table-bordered">
                    </table>
                </div>
            </div>

        </div>
        <%@include file="assets/include/modal/header.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/repertoire/compteCourantFiscale.js"></script>
    </body>
</html>