<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Nouvel assujettissement v1</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .selectBien,.selectAB{width:85%;float: left}
                .selectABAll{width:100%}
            }
        </style>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">

                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DU CONTRIBUABLE / REDEVABLE & IMPÔT
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal"> Contribuable / Redevable : </label>
                                            </div>
                                            <div class="col-lg-9">
                                                <b id="lblNameResponsible" style="font-size: 16px">
                                                    NOM DU CONTRIBUABLE / REDEVABLE
                                                </b>
                                            </div>
                                            <button  
                                                id="btnCallAllAssuj"
                                                style="display: none"
                                                class="btn btn-warning">
                                                <i class="fa fa-info-circle"></i>
                                            </button>
                                        </div><hr/>
                                        <div id="divBien">
                                            <div class="row" >
                                                <div class="col-lg-2">
                                                    <label style="font-weight: normal">Bien : </label>
                                                </div>
                                                <div class="col-lg-9 selectBien">
                                                    <select class="form-control" id="cmbBien" placeholder="Bien"></select>
                                                </div>
                                                <button  
                                                    id="btnCallAllBien"
                                                    class="btn btn-warning"
                                                    style="display: none">
                                                    <i class="fa fa-info-circle"></i>
                                                </button>
                                                <button  
                                                    id="btnCreateNewBien"
                                                    type="button"
                                                    class="btn btn-warning"
                                                    style="display: none">
                                                    <i class="fa fa-plus-circle"></i> Nouveau Bien
                                                </button>
                    
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2">
                                                </div>
                                                <div class="col-lg-10">
                                                    <label id="lblADressePersonne" style="font-weight: normal;color: #039"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal"> Imp&ocirc;t : </label>
                                            </div>
                                            <div class="col-lg-9 selectAB" id="divAB">
                                                <select class="form-control" id="cmbArticleBudgetaire" ></select>
                                            </div>
                                            <button  id="btnOtherArticleB" class="btn btn-warning">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>   
                                        <hr/>
                                        <div class="row" id="divML">
                                            <div class="col-lg-2" >
                                                <label style="font-weight: normal">Montant Mensuel Loyer : </label>
                                            </div>
                                            <div class="col-lg-7">
                                                <input type="number" value="0" class="form-control" id="inputMontantLoyer" style="font-weight: bold"/>
                                            </div>
                                            <div class="col-lg-2">
                                                <select class="form-control" id="cbDeviseML" style="font-weight: bold">
                                                    <option value="0">-- Devise --</option>
                                                    <option value="USD" style="font-weight: bold">USD</option>
                                                    <option value="CDF" style="font-weight: bold">CDF</option>
                                                </select>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row" id="divMLA">
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal;font-weight: bold">Montant Annuel Loyer : </label>
                                            </div>
                                            <div class="col-lg-8">
                                                <input type="number" value="1" class="form-control" id="inputMontantLoyerAnnuel" style="font-weight: bold"/>
                                            </div>
                                            <div class="col-lg-2">
                                                <select class="form-control" id="cbDeviseMLA" style="font-weight: bold">
                                                    <option value="0">-- Devise --</option>
                                                    <option value="USD" style="font-weight: bold">USD</option>
                                                    <option value="CDF" style="font-weight: bold">CDF</option>
                                                </select>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row" id="divSB">
                                            <div class="col-lg-2">
                                                <label id="lblComplement" style="font-weight: normal">Valeur complement : </label>
                                            </div>
                                            <div class="col-lg-8">
                                                <input type="number" value="1" class="form-control" id="inputSuperficieBatie"/>
                                            </div>
                                            <div class="col-lg-1">
                                                <select class="form-control" id="cbUniteSB">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="panel-footer">   
                                    </div>-->
                                </div>
                            </div>
                        </div>

                    </div>  

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS SUR L'ASSUJETTISSEMENT 
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in" id="divInfoAssujetissements">
                                    <div class="panel-body"> 
                                        <div class="row">
                                            <div class="col-lg-2">
                                            </div>
                                            <div class="col-lg-10">
                                                <label id="lblComplement" style="font-weight: normal;color: red"></label>
                                            </div>
                                        </div>
                                        <div class="row" style="display: none">
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal">Base taxable : </label>
                                            </div>
                                            <div class="col-lg-4">
                                                <input type="number" value="1" class="form-control" id="inputBase"/>
                                            </div>
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal">Unité : </label>
                                            </div>
                                            <div class="col-lg-4">
                                                <input readonly="true" type="text" class="form-control" id="inputUnite"/>
                                            </div>
                                        </div><!--<hr/>-->
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal">Périodicité : </label>
                                            </div>
                                            <div class="col-lg-4">
                                                <select disabled="true" class="form-control" id="cmbPeriodicite" placeholder="Périodicité"></select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal"> Rang/Localité/Catégorie : </label>
                                            </div>
                                            <div class="col-lg-2" id="divCmbTarif">
                                                <select class="form-control" id="cmbTarif" placeholder="Tarif"></select>
                                            </div>
                                            <div class="col-lg-2" id="divInputTaux">
                                                <input type="text" style="color:red" class="form-control" id="inputTaux"/>
                                            </div>
                                        </div> <br/>
                                        <div class="row" id="divPeriodEcheance">
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal" id="lblDebut">Début : </label>
                                            </div>
                                            <div class="col-lg-1" hidden="false">
                                                <input type="number" class="form-control" id="inputJour"/>
                                            </div>
                                            <div class="col-lg-2" id="divCmbMois">
                                                <select class="form-control" id="cmbMois" placeholder="Mois"></select>
                                            </div>
                                            <div class="col-lg-2" id="divCmbAnnee">
                                                <select class="form-control" id="cmbAnnee" placeholder="Année"></select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal"> Echéance déclaration : </label>
                                            </div>
                                            <div class="col-lg-4" id="divContenerEcheanceD">
                                                <input readonly="true" type="text" class="form-control" id="inputEcheance"/>
                                            </div>
                                            <!--div class="col-lg-2" id="divContenerEcheanceP">
                                                <input readonly="true" type="text" class="form-control" id="inputEcheancePaiement"/>
                                            </div-->
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-lg-12">
                                            <table 
                                                width="98%" 
                                                class="table table-bordered table-hover" 
                                                id="tablePeriodes"
                                                style="margin: auto">                                                       
                                            </table>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label id='lblNoteDateExi' style="font-weight: normal;color:red;">Note : Les dates d'écheance en rouge sont depassées.</label>
                                        </div>
                                    </div>
                                    <div class="panel-footer" style="text-align: right">
                                        <button 
                                            id="btnSaveAssujettissement"
                                            class="btn btn-success">
                                            <i class="fa fa-save"></i> Enregistrer
                                        </button>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="modal fade" id="articleAssujettissableModal" role="dialog" data-backdrop="static" data-keyboard="false">
                        <style>
                            @media (max-width: 768px) {
                                #idSearchArticleAssujettissable{overflow: auto}
                            }
                        </style>
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title" id="exampleModalLongTitle">Rechercher et s&eacute;lectionner un article budg&eacute;taire</h5>
                                </div>
                                <div class="modal-body" id="divArticleAssujettissable">
                                    <div class="row" style="margin-left: 0px">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Service :</label>
                                                <select class="form-control" id="selectService" style="width:95%; height: 30px;"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>&nbsp;</label>
                                            <div class="input-group">
                                                <input 
                                                    type="text" class="form-control" 
                                                    id="inputValueResearchArticleAssujettissable" 
                                                    placeholder="Critère de recherche">
                                                <div class="input-group-btn">
                                                    <button 
                                                        class="btn btn-primary" 
                                                        id="btnLauchSearchArticleAssujettissable">
                                                        <i class="fa fa-search"></i> Rechercher
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="idSearchArticleAssujettissable">
                                        <table width="100%" class="table table-bordered table-hover" 
                                               id="tableSearchArticleAssujettissable">
                                        </table>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-success" 
                                            id="btnSelectArticleAssujettissable"
                                            style="display: none">
                                        <i class="fa fa-check-circle"></i> &nbsp;
                                        Valider</button>
                                    <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="infosBaseCalculModal" 
                         role="dialog"
                         data-backdrop="static" data-keyboard="false" >
                        <div class="modal-dialog modal-dialog-centered" style="margin-top: 100px">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title" 
                                        id="exampleModalLongTitle">Fournir des informations</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <label class="control-label col-sm-12" >Veuillez fournir la valeur &nbsp;<span style="font-weight: lighter;color: #08C" id="lblUniteBase"></span> : </label>
                                        <div class="col-sm-12">
                                            <input class="form-control" id="inputModalBaseCalcul" 
                                                   placeholder="Valeur"
                                                   type="number" min="1" value="1">
                                            </input>
                                        </div>
                                        <p id="lblExampleBase"
                                           style="color: red;margin-left: 3px;font-size: 13px"
                                           class="control-label col-sm-12" >

                                        </p>
                                    </div> 
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-success" id="btnValidateInfoBase">
                                        <i class="fa fa-check-circle"></i>
                                        Valider
                                    </button>
                                    <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="editEcheancesLegalesModal" 
                         role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title" id="echeanceModalTitle">Edition de l'échéance légale</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="row" style="width: 125px">                                      
                                        <div class="col-lg-12">
                                            <label >Jour :</label>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="number" min="1" max="31" class="form-control" id="inputDayEcheance"/>
                                            <br/>
                                        </div>

                                        <div class="col-lg-12">
                                            <label id="lblMonthEcheance">Mois :</label>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="number" min="1" max="12" class="form-control" id="inputMonthEcheance"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <label class="pull-left" style="font-weight: normal">De chaque période</label>
                                    <button class="btn btn-success" id="btnValidateEcheance"> <i class="fa fa-check-circle"></i> &nbsp; Valider</button>
                                    <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form> 

            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/lib/js/dknotus-tour.min.js"></script>
        <script src="assets/lib/choosen/chosen.jquery.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/assujettissement_1.js"></script>


    </body>

</html>
