<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Registre des apurements rejetés</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            #ResearchValue{width:300px}
            @media (max-width: 768px) {
                #spanSearch{display: none}
                #ResearchValue{width:100%}
                #divInputGroupSearch{margin-top: 2px}
                .journal{overflow: auto}
                .btnPerso{background-color: transparent;color: blue;cursor: pointer;border-color: transparent;text-decoration: underline}
                .btnPerso:focus{background-color: transparent;border-color: transparent}
            }
        </style>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="ResearchType">
                                <option value="1" >Assujetti</option>
                                <option value="2">Référence document</option>
                            </select>
                            <div class="input-group" id="divInputGroupSearch">
                                <input type="text" class="form-control" id="ResearchValue" placeholder="Le nom de l'assujetti">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"id="btnSimpleSearch">
                                        <i class="fa fa-search"></i> <span id="spanSearch">Rechercher</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnShowAdvancedSerachModal" style="margin-right: 20px" class="btn btn-warning pull-right btnPerso">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée</button>
                </div>

                <hr/>

                <div class="journal" >
                    <table id="tableJournal" class="table table-bordered">

                    </table>
                </div>


            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/modalObservationApurement.html" %>

        <%@include file="assets/include/modal/rechercheAvanceePaiement.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script src="assets/js/rechercheAvancee.js" type="text/javascript"></script>
        <script src="assets/js/paiement.js" type="text/javascript"></script>
        <script src="assets/js/registreApurementRejete.js" type="text/javascript"></script>

    </body>
</html>
