<%-- 
    Document   : gestionCleRepartion
    Created on : 31 janv. 2020, 12:24:32
    Author     : enoch.bukasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des paiements</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>


                <div class="row">
                    <div class="panel panel-success">
                        <div class="panel-heading" style="display: flex;justify-content: space-between;cursor: pointer">
                            <h4 class="panel-title" style="font-weight: bold">Cr&eacute;er une cl&eacute; de R&eacute;partition</h4>

                        </div>
                    </div>

                    <div class="col-lg-12" id="DivNewFaitGenerateur" style="display: inline">
                        <div class="panel panel-success">


                        </div>      
                    </div>
                </div>

                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="display: flex;justify-content: space-between;cursor: pointer">
                            <h4 class="panel-title">S&eacute;lectionner un Article budg&eacute;taire</h4>
                            <a id="btnmodalcle" type="button" a class="btn btn-primary">Ajouter</a>

                                             

                        </div>

                    </div>

                    <div class="col-lg-12" id="DivNewFaitGenerateur" style="display: none">
                        <div class="panel panel-success">


                        </div>      
                    </div>
                </div>
                </br>
                <div class="navbar-form" role="search">
                    <div class="input-group">
                        <div class="col-lg-6">
                            <select style="width: 100%" class="form-control" id="cmbArticleCleRep">
                            </select>
                        </div>
                                       
                    </div>

                </div>
                <div >
                    <table id="generationactegenerateur" class="table table-bordered">
                    </table>

                    <a id="btnvaliderCleRepartitionArticle" type="button" a class="btn btn-success pull-right">valider</a>
                </div>

            </div>
            <%@include file="assets/include/modal/header.html" %>
            <%@include file="assets/include/modal/cleRepartition.html" %>
            <%@include file="assets/include/modal/header.html" %>
            <%@include file="assets/include/modal/rechercheAvanceePaiement.html" %>

            <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

            <script type="text/javascript" src="assets/js/utils.js"></script>
            <script type="text/javascript" src="assets/js/main.js"></script>
            <script src="assets/js/rechercheAvancee.js" type="text/javascript"></script>
            <script type="text/javascript" src="assets/js/modalclerepartition.js"></script>
            <script src="assets/js/modalSearchArticleBudgetaire.js" type="text/javascript"></script>
            <script src="assets/js/paiement.js" type="text/javascript"></script>
            <script src="assets/lib/choosen/chosen.jquery.js" type="text/javascript"></script>
            <script src="assets/js/articleBudgetaire/gestionCleRepartion.js" type="text/javascript"></script>


    </body>
</html>
