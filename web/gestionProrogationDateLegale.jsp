<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion de la prorogation des dates l&eacute;gales</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">Registre des prorogations des dates légales</h4> 
                            <button type="button" class="btn btn-success btn-add-fonction" id="btnAddProrogation">
                                <i class="fa fa-plus-circle"></i> Ajouter une prorogation
                            </button>
                        </div>
                        <div class="panel-body">

                            <div class="fonctions">
                                <table id="tableProrogation" class="table">

                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="modal fade" id="modalUpdateProrogationDateLegale" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form class="form formulaire-edition-fonction">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Ajouter une prorogation </h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group" >
                                <label style="font-weight: normal">Imp&ocirc;t : </label>
                                <select  class="form-control" id="selectImpot">
                                </select>
                            </div>

                            <div class="form-group">
                                <label style="font-weight: normal">Exercice : </label>
                                <select  class="form-control" id="selectExercice">
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Ech&eacute;ance l&eacute;gale de d&eacute;claration : </label>
                                <input type="text" class="form-control" id="inputEcheanceLegaleDeclaration" placeholder="Saisir l'échéance légale de déclaration">
                            </div>
                            
                            <div class="form-group" >
                                <label style="font-weight: normal">Ech&eacute;ance l&eacute;gale de d&eacute;claration prorogation :</label>
                                <div class="input-group date" style="width: 100%;">
                                    <input type="text" class="form-control" id="inputEcheanceLegaleDeclarationProrogation">
                                    <div class="input-group-addon">
                                        <span class="fa fa-th"></span>
                                    </div>
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Ech&eacute;ance l&eacute;gale de paiement : </label>
                                <input type="text" class="form-control" id="inputEcheanceLegalePaiement" placeholder="Saisir l'échéance légale de paiement">
                            </div>
                            
                            <div class="form-group" >
                                <label style="font-weight: normal">Ech&eacute;ance l&eacute;gale de paiement prorogation :</label>
                                <div class="input-group date" style="width: 100%;">
                                    <input type="text" class="form-control" id="inputEcheanceLegalePaiementProrogation">
                                    <div class="input-group-addon">
                                        <span class="fa fa-th"></span>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label style="font-weight: normal" class="control-label">Motif de la prorogation</label>
                                <textarea rows="5" class="form-control" id="inputMotifProrogation" 
                                          placeholder="Saisir le motif de la prorogation">
                                              
                                </textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-dismiss" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-danger" id="btnDeleteProrogation" style="display: none">Supprimer</button>
                            <button type="submit" class="btn btn-success" id="btnSaveProrogation">Enregistrer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/articleBudgetaire/gestionProrogationEcheanceLegale.js"></script>

    </body>
</html>
