<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Associer le compte bancaire à l'impot</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">Informations des affectations de compte bancaires aux imp&ocirc;ts</h4> 

                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Banque</label>
                                <select class="form-control" id="selectBanque">
                                    <option value="0">--</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Compte bancaire</label>
                                <select class="form-control" id="selectCompteBancaire" disabled="true">
                                    <option value="0">--</option>
                                </select>

                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Imp&ocirc;t</label>
                                <select class="form-control" id="selectImpot" >
                                    <option value="0">--</option>
                                </select>

                            </div><br/>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success" id="btnSave">
                                    <i class="fa fa-save"></i> Enregistrer
                                </button>
                            </div><hr/>

                            <div class="fonctions">
                                <table id="tableData" class="table">

                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>

        <%@include file="assets/include/modal/rechercheAssujetti.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/gestionAssociationAccountAB.js"></script>

    </body>
</html>
