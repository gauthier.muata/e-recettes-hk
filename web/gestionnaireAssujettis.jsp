<%-- 
    Document   : gestionnaireAssujettis
    Created on : 17 mars 2020, 09:48:35
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestionnaire des assujettis</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> Informations du gestionnaire</h4>
                                </div>

                                <div class="panel-wrapper collapse in"> 
                                    <div class="panel-body">
                                        <span id="lbl1" style="font-size: 16px;">NOM :</span>
                                        <span  >
                                            <b id="lblNameGestionnaire" ></b>
                                        </span><br/><br/>
                                        
                                        <span id="lbl2" style="font-size: 16px;">FONCTION :</span>
                                        <span  >
                                            <b id="lblFonctionGestionnaire" ></b>
                                        </span>
                                    </div>                                     
                                </div>  
                                <div class="panel-footer">
                                    <button  
                                        id="btnCallModalSearchGestionnaire"
                                        class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                        Rechercher un gestionnaire
                                    </button>
                                    <button class="btn btn-warning" 
                                            id="btnSelectModalAssujetti"
                                            style="float:right;margin-right: 15px;margin-top: 5px;display: none">
                                        <i class="fa fa-link"></i> 
                                        Sélectionner un assujetti</button>

                                </div>
                            </div>                           
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> Informations de l'assujetti
                                    </h4>
                                </div>

                                <div class="panel-body">                                            
                                    <table id="tableAssujetti" class="table table-bordered" >
                                    </table>

                                </div> 
                                <div class="panel-footer" style="text-align: right">
                                    <button class="btn btn-danger" 
                                            id="btnDesaffecterAssujetti"
                                            style="display: none">
                                        <i class="fa fa-close"></i> 
                                        Désaffecter un assujetti</button>

                                </div>

                            </div>
                        </div>
                    </div>  
                </form>
            </div>             
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/modalSearchGestionnaire.jsp" %>
        <%@include file="assets/include/modal/modalSelectAssujetti.html" %>
        <%@include file="assets/include/modal/rechercheAvanceeAssujetti.html" %>
        
        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 

        <script type="text/javascript" src="assets/js/utilisateur/gestionnaireAssujettis.js"></script>
        <script type="text/javascript" src="assets/js/utilisateur/modalSearchGestionnaire.js"></script>
        <script src="assets/js/utilisateur/modalSelectAssujetti.js" type="text/javascript"></script>
    </body>
</html>
