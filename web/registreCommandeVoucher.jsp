<%-- 
    Document   : registreCommandeVoucher
    Created on : 24 mars 2021, 13:51:26
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Registre des commandes des vouchers</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css" />
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .lblTxtAssuj{text-align: justify;margin-top: 3px}
                .divTableBien,.divTableArticle{overflow: auto}
            }

            .details-control {
                background: url('assets/images/details_open.png') no-repeat center center;
                cursor: pointer;
            }
            tr.shown td.details-control {
                background: url('assets/images/details_close.png') no-repeat center center;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par
                            <select  class="form-control" id="cbxResearchTypeCommandeVoucher">
                                <option value="1" selected>Soci&eacute;t&eacute;</option>
                                <option value="2">R&eacute;f&eacute;rence commande</option>
                            </select>

                            <div class="input-group" id="divInputGroupSearch">
                                <input type="text" class="form-control" id="inputSearchCommandeVoucher" style="width: 400px"
                                       placeholder="Veuillez saisir le nom de la soci&eacute;t&eacute;">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" id="btnSearchCommandeVoucher">
                                        <i class="fa fa-search"></i> <span id="spanSearch">Rechercher</span>
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnShowAdvancedSearchModal" style="margin-right: 20px" class="btn btn-warning pull-right btnPerso">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée</button>                      
                </div>

                <hr/>

                <div class="row" id="isAdvance">
                    <div  class="col-lg-12">
                        <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour :</label>
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <div class="row" style="margin-left: 5px;">
                                    <div class="journal" style="float: left">
                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Axe p&eacute;age : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblAxePeage" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Etat : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblStatePaiement" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black;">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Type : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="lblType" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span id="lbl4" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Du :&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblDateDebut" >&nbsp;</b>
                                        </span>

                                        <span id="lbl5" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Au :&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblDateFin" >&nbsp;</b>
                                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="journal" >
                    <button  class="btn btn-sm btn-primary" id="btnGenereRapportCommandeValider" style="display: none"><i class="fa fa-print"></i>&nbsp;&nbsp;Imprimer le Rapport de commandes validées</button>
                    <button class="btn btn-sm btn-primary" id="btnGenereRapportCommandePayer" style="display: none"><i class="fa fa-print"></i>&nbsp;&nbsp;Imprimer le Rapport de commandes payées</button>

                    <hr/>

                    <table id="tableCommandeVoucher" class="table table-bordered responsive" ></table>
                </div>

            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/modalSelectedCompteBancaire.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>
        <%@include file="assets/include/modal/rechercheAvanceeCommandeVoucher.html" %>
        <%@include file="assets/include/modal/modalDisplayVoucherUsing.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/uploading.js"></script>
        
        <script type="text/javascript" src="assets/js/export/jspdf.min.js"></script>
        <script type="text/javascript" src="assets/js/export/jspdf.plugin.autotable.js"></script>
        
        <script type="text/javascript" src="assets/js/rechercheAvancee.js"></script>
        <script type="text/javascript" src="assets/js/previsionCredit/rechercheAvanceeCommandeVouchers.js"></script>
        <script type="text/javascript" src="assets/js/gestionPeage/registreCommandeVoucher.js"></script>
    </body>
</html>
