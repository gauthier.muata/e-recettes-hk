<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Registre des sous-provision des miniers</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css" />
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            .details-control {
                background: url('assets/images/details_open.png') no-repeat center center;
                cursor: pointer;
            }
            tr.shown td.details-control {
                background: url('assets/images/details_close.png') no-repeat center center;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>
                <div class="row">
                    <button 
                        id="btnCallModalSearchAvanded"
                        style="margin-right: 20px" 
                        class="btn btn-warning pull-right btnPerso">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche par minier
                    </button><br/>

                    <hr/>

                </div>

                <div>
                    <table id="tableSousProvision" class="table table-bordered"></table>
                </div>

                <div class="row">
                    <button 
                        id="btnPrintRapportVoirie"
                        style="margin-left:5px" 
                        class="btn btn-warning pull-right">
                        <i class="fa fa-print"></i> &nbsp;
                        Imprimer le rapport sur la situation journali&egrave; des entreprises (VOIRIE)
                    </button>
                    <button 
                        id="btnPrintRapportConcentre"
                        style="margin-right: 20px" 
                        class="btn btn-warning pull-right">
                        <i class="fa fa-print"></i> &nbsp;
                        Imprimer le rapport sur la situation journali&egrave; des entreprises (CONCENTR&Eacute;E)
                    </button><br/>

                    <hr/>

                </div>
            </div>

        </div>
                
        <%@include file="assets/include/modal/header.html"%>
        <%@include file="assets/include/modal/modalResearchContribuableMinier.html"%>
        <%@include file="assets/include/modal/modalAddNewOrUpdateManifeste.html"%>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        
        <script type="text/javascript" src="assets/js/export/jspdf.min.js"></script>
        <script type="text/javascript" src="assets/js/export/jspdf.plugin.autotable.js"></script>
        
        <script type="text/javascript" src="assets/js/gestionSousprovisionminier.js"></script>
    </body>
</html>