<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Remplacement des biens automobile dans la taxation</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <hr style="margin-left: 15px"/>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">Informations du bien automobile &abreve; remplacer</h4> 

                        </div>
                        <div class="panel-body">


                            <h4 style="font-weight: normal;color: red">
                                <u>
                                    LE BIEN AUTOMOBILE A REMPLACER
                                </u>
                            </h4>

                            <div class="fonctions">
                                <table id="tableBienAutomobileSelected" class="table">

                                </table>
                            </div><hr/>


                            <h4 style="font-weight: bold;color: blue">
                                <u>
                                    LE(S) BIEN(S) AUTOMOBILE(S) NON TAX&Eacute;;(S) 
                                </u>
                            </h4>
                            <hr/>


                            <div class="fonctions">
                                <table id="tableBienAutomobile" class="table">

                                </table>
                            </div>

                        </div>
                    </div>
                </div>



            </div>

        </div>


    </div>


    <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/select2.full.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script> 
    <script type="text/javascript" src="assets/js/utils.js"></script>
    <script type="text/javascript" src="assets/lib/choosen/chosen.jquery.js"></script>

    <script type="text/javascript" src="assets/js/remplacerBienAutomobileTaxation.js"></script>

    <!--script>
        $(function () {
            $('.select2').select2()
        })
    </script-->


</body>
</html>
