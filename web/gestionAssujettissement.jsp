<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Déclaration</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css" />

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> Informations de l'assujetti
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body"> 
                                        <span id="lbl1" style="font-size: 16px;display: none">TYPE :</span>
                                        <span  >
                                            <p id="lblLegalForm"
                                               style="font-weight: bold;color: black"></p>
                                        </span><br/>
                                        <span id="lbl2" style="font-size: 16px;display: none">NOMS :</span>
                                        <span  >
                                            <b id="lblNameResponsible" ></b>
                                        </span><br/><br/>
                                        <span id="lbl3" style="font-size: 16px;display: none">ADRESSE :</span>
                                        <span  >
                                            <p id="lblAddress"
                                               style="font-size: 14px;
                                               font-weight: bold;color: black"></p>
                                        </span>
                                    </div>
                                    <div class="panel-footer">
                                        <button  
                                            id="btnCallModalSearchResponsable"
                                            class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                            Rechercher un assujetti 
                                        </button>
                                        <button class="btn btn-warning" 
                                                id="btnPasserAB2"
                                                style="float:right;margin-right: 15px;margin-top: 5px">
                                            <i class="fa fa-link"></i> 
                                            Passer à l'assujettissement simple</button>
                                        <button class="btn btn-warning" 
                                                id="btnPasserAB21"
                                                style="float:right;margin-right: 15px;margin-top: 5px;display: none">
                                            <i class="fa fa-link"></i> 
                                            Passer à l'assujettissement multiple</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary"> 

                                <div class="panel-heading">
                                    <h4 class="panel-title"> Informations sur les assujettissements 
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in" hidden="true">

                                    <div class="panel-body" hidden="true">

                                        <!--               -->

                                        <ul class="nav nav-tabs">
                                            <li id="tabBiens" class="active"><a data-toggle="tab" href="#idBien">Liste des biens</a></li>
                                            <li id="tabArticles"><a data-toggle="tab" href="#idArticle">Actes g&eacute;n&eacute;rateurs</a></li>
                                        </ul>

                                        <div class="tab-content">

                                            <div id="idBien" class="tab-pane fade in active">
                                                <div>
                                                    <button class="btn btn-success" 
                                                            id="btnPasserAB"
                                                            style="float:right;margin-right: 15px;margin-top: 5px">
                                                        <i class="fa fa-link"></i> 
                                                        Passer à l'assujettissement</button>

                                                    <button class="btn btn-default" 
                                                            id="btnLocateBien"
                                                            style="float:right;margin-right: 5px;margin-top: 5px">
                                                        <i class="fa fa-plus-circle"></i> 
                                                        Louer un bien</button>

                                                    <button class="btn btn-default" 
                                                            id="btnNewBien"
                                                            style="float:right;margin-right: 5px;margin-top: 5px">
                                                        <i class="fa fa-plus-circle"></i> 
                                                        Créer un bien</button>

                                                </div>

                                                <div class="panel-body">                                            
                                                    <table id="tableBiens" class="table table-bordered" >
                                                    </table>
                                                    <label style="font-weight: normal;font-size: 12px">
                                                        Cliquer sur un bien si vous voulez le prendre en compte dans l'assujettissement.
                                                    </label>
                                                </div>                                               

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div id="idArticle" class="panel-body">
                                        <!--Liste des articles budgetaires-->
                                        <table id="tableArticles" class="table table-bordered" >
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="modal fade" id="modalPeriodeDeclaration" data-backdrop="static" data-keyboard="false"
                         tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="margin-top: 100px;width: 80%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Périodes des déclarations</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div style="margin-left: 2%" class="modal-body">
                                    <div class="row">
                                        <div class="table-responsive">
                                            <table
                                                class="table table-bordered table-hover" 
                                                id="tablePeriodeDeclaration">

                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="modalMiseEnQuarantaine" tabindex="-1" role="dialog" 
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true"
                         data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="width: 50%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Mise en quarantaine --> Observation</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">

                                    <div class="panel panel-primary" id="divMiseQuarantaine" >
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">

                                                <form id="formRadioButton"> 
                                                    <label style="font-weight: normal">Période déclaration : </label>&nbsp;&nbsp;<span id="valuePeriode1" style="font-size: 16px;font-weight: bold"></span><br/>
                                                    <label style="font-weight: normal">Par l'agent : </label>&nbsp;&nbsp;<span id="valueAgent1" style="font-size: 16px;font-weight: bold"></span><hr/>
                                                    <label style="font-weight: normal">Observation : </label>
                                                    <div class="form-group">
                                                        <textarea
                                                            rows="10"
                                                            style="margin-left: 10px;width: 850px;"
                                                            id="textaeraObservation">

                                                        </textarea>
                                                    </div> 
                                                </form>
                                            </div>
                                            <div class="panel-footer" style="text-align: right">
                                                <button type="button" class="btn btn-success" id="btnMiseQuarantaine">
                                                    <i class="fa fa-check-circle"></i>
                                                    Mettre en quarantaine
                                                </button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Fermer</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal fade" id="modalInfoMiseEnQuarantaine" tabindex="-1" role="dialog" 
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true"
                         data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="width: 50%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Observation sur la mise en quarantaine</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">

                                    <div class="panel panel-primary" id="divMiseQuarantaine" >
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">

                                                <form id="formRadioButton"> 
                                                    <label style="font-weight: normal">Période déclaration : </label>&nbsp;&nbsp;<span id="valuePeriode2" style="font-size: 16px;font-weight: bold"></span><br/>
                                                    <label style="font-weight: normal">Date mise en quarantaine : </label>&nbsp;&nbsp;<span id="valueDate2" style="font-size: 16px;font-weight: bold"></span><br/>
                                                    <label style="font-weight: normal">Mise en quarantaine par : </label>&nbsp;&nbsp;<span id="valueAgent2" style="font-size: 16px;font-weight: bold"></span><hr/>
                                                    <label style="font-weight: normal">Observation : </label>
                                                    <div class="form-group">
                                                        <textarea
                                                            rows="10"
                                                            readonly="false"
                                                            style="weidht:30%;margin-left: 10px;width: 850px;"
                                                            id="valueObservation">

                                                        </textarea>
                                                    </div> 
                                                </form>
                                            </div>
                                            <div class="panel-footer" style="text-align: right">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Fermer</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </form> 

            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/locationBien.html" %>
        <%@include file="assets/include/modal/noteTaxationDeclaration.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/lib/js/dknotus-tour.min.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/taxationAssujettissement.js"></script>
        <script type="text/javascript" src="assets/js/gestionBien.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script type="text/javascript" src="assets/js/locationBien.js"></script>
        <script src="assets/js/uploading.js" type="text/javascript"></script>

    </body>

</html>