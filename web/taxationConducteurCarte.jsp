<%-- 
    Document   : taxation
    Created on : 16 juin 2018, 08:03:05
    Author     : gauthier.muata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Taxation ordinaire</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title"> Informations de l'assujetti (Etape 1)
                                </h4>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body"> 
                                    <span id="lbl1" style="font-size: 16px;display: none">Type assujetti :</span>
                                    <span >
                                        <p id="lblLegalForm"
                                           style="font-weight: bold;color: black">&nbsp;</p>
                                    </span><br/>
                                    <span id="lbl2" style="font-size: 16px;display: none">Nif :</span>
                                    <span >
                                        <p id="lblNifResponsible"
                                           style="font-weight: bold;color: black">&nbsp;</p>
                                    </span><br/>
                                    <span id="lbl3" style="font-size: 16px;display: none">Noms :</span>
                                    <span  >
                                        <b id="lblNameResponsible" >&nbsp;</b>
                                    </span><br/><br/>
                                    <span id="lbl4" style="font-size: 15px;display: none">Adresse :</span>
                                    <span  >
                                        <p id="lblAddress"
                                           style="font-size: 12px;
                                           font-weight: bold;color: black">&nbsp;</p>
                                    </span>
                                    <br/><br/>
                                </div>
                                <div class="panel-footer">
                                    <button  
                                        id="btnCallModalSearchResponsable"
                                        class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                        Rechercher un assujetti
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title"> Informations sur la moto (Etape 2)
                                </h4>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body"> 
                                    <span  id="lblMarque1" style="font-size: 16px;display: none">Marque :</span>
                                    <span >
                                        <p id="lblMarque"
                                           style="font-weight: bold;color: black">&nbsp;</p>
                                    </span><br/>
                                    <span id="lblPlaque1" style="font-size: 16px;display: none">N° Plaque :</span>
                                    <span >
                                        <p id="lblPlaque"
                                           style="font-weight: bold;color: black">&nbsp;</p>
                                    </span><br/>
                                    <span id="lblDescription1" style="font-size: 13px;display: none">Description :</span>
                                    <span  >
                                        <b id="lblDescription" >&nbsp;</b>
                                    </span><br/><br/>
                                    <span  id="lblEtat1"style="font-size: 16px;display: none">Etat :</span>
                                    <span  >
                                        <p id="lblEtat"
                                           style="font-size: 16px;
                                           font-weight: bold;color: black">&nbsp;</p>
                                    </span>
                                    <br/><br/>
                                </div>
                                <div class="panel-footer">
                                    <button  
                                        id="btnCallModalSearchMoto"
                                        class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                        Rechercher une moto
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div  class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title"> Article(s) budgétaire taxé(s) (Etape 3)
                                </h4>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body" >
                                    <table width="100%" 
                                           class="table table-responsive table-bordered table-hover" 
                                           id="tableTaxation">
                                    </table>
                                </div>
                                <div class="panel-footer" style="margin-top: 15px">
                                    <div class="row">
                                        <div class="col-lg-6 pull-left" style="margin-top: 2%">
                                            <b  
                                                for="labelTotalAmount">Montant total Dû : &nbsp; <span 
                                                    id="labelTotalAmount"
                                                    style="color: red;font-size: 22px" ></span>
                                            </b>
                                        </div>
                                        <div class="col-lg-6 " style="margin-top: 1%">
                                            <button  
                                                id="btnSaveTaxation"
                                                class="btn btn-success pull-right">
                                                <i class="fa fa-save"></i>  
                                                Enregistrer la taxation
                                            </button>
                                            <button  
                                                style="margin-right: 25px;display: none"
                                                id="btnAssociateBien"
                                                class="btn btn-warning pull-right">
                                                <i class="fa fa-save"></i>  
                                                Associer un bien
                                            </button>
                                            <span id="lblZoneBien">
                                                <span  style="font-size: 16px;">Bien sélectionné :</span>
                                                <span>
                                                    <p id="lblSelectedBien"
                                                       style="font-weight: bold;color: red;font-size: 20px">&nbsp;</p>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="budgetArticleModal" 
             role="dialog"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" style="width: 865px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Registre des articles budgétaires</h5>
                    </div>
                    <div class="modal-body" id="divBudgetArticle">
                        <div class="navbar-form" role="search">
                            <div class="input-group">
                                <input 
                                    type="text" class="form-control" 
                                    id="inputValueResearchBudgetArticle" 
                                    placeholder="Critère de recherche">
                                <div class="input-group-btn">
                                    <button 
                                        class="btn btn-primary" 
                                        id="btnResearchBudgetArticle">
                                        <i class="fa fa-search"></i>
                                        Rechercher
                                    </button>
                                </div>
                            </div>
                        </div>
                        <table width="100%" 
                               class="table table-bordered table-hover" 
                               id="tableBudgetArticles">
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" 
                                id="btnSelectedBudgetArticle"
                                style="display: none">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="motoCycleModal" 
             role="dialog"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" style="width: 865px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Registre des motos</h5>
                    </div>
                    <div class="modal-body" id="divMotoCycle">

                        <table width="100%" 
                               class="table table-bordered table-hover" 
                               id="tableMotoCycle">
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" 
                                id="btnSelectedMoto"
                                style="display: none">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="tarifBudgetArticleModal" 
             role="dialog"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" style="width: 865px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Sélection du tarif de l'article budgétaire</h5>
                    </div>
                    <div class="modal-body" id="divTarifBudgetArticle">
                        <div class="row">
                            <label class="control-label col-sm-12" >Veuillez sélectionner le tarif : </label>
                            <div class="col-sm-12">
                                <select class="form-control" id="cmbTarif" 
                                        placeholder="Tarif de l'article budgétaire">
                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnSelectedTarif">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="typeTaxationModal" 
             role="dialog"
             data-backdrop="static" 
             data-keyboard="false" 
             aria-hidden="false">

            <div class="modal-dialog modal-dialog-centered" style="width: 865px;margin-top: 200px" role="document">>
                <div class="modal-content">
                    <div class="modal-header">
                        <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Que voulez-vous faire ? </h5>
                    </div>
                    <div class="modal-body" id="divTarifBudgetArticle">
                        <div class="row">
                            <label class="control-label col-sm-12" >Veuillez sélectionner le type de taxation : </label>
                            <div class="col-sm-12">
                                <select class="form-control" id="cmbTypeTaxation" 
                                        placeholder="Type de taxation">

                                    <option value="0">--</option>
                                    <option value="1">Taxer seulement pour la carte professionnelle de conducteur</option>
                                    <option value="2">Taxer seulement pour l'autocollant et l'autorisation de transport</option>
                                    <option value="3">Taxer pour la carte professionnelle, l'autocollant et l'autorisation de transport</option>
                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnSelectedTypeTaxation">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="infosBaseCalculModal" 
             role="dialog"
             data-backdrop="static" data-keyboard="false" >
            <div class="modal-dialog modal-dialog-centered" style="margin-top: 100px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Informations de la borne de calcul</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="control-label col-sm-12" >Veuillez fournir la valeur de la base en &nbsp;<span style="font-weight: lighter;color: #08C" id="lblUniteBase2"></span> : </label>
                            <div class="col-sm-12">
                                <input class="form-control" id="inputBaseCalculOnCasePourcent" 
                                       placeholder="Base de calcul"
                                       type="number" min="1" value="1">
                                </input>
                            </div>
                            <p id="lblExampleBase"
                               style="color: red;margin-left: 3px;font-size: 13px"
                               class="control-label col-sm-12" >

                            </p>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnValidateInfoBase">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalTransactionnel" 
             role="dialog"
             data-backdrop="static" data-keyboard="false" >
            <div class="modal-dialog modal-dialog-centered" style="margin-top: 100px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Informations de l'amende transactionnel</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="control-label col-sm-12" >Veuillez fournir le montant d&ucirc;  : </label>

                            <div class="col-sm-12" style="margin-top: 10px">
                                <input class="form-control" id="inputMontanTransactionnel" 
                                       placeholder="Montant dû"
                                       type="number" min="1" value="1">
                                </input>
                            </div>
                            <p id="lblTransactionnelValue"
                               style="color: red;margin-left: 3px;margin-top: 15px;font-size: 15px"
                               class="control-label col-sm-12" >
                                Le montant d&ucirc; doit être compris entre 2000 et 3000 (Devise)
                            </p>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnValidateTTransactionnel">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>        

<%@include file="assets/include/modal/header.html" %>
<%@include file="assets/include/modal/rechercheAssujetti.html" %>
<%@include file="assets/include/modal/rechercheBienPersonne.html" %>

<script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
<script src="assets/lib/js/datatables.min.js" type="text/javascript"></script>
<script src="assets/lib/js/dataTables.select.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="assets/js/utils.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/taxationCarteConducteur.js"></script>
<script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
<script type="text/javascript" src="assets/js/rechercheBienPersonne.js"></script>
</body>
</html>
