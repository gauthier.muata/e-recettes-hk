<%-- 
    Document   : carteNFCPrint
    Created on : 6 mai 2021, 09:37:53
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
    <head>
        <title>html2pdf Test - Template</title>
        <link rel="stylesheet" href="assets/lib/css/style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style type="text/css"> /* Basic styling for root. #root{width: 500px; height: 700px; background-color: yellow;}*/ </style>
    </head>
    <body>
        <button onclick="test()">Generer le PDF</button>
        <div id="root">
            <div class="container" id="root2">
                <div class="row justify-content-center">
                    <div class="box-card">
                        <div class="carte">
                            <div class="col-12 head">
                                <div class="container-head">
                                    <div class="head-text">
                                        <h1 class="h4 text-center" style="margin-top:9px; font-size: 7pt; font-weight: bold;text-decoration: underline;">
                                            <span>REPUBLIQUE DEMOCRATIQUE DU CONGO</span>
                                        </h1>
                                        <h3 class=" text-center" style="font-size: 7px; color: red;">CARTE NFC VOUCHER
                                        </h3>
                                    </div>
                                    <div class="logo-hotel">
                                        <div class="logo">
                                            <img id="" width="100%" src="<a name="logoKatanga"/>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 content">
                                <div class="contenu">
									<p style="font-size: 12px;"> <a name="nomSociete"/> </p><br/>
                                   <p style="font-size: 18px; color: blue; font-weight: bold;"> <a name="numCarteNFC"/> </p>
                                </div>
                            </div>
                            <div class="col-12 row footer">
                                <div class="col text-left" style="font-weight: 600;">
                                    <!--p style="font-size: 5pt; margin-bottom: 2px;"><a name="district"/></p-->
                                    <p style="font-size: 5pt; margin-bottom: 2px;">Axe de <a name="nomAxe"/></p>
                                </div>
                                <div class="col text-right">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container" id="root2">
                <div class="row justify-content-center">
                    <div class="box-card">
                        <div class="carte">
                            <div class="col-12 head">
                                <div class="container-head">
                                    <div class="head-text">
                                        <h3 class="title text-center" style="font-size: 8pt;">CARTE NFC VOUCHER</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 content">
                                <div class="row" style="width: 100%;">
                                    <div class="col-8" style="font-size: 6pt; margin-top: 20px; margin-left: 20px;align-items: stretch;">
                                        
                                        <p id="lbl3" style="margin-bottom: 10px;">Date de livraison : <span><b id="dateLivrais"><a name="datelivraison"/></b></span></p>
                                        <p id="lbl3" style="margin-bottom: 10px;">Date de fin de validité: <span><b id="lblNameResponsible"><a name="datefinvalidite"/></b></span></p>
                                    </div>
                                    <div class=" col-3 qr-code" style="margin-top: 10px;">
                                        <img src="<a name="qrcode"/>" alt="code" width="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 row footer">
                                <div class="col text-center">
                                    <p style="font-size: 7pt;">En utilisant cette carte, le titulaire s'engage à tous les termes dans lequels elle a été delivrée.
									Cette carte est delivrée par Hologram.</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="assets/lib/js/html2pdf.bundle.js" type="text/javascript"></script>
        <script>function test() {
                var element = document.getElementById('root');
                html2pdf().from(element).set({margin: 0, filename: 'carte.pdf', html2canvas: {scale: 2}, jsPDF: {orientation: 'landscape', unit: 'pt', format: [241, 153], compressPDF: false}}).save();
            }</script>
    </body>
</html>
