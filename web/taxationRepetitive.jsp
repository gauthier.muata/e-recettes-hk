<%-- 
    Document   : taxationRepetitive
    Created on : 9 juil. 2018, 12:39:16
    Author     : gauthier.muata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Déclaration</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css" />
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
               #divTablePeriodeDeclaration{overflow: auto} 
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-primary">

                            <div class="panel-heading">
                                <h4 class="panel-title"> Informations de l'assujetti
                                </h4>
                            </div>

                            <div class="panel-wrapper collapse in">
                                <div class="panel-body"> 
                                    <span style="font-size: 16px">TYPE :</span>
                                    <span  >
                                        <p id="lblLegalForm"
                                           style="font-weight: bold;color: black"></p>
                                    </span><br/>
                                    <span style="font-size: 16px">NOMS :</span>
                                    <span  >
                                        <b id="lblNameResponsible" ></b>
                                    </span><br/><br/>
                                    <span style="font-size: 16px">ADRESSE :</span>
                                    <span  >
                                        <p id="lblAddress"
                                           style="font-size: 13px;
                                           font-weight: bold;color: black"></p>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-primary">

                            <div class="panel-heading">
                                <h4 class="panel-title"> Informations de l'article budg&eacute;taire
                                </h4>
                            </div>

                            <div class="panel-wrapper collapse in">
                                <div class="panel-body"> 
                                    <span >
                                        <p id="lblArticleBudgetaire"
                                           style="font-weight: bold;color: black;font-size: 14px"></p>
                                    </span><br/>
                                    <span style="font-size: 16px">Périodicité :</span>
                                    <span  >
                                        <b id="lblPeriodiciteName" ></b>
                                    </span><br/><br/>
                                    <span style="font-size: 16px">Taux :</span>
                                    <span  >
                                        <p id="lblInfoArticle"
                                           style="font-size: 14px;
                                           font-weight: bold;color: black"></p>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div><hr style="margin-top: -10px;margin-left: 15px"/><br/>
                    <div class="row">
                        <br/>
                        <div id="divOldNC" class="col-lg-12" style="font-size: 17px;display: none">
                            <span>&nbsp;&nbsp;&nbsp;Ancienne note de taxation : </span>
                            <span><b id="lblAncienneNote" style="color: #00acee;text-decoration: underline;font-style: italic">&nbsp;</b></span>
                            <br/> <br/>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <label id='lblNoteDateExi' style="font-weight: normal;color:red;">Note : Les dates d'écheance en rouge sont depassées.</label>
                            </div>
                            <div class="col-lg-4">
                                <span id="blockCancelPenalite" class="pull-right" style="font-size: 16px" >
                                    <input val="0" id="checkBoxAddPenalite" checked type="checkbox" >
                                    &nbsp; Appliqué les pénalités à la taxation  ?
                                </span>
                            </div>
                        </div>
                        <div id="divTablePeriodeDeclaration">
                            <table width="100%" 
                                   class="table table-responsive table-bordered table-hover" 
                                   id="tablePeriodeDeclaration">
                            </table> 
                        </div>

                    </div>

                </div>
            </div>
        </div>


        <div class="modal fade" id="infosBaseCalculModal" role="dialog" data-backdrop="static" data-keyboard="false" >
            <div class="modal-dialog modal-dialog-centered modal-lg" style="margin-top: 100px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="modalLongBaseTaxableTitle">Base Taxable pour : </h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="control-label col-sm-12" >
                                Veuillez fournir la base taxable en &nbsp;
                                <span style="font-weight: lighter;color: #08C" id="lblUniteBase"></span> : 
                            </label>
                            <div class="col-sm-12">
                                <input class="form-control" id="inputBaseCalculOnCasePourcent" 
                                       placeholder="Base taxable"
                                       type="number" >
                                </input>
                            </div>
                            <p id="lblExampleBase"
                               style="color: red;margin-left: 3px;font-size: 13px"
                               class="control-label col-sm-12" >

                            </p>
                        </div> 


                        <div class="alert alert-info" id="divInfosExo" style="display: none"></div>

                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnValidateInfoBase">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <!--%@include file="assets/include/modal/modalPenalite.html" %-->
        <%@include file="assets/include/modal/modalFichePriseEnCharge.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script src="assets/lib/js/datatables.min.js" type="text/javascript"></script>
        <script src="assets/lib/js/dataTables.select.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>

        <script type="text/javascript" src="assets/js/taxationRepetitive.js"></script>
        <!--script type="text/javascript" src="assets/js/modalAddPenalite.js"></script-->
        <script type="text/javascript" src="assets/js/ficheEnPriseCharge.js"></script>
        <script type="text/javascript" src="assets/js/uploading.js"></script>

    </body>
</html>
