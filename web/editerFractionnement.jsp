<%-- 
    Document   : editerReclamation
    Created on : 20 juin 2019, 11:07:24
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Edition de la demande d'échelonnement</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">

                    <div  class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <span id="lbl1" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;Noms de l'assujetti &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblNameResponsible" >&nbsp;</b>
                                </span>

                                <span id="lbl2" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;Type&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblLegalForm" >&nbsp;</b>
                                </span>

                                <span id="lbl3" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;ID &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblNifResponsible" >&nbsp;</b>
                                </span>
                                <br/>
                                <span id="lbl4" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;Adresse principale &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblAddress" >&nbsp;</b>
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <span style="float: right;">
                                    <button id="btnCallModalSearchResponsable" class="btn btn-primary">
                                        <i class="fa fa-search"></i> 
                                        Rechercher un assujetti
                                    </button>&nbsp;&nbsp;&nbsp;&nbsp;
                                </span>
                                <br/><br/>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <h4 class="panel-title"> INFORMATIONS DE LA DEMANDE D'ECHELONNEMENT
                                </h4>
                            </div>

                            <div class="panel-wrapper collapse in">
                                <div class="panel-body"> 

                                    <div class="row" id="divRecAddDoc">
                                        <div class="col-lg-1">
                                            <label style="font-weight: normal"> N° titre de perception</label>
                                        </div>
                                        <div class="col-lg-4">
                                            <input class="form-control" id="inputNumeroDocument" >
                                        </div>
                                        <div class="col-lg-1">
                                            <label style="font-weight: normal"> Type document <span style="color: red"> *</span></label>
                                        </div>
                                        <div class="col-lg-3">
                                            <select class="form-control" id="cmbTypeDocument" >
                                                <option value ="0">--</option>>
                                                <option value ="NT">NOTE DE TAXATION</option>
                                                <option value ="NP">NOTE DE PERCEPTION</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
                                            <button  style="width: 100%"
                                                     id="btnSearchAndAddDocument"
                                                     class="btn btn-warning">
                                                <i class="fa fa-plus-circle"></i>
                                                Rechercher / ajouter un titre
                                            </button>
                                        </div>

                                    </div>

                                    <br/>

                                    <div class="row" id="divRecAddDoc">
                                        <div class="col-lg-1">
                                            Référence de la lettre de demande <span style="color: red"> *</span>
                                        </div>
                                        <div class="col-lg-4">
                                            <input class="form-control" id="textReferenceCourrier" >
                                        </div>
                                        <div class="col-lg-1">
                                            <label style="font-weight: normal">A l'attention du chef de <span style="color: red"> *</span></label>
                                        </div>
                                        <div class="col-lg-6">
                                            <select class="form-control" id="cmbDivision" style="width: 100%">
                                            </select>
                                        </div><br/><br/>
                                        <div class="col-lg-6">
                                           <label style="font-weight: normal;color:red;font-style: italic">
                                                <input type="checkbox" id="checkApplyTenPercent" name="checkApplyTenPercent"/>
                                                Appliquer le 10% d'int&eacute;r&ecirc;t moratoire sur la premi&egrave;re note de d'echelonnement
                                            </label>
                                        </div>
                                     

                                    </div>

                                    <div id="divEntiteTableRefObs">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-bordered table-hover" 
                                                       id="tableDemandeEchelonnement"
                                                       style="margin: auto">                                                       
                                                </table>
                                            </div>
                                            <br/>
                                            <div class="col-lg-12">
                                                <label id='lblNoteDateExi' style="font-weight: normal;color:red;display: none">Note : Les dates d'exigibilité en rouge sont depassées.</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <br/>
                                            <div class="col-lg-12">
                                                <label for="textObservation" style="font-weight: normal">Observation</label>
                                                <textarea id="textObservation" rows="5" style="width: 100%"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="panel-footer" style="text-align: right">
                                    <button                    
                                        type="button" 
                                        class="btn btn-warning pull-left" 
                                        id="btnJoindreArchiveEchelonnement">
                                        <i class="fa fa-download"></i>  
                                        Joindre les pi&egrave;ces jointes
                                    </button>
                                    <a 
                                        id="lblNbDocumentEchelonnement"
                                        class="pull-left"
                                        style="font-style: italic;margin-top: 8px;margin-left: 8px;text-decoration: underline;color: blue">
                                    </a>
                                    <button 
                                        id="btnSaveDemandeEchelonnement"
                                        class="btn btn-success">
                                        <i class="fa fa-save"></i> Enregistrer la demande
                                    </button>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/rechercheAdressePersonne.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/lib/js/dknotus-tour.min.js"></script>
        <script  type="text/javascript" src="assets/js/uploading.js"></script>
        <script type="text/javascript" src="assets/js/editerDemandeFractionnement.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
    </body>
</html>
