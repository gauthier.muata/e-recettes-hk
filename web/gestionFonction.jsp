<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des Fonctions</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>
                
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">REGISTRE DES FONCTIONS</h4> 
                            <button type="button" class="btn btn-success" id="btnAddFonction">
                                <i class="fa fa-plus-circle"></i> Ajouter une fonction 
                            </button>
                        </div>
                        <div class="panel-body">
                            <div class="">
                                <div class="formulaire-edition">
                                    
                                </div>
                            </div>
                            <div class="fonctions">
                                <table id="tableFonction" class="table">
                                    <thead>
                                        <tr>
                                            <th>
                                                FONCTION
                                            </th>
                                            <th class="text-center">ETAT</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                           
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
        
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form class="form formulaire-edition-fonction">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel"> Ajouter une fonction </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="intitule" class="control-label" style="font-weight: normal">INTITULE FONCTION</label>
                                <input type="text" class="form-control" name="intitule" id="intitule" placeholder="Taper l'intitulé">
                            </div>
                            <input type="hidden" class="form-control" name="code" id="code">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default btn-dismiss" data-dismiss="modal">Fermer</button>
                          <button type="submit" class="btn btn-success" id="btnUpdateFonction">Enregistrer</button>
                        </div>
                   </form>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/gestionFonction.js"></script>

    </body>
</html>
