<%-- 
    Document   : gestionDroits
    Created on : 29 janv. 2020, 13:34:39
    Author     : joelkhang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des Droits supplémentaires des utilisateurs</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">

                                        <div class="form-group">

                                            <p class="control-label col-sm-1" for="cmbBureau">
                                                Bureau/Site
                                            <div class="col-sm-2">
                                                <select class="form-control" id="cmbBureau"></select>
                                            </div>

                                            <p class="control-label col-sm-1" for="cmbUsers">
                                                Utilisateurs
                                            <div class="col-sm-4">
                                                <select class="form-control" id="cmbUsers"></select>
                                            </div>

                                            <p class="control-label col-sm-1" for="cmbDroit">
                                                Droits accordés
                                            <div class="col-sm-3">
                                                <select class="form-control" id="cmbDroit"></select>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>                                 

                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> Registre des droits non accordés &abreve; : 

                                        <span style="font-weight: bold" id="spnNameUserSelected"></span>
                                    </h4>
                                </div>
                                <div class="panel-wrapper collapse in">

                                    <button id="btnAddNewDroitUser" style="margin-left: 15px;margin-top: 5px" disabled="true" class="btn btn-success pull-left">
                                        <i class="fa fa-check-circle"></i>&nbsp;
                                        Accorder ce(s)&nbsp;<span style="font-weight: bold;font-size: 16px" id="spnNbreDroit"></span>&nbsp;droit(s) supplémentaires</button>

                                    <div class="panel-body">

                                        <div class="journal" >
                                            <table id="tableDroitAccess" class="table table-bordered">

                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>       
                </form>

            </div>
            <%@include file="assets/include/modal/header.html" %>

            <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
            <script type="text/javascript" src="assets/js/main.js"></script> 
            <script type="text/javascript" src="assets/js/utils.js"></script>
            <script src="assets/js/utilisateur/gestionDroitSuppUser.js"></script>

    </body>
</html>
