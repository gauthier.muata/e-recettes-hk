<%-- 
    Document   : gestionTauxChange
    Created on : 7 janv. 2021, 10:01:21
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des Taux de change</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> Affectation des taux de change</h4>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">

                                        <div class="row">

                                            <div class="col-lg-10">
                                                <div class="row">
                                                    <p class="control-label col-sm-1" for="cmbDevise">
                                                        Devise
                                                    <div class="col-sm-11">
                                                        <select class="form-control" id="cmbDevise"></select>
                                                    </div>
                                                </div>
                                            </div>

                                            <button class="btn btn-default" 
                                                    id="btnEnregistrerTaux"
                                                    style="float:right;margin-right: 45px;margin-top: 2px">
                                                <i class="fa fa-save"></i> 
                                                Enregistrer
                                            </button>
                                        </div>

                                        <hr/> 

                                        <div class="row">

                                            <div class="form-group">

                                                <div class="col-lg-10">
                                                    <div class="row">
                                                        <p class="control-label col-sm-2" for="cmbCorrespondance">
                                                            Correspondance
                                                        <div class="col-sm-10">
                                                            <select class="form-control" id="cmbCorrespondance"></select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2">
                                                    <div class="row">
                                                        <p class="control-label col-sm-2" for="inputTauxChange">
                                                            Taux</p>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="inputTauxChange" placeholder="Taux"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div  class="col-lg-12" style="margin-top: -1%">

                            <div class="panel-wrapper collapse in">
                                <div class="panel-body" >
                                    <div class="journal" >
                                            <table id="tableTauxChange" class="table table-bordered">
                                            </table>
                                        </div>                           
                                </div>

                            </div>                      
                        </div>  

                    </div>
                </form>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>


        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>

        <script type="text/javascript" src="assets/js/gestionBanque/tauxChange.js" ></script>
    </body>
</html>
