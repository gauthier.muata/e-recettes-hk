<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Edition d'un bien concession minière</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .fieldsetAdresse{margin-left: -1px}
                .divTxtAreaControl{width: 80%}               
                .divTxtAreaControl,.divBtnAdresseControl{display: inline-block;vertical-align: top}
            }
        </style>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row" style="margin-bottom: 5px">
                        <div class="col-lg-12">                         


                            <label style="font-size: 16px;font-weight: normal">
                                CONTRIBUABLE : <span id="labelAssujettiName" style="text-decoration: underline;font-weight: bold">
                                </span>
                            </label>

                            <br/><br/>
                            <label style="font-weight: normal">Champs obligatoire <span style="color:red">(*)</span></label>

                            <button class="btn btn-success" id="btnEnregistrer" style="float: right">
                                <i class="fa fa-save"></i>  Enregistrer le bien
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DE BASE (Etape 1)</h4>
                                </div>
                                <div class="panel-wrapper collapse in" style="height:500px">
                                    <div class="panel-body"> 
                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="cmbTypeBien">
                                                Nature <span style="color:red"> *</span></p>
                                            <div class="col-sm-7">
                                                <select class="form-control" id="cmbTypeBien" placeholder="Nature bien"></select>
                                            </div>
                                            <a class="btn btn-primary" 
                                               id="BtnEditerTypeBien"
                                               style="width: 5%">
                                                <i class="fa fa-plus-circle"></i> 
                                            </a>
                                        </div>  

                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="cmbCategorieBien">
                                                Destination <span style="color:red"> *</span></p>
                                            <div class="col-sm-7">
                                                <select class="form-control" id="cmbCategorieBien" placeholder="Destination">
                                                    <option value ="0">-- Sélectionner --</option>  
                                                </select>
                                            </div>
                                            <a class="btn btn-primary" 
                                               id="BtnCategorieBien"
                                               style="width: 5%;display: none">
                                                <i class="fa fa-plus-circle"></i> 
                                            </a>
                                        </div> 

                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="inputIntitule">
                                                Dénomination <span style="color:red"> *</span>
                                            </p>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputIntitule" readonly="true" placeholder="Dénomination concession"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="inputDescription">
                                                Description </p>
                                            <div class="col-sm-8">
                                                <textarea class="form-control" 
                                                          id="inputDescription" placeholder="Description"
                                                          style="resize: none">
                                                </textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="inputDateAcquissition">
                                                Date d'acquisition </p></p>
                                            <div class="col-sm-8">
                                                <input type="date" class="form-control" id="inputDateAcquissition" 
                                                       placeholder="Date d'acquisiition"/>
                                            </div>
                                        </div>

                                        <fieldset class="fieldsetAdresse">

                                            <legend style="font-size: 16px;color: #9e9e9e">Adresse géographique de la concession</legend>

                                            <div class="row">                                                                               
                                                <div class="col-lg-11 divTxtAreaControl">
                                                    <textarea readonly="true" class="form-control " 
                                                              id="inputAdresse" placeholder="Adresse géographique de la concession">                                                 
                                                    </textarea>
                                                </div>
                                                <div class="col-col-1 divBtnAdresseControl">
                                                    <button class="btn btn-warning" id="btnAjouterAdresse" 
                                                            style="margin-top: 10px">
                                                        <i class="fa fa-folder-open"></i>  
                                                    </button>
                                                </div>
                                            </div>

                                        </fieldset>

                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-default" id="divPanelComplement">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS COMPLEMNTAIRES (Etape 2)
                                    </h4>
                                </div>
                                <div class="panel-wrapper collapse in" style="height: 500px;overflow: auto">

                                    <div class="panel-body" id="divComplement"></div>
                                    <a 
                                        id="lblNbDocument"
                                        class="pull-left"
                                        style="font-style: italic;margin-top: 8px;margin-left: 8px;text-decoration: underline;color: blue">
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>

                    <button class="btn btn-success" id="btnEnregistrer2" style="float: right">
                        <i class="fa fa-save"></i>  Enregistrer le bien
                    </button>

                </form> 

            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAdressePersonne.html" %>
        <%@include file="assets/include/modal/modalEditerTypeBien.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/editionBienConcessionMine.js"></script>
        <script type="text/javascript" src="assets/js/gestionBien/editerTypeBien.js"></script>
        <script type="text/javascript" src="assets/js/rechercheAdressePersonne.js"></script>
        <script type="text/javascript" src="assets/js/uploading.js"></script>  

    </body>
</html>
