<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des taux vignettes</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">Informations taux vignette</h4> 

                        </div>
                        <div class="panel-body">

                            <div class="row">

                                <div class="col-lg-12">
                                    <label class="control-label" style="font-weight: normal">Type de personne :</label>
                                    <select class="form-control" id="selectFormeJuridique" style="font-weight: bold">
                                        <option value="0">--</option>
                                        <option value="03">PERSONNE MORALE</option>
                                        <option value="04">PERSONNE PHYSIQUE</option>
                                        <option value="*">TOUS</option>

                                    </select>
                                </div>
                            </div><br/>
                            <div class="row">

                                <div class="col-lg-12">
                                    <label class="control-label" style="font-weight: normal">Tarif :</label>
                                    <select class="form-control" id="selectTarif" style="font-weight: bold">
                                        <option value="0">--</option>

                                    </select>
                                </div>
                            </div><br/>

                            <div class="row">

                                <div class="col-lg-3">
                                    <label class="control-label" style="font-weight: normal">Montant Vignette :</label>
                                    <input type="number" class="form-control" id="inputMontantVignette"   placeholder="Montant Vignette" min="0">
                                </div>
                                
                                <div class="col-lg-3">
                                    <label class="control-label" style="font-weight: normal">Montant TSCR :</label>
                                    <input type="number" class="form-control" id="inputMontantTscr"   placeholder="Montant TSCR" min="0">
                                </div>
                                
                                <div class="col-lg-2">
                                    <label class="control-label" style="font-weight: normal">Taux Vignette (%) :</label>
                                    <input type="number" class="form-control" id="inputTauxVignettePercent"   placeholder="Taux Vignette en %" min="0">
                                </div>
                                
                                <div class="col-lg-2">
                                    <label class="control-label" style="font-weight: normal">Taux TSCR (%):</label>
                                    <input type="number" class="form-control" id="inputTauxTscrPercent"   placeholder="Taux TSCR en %" min="0">
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label" style="font-weight: normal">TOTAL :</label>
                                    <input type="text" class="form-control" id="inputTotal"   placeholder="TOTAL" min="0" readonly="true" style="font-weight: bold">
                                </div>
                            </div><br/>

                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" id="btnSaveTauxVignette">
                                    <i class="fa fa-save"></i> Enregisgtrer
                                </button>
                                <button type="submit" class="btn btn-warning" id="btnEffacer">
                                    <i class="fa fa-trash-o"></i> Effacer les champs
                                </button>
                                <button type="submit" class="btn btn-danger pull-right" id="btnDeleteTauxVignette" disabled="true">
                                    <i class="fa fa-trash-o"></i> Supprimer 
                                </button>

                            </div><hr/>
                            <div class="fonctions">
                                <table id="tableTauxVignette" class="table">

                                </table>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal fade" id="periodeDeclarationModal" role="dialog" data-backdrop="static" data-keyboard="false" >

                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title" id="exampleModalLongTitle">S&eacute;lectionner la p&eacute;riode de d&eacute;claration</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row" style="margin-left: 5px">
                                    <div class="col-lg-12">
                                        <label class="control-label" style="font-weight: normal">Banque :</label>
                                        <select class="form-control" id="selectBanqueExo" style="font-weight: bold">
                                            <option value="0">--</option>
                                        </select>
                                        <br/>
                                    </div>
                                    <br/><br/>
                                    <div class="col-lg-12">
                                        <label class="control-label" style="font-weight: normal">Compte bancaire :</label>
                                        <select class="form-control" id="selectCompteBancaireExo" style="font-weight: bold">
                                            <option value="0">--</option>
                                        </select>
                                    </div>

                                </div> 
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success" id="btnValidateTaxationExo">
                                    <i class="fa fa-check-circle"></i>
                                    Valider taxation Exon&eacute;ration
                                </button>
                                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>


    <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/select2.full.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script> 
    <script type="text/javascript" src="assets/js/utils.js"></script>
    <script type="text/javascript" src="assets/lib/choosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="assets/js/gestionTauxVignette.js"></script>

</body>
</html>
