<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Répertoire des contribuables</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                #inputSearchAssujetti{width: 50%;}
                #idRechercerAvancesAssujetti{width: 100%;margin-top: 8px;margin-right: 0px}
                .btnPerso{background-color: transparent;color: blue;cursor: pointer;border-color: transparent;text-decoration: underline}
                .btnPerso:focus{background-color: transparent;border-color: transparent}
                .inputGroupAssuj{margin-top: 3px}
                .spanLibelles{display: none}
                .journal{overflow: auto}   
            }
        </style>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-9">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="cmbSearchType">
                                <option value ="0">Nom</option>
                                <option value ="1">NIF</option>
                                <option value ="2">NTD</option>
                                <option value ="3">Téléphone</option>
                            </select>
                            <div class="input-group inputGroupAssuj">
                                <input type="text" class="form-control" style="width: 100%" id="inputSearchAssujetti" placeholder="Nom du contribuable">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" id="btnSearchAssujetti">
                                        <i class="fa fa-search"></i> <span class="spanLibelles">Rechercher</span>
                                    </button>
                                    <button class="btn btn-success" id="btnCreateAssujetti" style="margin-left: 5px; display: inline">
                                        <i class="fa fa-plus-circle"></i> <span class="spanLibelles">Créer un contribuable</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-3">
                        <button  id="idRechercerAvancesAssujetti" class="btn btn-warning pull-right btnPerso">
                            <i class="fa fa-filter"></i> &nbsp;
                            Effectuer une recherche avancée
                        </button>
                    </div> 
                </div>

                <hr/>

                <div class="journal" >
                    <table id="tableAssujetti" class="table table-bordered">

                    </table>
                </div>


            </div>

            <div class="modal fade" id="modalChooseModule" 
                 role="dialog"
                 data-backdrop="static" data-keyboard="false" >
                <div class="modal-dialog modal-dialog-centered" style="margin-top: 100px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title" 
                                id="exampleModalLongTitle">Que voulez-vous voir ?</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <label class="control-label col-sm-12" >Veuillez sélectionner le module : </label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="selectModule">
                                        <option value="0">--</option>
                                        <option value="TAX">TAXATION</option>
                                        <option value="REC">RECOUVREMENT</option>
                                        <option value="FRAC">FRACTIONNEMENT</option>
                                        <option value="MED">MISES EN DEMEURES</option>
                                        <option value="POUR">POURSUITE</option>
                                        <option value="CONT">CONTENTIEUX</option>
                                    </select>
                                </div>
                            </div> 
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" id="btnDisplay">
                                <i class="fa fa-check-circle"></i>
                                Afficher
                            </button>
                            <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAvanceeAssujetti.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/repertoire.js"></script>
        <script type="text/javascript" src="assets/js/repertoire/searchCompteCourantFiscale.js"></script>

    </body>
</html>
