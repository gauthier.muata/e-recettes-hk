<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion de taux impot foncier pour exercices anterieurs</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">Tarification</h4> 

                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Imp&ocirc;t </label>
                                <select class="form-control" id="selectImpotEdit" >
                                    <option value="0">--</option>
                                </select>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Exercice </label>
                                        <select class="form-control" id="selectExerciceEdit" >
                                            <option value="0">--</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Forme juridique </label>
                                        <select class="form-control" id="selectFormeJuridiqueEdit" >
                                            <option value="0">--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Cat&eacute;gorie </label>
                                        <select class="form-control" id="selectCategorieEdit" >
                                            <option value="0">--</option>
                                            <option value="1">AUTOMOBILE</option>
                                            <option value="2">IMMOBILIER</option>
                                            <option value="3">CONCESSION MINIERE</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Nature bien</label>
                                        <select class="form-control" id="selectNatureEdit" disabled="true">
                                            <option value="0">--</option>
                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Rang / Localit&eacute; </label>
                                <select class="form-control" id="selectLocaliteEdit" disabled="true">
                                    <option value="0">--</option>
                                </select>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Taux En (USD)</label>
                                        <input type="number" class="form-control" min="1"
                                               id="tauxEdit" placeholder="Saisir le taux">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Base / Unit&eacute;</label>
                                        <select class="form-control" id="selectUniteEdit">
                                            <option value="0">--</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">

                                <label  for="checkBoxMultiplyVB"></label>
                                <input type="checkbox" id="checkBoxMultiplyVB" name="checkBoxMultiplyVB" checked="true"/>
                                <label style="font-weight: normal">Multiplier par la base taxable</label><br/>
                                <label style="font-size: 12px;color:red;font-weight: normal;font-style: italic">En cochant cette option, lors de la taxation, le nombre de bien à taxer sera multiplié par le taux </label>

                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" id="btnSaveConfigEdit">
                                    <i class="fa fa-save"></i> Enregistrer
                                </button>
                            </div><hr/>

                            <div class="fonctions">
                                <table id="tableConfigTaux" class="table">

                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>


        <div class="modal fade" id="modalEditConfigTauxExercice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form class="form formulaire-edition-fonction">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Mise à jour des informations</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Imp&ocirc;t </label>
                                <select class="form-control" id="selectImpotEdit2" >
                                    <option value="0">--</option>
                                </select>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Exercice </label>
                                        <select class="form-control" id="selectExerciceEdit2" >
                                            <option value="0">--</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Forme juridique </label>
                                        <select class="form-control" id="selectFormeJuridiqueEdit2" >
                                            <option value="0">--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Cat&eacute;gorie </label>
                                        <select class="form-control" id="selectCategorieEdit2" >
                                            <option value="0">--</option>
                                            <option value="1">AUTOMOBILE</option>
                                            <option value="2">IMMOBILIER</option>
                                            <option value="3">CONCESSION MINIERE</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Nature bien</label>
                                        <select class="form-control" id="selectNatureEdit2">
                                            <option value="0">--</option>
                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Rang / Localit&eacute; </label>
                                <select class="form-control" id="selectLocaliteEdit2">
                                    <option value="0">--</option>
                                </select>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Taux En (USD)</label>
                                        <input type="number" class="form-control" min="1"
                                               id="tauxEdit2" placeholder="Saisir le taux">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label" style="font-weight: normal">Base / Unit&eacute;</label>
                                        <select class="form-control" id="selectUniteEdit2">
                                            <option value="0">--</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">

                                <label  for="checkBoxMultiplyVB2"></label>
                                <input type="checkbox" id="checkBoxMultiplyVB2" name="checkBoxMultiplyVB2"/>
                                <label style="font-weight: normal">Multiplier par la base taxable</label><br/>
                                <label style="font-size: 12px;color:red;font-weight: normal;font-style: italic">En cochant cette option, lors de la taxation, le nombre de bien à taxer sera multiplié par le taux </label>

                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-dismiss" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-danger" id="btnDelete">Supprimer</button>
                            <button type="submit" class="btn btn-success" id="btnSave">Modifier</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/rechercheAssujetti.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script type="text/javascript" src="assets/js/gestionTauxIfExerciceAnterieur.js"></script>

    </body>
</html>
