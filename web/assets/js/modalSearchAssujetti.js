/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnSelectAssujeti, btnLauchSearchAssujetti;
var inputValueResearchAssujetti, tableResultSearchAssujeti;
var selectAssujettiData;
var assujettiModal;

var codeFormeJ_X;

$(function () {

    btnSelectAssujeti = $('#btnSelectAssujeti');
    btnLaunchSearchAssujetti = $('#btnLauchSearchAssujetti');
    inputValueResearchAssujetti = $('#inputValueResearchAssujetti');
    tableResultSearchAssujeti = $('#tableResultSearchAssujeti');
    assujettiModal = $('#assujettiModal');

    btnLaunchSearchAssujetti.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        searchAssujetti();

    });

    btnSelectAssujeti.click(function (e) {

        if (!isUndefined(selectAssujettiData)) {
            if (selectAssujettiData.code === empty) {
                alertify.alert('Veuillez d\'abord sélectionner un assujetti');
                return;
            }
        } else {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti');
            return;
        }


        alertify.confirm('Etes-vous sûr de vouloir sélectionner <b>' + selectAssujettiData.nomComplet + '</b> ?', function () {
            getSelectedAssujetiData();
            assujettiModal.modal('hide');
        });
    });

    inputValueResearchAssujetti.on('keypress', function (e) {
        if (e.keyCode == 13) {
            searchAssujetti();
        }

    });

    printAssujettiTable('');

});

function printAssujettiTable(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">NTD</th>';
    tableContent += '<th scope="col">NOMS ASSUJETTI</th>';
    tableContent += '<th scope="col">TYPE</th>';
    tableContent += '<th scope="col">ADRESSE</th>';
    tableContent += '<th hidden="true" scope="col">Code assujetti</th>';
    tableContent += '<th hidden="true" scope="col">Code forme</th>';
    tableContent += '<th hidden="true" scope="col">Code adresse</th>';
    tableContent += '<th hidden="true" scope="col">Code adresse personne</th>';
    tableContent += '<th hidden="true" scope="col">Type Exoneration</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;vertical-align:middle;font-weight:bold">' + data[i].codeNTD + '</td>';
        tableContent += '<td style="text-align:left;vertical-align:middle">' + data[i].nomCompletAssujetti + '</td>';
        tableContent += '<td style="text-align:left;vertical-align:middle">' + data[i].libelleFormeJuridique + '</td>';
        tableContent += '<td style="text-align:left;vertical-align:middle">' + data[i].adresseAssujetti + '</td>';
        tableContent += '<td hidden="true">' + data[i].code + '</td>';
        tableContent += '<td hidden="true">' + data[i].codeFormeJuridique + '</td>';
        tableContent += '<td hidden="true">' + data[i].adresseId + '</td>';
        tableContent += '<td hidden="true">' + data[i].codeAdressePersonne + '</td>';
        tableContent += '<td hidden="true">' + data[i].exonerationType + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultSearchAssujeti.html(tableContent);

    var myDt = tableResultSearchAssujeti.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste des personnes ici ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableResultSearchAssujeti tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        selectAssujettiData = new Object();
        selectAssujettiData.code = myDt.row(this).data()[4];
        selectAssujettiData.codeAdresse = myDt.row(this).data()[6];
        selectAssujettiData.codeAdressePersonne = myDt.row(this).data()[7];
        selectAssujettiData.exonerationType = myDt.row(this).data()[8];

        switch (myDt.row(this).data()[8]) {
            case 1:
            case '1':
                selectAssujettiData.exonerationName = 'EXONERATION PARTIELLE (SEULEMENT POUR LA VIGNETTE)';
                break;
            case 2:
            case '2':
                selectAssujettiData.exonerationName = 'EXONERATION TOTALE (VIGNETTE & TSCR)';
                break;
            default:
                selectAssujettiData.exonerationName = '';
                break;
        }

        selectAssujettiData.codeForme = myDt.row(this).data()[5];
        selectAssujettiData.nif = myDt.row(this).data()[0];
        selectAssujettiData.nomComplet = myDt.row(this).data()[1].toUpperCase();
        selectAssujettiData.categorie = myDt.row(this).data()[2].toUpperCase();
        selectAssujettiData.adresse = myDt.row(this).data()[3].toUpperCase();

    });
}

function searchAssujetti() {

    if (inputValueResearchAssujetti.val() === empty || inputValueResearchAssujetti.val().length < SEARCH_MIN_TEXT) {
        showEmptySearchMessage();
        return;
    }

    //var morale = sessionStorage.getItem('personneMorale');

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchAssujetti.val(),
            'formeJuridique': codeFormeJ_X,
            'operation': 'researchResponsible'
        },
        beforeSend: function () {
            assujettiModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                assujettiModal.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnSelectAssujeti.hide();
                    printAssujettiTable('');
                    return;
                } else {
                    var dataAssujetti = JSON.parse(JSON.stringify(response));
                    if (dataAssujetti.length > 0) {
                        btnSelectAssujeti.show();
                        printAssujettiTable(dataAssujetti);
                    } else {
                        btnSelectAssujeti.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            assujettiModal.unblock();
            showResponseError();
        }

    });
}
