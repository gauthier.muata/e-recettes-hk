/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var cbxResearchTypeCommandeVoucher;

var btnSearchCommandeVoucher, btnShowAdvancedSearchModal,
        btnAdvencedSearch, btnSelectedCompteBancaire;

var inputSearchCommandeVoucher, inputDateDebut, inputdateLast;

var codeResearchCommande;

var tableCommandeVoucher;

var modalRechercheAvanceeNC, modalSelectedCompteBancaire;

var tempCmdVoucherList, tempDetailCmdVoucheList;

var checkLoad = true;

var checkUpdate = false;

var codePersonne, cmdId, codeCompteBancaire,
        codeBancaire;

var lblDateDebut, lblDateFin, isAdvance;

var cmbBanques, cmbCompteBancaires;
var dataBankList;

var checkRemise;

var voucherList = [];
var rechercheAvanceeCommandeVoucher;

lblAxePeage,
        lblType;

var referenceSelected;
var modalDisplayVoucherUsing, tableVoucherUsing, idCommande;

var btnGenereRapportVaucherUsing;

var onLoad;

$(function () {

    onLoad = true;

    mainNavigationLabel.text('PEAGE');
    secondNavigationLabel.text('Registre des commandes vouchers');

    tableCommandeVoucher = $('#tableCommandeVoucher');
    modalRechercheAvanceeNC = $('#modalRechercheAvanceeNC');
    modalSelectedCompteBancaire = $('#modalSelectedCompteBancaire');

    cbxResearchTypeCommandeVoucher = $('#cbxResearchTypeCommandeVoucher');

    cmbCompteBancaires = $('#cmbCompteBancaires');
    cmbBanques = $('#cmbBanques');

    btnSearchCommandeVoucher = $('#btnSearchCommandeVoucher');
    btnShowAdvancedSearchModal = $('#btnShowAdvancedSearchModal');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    btnSelectedCompteBancaire = $('#btnSelectedCompteBancaire');

    inputSearchCommandeVoucher = $('#inputSearchCommandeVoucher');
    inputdateLast = $('#inputdateLast');
    inputDateDebut = $('#inputDateDebut');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    isAdvance = $('#isAdvance');

    isAdvance.attr('style', 'display: none');

    lblStatePaiement = $('#lblStatePaiement');
    lblAxePeage = $('#lblAxePeage');
    lblType = $('#lblType');

    modalDisplayVoucherUsing = $('#modalDisplayVoucherUsing');
    tableVoucherUsing = $('#tableVoucherUsing');
    idCommande = $('#idCommande');

    rechercheAvanceeCommandeVoucher = $('#rechercheAvanceeCommandeVoucher');
    btnGenereRapportVaucherUsing = $('#btnGenereRapportVaucherUsing');

    btnGenereRapportVaucherUsing.click(function (e) {

        alertify.confirm('Etes-vous de vouloir imprimer ce rapport de production ?', function () {
            printData();
        });
    });

    btnShowAdvancedSearchModal.click(function (e) {
        e.preventDefault();
        checkUpdate = true;
        rechercheAvanceeCommandeVoucher.modal('show');
    });

    btnSearchCommandeVoucher.click(function (e) {
        e.preventDefault();

        if (inputSearchCommandeVoucher.val() === empty) {

            switch (cbxResearchTypeCommandeVoucher.val()) {

                case '1':
                    alertify.alert('Veuillez fournir le nom de la société comme critère de recherche ');
                    return;
                    break;
                case '2':
                    alertify.alert('Veuillez fournir la référence de la commande comme critère de recherche ');
                    return;
                    break;
            }


        } else {
            onLoad = false;
            loadCommandeVoucher(false);
        }

    });

    cbxResearchTypeCommandeVoucher.on('change', function (e) {

        e.preventDefault();

        codeResearchCommande = cbxResearchTypeCommandeVoucher.val();
        checkLoad = true;

        if (codeResearchCommande === "1") {
            inputSearchCommandeVoucher.attr('placeholder', 'Veuillez saisir le nom de la société');
            inputSearchCommandeVoucher.val('');
        } else {
            inputSearchCommandeVoucher.attr('placeholder', 'Veuillez saisir la référence de la commande');
            inputSearchCommandeVoucher.val('');
        }
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        //modalRechercheAvanceeNC.modal('hide');
        onLoad = false;
        loadCommandeVoucher(true);
    });

    btnSelectedCompteBancaire.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbBanques.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner une banque');
            return;
        }

        if (cmbCompteBancaires.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un compte bancaire');
            return;
        }

        modalSelectedCompteBancaire.modal('hide');
        if (checkRemise) {
            checkVaucher();
        } else {
            ajustValidation(cmdId);
            checkVaucher();
        }
    });

    cmbBanques.on('change', function () {
        codeBancaire = cmbBanques.val();
        getCompteBancairfeOfBank(codeBancaire);
    });

    cmbCompteBancaires.on('change', function () {
        codeCompteBancaire = cmbCompteBancaires.val();
    });

    if (checkLoad) {
        codeResearchCommande = cbxResearchTypeCommandeVoucher.val();
        checkLoad = false;
    }


    loadCommandeVoucher(true);
    getBankList();
    printCommandeVoucher('');

});

function loadCommandeVoucher(checkAdvance) {

    if (checkAdvance) {

        lblDateDebut.html(inputDateDebut.val());
        lblDateFin.html(inputdateLast.val());

        lblStatePaiement.html($('#selectEtatCommande option:selected').text());
        lblAxePeage.html($('#selectSitePeage option:selected').text());
        lblType.html($('#selectTypeCommande option:selected').text());

        isAdvance.attr('style', 'display: block');

    } else {
        isAdvance.attr('style', 'display: none');
    }


    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'valueSearch': inputSearchCommandeVoucher.val().trim(),
            'typeSearch': codeResearchCommande,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'isAdvancedSearch': checkAdvance,
            'selectSitePeage': selectSitePeage.val(),
            'selectEtatCommande': selectEtatCommande.val(),
            'selectTypeCommande': selectTypeCommande.val(),
            'operation': 'loadCommandeVoucher'
        },
        beforeSend: function () {

            if (checkAdvance) {
                rechercheAvanceeCommandeVoucher.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
            } else {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
            }

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            if (checkAdvance) {
                rechercheAvanceeCommandeVoucher.unblock();
            } else {
                $.unblockUI();
            }

            setTimeout(function () {

                if (response == '-1') {
                    printCommandeVoucher('');
                    showResponseError();
                    return;

                } else if (response == '0') {

                    printCommandeVoucher('');

                    if (onLoad) {
                        alertify.alert('Désolé, il n\'y a pas de commande des vouchers disponible à la date d\'aujourd\'hui');
                        return;
                    } else {

                        if (checkAdvance) {

                            alertify.alert('Désolé, aucune commande de vouchers correspond au critère de recherche fournis');
                            return;

                        } else {

                            switch (cbxResearchTypeCommandeVoucher.val()) {

                                case '1':
                                    alertify.alert('Désolé, la société : ' + '<span style="font-weight:bold">' + inputSearchCommandeVoucher.val().toUpperCase() + '</span>' + ', n\'a pas de commande de vouchers');
                                    return;
                                    break;
                                case '2':
                                    alertify.alert('Désolé, cette référence : ' + '<span style="font-weight:bold">' + inputSearchCommandeVoucher.val().toUpperCase() + '</span>' + ', n\'est pas liée à une commande de vouchers');
                                    return;
                                    break;
                            }
                        }

                    }


                } else {

                    tempCmdVoucherList = null;
                    tempCmdVoucherList = JSON.parse(JSON.stringify(response));
                    printCommandeVoucher(tempCmdVoucherList);

                    rechercheAvanceeCommandeVoucher.modal('hide');

                }


            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {

            if (checkAdvance) {
                rechercheAvanceeCommandeVoucher.unblock();
            } else {
                $.unblockUI();
            }
            printCommandeVoucher('');
            showResponseError();
        }

    });
}

var sumCDF,
        sumUSD,
        frequence = 0;

var axePeageSelected;
var societeSelected;

function printCommandeVoucher(tempCmdVouchList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> </th>';
    header += '<th scope="col" style="width:15%"> REFERENCE COMMANDE</th>';
    header += '<th scope="col" style="width:25%"> SOCIETE / ASSUJETTI </th>';
    header += '<th scope="col" style="width:10%"> CONTACTS </th>';
    header += '<th scope="col" style="width:15%"> DATE COMMANDE</th>';
    header += '<th scope="col" style="width:15%"> ETAT COMMANDE</th>';
    header += '<th scope="col" style="width:15%;text-align:right"> MONTANT COMMANDE</th>';
    header += '<th hidden="true" scope="col"> Code Personne </th>';
    header += '<th hidden="true" scope="col"> Code Personne </th>';
    header += '<th hidden="true" scope="col"> Nbre voucher Use </th>';
    header += '<th scope="col"> </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyBien">';

    sumCDF = 0;
    sumUSD = 0;
    frequence = 0;

    for (var i = 0; i < tempCmdVouchList.length; i++) {

        if (tempCmdVouchList[i].devise == 'CDF') {
            sumCDF += +tempCmdVouchList[i].montant;
        } else {
            sumUSD += +tempCmdVouchList[i].montant;
        }

        frequence++;

        var etatLibelle;
        var btnPreuvePaiement = '';
        var btnValiderPaiement = '';

        if (tempCmdVouchList[i].preuvePaiement) {
            btnPreuvePaiement = '<button class="btn btn-warning" onclick="showPreuve(\'' + tempCmdVouchList[i].id + '\')"><i class="fa fa-eye"></i>&nbsp;Visualisation</button>'
        }

        var dateLivraisonInfo = '';
        var caseInfo = '<br/><br/> <span style="font-weight:bold;font-style: italic;color:orange">' + tempCmdVouchList[i].case + '</span>';

        dateLivraisonInfo = '<br/><br/> En la date du : ' + '<span style="font-weight:bold;font-style: italic;color:green">' + tempCmdVouchList[i].datepaiement + '</span>';
        switch (tempCmdVouchList[i].etat) {
            case 1 :
                etatLibelle = 'Payée';
                dateLivraisonInfo = '<br/><br/> En la date du : ' + '<span style="font-weight:bold;font-style: italic;color:green">' + tempCmdVouchList[i].datepaiement + '</span>';
                break;
            case 2 :
                etatLibelle = 'Validée';
                //if (tempCmdVouchList[i].preuvePaiement) {
                if (tempCmdVouchList[i].referencePaiement) {
                    btnValiderPaiement = '<br/><br/><button class="btn btn-primary" onclick="validerPaiement(\'' + tempCmdVouchList[i].id + '\',\'' + tempCmdVouchList[i].reference + '\')"><i class="fa fa-check-circle"></i>&nbsp;Valider le paiement</button>'
                }
                break;
            case 3 :
                etatLibelle = 'En attente de validation';
                break;
            case 4 :
                etatLibelle = 'Livrée';
                dateLivraisonInfo = '<br/><br/> En la date du : ' + '<span style="font-weight:bold;font-style: italic;color:green">' + tempCmdVouchList[i].dateLivraison + '</span>';

                break;
        }

        var societeInfo = '<span style="font-weight:bold">' + tempCmdVouchList[i].responsable.toUpperCase() + '</span>';

        var typeInfo = '  (NTD : <span style="font-style:italic;color:red">' + tempCmdVouchList[i].NTD + '</span>' + ')';

        var phoneNumberInfo = '';

        if (tempCmdVouchList[i].phoneNumber !== '') {
            phoneNumberInfo = 'TELEPHONE : <span style="font-weight:bold">' + tempCmdVouchList[i].phoneNumber + '</span>';
        }

        var mailInfo = '';

        if (tempCmdVouchList[i].mail !== '') {
            mailInfo = '<br/><br/>MAIL : <span style="font-weight:bold">' + tempCmdVouchList[i].mail + '</span>';
        }

        var contactInfo = phoneNumberInfo + mailInfo;
        var typeCommande = '<br/><br/>' + '<span style="font-weight:normal;color:red;font-style:italic">' + tempCmdVouchList[i].typeCmd + '</span>';

        body += '<tr>';
        body += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        body += '<td style="vertical-align:middle;font-weight:bold">' + tempCmdVouchList[i].reference + typeCommande + '</td>';
        body += '<td style="vertical-align:middle;width:25%">' + societeInfo + typeInfo + '<br/><br/>' + tempCmdVouchList[i].adresse + '</td>';
        body += '<td style="vertical-align:middle;width:10%">' + contactInfo + '</td>';
        body += '<td style="vertical-align:middle;">' + tempCmdVouchList[i].dateCreat + '</td>';
        body += '<td style="vertical-align:middle;">' + etatLibelle.toUpperCase() + dateLivraisonInfo + caseInfo + '</td>';
        body += '<td style="vertical-align:middle;font-weight:bold;font-size:16px;text-align:right" id="totalCmd_' + tempCmdVouchList[i].id + '">' + formatNumber(tempCmdVouchList[i].montant, tempCmdVouchList[i].devise) + '</td>';
        body += '<td hidden="true">' + tempCmdVouchList[i].id + '</td>';
        body += '<td hidden="true">' + tempCmdVouchList[i].responsable.toUpperCase() + '</td>';
        body += '<td hidden="true">' + tempCmdVouchList[i].nbreVoucherUse + '</td>';
        body += '<td style="text-align:center;vertical-align:middle;width:4%">' + btnPreuvePaiement + btnValiderPaiement + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    body += '<tfoot>';
    body += '<tr><th colspan="6" style="text-align:right;font-size:14px;vertical-align:middle">RESUME REALISATION :</th><th style="text-align:right;font-size:15px;color:red"></th></tr>';
    body += '</tfoot>';

    var tableContent = header + body;

    tableCommandeVoucher.html(tableContent);

    var dataDetailCmdVcher = tableCommandeVoucher.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste ici  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 50,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;
            $(api.column(6).footer()).html(
                    'TOTAL EN CDF : ' + formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    'TOTAL USD : ' + formatNumber(sumUSD, 'USD') +
                    '<hr/>' +
                    'TOTAL COMMANDE : ' + frequence
                    );

        }

    });


    $('#tableCommandeVoucher tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = dataDetailCmdVcher.row(tr);
        var dataDetail = dataDetailCmdVcher.row(tr).data();

        cmdId = dataDetail[7];
        societeSelected = dataDetail[8];
        referenceSelected = dataDetail[1];
        var nbreVoucherUse = dataDetail[9];

        var valueNbreVoucherUse = '';

        if (controlAccess('VIEW_NBRE_VOUCHER_USE_CMD')) {
            if (nbreVoucherUse > 0) {
                valueNbreVoucherUse = nbreVoucherUse + ' / ';
            }
        }


        idCommande.html(referenceSelected);

        //alert(nbreVoucherUse);

        var refCommande = '<span style="font-weight:bold;color:green">' + dataDetail[1] + '</span>';

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');
        }

        else {

            var detailTable = '<center><h4>Les détails de la commande référencée sous le numéro : ' + refCommande + ' </h4><br/></center>';
            detailTable += '<table class="table table-bordered">';
            detailTable += '<thead><tr style="background-color:#e6ceac;color:black">';
            detailTable += '<td style="text-align:center">AXE PEAGE</td>';
            detailTable += '<td style="text-align:center">CATEGORIE</td>';
            detailTable += '<td style="text-align:left">INFOS NBRE. PASSAGE</td>';
            detailTable += '<td style="text-align:left">TAUX</td>';
            detailTable += '<td style="text-align:right">MONTANT SANS REMISE</td>';
            detailTable += '<td style="text-align:right">MONTANT AVEC REMISE</td>';
            detailTable += '<td style="text-align:left">TYPE COMMANDE</td>';
            detailTable += '</tr></thead>';
            detailTable += '<tbody>';

            var etatCmd = 0;
            var idCmd;
            var voucherUsingExist;

            var nbrTotPassage = 0;

            for (var i = 0; i < tempCmdVoucherList.length; i++) {

                if (tempCmdVoucherList[i].id == cmdId) {

                    voucherList = [];

                    //alert(JSON.stringify(tempCmdVoucherList[i]));

                    etatCmd = tempCmdVoucherList[i].etat;
                    idCmd = tempCmdVoucherList[i].id;
                    checkRemise = tempCmdVoucherList[i].checkRemise;

                    tempDetailCmdVoucheList = JSON.parse(tempCmdVoucherList[i].detailCmdVoucheList);

                    for (var j = 0; j < tempDetailCmdVoucheList.length; j++) {

                        voucherUsingExist = tempDetailCmdVoucheList[j].voucherUseExisting;

                        var inputNumber = "inputNumber_" + j;
                        var finalMount = "mountFinal_" + j;

                        var remiseInfo = '&nbsp;Soit une remise de ' + '<span style="font-weight:bold">' + formatNumberOnly(tempDetailCmdVoucheList[j].remise) + '%' + '</span>';
                        var tauxInitInfo = 'Taux normal : ' + '<span style="font-weight:bold">' + formatNumber(tempDetailCmdVoucheList[j].prixUnitaire, tempDetailCmdVoucheList[j].codeDevise) + '</span>';
                        var tauxRemiseInfo = 'Taux remise : ' + '<span style="font-weight:bold;color:green">' + formatNumber(tempDetailCmdVoucheList[j].prixUnitaireRemise, tempDetailCmdVoucheList[j].codeDevise) + '</span>' + remiseInfo;

                        var tauxInfo = tauxInitInfo + '<br/><br/>' + tauxRemiseInfo;

                        var colorValue = 'green';

                        if (tempDetailCmdVoucheList[j].typePaiement == 'CREDIT') {
                            colorValue = 'red';
                        }

                        nbrTotPassage += tempDetailCmdVoucheList[j].qte;

                        var passageAchete = 'Nombre de passage achetés : ' + '<span style="font-weight:bold;font-size:15px">' + tempDetailCmdVoucheList[j].qte + '</span>';
                        var passageUsed = 'Nombre de passage utilisés : ' + '<span style="font-weight:bold;font-size:15px">' + tempDetailCmdVoucheList[j].consommationVoucher + '</span>';

                        var passageInfo = passageAchete + '<br/><br/>' + passageUsed;

                        var btnDisplayVoucherUsing = '';

                        if (voucherUsingExist == '1') {
                            btnDisplayVoucherUsing = '<br/><br/><a type="button" class="btn btn-warning" onclick="loadVoucherUsing(\'' + tempDetailCmdVoucheList[j].id + '\',\'' + tempDetailCmdVoucheList[j].intituleSite + '\')"><i class="fa fa-list"></i>&nbsp;Afficher les vauchers utilisés</a>'
                        }

                        detailTable += '<tr>';
                        detailTable += '<td style="text-align:center;width:18%;vertical-align:middle">' + tempDetailCmdVoucheList[j].intituleSite + '</td>';
                        detailTable += '<td style="text-align:center;width:18%;vertical-align:middle">' + tempDetailCmdVoucheList[j].intituleTarif.toUpperCase() + '</td>';
                        detailTable += '<td style="text-align:left;width:13%;vertical-align:middle">' + passageInfo + btnDisplayVoucherUsing + '</td>';
                        detailTable += '<td style="text-align:left;width:13%;vertical-align:middle">' + tauxInfo + '</td>';
                        //detailTable += '<td style="text-align:right;width:13%;vertical-align:middle">' + formatNumber(tempDetailCmdVoucheList[j].montantVoucher, tempDetailCmdVoucheList[j].codeDevise) + '</td>';
                        detailTable += '<td style="text-align:right;width:13%;vertical-align:middle">' + formatNumber((tempDetailCmdVoucheList[j].qte * tempDetailCmdVoucheList[j].prixUnitaire), tempDetailCmdVoucheList[j].codeDevise) + '</td>';
                        //detailTable += '<td style="text-align:right;width:13%;vertical-align:middle" id="' + finalMount + '">' + formatNumber(tempDetailCmdVoucheList[j].montantFinal, tempDetailCmdVoucheList[j].codeDevise) + '</td>';
                        detailTable += '<td style="text-align:right;width:13%;vertical-align:middle" id="' + finalMount + '">' + formatNumber((tempDetailCmdVoucheList[j].qte * tempDetailCmdVoucheList[j].prixUnitaireRemise), tempDetailCmdVoucheList[j].codeDevise) + '</td>';
                        detailTable += '<td style="text-align:left;width:10%;vertical-align:middle;color:' + colorValue + ';font-weight:bold">' + tempDetailCmdVoucheList[j].typePaiement + '</td>';
                        detailTable += '</tr>';
                    }

                    break;
                }

            }

            detailTable += '</tbody>';

            detailTable += '<tfoot>';
            detailTable += '<tr><th colspan="2" style="text-align:right;font-size:14px;vertical-align:middle">TOTAL PASSAGE ACHETES :</th><th style="text-align:center;font-size:18px;color:red">' + valueNbreVoucherUse + nbrTotPassage + '</th></tr>';
            detailTable += '</tfoot>';

            if (etatCmd == 3) {

                detailTable += '<tfoot>';
                detailTable += '<tr><th colspan="7" style="vertical-align:middle;text-align:right"><button class="btn btn-success" type="submit" id="btnvalidateCmd"><i class="fa fa-check-circle"></i> Valider la commande</button></th></tr>';
                detailTable += '</tfoot>';
            }
            if (etatCmd == 2) {

                detailTable += '<tfoot>';
                detailTable += '<tr><th colspan="7" style="vertical-align:middle;text-align:right"><a onclick="preparationSend()" style="display:none" class="btn btn-success" type="submit" id="btnEnvoyerVchr">Envoyer code voucher <i class="fa fa-arrow-circle-right"></i></a></th></tr>';
                detailTable += '</tfoot>';
            }

            detailTable += '</table>';
            row.child(detailTable).show();
            tr.addClass('shown');

            btnvalidateCmd = $('#btnvalidateCmd');

            btnvalidateCmd.click(function (e) {
                e.preventDefault();
                modalSelectedCompteBancaire.modal('show');
            });

            btnEnvoyerVchr = $('#btnEnvoyerVchr');

        }

    });
}

function ajustMontant(indice) {

    for (var i = 0; i < tempDetailCmdVoucheList.length; i++) {

        if (indice == i) {

            var row = tempDetailCmdVoucheList[i];

            var montVoucher = parseFloat(row.montantVoucher);
            var percentRemise = $("#inputNumber_" + i).val();
            var montantFinal = row.montantVoucher - (parseFloat(percentRemise) * montVoucher / 100);
            $("#mountFinal_" + i).html(formatNumber(montantFinal, row.codeDevise));

            voucher = {};
            voucher.id = row.id;
            voucher.codeAssujetti = row.codeAssujetti;
            voucher.montantFinal = montantFinal;
            voucher.remise = percentRemise;
            voucher.idCmd = row.idCmd;
            voucher.checkRemise = row.checkRemise;
            voucher.reference = row.reference;
            voucherList.push(voucher);

            break;
        }

    }

}

function checkVaucher() {
    if (checkRemise) {
        if (voucherList.length < tempDetailCmdVoucheList.length) {
            alertify.alert('Veuillez définir le(s) remise(s)');
            return;
        }
    }

    var valueReference = '<span style="font-weight:bold">' + referenceSelected + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir valider cette commande de vouchers n° ' + valueReference + ' ?', function () {

        updateVoucher(true);

    });
}

function updateVoucher(checkOperation) {

    var valueReference = '<span style="font-weight:bold">' + referenceSelected + '</span>';

    checkUpdate = true;

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'updateVoucher',
            'agentMaj': userData.idUser,
            'commandeId': cmdId,
            'codeCompteBancaire': cmbCompteBancaires.val(),
            'checkOperation': checkOperation,
            'detailCmdVoucheList': JSON.stringify(voucherList)
        },
        beforeSend: function () {

            modalSelectedCompteBancaire.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Validation de la commande ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalSelectedCompteBancaire.unblock();

                if (response == '1') {

                    alertify.alert('La validation de la commande n° ' + valueReference + ' s\'est effectuée avec succès.');

                    modalSelectedCompteBancaire.modal('hide');

                    if (checkUpdate) {
                        loadCommandeVoucher(true);
                    } else {
                        loadCommandeVoucher(false);
                    }
                    codeCompteBancaire = '';

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de cette opération.');
                } else if (response == '3') {
                    alertify.alert('L\'opération effectuée avec succès sans notification.');
                    if (checkUpdate) {
                        loadCommandeVoucher(true);
                    } else {
                        loadCommandeVoucher(false);
                    }
                    codeCompteBancaire = '';
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalSelectedCompteBancaire.unblock();
            //showResponseError();

            alertify.alert('L\'opération effectuée avec succès sans notification.');
            loadCommandeVoucher(true);
        }
    });
}

function preparationSend() {

    for (var i = 0; i < tempDetailCmdVoucheList.length; i++) {

        voucher = {};
        voucher.id = tempDetailCmdVoucheList[i].id;
        voucher.codeAssujetti = tempDetailCmdVoucheList[i].codeAssujetti;
        voucher.montantFinal = tempDetailCmdVoucheList[i].montantFinal;
        voucher.remise = tempDetailCmdVoucheList[i].remise;
        voucher.idCmd = tempDetailCmdVoucheList[i].idCmd;
        voucher.reference = tempDetailCmdVoucheList[i].reference;
        voucherList.push(voucher);
    }
    alertify.confirm('Etes-vous sûre de vouloir envoyer ces codes vouchers ?', function () {
        updateVoucher(false);
    });
}

function getBankList() {
    var dataBank = '<option value ="0">-- Sélectionner --</option>';
    dataBankList = JSON.parse(userData.banqueUserList);
    var nameBank = "TMB";
    dataBank += '<option value="B0011" >' + nameBank + '</option>';
    cmbBanques.html(dataBank);
}

function getCompteBancairfeOfBank(codeBanque) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'codeBanque': codeBanque,
            'operation': 'loadCompteBancaireOfBank'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else {
                    var dataCompteBancaireList = $.parseJSON(JSON.stringify(response));

                    if (dataCompteBancaireList.length > 0) {
                        var dataCompteBancaire = '<option value ="0">-- Sélectionner --</option>';
                        for (var i = 0; i < dataCompteBancaireList.length; i++) {
                            dataCompteBancaire += '<option value =' + dataCompteBancaireList[i].code + '>' + dataCompteBancaireList[i].intitule + '</option>';
                        }
                        cmbCompteBancaires.html(dataCompteBancaire);
                        codeBancaire = '';
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function showPreuve(idCommande, preuve) {
    getPreuvePaiement(idCommande);
    btnValiderDocument.attr('style', 'display:none');
}


function getPreuvePaiement(idCommande) {
    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getPreuvePaiement',
            'idCommande': idCommande

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche de la preuve de paiement en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de cette opération.');
                } else if (response == '-1') {
                    showResponseError();
                } else {
                    var dataPreuve = JSON.stringify(response);

                    initUpload(dataPreuve, 'PP');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function ajustValidation(idCmd) {

    for (var i = 0; i < tempCmdVoucherList.length; i++) {

        if (idCmd == tempCmdVoucherList[i].id) {

            voucherList = [];

            tempDetailCmdVoucheList = JSON.parse(tempCmdVoucherList[i].detailCmdVoucheList);

            for (var j = 0; j < tempDetailCmdVoucheList.length; j++) {

                var row = tempDetailCmdVoucheList[j];

                voucher = {};
                voucher.id = row.id;
                voucher.codeAssujetti = row.codeAssujetti;
                voucher.montantFinal = row.montantFinal;
                voucher.remise = row.remise;
                voucher.idCmd = row.idCmd;
                voucher.reference = row.reference;
                voucher.checkRemise = row.checkRemise;
                voucherList.push(voucher);
            }
            break;
        }

    }

}

function validerPaiement(idCommande, reference) {

    referenceSelected = reference;
    var valueReference = '<span style="font-weight:bold">' + reference + '</span>';

    checkUpdate = true;

    alertify.confirm('Etes-vous sûre de vouloir valider cette preuve de paiement de commande n° ' + valueReference + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'peage_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'validerPaiement',
                'idCommande': idCommande,
                'userId': userData.idUser
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Validation de la preuve ...</h5>'});

            },
            success: function (response)
            {

                if (response == '-1') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    $.unblockUI();

                    if (response == '1') {

                        alertify.alert('La validation de la preuve de paiement s\'est effectuée avec succès.');

                        if (checkUpdate) {
                            loadCommandeVoucher(true);
                        } else {
                            loadCommandeVoucher(false);
                        }
                    } else if (response == '0') {
                        alertify.alert('Une erreur inattendue s\'est produite lors de cette opération.');
                    } else {
                        showResponseError();
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });


}

function loadVoucherUsing(id, axePeage) {

    modalDisplayVoucherUsing.modal('show');

    axePeageSelected = axePeage;

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'voucherId': id,
            'operation': 'getListVoucherUsing'
        },
        beforeSend: function () {
            modalDisplayVoucherUsing.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            modalDisplayVoucherUsing.unblock();

            if (response == '-1' || response == '0') {

                showResponseError();
                return;
            }

            setTimeout(function () {

                var result = JSON.parse(JSON.stringify(response));
                printVoucherUsing(result);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {

            modalDisplayVoucherUsing.unblock();
            printVoucherUsing('');
            showResponseError();
        }

    });
}

function printVoucherUsing(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';

    header += '<th scope="col" style="width:15%">N° VOUCHER</th>';
    header += '<th scope="col" style="width:15%">N° TICKET </th>';
    header += '<th scope="col" style="width:10%">N° PLAQUE</th>';
    header += '<th scope="col" style="width:15%">DATE UTILISATION</th>';
    header += '<th scope="col" style="width:20%">POSTE PEAGE</th>';
    header += '<th scope="col" style="width:15%">TAXATEUR</th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyBien">';

    sumCDF = 0;
    sumUSD = 0;
    frequence = 0;
    vaucherUsingList = [];
    var count = 1;

    for (var i = 0; i < result.length; i++) {


        var data = {};

        data.number = count;
        data.vaucher = result[i].code;
        data.categorie = result[i].categorie;
        data.ticket = result[i].ticket;
        data.plaque = result[i].plaque;
        data.tarif = result[i].tarif + ' ' + result[i].devise;
        data.dateUsing = result[i].dateUsing;
        data.poste = result[i].poste;
        data.forToDays = result[i].forToDays;

        if (result[i].devise !== '') {

            if (result[i].devise == 'CDF') {
                sumCDF += +result[i].tarif;
            } else {
                sumUSD += +result[i].tarif;
            }

            frequence++;
        }


        vaucherUsingList.push(data);
        count++;

        body += '<tr>';

        body += '<td style="vertical-align:middle;width:10%;font-weight:bold">' + result[i].code + '</td>';
        body += '<td style="vertical-align:middle;width:16%;font-weight:bold">' + result[i].ticket + '</td>';
        body += '<td style="vertical-align:middle;width:8%">' + result[i].plaque + '</td>';
        body += '<td style="vertical-align:middle;width:12%">' + result[i].dateUsing + '</td>';
        body += '<td style="vertical-align:middle;width:10%">' + result[i].poste + '</td>';
        body += '<td style="vertical-align:middle;width:15%">' + result[i].agentUsing + '</td>';

        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableVoucherUsing.html(tableContent);

    tableVoucherUsing.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste des vauchers ici  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3

    });

    /*if (result.length > 0) {
     modalDisplayVoucherUsing.modal('show');
     }*/


}

var sumCDF = 0;
var sumUSD = 0;
var frequence = 0;
var vaucherUsingList = [];

function printData() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    //if (data == null) {

    docTitle = 'RAPPORT DES VOUCHERS UTILISES POUR LA SOCIETE : ' + societeSelected;
    position = 450;

    docTitle += '\nAXE DE PEAGE : ' + axePeageSelected + '\n\n';
    position = 300;


    var columns = [
        {title: "N°", dataKey: "number"},
        {title: "VOUCHER", dataKey: "vaucher"},
        {title: "CATEGORIE", dataKey: "categorie"},
        {title: "N° TICKET", dataKey: "ticket"},
        {title: "N° PLAQUE", dataKey: "plaque"},
        {title: "TARIF", dataKey: "tarif"},
        {title: "POSTE PEAGE", dataKey: "poste"},
        {title: "DATE UTILISATION", dataKey: "dateUsing"}];

    var rows = vaucherUsingList;

    var pageContent = function (data) {

        doc.setFontSize(10);
        doc.text("REPUBLIQUE DEMOCRATIQUE DU CONGO", 40, 25);
        doc.setFontSize(9);
        doc.text("PROVINCE DU HAUT-KATANGA", 40, 40);
        doc.setFontSize(8);
        doc.text("DIRECTION PROVINCIALE DE GESTION DES PEAGES DU HAUT-KATANGA", 40, 55);
        doc.setFontSize(10);
        doc.text(docTitle, position, 140);
        doc.addImage(docImageDpgp, 'PNG', 105 + 40 + (5 * 2), 60, 65, 65);

        doc.setFontSize(8);
        doc.text("Imprimé le " + day + '/' + month + '/' + year + ' à ' + hh + ':' + mm + ':' + ss + ' avec E-RECETTES 1.0', 590, 25);
        doc.text("Par : " + userData.nomComplet, 590, 35);
        doc.text("Page : " + data.pageCount, 590, 45);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: {
            number: {columnWidth: 40, fontSize: 8, overflow: 'linebreak'},
            vaucher: {columnWidth: 70, fontSize: 8, overflow: 'linebreak'},
            categorie: {columnWidth: 180, overflow: 'linebreak', fontSize: 8},
            ticket: {columnWidth: 130, overflow: 'linebreak', fontSize: 8},
            plaque: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            tarif: {columnWidth: 90, overflow: 'linebreak', fontSize: 8},
            poste: {columnWidth: 100, overflow: 'linebreak', fontSize: 8},
            dateUsing: {columnWidth: 100, overflow: 'linebreak', fontSize: 8}
        },
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 15 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');
    doc.text("TOTAL EN CDF  :  " + formatNumberOnly(sumCDF), 40, finalY + 40);
    doc.text("TOTAL EN USD :  " + formatNumberOnly(sumUSD), 250, finalY + 40);
    doc.text("TOTAL VAUCHERS :  " + frequence, 500, finalY + 40);
    window.open(doc.output('bloburl'), '_blank');
}

