/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var btnSearchCommandeCarte, btnShowAdvancedSearchModal, btnTraiterCmdCarte;

var inputSearchCommandeCarte, textObservation;

var inputDateDebut, inputdateLast, lblDateDebut, lblDateFin;

var cbxResearchTypeCommandeCarte;

var tableCommandeCarte, divTableCommandeCarte;

var codeResearchCmdarte, isAdvance;

var tempCmdCarteList;

var modalRechercheAvanceeModelTwo, modalObservationCmdCarte;

var codeCmd, codeAssujetti, qteCodeCmd;

var checkTraitement = false;

var tempDetailCmdCarte = [];

var codeCmdCarte;


$(function () {

    mainNavigationLabel.text('PEAGE');
    secondNavigationLabel.text('Registre des commandes des cartes');


    tableCommandeCarte = $('#tableCommandeCarte');
    divTableCommandeCarte = $('#divTableCommandeCarte');

    modalRechercheAvanceeModelTwo = $('#modalRechercheAvanceeModelTwo');
    modalObservationCmdCarte = $('#modalObservationCmdCarte');

    cbxResearchTypeCommandeCarte = $('#cbxResearchTypeCommandeCarte');

    btnSearchCommandeCarte = $('#btnSearchCommandeCarte');
    btnShowAdvancedSearchModal = $('#btnShowAdvancedSearchModal');
    btnTraiterCmdCarte = $('#btnTraiterCmdCarte');

    inputSearchCommandeCarte = $('#inputSearchCommandeCarte');
    textObservation = $('#textObservation');

    btnShowAdvancedSearchModal = $('#btnShowAdvancedSearchModal');

    inputdateLast = $('#inputdateLast');
    inputDateDebut = $('#inputDateDebut');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    isAdvance = $('#isAdvance');

    codeResearchCmdarte = 1;
    textObservation.val('');

    btnShowAdvancedSearchModal.click(function (e) {
        e.preventDefault();
        checkTraitement = true;
        modalRechercheAvanceeModelTwo.modal('show');
    });

    btnSearchCommandeCarte.click(function (e) {
        e.preventDefault();
        if (inputSearchCommandeCarte.val() === empty) {
            alertify.alert('Veuillez fournir un critère de recherche ');
            return;
        }
        loadCommandeCarte(false);
    })

    cbxResearchTypeCommandeCarte.on('change', function (e) {
        e.preventDefault();
        codeResearchCmdarte = cbxResearchTypeCommandeCarte.val();

        if (codeResearchCmdarte === "1") {
            inputSearchCommandeCarte.attr('placeholder', 'Le nom de l\'assujetti');
            inputSearchCommandeCarte.val('');
        } else {
            inputSearchCommandeCarte.attr('placeholder', 'Reference commande');
            inputSearchCommandeCarte.val('');
        }
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalRechercheAvanceeModelTwo.modal('hide');
        loadCommandeCarte(true);
    });

    btnTraiterCmdCarte.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (textObservation.val() == empty) {
            alertify.alert('Veuillez d\'abord saisir une observation');
            return;
        }
        modalObservationCmdCarte.modal('hide');
        traiterCmdCarte();
    });

//    printCommandeCarte('');
    loadCommandeCarte(false);
});

function loadCommandeCarte(checkAdvance) {

    if (checkAdvance) {

        lblDateDebut.html(inputDateDebut.val());
        lblDateFin.html(inputdateLast.val());
        isAdvance.attr('style', 'display: block');

    } else {

        isAdvance.attr('style', 'display: none');
    }


    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'valueSearch': inputSearchCommandeCarte.val(),
            'typeSearch': codeResearchCmdarte,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'isAdvancedSearch': checkAdvance,
            'operation': 'loadCommandeCarte'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();

                tempCmdCarteList = null;

                tempCmdCarteList = JSON.parse(JSON.stringify(response));

                printCommandeCarte(tempCmdCarteList);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printCommandeCarte(tempCmdCarteList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> </th>';
    header += '<th scope="col" style="width:13%"> REFERENCE </th>';
    header += '<th scope="col" style="width:20%"> ASSUJETTI </th>';
    header += '<th scope="col" style="width:15%"> TYPE ASSUJETTI </th>';
    header += '<th scope="col" style="width:10%"> DATE </th>';
    header += '<th scope="col" style="width:13%"> ETAT </th>';
    header += '<th scope="col" style="width:10%"> QUANTITE </th>';
    header += '<th scope="col" style="width:12%"> PRIX UNITAIRE </th>';
    header += '<th scope="col" style="width:15%"> PRIX TOTAL </th>';
    header += '<th hidden="true" scope="col"> Code Personne </th>';
    header += '<th scope="col"> </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyCarte">';

    for (var i = 0; i < tempCmdCarteList.length; i++) {

        var etatLibelle;
        var btnTraiteCmdCarte = '';

        switch (tempCmdCarteList[i].etat) {
            case 1 :
                etatLibelle = 'Carte générée';
                break;
            case 2 :
                etatLibelle = 'Validé';
                btnTraiteCmdCarte = '<button class="btn btn-primary" onclick="prepareTraiteCmdCarte(\'' + tempCmdCarteList[i].id + '\', \'' + tempCmdCarteList[i].codePersonne + '\', \'' + tempCmdCarteList[i].qte + '\')"><i class="fa fa-list-alt "></i></button>'
                break;
            case 3 :
                etatLibelle = 'En attente de validation';
                break;
            case 4 :
                etatLibelle = 'Livré';
                break;
        }

        body += '<tr>';
        body += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        body += '<td style="vertical-align:middle;">' + tempCmdCarteList[i].reference + '</td>';
        body += '<td style="vertical-align:middle;">' + tempCmdCarteList[i].responsable + '</td>';
        body += '<td style="vertical-align:middle;">' + tempCmdCarteList[i].formeJuridique + '</td>';
        body += '<td style="vertical-align:middle;">' + tempCmdCarteList[i].dateCreat + '</td>';
        body += '<td style="vertical-align:middle;">' + etatLibelle + '</td>';
        body += '<td style="vertical-align:middle;">' + tempCmdCarteList[i].qte + '</td>';
        body += '<td style="vertical-align:middle;">' + formatNumber(tempCmdCarteList[i].prixUnitaire, tempCmdCarteList[i].codeDevise) + '</td>';
        body += '<td style="vertical-align:middle;" id="totalCmd_' + tempCmdCarteList[i].id + '">' + formatNumber(tempCmdCarteList[i].prixTotal, tempCmdCarteList[i].codeDevise) + '</td>';
        body += '<td hidden="true">' + tempCmdCarteList[i].id + '</td>';
        body += '<td style="text-align:center;vertical-align:middle;width:4%">' + btnTraiteCmdCarte + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableCommandeCarte.html(tableContent);

    var dataDetailCmdCarte = tableCommandeCarte.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par le nom _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 50,
        lengthMenu: [[9, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableCommandeCarte tbody').on('click', 'td.details-control', function () {

        codeCmdCarte = '';

        var tr = $(this).closest('tr');
        var row = dataDetailCmdCarte.row(tr);
        var dataDetail = dataDetailCmdCarte.row(tr).data();

        codeCmdCarte = dataDetail[9];

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');
        } else {
            var detailTable = '<center><h4>LES DETAILS COMMANDES CARTES </h4><br/></center>';
            detailTable += '<div id="childContent_' + codeCmdCarte + '"></div>';

            getDetailCommandeCarte(codeCmdCarte);

            row.child(detailTable).show();
            tr.addClass('shown');

        }
    });
}

function childTable(tempDetailCmdCarte, idCmdCarte) {

    var detailTable = '<table class="table table-bordered">';
    detailTable += '<thead><tr style="background-color:#e6ceac;color:black">';
    detailTable += '<td style="text-align:center">NUMERO CARTE</td>';
    detailTable += '<td style="text-align:center">ETAT</td>';
    detailTable += '<th scope="col"> </th>';
    detailTable += '</tr></thead>';
    detailTable += '<tbody>';

    if (tempDetailCmdCarte.length) {

        for (var j = 0; j < tempDetailCmdCarte.length; j++) {

            var bntActions = '';

            switch (tempDetailCmdCarte[j].etat) {
                case 0 :
                    etatLibelle = 'Carte désactivée';
                    bntActions = '<a onclick="desactiverCarte(\'' + tempDetailCmdCarte[j].id + '\', 3)" class="btn btn-warning" title="Activer carte"><i class="fa fa-check-circle"></i>&nbsp;Activer</a>';
                    break;
                case 2 :
                    etatLibelle = 'Carte imprimée';
                    bntActions = '<a onclick="desactiverCarte(\'' + tempDetailCmdCarte[j].id + '\', 0)" class="btn btn-danger" title="Désactiver carte"><i class="fa fa-trash-o"></i>&nbsp;Désactiver</a>';
                    break;
                case 3 :
                    etatLibelle = 'Carte non imprimer';
                    bntActions += '<a onclick="preparePrintCarte(\'' + tempDetailCmdCarte[j].id + '\')" class="btn btn-success" title="Imprimer carte"><i class="fa fa-check-circle"></i>&nbsp;Imprimer</a>'
                        + '<a onclick="desactiverCarte(\'' + tempDetailCmdCarte[j].id + '\', 0)" class="btn btn-danger" title="Désactiver carte"><i class="fa fa-trash-o"></i>&nbsp;Désactiver</a>';
                    break;
                case 4 :
                    etatLibelle = 'Carte Livrée';
                    break;
            }
            
            detailTable += '<tr>';
            detailTable += '<td style="text-align:center;width:50%;">' + tempDetailCmdCarte[j].numeroCarte + '</td>';
            detailTable += '<td style="text-align:center;width:30%;">' + etatLibelle + '</td>';
            detailTable += '<td style="text-align:center;vertical-align:middle;width:20%">' + bntActions + '</td>';
            detailTable += '</tr>';
        }
    }
    detailTable += '</tbody>';

    detailTable += '</table>';

    var contentChildDiv = $('#childContent_' + idCmdCarte + '');
    contentChildDiv.html(detailTable); 

    btnPrintCarteCmde = $('#btnPrintCarteCmde');

    btnPrintCarteCmde.click(function (e) {
        e.preventDefault();
    });
}

function prepareTraiteCmdCarte(id, codePersonne, qte) {

    alertify.confirm('Etes-vous sûre de traiter cette commande ?', function () {
        codeCmd = id;
        codeAssujetti = codePersonne;
        qteCodeCmd = qte;
        textObservation.val('');
        modalObservationCmdCarte.modal('show');
    });
}

function traiterCmdCarte() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'traiteCmdCarte',
            'observation': textObservation.val(),
            'codeAssujetti': codeAssujetti,
            'qteCodeCmd': qteCodeCmd,
            'agentMaj': userData.idUser,
            'codeCmd': codeCmd
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Traitement de la commande ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'opération effectuée avec succès.');
                    if (checkTraitement) {
                        loadCommandeCarte(true);
                    } else {
                        loadCommandeCarte(false);
                    }
                    codeAssujetti = '';
                    qteCodeCmd = '';
                    codeCmd = '';

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de cette opération.');
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function getDetailCommandeCarte(idCmdCarte) {


    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codeCmd': idCmdCarte,
            'operation': 'getDetailCommandeCarte'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();

                var detailCmdCarteList = null;
                tempDetailCmdCarte = [];

                detailCmdCarteList = JSON.parse(JSON.stringify(response));
                childTable(detailCmdCarteList, idCmdCarte);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printerCarteNFC(reference) {

    if (reference == '') {
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'printCarteNFC',
            'referenceCarte': reference,
            'isToTracking': '0',
            'idUser': userData.idUser
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression de la carte en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                setDocumentContent(response);
                getDetailCommandeCarte(codeCmdCarte);
                $('#tableCommandeCarte tbody').trigger('click');
                window.open('impression-carte ', '_blank');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function preparePrintCarte(ref) {

    alertify.confirm('Etes-vous sûre de vouloir imprimer cette carte ?', function () {
        printerCarteNFC(ref);
    });
}

function desactiverCarte(idCarte, etat) {

    alertify.confirm('Etes-vous sûre de vouloir effectuer cette opération ?', function () {

        disableCarte(idCarte, etat);
    });
}

function disableCarte(idCarte, etat) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'disableCarte',
            'etat': etat,
            'idCarte': idCarte

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Opération d\'une carte en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'opération d\'une carte s\'est effectuée avec succès.');
                    getDetailCommandeCarte(codeCmdCarte);

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'opération d\'une carte.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}