/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var modalCorrectionNoteTaxation,
        selectTypeCorrection,
        divItem1,
        numeroNoteDeleted,
        divItem2,
        selectBankAccountBank,
        selectNewAccountBank,
        divItem3,
        amountNote,
        numeroNoteUpdate1,
        numeroNoteUpdate2,
        btnValidateAction;

var divItem4,
        numeroNoteUpdate3,
        selectDevise;

var value456, lblDivItem456, divItem456;
var divItem7, numeroNoteUpdate7, selectModePaiement;
var divItem89, valueQteOrLot, lblDivItem89;

$(function () {

    mainNavigationLabel.text('DECLARATION');
    secondNavigationLabel.text('Correction de la note de perception');

    removeActiveMenu();
    linkSubMenuEditerPaiement.addClass('active');

    modalCorrectionNoteTaxation = $('#modalCorrectionNoteTaxation');
    selectTypeCorrection = $('#selectTypeCorrection');
    divItem1 = $('#divItem1');
    numeroNoteDeleted = $('#numeroNoteDeleted');
    divItem2 = $('#divItem2');
    selectBankAccountBank = $('#selectBankAccountBank');
    selectNewAccountBank = $('#selectNewAccountBank');
    divItem3 = $('#divItem3');
    btnValidateAction = $('#btnValidateAction');
    amountNote = $('#amountNote');
    numeroNoteUpdate1 = $('#numeroNoteUpdate1');
    numeroNoteUpdate2 = $('#numeroNoteUpdate2');

    divItem4 = $('#divItem4');
    numeroNoteUpdate3 = $('#numeroNoteUpdate3');
    selectDevise = $('#selectDevise');

    value456 = $('#value456');
    lblDivItem456 = $('#lblDivItem456');
    divItem456 = $('#divItem456');

    divItem7 = $('#divItem7');
    numeroNoteUpdate7 = $('#numeroNoteUpdate7');
    selectModePaiement = $('#selectModePaiement');

    divItem89 = $('#divItem89');
    valueQteOrLot = $('#valueQteOrLot');
    lblDivItem89 = $('#lblDivItem89');

    selectBankAccountBank.change(function (e) {

        e.preventDefault();

        if (selectBankAccountBank.val() !== '0') {
            loadingAccountBankData(selectBankAccountBank.val());
        } else {

            selectNewAccountBank.val('0');
            selectNewAccountBank.attr('disabled', true);
            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;
        }

    });

    selectTypeCorrection.on('change', function (e) {

        if (selectTypeCorrection.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un type de correction valide');
            return;

        } else {

            switch (selectTypeCorrection.val()) {

                case '1' : //Suppression de la note

                    divItem1.attr('style', 'display:inline');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem7.attr('style', 'display:none');
                    divItem456.attr('style', 'display:none');
                    divItem89.attr('style', 'display:none');

                    break;
                case '2' : //Modifier le compte bancaire de la note

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:inline');
                    divItem3.attr('style', 'display:none');
                    divItem456.attr('style', 'display:none');
                    divItem7.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem89.attr('style', 'display:none');

                    loadingBankData();
                    break;
                case '3' : //Modifier la devsie de la note
                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem456.attr('style', 'display:none');
                    divItem4.attr('style', 'display:inline');
                    divItem7.attr('style', 'display:none');
                    divItem89.attr('style', 'display:none');
                    break;

                case '4' : //Modifier le nom de l'agence
                case '5' : //Modifier la nature du produit
                case '6' : //Modifier le numéro de la plaque

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem7.attr('style', 'display:none');
                    divItem89.attr('style', 'display:none');
                    divItem456.attr('style', 'display:inline');

                    if (selectTypeCorrection.val() == '4') {
                        lblDivItem456.html('Veuillez saisir le nom de l\'agence');
                        value456.attr('placeholder', 'Saisir le nom de l\'agence');

                    } else if (selectTypeCorrection.val() == '5') {

                        lblDivItem456.html('Veuillez saisir la nature de produit');
                        value456.attr('placeholder', 'Saisir la nature du produit');

                    } else if (selectTypeCorrection.val() == '6') {

                        lblDivItem456.html('Veuillez saisir le nouveau numéro de plaque');
                        value456.attr('placeholder', 'Saisir le nouveau numéro de plaque');

                    }

                    break;

                case '7' : //Modifier le mode de paiement

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem456.attr('style', 'display:none');
                    divItem89.attr('style', 'display:none');
                    divItem7.attr('style', 'display:inline');
                    break;

                case '8' : //Modifier le nombre de lot
                case '9' : //Modifier le tonnage

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem456.attr('style', 'display:none');
                    divItem89.attr('style', 'display:inline');
                    divItem7.attr('style', 'display:none');

                    if (selectTypeCorrection.val() == '8') {

                        lblDivItem89.html('Veuillez saisir le nombre de lôt');
                        valueQteOrLot.attr('placeholder', 'Saisir le nombre de lôt');

                    } else if (selectTypeCorrection.val() == '9') {

                        lblDivItem89.html('Veuillez saisir le tonnage');
                        valueQteOrLot.attr('placeholder', 'Saisir le tonnage');

                    }

                    break;
            }
        }
    });

    btnValidateAction.on('click', function (e) {

        e.preventDefault();

        if (selectTypeCorrection.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un type de correction valide');
            return;

        } else {

            switch (selectTypeCorrection.val()) {

                case '1' : //Suppression de la note

                    if (numeroNoteDeleted.val() == empty) {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de perception à supprimer');
                        return;
                    } else {
                        deleteNotePerception();
                    }

                    break;
                case '2' : //Modifier le compte bancaire de la note

                    if (numeroNoteUpdate1.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de perception');
                        return;
                    }

                    if (selectBankAccountBank.val() == '0') {
                        alertify.alert('Veuillez d\'abord sélectionner une banque valide');
                        return;
                    }

                    if (selectNewAccountBank.val() == '0') {
                        alertify.alert('Veuillez d\'abord sélectionner un compte bancaire valide');
                        return;
                    }

                    updateAccountBank();

                    break;

                case '3' : //Modifier le montant de la note

                    if (numeroNoteUpdate2.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de perception');
                        return;
                    }

                    if (amountNote.val() == '') {

                        alertify.alert('Veuillez d\'abord fournir le nouveau montant de la note');
                        return;

                    } else if (amountNote.val() <= 0) {
                        alertify.alert('Le nouveau montant de la note ne doit pas être inférieur à zéro');
                        return;
                    }

                    updateAmountNoteTaxation();
                    break;

                case '4': //Modifier le nom de l'agence

                    if (numeroNoteUpdate2.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de perception');
                        return;
                    }

                    if (value456.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le nouveau nom de l\'agence');
                        return;
                    }

                    updated456('4');
                    break;
                case '5': //Modifier le nom du produit

                    if (numeroNoteUpdate2.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de perception');
                        return;
                    }

                    if (value456.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir la nouvelle nature du produit');
                        return;
                    }

                    updated456('5');
                    break;
                case '6': //Modifier le numéro de plaque

                    if (numeroNoteUpdate2.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de perception');
                        return;
                    }

                    if (value456.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le nouveau numéro de plaque');
                        return;
                    }

                    updated456('6');
                    break;
                case '7': //Modifier le mode de paiement

                    if (numeroNoteUpdate7.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de perception');
                        return;
                    }

                    if (selectModePaiement.val() == '0') {
                        alertify.alert('Veuillez d\'abord sélectionner le nouveau mode de paiement de la note de perception');
                        return;
                    }

                    updateModePaiementNotePerception();
                    break;
            }
        }
    });

    modalCorrectionNoteTaxation.modal('show');

})

function deleteNotePerception() {

    var value = '<span style="font-weight:bold">' + numeroNoteDeleted.val().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir supprimer cette note de perception n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'registrenoteperception',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numero': numeroNoteDeleted.val().trim(),
                'userId': userData.idUser,
                'operation': 'deleteNotePerception'

            },
            beforeSend: function () {
                modalCorrectionNoteTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionNoteTaxation.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette note de perception n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else if (response == '3') {

                    alertify.alert('SUPPRESSION IMPOSSIBLE. <br/><br/> Cette note de perception n° ' + value + ' est déjà payée à la banque !');
                    return;

                } else {

                    alertify.alert('La supression de la note de perception n° ' + value + ' s\'est effectuée avec succès');
                    numeroNoteDeleted.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem7.attr('style', 'display:none');
                    divItem456.attr('style', 'display:none');
                    divItem89.attr('style', 'display:none');

                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionNoteTaxation.unblock();
                showResponseError();
            }

        });
    });

}

function updateAccountBank() {

    var value = '<span style="font-weight:bold">' + numeroNoteUpdate1.val().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir modifier le compte bancaire cette note de perception n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'registrenoteperception',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numero': numeroNoteUpdate1.val().trim(),
                'userId': userData.idUser,
                'compteBancaire': selectNewAccountBank.val(),
                'banque': selectBankAccountBank.val(),
                'operation': 'updateAccountBank'

            },
            beforeSend: function () {
                modalCorrectionNoteTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionNoteTaxation.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette note de perception n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else if (response == '3') {

                    alertify.alert('MODIFICATION IMPOSSIBLE. <br/><br/> Cette note de perception n° ' + value + ' est déjà payée à la banque !');
                    return;

                } else {

                    alertify.alert('La modification du compte bancaire de la note de perception n° ' + value + ' s\'est effectuée avec succès');
                    numeroNoteUpdate1.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem7.attr('style', 'display:none');
                    divItem456.attr('style', 'display:none');
                    divItem89.attr('style', 'display:none');
                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionNoteTaxation.unblock();
                showResponseError();
            }

        });
    });

}

function loadingBankData() {


    var dataBank = empty;
    var defautlValue = '--';
    dataBank += '<option value="0">' + defautlValue + '</option>';

    var banqueUserList = JSON.parse(userData.banqueUserList);

    for (var i = 0; i < banqueUserList.length; i++) {

        dataBank += '<option value="' + banqueUserList[i].banqueCode + '" >' + banqueUserList[i].banqueName + '</option>';

    }

    selectBankAccountBank.html(dataBank);
}

function loadingAccountBankData(bankCode) {

    var dataAccountBank = empty;
    var defautlValue = '--';

    dataAccountBank += '<option value="0">' + defautlValue + '</option>';

    var accountBankList = JSON.parse(userData.accountList);

    for (var i = 0; i < accountBankList.length; i++) {
        if (bankCode == accountBankList[i].acountBanqueCode && (accountBankList[i].typeCompteCode == 2 | accountBankList[i].typeCompteCode == 3)) {
            dataAccountBank += '<option value="' + accountBankList[i].accountCode + '" >' + accountBankList[i].accountName + '</option>';
        }
    }

    selectNewAccountBank.html(dataAccountBank);
    selectNewAccountBank.attr('disabled', false);
}

function updateDeviseNotePerception() {

    var value = '<span style="font-weight:bold">' + numeroNoteUpdate3.val().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir modifier la devise de la note de perception n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'registrenoteperception',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numero': numeroNoteUpdate3.val().trim(),
                'userId': userData.idUser,
                'deviseNote': selectDevise.val(),
                'operation': 'updateDeviseNotePerception'

            },
            beforeSend: function () {
                modalCorrectionNoteTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionNoteTaxation.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette note de perception n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else if (response == '3') {

                    alertify.alert('MODIFICATION IMPOSSIBLE. <br/><br/> Cette note de perception n° ' + value + ' est déjà payée à la banque !');
                    return;

                } else if (response == '4') {

                    alertify.alert('MODIFICATION IMPOSSIBLE. <br/><br/> Cette note de perception n° ' + value + ' car la devise initiale est égale à celle sélectionner');
                    return;

                } else {

                    alertify.alert('La modification de devise de la note de perception n° ' + value + ' s\'est effectuée avec succès');
                    selectDevise.val('0');
                    numeroNoteUpdate3.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem7.attr('style', 'display:none');
                    divItem456.attr('style', 'display:none');
                    divItem89.attr('style', 'display:none');
                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionNoteTaxation.unblock();
                showResponseError();
            }

        });
    });

}

function updateModePaiementNotePerception() {

    var value = '<span style="font-weight:bold">' + numeroNoteUpdate7.val().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir modifier le nouveau mode de paiement de la note de perception n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'registrenoteperception',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numero': numeroNoteUpdate7.val().trim(),
                'userId': userData.idUser,
                'modePaiementNote': selectModePaiement.val(),
                'operation': 'updateModePaiementNotePerception'

            },
            beforeSend: function () {
                modalCorrectionNoteTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionNoteTaxation.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette note de perception n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else if (response == '3') {

                    alertify.alert('MODIFICATION IMPOSSIBLE. <br/><br/> Cette note de perception n° ' + value + ' est déjà payée à la banque !');
                    return;

                } else if (response == '4') {

                    alertify.alert('MODIFICATION IMPOSSIBLE. <br/><br/> Cette note de perception n° ' + value + ' n\'est pas liée à la taxe de VOIRIE ou CONCENTREE');
                    return;

                } else {

                    alertify.alert('La modification du mode de paiement de la note de perception n° ' + value + ' s\'est effectuée avec succès');
                    selectDevise.val('0');
                    numeroNoteUpdate3.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem7.attr('style', 'display:none');
                    divItem456.attr('style', 'display:none');
                    divItem89.attr('style', 'display:none');
                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionNoteTaxation.unblock();
                showResponseError();
            }

        });
    });

}

function updated456(valueSelect) {

    var value = '<span style="font-weight:bold">' + numeroNoteUpdate2.val().trim() + '</span>';

    var operationName = '';

    switch (valueSelect) {
        case '4':
            operationName = ' le nom de l\'agence ';
            break;
        case '5':
            operationName = ' la nature du produit ';
            break;
        case '6':
            operationName = ' le numéro de la plaque ';
            break;
    }

    alertify.confirm('Etes-vous sûre de vouloir modifier ' + operationName + ' pour la note de perception n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'registrenoteperception',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numero': numeroNoteUpdate2.val().trim(),
                'typeValueSearch': selectTypeCorrection.val(),
                'userId': userData.idUser,
                'value456': value456.val(),
                'operation': 'updated456'

            },
            beforeSend: function () {
                modalCorrectionNoteTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionNoteTaxation.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette note de perception n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else if (response == '3') {

                    alertify.alert('MODIFICATION IMPOSSIBLE. <br/><br/> Cette note de perception n° ' + value + ' est déjà payée à la banque !');
                    return;

                } else {

                    alertify.alert('La modification de ' + operationName + ' pour la note de perception n° ' + value + ' s\'est effectuée avec succès');

                    value456.val('');
                    numeroNoteUpdate2.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem7.attr('style', 'display:none');
                    divItem456.attr('style', 'display:none');
                    divItem89.attr('style', 'display:none');
                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionNoteTaxation.unblock();
                showResponseError();
            }

        });
    });

}

function updated89(valueSelect) {

    var value = '<span style="font-weight:bold">' + numeroNoteUpdate2.val().trim() + '</span>';

    var operationName = '';

    switch (valueSelect) {
        case '8':
            operationName = ' le nombre de lot ';
            break;
        case '9':
            operationName = ' la tonnage ';
            break;

    }

    alertify.confirm('Etes-vous sûre de vouloir modifier ' + operationName + ' pour la note de perception n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'registrenoteperception',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numero': numeroNoteUpdate2.val().trim(),
                'typeValueSearch': selectTypeCorrection.val(),
                'userId': userData.idUser,
                'valueQteOrLot': valueQteOrLot.val(),
                'operation': 'updated89'
            },
            beforeSend: function () {
                modalCorrectionNoteTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionNoteTaxation.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette note de perception n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else if (response == '3') {

                    alertify.alert('MODIFICATION IMPOSSIBLE. <br/><br/> Cette note de perception n° ' + value + ' est déjà payée à la banque !');
                    return;

                } else {

                    alertify.alert('La modification de ' + operationName + ' pour la note de perception n° ' + value + ' s\'est effectuée avec succès');

                    valueQteOrLot.val('');
                    numeroNoteUpdate2.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem7.attr('style', 'display:none');
                    divItem456.attr('style', 'display:none');
                    divItem89.attr('style', 'display:none');

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionNoteTaxation.unblock();
                showResponseError();
            }

        });
    });

}

