/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var codeResponsible = '';

var lblLegalForm, lblNifResponsible, lblNameResponsible, lblAddress;
var lbl1, lbl2, lbl3, lbl4;
var btnCallModalSearchResponsable;

var cmbTypeDocument;

var inputNumeroDocument, textReferenceCourrier;
var textObservation;
var btnSearchAndAddDocument;

var tableDemandeEchelonnement;

var codeTypeDoc = 'TD00000052';
var codeDoc = undefined;

var tempDocumentList = [];
var tempDataEchelonnementList = [];

var lblNoteDateExi;

var btnJoindreArchiveEchelonnement;
var lblNbDocumentEchelonnement;

var archivesEchelonnement = '';

var btnSaveDemandeEchelonnement;
var cmbDivision;
var forCd;

$(function () {

    mainNavigationLabel.text('ECHELONNEMENT');
    secondNavigationLabel.text('Edition d\'une demande d\'échelonnement');

    removeActiveMenu();
    linkMenuFractionnement.addClass('active');

//    if (!controlAccess('12001')) {
//        window.location = 'dashboard';
//    }

    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    lblNifResponsible = $('#lblNifResponsible');
    cmbDivision = $('#cmbDivision');

    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');
    lbl3 = $('#lbl3');
    lbl4 = $('#lbl4');

    lblNoteDateExi = $('#lblNoteDateExi');

    cmbTypeDocument = $('#cmbTypeDocument');
    inputNumeroDocument = $('#inputNumeroDocument');
    textReferenceCourrier = $('#textReferenceCourrier');
    textObservation = $('#textObservation');
    btnSearchAndAddDocument = $('#btnSearchAndAddDocument');

    tableDemandeEchelonnement = $('#tableDemandeEchelonnement');
    btnJoindreArchiveEchelonnement = $('#btnJoindreArchiveEchelonnement');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');

    btnSaveDemandeEchelonnement = $('#btnSaveDemandeEchelonnement');
    textReferenceCourrier = $('#textReferenceCourrier');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');

    cmbDivision.on('change', function (e) {

        if (cmbDivision.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner une division valide');
            return;

        } else {

            for (var i = 0; i < tempDataEchelonnementList.length; i++) {

                if (tempDataEchelonnementList[i].code == cmbDivision.val()) {

                    forCd = tempDataEchelonnementList[i].forDivision;
                }
            }
        }
    });

    btnCallModalSearchResponsable.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        assujettiModal.modal('show');

    });


    textReferenceCourrier.val('');

    btnSearchAndAddDocument.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        if (inputNumeroDocument.val().trim() == '') {
            alertify.alert('Veuillez d\'abord saisir un titre de perception.');
            return;
        }

        if (!checkTitreExist(inputNumeroDocument.val().trim())) {
            searchtitre();
        } else {
            alertify.alert('Ce titre existe déjà dans la liste. Impossible de l\'ajouter plus d\'une fois.');
            return;
        }

    });

    btnJoindreArchiveEchelonnement.click(function (e) {

        e.preventDefault();

        if (archivesEchelonnement == '') {
            initUpload(codeDoc, codeTypeDoc);
        } else {
            initUpload(archivesEchelonnement, codeTypeDoc);
        }

    });

    lblNbDocumentEchelonnement.click(function (e) {

        initUpload(archivesEchelonnement, codeTypeDoc);

        e.preventDefault();

    });

    btnSaveDemandeEchelonnement.click(function (e) {

        if (tempDocumentList.length == 0) {

            alertify.alert('Veuillez ajouter au moins un titre dans la liste des titres à échelonner');
            return;
        }

        if (textReferenceCourrier.val() == empty) {

            alertify.alert('Veuillez founir la référence de la lettre de la demande d\'échelonnement');
            return;
        }

        if (archivesEchelonnement.length == 0 || archivesEchelonnement == '') {

            alertify.alert('Veuillez d\'abord joindre le courrier de la demannde d\'échelonnement');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir enregistrer cette demande d\'échelonnement ?', function () {

            saveDemandeEchelonnement();

        });

    });

    initDataEchelonnement();
});

function saveDemandeEchelonnement() {

    var checkApplyPenalities = '0';

    if (document.getElementById("checkApplyTenPercent").checked) {
        checkApplyPenalities = '1';
    }

    $.ajax({
        type: 'POST',
        url: 'fractionnement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': '1201',
            'titreList': JSON.stringify(tempDocumentList),
            'letterReference': textReferenceCourrier.val(),
            'codePersonne': codeResponsible,
            'observation': textObservation.val(),
            'archives': archivesEchelonnement,
            'userId': userData.idUser,
            'fkDocument': codeTypeDocumentSelected,
            'observationDocument': textObservDoc.val(),
            'checkApplyPenalities': checkApplyPenalities,
            'forCd': forCd,
            'divisionCode': cmbDivision.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de la demande en cours...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '1') {
                alertify.alert('L\'enregistrement de la demande d\'échelonnement s\'est effectué avec succès.');

                setTimeout(function () {
                    window.location = 'editer-fractionnement';
                }, 1500);

            } else if (response == '0') {
                alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement');
            } else {
                showResponseError();
            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI()();
            showResponseError();
        }
    });
}

function getSelectedAssujetiData() {

    codeResponsible = selectAssujettiData.code;
    lblNameResponsible.html(selectAssujettiData.nomComplet);
    lblLegalForm.html(selectAssujettiData.categorie);
    lblAddress.html(selectAssujettiData.adresse);
    lblNifResponsible.html(selectAssujettiData.nif);

    lbl1.show();
    lbl2.show();
    lbl3.show();
    lbl4.show();
}

function searchtitre() {

    $.ajax({
        type: 'POST',
        url: 'fractionnement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTitreDetail',
            'codeAssujetti': codeResponsible,
            'numeroDocument': inputNumeroDocument.val().trim(),
            'typeSearchDocument': cmbTypeDocument.val(),
            'codeService': userData.allServiceList,
            'codeSite': userData.allSiteList,
            'codeEntite': userData.allEntiteList
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            switch (response) {
                case 400 :
                    alertify.alert('Le titre de perception fourni n\'a pas été trouvé.');
                    return;
                case 500 :
                    alertify.alert('Le titre de perception fourni n\'a pas un accusé de réception ou imprimé.');
                    return;
                case 600 :
                    alertify.alert('Le titre de perception fourni est un intercalaire');
                    return;
                case 700 :
                    alertify.alert('Le titre de perception fourni déjà echelonné');
                    return;
                case 800 :
                    alertify.alert('Le titre de perception est complètement payé');
                    return;
                case 900 :
                    alertify.alert('Une demande existante contient le titre fourni. <br/> Vérifier dans le registre des demandes d\'échelonnements.');
                    return;
                case 901 :
                    alertify.alert('Le titre de perception fourni a déjà été enrôlé. <br/> Impossible d\'effectuer une demande d\'échelonnement.');
                    return;
                case 902 :
                    alertify.alert('Impossible d\'échelonner ce titre de perception fourni.');
                    return;
                case 1000 :
                    alertify.alert('Impossible d\'échelonner ce titre de perception car c\'est un échelonnement');
                    return;
            }

            setTimeout(function () {

                tempDocumentList.push(response);

                printTitreList();

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printTitreList() {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> TYPE TITRE </th>';
    header += '<th scope="col"> NUM. TITRE </th>';
    header += '<th scope="col"> BUREAU </th>';
    header += '<th scope="col"> ARTICLE BUDGETAIRE </th>';
    header += '<th scope="col"> DATE CREATION</th>';
    header += '<th scope="col"> DATE ECHEANCE </th>';
    header += '<th scope="col" style="text-align:right"> MONTANT DÛ </th>';
    header += '<th></th>';
    header += '</tr></thead>';

    var body = '<tbody id="idBodyReclamation">';

    for (var i = 0; i < tempDocumentList.length; i++) {

        var firstLineAB = '';
        var colorEcheance = 'black';


        if (tempDocumentList[i].isEchanceEchus) {
            colorEcheance = 'red';
            lblNoteDateExi.show();
        }

        body += '<tr>';
        body += '<td style="vertical-align:middle;width:10%">' + getTypeDocumentName(tempDocumentList[i].TYPE_DOCUMENT) + '</td>';
        body += '<td style="vertical-align:middle;width:10%">' + tempDocumentList[i].referenceDocumentManuel + '</td>';
        body += '<td style="vertical-align:middle;width:20%">' + tempDocumentList[i].SERVICE.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;text-align:left;width:25%;">' + tempDocumentList[i].libelleArticleBudgetaire.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;width:10%">' + tempDocumentList[i].DATECREAT + '</td>';
        body += '<td style="vertical-align:middle;width:10%;color:' + colorEcheance + '">' + tempDocumentList[i].DATE_ECHEANCE_PAIEMENT + '</td>';
        body += '<td style="vertical-align:middle;width:10%;text-align:right;font-weight:bold;font-sie:16px">' + formatNumber(tempDocumentList[i].MONTANTDU, tempDocumentList[i].DEVISE) + '</td>';
        body += '<td style="vertical-align:middle;text-align:center"><a onclick="removeTitre(\'' + tempDocumentList[i].referenceDocument + '\')" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableDemandeEchelonnement.html(tableContent);
    tableDemandeEchelonnement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });

    textReferenceCourrier.val('');
}

function getTypeDocumentName(type) {
    var nameDocument = '';
    switch (type) {
        case 'NP':
            nameDocument = 'NOTE DE PERCEPTION';
            break;
        case 'NT':
            nameDocument = 'NOTE DE TAXATION';
            break;

    }
    return nameDocument;
}

function removeTitre(idTitre) {

    alertify.confirm('Etes-vous sûre de vouloir retirer ce titre de la liste ?', function () {

        for (var i = 0; i < tempDocumentList.length; i++) {

            if (tempDocumentList[i].referenceDocument == idTitre) {

                tempDocumentList.splice(i, 1);
                printTitreList();
                break;
            }
        }

    });

}

function checkTitreExist(refTitre) {

    var exist = false;

    for (var i = 0; i < tempDocumentList.length; i++) {

        if (tempDocumentList[i].referenceDocument == refTitre) {

            exist = true;
            break;
        }
    }

    return exist;
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesEchelonnement = getUploadedData();

    nombre = JSON.parse(archivesEchelonnement).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function initDataEchelonnement() {

    $.ajax({
        type: 'POST',
        url: 'fractionnement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'initDataEchelonnement'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            tempDataEchelonnementList = JSON.parse(JSON.stringify(response));

            var dataDivision = '';
            dataDivision += '<option value="0">--</option>';

            for (var i = 0; i < tempDataEchelonnementList.length; i++) {
                dataDivision += '<option value="' + tempDataEchelonnementList[i].code + '">' + tempDataEchelonnementList[i].intitule + '</option>';

            }

            cmbDivision.html(dataDivision);

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI()();
            showResponseError();
        }
    });
}
