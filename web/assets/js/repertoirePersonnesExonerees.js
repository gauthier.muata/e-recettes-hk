/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var rechercheAvanceeExoneres = $('.rechercheAvanceeExoneres'),
        inputAssujettisExoneres = $('.inputAssujettisExoneres'),
        btnSearchAssujettisExoneres = $('.btnSearchAssujettisExoneres'),
        btnAnnulerExoneration = $('.btnAnnulerExoneration'),
        btnAdvancedSearch = $('.btnAdvancedSearch'),
        btnSearchAdvanced = $('.btnSearchAdvanced'),
        inputObservation = $('.inputObservation'),
        etatExoneration = $('.etatExoneration'),
        btnRapport = $('.btnRapport'),
        dateDebut = $('.dateDebut'),
        dateFin = $('.dateFin'),
        cmbAB = $('.cmbAB');
var date_debut,
        date_fin,
        typeRecherche = '0',
        cmb_articleB = '*',
        codeExo = '',
        total_CDF = 0,
        total_USD = 0;
var dataExonerationPrint = [];

$(function () {

    removeActiveMenu();

    if (getMenuExoneration() === '1') {
        mainNavigationLabel.text('EXONERATION');
        secondNavigationLabel.text('R\351pertoire des personnes exon\351r\351es');
        linkMenuExoneration.addClass('active');
    }

    btnAdvancedSearch.on('click', function (e) {
        e.preventDefault();
        rechercheAvanceeExoneres.modal('show');
    });

    btnSearchAdvanced.on('click', function (e) {
        e.preventDefault();
        getDataExonerees();
    });

    btnSearchAssujettisExoneres.on('click', function (e) {
        e.preventDefault();
        typeRecherche = '1';
        getDataExonerees();
    });

    cmbAB.on('change', function (e) {
        e.preventDefault();
        cmb_articleB = cmbAB.val();
    });

    btnAnnulerExoneration.on('click', function (e) {
        e.preventDefault();
        annulerExoneration();
    });

    btnRapport.on('click', function (e) {
        e.preventDefault();
        if (dataExonerationPrint.length != 0) {
            printDataExoneration();
        } else {
            alertify.alert('Aucune donnée n\'est disponnible pour imprimer ce rapport.');
        }
    });

    dateDebut.val(getDateFormatYyMmDd(getDateToday()));
    dateFin.val(getDateFormatYyMmDd(getDateToday()));

    getDataExonerees();
    setArticleBudgetaire();

});

function getDataExonerees() {

    $.ajax({
        type: 'POST',
        url: 'exonerations_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getListExoneration',
            'fkSite': userData.SiteCode,
            'fkAgent': userData.idUser,
            'fkAB': cmb_articleB,
            'dateDebut': dateDebut.val(),
            'dateFin': dateFin.val(),
            'assujetti': inputAssujettisExoneres.val(),
            'typeRecherche': typeRecherche
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /><h5>Chargement en cours ...</h5>'});
        },
        success: function (data) {

            $.unblockUI();

            if (data == '-1') {
                showResponseError();
                return;
            } else if (data == '0') {
                var table = [];
                setTableExonerees(table);
            }

            setTableExonerees(data);
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function setTableExonerees(data) {

    rechercheAvanceeExoneres.modal('hide');

    var content = '';
    content += '<thead style="background-color:#0085c7;color:white">';
    content += '<tr>';
    content += '<th style="text-align:left">ASSUJETTIS</th>';
    content += '<th style="text-align:left">INTITULE BIEN</th>';
    content += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    content += '<th style="text-aLign:left">DATE EXONERATION</th>';
    content += '<th style="text-aLign:left">MONTANT EXONERE</th>';
    content += '<th style="text-aLign:left">PERIODE</th>';
    content += '<th></th>';
    content += '</tr>';
    content += '</thead>';
    content += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        var descriptionBien = '<span style="font-weight:bold;color:red;">' + data[i].intituleBien + '</span>';
        var descriptionBien2 = data[i].intituleBien;

        if (data[i].isImmobilier === '1') {
            var intituleBien = '<span style="font-weight:bold;color:red;">' + data[i].intituleBien + '</span>';
            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + data[i].typeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + data[i].usageName + '</span>';
            var categorieInfo = 'Cat\351gorie : ' + '<span style="font-weight:bold">' + data[i].tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + data[i].communeName + '</span>';
            var quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + data[i].quartierName + '</span>';
            var adresseBien = 'Adresse : ' + '<span style="font-weight:bold">' + data[i].adresseBien.toUpperCase() + '</span>';
            descriptionBien = intituleBien + '<hr/>' + natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<br/>' + adresseBien;
            descriptionBien2 = data[i].intituleBien + '\n' + 'Nature : ' + data[i].typeBien + '\n' + 'Usage : ' + data[i].usageName + '\n' + 'Cat\351gorie : ' + data[i].tarifName + '\n' + 'Commune : ' + data[i].communeName2 + '\n' + 'Quartier : ' + data[i].quartierName + '\n' + 'Adresse : ' + data[i].adresseBien.toUpperCase();
        }

        var exon = new Object();
        exon.assujetti = data[i].noms.toUpperCase();
        exon.descriptionBien = descriptionBien2;
        exon.articleB = data[i].articleB;
        exon.dateExoneration = data[i].dateExoneration;
        exon.montant = data[i].montant + ' ' + data[i].devise;
        exon.periode = data[i].periode;
        dataExonerationPrint.push(exon);

        if (data[i].devise == 'CDF') {
            total_CDF += data[i].montant;
        } else if (data[i].devise == 'USD') {
            total_USD += data[i].montant;
        }

        content += '<tr>';
        content += '<td style="text-align:left;width:58%;vertical-align:middle;"><i class="fa fa-user"></i>&nbsp;&nbsp;' + data[i].noms.toUpperCase() + '</td>';
        content += '<td style="text-align:left;width:15%;vertical-align:middle">' + descriptionBien + '</td>';
        content += '<td style="text-align:left;width:10%;vertical-align:middle">' + data[i].articleB + '</td>';
        content += '<td style="text-align:center;width:5%;vertical-align:middle">' + data[i].dateExoneration + '</td>';
        content += '<td style="text-align:center;width:8%;vertical-align:middle">' + data[i].montant + ' ' + data[i].devise + '</td>';
        content += '<td style="text-align:center;width:2%;vertical-align:middle">' + data[i].periode + '</td>';
        content += '<td style="text-align:center;width:2%;vertical-align:middle"><a onclick="setAnnulerExoneration(\'' + data[i].code + '\')" class="btn btn-danger" title="D\351sactiver le bien"><i class="fa fa-eraser"></i>&nbsp;Annuler</a></td>';
        content += '</tr>';
    }

    content += '</tbody>';
    $('#tableExonerees').html(content);
    $('#tableExonerees').DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 12,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        columnDefs: [
            {"visible": false, "targets": 0}
        ],
        order: [[0, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(0, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="6">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

}

function setArticleBudgetaire() {

    var option = '';
    option += '<option value="*" selected >S\351lectionner un article</option>';
    option += '<option value="' + AB_IF + '">Impot foncier (IF)</option>';
    option += '<option value="' + AB_IRL + '">Impot sur la Retenue Locative (IRL)</option>';
    option += '<option value="' + AB_RL + '">Revenu Locatif (RL)</option>';
    cmbAB.html(option);

}

//function setEtat() {
//
//    var option = '';
//    option += '<option value="*" selected >S\351lectionner un \351tat</option>';
//    option += '<option value="0">Impot foncier (IF)</option>';
//    option += '<option value="1">Impot sur la Retenue Locative (IRL)</option>';
//    etatExoneration.html(option);
//
//}

function annulerExoneration() {

    if (inputObservation.val() === '') {
        alertify.alert('Veuillez fournir une observation concernant cette annulation');
        return;
    }
    alertify.confirm('Etes-vous sûr de vouloir annuler cette exon\351ration ?', function () {

        $('.annulerExoneration').modal('hide');

        $.ajax({
            type: 'POST',
            url: 'exonerations_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'annulerExoneration',
                'code': codeExo,
                'userId': userData.idUser,
                'observation': inputObservation.val()
            },
            beforeSend: function () {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /><h5>Traitement en cours ...</h5>'});
            },
            success: function (data) {

                $.unblockUI();

                if (data == '-1') {
                    showResponseError();
                    return;
                } else if (data == '0') {
                    alertify.alert('L\'annulation de cette exon\351ration n\'a pas abouti. Veuillez r\351essayer ou contacter l\'administrateur.');
                    $('.annulerExoneration').modal('show');
                    return;
                }

                alertify.alert('L\'annulation de cette exon\351ration a \351t\351 faite avec succès');

                getDataExonerees();
            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });

    });
}

function setAnnulerExoneration(code) {
    codeExo = code;
    $('.annulerExoneration').modal('show');
}

function printDataExoneration() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    var columns = [], columnStyles;
    var carEspace = '';

    if (dataExonerationPrint.length > 0) {

//        if (checkSearch) {
//            position = 270;
//        } else {
        var critere = '';
        docTitle = carEspace.substring(0, critere.length * 2) + docTitle + ' : ' + critere;
        position = 250;
//        }
        hauteur = 160;

    } else {
        position = 160;
    }

    carEspace = '                                            ';
    docTitle = carEspace + "DIRECTION\n                         Liste des assujettis exonérés".toUpperCase();
    columns = [
        {title: "ASSUJETTI", dataKey: "assujetti"},
        {title: "DESCRIPTION DU BIEN", dataKey: "descriptionBien"},
        {title: "ARTICLE BUDGETAIRE", dataKey: "articleB"},
        {title: "DATE EXONER.", dataKey: "dateExoneration"},
        {title: "MONTANT", dataKey: "montant"},
        {title: "PERIODE", dataKey: "periode"}
    ];

    columnStyles = {
        assujetti: {columnWidth: 95, fontSize: 8, overflow: 'linebreak'},
        descriptionBien: {columnWidth: 280, overflow: 'linebreak', fontSize: 9},
        articleB: {columnWidth: 165, overflow: 'linebreak', fontSize: 9},
        dateExoneration: {columnWidth: 85, overflow: 'linebreak', fontSize: 9},
        montant: {columnWidth: 65, overflow: 'linebreak', fontSize: 9},
        periode: {columnWidth: 65, overflow: 'linebreak', fontSize: 9}
    };


    var rows = dataExonerationPrint;
    var pageContent = function (data) {

        var bureau = userData.site;
        setDocParams(doc, docTitle, position, day, month, year, hh, mm, ss, data, bureau);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: columnStyles,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 5 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');

    doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(total_CDF), 540, finalY + 25);
    doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(total_USD), 540, finalY + 40);


//    doc.text(getNameChefBureau(codeBureauSelected), 540, finalY + 65);
//    doc.text(getNameChefTaxateur(codeBureauSelected, codeABSelected), 40, finalY + 65);

    window.open(doc.output('bloburl'), '_blank');
}