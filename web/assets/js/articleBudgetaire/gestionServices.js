/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tableServices, inputIntitule;
var id_maj = '0', serviceCode, indice = 2, intitule, code_service, etat, intituleMinistere;
var listServices = [], listMinisteres = [];
var btnNewMinistere, btnNewService, btnNouveau;

$(function () {

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('Services d\'assiette');

    removeActiveMenu();
    linkMenuGestionArticleBudgetaire.addClass('active');

    btnEnregistrer = $('#btnEnregistrer');
    dateDebut = $('#dateDebut');
    dateCloture = $('#dateCloture');
    inputIntitule = $('#inputIntitule');
    tableServices = $('#tableServices');
    btnNewMinistere = $('#btnNewMinistere');
    btnNewService = $('#btnNewService');
    btnNouveau = $('#btnNouveau');

    services_ajax('loadMinistere', 'Chargement en cours ...');
//    services_ajax('loadServices', 'Chargement en cours ...');

    btnNouveau.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        $('#lblTitle').html('Ajouter un nouveau service');
        btnNewMinistere.html('Ajouter');
        inputIntitule.val('');
        $('#addService').modal('show');
        serviceCode = undefined;
    });

    btnNewMinistere.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        intitule = inputIntitule.val();

        if (inputIntitule.val() == '') {
            alertify.alert('Veuillez saisir l\'intitulé de ce service avant de continuer.');
            return;
        }

        var titleSaving;
        if (serviceCode == undefined) {
            titleSaving = 'Etes-vous sûr de vouloir enregistrer ce service ?';
        } else {
            titleSaving = 'Etes-vous sûr de vouloir enregistrer ce service ?';
        }

        if (id_maj == '0') {
            alertify.confirm(titleSaving, function () {

                services_ajax('addService', 'Enregistrement en cours ...');

            });

        } else {

            alertify.confirm('Etes-vous sûr de vouloir modifier ce service ?', function () {

                services_ajax('updateService', 'Modification en cours ...');

            });
        }
    });

});

function services_ajax(operation, message_excepting) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': operation,
            'userId': userData.idUser,
            'intitule': intitule,
            'code_ministere': serviceCode,
            'code_service': code_service,
            'etat': etat,
            'code': indice
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>' + message_excepting + '</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            if (operation == 'loadServices') {
                listServices = response;
            } else {
                if (operation == 'loadMinistere') {
                    listMinisteres = response;
                    listServices = response;
                } else if (operation == 'addService') {
                    if (response == '1') {
                        if (serviceCode == undefined) {
                            alertify.alert('Le service a été ajouté avec succès.');
                        } else {
                            alertify.alert('Le service a été ajouté avec succès.');
                        }
                        $('#addService').modal('hide');
                    } else {
                        if (serviceCode == undefined) {
                            alertify.alert('L\'ajouté de ce service a échoué. Veuillez réessayer.');
                        } else {
                            alertify.alert('L\'ajouté de ce service a échoué. Veuillez réessayer.');
                        }

                    }
                    services_ajax('loadMinistere', 'Chargement en cours ...');
                } else if (operation == 'updateService') {
                    if (response == '1') {
                        if (serviceCode == undefined) {
                            alertify.alert('L\'intitulé de ce service a été modifié avec succès.');
                        } else {
                            alertify.alert('L\'intitulé de ce service a été modifié avec succès.');
                        }
                        $('#addService').modal('hide');
                    } else {
                        if (serviceCode == undefined) {
                            alertify.alert('La modification de ce service a échoué. Veuillez réessayer.');
                        } else {
                            alertify.alert('La modification de ce service a échoué. Veuillez réessayer.');
                        }
                    }
                    services_ajax('loadMinistere', 'Chargement en cours ...');
                } else if (operation == 'activeAndDeactiveService') {
                    if (response == '1') {
                        if (etat == '1') {
                            if (serviceCode == undefined) {
                                alertify.alert('Le service de ' + intituleMinistere + ' vient d\'être désactivé avec succès.');
                            } else {
                                alertify.alert('Le service de ' + intituleMinistere + ' vient d\'être désactivé avec succès.');
                            }

                            $('#addService').modal('hide');
                        } else if (etat == '0') {
                            if (serviceCode == undefined) {
                                alertify.alert('Le service de ' + intituleMinistere + ' vient d\'être activé avec succès.');
                            } else {
                                alertify.alert('Le service de ' + intituleMinistere + ' vient d\'être activé avec succès.');
                            }

                            $('#addService').modal('hide');
                        }
                    } else {
                        if (etat == '1') {
                            if (serviceCode == undefined) {
                                alertify.alert('La désactivation de ce service a échoué.');
                            } else {
                                alertify.alert('La désactivation de ce service a échoué.');
                            }

                        } else if (etat == '0') {
                            if (serviceCode == undefined) {
                                alertify.alert('L\'activation de ce service a échoué..');
                            } else {
                                alertify.alert('L\'activation de ce service a échoué..');
                            }

                        }
                    }
                    services_ajax('loadMinistere', 'Chargement en cours ...');
                }
                printServices(response);
            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printServices(serviceList) {

    var header = '';
    header += '<thead style="background-color:#0085c7;color:white">';
    header += '<tr>';
    header += '<th scope="col"> </th>';
    header += '<th scope="col" style="display:none"> CODE </th>';
    header += '<th scope="col"> INTITULE </th>';
    header += '<th scope="col"> ETAT </th>';
    header += '<th scope="col"></th>';
    header += '</tr>';
    header += '</thead>';

    var body = '<tbody>';

    for (var i = 0; i < serviceList.length; i++) {
        if (serviceList[i].fkService == '0') {
            body += '<tr>';
            body += '<td class="details-control" style="width:5%;vertical-align:middle;text-align:center;"></td>';
            body += '<td style="display:none">' + serviceList[i].code + '</td>';
            body += '<td style="width:70%;vertical-align:middle">' + serviceList[i].intitule.toUpperCase() + '</td>';
            if (serviceList[i].etat == '1') {
                body += '<td style="width:15%;vertical-align:middle;text-align:right"><span>Activer&nbsp;&nbsp;</span><button style="background:#cc0000;box-shadow:2px 2px 8px #aaa;font:bold 13px Arial;border-radius:50%;border:none;color:#fff;" type="button" onclick="activer_desactiverService(\'' + serviceList[i].code + '\',\'0\',\'' + serviceList[i].intitule + '\',\'' + i + '\')" class="btn btn-danger" title="Désactiver"><i class="fa fa-close"></i></button></td>';
            } else {
                body += '<td style="width:15%;vertical-align:middle;text-align:right"><span style="color:red;">Désactiver&nbsp;&nbsp;</span><button style="background:#5cb85c;box-shadow:2px 2px 8px #aaa;font:bold 13px Arial;border-radius:50%;border:none;color:#fff;" type="button" onclick="activer_desactiverService(\'' + serviceList[i].code + '\',\'1\',\'' + serviceList[i].intitule + '\',\'' + i + '\')" class="btn btn-success" title="Activer"><i class="fa fa-check"></i></button></td>';
            }
            body += '<td style="width:10%;vertical-align:middle;text-align:center"><button type="button" onclick="updateService(\'' + serviceList[i].code + '\',\'' + serviceList[i].intitule + '\',\'' + i + '\')" class="btn btn-primary" title="Modification"><i class="fa fa-edit"></i></button></td>';
            body += '</tr>';
        }
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableServices.html(tableContent);

    var tableDetail = tableServices.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableServices tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = tableDetail.row(tr);

        var dataDetail = tableDetail.row(tr).data();

        serviceCode = dataDetail[1];
        indice = 1;

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');
        } else {

            var count = 0;
            for (var i = 0; i < listServices.length; i++) {

                if (listServices[i].fkService == serviceCode) {
                    count += count + 1;
                }
            }

            tr.addClass('shown');

            var detailTable = '';

            detailTable += '<div class="row" style="margin-bottom: 5px;">'
                    + '<div class="col-lg-12">'
                    + '<button type="button" class="btn btn-success" onclick="setNewService()" style="float: right">'
                    + '<i class="fa fa-save"></i>  Ajouter un service'
                    + '</button>'
                    + '</div>'
                    + '</div>';

            if (count == 0) {
                detailTable += '<center><h5 style="background-color:#e6ceac;color:black"> AUCUN SERVICE </h5></center>';
                row.child(detailTable).show();
                detailTable += '<div>'
                        + '<button type="button" style="margin-top:5px; margin-bottom: 5px; margin-right: 5px; margin-left: 5px" onclick="setDivNewUser()" class="btn btn-primary pull-right" title="Ajouter nouveau"><i class="fa fa-plus"></i></button>'
                        + '</div>';
            } else {

                detailTable += '<table class="table table-bordered">';
                detailTable += '<thead><tr style="background-color:#e6ceac;color:black">';
                detailTable += '<td style="text-align:center"> SERVICE </td>';
                detailTable += '<td style="text-align:center"> ETAT </td>';
                detailTable += '<td style="text-align:right"> </td>';
                detailTable += '<tbody>';

                for (var i = 0; i < listServices.length; i++) {

                    if (listServices[i].fkService == serviceCode) {
                        detailTable += '<tr>';
                        detailTable += '<td style="width:70%;vertical-align:middle;text-align:left">' + listServices[i].intitule + '</td>';
                        if (listServices[i].etat == '1') {
                            detailTable += '<td style="width:15%;vertical-align:middle;text-align:right"><span>Activer&nbsp;&nbsp;</span><button style="background:#cc0000;box-shadow:2px 2px 8px #aaa;font:bold 13px Arial;border-radius:50%;border:none;color:#fff;" type="button" onclick="activer_desactiverService(\'' + listServices[i].code + '\',\'0\',\'' + addslashes(listServices[i].intitule) + '\',\'1\')" class="btn btn-danger" title="Désactiver"><i class="fa fa-close"></i></button></td>';
                        } else {
                            detailTable += '<td style="width:15%;vertical-align:middle;text-align:right"><span style="color:red;">Désactiver&nbsp;&nbsp;</span><button style="background:#5cb85c;box-shadow:2px 2px 8px #aaa;font:bold 13px Arial;border-radius:50%;border:none;color:#fff;" type="button" onclick="activer_desactiverService(\'' + listServices[i].code + '\',\'1\',\'' + addslashes(listServices[i].intitule) + '\',\'1\')" class="btn btn-success" title="Activer"><i class="fa fa-check"></i></button></td>';
                        }
                        detailTable += '<td style="width:10%;vertical-align:middle;text-align:center"><button type="button" onclick="updateService(\'' + listServices[i].code + '\',\'' + addslashes(listServices[i].intitule) + '\',\'' + i + '\')" class="btn btn-primary" title="Modification"><i class="fa fa-edit"></i></button></td>';
                        detailTable += '</tr>';
                    }

                }

                detailTable += '</tbody>';
                detailTable += '</tbody>';
                detailTable += '<tfoot>';

                row.child(detailTable).show();
            }
        }
    });
}

function activer_desactiverService(code, etat_service, intitule, i) {
    code_service = code;
    etat = etat_service;
    intituleMinistere = intitule;

    var active_deactive;
    if (i != '1') {
        active_deactive = 'ministère';
    } else {
        active_deactive = 'service';
    }

    if (etat == '0') {
        alertify.confirm('Etes-vous sûr de vouloir activer ce ' + active_deactive + ' ?', function () {
            services_ajax('activeAndDeactiveService', 'Désactivation en cours...');
        });
    } else {
        alertify.confirm('Etes-vous sûr de vouloir désactiver ce '+active_deactive+' ?', function () {
            services_ajax('activeAndDeactiveService', 'Activation en cours...');
        });
    }

}

function updateService(code, intitule, i) {
    code_service = code;
    $('#div_code').attr('style', 'display:block');

    inputIntitule.val(intitule);

    id_maj = '1';

    $('#lblTitle').html('Modifier un ministère');
    btnNewMinistere.html('Modifier');
    $('#addService').modal('show');
}

function addslashes(ch) {
    ch = ch.replace(/\\/g, "\\\\");
    ch = ch.replace(/\'/g, "\\'");
    ch = ch.replace(/\"/g, "\\\"");
    return ch;
}

function setNewService() {
    $('#lblTitle').html('Ajouter un nouveau service');
    btnNewMinistere.html('Ajouter');
    inputIntitule.val('');
    $('#addService').modal('show');
}