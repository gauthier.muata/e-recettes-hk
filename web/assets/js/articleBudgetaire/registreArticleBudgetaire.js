/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var cmbMinistere, cmbServiceAssiette, cmbFormeJuridique;
var cmbMinistereRegistre, cmbServiceRegistre;
var tableFaitGenerateur;
var DivNewFaitGenerateur;
var BtnNouveau, BtnEnregistrer, BtnDismiss,
        btnModifierArticle, btnResearchTaxation;

var inputResearchTaxation;

var inputgenerateur;
var codeFaitGenerateur = '';
var tableDetailTaxation;
var tableTaxation;
var faitGenerateurList;
var listArticlebugdetaire;

var codeMinisterRegistre = '';
var codeServiceRegistre = '';

$(function () {

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('Registre des articles budgétaires');

    removeActiveMenu();
    loadArticleBugdtaire('');
    tableTaxation = $('#tableTaxation');
    linkMenuGestionArticleBudgetaire.addClass('active');

    inputgenerateur = $('#inputgenerateur');
    inputResearchTaxation = $('#inputResearchTaxation');

    cmbMinistere = $('#cmbMinistere');
    cmbServiceAssiette = $('#cmbServiceAssiette');
    cmbFormeJuridique = $('#cmbFormeJuridique');

    cmbServiceRegistre = $('#cmbServiceRegistre');
    cmbMinistereRegistre = $('#cmbMinistereRegistre');

    btnModifierArticle = $('#btnModifierArticle');
    BtnNouveau = $('#BtnNouveau');
    BtnEnregistrer = $('#BtnEnregistrer');
    BtnDismiss = $('#BtnDismiss');

    btnResearchTaxation = $('#btnResearchTaxation');

    cmbMinistere.on('change', function () {
        var code = cmbMinistere.val();
        if (code == '0') {
            cmbServiceAssiette.html('');
            DivNewFaitGenerateur.hide();
            printFaitsGenerateurs('');
            return;
        }
        loadServicesByMinistere(code);
    });

    cmbMinistereRegistre.on('change', function () {
        codeMinisterRegistre = cmbMinistereRegistre.val();
        loadServicesByMinistere(codeMinisterRegistre);
    });

    cmbServiceRegistre.on('change', function () {
        codeServiceRegistre = cmbServiceRegistre.val();
        getArticleBugdtaireByService(codeServiceRegistre);
    });

    cmbServiceAssiette.on('change', function () {
        var code = cmbServiceAssiette.val();
        if (code == '0') {
            DivNewFaitGenerateur.hide();
            printFaitsGenerateurs('');
            return;
        }
        loadFaitGenerateursByService(code);
    });

    cmbFormeJuridique.on('change', function () {
        var intitule = cmbFormeJuridique.val();
        inputgenerateur = $('# inputgenerateur');
    });

    BtnNouveau.click(function (e) {
        e.preventDefault();
        if (cmbMinistere.val() == '0') {
            alertify.alert('Veuillez sélectionner un ministere.');
            return;
        }

        if (cmbServiceAssiette.val() == '' || cmbServiceAssiette.val() == 'null' || cmbServiceAssiette.val() == '0') {
            alertify.alert('Veuillez sélectionner un service.');
            return;
        }
        BtnEnregistrer.html('<i class="fa fa-save"></i> Enregister');
        codeFaitGenerateur = '';
        DivNewFaitGenerateur.show();
    });


    BtnEnregistrer.click(function (e) {
        e.preventDefault();
        if (inputgenerateur.val() == '' || inputgenerateur.val() == 'null' || inputgenerateur.val() == '0') {
            alertify.alert('Veuillez saisir un fait generateur.');
            return;
        }
        if (cmbFormeJuridique.val() == '' || cmbFormeJuridique.val() == 'null' || cmbFormeJuridique.val() == '0') {
            alertify.alert('Veuillez selectionner une forme juridique.');
            return;
        }
        var action;
        if (codeFaitGenerateur == '') {

            if (checkDoublon()) {
                alertify.alert('ce fait generateur existe deja');
                return;
            }

            action = 'enregistrer ?';
        } else {
            action = 'modifier ?';
        }

        var message = 'Etes-vous sûrs de vouloir ' + action;

        alertify.confirm(message, function () {
            saveFaitGenerateur(1);
        });
    });


    BtnDismiss.click(function (e) {
        e.preventDefault();
        DivNewFaitGenerateur.hide();

    });

    btnResearchTaxation.click(function (e) {
        e.preventDefault();
        if (inputResearchTaxation.val() == '' || inputResearchTaxation.val() == 'null') {
            alertify.alert('Veuillez d\'abord fournir le critère de recherche.');
            return;
        }
        if (codeMinisterRegistre != '') {
            if (codeServiceRegistre == '' || codeServiceRegistre == 'null') {
                alertify.alert('Veuillez d\'abord sélectionner un service.');
                return;
            }

        }
        getArticleBugdtaireByIntitule();
    });

    btnModifierArticle.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var objetArticle = preparerModificationArticle();

        if (objetArticle == undefined) {
            alertify.alert('Veuillez d\'abord cochez sur un article budgétaire avant de procéder à sa modification');
            return;
        } else {
            var valueAB = '<span style="font-weight:bold">' + objetArticle.intitule.toUpperCase() + '</span>';

            alertify.confirm('Voulez-vous modifier l\'article budgétaire :  ' + valueAB + ' ?', function () {

                window.location = 'gestion-article-budgetaire?code=' + btoa(objetArticle.codeArticle);

            });
        }
    });

    tableFaitGenerateur = $('#tableFaitGenerateur');
    DivNewFaitGenerateur = $('#DivNewFaitGenerateur');


    loadMinistere();
    loadformeJuridique();
    loadFaitGenerateursByService('');
    loadTaxationTable('');

});

function loadArticleBugdtaire(codearticle) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadArticleBudgetaire',
            'code_article': codearticle
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            listArticlebugdetaire = $.parseJSON(JSON.stringify(response));
            loadTaxationTable(listArticlebugdetaire);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}
function loadMinistere() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadMinistere',
            'code': '1'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            var ministereList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--</option>';

            for (var i = 0; i < ministereList.length; i++) {
                data += '<option value="' + ministereList[i].code + '">' + ministereList[i].intitule + '</option>';
            }
            cmbMinistere.html(data);
            cmbMinistereRegistre.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadServicesByMinistere(codeMinistere) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadServices',
            'code_ministere': codeMinistere,
            'code': '1'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            var serviceList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--</option>';

            for (var i = 0; i < serviceList.length; i++) {
                data += '<option value="' + serviceList[i].code + '">' + serviceList[i].intitule.toUpperCase() + '</option>';
            }
            cmbServiceAssiette.html(data);
            cmbServiceRegistre.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadFaitGenerateursByService(codeService) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadFaitGenerateurs',
            'code_service': codeService
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            faitGenerateurList = $.parseJSON(JSON.stringify(response));
            printFaitsGenerateurs(faitGenerateurList);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printFaitsGenerateurs(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> MINISTERE </th>';
    header += '<th scope="col"> SERVICE D\'ASSIETTE </th>';
    header += '<th scope="col"> FAIT GENERATEUR </th>';
    header += '<th scope="col"> FORME JURIDIQUE </th>';
    header += '<th scope="col"></th>';
    header += '<th scope="col"></th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < result.length; i++) {
        body += '<tr>';
        body += '<td>' + result[i].ministere + '</td>';
        body += '<td>' + result[i].service + '</td>';
        body += '<td>' + result[i].intitule + '</td>';
        body += '<td>' + result[i].intituleForme + '</td>';

        body += '<td><button type="button" onclick="editFaitGenerateur(\'' + result[i].code + '\')" class="btn btn-success" title="Modification"><i class="fa fa-edit"></i></button></td>';
        body += '<td><button type="button" onclick="removeFaitGenerateur(\'' + result[i].code + '\')" class="btn btn-danger" title="Suppression"><i class="fa fa-trash"></i></button></td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableFaitGenerateur.html(tableContent);
    tableFaitGenerateur.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}


function loadTaxationTable(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:42%"scope="col">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:left;width:33%"scope="col">FAITS GENERATEURS</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">PERIODICITE</th>';
    tableContent += '<th style="width:5%;text-align:center"scope="col"></th>';
//    tableContent += '<th style="text-align:center;width:2%"scope="col">CLE DE RP</th>';
    tableContent += '<th hidden="true" scope="col"> Code article </th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';



    for (var i = 0; i < result.length; i++) {

        var firstLineAB = '';

        if (result[i].intitule.length > 250) {
            firstLineAB = result[i].intitule.substring(0, 250).toUpperCase() + ' ...';
        } else {
            firstLineAB = result[i].intitule.toUpperCase();
        }

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;title="' + result[i].intitule.toUpperCase() + '"><span style="font-weight:bold;"></span>' + firstLineAB.toUpperCase() + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + result[i].faitgenerateur.toUpperCase() + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + result[i].intituleperiode.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:center"><center><input onclick="checkArticle(\'' + result[i].codeArticle + '\')" type="checkbox" id="checkBoxSelectArticle' + result[i].codeArticle + '" name="checkBoxSelectArticle' + result[i].codeArticle + '"></center></td>';
//        tableContent += '<td style="vertical-align:middle;">' + result[i].clerepartion + '</td>';
        tableContent += '<td hidden="true">' + result[i].codeArticle + '</td>';
        tableContent += '</tr>';

    }
    tableContent += '</tbody>';
    tableTaxation.html(tableContent);

    var myDataTable = tableTaxation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par article ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: true,
        pageLength: 50,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

}

function loadformeJuridique() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadFormeJuridique'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            var formejuridiqueList = $.parseJSON(JSON.stringify(response));

            console.log(formejuridiqueList)
            var data = '<option value="0">--</option>';

            for (var i = 0; i < formejuridiqueList.length; i++) {
                data += '<option value="' + formejuridiqueList[i].codeFormeJuridique + '">' + formejuridiqueList[i].libelleFormeJuridique + '</option>';
            }
            cmbFormeJuridique.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function editFaitGenerateur(code) {

    for (var i = 0; i < faitGenerateurList.length; i++) {
        if (faitGenerateurList[i].code == code) {
            inputgenerateur.val(faitGenerateurList[i].intitule);
            cmbFormeJuridique.val(faitGenerateurList[i].codeForme);
            BtnEnregistrer.html('<i class="fa fa-save"></i> Modifier');
            codeFaitGenerateur = code;
            DivNewFaitGenerateur.show();
            break;
        }
    }
}

function saveFaitGenerateur(etat) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveFaitGenerateur',
            'code': codeFaitGenerateur,
            'intitule': inputgenerateur.val(),
            'codeService': cmbServiceAssiette.val(),
            'codeForme': cmbFormeJuridique.val(),
            'etat': etat
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Opération en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            if (etat == 1) {
                var message;
                if (codeFaitGenerateur == '') {
                    message = 'Fait générateur enregistré avec succès!';
                } else {
                    message = 'Fait générateur modifié avec succès!';
                }

                inputgenerateur.val('');
                cmbFormeJuridique.val('0');

                alertify.alert(message);
                DivNewFaitGenerateur.hide();
            } else {
                alertify.alert('Fait générateur supprimé avec succès!');
            }

            codeFaitGenerateur = '';
            BtnEnregistrer.html('<i class="fa fa-save"></i> Enregister');

            loadFaitGenerateursByService(cmbServiceAssiette.val());
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function removeFaitGenerateur(code) {

    alertify.confirm('Etes-vous sûrs de vouloir supprimer ?', function () {
        codeFaitGenerateur = code;
        saveFaitGenerateur(0);
    });

}

function checkDoublon() {
    for (var i = 0; i < faitGenerateurList.length; i++) {
        if (faitGenerateurList[i].intitule == inputgenerateur) {

        }
    }
    return true;
}

function checkArticle(codeArticle) {

    for (var i = 0; i < listArticlebugdetaire.length; i++) {
        if (codeArticle != listArticlebugdetaire[i].codeArticle) {
            var control = document.getElementById("checkBoxSelectArticle" + listArticlebugdetaire[i].codeArticle);
            control.checked = false;
        }
    }
}

function preparerModificationArticle() {

    for (var i = 0; i < listArticlebugdetaire.length; i++) {

        var selectArticle = '#checkBoxSelectArticle' + listArticlebugdetaire[i].codeArticle;

        var select = $(selectArticle);

        if (select.is(':checked')) {

            var articleObjet = new Object();

            articleObjet.codeArticle = listArticlebugdetaire[i].codeArticle;
            articleObjet.intitule = listArticlebugdetaire[i].intitule;

        }

    }

    return articleObjet;
}

function getArticleBugdtaireByService(codeService) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getArticleBudgetaireByService',
            'codeService': codeService
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                loadTaxationTable('');
            }

            listArticlebugdetaire = $.parseJSON(JSON.stringify(response));
            if (listArticlebugdetaire.length > 0) {
                loadTaxationTable(listArticlebugdetaire);
            } else {
                loadTaxationTable('');
            }

        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function getArticleBugdtaireByIntitule() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getArticleBudgetaireByIntitule',
            'intituleArticle': inputResearchTaxation.val(),
            'codeMinistere': codeMinisterRegistre,
            'codeService': codeServiceRegistre
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                loadTaxationTable('');
            }

            listArticlebugdetaire = $.parseJSON(JSON.stringify(response));
            if (listArticlebugdetaire.length > 0) {
                loadTaxationTable(listArticlebugdetaire);
            } else {
                loadTaxationTable('');
            }
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}



