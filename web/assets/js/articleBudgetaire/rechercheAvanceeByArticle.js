/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalRechercheAvanceeByArticle;

var selectAB;
var selectAnnee_1, selectTypeRegistre;
var selectMois;
var lblMois_1;
var selectMois_1;
var selectService_, selectMinistere;

var btnAdvencedSearchByArticle;

var AllServiceList, AllSiteList, UsersSiteList;

var codePeriodicite;
var codeABSelected;

var listAB = [];

var objSelectedAB = {};
var selectPeriode;
var yearCurrent;

$(function () {

    modalRechercheAvanceeByArticle = $('#modalRechercheAvanceeByArticle');

    selectAB = $('#selectAB');
    selectAnnee_1 = $('#selectAnnee_1');
    selectMois_1 = $('#selectMois_1');
    lblMois = $('#lblMois_1');
    selectService_ = $('#selectService_');
    selectMinistere = $('#selectMinistere');
    selectPeriode = $('#selectPeriode');
    btnAdvencedSearchByArticle = $('#btnAdvencedSearchByArticle');

    UsersSiteList = JSON.parse(userData.siteUserList);
    AllSiteList = JSON.parse(userData.allSiteList);
    AllServiceList = JSON.parse(userData.allServiceList);

    selectAB.change(function (e) {
        e.preventDefault();

        getABbyCode(selectAB.val());

    });

    /*selectService_.on('change', function (e) {
     e.preventDefault();
     loadArticlesBudgetairesAssujettissable();
     });*/

    getService();
    loadMois();
    loadAnnee();
    loadArticlesBudgetairesAssujettissable();
});

function loadArticlesBudgetairesAssujettissable() {

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'researchArticleBudgetaireAssujettissable',
            'codeService': selectService_.val()
        },
        beforeSend: function () {
        },
        success: function (response)
        {

            if (response == '-1') {
                selectAB.attr('disabled', true);
                showResponseError();
                return;
            } else if (response == '0') {
                selectAB.attr('disabled', true);
                //alertify.alert('Ce service : ' + $('#selectService_ option:selected').text().toUpperCase() + ' n\'a pas des actes générateurs actifs.');
                return;
            } else {
                setTimeout(function () {
                    listAB = null;
                    listAB = JSON.parse(JSON.stringify(response));
                    consituateABList();
                    selectAB.attr('disabled', false);
                }
                , 1);
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            showResponseError();
        }

    });
}

function getService() {

    var dataService = '';
    var selectServiceContent = '';

    var codeSite = '';

    if (!controlAccess('VIEW_ALL_SITE')) {

        //dataService += '<option value="0">--</option>';

        for (var i = 0; i < UsersSiteList.length; i++) {
            dataService += '<option value="' + UsersSiteList[i].SiteCode + '">' + UsersSiteList[i].siteName + '</option>';
            codeSite = UsersSiteList[i].SiteCode;
        }

        selectService_.html(dataService);
        selectService_.val(codeSite);

    } else {

        dataService += '<option value="*">TOUS LES BUERAUX</option>';

        for (var i = 0; i < UsersSiteList.length; i++) {
            dataService += '<option value="' + UsersSiteList[i].SiteCode + '">' + UsersSiteList[i].siteName + '</option>';

        }
        selectServiceContent = dataService;

        selectService_.html(selectServiceContent);
    }

    /*if (!selectService_.val('*')) {
     selectAB.attr('disabled', false);
     }*/

}

/*function getService_() {
 
 var dataService = '';
 var selectServiceContent = '';
 
 if (!controlAccess('SEARCH_FOR_ALL_SERVICES')) {
 
 dataService += '<option value="' + userData.serviceCode + '">' + userData.serviceAssiette + '</option>';
 selectServiceContent = dataService;
 
 selectService_.html(selectServiceContent);
 
 } else {
 
 dataService += '<option value="*">*</option>';
 
 for (var i = 0; i < AllServiceList.length; i++) {
 dataService += '<option value="' + AllServiceList[i].serviceCode + '">' + AllServiceList[i].serviceName + '</option>';
 
 }
 selectServiceContent = dataService;
 
 selectService_.html(selectServiceContent);
 }
 
 if (!selectService_.val('*')) {
 selectAB.attr('disabled', false);
 }
 
 }*/

function loadAnnee() {

    var anneeDebut = 2015;
    var date = new Date();
    var anneeFin = date.getFullYear() + 5;

    var dataAnnee = '<option value ="0"> ' + "--" + '</option>';

    while (anneeDebut <= anneeFin) {
        dataAnnee += '<option value =' + anneeDebut + '> ' + anneeDebut + '</option>';
        anneeDebut++;
    }
    
    dataAnnee += '<option value ="2050"> ' + "TOUTES LES ANNEES" + '</option>';
    
    selectPeriode.html(dataAnnee);
    selectPeriode.val(date.getFullYear());
    yearCurrent = date.getFullYear();
    selectPeriode.attr('disabled', false);
}

function loadMois() {

    var date = new Date();
    var mois = date.getMonth() + 1;


    var dataMois = '<option value ="0"> ' + "--" + '</option>';
    dataMois += '<option value =1> Janvier </option>';
    dataMois += '<option value =2> Février </option>';
    dataMois += '<option value =3> Mars </option>';
    dataMois += '<option value =4> Avril </option>';
    dataMois += '<option value =5> Mai </option>';
    dataMois += '<option value =6> Juin </option>';
    dataMois += '<option value =7> Juillet </option>';
    dataMois += '<option value =8> Aôut </option>';
    dataMois += '<option value =9> Septembre </option>';
    dataMois += '<option value =10> Octobre </option>';
    dataMois += '<option value =11> Novembre </option>';
    dataMois += '<option value =12> Décembre </option>';
    
    dataMois += '<option value ="13"> ' + "TOUS LES MOIS" + '</option>';
    
    selectPeriode.html(dataMois.toUpperCase());
    selectPeriode.val(mois - 1);
    selectPeriode.attr('disabled', false);
}

function consituateABList() {

    var dataABList = '';

    dataABList += '<option value="0">--</option>';

    for (var i = 0; i < listAB.length; i++) {
        objSelectedAB = listAB[0];
        dataABList += '<option value =' + listAB[i].codeArticleBudgetaire + '>' + listAB[i].libelleArticleBudgetaire.toUpperCase() + '</option>';
    }

    selectAB.html(dataABList);
    getABbyCode(selectAB.val());
}

function getABbyCode(code) {

    for (var i = 0; i < listAB.length; i++) {

        if (listAB[i].codeArticleBudgetaire == code) {

            codePeriodicite = listAB[i].codePeriodiciteAB;
            codeABSelected = code;

            objSelectedAB = listAB[i];
            hideMonth(codePeriodicite);
            break;
        }

    }
}

function hideMonth(periodicity) {
    if (periodicity == 'PR0042015') {
        selectMois_1.hide();
        lblMois.hide();
    } else {
        selectMois_1.show();
        lblMois.show();
    }
}

