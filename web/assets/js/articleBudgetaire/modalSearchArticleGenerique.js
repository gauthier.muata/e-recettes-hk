/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalCategorieBudgetaire;

var tableResultArticleGenerique;

var inputValueResearchArticleGenerique;

var btnLauchSearchArticleGenerique, btnSelectArticleGenerique;

$(function () {
    
    modalCategorieBudgetaire = $('#modalCategorieBudgetaire');
    
    tableResultArticleGenerique = $('#tableResultArticleGenerique');
    
    btnLauchSearchArticleGenerique = $('#btnLauchSearchArticleGenerique');
    btnSelectArticleGenerique = $('#btnSelectArticleGenerique');
    
    inputValueResearchArticleGenerique = $('#inputValueResearchArticleGenerique');
    
    
    btnLauchSearchArticleGenerique.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        searchArticleGenerique();

    });
    
    inputValueResearchArticleGenerique.on('keypress', function (e) {
        if (e.keyCode == 13) {
            searchArticleGenerique();
        }

    });
    
    printArticleGeneriqueTable('');
    
});


function searchArticleGenerique() {

    if (inputValueResearchArticleGenerique.val() === empty || inputValueResearchArticleGenerique.val().length < SEARCH_MIN_TEXT) {
        showEmptySearchMessage();
        return;
    }
    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchArticleGenerique.val(),
            'operation': 'getArticleGenerique'
        },
        beforeSend: function () {
            modalCategorieBudgetaire.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                modalCategorieBudgetaire.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnSelectArticleGenerique.hide();
                    printArticleGeneriqueTable('');
                    return;
                } else {
                    var dataArticleGenerique = JSON.parse(JSON.stringify(response));
                    if (dataArticleGenerique.length > 0) {
                        btnSelectArticleGenerique.show();
                        printArticleGeneriqueTable(dataArticleGenerique);
                    } else {
                        btnSelectArticleGenerique.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalCategorieBudgetaire.unblock();
            showResponseError();
        }

    });
}


function printArticleGeneriqueTable(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:60%">ACTE GENERATEUR</th>';
    tableContent += '<th style="text-align:left;width:40%">SERCICE D\'ASSIETTE</th>';
    tableContent += '<th hidden="true" scope="col">Code intitule</th>';
    tableContent += '<th hidden="true" scope="col">Code service</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;width:60%;text-align:left">' + data[i].intituleArticleGenerique.toUpperCase() + '</td>';
        tableContent += '<td style="vertical-align:middle;width:40%;text-align:left">' + data[i].intituleService.toUpperCase() + '</td>';
        tableContent += '<td hidden="true">' + data[i].codeArticleGenerique + '</td>';
        tableContent += '<td hidden="true">' + data[i].codeService + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultArticleGenerique.html(tableContent);

    var myDt = tableResultArticleGenerique.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 15,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableResultArticleGenerique tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        selectArticleGeneriqueData = new Object();
        selectArticleGeneriqueData.intituleArticleGenerique = myDt.row(this).data()[0].toUpperCase();
        selectArticleGeneriqueData.intituleService = myDt.row(this).data()[1];
        selectArticleGeneriqueData.codeArticleGenerique = myDt.row(this).data()[2];
        selectArticleGeneriqueData.codeService = myDt.row(this).data()[3];

    });
}