/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tablePalier, tableResultArticleGenerique;

var modalCategorieBudgetaire, modalDocumentOfficiel, modalTarif;

var inputCategorieArticle, inputDocumentOfficiel, inputIntituleArticle,
        inputCodeOfficiel, inputArticleMere, inputPriseChargeDeclaration,
        inputPriseChargePenalite, inputNbrJourLimite, inputTaux;

var inputBorneMinimale, inputBorneMaximale, inputTarif,
        inputValueResearchArticleGenerique, inputTarifArticle;

var checkBoxAssujettissable, checkBoxProprietaire, checkBoxPeriodeVariable,
        checkBoxQteVariable, checkBoxTauxtransactionnel, checkBoxPalier;

var checkBoxMultiplierBase, checkBoxBorneSuperInfinie;

var BtnCategorieArticle, BtnDocumentOfficiel, BtnArticleMere,
        btnAjouterPalier, btnLauchSearchArticleGenerique, btnTarif;

var btnSelectArticleGenerique, btnSelectDocumentOfficiel,
        btnSelectTarif, BtnTarifArticle, BtnUniteArticle;

var cmbNatureArticleBudgetaire, cmbPeriodicite, cmbFormeJuridique,
        cmbTypeTaux, cmbDeviseDefaut, cmbUnite, cmbUniteArticle,
        cmbCommune, cmbVille, cmbQuartier, cmbNatureBien;

var codeArticleGenerique, codeService, codeDocumentOfficiel,
        codeNatureArticle, codeUniteArticle,
        codeFormeJuridique, codeDevise, codeCommune, codeQuartier;

var codeTarif = '';

var codeVille = '';

var codeTarifArticle = '';

var checkNatureArticle = '';

var codePeriodicite, objetTarif, checkValBase,
        checkedPalier, checkedAssujettissable,
        checkPeriodeVariable, checkProprietaire,
        checkTauxTransactionnel, checkQteVariable;

var dataCheckPalier = false;

var modifierPalier = false;

var dataCheckNatureAB = false;

var lblService, codePaliers, codeTypeTaux;

var codeAgentCreat, codeUniteArticle;

var codeEditerArticle = '';

var codeSelectedQuartier = '';
var codeSelectedCommune = '';

var etatRemove = 0;

var valeurRemove = 1;

var listTarif = [];
var palierList = [];

var dataPalierList = [];

var paliersList;

$(function () {

    var urlCode = getUrlParameter('code');
    if (jQuery.type(urlCode) !== 'undefined') {
        codeEditerArticle = atob(urlCode);
        dataCheckPalier = true;
        getNatureArticle();
        initDataArticle();

    }

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('Edition article budgétaire');

    removeActiveMenu();

    tablePalier = $('#tablePalier');
    tableResultArticleGenerique = $('#tableResultArticleGenerique');

    modalCategorieBudgetaire = $('#modalCategorieBudgetaire');
    modalDocumentOfficiel = $('#modalDocumentOfficiel');
    modalTarif = $('#modalTarif');

    BtnCategorieArticle = $('#BtnCategorieArticle');
    BtnDocumentOfficiel = $('#BtnDocumentOfficiel');
    BtnArticleMere = $('#BtnArticleMere');
    btnAjouterPalier = $('#btnAjouterPalier');
    btnEnregistrerArticle = $('#btnEnregistrerArticle');
    btnLauchSearchArticleGenerique = $('#btnLauchSearchArticleGenerique');
    btnTarif = $('#btnTarif');

    btnSelectArticleGenerique = $('#btnSelectArticleGenerique');
    btnSelectDocumentOfficiel = $('#btnSelectDocumentOfficiel');
    btnSelectTarif = $('#btnSelectTarif');
    BtnTarifArticle = $('#BtnTarifArticle');
    BtnUniteArticle = $('#BtnUniteArticle');

    checkBoxAssujettissable = $('#checkBoxAssujettissable');
    checkBoxProprietaire = $('#checkBoxProprietaire');
    checkBoxPeriodeVariable = $('#checkBoxPeriodeVariable');
    checkBoxQteVariable = $('#checkBoxQteVariable');
    checkBoxTauxtransactionnel = $('#checkBoxTauxtransactionnel');
    checkBoxPalier = $('#checkBoxPalier');

    checkBoxMultiplierBase = $('#checkBoxMultiplierBase');
    checkBoxBorneSuperInfinie = $('#checkBoxBorneSuperInfinie');

    inputCategorieArticle = $('#inputCategorieArticle');
    inputDocumentOfficiel = $('#inputDocumentOfficiel');
    inputIntituleArticle = $('#inputIntituleArticle');
    inputCodeOfficiel = $('#inputCodeOfficiel');
    inputArticleMere = $('#inputArticleMere');
    inputPriseChargeDeclaration = $('#inputPriseChargeDeclaration');
    inputPriseChargePenalite = $('#inputPriseChargePenalite');
    inputNbrJourLimite = $('#inputNbrJourLimite');
    inputTaux = $('#inputTaux');


    inputBorneMinimale = $('#inputBorneMinimale');
    inputBorneMaximale = $('#inputBorneMaximale');
    inputTarif = $('#inputTarif');
    inputValueResearchArticleGenerique = $('#inputValueResearchArticleGenerique');
    inputTarifArticle = $('#inputTarifArticle');

    lblService = $('#lblService');

    cmbNatureArticleBudgetaire = $('#cmbNatureArticleBudgetaire');
    cmbPeriodicite = $('#cmbPeriodicite');
    cmbFormeJuridique = $('#cmbFormeJuridique');
    cmbTypeTaux = $('#cmbTypeTaux');
    cmbDeviseDefaut = $('#cmbDeviseDefaut');
    cmbUnite = $('#cmbUnite');
    cmbUniteArticle = $('#cmbUniteArticle');
    cmbCommune = $('#cmbCommune');
    cmbVille = $('#cmbVille');
    cmbQuartier = $('#cmbQuartier');
    cmbNatureBien = $('#cmbNatureBien');

    checkValBase = document.getElementById("checkBoxMultiplierBase").checked;
    checkedPalier = document.getElementById("checkBoxPalier").checked;
    checkedAssujettissable = document.getElementById("checkBoxAssujettissable").checked;
    checkPeriodeVariable = document.getElementById("checkBoxPeriodeVariable").checked;
    checkProprietaire = document.getElementById("checkBoxProprietaire").checked;
    checkTauxTransactionnel = document.getElementById("checkBoxTauxtransactionnel").checked;
    checkQteVariable = document.getElementById("checkBoxQteVariable").checked;

    inputNbrJourLimite.val(0);
    inputBorneMinimale.val(0);
    inputBorneMaximale.val(0);
    codeFormeJuridique = "*";
    inputBorneMaximale.attr('readOnly', true);
    inputBorneMinimale.attr('readOnly', true);
    inputArticleMere.attr('disabled', true);
    
    inputTarifArticle.attr('disabled', true);
    
    inputCategorieArticle.attr('disabled', true);
    inputDocumentOfficiel.attr('disabled', true);
    
    BtnArticleMere.attr('disabled', true);

    BtnCategorieArticle.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalCategorieBudgetaire.modal('show');
    });

    BtnUniteArticle.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalEditeUnite.modal('show');
    });

    BtnDocumentOfficiel.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalDocumentOfficiel.modal('show');
    });

    btnTarif.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        sessionStorage.setItem('checkTarif', 1);

        modalTarif.modal('show');
    });

    BtnTarifArticle.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        sessionStorage.setItem('checkTarif', 2);

        modalTarif.modal('show');
    });

    btnEnregistrerArticle.click(function (e) {

        e.preventDefault();

//        if (checkSession()) {
//            showSessionExpiredMessage();
//            return;
//        }


        if (cmbNatureArticleBudgetaire.val() === '0') {
            alertify.alert('Veuillez d\'abord fournir la nature de l\'article budgétaire');
            return;
        }

        if (inputTarifArticle.val().trim() == '' || inputTarifArticle.val() === '') {
            alertify.alert('Veuillez d\'abord fournir le tarif de l\'article budgétaire');
            return;
        }

        if (cmbUniteArticle.val() === '0') {
            alertify.alert('Veuillez d\'abord sélectionner une unité !');
            return;
        }

        if (inputNbrJourLimite.val() < 0) {
            alertify.alert('La valeur fournie est incorrecte.');
            return;
        }

        if (cmbPeriodicite.val() === '0') {
            alertify.alert('Veuillez d\'abord sélectionner une période !');
            return;
        }

        if (dataCheckNatureAB) {
            if (inputPriseChargeDeclaration.val().trim() == '' || inputPriseChargeDeclaration.val() === '') {
                alertify.alert('Veuillez d\'abord fournir la date de la déclaration');
                return;
            }
            if (inputPriseChargePenalite.val().trim() == '' || inputPriseChargePenalite.val() === '') {
                alertify.alert('Veuillez d\'abord fournir la date limite du paiement');
                return;
            }
        }



        if (dataCheckPalier) {
            if (dataPalierList.length <= 0) {
                alertify.alert('Veuillez d\'abord ajouter au moins un palier.');
                return;
            }
            palierList = JSON.stringify(dataPalierList);
        } else {
            if (listTarif.length <= 0) {
                alertify.alert('Veuillez d\'abord ajouter au moins un palier.');
                return;
            }
            palierList = JSON.stringify(listTarif);
        }

        alertify.confirm('Etes-vous sûre de vouloir enregistrer cet article budgétaire ?', function () {
            saveArticleBudgetaire();

        });

    });

    btnAjouterPalier.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbFormeJuridique.val() == '0' || cmbFormeJuridique.val() == '') {
            alertify.alert('Veuillez d\'abord sélectionner un type personne');
            return;
        }

        if (codeTarif == '') {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un tarif avant d\'ajouter le palier !');
            return;
        }

        if (checkTauxTransactionnel) {

            var borneMaximal = parseInt(inputBorneMaximale.val());
            var borneMinimale = parseInt(inputBorneMinimale.val());

            if (borneMinimale < 0 || borneMinimale == 0) {
                alertify.alert('La valeur fournie pour la borne minimale est incorrecte.');
                return;
            }

            if (borneMaximal < 0 || borneMaximal == 0) {
                alertify.alert('La valeur fournie pour la borne supérieure est incorrecte.');
                return;
            }

            if (borneMaximal < borneMinimale) {
                alertify.alert('Les valeurs fournies sont incorrectes.');
                return;
            }

        } else {
            inputBorneMaximale.val(0);
            inputBorneMinimale.val(0)
        }

        if (inputTaux.val() === empty) {
            alertify.alert('Veuillez d\'abord fournir le taux.');
            return;
        }

        if (cmbDeviseDefaut.val() == '0') {
            alertify.alert('Veuillez d\'abord fournir la devise.');
            return;
        }

        if (cmbUnite.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner l\'unité.');
            return;
        }
        if (cmbCommune.val() == '0') {
            cmbCommune.val('');
        }
        if (cmbVille.val() === '0') {
            cmbVille.val('');
        }
//        if (cmbQuartier.val() == '0') {
//            alertify.alert('Veuillez d\'abord sélectionner le quartier');
//            return;
//        }

//        if (cmbNatureBien.val() == '0') {
//            alertify.alert('Veuillez d\'abord sélectionner la nature du bien');
//            return;
//        }


        if (dataCheckPalier) {

            if (modifierPalier) {
                alertify.confirm('Etes-vous sûre de vouloir modifier ce palier ?', function () {
                    updatePalier();
                    return;
                });
            } else {
                alertify.confirm('Etes-vous sûre de vouloir ajouter ce palier ?', function () {
                    addTarifList()
                    return;
                });
            }



        } else {

            etatRemove = parseInt(etatRemove) - parseInt(1);

            alertify.confirm('Etes-vous sûre de vouloir ajouter ce palier ?', function () {
                addTarifList();
                return;
            });
        }


    });

    btnSelectArticleGenerique.click(function (e) {

        getSelectedArticleGeneriqueData();

        if (selectArticleGeneriqueData.codeArticleGenerique === empty) {
            alertify.alert('Veuillez d\'abord sélectionner un article générique');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir sélectionner <b>' + selectArticleGeneriqueData.intituleArticleGenerique + '</b> ?', function () {
            modalCategorieBudgetaire.modal('hide');
        });
    });

    btnSelectDocumentOfficiel.click(function (e) {

        getSelectedDocumentOfficielData();

        if (selectDocumentOfficielData.codeDocumentOfficiel === empty) {
            alertify.alert('Veuillez d\'abord sélectionner un document officiel');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir sélectionner <b>' + selectDocumentOfficielData.numeroArrete + '</b> ?', function () {
            modalDocumentOfficiel.modal('hide');
        });
    });

    btnSelectTarif.click(function (e) {

        var checkTarifs = sessionStorage.getItem('checkTarif');

        if (checkTarifs == 1) {
            getSelectedTarifData();
        } else {
            getSelectedTarifArticleData()
        }



        if (selectTarifData.codeTarif === empty) {
            alertify.alert('Veuillez d\'abord sélectionner un tarif');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir sélectionner <b>' + selectTarifData.intituleTarif + '</b> ?', function () {
            modalTarif.modal('hide');
        });
    });

    cmbNatureArticleBudgetaire.on('change', function () {
        codeNatureArticle = cmbNatureArticleBudgetaire.val();
        if (checkNatureArticle == codeNatureArticle) {
            dataCheckNatureAB = true;
        } else {
            dataCheckNatureAB = false;
        }
    });

    cmbUniteArticle.on('change', function () {
        codeUniteArticle = cmbUniteArticle.val();
    });

    cmbTypeTaux.on('change', function () {
        codeTypeTaux = cmbTypeTaux.val();
    });

    cmbPeriodicite.on('change', function () {
        codePeriodicite = cmbPeriodicite.val();
    });

    cmbDeviseDefaut.on('change', function () {
        codeDevise = cmbDeviseDefaut.val();
    });

    cmbFormeJuridique.on('change', function () {

        if (cmbFormeJuridique.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner une forme juridique');
            return;
        } else {
            codeFormeJuridique = cmbFormeJuridique.val();
        }
    });

    cmbCommune.on('change', function () {
        codeCommune = cmbCommune.val();
//        loadQuartierByCommune(codeCommune);
        loadEntiteAdministrativeFille(codeCommune, 2);
    });

    cmbVille.on('change', function () {

        if (cmbVille.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner une ville');
            return;
        } else {
            codeVille = '';
            codeVille = cmbVille.val();
            loadCommuneByVille(codeVille);
        }

    });

    cmbQuartier.on('change', function () {
        codeQuartier = cmbQuartier.val();
    });

    checkBoxMultiplierBase.on('change', function () {
        checkValBase = document.getElementById("checkBoxMultiplierBase").checked;
    });

    checkBoxPalier.on('change', function () {
        checkedPalier = document.getElementById("checkBoxPalier").checked;
    });

    checkBoxAssujettissable.on('change', function () {
        checkedAssujettissable = document.getElementById("checkBoxAssujettissable").checked;
    });

    checkBoxPeriodeVariable.on('change', function () {
        checkPeriodeVariable = document.getElementById("checkBoxPeriodeVariable").checked;
    });

    checkBoxProprietaire.on('change', function () {
        checkProprietaire = document.getElementById("checkBoxProprietaire").checked;
    });

    checkBoxTauxtransactionnel.on('change', function () {
        checkTauxTransactionnel = document.getElementById("checkBoxTauxtransactionnel").checked;

        if (checkTauxTransactionnel) {
            inputBorneMaximale.attr('readOnly', false);
            inputBorneMinimale.attr('readOnly', false);
        } else {
            inputBorneMaximale.attr('readOnly', true)
            inputBorneMinimale.attr('readOnly', true)
            inputBorneMaximale.val(0);
            inputBorneMinimale.val(0)
        }
    });

    checkBoxQteVariable.on('change', function () {
        checkQteVariable = document.getElementById("checkBoxQteVariable").checked;
    });



    printPalier(empty);

    if (!dataCheckPalier) {
        getNatureArticle();
        initDataArticle();
    }

    initDataNatureBien();

});

function getSelectedArticleGeneriqueData() {

    inputCategorieArticle.val(selectArticleGeneriqueData.intituleArticleGenerique.toUpperCase());
    codeArticleGenerique = selectArticleGeneriqueData.codeArticleGenerique;
    lblService.html(selectArticleGeneriqueData.intituleService.toUpperCase());
}

function getSelectedDocumentOfficielData() {
    inputDocumentOfficiel.val(selectDocumentOfficielData.intituleDocumentOfficiel + " : " + selectDocumentOfficielData.numeroArrete);
    codeDocumentOfficiel = selectDocumentOfficielData.codeDocumentOfficiel;
}
function getSelectedTarifData() {
    //inputTarif.val(selectTarifData.intituleTarif + " : " + selectTarifData.valeurTarif);
    inputTarif.val(selectTarifData.intituleTarif.toUpperCase());
    codeTarif = selectTarifData.codeTarif;
}
function getSelectedTarifArticleData() {
    //inputTarifArticle.val(selectTarifData.intituleTarif + " : " + selectTarifData.valeurTarif);
    inputTarifArticle.val(selectTarifData.intituleTarif.toUpperCase());
    codeTarifArticle = selectTarifData.codeTarif;
}

function getNatureArticle() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadNatureArticleBudgetaire'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else {
                    var dataNatureArticle = $.parseJSON(JSON.stringify(response));

                    var dataNatureArticles = '<option value ="0">-- Sélectionner --</option>';
                    for (var i = 0; i < dataNatureArticle.length; i++) {
                        dataNatureArticles += '<option value =' + dataNatureArticle[i].codeNatureArticle + '>' + dataNatureArticle[i].intitule.toUpperCase() + '</option>';
                    }
                    cmbNatureArticleBudgetaire.html(dataNatureArticles);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function initDataArticle() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'initDataArticle'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                var dataPeriodiciteList = JSON.parse(result.periodicite);
                if (dataPeriodiciteList.length > 0) {
                    var dataPeriodicite = '<option value ="0">--</option>';
                    for (var i = 0; i < dataPeriodiciteList.length; i++) {
                        dataPeriodicite += '<option value =' + dataPeriodiciteList[i].codePeriodicite + '>' + dataPeriodiciteList[i].intitulePeriodicite.toUpperCase() + '</option>';
                    }
                    cmbPeriodicite.html(dataPeriodicite);
                }

                var dataDeviseList = JSON.parse(result.devise);
                if (dataDeviseList.length > 0) {
                    var dataDevise = '<option value ="0">--</option>';
                    for (var i = 0; i < dataDeviseList.length; i++) {
                        dataDevise += '<option value =' + dataDeviseList[i].codeDevise + '>' + dataDeviseList[i].intituleDevise.toUpperCase() + '</option>';
                    }
                    cmbDeviseDefaut.html(dataDevise);
                }

                var dataUniteList = JSON.parse(result.unite);
                if (dataUniteList.length) {
                    var dataUnite = '<option value ="0">--</option>';
                    for (var i = 0; i < dataUniteList.length; i++) {
                        dataUnite += '<option value =' + dataUniteList[i].codeUnite + '>' + dataUniteList[i].intituleUnite.toUpperCase() + '</option>';
                    }
                    cmbUnite.html(dataUnite);
                    cmbUniteArticle.html(dataUnite);
                }

                var dataFormeJuridiqueList = JSON.parse(result.formeJurique);
                if (dataFormeJuridiqueList.length > 0) {
                    var dataFormeJuridique = '<option value ="0">--</option>';
                    for (var i = 0; i < dataFormeJuridiqueList.length; i++) {
                        dataFormeJuridique += '<option value =' + dataFormeJuridiqueList[i].codeFormeJuridique + '>' + dataFormeJuridiqueList[i].intituleFormeJurique.toUpperCase() + '</option>';
                    }
                    cmbFormeJuridique.html(dataFormeJuridique);
                }

                var dataEntiteList = JSON.parse(result.entiteAdministrative);
                if (dataEntiteList.length > 0) {
                    var dataEntite = '<option value ="0">--</option>';
                    for (var i = 0; i < dataEntiteList.length; i++) {
                        dataEntite += '<option value =' + dataEntiteList[i].codeEntite + '>' + dataEntiteList[i].intituleEntite.toUpperCase() + '</option>';
                    }
                    cmbVille.html(dataEntite);
                }


                checkNatureArticle = result.checkNatureArticle;


                if (dataCheckPalier) {
                    getInfosArticleBudgetaire(codeEditerArticle);
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function addTarifList() {

    objetTarif = new Object();

    objetTarif.codeTarif = codeTarif;
    objetTarif.code = '';
    objetTarif.agentCreat = userData.idUser;
    objetTarif.borneMinimale = inputBorneMinimale.val();
    objetTarif.borneMaximale = inputBorneMaximale.val();
    objetTarif.codeTypeTaux = cmbTypeTaux.val();
    objetTarif.typeTaux = $('#cmbTypeTaux option:selected').text();
    objetTarif.taux = inputTaux.val();
    objetTarif.tarif = inputTarif.val();
    objetTarif.codeFormeJuridique = codeFormeJuridique;
    objetTarif.intituleFormeJurique = $('#cmbFormeJuridique option:selected').text();
    objetTarif.codeDevise = cmbDeviseDefaut.val();
    objetTarif.devise = $('#cmbDeviseDefaut option:selected').text();
    objetTarif.codeUnite = cmbUnite.val();
    objetTarif.unite = $('#cmbUnite option:selected').text();
    objetTarif.intituleEntite = $('#cmbVille option:selected').text();
    objetTarif.codeEntite = cmbVille.val();
    objetTarif.intituleCommune = $('#cmbCommune option:selected').text();
    objetTarif.codeCommune = cmbCommune.val();
    objetTarif.intituleQuartier = $('#cmbQuartier option:selected').text();
    objetTarif.intituleNature = $('#cmbNatureBien option:selected').text();
    objetTarif.codeNature = cmbNatureBien.val();
    objetTarif.codeQuartier = cmbQuartier.val();
    objetTarif.checkedValBase = document.getElementById('checkBoxMultiplierBase').checked;
    objetTarif.etat = 1;
    objetTarif.code = etatRemove;

    if (dataCheckPalier) {
        dataPalierList.push(objetTarif);

        printPalier(dataPalierList);

    } else {
        listTarif.push(objetTarif);

        printPalier(listTarif);
    }

    codeTarif = '';
    inputBorneMinimale.val('');
    inputBorneMaximale.val('');
    inputTaux.val('');
    inputTarif.val('');
    document.getElementById('checkBoxMultiplierBase').checked = false;

    objetTarif = new Object();
}

function printPalier(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="text-align:center;width:11%" scope="col"> BORNE MINIMALE </th>';
    header += '<th style="text-align:center;width:11%" scope="col"> BORNE MAXIMALE </th>';
    header += '<th style="text-align:center;width:10%" scope="col"> TAUX </th>';
    header += '<th style="text-align:left;width:10% scope="col"> DEVISE </th>';
    header += '<th style="text-align:left;width:25%" scope="col"> TARIF </th>';
    header += '<th style="text-align:left;width:15%" scope="col"> TYPE PERSONNE </th>';
    header += '<th style="text-align:left;width:15%" scope="col"> QUARTIER </th>';
    header += '<th style="text-align:left;width:15%" scope="col"> NATURE </th>';
    header += '<th style="text-align:center;width:5%" scope="col"> MULTIPLIER </th>';
    header += '<th scope="col"style="width:5%"></th>';
    header += '<th hidden="true" scope="col">Code</th>';
    header += '<th hidden="true" scope="col">Code unite</th>';
    header += '<th hidden="true" scope="col">Code entite</th>';
    header += '<th hidden="true" scope="col">Code forme</th>';
    header += '<th hidden="true" scope="col">CheckValeur</th>';
    header += '<th hidden="true" scope="col">Etat Remove</th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var typeTauxInfo = ' (' + result[i].typeTaux + ')';

        body += '<tr id="tr_' + result[i].code + '">';
        body += '<td style="text-align:center;vertical-align:middle">' + result[i].borneMinimale + '</td>';
        body += '<td style="text-align:center;vertical-align:middle">' + result[i].borneMaximale + '</td>';
        body += '<td style="text-align:center;vertical-align:middle">' + result[i].taux + typeTauxInfo  + '<hr/>' + 'PAR : ' + result[i].unite.toUpperCase()+ '</td>';
        body += '<td style="text-align:center;vertical-align:middle">' + result[i].codeDevise + '</td>';
        body += '<td style="text-align:left;vertical-align:middle">' + result[i].tarif.toUpperCase() + '</td>';
        body += '<td style="text-align:left;vertical-align:middle">' + result[i].intituleFormeJurique.toUpperCase() + '</td>';
        body += '<td style="text-align:left;vertical-align:middle">' + result[i].intituleQuartier.toUpperCase() + '</td>';
        body += '<td style="text-align:left;vertical-align:middle">' + result[i].intituleNature.toUpperCase() + '</td>';
        if (result[i].checkedValBase) {
            body += '<td style="text-align:center;vertical-align:middle"><center><input type="checkbox" checked id="checkBoxSelectValBase' + result[i].codeTarif + '" name="checkBoxSelectValBase' + result[i].codeTarif + '"></center></td>';
        } else {
            body += '<td style="text-align:center;vertical-align:middle"><center><input type="checkbox" id="checkBoxSelectValBase' + result[i].codeTarif + '" name="checkBoxSelectValBase' + result[i].codeTarif + '"></center></td>';
        }
        body += '<td style="vertical-align:middle;text-align:center">'
                + '<a onclick="preparationUpdate(\'' + result[i].code + '\')" class="btn btn-success" title="Modifier le palier"><i class="fa fa-edit"></i>&nbsp;&nbsp;Modifier</a><br/><br/>'
                + '<a onclick="removePalier(\'' + result[i].code + '\')" class="btn btn-danger" title="Désactiver le palier"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Supprimer</a></td>'
        body += '<td hidden="true">' + result[i].code + '</td>';
        body += '<td hidden="true">' + result[i].codeUnite + '</td>';
        body += '<td hidden="true">' + result[i].codeQuartier + '</td>';
        body += '<td hidden="true">' + result[i].codeFormeJuridique + '</td>';
        body += '<td hidden="true">' + result[i].checkedValBase + '</td>';
        body += '<td hidden="true">1</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tablePalier.html(tableContent);
    var dtTablePalier = tablePalier.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 50,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function saveArticleBudgetaire() {


    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveArticlebudgetaire',
            'intituleArticle': inputIntituleArticle.val(),
            'codeArticleGenerique': codeArticleGenerique,
            'codeUniteArticle': cmbUniteArticle.val(),
            'checkedPalier': checkedPalier,
            'codePeriodicite': cmbPeriodicite.val(),
            'checkedAssujettissable': checkedAssujettissable,
            'codeTarifArticle': codeTarifArticle,
            'codeOfficiel': inputCodeOfficiel.val(),
            'codeNatureArticle': cmbNatureArticleBudgetaire.val(),
            'codeDocumentOfficiel': codeDocumentOfficiel,
            'priseChargeDeclaration': inputPriseChargeDeclaration.val(),
            'priseChargePenalite': inputPriseChargePenalite.val(),
            'nbrJourLimite': inputNbrJourLimite.val(),
            'checkPeriodeVariable': checkPeriodeVariable,
            'checkProprietaire': checkProprietaire,
            'checkTauxTransactionnel': checkTauxTransactionnel,
            'checkQteVariable': checkQteVariable,
            'agentCreat': userData.idUser,
            'codeArticle': codeEditerArticle,
            'palierList': palierList
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du dépôt déclaration en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'opération effectuée avec succès.');
                    cleanData();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement de l\'article.');
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });

}

function cleanData() {
    inputIntituleArticle.val('');
    inputCodeOfficiel.val('');
    inputPriseChargeDeclaration.val('');
    inputPriseChargePenalite.val('');
    inputTarif.val('');
    inputTaux.val('');
    inputNbrJourLimite.val(0);
    inputBorneMaximale.val(0);
    inputBorneMinimale.val(0);
    printPalier(empty);
    listTarif = [];
    palierList = [];
    dataPalierList = [];
    etatRemove = 0;
    valeurRemove = 1;
}

function getInfosArticleBudgetaire(codeArticleBudg) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadInfoArticleBudgetaire',
            'codeArticle': codeArticleBudg
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                var result = $.parseJSON(JSON.stringify(response));

                dataArticle(result);

                $.unblockUI();
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function dataArticle(objectArticle) {

    codeArticleGenerique = objectArticle.codeArticleGenerique;
    codeDocumentOfficiel = objectArticle.codeDocumentOfficiel;
    codeTarifArticle = objectArticle.codeTarifArticle;
    codePeriodicite = objectArticle.codePeriodicite;
    codeNatureArticle = objectArticle.codeNatureArticle;
    codeUniteArticle = objectArticle.codeUnite;

    lblService.html(objectArticle.intituleService.toUpperCase());
    inputCategorieArticle.val(objectArticle.intituleArticleGenerique.toUpperCase());
    inputDocumentOfficiel.val(objectArticle.intituleDocumentOfficiel + " : " + objectArticle.numeroArrete);
    inputIntituleArticle.val(objectArticle.intituleArticle.toUpperCase());
    inputCodeOfficiel.val(objectArticle.codeOfficiel);
    inputTarifArticle.val(objectArticle.intituleTarifArticle + " : " + objectArticle.valeurTarifArticle);
    inputPriseChargeDeclaration.val(objectArticle.echeanceLegale);
    inputPriseChargePenalite.val(objectArticle.dateLimiteLegale);
    inputNbrJourLimite.val(objectArticle.nbrJourLimite);

    checkNatureArticle = objectArticle.checkNatureArticle;


    if (objectArticle.periodiciteVariable == 1) {
        checkBoxPeriodeVariable.attr('checked', "true");
    } else {
        checkBoxPeriodeVariable.removeAttr('checked');
    }
    checkPeriodeVariable = document.getElementById("checkBoxPeriodeVariable").checked;

    if (objectArticle.proprietaire == 1) {
        checkBoxProprietaire.attr('checked', "true");
    } else {
        checkBoxProprietaire.removeAttr('checked');
    }
    checkProprietaire = document.getElementById("checkBoxProprietaire").checked;

    if (objectArticle.transactionnel == 1) {
        checkBoxTauxtransactionnel.attr('checked', "true");
    } else {
        checkBoxTauxtransactionnel.removeAttr('checked');
    }
    checkTauxTransactionnel = document.getElementById("checkBoxTauxtransactionnel").checked;

    if (objectArticle.quantiteVariable) {
        checkBoxQteVariable.attr('checked', "true");
    } else {
        checkBoxQteVariable.removeAttr('checked');
    }
    checkQteVariable = document.getElementById("checkBoxQteVariable").checked;

    if (objectArticle.assujettisable) {
        checkBoxAssujettissable.attr('checked', "true");
    } else {
        checkBoxAssujettissable.removeAttr('checked');
    }
    checkedAssujettissable = document.getElementById("checkBoxAssujettissable").checked;

    if (objectArticle.valeurPalier) {
        checkBoxPalier.attr('checked', "true");
    } else {
        checkBoxPalier.removeAttr('checked');
    }
    checkedPalier = document.getElementById("checkBoxPalier").checked;

    cmbNatureArticleBudgetaire.val(objectArticle.codeNatureArticle);
    cmbUniteArticle.val(objectArticle.codeUnite);
    cmbPeriodicite.val(objectArticle.codePeriodicite);

    paliersList = $.parseJSON(objectArticle.palierList);

    var palierData;

    for (var i = 0; i < paliersList.length; i++) {

        palierData = new Object();

        palierData.borneMinimale = paliersList[i].borneMinimale;
        palierData.borneMaximale = paliersList[i].borneMaximale;
        palierData.taux = paliersList[i].taux;
        palierData.typeTaux = paliersList[i].typeTaux;
        palierData.codeTarif = paliersList[i].codeTarif;
        palierData.tarif = paliersList[i].tarif;
        palierData.intituleFormeJurique = paliersList[i].intituleFormeJurique;
        palierData.codeFormeJuridique = paliersList[i].codeFormeJuridique;
        palierData.checkedValBase = paliersList[i].checkedValBase;
        palierData.devise = paliersList[i].devise;
        palierData.codeDevise = paliersList[i].codeDevise;
        palierData.codeUnite = paliersList[i].codeUnite;
        palierData.unite = paliersList[i].unite;
        palierData.codeEntite = paliersList[i].codeEntite;
        palierData.codeCommune = paliersList[i].codeCommune;
        palierData.intituleCommune = paliersList[i].intituleCommune;
        palierData.codeQuartier = paliersList[i].codeQuartier;
        palierData.intituleQuartier = paliersList[i].intituleQuartier;
        palierData.agentCreat = userData.idUser;
        palierData.code = paliersList[i].code;
        palierData.etat = paliersList[i].etat;

        palierData.codeNature = paliersList[i].idNature;
        palierData.intituleNature = paliersList[i].intituleNature;

        codeTarif = paliersList[i].codeTarif;


        dataPalierList.push(palierData);
        printPalier(dataPalierList);


    }
//                cmbFormeJuridique.attr('disabled', 'true');
}

function preparationUpdate(codePalier) {

    for (var i = 0; i < paliersList.length; i++) {

        if (paliersList[i].code == codePalier) {
            alertify.confirm('Voulez-vous modifier ce palier ?', function () {

                codePaliers = paliersList[i].code;
                inputTaux.val(paliersList[i].taux);
                inputBorneMinimale.val(paliersList[i].borneMinimale);
                inputBorneMaximale.val(paliersList[i].borneMaximale);
                inputTarif.val(paliersList[i].tarif);
                codeTarif = paliersList[i].tarif;
                cmbUnite.val(paliersList[i].codeUnite);

                if (paliersList[i].codeEntite != '') {
                    cmbVille.val(paliersList[i].codeEntite);
                    loadCommuneByVille(paliersList[i].codeEntite);
                    //cmbCommune.trigger('click');                    
                } else {
                    cmbVille.val('');
                }

                cmbFormeJuridique.val(paliersList[i].codeFormeJuridique);
                cmbDeviseDefaut.val(paliersList[i].codeDevise);
                cmbTypeTaux.val(paliersList[i].typeTaux);
                cmbNatureBien.val(paliersList[i].idNature);

                if (paliersList[i].checkedValBase) {
                    checkBoxMultiplierBase.attr('checked', "true");
                } else {
                    checkBoxMultiplierBase.removeAttr('checked');
                }
                checkValBase = document.getElementById("checkBoxMultiplierBase").checked;

                if (paliersList[i].codeCommune != '') {
                    loadEntiteAdministrativeFille(paliersList[i].codeCommune, 2);
                    codeSelectedQuartier = paliersList[i].codeQuartier;
                    codeSelectedCommune = paliersList[i].codeCommune;
                } else {
                    cmbQuartier.val('');
                    cmbCommune.val('');
                }

                modifierPalier = true;
            });

            break;
        }
    }
}

function removePalier(code) {

    if (dataCheckPalier) {

        for (var i = 0; i < dataPalierList.length; i++) {
            if (dataPalierList[i].code == code) {
                alertify.confirm('Etes-vous sûre de vouloir rétirer ce palier', function () {
                    dataPalierList[i].etat = 0;
                    $('#tr_' + code).remove();
                });
                break;

            }
        }

    } else {

        for (var i = 0; i < listTarif.length; i++) {
            if (listTarif[i].code == code) {
                alertify.confirm('Etes-vous sûre de vouloir rétirer ce palier', function () {
                    listTarif.splice(i, 1);
                    $('#tr_' + code).remove();
                });
                break;

            }

        }

    }

}

function updatePalier() {

    for (var i = 0; i < dataPalierList.length; i++) {

        if (dataPalierList[i].code == codePaliers) {
            dataPalierList[i].borneMinimale = inputBorneMinimale.val();
            dataPalierList[i].borneMaximale = inputBorneMaximale.val();
            dataPalierList[i].tarif = inputTarif.val();
            dataPalierList[i].taux = inputTaux.val();
            if (cmbTypeTaux.val() == 1) {
                dataPalierList[i].typeTaux = "F";
            } else {
                dataPalierList[i].typeTaux = "%";
            }

            dataPalierList[i].intituleFormeJurique = $('#cmbFormeJuridique option:selected').text();
            dataPalierList[i].codeFormeJuridique = cmbFormeJuridique.val();

            dataPalierList[i].codeCommune = cmbCommune.val();
            dataPalierList[i].intituleCommune = $('#cmbCommune option:selected').text();

            dataPalierList[i].codeQuartier = cmbQuartier.val();
            dataPalierList[i].intituleQuartier = $('#cmbQuartier option:selected').text();

            dataPalierList[i].codeUnite = cmbUnite.val();
            dataPalierList[i].unite = $('#cmbUnite option:selected').text();

            dataPalierList[i].devise = $('#cmbDeviseDefaut option:selected').text();
            dataPalierList[i].codeDevise = cmbDeviseDefaut.val();
            dataPalierList[i].codeNature = cmbNatureBien.val();
            dataPalierList[i].intituleNature = $('#cmbNatureBien option:selected').text();

            dataPalierList[i].checkedValBase = document.getElementById('checkBoxMultiplierBase').checked;

            modifierPalier = false;

            break;
        }

    }
    printPalier(dataPalierList);
}

function loadEntiteAdministrativeFille(codeEntite, niveau) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadEntiteAdministrativeFille',
            'codeEntite': codeEntite,
            'niveau': niveau
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else {
                    var dataEntiteFille = $.parseJSON(JSON.stringify(response));

                    var dataEntiteAdminFille = '<option value ="0">--</option>';
                    if (niveau == '1') {
//                        for (var i = 0; i < dataEntiteFille.length; i++) {
//                            dataEntiteAdminFille += '<option value =' + dataEntiteFille[i].codeCommune + '>' + dataEntiteFille[i].intituleCommune + '</option>';
//                        }
//                        cmbCommune.html(dataEntiteAdminFille);
                    } else {
                        for (var i = 0; i < dataEntiteFille.length; i++) {
                            dataEntiteAdminFille += '<option value =' + dataEntiteFille[i].codeQuartier + '>' + dataEntiteFille[i].intituleQuartier + '</option>';
                        }
                        cmbQuartier.html(dataEntiteAdminFille);

                        if (codeSelectedQuartier != '') {
                            cmbQuartier.val(codeSelectedQuartier);
                        } else {
                            cmbQuartier.val('');
                        }

                    }

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadCommuneByVille(codeEntite) {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadCommuneByVille',
            'codeEntite': codeEntite
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else {

                    var dataEntiteFille = $.parseJSON(JSON.stringify(response));

                    if (dataEntiteFille.length > 0) {
                        codeVille = '';

                        var dataEntiteAdminF = '<option value ="0">--</option>';

                        for (var i = 0; i < dataEntiteFille.length; i++) {
                            dataEntiteAdminF += '<option value =' + dataEntiteFille[i].codeCommune + '>' + dataEntiteFille[i].intituleCommune + '</option>';
                        }
                        cmbCommune.html(dataEntiteAdminF);
                        if (codeSelectedCommune != '') {
                            cmbCommune.val(codeSelectedCommune);
                        } else {
                            cmbCommune.val('');
                        }
                    } else {
                        codeVille = '';
                        cmbCommune.val('');
                    }

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function initDataNatureBien() {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'initDataNatureBien'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else {
                    var natureBienList = $.parseJSON(JSON.stringify(response));

                    var dataNatureBien = '<option value ="0">--</option>';

                    for (var i = 0; i < natureBienList.length; i++) {
                        dataNatureBien += '<option value =' + natureBienList[i].natureCode + '>' + natureBienList[i].natureName + '</option>';
                    }
                    cmbNatureBien.html(dataNatureBien);

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}
function loadEntiteAdministrativeByCode() {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadEntiteByCode',
            'codeEntite': codeEntite
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    alertify.alert('Aucune donnée trouvées');
                    return;
                } else {
                    var dataEntiteFille = $.parseJSON(JSON.stringify(response));

                    var dataEntiteAdminF = '<option value ="0">--</option>';

                    for (var i = 0; i < dataEntiteFille.length; i++) {
                        dataEntiteAdminF += '<option value =' + dataEntiteFille[i].codeEntite + '>' + dataEntiteFille[i].intituleEntite + '</option>';
                    }
//                    cmbCommune.html(dataEntiteAdminF);

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadQuartierByCommune(codeEntite) {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadQuertierByCommune',
            'codeEntite': codeEntite
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
//                    alertify.alert('Aucune donnée trouvées');
                    codeEntite = '';
                    cmbQuartier.html('');
                    return;
                } else {
                    var dataEntiteQuartiers = $.parseJSON(JSON.stringify(response));

                    var dataEntiteQuartier = '<option value ="0">--</option>';

                    for (var i = 0; i < dataEntiteQuartiers.length; i++) {
                        dataEntiteAdmin += '<option value =' + dataEntiteQuartiers[i].codeQuartier + '>' + dataEntiteQuartiers[i].intituleQuartier + '</option>';
                    }
                    cmbQuartier.html(dataEntiteQuartier);

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}


