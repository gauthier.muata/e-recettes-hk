/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var tableResultPeriodicite;

var inputNbrJour, inputPeriodeRecidive,
        inputIntitulePeriodicite;

var btnEnregistrerPeriodicite, btnModifierPeriodicite;

var selectPreiodiciteData, codePeriodicite;

$(function () {

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('Edition des periodicités');

    btnModifierPeriodicite = $('#btnModifierPeriodicite');
    btnEnregistrerPeriodicite = $('#btnEnregistrerPeriodicite');

    tableResultPeriodicite = $('#tableResultPeriodicite');

    inputNbrJour = $('#inputNbrJour');
    inputPeriodeRecidive = $('#inputPeriodeRecidive');
    inputIntitulePeriodicite = $('#inputIntitulePeriodicite');
    
     btnEnregistrerPeriodicite.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputIntitulePeriodicite.val() == '' || inputIntitulePeriodicite.val() == 'null') {
            alertify.alert('Veuillez saisir l\'intitulé de périodicité');
            return;
        }
        if (inputNbrJour.val() == '' || inputNbrJour.val() == 'null' || inputNbrJour.val() < 0) {
            alertify.alert('Veuillez saisir le nombre de jour');
            return;
        }
        if (inputPeriodeRecidive.val() == '' || inputPeriodeRecidive.val() == 'null' || inputPeriodeRecidive.val() < 0) {
            alertify.alert('Veuillez saisir la période recidive.');
            return;
        }

        alertify.confirm('Etes-vous de vouloir enregistrer cette périodicité ?', function () {

            savePeriodicite();

        });
    });
    
    btnModifierPeriodicite.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputIntitulePeriodicite.val() == '' || inputIntitulePeriodicite.val() == 'null') {
            alertify.alert('Veuillez saisir l\'intitulé de périodicité');
            return;
        }
        if (inputNbrJour.val() == '' || inputNbrJour.val() == 'null' || inputNbrJour.val() < 0) {
            alertify.alert('Veuillez saisir le nombre de jour');
            return;
        }
        if (inputPeriodeRecidive.val() == '' || inputPeriodeRecidive.val() == 'null' || inputPeriodeRecidive.val() < 0) {
            alertify.alert('Veuillez saisir la période recidive.');
            return;
        }

        alertify.confirm('Etes-vous de vouloir modifier cette périodicité ?', function () {
            updatePeriodicite();
        });
    });


    loadPeriodicite();

});

function loadPeriodicite() {


    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadPeriodicite'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var perfiodiciteList = JSON.parse(JSON.stringify(response));

                printPeriodicite(perfiodiciteList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printPeriodicite(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col" style="width:60%">INTITULE PERIODICITE</th>';
    tableContent += '<th scope="col" style="width:15%">NOMBRE DE JOUR</th>';
    tableContent += '<th scope="col" style="width:15%">PRIODE RECIDIVE</th>';
    tableContent += '<th scope="col"style="width:5%"></th>';
    tableContent += '<th scope="col"style="width:5%; display:none"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left">' + data[i].intitulePeriodicite + '</td>';
        tableContent += '<td style="text-align:right">' + data[i].nombreJour + '</td>';
        tableContent += '<td style="text-align:right">' + data[i].periodeRecidive + '</td>';
        tableContent += '<td style="width:10px"><button type="button" onclick="removePeriodicite(\'' + data[i].codePeriodicite + '\',\'' + data[i].intitulePeriodicite + '\')" class="btn btn-danger" title="Désactiver tarif"><i class="fa fa-trash"></i></button></td>';
        tableContent += '<td style="text-align:left; display:none">' + data[i].codePeriodicite + '</td>';

        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultPeriodicite.html(tableContent);

    var myDt = tableResultPeriodicite.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par Intitulé periodicité _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 13,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableResultPeriodicite tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        selectPreiodiciteData = new Object();
        selectPreiodiciteData.intitulePeriodicite = myDt.row(this).data()[0].toUpperCase();
        selectPreiodiciteData.nombreJour = myDt.row(this).data()[1].toUpperCase();
        selectPreiodiciteData.periodeRecidive = myDt.row(this).data()[2].toUpperCase();
        selectPreiodiciteData.codePeriodicite = myDt.row(this).data()[4].toUpperCase();
        preparerModificationPeriodicite();
    });
}

function preparerModificationPeriodicite() {
    inputIntitulePeriodicite.val(selectPreiodiciteData.intitulePeriodicite);
    inputNbrJour.val(selectPreiodiciteData.nombreJour);
    inputPeriodeRecidive.val(selectPreiodiciteData.periodeRecidive);
    codePeriodicite = selectPreiodiciteData.codePeriodicite;
    btnEnregistrerPeriodicite.attr('style', 'display:none');
    btnModifierPeriodicite.attr('style', 'display:inline; right: 9%');
}

function removePeriodicite(codePeriodicite, intitule) {
    alertify.confirm('Voulez-vous désactiver ' + intitule + ' ?', function () {
        validateDisablePeriodicite(codePeriodicite);
    });
}

function savePeriodicite() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'savePeriodicite',
            'intitulePeriodicite': inputIntitulePeriodicite.val(),
            'nombreJour': inputNbrJour.val(),
            'periodeRecidive': inputPeriodeRecidive.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de la periodicité en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\enregistrement de la périodicité s\'est effectué avec succès.');
                    loadPeriodicite()
                    inputIntitulePeriodicite.val('');
                    inputNbrJour.val('');
                    inputPeriodeRecidive.val('');
                    btnEnregistrerPeriodicite.attr('style', 'display:inline; right: 7%');
                    btnModifierPeriodicite.attr('style', 'display:none');
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors d\'enregistrement de la periodicité.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function updatePeriodicite() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'updatePeriodicite',
            'intitulePeriodicite': inputIntitulePeriodicite.val(),
            'nombreJour': inputNbrJour.val(),
            'periodeRecidive': inputPeriodeRecidive.val(),
            'codePeriodicite': codePeriodicite
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Mise à jour de la périodicité en cours ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('L\enregistrement de la périodicité s\'est effectué avec succès.');
                    loadPeriodicite();
                    inputIntitulePeriodicite.val('');
                    inputNbrJour.val('');
                    inputPeriodeRecidive.val('');
                    codePeriodicite = '';
                    btnEnregistrerPeriodicite.attr('style', 'display:inline; right: 7%');
                    btnModifierPeriodicite.attr('style', 'display:none');

                } else if (response == '0') {
                     alertify.alert('Une erreur inattendue s\'est produite lors d\'enregistrement de la periodicité.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function validateDisablePeriodicite(codePeriodicite) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'disablePeriodicite',
            'codePeriodicite': codePeriodicite
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Désactivation de périodicité en cours ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                   alertify.alert('La désactivation de périodicité s\'est effectué avec succès.');
                    loadPeriodicite()
                    inputIntitulePeriodicite.val('');
                    inputNbrJour.val('');
                    inputPeriodeRecidive.val('');
                    btnEnregistrerPeriodicite.attr('style', 'display:inline; right: 7%');
                    btnModifierPeriodicite.attr('style', 'display:none');

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de la désactivation de périodicité.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}