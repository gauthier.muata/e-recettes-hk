/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var cmbBanque, cmbCompteBancaire, cmbMinistere,
        cmbServiceAssiette;

var codeBanque, codeCompteBancaire, codeService,
        codeMinistere;

var btnEnregistrerArticle, btnDesactiverArticle;

var tableArticleAffecter, tableArticleBudgetaire;

var articleLists, articleAffecterList;

$(function () {

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('Affectaion d\'un article budgétaire dans une banque');

    removeActiveMenu();
    linkSubMenuAffecterArticleBudgetaireBanque.addClass('active');

    cmbBanque = $('#cmbBanque');
    cmbCompteBancaire = $('#cmbCompteBancaire');
    cmbMinistere = $('#cmbMinistere');
    cmbServiceAssiette = $('#cmbServiceAssiette');

    tableArticleAffecter = $('#tableArticleAffecter');
    tableArticleBudgetaire = $('#tableArticleBudgetaire');

    btnEnregistrerArticle = $('#btnEnregistrerArticle');
    btnDesactiverArticle = $('#btnDesactiverArticle');

    cmbBanque.on('change', function (e) {
        codeBanque = cmbBanque.val();
        loadCompteBancaire(codeBanque);

    });

    cmbCompteBancaire.on('change', function (e) {
        codeCompteBancaire = cmbCompteBancaire.val();
        loadArticleAffecter(codeCompteBancaire);
    });

    cmbMinistere.on('change', function (e) {
        codeMinistere = cmbMinistere.val();
        loadService(codeMinistere);
    });

    cmbServiceAssiette.on('change', function (e) {
        codeService = cmbServiceAssiette.val();
        loadArticleByService(codeService);
    });

    btnEnregistrerArticle.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (codeBanque == '0') {
            alertify.alert('Veuillez selectionner une banque.');
            return;
        }

        if (codeCompteBancaire == '0') {
            alertify.alert('Veuillez selectionner un compte bancaire.');
            return;
        }

        if (codeMinistere == '0') {
            alertify.alert('Veuillez selectionner un ministère.');
            return;
        }

        if (codeService == '0') {
            alertify.alert('Veuillez selectionner un service d\'assiette.');
            return;
        }

        alertify.confirm('Etes-vous de vouloir affecter ces articles ?', function () {

            var articleAffecterLists = preparerAffectationArticle();
            affecterArticleCompte(articleAffecterLists);

        });
    });
    btnDesactiverArticle.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (codeBanque == '0') {
            alertify.alert('Veuillez selectionner une banque.');
            return;
        }

        if (codeCompteBancaire == '0') {
            alertify.alert('Veuillez selectionner un compte bancaire.');
            return;
        }

        alertify.confirm('Etes-vous de vouloir désactiver ces articles ?', function () {

            var articleDesactiverLists = preparerDesaffectationArticle();
            desaffecterArticleCompte(articleDesactiverLists);

        });
    });

    loadBanque();
    loadMinistere();
    printArticleAffecter('');
    printArticleBudgetaire('');

});


function loadBanque() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBanque',
            'codeSite': userData.SiteCode
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            var banqueList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner une banque ---</option>';

            for (var i = 0; i < banqueList.length; i++) {
                data += '<option value="' + banqueList[i].codeBanque + '">' + banqueList[i].intituleBanque + '</option>';
            }
            cmbBanque.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadCompteBancaire(codeBancaire) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadCompteBancaire',
            'codeBanque': codeBancaire
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '0') {
                alertify.alert('Aucun compte trouver pour cette banque');
                return;
            }

            var compteBancaireList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner un compte bancaire ---</option>';
            data += '<option value="*">Tous les comptes</option>';


            for (var i = 0; i < compteBancaireList.length; i++) {
                data += '<option value="' + compteBancaireList[i].codeCompteBancaire + '">' + compteBancaireList[i].intituleCompteBancaire + '</option>';
            }
            cmbCompteBancaire.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadArticleAffecter(codeCompteBancaire) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadArticleAffecter',
            'codeCompteBancaire': codeCompteBancaire
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '0') {
                alertify.alert('Aucun article trouver pour cet article');
                return;
            }

            articleAffecterList = $.parseJSON(JSON.stringify(response));
            printArticleAffecter(articleAffecterList);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadMinistere() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadMinistere',
            'code': '1'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '0') {
                alertify.alert('Aucun ministère trouver');
                return;
            }

            var ministereList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner un ministère ---</option>';

            for (var i = 0; i < ministereList.length; i++) {
                data += '<option value="' + ministereList[i].code + '">' + ministereList[i].intitule + '</option>';
            }
            cmbMinistere.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadService(codeMinistere) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getServiceAssiette',
            'code': codeMinistere
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '0') {
                alertify.alert('Aucun service trouver pour ce ministère');
                return;
            }

            var serviceList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner un service d\'assiette ---</option>';

            for (var i = 0; i < serviceList.length; i++) {
                data += '<option value="' + serviceList[i].code + '">' + serviceList[i].intitule + '</option>';
            }
            cmbServiceAssiette.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadArticleByService(codeService) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadArticleByService',
            'codeService': codeService,
            'codeCompteBancaire': codeCompteBancaire
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '0') {
                alertify.alert('Aucun article trouver pour ce service');
                printArticleBudgetaire('');
                return;
            }

            articleLists = $.parseJSON(JSON.stringify(response));
            printArticleBudgetaire(articleLists);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printArticleAffecter(tempArticleAffecterList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:92%" >ARTICLE AFFECTER</th>';
    header += '<th style="width:8%;text-align:center"> </th>';
    header += '<th hidden="true" scope="col"> Code articleBanque </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyPersonnes">';

    for (var i = 0; i < tempArticleAffecterList.length; i++) {
        body += '<tr>';
        body += '<td>' + tempArticleAffecterList[i].intituleArticle + '</td>';
        body += '<td style="text-align:center"><center><input onclick="checkArtBudgAffect(\'' + tempArticleAffecterList[i].banqueArticleCode + '\')" type="checkbox" id="checkBoxSelectArtBudgAffect' + tempArticleAffecterList[i].banqueArticleCode + '" name="checkBoxSelectArtBudgAffect' + tempArticleAffecterList[i].banqueArticleCode + '"></center></td>';
        body += '<td hidden="true">' + tempArticleAffecterList[i].banqueArticleCode + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableArticleAffecter.html(tableContent);

    tableArticleAffecter.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 6,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}
function printArticleBudgetaire(tempArticleList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:92%" >INTITULE ARTICLE</th>';
    header += '<th style="width:8%;text-align:center"> </th>';
    header += '<th hidden="true" scope="col"> Code article </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyPersonnes">';

    for (var i = 0; i < tempArticleList.length; i++) {
        body += '<tr>';
        body += '<td>' + tempArticleList[i].intituleArticle + '</td>';
        body += '<td style="text-align:center"><center><input onclick="checkArticle(\'' + tempArticleList[i].codeArticle + '\')" type="checkbox" id="checkBoxSelectArticle' + tempArticleList[i].codeArticle + '" name="checkBoxSelectArticle' + tempArticleList[i].codeArticle + '"></center></td>';
        body += '<td hidden="true">' + tempArticleList[i].codeArticle + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableArticleBudgetaire.html(tableContent);

    tableArticleBudgetaire.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 8,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function preparerAffectationArticle() {

    articleList = [];

    for (var i = 0; i < articleLists.length; i++) {

        var selectArticle = '#checkBoxSelectArticle' + articleLists[i].codeArticle;

        var select = $(selectArticle);

        if (select.is(':checked')) {

            var userObjet = new Object();

            userObjet.codeArticle = articleLists[i].codeArticle;
            userObjet.intituleArticle = articleLists[i].intituleArticle;
            articleList.push(userObjet);
        }

    }

    articleList = JSON.stringify(articleList);
    return articleList;
}

function preparerDesaffectationArticle() {

    articleDesaffecterList = [];

    for (var i = 0; i < articleAffecterList.length; i++) {

        var selectArticle = '#checkBoxSelectArtBudgAffect' + articleAffecterList[i].banqueArticleCode;

        var select = $(selectArticle);

        if (select.is(':checked')) {

            var userObjet = new Object();

            userObjet.banqueArticleCode = articleAffecterList[i].banqueArticleCode;
            userObjet.intituleArticle = articleAffecterList[i].intituleArticle;
            articleDesaffecterList.push(userObjet);
        }

    }
    articleDesaffecterList = JSON.stringify(articleDesaffecterList);
    return articleDesaffecterList;
}

function affecterArticleCompte(articleList) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'affecterArticleCompte',
            'codeBanque': codeBanque,
            'codeCompteBancaire': codeCompteBancaire,
            'idUser': userData.idUser,
            'droitList': articleList

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Affectation des droits en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\affectation des droits s\'est effectué avec succès.');
                    loadArticleAffecter(codeCompteBancaire);
                    loadArticleByService(codeService);
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors d\'affectation des droits.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}
function desaffecterArticleCompte(articleList) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'desaffecterArticleCompte',
            'droitList': articleList

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Affectation des droits en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('La désaffectation des droits s\'est effectué avec succès.');
                    loadArticleAffecter(codeCompteBancaire);
                    loadArticleByService(codeService);
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de désaffectation des droits.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}