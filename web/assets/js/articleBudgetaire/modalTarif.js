/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalTarif, tableResultTarif;

var inputValueResearchTarif;

var btnLauchSearchTarif, btnSelectTarif;

$(function () {

    tableResultTarif = $('#tableResultTarif');

    modalTarif = $('#modalTarif');

    btnLauchSearchTarif = $('#btnLauchSearchTarif');
    btnSelectTarif = $('#btnSelectTarif');

    inputValueResearchTarif = $('#inputValueResearchTarif');

    btnLauchSearchTarif.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        searchTarif();
    });

    inputValueResearchTarif.on('keypress', function (e) {
        if (e.keyCode == 13) {
            searchTarif();
        }

    });

    printTarif('');

});

function searchTarif() {

    if (inputValueResearchTarif.val() === empty || inputValueResearchTarif.val().length < SEARCH_MIN_TEXT) {
        showEmptySearchMessage();
        return;
    }
    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchTarif.val(),
            'userId': userData.idUser,
            'operation': 'loadtarif'
        },
        beforeSend: function () {
            modalTarif.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                modalTarif.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnSelectTarif.hide();
                    printTarif('');
                    return;
                } else {
                    var dataTarif = JSON.parse(JSON.stringify(response));
                    if (dataTarif.length > 0) {
                        btnSelectTarif.show();
                        printTarif(dataTarif);
                    } else {
                        btnSelectTarif.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalTarif.unblock();
            showResponseError();
        }

    });
}

function printTarif(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:70%">TARIF</th>';
    tableContent += '<th style="text-align:center;width:15%">VALEUR</th>';
    tableContent += '<th style="text-align:center;width:15%">Type</th>';
    tableContent += '<th hidden="true" scope="col">Code officiel</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="width:70%;vertical-align:middle;text-align:left">' + data[i].intituleTarif.toUpperCase() + '</td>';
        tableContent += '<td style="width:15%;vertical-align:middle;text-align:center">' + data[i].valeurTarif + '</td>';
        tableContent += '<td style="width:15%;vertical-align:middle;text-align:center">' + data[i].typeValeurTarif + '</td>';
        tableContent += '<td hidden="true">' + data[i].codeTarif + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultTarif.html(tableContent);

    var myDt = tableResultTarif.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 15,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableResultTarif tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        selectTarifData = new Object();
        selectTarifData.intituleTarif = myDt.row(this).data()[0].toUpperCase();
        selectTarifData.valeurTarif = myDt.row(this).data()[1].toUpperCase();
        selectTarifData.typeValeurTarif = myDt.row(this).data()[2].toUpperCase();
        selectTarifData.codeTarif = myDt.row(this).data()[3];

    });
}


