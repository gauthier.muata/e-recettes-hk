/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnAddProrogation;
var modalUpdateProrogationDateLegale;
var tableProrogation;

var prorogationList = [];
var dataInitList = [];
var abList = [];

var bankCodeSwift, btnSaveProrogation;

var idProrogation;

var btnDeleteProrogation;
var selectImpot, selectExercice, inputEcheanceLegaleDeclaration, inputEcheanceLegaleDeclarationProrogation, inputEcheanceLegalePaiement, inputEcheanceLegalePaiementProrogation;
var inputMotifProrogation;

var datePickerDebut;
var datePickerFin;

$(function () {

    mainNavigationLabel.text('Gestion des articles budgétaires');
    secondNavigationLabel.text('Prorogation des échéances légales');
    removeActiveMenu();
    linkMenuTaxation.addClass('active');

    idProrogation = empty;

    btnAddProrogation = $('#btnAddProrogation');
    modalUpdateProrogationDateLegale = $('#modalUpdateProrogationDateLegale');
    tableProrogation = $('#tableProrogation');

    selectImpot = $('#selectImpot');
    selectExercice = $('#selectExercice');
    inputEcheanceLegaleDeclaration = $('#inputEcheanceLegaleDeclaration');

    inputEcheanceLegaleDeclarationProrogation = $('#inputEcheanceLegaleDeclarationProrogation');
    inputEcheanceLegalePaiement = $('#inputEcheanceLegalePaiement');
    inputEcheanceLegalePaiementProrogation = $('#inputEcheanceLegalePaiementProrogation');
    inputMotifProrogation = $('#inputMotifProrogation');
    inputMotifProrogation.val(empty);


    btnSaveProrogation = $('#btnSaveProrogation');
    btnDeleteProrogation = $('#btnDeleteProrogation');
    modalAccoutBank = $('#modalAccoutBank');
    tableAccountBank = $('#tableAccountBank');

    datePickerDebut = $("#inputEcheanceLegaleDeclarationProrogation");
    datePickerFin = $('#inputEcheanceLegalePaiementProrogation');

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerDebut.datepicker("setDate", new Date());
    datePickerFin.datepicker("setDate", new Date());


    btnAddProrogation.on('click', function (e) {
        e.preventDefault();

        id = empty;

        selectImpot.val(empty);
        selectExercice.val(empty);
        inputEcheanceLegaleDeclaration.val(empty);

        inputEcheanceLegaleDeclarationProrogation.val(empty);
        inputEcheanceLegalePaiement.val(empty);
        inputEcheanceLegalePaiementProrogation.val(empty);
        inputMotifProrogation.val(empty);

        btnDeleteProrogation.attr('style', 'display:none');
        modalUpdateProrogationDateLegale.modal('show');

    });

    btnSaveProrogation.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();

    });

    btnDeleteProrogation.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        deleteProrogation();

    });

    initDataProrogationEcheanceLegale();
    loadProrogations();
});

function deleteProrogation() {

    if (selectImpot.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner un impôt valide');
        selectImpot.focus()
        return;
    }

    if (selectExercice.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner un exercice fiscale valide');
        selectExercice.focus()
        return;
    }

    if (inputMotifProrogation.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir le motif de la supression de cette prorogation');
        inputMotifProrogation.focus()
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir supprimer la prorogation ?', function () {

        $.ajax({
            type: 'POST',
            url: 'articleBudgetaire_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'id': idProrogation,
                'userId': userData.idUser,
                'selectImpot': selectImpot.val(),
                'selectExercice': selectExercice.val(),
                'inputMotifProrogation': inputMotifProrogation.val(),
                'operation': 'deleteProrogation'
            },
            beforeSend: function () {
                modalUpdateProrogationDateLegale.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression prorogation en cours ...</h5>'});
            },
            success: function (response)
            {

                setTimeout(function () {

                    modalUpdateProrogationDateLegale.unblock();

                    if (response == '-1' | response == '0') {

                        showResponseError();
                        return;

                    } else {
                        alertify.alert('La suppression de la prorogation s\'est effectuée avec succès');
                        resetFields();
                        modalUpdateProrogationDateLegale.modal('hide');
                        loadProrogations();
                    }
                }
                , 1);
            },
            complete: function () {
            },
            error: function () {
                modalUpdateProrogationDateLegale.unblock();
                showResponseError();
            }
        });
    });
}

function resetFields() {

    idProrogation = empty;
    selectImpot.val(empty);
    selectExercice.val(empty);
    inputEcheanceLegaleDeclarationProrogation.val(empty);
    inputEcheanceLegalePaiementProrogation.val(empty);
    inputEcheanceLegaleDeclaration.val(empty);
    inputEcheanceLegalePaiement.val(empty);
    inputMotifProrogation.val(empty);
}

function checkFields() {

    if (selectImpot.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner un impôt valide');
        selectImpot.focus()
        return;
    }

    if (selectExercice.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner un exercice fiscale valide');
        selectExercice.focus()
        return;
    }

    if (inputEcheanceLegaleDeclarationProrogation.val() == empty) {
        alertify.alert('Veuillez d\'abord indiquer l\'échéance prorogée de la déclaration');
        inputEcheanceLegaleDeclarationProrogation.focus()
        return;
    }

    if (inputEcheanceLegalePaiementProrogation.val() == empty) {
        alertify.alert('Veuillez d\'abord indiquer l\'échéance prorogée de paiement');
        inputEcheanceLegalePaiementProrogation.focus()
        return;
    }

    if (inputMotifProrogation.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir le motif de la prorogation');
        inputMotifProrogation.focus()
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir enregistrer cette prorogation ?', function () {
        saveProrogation();
    });
}

function saveProrogation() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idProrogation,
            'selectImpot': selectImpot.val(),
            'selectExercice': selectExercice.val(),
            'inputEcheanceLegaleDeclaration': inputEcheanceLegaleDeclaration.val(),
            'inputEcheanceLegaleDeclarationProrogation': inputEcheanceLegaleDeclarationProrogation.val(),
            'inputEcheanceLegalePaiement': inputEcheanceLegalePaiement.val(),
            'inputEcheanceLegalePaiementProrogation': inputEcheanceLegalePaiementProrogation.val(),
            'userId': userData.idUser,
            'inputMotifProrogation': inputMotifProrogation.val(),
            'operation': 'saveProrogation'
        },
        beforeSend: function () {
            modalUpdateProrogationDateLegale.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement prorogation en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalUpdateProrogationDateLegale.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('L\'enregistrement de la prorogation s\'est effectué avec succès');
                    resetFields();
                    modalUpdateProrogationDateLegale.modal('hide');
                    loadProrogations();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalUpdateProrogationDateLegale.unblock();
            showResponseError();
        }
    });
}

function loadProrogations() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadProrogations'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    printTableProrogation(empty);

                } else {

                    prorogationList = JSON.parse(JSON.stringify(response));
                    printTableProrogation(prorogationList);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printTableProrogation(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:9%">EXERCICE</th>';
    tableContent += '<th style="text-align:left;width:20%">IMPOT</th>';
    tableContent += '<th style="text-align:left;width:18%">INFOS ECHEANCE DECLARATION</th>';
    tableContent += '<th style="text-align:left;width:18%">INFOS ECHEANCE PAIEMENT</th>';
    tableContent += '<th style="text-align:left;width:25%">MOTIFS</th>';
    tableContent += '<th style="text-align:left;width:10%"></th>';

    tableContent += '</thead>';
    tableContent += '<tbody>';

    var echeanceMonth = '';
    var echeanceYear = '';

    var valueEcheance1 = '';
    var valueEcheance2 = '';

    var valueEcheance3 = '';
    var valueEcheance4 = '';

    for (var i = 0; i < result.length; i++) {

        if (result[i].echeanceLegaleDeclarationAb != '') {
            var echeance = result[i].echeanceLegaleDeclarationAb.split('-');
            echeanceMonth = echeance[2];
            echeanceYear = echeance[2] + '/' + echeance[1];
            valueEcheance1 = echeanceYear + ' de chaque année';
        }

        if (result[i].echeanceLegaleDeclarationProrogee != '') {
            var echeance = result[i].echeanceLegaleDeclarationProrogee.split('-');
            echeanceMonth = echeance[2];
            echeanceYear = echeance[2] + '/' + echeance[1];
            valueEcheance2 = echeanceYear + ' de chaque année';
        }


        var echeanceLegalDeclaration = 'Echéance légale de déclaration : ' + '<span style="font-weight:bold">' + valueEcheance1.toUpperCase() + '</span>';
        var echeanceLegalDeclarationProrogation = 'Echéance de déclaration prorogée : ' + '<span style="font-weight:bold">' + valueEcheance2.toUpperCase() + '</span>';

        var echeanceLegalDeclarationInfo = echeanceLegalDeclaration + '<hr/>' + echeanceLegalDeclarationProrogation;

        if (result[i].echeanceLegalePaiementAb != '') {
            var echeance = result[i].echeanceLegalePaiementAb.split('-');
            echeanceMonth = echeance[2];
            echeanceYear = echeance[2] + '/' + echeance[1];
            valueEcheance3 = echeanceYear + ' de chaque année';
        }

        if (result[i].echeanceLegalePaiementProrogee != '') {
            var echeance = result[i].echeanceLegalePaiementProrogee.split('-');
            echeanceMonth = echeance[2];
            echeanceYear = echeance[2] + '/' + echeance[1];
            valueEcheance4 = echeanceYear + ' de chaque année';
        }


        var echeanceLegalPaiement = 'Echéance légale de paiement : ' + '<span style="font-weight:bold">' + valueEcheance3.toUpperCase() + '</span>';
        var echeancepaiementProrogation = 'Echéance de paiement prorogée : ' + '<span style="font-weight:bold">' + valueEcheance4.toUpperCase() + '</span>';

        var echeanceLegalPaiementInfo = echeanceLegalPaiement + '<hr/>' + echeancepaiementProrogation;

        var btnEditProrogation = '<button class="btn btn-success" onclick="editProrogation(\'' + result[i].id + '\')"><i class="fa fa-edit"></i></button>';


        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:9%;vertical-align:middle">' + result[i].exercice + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].libelleAb + '</td>';
        tableContent += '<td style="text-align:left;width:18%;vertical-align:middle">' + echeanceLegalDeclarationInfo + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + echeanceLegalPaiementInfo + '</td>';
        tableContent += '<td style="text-align:left;width:28%;vertical-align:middle">' + result[i].motif + '</td>';
        tableContent += '<td style="text-align:left;width:5%;vertical-align:middle">' + btnEditProrogation + '</td>';

        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableProrogation.html(tableContent);

    tableProrogation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des articles budgétaires est vide",
            search: "Rechercher par intitulé _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}

function editProrogation(id) {

    for (var i = 0; i < prorogationList.length; i++) {

        if (prorogationList[i].id == id) {

            idProrogation = id;

            selectImpot.val(prorogationList[i].codeAb);
            selectExercice.val(prorogationList[i].exercice);

            inputEcheanceLegaleDeclaration.val(prorogationList[i].echeanceLegaleDeclarationAbF);
            inputEcheanceLegalePaiement.val(prorogationList[i].echeanceLegalePaiementAbF);

            inputEcheanceLegaleDeclarationProrogation.val(prorogationList[i].echeanceLegaleDeclarationProrogeeF);
            inputEcheanceLegalePaiementProrogation.val(prorogationList[i].echeanceLegalePaiementProrogeeF);
            inputMotifProrogation.val(prorogationList[i].motif);

            btnDeleteProrogation.attr('style', 'display:inline');

            modalUpdateProrogationDateLegale.modal('show');

            break;
        }
    }

}

function initDataProrogationEcheanceLegale() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'initDataProrogationEcheanceLegale'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                } else {

                    //dataInitList = JSON.stringify(response);
                    dataInitList = JSON.parse(JSON.stringify(response));
                    abList = JSON.parse(dataInitList[0].abList);

                    var dataAb = '';

                    dataAb += '<option value="0">--</option>';
                    for (var i = 0; i < abList.length; i++) {
                        dataAb += '<option value="' + abList[i].codeAb + '">' + abList[i].libelleAb + '</option>';

                    }

                    selectImpot.html(dataAb);
                    selectImpot.val('0');

                    var exerciceList = JSON.parse(dataInitList[0].exerciceFiscaleList);

                    var dataExercice = '';

                    dataExercice += '<option value="0">--</option>';
                    for (var i = 0; i < exerciceList.length; i++) {
                        dataExercice += '<option value="' + exerciceList[i].code + '">' + exerciceList[i].code + '</option>';

                    }

                    selectExercice.html(dataExercice);
                    selectExercice.val('0');
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

