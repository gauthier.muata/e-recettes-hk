/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var cmbMinistere, cmbService, cmbFaitGenerateur, cmbDocumentOfficiel, 
        cmbNatureArticleBudgetaire;
var inputLibelleArticleBudgetaire, inputArticleBudgetaire;
var codeMinistere, codeService, codeFaitGenerateur, codeEntiteAdministrative;
var ministereList = [], servciceList = [], faitGenerateurList = [], 
        documentOfficielList = [], entiteAdministrativeList = [];

$(function () {

    cmbMinistere = $('#cmbMinistere');
    cmbService = $('#cmbService');
    cmbFaitGenerateur = $('#cmbFaitGenerateur');
    cmbDocumentOfficiel = $('#cmbDocumentOfficiel');
    cmbNatureArticleBudgetaire = $('#cmbNatureArticleBudgetaire');
    
    inputLibelleArticleBudgetaire = $('#inputLibelleArticleBudgetaire');
    inputArticleBudgetaire = $('#inputArticleBudgetaire');

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('Edition article budgétaire');

    removeActiveMenu();
    linkMenuGestionArticleBudgetaire.addClass('active');

    ajax_article_budgetaire('loadMinistere');

    cmbMinistere.change(function (e) {
        codeMinistere = cmbMinistere.val();
        ajax_article_budgetaire('loadServices');
    });

    cmbService.change(function (e) {
        codeService = cmbService.val();
        ajax_article_budgetaire('loadFaitGenerateurs');
    });

    cmbFaitGenerateur.change(function (e) {
        codeFaitGenerateur = cmbFaitGenerateur.val();
        ajax_article_budgetaire('loadDocumentOfficiel');
        ajax_article_budgetaire('loadNatureArticleBudgetaire');
    });
    
});

function ajax_article_budgetaire(operation) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': operation,
            'code_ministere': codeMinistere,
            'code_service': codeService,
            'code_fait_generateur': codeFaitGenerateur,
            'code': '1'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if(response == '0'){
                
            }

            switch (operation) {
                case 'loadMinistere' :
                    ministereList = $.parseJSON(JSON.stringify(response));
                    fillComboMinistere(ministereList);
                    break;
                case 'loadServices' :
                    servciceList = $.parseJSON(JSON.stringify(response));
                    fillComboService(servciceList);
                    break;
                case 'loadFaitGenerateurs' :
                    faitGenerateurList = $.parseJSON(JSON.stringify(response));
                    fillComboFaitGenerateur(faitGenerateurList);
                    break;
                case 'loadDocumentOfficiel' :
                    documentOfficielList = $.parseJSON(JSON.stringify(response));
                    fillComboDocumentOfficiel(documentOfficielList);
                    break;
                case 'loadNatureArticleBudgetaire' :
                    natureArticleBudgetaireList = $.parseJSON(JSON.stringify(response));
                    fillComboNatureArticleBudgetaire(natureArticleBudgetaireList);
                    break;
            }

        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function fillComboMinistere(ministereList) {
    var data = '<option value="0">--- S&eacute;lectionner un ministère ---</option>';

    for (var i = 0; i < ministereList.length; i++) {
        data += '<option value="' + ministereList[i].code + '">' + ministereList[i].intitule + '</option>';
    }
    cmbMinistere.html(data);
}

function fillComboService(servciceList) {
    var data = '<option value="0">--- S&eacute;lectionner un service ---</option>';

    for (var i = 0; i < servciceList.length; i++) {
        data += '<option value="' + servciceList[i].code + '">' + servciceList[i].intitule + '</option>';
    }
    cmbService.html(data);
}

function fillComboFaitGenerateur(faitGenerateurList) {
    var data = '<option value="0">--- S&eacute;lectionner un service ---</option>';

    for (var i = 0; i < faitGenerateurList.length; i++) {
        data += '<option value="' + faitGenerateurList[i].code + '">' + faitGenerateurList[i].intitule + '</option>';
    }
    cmbFaitGenerateur.html(data);
}

function fillComboDocumentOfficiel(documentOfficielList) {
    var data = '<option value="0">--- S&eacute;lectionner un service ---</option>';

    for (var i = 0; i < documentOfficielList.length; i++) {
        data += '<option value="' + documentOfficielList[i].code + '">' + documentOfficielList[i].numero + '</option>';
    }
    cmbDocumentOfficiel.html(data);
}

function fillComboNatureArticleBudgetaire(natureArticleBudgetaireList) {
    var data = '<option value="0">--- S&eacute;lectionner un service ---</option>';

    for (var i = 0; i < natureArticleBudgetaireList.length; i++) {
        data += '<option value="' + natureArticleBudgetaireList[i].code + '">' + natureArticleBudgetaireList[i].intitule + '</option>';
    }
    cmbNatureArticleBudgetaire.html(data);
}