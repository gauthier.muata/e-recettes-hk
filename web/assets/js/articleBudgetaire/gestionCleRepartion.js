/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var cmbMinistere, cmbServiceAssiette, cmbFormeJuridique, cmbArticleCleRep;
var tableFaitGenerateur;
var DivNewFaitGenerateur;
var BtnNouveau, BtnEnregistrer, BtnDismiss, btnCallModalCleRepartition, btnvaliderclerepartition, btnvaliderCleRepartitionArticle;
var btnEnregistrerActeGenerateur;
var inputgenerateur;
var codeFaitGenerateur = '';
var taux;
var codeministere;
var cleRepartitionModal;
var clerepartitionarticle;
var faitGenerateurList;
var LiscleRepartition;
var entitebeneficiaire;
var entitearticle;
var enregistrercle;
var codeclerapartition;
var generationactegenerateur;
var tauxactegenerateur;
var cbtresorpart;
var btnmodalcle;
var listclerepartition, codeArticleBudgetaire;

$(function () {

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('clés de repartition');

    removeActiveMenu();

    cleRepartitionModal = $('#cleRepartitionModal');

    listclerepartition = [];

    linkMenuGestionArticleBudgetaire.addClass('active');
    inputgenerateur = $('#inputgenerateur');
    btnmodalcle = $('#btnmodalcle');

    btnmodalcle.on('click', function (e) {
        e.preventDefault();
        //  modaleclerepartition.modal('show');
        // alert('veuillez inserer une clé de repartition');
        //cleRepartitionModal.modal('show');

    });
    cmbMinistere = $('#cmbMinistere');

    cmbMinistere.on('change', function () {
        var code = cmbMinistere.val();
        if (code == '0') {
            cmbServiceAssiette.html('');
            DivNewFaitGenerateur.hide();
            printFaitsGenerateurs('');
            return;
        }
        loadServicesByMinistere(code);
    });

//    loadArticleBugdetaireCleRepByService
    enregistrercle = $('#enregistrercle');
    enregistrercle.on('click', function (e) {
        e.preventDefault();
        //   alert(JSON.stringify(listclerepartition));
        saveCleRepartition();
    });

    cmbArticleCleRep = $('#cmbArticleCleRep');

    cmbArticleCleRep.on('change', function () {
        var code = cmbArticleCleRep.val();
        if (code == '0') {
            //  DivNewFaitGenerateur.hide();
            printFaitsGenerateurs('');
            return;
        }
        loadArticleBugdetaireCleRepByService(code);
    });

    cbtresorpart = false;

    entitebeneficiaire = $('#entitebeneficiaire');
    entitearticle = $('#entitearticle');
    tauxactegenerateur = $('#tauxactegenerateur');
    codeclerapartition = $('#codeclerapartition');
    codeclerapartition.attr('style', 'display:none');

    cbtresorpart = $('#cbtresorpart');
    btnEnregistrerActeGenerateur = $('#btnEnregistrerActeGenerateur');
    btnEnregistrerActeGenerateur.on('click', function (e) {

        e.preventDefault();


        if (tauxactegenerateur.val() == '') {
            alertify.alert('Veuillez entrer un taux.');
            return;
        } else {
            AddCleRepartition(codeclerapartition.val(), tauxactegenerateur.val(), entitearticle.val(), '0', cbtresorpart);
            // saveCleRepartition(codeclerapartition.val(), tauxactegenerateur.val(), entitebeneficiaire.val(), '0', cbtresorpart);

        }
        //  saveCleRepartition(codeclerapartition.val(), tauxactegenerateur.val(), entitebeneficiaire.val(), '0', '1');
        //saveCleRepartition(codeclerapartition.val(), tauxactegenerateur.val(), entitebeneficiaire.val(), '0', cbtresorpart);
        // alertify.alert('Veuillez selectionner une clé de repartition.');


    });

    btnvaliderclerepartition = $('#btnvaliderclerepartition');

    btnvaliderclerepartition.on('click', function (e) {
        e.preventDefault();
        if (inputclerepartition.val() == '') {
            alertify.alert('Veuillez selectionner une clé de repartition.');
            return;
        }
        //  alert('Veuillez sélectionner un ministere.');

    });

    btnvaliderCleRepartitionArticle = $('#btnvaliderCleRepartitionArticle');

    btnvaliderCleRepartitionArticle.on('click', function (e) {
        e.preventDefault();
        alert('veuillez inserer une clé de repartition');
        //  saveCleRepartition();   

    });

    btnCallModalCleRepartition = $('#btnCallModalCleRepartition');

    btnCallModalCleRepartition.on('click', function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        cleRepartitionModal.modal('show');
    });

    cmbServiceAssiette = $('#cmbServiceAssiette');

    cmbServiceAssiette.on('change', function () {
        var code = cmbServiceAssiette.val();
        if (code == '0') {
            DivNewFaitGenerateur.hide();
            printFaitsCleRepartition('');
            return;
        }
        loadFaitGenerateursByService(code);
    });

    cmbFormeJuridique = $('#cmbFormeJuridique');

    cmbFormeJuridique.on('change', function () {
        var intitule = cmbFormeJuridique.val();
        inputgenerateur = $('# inputgenerateur');
    });

    BtnNouveau = $('#BtnNouveau');
    BtnNouveau.click(function (e) {
        e.preventDefault();
        if (cmbMinistere.val() == '0') {
            alertify.alert('Veuillez sélectionner un ministere.');
            return;
        }

        if (cmbServiceAssiette.val() == '' || cmbServiceAssiette.val() == 'null' || cmbServiceAssiette.val() == '0') {
            alertify.alert('Veuillez sélectionner un service.');
            return;
        }
        BtnEnregistrer.html('<i class="fa fa-save"></i> Enregister');
        codeFaitGenerateur = '';
        DivNewFaitGenerateur.show();
    });

    BtnEnregistrer = $('#BtnEnregistrer');

    BtnEnregistrer.click(function (e) {
        e.preventDefault();
        if (inputgenerateur.val() == '' || inputgenerateur.val() == 'null' || inputgenerateur.val() == '0') {
            alertify.alert('Veuillez saisir un fait generateur.');
            return;
        }
        if (cmbFormeJuridique.val() == '' || cmbFormeJuridique.val() == 'null' || cmbFormeJuridique.val() == '0') {
            alertify.alert('Veuillez selectionner une forme juridique.');
            return;
        }
        var action;
        if (codeFaitGenerateur == '') {

            if (checkDoublon()) {
                alertify.alert('ce fait generateur existe deja');
                return;
            }

            action = 'enregistrer ?';
        } else {
            action = 'modifier ?';
        }

        var message = 'Etes-vous sûrs de vouloir ' + action;

        alertify.confirm(message, function () {
            saveFaitGenerateur(1);
        });
    });

    BtnDismiss = $('#BtnDismiss');

    BtnDismiss.click(function (e) {
        e.preventDefault();
        DivNewFaitGenerateur.hide();

    });

    tableFaitGenerateur = $('#tableFaitGenerateur');
    DivNewFaitGenerateur = $('#DivNewFaitGenerateur');
    generationactegenerateur = $('#generationactegenerateur');

    loadMinistere();
    //  loadCleRepartition('');
    loadformeJuridique();
    printFaitsGenerateurs('');
    //printFaitsCleRepartition
});


function loadMinistere() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadMinistere',
            'code': '1'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            var ministereList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner un ministère ---</option>';

            for (var i = 0; i < ministereList.length; i++) {
                data += '<option value="' + ministereList[i].code + '">' + ministereList[i].intitule + '</option>';
            }
            cmbArticleCleRep.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadCleRepartition(code) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadCleRepartition',
            'code': code
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            LiscleRepartition = $.parseJSON(JSON.stringify(response));
            printFaitsGenerateurs(LiscleRepartition);
            //  printFaitsGenerateurs(faitGenerateurList);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadServicesByMinistere(codeMinistere) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadServices',
            'code_ministere': codeMinistere,
            'code': '1'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            var serviceList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner un service d\'assiètte ---</option>';

            for (var i = 0; i < serviceList.length; i++) {
                data += '<option value="' + serviceList[i].code + '">' + serviceList[i].intitule + '</option>';
            }
            cmbServiceAssiette.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadFaitGenerateursByService(codeService) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadFaitGenerateurs',
            'code_service': codeService
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            faitGenerateurList = $.parseJSON(JSON.stringify(response));
            printFaitsGenerateurs(faitGenerateurList);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadArticleBugdetaireCleRepByService(codeministere) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadArticleBudgetaireCleRep',
            'codeministere': codeministere
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }
            clerepartitionarticle = $.parseJSON(JSON.stringify(response));
            printFaitsCleRepartition(clerepartitionarticle);


        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printFaitsGenerateurs(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col">ENTITE BENEFICIAIRE</th>';
    header += '<th scope="col">TAUX</th>';
    header += '<th scope="col">PART</th>';
    header += '<th scope="col"></th>';
    header += '<th scope="col"></th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < result.length; i++) {
        body += '<tr>';
        body += '<td>' + result[i].entitearticle + '</td>';
        body += '<td>' + result[i].taux + '</td>';
        body += '<td><button type="button" onclick="addCleRepartition(\'' + result[i].code + '\',\'' + result[i].intitule + '\')" class="btn btn-info" title="Ajouter"><i class="fa fa-check"></i></button></td>';
        body += '<td><button type="button" onclick="editCleRepartition(\'' + result[i].code + '\',\'' + result[i].intitule + '\')" class="btn btn-info" title="Editer"><i class="fa fa-edit"></i></button></td>';
        body += '<td><button type="button" onclick="deleteCleRepartition(\'' + result[i].code + '\',\'' + result[i].intitule + '\')" class="btn btn-danger" title="Suppression"><i class="fa fa-trash"></i></button></td>';

        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableFaitGenerateur.html(tableContent);
    tableFaitGenerateur.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

}

function printFaitsCleRepartition(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';

    header += '<th scope="col">CODE </th>';
    header += '<th scope="col"> ENTITE BENEFICIAIRE </th>';
    header += '<th scope="col"></th>';
    header += '<th scope="col"></th>';
    header += '<th scope="col"></th>';
    header += '</tr></thead>';
    header += '</tr></thead>';


    var body = '<tbody>';

    for (var i = 0; i < result.length; i++) {

        if (result[i].intitule.length > 122) {
            firstLineAB = result[i].intitule.substring(0, 122).toUpperCase() + ' ...';
        } else {
            firstLineAB = result[i].intitule.toUpperCase();
        }

        body += '<tr>';
        body += '<td>' + result[i].code + '</td>';
        body += '<td style="text-align:left;width:122%;vertical-align:middle; title="' + result[i].intitule + '"><span style="font-weight:bold;"></span>' + firstLineAB + '</td>';
        body += '<td><button type="button" onclick="addCleRepartition(\'' + result[i].code + '\',\'' + result[i].intitule + '\')" class="btn btn-info" title="Ajouter"><i class="fa fa-check"></i></button></td>';
        body += '<td><button type="button" onclick="editCleRepartition(\'' + result[i].intitule + '\',\'' + result[i].intitule + '\')" class="btn btn-info" title="Editer"><i class="fa fa-edit"></i></button></td>';
        body += '<td><button type="button" onclick="deleteCleRepartition(\'' + result[i].taux + '\',\'' + result[i].intitule + '\')" class="btn btn-danger" title="Suppression"><i class="fa fa-trash"></i></button></td>';


        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    generationactegenerateur.html(tableContent);
    generationactegenerateur.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function loadformeJuridique() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadFormeJuridique'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            var formejuridiqueList = $.parseJSON(JSON.stringify(response));

            console.log(formejuridiqueList)
            var data = '<option value="0">--- Sélectionner une forme juridique ---</option>';

            for (var i = 0; i < formejuridiqueList.length; i++) {
                data += '<option value="' + formejuridiqueList[i].codeFormeJuridique + '">' + formejuridiqueList[i].libelleFormeJuridique + '</option>';
            }
            cmbFormeJuridique.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function editFaitGenerateur(code) {

    for (var i = 0; i < faitGenerateurList.length; i++) {
        if (faitGenerateurList[i].code == code) {
            inputgenerateur.val(faitGenerateurList[i].intitule);
            cmbFormeJuridique.val(faitGenerateurList[i].codeForme);
            BtnEnregistrer.html('<i class="fa fa-save"></i> Modifier');
            codeFaitGenerateur = code;
            DivNewFaitGenerateur.show();
            break;
        }
    }
}

function saveCleRepartition() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'addclerepartition',
            'repartitionList': JSON.stringify(listclerepartition)
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Opération en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            if (response == '1') {
                alertify.alert('Cle de repartition enregistré avec succès!');

            } else if (response == '2') {
                alertify.alert('le taux doit etre inferieur ou égale a 100');
            }

        }
        ,
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function saveFaitGenerateur(etat) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveFaitGenerateur',
            'code': codeFaitGenerateur,
            'intitule': inputgenerateur.val(),
            'codeService': cmbServiceAssiette.val(),
            'codeForme': cmbFormeJuridique.val(),
            'etat': etat
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Opération en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            if (etat == 1) {
                var message;
                if (codeFaitGenerateur == '') {
                    message = 'Fait générateur enregistré avec succès!';
                } else {
                    message = 'Fait générateur modifié avec succès!';
                }

                inputgenerateur.val('');
                cmbFormeJuridique.val('0');

                alertify.alert(message);
                DivNewFaitGenerateur.hide();
            } else {
                alertify.alert('Fait générateur supprimé avec succès!');
            }

            codeFaitGenerateur = '';
            BtnEnregistrer.html('<i class="fa fa-save"></i> Enregister');

            loadFaitGenerateursByService(cmbServiceAssiette.val());
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function removeFaitGenerateur(code) {

    alertify.confirm('Etes-vous sûrs de vouloir supprimer ?', function () {
        codeFaitGenerateur = code;
        saveFaitGenerateur(0);
    });

}

function checkDoublon() {
    for (var i = 0; i < faitGenerateurList.length; i++) {
        if (faitGenerateurList[i].intitule == inputgenerateur) {

        }
    }
    return true;
}

function addCleRepartition(code, entitebene) {
    codeArticleBudgetaire = code;
    cleRepartitionModal.modal('show');
    codeclerapartition.val(code);
    //tauxactegenerateur.val(taux);
    entitebeneficiaire.val(entitebene);

}

function updateExerciceFiscal(code, intitule, debut, i) {
    id_code = code;
    $('#div_code').attr('style', 'display:block');
    code_ef.val(code);
    inputTitre.val(intitule);
    dateDebut.val(debut);

    var lastDate = getCloseDateByYear(dateDebut.val());
    dateCloture.val(lastDate);

    id_maj = '1';
}

function deleteCleRepartition(code, i) {
    annee = code;
    alertify.confirm('Etes-vous sûr de vouloir supprimer cet exercice fiscal ?', function () {

        loadExerciceFiscal('deleteExerciceFiscal');

    });

}

function deleteE(code, i) {
    annee = code;
    alertify.confirm('Etes-vous sûr de vouloir supprimer cet exercice fiscal ?', function () {

        loadExerciceFiscal('deleteExerciceFiscal');

    });

}

function setTresorValue(value) {

    if (document.getElementById('cbtresorpart').checked == true) {
        cbtresorpart = true;
    } else {
        cbtresorpart = false;
    }

}

function AddCleRepartition(article_bugdetaire, tauxactegenerateur, entitearticle, etat, cbtresorpart) {

    if (checkIfTresor(cbtresorpart) != true) {

        var taux = checkIfGetTaux() + parseInt(tauxactegenerateur);
        if (taux <= 100) {

            var article = {};

            article.article_bugdetaire = codeArticleBudgetaire;
            article.tauxactegenerateur = tauxactegenerateur;
            article.entitearticle = entitearticle;
            article.etat = etat;
            article.cbtresorpart = cbtresorpart;

            listclerepartition.push(article);

            buildTable(listclerepartition);

            $('#codeclerapartition').val('');
            $('#tauxactegenerateur').val('');
            $('#entitearticle').val('');

        } else {
            alertify.alert('impossible d\'insérer. Revoir le taux');
        }

    }
}

if (listclerepartition[i].cbtresorpart == true) {
    totalTauxTresor += parseInt(listclerepartition[i].tauxactegenerateur);
}

function buildTable(liste) {
    var tbody = $('#tableFaitGenerateur tbody');
    tbody.html('');
    var tr = '';
    for (var i = 0; i < liste.length; i++) {
        tr += '<tr>';
        tr += '<td>' + liste[i].entitearticle + '</td>';

        tr += '<td>' + liste[i].tauxactegenerateur + '</td>';
        if (listclerepartition[i].cbtresorpart == true) {
            tr += '<td>' + "tresor" + '</td>';
        } else {
            tr += '<td>' + "Autres" + '</td>';
        }

        tr += '<td><button class="btn btn-primary" onclick="updateData("id")">Modifier</button>';
        tr += '<td><button class="btn btn-success">Supprimer</button>';
        tr += '</tr>';

    }

    tbody.html(tr);

    generationactegenerateur.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function updateclerepartition(entite, tauxacte) {
    entitearticle.val(entite)
    tauxactegenerateur.val(tauxacte);


}

function checkEntiteBeneficiaire(entitearticle) {
    for (var i = 0; i < listclerepartition.length; i++) {
        if (listclerepartition[i].entitearticle == entitearticle) {
            return true;
        }
    }
    return false;
}

function checkTaux(entitearticle, taux) {
    for (var i = 0; i < listclerepartition.length; i++) {
        if (listclerepartition[i].entitearticle == entitearticle) {
            var tauxActuel = parseInt(listclerepartition[i].tauxactegenerateur) + parseInt(taux);
            listclerepartition.splice(i, 1);
            return tauxActuel;
        }
    }
    return 0;
}

function checkIfTresor(cbtresorpart) {
    var ret = false;
    for (var i = 0; i < listclerepartition.length; i++) {
        if (listclerepartition[i].cbtresorpart == cbtresorpart && listclerepartition[i].cbtresorpart == true) {
            ret = true;
        }
    }
    return ret;
}

function checkIfGetTaux() {

    var totalTauxTresor = 0;
    var totalTauxAutres = 0;

    for (var i = 0; i < listclerepartition.length; i++) {
        if (listclerepartition[i].cbtresorpart == true) {
            totalTauxTresor += parseInt(listclerepartition[i].tauxactegenerateur);
        } else {
            totalTauxAutres += parseInt(listclerepartition[i].tauxactegenerateur);
        }
    }
    var totalTaux = Math.abs(parseInt(totalTauxAutres) + parseInt(totalTauxTresor));
    return totalTaux;
}


var updateData = function (id) {
    var entitearticle = document.getElementById('entitearticle').value;
    var tauxactegenerateur = document.getElementById('tauxactegenerateur').value;

    listclerepartition[id].entitearticle = entitearticle;
    listclerepartition[id].tauxactegenerateur = tauxactegenerateur;
    listclerepartition.push(article);
    buildTable(listclerepartition);

}