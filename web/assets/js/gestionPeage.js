/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var
        codeResponsible = '',
        codeTypeAssjetti = '',
        nameResponsible = '',
        selectedBien = '',
        yes4Assujetti = true; //Pour afficher les biens non loués

var
        tabBiens,
        tabArticles;
var
        tableBiens,
        tableArticles,
        tablePeriodeDeclaration;
var
        lblLegalForm,
        lblNameResponsible,
        lblAddress;
var
        btnCallModalSearchResponsable,
        btnNewBien,
        btnPasserAB, btnPasserAB2, btnPasserAB21,
        btnLocateBien, btnApplyTarication;
var
        tempBienList = [],
        tempArticleList = [];

var modalPeriodeDeclaration;
var lbl1, lbl2, lbl3;
var typePage;
var modalDetailDeclaration;
var lblNameAssujettiModal, lblAdress,
        lblNoteCalcul, lblAgentTaxateur, lblAgentValidateur, lblAgentOrdonnateur, btnVoirDeclaration;

var archiveContent = '';
var redressementObj = null;
var modalMiseEnQuarantaine, textaeraObservation, btnMiseQuarantaine;
var periodeDeclarationId, periodeDeclarationName;
var codeAssujettiSave;
var modalInfoMiseEnQuarantaine, valuePeriode2, valueDate2, valueAgent2, valueObservation;
var valuePeriode1, valueAgent1;
var numberChecked = 0;
var divTarification, divButtonAppliquer;
var divChooseTaxeTaxe, hrSeparator;
var taxePeagesList = [], palierList = [];
var cmbTaxesPeages, cmbTarification;
var bienListPublic, bienIDSelected;
var taxationList = [];

var bienName = '';
var tarifName = '';

$(function () {

    mainNavigationLabel.text('PEAGE');
    secondNavigationLabel.text('Assujettissement');

    tabBiens = $('#tabBiens');
    tabArticles = $('#tabArticles');
    assujettiModal = $('#assujettiModal');
    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    tableBiens = $('#tableBiens');
    tableArticles = $('#tableArticles');
    tablePeriodeDeclaration = $('#tablePeriodeDeclaration');
    modalPeriodeDeclaration = $('#modalPeriodeDeclaration');
    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');
    lbl3 = $('#lbl3');
    modalInfoMiseEnQuarantaine = $('#modalInfoMiseEnQuarantaine');
    valuePeriode2 = $('#valuePeriode2');
    valueDate2 = $('#valueDate2');
    valueAgent2 = $('#valueAgent2');
    valueObservation = $('#valueObservation');
    valuePeriode1 = $('#valuePeriode1');
    valueAgent1 = $('#valueAgent1');
    modalMiseEnQuarantaine = $('#modalMiseEnQuarantaine');
    textaeraObservation = $('#textaeraObservation');
    btnMiseQuarantaine = $('#btnMiseQuarantaine');
    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');
    divTarification = $('#divTarification');
    divButtonAppliquer = $('#divButtonAppliquer');
    divChooseTaxeTaxe = $('#divChooseTaxeTaxe');
    hrSeparator = $('#hrSeparator');
    cmbTaxesPeages = $('#cmbTaxesPeages');
    cmbTarification = $('#cmbTarification');
    btnApplyTarication = $('#btnApplyTarication');

    var urlCode = getUrlParameter('id');
    if (jQuery.type(urlCode) !== 'undefined') {
        codeResponsible = atob(urlCode);
        refreshDataAssujetti();
        loadBiens(codeResponsible, '0');
    }

    tabBiens.tab('show');
    btnCallModalSearchResponsable.click(function (e) {
        e.preventDefault();
        yes4Assujetti = true;
        allSearchAssujetti = '1';
        resetSearchAssujetti();
        assujettiModal.modal('show');
    });

    btnLocateBien = $('#btnLocateBien');
    btnLocateBien.click(function (e) {
        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti');
            return;
        }

        codeLocataireBien = codeResponsible;
        yes4Assujetti = false;
        allSearchAssujetti = '1';
        resetSearchAssujetti();
        var messageLocation = 'Location d\'un bien :<br/>'
                + '<br/>- Rechercher et sélectionner le propiétaire'
                + '<br/>- Sélectionner le bien à louer'
                + '<br/>- Renseigner les informations du bien.'
                + '<br/><br/>Voulez-vous continuer ?';
        alertify.confirm(messageLocation, function () {
            assujettiModal.modal('show');
        });
    });

    btnNewBien = $('#btnNewBien');
    btnNewBien.click(function (e) {
        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        window.location = 'edition-bien?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible);
    });

    btnPasserAB = $('#btnPasserAB');
    btnPasserAB2 = $('#btnPasserAB2');
    btnPasserAB21 = $('#btnPasserAB21');
    btnMiseQuarantaine.click(function (e) {

        e.preventDefault();
        if (textaeraObservation.val() == empty) {

            alertify.alert('Veuillez d\'abord motiver la mise en quarantaine de cette période');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir mettre en quarantaine cette période de déclaration : ' + periodeDeclarationName + ' ?', function () {
            miseEnQuarantaine();
        });
    });

    btnPasserAB.click(function (e) {

        e.preventDefault();
        $('#modalAssujMultipleBien').modal('show');

    });

    btnPasserAB2.click(function (e) {

        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        setBienData(JSON.stringify(tempBienList));
        window.location = 'assujettissement?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&type=' + btoa(codeTypeAssjetti) + '&bien=' + btoa(selectedBien);
        selectedBien = '';
    });

    btnPasserAB21.click(function (e) {

        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        if (!controlAccess('2004')) {
            alertify.alert('Vous n\'avez pas le droit de faire un assujettissement.');
            return;
        }

        setBienData(JSON.stringify(tempBienList));
        window.location = 'assujettissement-multiple?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&type=' + btoa(codeTypeAssjetti) + '&bien=' + btoa(selectedBien);
        selectedBien = '';
    });

    printBien('');
    printArticles('');
    var paramGuideValue = JSON.parse(getTourParam());
    if (paramGuideValue === 1) {

        Tour.run([
            {
                element: $('#btnCallModalSearchResponsable'),
                content: '<p style="font-size:17px;color:black">Commencer par rechercher un assujetti.</p>',
                language: 'fr',
                position: 'right',
                close: 'false'}
        ]);
    } else {
        Tour.run();
    }

    modalDetailDeclaration = $('#modalDetailDeclaration');
    btnVoirDeclaration = $('#btnVoirDeclaration');
    btnVoirDeclaration.click(function (e) {
        e.preventDefault();
        initPrintData(archiveContent);
    });

    cmbTarification.on('change', function () {
        var code = cmbTarification.val();
        getTarification(code);
    });

    btnApplyTarication.click(function (e) {
        e.preventDefault();

        saveTarification();
    });

    getTaxesPeages();
});

function getSelectedAssujetiData() {

    if (yes4Assujetti == true) {

        codeResponsible = selectAssujettiData.code;
        codeTypeAssjetti = selectAssujettiData.codeForme;
        nameResponsible = selectAssujettiData.nomComplet;
        codeLocataireBien = codeResponsible;
        codeAssujettiSave = codeResponsible;
        lblNameResponsible.html(nameResponsible);
        lblLegalForm.html(selectAssujettiData.categorie);
        lblAddress.html(selectAssujettiData.adresse);
        lbl1.show();
        lbl2.show();
        lbl3.show();
        setAssujettiData(JSON.stringify(selectAssujettiData));
        loadBiens(codeResponsible, '0');
    } else {

        if (codeResponsible == selectAssujettiData.code) {
            alertify.alert('Vous ne pouvez pas louer vos propres biens.');
            return;
        }

        codeProprietaireBien = selectAssujettiData.code;
        lblNameProprietaire.html(selectAssujettiData.nomComplet + (' (Propriétaire)'));
        lblAddressProprietaire.html(selectAssujettiData.adresse);
        loadBiens(codeProprietaireBien, '1');
    }

}

function refreshDataAssujetti() {

    var jsonAssujetti = getAssujettiData();
    var assujettiObj = JSON.parse(jsonAssujetti);
    codeResponsible = assujettiObj.code;
    nameResponsible = assujettiObj.nomComplet;
    lblNameResponsible.html(nameResponsible);
    lblLegalForm.html(assujettiObj.categorie);
    lblAddress.html(assujettiObj.adresse);
    lbl1.show();
    lbl2.show();
    lbl3.show();
}
function loadBiens(personneCode, forLocation) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBiensPersonne',
            'codePersonne': personneCode,
            'forLocation': forLocation,
            'codeService': userData.serviceCode
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();
                var assujettissementList = $.parseJSON(JSON.stringify(response));
                var bienList = $.parseJSON(JSON.stringify(assujettissementList.listBiens));
                bienListPublic = $.parseJSON(JSON.stringify(assujettissementList.listBiens));
                printBien(bienList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printBien(bienList) {

    tempBienList = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"><center><input id="check_all" name="check_all" onclick="cocheAll()" class="form-control" type="checkbox" style="width:12px"/></center></th>';
    //header += '<th scope="col" style="width:10%"> N° PLAQUE </th>';
    header += '<th scope="col" style="width:15%"> BIEN (AUTOMOBILE) </th>';
    //header += '<th scope="col" style="width:25%"> DESCRIPTION BIEN </th>';
    header += '<th scope="col" style="width:25%"> ADRESSE </th>';
    header += '<th scope="col" style="width:20%"> TARIFICATION </th>';
    header += '<th ></th>';
    header += '<th hidden="true" scope="col">Id bien</th>';
    header += '</tr></thead>';

    var body = '<tbody>';
    for (var i = 0; i < bienList.length; i++) {

        var tarif = '<span id="span_' + i + '">Aucun tarif appliquer</span>';
        if (bienList[i].hasTarif == '1') {
            var dataTarif = bienList[i].dataTarif;
            tarif = '<span id="span_' + i + '">' + dataTarif.tauxAB + ' ' + dataTarif.devise;
            tarif += '<br/>' + dataTarif.intituleTarif + '</span>';
        }

        var proprietaire = bienList[i].proprietaire;

        var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
        var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';

        descriptionBien = natureInfo + '<br/>' + categorieInfo + '<hr/>' + bienList[i].complement;

        body += '<tr>';
        body += '<td style="vertical-align:middle;width:5%"><center><input id="check_' + i + '" name="check_' + i + '" onclick="cocheDecoche(\'' + i + '\',\'' + bienList[i].idBien + '\')" class="form-control" type="checkbox" /></center></td>';
        //body += '<td style="vertical-align:middle">' + bienList[i].immatriculation.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;width:35%">' + descriptionBien + '</td>';
        //body += '<td>' + bienList[i].descriptionBien + '</td>';
        body += '<td style="vertical-align:middle;width:30%">' + bienList[i].chaineAdresse.toUpperCase() + '</td>';
        body += '<td id="tdSpan_' + i + '" style="vertical-align:middle;width:20%">' + tarif + '</td>';
        body += '<td style="vertical-align:middle;width:10%">'
                + '<a style="display:none" onclick="editBien(\'' + bienList[i].idBien + '\',\'' + bienList[i].codeTypeBien + '\')" class="btn btn-success" title="Modifier le bien"><i class="fa fa-edit"></i></a>&nbsp;'
                + '<a style="display:none" onclick="disabledBien(\'' + bienList[i].idAcquisition + '\',\'' + proprietaire + '\')" class="btn btn-danger" title="Désactiver le bien"><i class="fa fa-trash-o"></i></a>&nbsp;'
                + '<a style="display:none" onclick="storyBien(\'' + 0 + '\')" class="btn btn-warning" title="Voir l\'historique"><i class="fa fa-history"></i></a></td>';
        body += '<td hidden="true">' + bienList[i].idBien + '</td>';
        body += '</tr>';
        var bien = new Object();
        bien.idBien = bienList[i].idBien;
        bien.intituleBien = bienList[i].intituleBien;
        bien.chaineAdresse = bienList[i].chaineAdresse;
        bien.codeAP = bienList[i].codeAP;
        bien.coche = false;
        bien.tarif = tarif;
        bien.tarifId = empty;
        bien.devise = empty;
        bien.tauxTarif = empty;
        bien.tarifApply = 0;
        tempBienList.push(bien);
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableBiens.html(tableContent);
    var dtBien = tableBiens.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 5,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
//        select: {
//            style: 'os',
//            blurable: true
//        },
        datalength: 5
    });
    $('#tableBiens tbody').on('click', 'tr', function () {
        var data = dtBien.row(this).data();
        selectedBien = data[5];
    });
}

function printArticles(articleList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" hidden="true"> Id </th>';
    header += '<th scope="col" hidden="true"> Code </th>';
    header += '<th scope="col" style="width:300px"> SERVICE D\'ASSIETE </th>';
    header += '<th scope="col" style="width:30px"> BIEN </th>';
    header += '<th scope="col" style="width:400px"> ARTICLE BUDGETAIRE </th>';
    header += '<th scope="col" style="width:100px"> PERIODICITE </th>';
    header += '<th scope="col" style="width:100px"> ETAT </th>';
    header += '<th scope="col" style="width:150px"> </th>';
    header += '</tr></thead>';
    tempArticleList = [];

    var body = '<tbody id="tbodyArticles">';
    var firstLineAB = '';
    for (var i = 0; i < articleList.length; i++) {

        var article = {};
        firstLineAB = articleList[i].intituleAB;
        if (articleList[i].intituleTarif.toUpperCase() != 'TOUT' && articleList[i].intituleTarif.toUpperCase() != 'UNIQUE') {
            firstLineAB += ' : ' + articleList[i].intituleTarif;
        }

        if (firstLineAB.length > 40) {
            firstLineAB = firstLineAB.substring(0, 40) + ' ...';
        }

        article.idAssujettissement = articleList[i].idAssujettissement;
        article.codeAB = articleList[i].codeAB;
        article.intituleAB = articleList[i].intituleAB.toUpperCase();
        article.valeurBase = articleList[i].valeurBase;
        article.unite = articleList[i].unite;
        article.duree = articleList[i].duree;
        article.dateDebut = articleList[i].dateDebut;
        article.dateFin = articleList[i].dateFin;
        article.dateFin2 = articleList[i].dateFin2;
        article.reconduction = articleList[i].reconduction;
        article.nombreJour = articleList[i].nombreJour;
        article.nombreJourLimite = articleList[i].nombreJourLimite;
        article.nombreJourLimitePaiement = articleList[i].nbreJourLegalePaiement;
        article.echeanceLegale = articleList[i].echeanceLegale;
        article.periodeEcheance = articleList[i].periodeEcheance;
        article.codePeriodiciteAB = articleList[i].codePeriodiciteAB;
        article.listPeriodesDeclarations = articleList[i].listPeriodesDeclarations;
        article.montantDu = formatNumber(articleList[i].montant, articleList[i].devise);
        article.intituleTarif = articleList[i].intituleTarif;
        article.libellePeriodiciteAB = articleList[i].libellePeriodiciteAB;
        tempArticleList.push(article);

        var etat = 'VALIDE';
        var etatColor = 'black';
        if (articleList[i].etat == 0) {
            etat = 'ANNULE';
            etatColor = 'red';
        }

        body += '<tr>';
        body += '<td hidden="true">' + articleList[i].idAssujettissement + '</td>';
        body += '<td hidden="true">' + articleList[i].codeAB + '</td>';
        body += '<td style="width:20%">' + articleList[i].secteurActivite.toUpperCase() + '</td>';
        body += '<td style="text-align:left;width:25%"><span style="font-weight:bold">' + articleList[i].intituleBien.toUpperCase() + '</span><br/><br/>' + articleList[i].infoComplementBien + '<br/><br/>' + articleList[i].adresseBien + '</td>';
        if (articleList[i].isManyAB) {
            body += '<td style="width:20%" title="Plusieurs"><a href="#3" style="font-weight:bold;color:blue;text-decoration:underline">Voir la liste</a></td>';
        } else {
            body += '<td style="width:20%" title="' + articleList[i].intituleAB + '"><span style="font-weight:bold;">' + articleList[i].codeOfficiel + '</span> / ' + firstLineAB + '</td>';
        }

        body += '<td style="width:10%"> ' + articleList[i].libellePeriodiciteAB + '</td>';
        body += '<td style="width:8%;color:' + etatColor + '"> ' + etat + '</td>';

        var taxButton = '<a  onclick="gotoTaxation(\'' + articleList[i].idAssujettissement + '\',' + articleList[i].isManyAB + ')" class="btn btn-success" title="Passer à la taxation"><i class="fa fa-arrow-right"></i></a>&nbsp;';
        var generateButton = '<a  onclick="generatePeriode(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-success" title="Générer période"><i class="fa fa-plus-circle"></i></a>&nbsp;';
        var deleteButton = '<a onclick="removeAssujettissement(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-danger" title="Annuler cet assujettissement"><i class="fa fa-close"></i></a>&nbsp';
        if (!controlAccess('3002')) {
            taxButton = '';
        }
        if (!controlAccess('3011')) {
            generateButton = '';
        }
        if (!controlAccess('3012')) {
            deleteButton = '';
        }

        if (articleList[i].etat == 1) {

            body += '<td style="text-align:center;width:17%">'
                    + '' + deleteButton + ''
                    + '<a onclick="openPeriodes(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-warning" title="Afficher les périodes"><i class="fa fa-list"></i></a>&nbsp;'
                    + '' + generateButton + ''
                    + '' + taxButton + '</td>';
        } else {

            body += '<td style="text-align:center;width:10%">'
                    + '<a onclick="openPeriodes(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-warning" title="Afficher les périodes"><i class="fa fa-list"></i></a></td>&nbsp;';
        }

        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableArticles.html(tableContent);
    var dtArticles = tableArticles.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 3,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os', blurable: true
        },
        datalength: 3
    });
    $('#tableArticles tbody').on('click', 'tr', function () {
        var data = dtArticles.row(this).data();
    });
}

function printOtherBien(bienList) {

    var dataBien = '<option value ="0">-- Sélectionner --</option>';
    for (var i = 0; i < bienList.length; i++) {
        dataBien += '<option value =' + bienList[i].idBien + '>' + bienList[i].intituleBien + '</option>';
    }

    return dataBien;
}

function generatePeriode(idAssujettissement) {

    alertify.confirm('Etes-vous sûre de vouloir générer une période supplémentaire ?', function () {

        var dateFin = '', nombreJour = '', nombreJourLimite = '', nombreJourLimitePaiement = '', echeanceLegale = '', codePeriodiciteAB = '', periodeEcheance = '';
        for (var i = 0; i < tempArticleList.length; i++) {

            if (tempArticleList[i].idAssujettissement == idAssujettissement) {

                dateFin = tempArticleList[i].dateFin2;
                nombreJour = tempArticleList[i].nombreJour;
                nombreJourLimite = tempArticleList[i].nombreJourLimite;
                echeanceLegale = tempArticleList[i].echeanceLegale;
                periodeEcheance = tempArticleList[i].periodeEcheance;
                codePeriodiciteAB = tempArticleList[i].codePeriodiciteAB;
                nombreJourLimitePaiement = tempArticleList[i].nombreJourLimitePaiement;
                break;
            }
        }

        createPeriodeDeclarationIfNotExist(1, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance);
    });
}

function gotoTaxation(idAssujettissement, isMany) {

    alertify.confirm('Etes-vous sûre de vouloir passer à la déclaration proprement dite ?', function () {

        var dateFin = '', nombreJour = '', nombreJourLimite = '', nombreJourLimitePaiement = '', echeanceLegale = '', codePeriodiciteAB = '', periodeEcheance = '';
        for (var i = 0; i < tempArticleList.length; i++) {

            if (tempArticleList[i].idAssujettissement == idAssujettissement) {

                dateFin = tempArticleList[i].dateFin2;
                nombreJour = tempArticleList[i].nombreJour;
                nombreJourLimite = tempArticleList[i].nombreJourLimite;
                echeanceLegale = tempArticleList[i].echeanceLegale;
                periodeEcheance = tempArticleList[i].periodeEcheance;
                codePeriodiciteAB = tempArticleList[i].codePeriodiciteAB;
                nombreJourLimitePaiement = tempArticleList[i].nombreJourLimitePaiement;
                break;
            }
        }

        createPeriodeDeclarationIfNotExist(0, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance, isMany);
    });
}

var tempPeriodeDeclaration;
function openPeriodes(idAssujettissement) {

    for (var i = 0; i < tempArticleList.length; i++) {

        if (tempArticleList[i].idAssujettissement == idAssujettissement) {

            var header = empty, body = empty;
            tempPeriodeDeclaration = tempArticleList[i].listPeriodesDeclarations;
            header += '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>';
            header += '<th style="text-align:left">EXERCICE</th>';
            header += '<th style="text-aLign:left">ECHEANCE DEC.</th>';
            header += '<th style="text-aLign:left">ECHEANCE PAIE.</th>';
            header += '<th style="text-aLign:left">DATE TAXATION</th>';
            header += '<th style="text-aLign:left">NUM. NOTE TAX.</th>';
            header += '<th style="text-aLign:left">NUM. NOTE NP</th>';
            header += '<th style="text-aLign:right">MONTANT DU</th>';
            header += '<th style="text-aLign:right">PENALITE DU</th>';
            header += '<th style="text-aLign:right">TOTAL DU</th>';
            header += '<th style="text-aLign:right">TOTAL PAYE</th>';
            header += '<th style="text-aLign:right">RESTE A PAYER</th>';
            header += '<th></th>';
            header += '</tr></thead>';
            for (var j = 0; j < tempPeriodeDeclaration.length; j++) {

                var periodeDeclaration = tempPeriodeDeclaration[j];
                var btnMiseQuarantaine = '';
                if (controlAccess('3015')) {

                    if (periodeDeclaration.noteTaxation == '' && periodeDeclaration.statePeriodeDeclaration == 1) {
                        btnMiseQuarantaine = '&nbsp;<button type="button" class="btn btn-danger" title="Cliquez ici pour mettre en quarantaine cette période de déclaration" onclick="loadModalMiseEnQuarantaine(\'' + periodeDeclaration.periodeId + '\',\'' + periodeDeclaration.periode + '\')"><i class="fa fa-check-circle"></i>&nbsp;&nbsp;Annuler</button>';
                    } else if (periodeDeclaration.statePeriodeDeclaration == 0) {
                        btnMiseQuarantaine = '&nbsp;<button type="button" class="btn btn-default" title="Cliquez ici pour afficher les observations de la mise mettre en quarantaine" onclick="loadModalInfoMiseEnQuarantaine(\'' + periodeDeclaration.periode + '\',\'' + periodeDeclaration.observation + '\',\'' + periodeDeclaration.agentMaj + '\',\'' + periodeDeclaration.dateMaj + '\')"><i class="fa fa-list"></i>&nbsp;&nbsp;Afficher</button>';
                    } else {
                        btnMiseQuarantaine = '';
                    }
                }

                var montantDu = empty, penaliteDu = empty, totalDu = empty, montantPayer = empty, restePayer = empty;
                var noteTaxation = periodeDeclaration.noteTaxation;
                var colorEcheance = 'color: black';
                var colorEcheancePaiement = 'color: black';
                if (noteTaxation != empty) {

                    montantDu = formatNumber(periodeDeclaration.montantDu, periodeDeclaration.devise);
                    penaliteDu = formatNumber(periodeDeclaration.penaliteDu, periodeDeclaration.devise);
                    totalDu = formatNumber(periodeDeclaration.totalDu, periodeDeclaration.devise);
                    montantPayer = formatNumber(periodeDeclaration.montantPayer, periodeDeclaration.devise);
                    restePayer = formatNumber(periodeDeclaration.restePayer, periodeDeclaration.devise);
                }

                switch (periodeDeclaration.estPenalise) {
                    case "1":
                        colorEcheance = 'color: red';
                        break;
                    case "0":
                        colorEcheance = 'color: black';
                        break;
                }

                switch (periodeDeclaration.estPenalisePaiement) {
                    case "1":
                        colorEcheancePaiement = 'color: red';
                        break;
                    case "0":
                        colorEcheancePaiement = 'color: black';
                        break;
                }

                body += '<tr>';
                body += '<td style="text-align:left;width:9%">' + periodeDeclaration.periode + '</td>';
                body += '<td style="text-align:left;width:5%;' + colorEcheance + '">' + periodeDeclaration.dateLimite + '</td>';
                body += '<td style="text-align:left;width:5%;' + colorEcheancePaiement + '">' + periodeDeclaration.dateLimitePaiement + '</td>';
                body += '<td style="text-align:left;width:5%">' + periodeDeclaration.dateDeclaration + '</td>';
                body += '<td style="text-align:left;width:10%">' + noteTaxation + '</td>';
                body += '<td style="text-align:left;width:10%">' + periodeDeclaration.notePerceptionManuel + '</td>';
                body += '<td style="text-align:right;width:10%">' + montantDu + '</td>';
                body += '<td style="text-align:right;width:10%">' + penaliteDu + '</td>';
                body += '<td style="text-align:right;width:10%">' + totalDu + '</td>';
                body += '<td style="text-align:right;width:10%">' + montantPayer + '</td>';
                body += '<td style="text-align:right;width:10%">' + restePayer + '</td>';
                if (noteTaxation != empty) {
                    body += '<td style="text-align:center;width:9%"><button type="button" class="btn btn-warning" onclick="showDeclarationDetail(\'' + noteTaxation + '\')"><i class="fa fa-list fa-1x"></i>&nbsp;&nbsp;Voir détails</button></td>';
                } else {
                    body += '<td style="text-align:center;width:9%">' + btnMiseQuarantaine + '</td>';
                }
            }

            body += '</tr></tbody>';
            var tableContent = header + body;
            tablePeriodeDeclaration.html(tableContent);
            tablePeriodeDeclaration.DataTable({
                language: {
                    processing: "Traitement en cours...",
                    track: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible pour le critère sélectionné",
                    search: "Rechercher par N° document _INPUT_  ",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                },
                info: false,
                destroy: true,
                searching: false,
                paging: true,
                order: [[0, 'asc']],
                lengthChange: false,
                tracking: false,
                ordering: false,
                pageLength: 5,
                lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
                select: {
                    style: 'os',
                    blurable: true
                },
                datalength: 3
            });
            modalPeriodeDeclaration.modal('show');
            break;
        }
    }
}

function removeAssujettissement(idAssujettissement) {

    alertify.confirm('Etes-vous sûre de vouloir retirer cet acte générateur du bien ?', function () {
        desactivateAssujettissement(idAssujettissement);
    });
}

function createPeriodeDeclarationIfNotExist(isGenerate, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance, isMany) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'createPeriodeDeclaration',
            'isGenerate': isGenerate,
            'idAssujettissement': idAssujettissement,
            'dateFin': dateFin,
            'nombreJour': nombreJour,
            'nombreJourLimite': nombreJourLimite,
            'nbreJourLegalePaiement': nombreJourLimitePaiement,
            'echeanceLegale': echeanceLegale, 'codePeriodiciteAB': codePeriodiciteAB,
            'periodeEcheance': periodeEcheance
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Veuillez patientier...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                if (response == '1' && isGenerate == 0) {

                    if (isMany) {
                        setAssujettiData(JSON.stringify(selectAssujettiData));
                    }

                    window.location = isMany ? 'taxation-repetitive_multiple?assuj=' + btoa(idAssujettissement) : 'taxation-repetitive?id=' + btoa(idAssujettissement);
                } else if (response == '1' && isGenerate == 1) {

                    alertify.alert('Des périodes supplémnetaires sont générées avec succès');
                    loadBiens(codeResponsible, '0');
                } else if (response == '0') {

                    alertify.alert('Echec lors du passage à la déclaration');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function desactivateAssujettissement(idAssujettissement) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'desactivateAssujettissement',
            'idAssujettissement': idAssujettissement
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Annulation de l\'assujettissement en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                if (response == '1') {

                    alertify.alert('l\'assujettissement est annulé avec succès.');
                    setTimeout(function () {

                        loadBiens(codeResponsible, '0');
                    }
                    , 1000);
                } else if (response == '0') {
                    alertify.alert('Echec lors de l\'annulation de l\'assujettissement');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function resetSearchAssujetti() {

    inputValueResearchAssujetti.val('');
    printAssujettiTable('');
}

function disabledBien(idAcquisition, proprietaire) {

    alertify.confirm('Etes-vous sûre de vouloir rétirer ce bien ?', function () {

        $.ajax({
            type: 'POST',
            url: 'peage_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'desactivateBienAcquisition',
                'idAcquisition': idAcquisition
            },
            beforeSend: function () {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Rétrait du bien en cours ...</h5>'});
            },
            success: function (response)
            {

                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    if (response == '1') {
                        alertify.alert('Rétrait du bien effectuer avec succès.');
                        loadBiens(codeResponsible, '0');
                    } else if (response == '0') {
                        alertify.alert('Echec lors du rétrait du bien');
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });
}

function showDeclarationDetail(nc) {

    alertify.confirm('Etes-vous sûre de vouloir visualiser les détails de cette taxation ?', function () {

        setRegisterType('RNT');
        window.open('registre-notes-taxations?nc=' + btoa(nc), '_blank');
    });
}

function loadModalMiseEnQuarantaine(periodeId, periodeName) {

    periodeDeclarationId = periodeId;
    periodeDeclarationName = periodeName;
    textaeraObservation.val(empty);
    valuePeriode1.html(periodeName);
    valueAgent1.html(userData.nomComplet);
    modalMiseEnQuarantaine.modal('show');
}

function miseEnQuarantaine() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'miseQuarantaine',
            'periodeId': periodeDeclarationId,
            'observation': textaeraObservation.val(),
            'userId': userData.idUser
        },
        beforeSend: function () {

            modalMiseEnQuarantaine.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>La mise en quarantaine en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                modalMiseEnQuarantaine.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalMiseEnQuarantaine.unblock();
                modalMiseEnQuarantaine.modal('hide');
                alertify.alert('La mise en quarantaine s\'est effectuée avec succès.');
                loadBiens(codeAssujettiSave, '1');
                modalPeriodeDeclaration.modal('hide');
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalMiseEnQuarantaine.unblock();
            showResponseError();
        }
    });
}

function loadModalInfoMiseEnQuarantaine(periodeName, obervation, agent, date) {

    valuePeriode2.html(periodeName);
    valueDate2.html(date);
    valueAgent2.html(agent);
    valueObservation.val(obervation);
    modalInfoMiseEnQuarantaine.modal('show');
}


function editBien(idBien, codeTypeBien) {

    var codeBien = idBien + ';' + codeTypeBien;
    window.location = 'edition-bien?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&codeBien=' + btoa(codeBien);
}

function cocheDecoche(i, bienId) {

    bienIDSelected = bienId;

    var res = document.getElementById("check_" + i).checked;
    if (res) {
        numberChecked++;
        if (tempBienList.length == numberChecked) {
            document.getElementById("check_all").checked = true;
        }

        $('#span_' + i).addClass('blink_me');
        $('#span_' + i).attr('style', 'color :red');
        tempBienList[i].coche = true;

        var code = cmbTarification.val();
        if (code != '0') {
            getTarification(code);
        }

    } else {
        numberChecked--;
        document.getElementById("check_all").checked = false;

        $('#span_' + i).removeClass('blink_me');
        $('#span_' + i).removeAttr('style');
        tempBienList[i].coche = false;

        $('#span_' + i).html(tempBienList[i].tarif);
    }

    var messageChecked = (numberChecked == 0) ? '0 bien sélectionné.'
            : (numberChecked == 1) ? '1 bien sélectionné.' : (numberChecked + ' biens sélectionnés.');
    $('#idLabelCheckedNumber').text(messageChecked);
    if (numberChecked == 0) {
        divTarification.attr('style', 'visibility:hidden');
        divButtonAppliquer.attr('style', 'visibility:hidden');
        divChooseTaxeTaxe.attr('style', 'display:none');
        hrSeparator.attr('style', 'display:none');
    } else {
        divTarification.attr('style', 'visibility:visible');
        divButtonAppliquer.attr('style', 'visibility:visible');
        divChooseTaxeTaxe.attr('style', 'display:block');
        hrSeparator.attr('style', 'display:block');
    }

}

function cocheAll() {

    var res = document.getElementById("check_all").checked;
    var max = tempBienList.length;
    if (max == 0) {
        document.getElementById("check_all").checked = false;
    }

    if (res) {
        numberChecked = 0;
    }

    for (var i = 0; i < max; i++) {
        var idControl = "check_" + i;
        if (res) {
            document.getElementById(idControl).checked = true;
        } else {
            document.getElementById(idControl).checked = false;
        }
        cocheDecoche(i);
    }

    numberChecked = (res) ? max : 0;
}

function getTaxesPeages() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'siteCode': userData.SiteCode,
            'operation': 'loadTaxesPeages'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5> Initialisation des données ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            taxePeagesList = $.parseJSON(JSON.stringify(response));

            var content = '';

            for (var i = 0; i < taxePeagesList.length; i++) {

                content += '<option value="' + taxePeagesList[i].codeAB + '">' + taxePeagesList[i].intituleAB + '</option>';

                if (i == 0) {

                    if (taxePeagesList[i].hasPalier == '1') {

                        palierList = taxePeagesList[i].palierList;

                        var contentTarif = '<option value="0">-- Sélectionner --</option>';
                        for (var j = 0; j < palierList.length; j++) {
                            contentTarif += '<option value="' + palierList[j].codeTarif + '">' + palierList[j].intituleTarif + '</option>';
                        }

                        cmbTarification.html(contentTarif);
                    }

                }
            }

            cmbTaxesPeages.html(content);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function getTarification(code) {

    if (code == '0') {
        for (var i = 0; i < tempBienList.length; i++) {
            if (tempBienList[i].coche == true) {
                $('#span_' + i).html(tempBienList[i].tarif);
                $('#span_' + i).removeClass('blink_me');
                $('#span_' + i).removeAttr('style');
            }
        }
        return;
    }

    for (var i = 0; i < palierList.length; i++) {

        if (palierList[i].codeTarif == code) {
            setDataTableEditing(palierList[i]);
            break;
        }
    }

}

function setDataTableEditing(data) {

    for (var i = 0; i < tempBienList.length; i++) {
        var tauxAB = data.tauxAB;
        var devise = data.devise;
        var intituleTarif = data.intituleTarif;
        tarifName = data.intituleTarif;


        if (tempBienList[i].coche == true && tempBienList[i].idBien == bienIDSelected) {
            var tarif = '<span id="span_' + i + '">' + tauxAB + ' ' + devise;
            tarif += '<br/>' + intituleTarif + '</span>';

            $('#span_' + i).html(tarif);

            if (!$('#span_' + i).hasClass('blink_me')) {
                $('#span_' + i).addClass('blink_me');
            }

            $('#span_' + i).attr('style', 'color:red');

            tempBienList[i].tarifId = cmbTarification.val();
            tempBienList[i].tauxAB = data.tauxAB;
            tempBienList[i].devise = data.devise;
            tempBienList[i].tarifApply = 1;


        }
    }

    if (cmbTarification.val() !== '0') {
        cmbTarification.val('0');
    }
}

function saveTarification() {

    var result = false;

    for (var i = 0; i < tempBienList.length; i++) {

        if (tempBienList[i].tarifApply === 1) {
            result = true;

        }
    }

    if (result === false) {
        alertify.alert('Veuillez d\'abord sélectionner et appliquer le tarif pour au moins un bien automobile');
        return;
    }

    var dataList = [];

    for (var i = 0; i < tempBienList.length; i++) {
        //if (tempBienList[i].coche == true) {
        if (tempBienList[i].tarifApply === 1) {
            var data = {};
            data.i = i;
            data.idBien = tempBienList[i].idBien;
            data.codeTarif = tempBienList[i].tarifId;
            data.tauxAB = tempBienList[i].tauxAB;
            data.devise = tempBienList[i].devise;
            bienName = tempBienList[i].intituleBien;
            dataList.push(data);
        }
    }

    var bienNameInfo = '<span style="font-weight:bold">' + bienName.toUpperCase() + '</span>';
    var tarifNameInfo = '<span style="font-weight:bold">' + tarifName.toUpperCase() + '</span>';

    alertify.confirm('Etes-vous sûrs de vouloir assukettir le bien : ' + bienNameInfo + ' à ce tarif : ' + tarifNameInfo + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'peage_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'saveAssujettissementPeage',
                'codeAB': cmbTaxesPeages.val(),
                'codePersonne': codeResponsible,
                'idUser': userData.idUser,
                'tarifs': JSON.stringify(dataList)
            },
            beforeSend: function () {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours...</h5>'});
            },
            success: function (response)
            {

                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    alertify.alert('Echec lors de l\'enregistrement du tarif sélectionné');
                } else {

                    alertify.alert('L\'assujettissement s\'est effectué avec succès');

                    if (yes4Assujetti == true) {
                        loadBiens(codeResponsible, '0');
                    } else {
                        loadBiens(codeProprietaireBien, '1');
                    }

                    for (var i = 0; i < dataList.length; i++) {
                        var indice = dataList[i].i;
                        var tdControl = $('#span_' + indice);
                        tdControl.removeClass('blink_me');
                        tdControl.removeAttr('style');
                        tempBienList[indice].tarif = $('#tdSpan_' + indice).html();
                    }
                }

            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });
}