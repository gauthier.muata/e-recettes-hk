/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var userData;

var linkadminName;
var lblUserSite, lblUserSA, lblUserSiteAdresse;
var lblUserLogin, lblUserMatricule, lblUserName, lblUserFonction, lblUserSiteModal;
var lblUserSAModal, lblUserSiteAdresseModal, lblLastConnexion;
var imgSignatureAgent, divApercuSignature;

var linkMenuDahsboard, linkMenuRepertoire, linkMenuDeclaration, linkMenuTaxation, linkMenuOrdonnancement, linkMenuPaiement, linkMenuApurement, linkMenuAcquitLiberatoire;

var linkSubMenuIdentification, linkSubMenuRegistreAssujetis, linkSubMenuGestionBien, linkSubMenuAssujettissement,linkSubMenuGestionBienAutomobile;
var linkSubMenuTaxationOrdinaire, linkSubMenuClotureTaxation, linkSubMenuRegistreTaxation;
var linkSubMenuOrdonnancement, linkSubMenuRegistreNotePerception, linkSubMenuRegistreTaxationRejetee, linkSubMenuRegistreTaxationRejeteeOrdonnancement;
var linkSubMenuRegistrePaiement, linkSubMenuEditerPaiement;
var linkSubMenuApurementAdmininistratif, linkSubMenuApurementCompatable, linkSubMenuRegistreApurement, linkSubMenuRegistreApurementRejete;
var linkSubMenuEditerAcquitLiberatoire, linkSubMenuRegistreAcquitLiberatoire, linkMenuContentieux, linkSubMenuEditerReclamation;
var linkSubMenuRegistreReclamation, linkSubMenuDecisionJuridictionnelle, linkSubMenuEditerRecours, linkSubMenuRegistreRecours;
var linkSubMenuGestionExerciceFiscal;

var linkSubMenuCorrectionBienAutomobileVignette;

var linkMenuGestionUtilisateur, linkSubMenuGestionFonction,
        linkSubMenuGestionUtilisateur, linkSubMenuGestionGroupe, linkSubMenuGestionDroit,
        linkSubMenuGestionnaireAssujettis, linkSubMenuRegistreUtilisateurs,
        linkSubMenuAffecterUtilisateurDivision, linkSubMenuGestionDroitSuppUser;

var linkMenuGestionArticleBudgetaire, linkSubMenuGestionServiceAssiette,
        linkSubMenuGestionCleRepartition, linkSubMenuGestionArticleBudgetaire,
        linkSubMenuRegistreArticleBudgetaire, linkSubMenuGestionFaitGenerateur,
        linkSubMenuAffecterArticleBudgetaireBanque, linkSubMenuGestionTarif,
        linkSubMenuGestionPeriodicite, linkSubMenuRegistreTarifSitePeage, linkSubMenuRegistreProrogationDateLegale;

var linkSubMenuGestionDepotDeclaration, linkSubMenuGestionDeclaration,
        linkSubMenuRegistreDepotDeclaration, linkSubMenuRegistreRetraitDeclaration, 
        linkSubMenuRetraitDeclaration, linkSubMenuCorrectionNoteTaxation,linkSubMenuRegistreRetraitDeclarationVignette;

var linkSubMenuExonerationVignette;

var modalUpdateUserPassword;
var inputCurrentPassword, inputNewPassword, inputConfirmNewPassword;
var btnUpdatePassword;

var linkMenuRecouvrement, linkSubMenuMed, linkSubMenuMep, linkSubMenuNoteTaxation;
var linkMenuFractionnement, linkMenuAssignation;
var linkSubMenuEditerDemandeFractionnement, linkSubMenuRegistreDemandeFractionnement, linkSubMenuOrdonnancementFractionnement;
var linkSubMenuEditionAssignationBudgetaire;

var linkMenuPoursuite, linkSubMenuMiseEnDemeure, linkSubMenuAmr, linkSubMenuInvitationApayer, linkSubMenuInvitationServiceNonPaiement;
var linkMenuGestionCarte, linkSubMenuEditerCarte, linkSubMenuRgistreCartes;

var linkSubMenuGestionUser, linkSubMenuValiderAssujettis;
var linkSubMenuContrainte, linkSubMenuCommandement, linkSubMenuAtd;

var linkSubMenuTaxationCarteConducteur;
var photoConducteurExist;

var linkSubMenuDocumentOfficial;
var linkSubMenuFichePriseCharge;
var linkSubMenuBonApayer;

var linkMenuGestionBanque, linkSubMenuEditerBanque, linkSubMenuCompteBancaire,
        linkSubMenuAffecterComptePaiement, linkSubMenuTauxChange;

var linkSubMenuGestionPrevisionCredit, linkSubMenuGestionExemptes, linkMenuPeage, linkSubMenuAssujettissementAutomobile;
var linkSubMenuStatistique;

var linkMenuSiteBanque, linkSubMenuEditionSite, linkSubMenuGestionBanqueCompte,
        linkSubMenuGestionBanqueSite,
        linkSubMenuAffectationSiteBanue, linkSubMenuEditerDivision;

var linkSubMenuRegistre;

var linkMenuGestionPenalite, linkSubMenuTypePenalite, linkSubMenuEditionPenalite, linkSubMenuParamTauxPenalite;

var linkMenuMajEcheance, linkSubMenuMajEcheance;
var linkSubMenuRegistreSousProvision, linkSubMenuRegistreCredit, linkSubMenuRegistreTicket, linkSubMenuRegistreTicketAnnules, linkSubMenuGestionRemisePrepaiementPeage;

var linkMenuControle, linkSubMenuControleOrdonancement, linkSubMenuOrdonancementControle;
var linkSubMenuGestionExistantPrepaiementPeage;
var linkMenuGestionComplement, linkSubMenuAffecterComplementFormeJuridique, linkSubMenuAffecterComplementTypeBien;

var linkSubMenuEditionRole,
        linkSubMenuRegisterRole,
        linkSubMenuOrdonnancementExtraitRole,
        linkSubMenuRegisterExtraitRole;

var linkSubMenuRegistreSousProvisionMinier;
var linkSubMenuRegistreCommandeVoucher;
var linkSubMenuTaxationFraisCertification,
        linkMenuExoneration,
        linkSubMenuGestionBienEx,
        linkSubMenuRegistrePersonne;

var linkSubMenuAssocierAccountImpot;
var linkSubMenuCorrectionNotePerception;
var linkSubMenuRelance,linkSubMenuAmrRelance;
var linkSubMenuGestionNotePerceptionBanque,linkSubMenuGestionTauxVignette;

$(function () {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Chargement des données de l\'utilisateur...</h5>'});

    initFields();
//    setMenuExoneration('0');

    jQuery.ajax({
        url: "assets/lib/js/aes.js",
        dataType: "script",
        async: false,
        cache: true
    }).done(function () {
    });

    try {
        photoConducteurExist = false;

        userData = CryptoJS.AES.decrypt(sessionStorage.getItem('userData'), CYPHER_KEY).toString(CryptoJS.enc.Utf8);

        if (userData === null || userData === 'null') {

            window.location = 'login';
            return;
        }

        userData = JSON.parse(userData);

    } catch (err) {
        window.location = 'login';
    }

    linkadminName.html('<i class="fa fa-user"></i>&nbsp;&nbsp;<span>' + userData.nomComplet.toUpperCase() + '</span>');

    lblUserSite.text(userData.site.toUpperCase());
    lblUserSA.text(userData.divisionName.toUpperCase());
    //lblUserSA.text(userData.serviceAssiette);
    lblUserSiteAdresse.text(userData.adresseSite.toUpperCase());

    lblUserLogin.text(userData.login);
    lblUserName.text(userData.nomComplet.toUpperCase());
    lblUserFonction.text(userData.fonction.toUpperCase());
    lblUserMatricule.text(userData.matricule.toUpperCase());
    lblUserSiteModal.text(userData.site.toUpperCase());
    lblUserSAModal.text(userData.serviceAssiette.toUpperCase());
    lblUserSiteAdresseModal.text(userData.adresseSite.toUpperCase());

    linkAdminName = $('#linkAdminName');
    linkAdminName.click(function (e) {
        e.preventDefault();
        getSignatureAgent();
    });

    lblLastConnexion.text('Aucune connexion');

    applyAccess();

    $.unblockUI();

    linkSubMenuMep.on('click', function (e) {
        e.preventDefault();
        setRegisterType('MEP');
        window.location = 'registre-defaillant-paiement';
    });

    linkSubMenuMed.on('click', function (e) {
        e.preventDefault();
        setRegisterType('SERVICE');
        window.location = 'registre-defaillant-declaration';
    });

    /*linkSubMenuMep2.on('click', function (e) {
     e.preventDefault();
     setRegisterType('MEP');
     window.location = 'registre-mep';
     });
     
     linkSubMenuMed2.on('click', function (e) {
     e.preventDefault();
     setRegisterType('MED');
     window.location = 'registre-mise-en-demeure-declarer';
     });*/

    linkSubMenuClotureTaxation.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RNC');
        window.location = 'cloturer-taxation';
    });

    linkSubMenuRegistreTaxation.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RNCC');
        window.location = 'registre-notes-taxations-cloturees';

    });

    linkSubMenuTaxationFraisCertification.on('click', function (e) {
        e.preventDefault();
        window.location = 'taxation-frais-certification';
    });

    linkSubMenuRegistreTaxationRejetee.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RNCR');
        window.location = 'registre-notes-taxations-rejetees';
    });

    linkSubMenuRegistreTaxationRejeteeOrdonnancement.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RNCRO');
        window.location = 'registre-notes-taxations-rejetees';
    });

    linkSubMenuOrdonnancement.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RNCO');
        window.location = 'ordonnancer-taxation';
    });

    linkSubMenuGestionUtilisateur.on('click', function (e) {
        e.preventDefault();
        setRegisterType('EUT');
        window.location = 'gestion-utilisateur';
    });

    linkSubMenuGestionnaireAssujettis.on('click', function (e) {
        e.preventDefault();
        setRegisterType('GA');
        window.location = 'gestionnaire-assujettis';
    });


    linkSubMenuRetraitDeclaration.on('click', function (e) {
        e.preventDefault();
        setRegisterType('DL');
        window.location = 'retrait-declaration';
    });

    linkSubMenuGestionDepotDeclaration.on('click', function (e) {
        e.preventDefault();
        setRegisterType('DCL');
        window.location = 'depot-declaration';
    });

    linkSubMenuRegistreDepotDeclaration.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RDCL');
        window.location = 'registre-depot-declaration';
    });

    linkSubMenuRegistreRetraitDeclaration.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RRCL');
        window.location = 'registre-retrait-declaration';
    });
    
    linkSubMenuRegistreRetraitDeclarationVignette.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RRCLV');
        window.location = 'registre-retrait-declaration-vignette';
    });

    linkSubMenuGestionPrevisionCredit.on('click', function (e) {
        e.preventDefault();
        setRegisterType('GPC');
        window.location = 'gestion-provision-credit';
    });

    linkSubMenuGestionExemptes.on('click', function (e) {
        e.preventDefault();
        setRegisterType('GEX');
        window.location = 'registre-exemptes';
    });

    linkSubMenuRegistreUtilisateurs.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RUT');
        window.location = 'registre-utilisateurs';
    });

    linkSubMenuValiderAssujettis.on('click', function (e) {
        e.preventDefault();
        setRegisterType('VLA');
        window.location = 'registre-demande-inscription-teleprocedure';
    });

    linkSubMenuGestionUser.on('click', function (e) {
        e.preventDefault();
        setRegisterType('DAU');
        window.location = 'gestion-user';
    });
    linkSubMenuDocumentOfficial.on('click', function (e) {
        e.preventDefault();
        setRegisterType('DOFF');
        window.location = 'document-officiels';
    });

    linkSubMenuEditerReclamation.on('click', function (e) {
        e.preventDefault();
        setRegisterType('ERCL');
        window.location = 'editer-reclamation';
    });

    linkSubMenuRegistreReclamation.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RRCL');
        window.location = 'registre-reclamation';
    });

    linkSubMenuEditerRecours.on('click', function (e) {
        e.preventDefault();
        setRegisterType('ERCR');
        window.location = 'traiter-reclamation';
    });

    linkSubMenuRegistreRecours.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RDRR');
        window.location = 'registre-decision-juridictionnelles';
    });

    linkSubMenuDecisionJuridictionnelle.on('click', function (e) {
        e.preventDefault();
        setRegisterType('TRJ');
        window.location = 'traiter-recours';
    });

    linkSubMenuGestionFaitGenerateur.on('click', function (e) {
        e.preventDefault();
        setRegisterType('GFG');
        window.location = 'gestion-faits-generateurs';
    });

    linkSubMenuGestionArticleBudgetaire.on('click', function (e) {
        e.preventDefault();
        setRegisterType('GARB');
        window.location = 'gestion-article-budgetaire';
    });

    linkSubMenuAffecterArticleBudgetaireBanque.on('click', function (e) {
        e.preventDefault();
        setRegisterType('AFFAB');
        window.location = 'affecter-article-budgetaire-banque';
    });
    linkSubMenuGestionTarif.on('click', function (e) {
        e.preventDefault();
        setRegisterType('GTRF');
        window.location = 'edition-tarif';
    });
    linkSubMenuGestionPeriodicite.on('click', function (e) {
        e.preventDefault();
        setRegisterType('GPDT');
        window.location = 'gestion-periodicite';
    });

    linkSubMenuControleOrdonancement.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RCDR');
        window.location = 'registre-controle-ordonnancement';
    });

    linkSubMenuOrdonancementControle.on('click', function (e) {
        e.preventDefault();
        setRegisterType('ROCD');
        window.location = 'registre-ordonnancement-controle';
    });

    linkSubMenuAffecterComplementFormeJuridique.on('click', function (e) {
        e.preventDefault();
        setRegisterType('ACFJ');
        window.location = 'affecter-complement-forme-juridique';
    });

    linkSubMenuAffecterComplementTypeBien.on('click', function (e) {
        e.preventDefault();
        setRegisterType('ACTB');
        window.location = 'affecter-complement-bien';
    });

    linkSubMenuAffecterUtilisateurDivision.on('click', function (e) {
        e.preventDefault();
        setRegisterType('AUDB');
        window.location = 'affecter-utilisateur-division';
    });

    linkSubMenuEditerDivision.on('click', function (e) {
        e.preventDefault();
        setRegisterType('EDIV');
        window.location = 'editer-division';
    });

    linkSubMenuTauxChange.on('click', function (e) {
        e.preventDefault();
        setRegisterType('TCH');
        window.location = 'gestion-taux-change';
    });

    linkSubMenuRegistreCommandeVoucher.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RCMV');
        window.location = 'registre-commande-voucher';
    });

    linkSubMenuGestionBienEx.on('click', function (e) {
        e.preventDefault();
        setRegisterType('EXBI');
        setMenuExoneration('1');
        window.location = 'exoneration';
    });

    linkSubMenuRegistrePersonne.on('click', function (e) {
        e.preventDefault();
        window.location = 'repertoire-personnes-exonerees';
    });

    linkSubMenuGestionBien.on('click', function (e) {
        e.preventDefault();
        setMenuExoneration('0');
        window.location = 'gestion-bien';
    });
    
    linkSubMenuGestionBienAutomobile.on('click', function (e) {
        e.preventDefault();
        setMenuExoneration('0');
        window.location = 'gestion-bien-automobile';
    });
    
    linkSubMenuCorrectionBienAutomobileVignette.on('click', function (e) {
        e.preventDefault();
        setMenuExoneration('0');
        window.location = 'correction-bien-automobile-vignette';
    });


    btnUpdatePassword.click(function (e) {

        e.preventDefault();
        updateUserPassword();

    });

});

function initFields() {

    //UserInfo
    linkadminName = $('#linkAdminName');
    lblUserSite = $('#lblUserSite');
    lblUserSA = $('#lblUserSA');

    //UserInfoModal
    lblUserSiteAdresse = $('#lblUserSiteAdresse');
    lblUserLogin = $('#lblUserLogin');
    lblUserName = $('#lblUserName');
    lblUserFonction = $('#lblUserFonction');
    lblUserSiteModal = $('#lblUserSiteModal');
    lblUserSAModal = $('#lblUserSAModal');
    lblUserSiteAdresseModal = $('#lblUserSiteAdresseModal');
    lblLastConnexion = $('#lblLastConnexion');
    lblUserMatricule = $('#lblUserMatricule');
    imgSignatureAgent = $('#imgSignatureAgent');
    divApercuSignature = $('#divApercuSignature');

    //Dashboard
    linkMenuDahsboard = $('#linkMenuDahsboard');

    //Menu Repertoire
    linkMenuRepertoire = $('#linkMenuRepertoire');
    linkSubMenuIdentification = $('#linkSubMenuIdentification');
    linkSubMenuRegistreAssujetis = $('#linkSubMenuRegistreAssujetis');
    linkSubMenuGestionBien = $('#linkSubMenuGestionBien');
    linkSubMenuGestionBienAutomobile = $('#linkSubMenuGestionBienAutomobile');
    linkSubMenuCorrectionBienAutomobileVignette = $('#linkSubMenuCorrectionBienAutomobileVignette');
    linkSubMenuAssujettissement = $('#linkSubMenuAssujettissement');
    linkSubMenuValiderAssujettis = $('#linkSubMenuValiderAssujettis');

    //Menu Taxation
    linkMenuTaxation = $('#linkMenuTaxation');
    linkSubMenuTaxationOrdinaire = $('#linkSubMenuTaxationOrdinaire');
    linkSubMenuClotureTaxation = $('#linkSubMenuClotureTaxation');
    linkSubMenuRegistreTaxation = $('#linkSubMenuRegistreTaxation');
    linkSubMenuRegistreTaxationRejetee = $('#linkSubMenuRegistreTaxationRejetee');
    linkSubMenuTaxationCarteConducteur = $('#linkSubMenuTaxationCarteConducteur');
    linkSubMenuTaxationFraisCertification = $('#linkSubMenuTaxationFraisCertification');


    //Menu Ordonnancement
    linkMenuOrdonnancement = $('#linkMenuOrdonnancement');
    linkSubMenuOrdonnancement = $('#linkSubMenuOrdonnancement');
    linkSubMenuRegistreNotePerception = $('#linkSubMenuRegistreNotePerception');
    linkSubMenuRegistreTaxationRejeteeOrdonnancement = $('#linkSubMenuRegistreTaxationRejeteeOrdonnancement');
    linkSubMenuRegistreSousProvisionMinier = $('#linkSubMenuRegistreSousProvisionMinier');
    linkSubMenuCorrectionNotePerception = $('#linkSubMenuCorrectionNotePerception');

    //Menu Péage

    linkSubMenuGestionPrevisionCredit = $('#linkSubMenuGestionPrevisionCredit');
    linkSubMenuGestionExemptes = $('#linkSubMenuGestionExemptes');
    linkMenuPeage = $('#linkMenuPeage');
    linkSubMenuAssujettissementAutomobile = $('#linkSubMenuAssujettissementAutomobile');
    linkSubMenuRegistreSousProvision = $('#linkSubMenuRegistreSousProvision');
    linkSubMenuRegistreCredit = $('#linkSubMenuRegistreCredit');
    linkSubMenuRegistreTicket = $('#linkSubMenuRegistreTicket');
    linkSubMenuGestionRemisePrepaiementPeage = $('#linkSubMenuGestionRemisePrepaiementPeage');
    linkSubMenuGestionExistantPrepaiementPeage = $('#linkSubMenuGestionExistantPrepaiementPeage');
    linkSubMenuRegistreCommandeVoucher = $('#linkSubMenuRegistreCommandeVoucher');
    linkSubMenuRegistreCommandeCarte = $('#linkSubMenuRegistreCommandeCarte');

    //Menu Paiement
    linkMenuPaiement = $('#linkMenuPaiement');
    linkSubMenuEditerPaiement = $('#linkSubMenuEditerPaiement');
    linkSubMenuRegistrePaiement = $('#linkSubMenuRegistrePaiement');

    //Menu Apurement
    linkMenuApurement = $('#linkMenuApurement');
    linkSubMenuApurementAdmininistratif = $('#linkSubMenuApurementAdmininistratif');
    linkSubMenuApurementCompatable = $('#linkSubMenuApurementCompatable');
    linkSubMenuRegistreApurement = $('#linkSubMenuRegistreApurement');
    linkSubMenuRegistreApurementRejete = $('#linkSubMenuRegistreApurementRejete');

    //Menu Acquit Libératoire
    linkMenuAcquitLiberatoire = $('#linkMenuAcquitLiberatoire');
    linkSubMenuEditerAcquitLiberatoire = $('#linkSubMenuEditerAcquitLiberatoire');
    linkSubMenuRegistreAcquitLiberatoire = $('#linkSubMenuRegistreAcquitLiberatoire');

    //Update Password

    modalUpdateUserPassword = $('#modalUpdateUserPassword');
    inputCurrentPassword = $('#inputCurrentPassword');
    inputNewPassword = $('#inputNewPassword');
    inputConfirmNewPassword = $('#inputConfirmNewPassword');
    btnUpdatePassword = $('#btnUpdatePassword');

    //Menu Contentieux

    linkMenuContentieux = $('#linkMenuContentieux');
    linkSubMenuEditerReclamation = $('#linkSubMenuEditerReclamation');
    linkSubMenuRegistreReclamation = $('#linkSubMenuRegistreReclamation');
    linkSubMenuDecisionJuridictionnelle = $('#linkSubMenuDecisionJuridictionnelle');
    linkSubMenuEditerRecours = $('#linkSubMenuEditerRecours');
    linkSubMenuRegistreRecours = $('#linkSubMenuRegistreRecours');
    linkSubMenuRelance = $('#linkSubMenuRelance');
    linkSubMenuAmrRelance = $('#linkSubMenuAmrRelance');

    //Menu Gestion utilisateur

    linkMenuGestionUtilisateur = $('#linkMenuGestionUtilisateur');
    linkSubMenuGestionUtilisateur = $('#linkSubMenuGestionUtilisateur');
    linkSubMenuGestionFonction = $('#linkSubMenuGestionFonction');
    linkSubMenuGestionGroupe = $('#linkSubMenuGestionGroupe');
    linkSubMenuGestionDroit = $('#linkSubMenuGestionDroit');
    linkSubMenuGestionDroitSuppUser = $('#linkSubMenuGestionDroitSuppUser');
    linkSubMenuGestionnaireAssujettis = $('#linkSubMenuGestionnaireAssujettis');
    linkSubMenuRegistreUtilisateurs = $('#linkSubMenuRegistreUtilisateurs');
    linkSubMenuGestionUser = $('#linkSubMenuGestionUser');
    linkSubMenuAffecterUtilisateurDivision = $('#linkSubMenuAffecterUtilisateurDivision');

    // Menu Gestion de Role

    linkSubMenuEditionRole = $('#linkSubMenuEditionRole');
    linkSubMenuRegisterRole = $('#linkSubMenuRegisterRole');
    linkSubMenuOrdonnancementExtraitRole = $('#linkSubMenuOrdonnancementExtraitRole');
    linkSubMenuRegisterExtraitRole = $('#linkSubMenuRegisterExtraitRole');

    // Menu Gestion des actes générateurs

    linkMenuGestionArticleBudgetaire = $('#linkMenuGestionArticleBudgetaire');
    linkSubMenuGestionExerciceFiscal = $('#linkSubMenuGestionExerciceFiscal');

    linkSubMenuGestionServiceAssiette = $('#linkSubMenuGestionServiceAssiette');
    linkSubMenuGestionFaitGenerateur = $('#linkSubMenuGestionFaitGenerateur');
    linkSubMenuDocumentOfficial = $('#linkSubMenuDocumentOfficial');

    linkSubMenuGestionServiceAssiette = $('#linkSubMenuGestionServiceAssiette');
    linkSubMenuDocumentOfficial = $('#linkSubMenuDocumentOfficial');
    linkSubMenuGestionCleRepartition = $('#linkSubMenuGestionCleRepartition');
    linkSubMenuGestionArticleBudgetaire = $('#linkSubMenuGestionArticleBudgetaire');
    linkSubMenuRegistreArticleBudgetaire = $('#linkSubMenuRegistreArticleBudgetaire');
    linkSubMenuRegistreProrogationDateLegale = $('#linkSubMenuRegistreProrogationDateLegale');
    linkSubMenuGestionFaitGenerateur = $('#linkSubMenuGestionFaitGenerateur');
    linkSubMenuAffecterArticleBudgetaireBanque = $('#linkSubMenuAffecterArticleBudgetaireBanque');
    linkSubMenuGestionTarif = $('#linkSubMenuGestionTarif');
    linkSubMenuGestionPeriodicite = $('#linkSubMenuGestionPeriodicite');
    linkSubMenuRegistreTarifSitePeage = $('#linkSubMenuRegistreTarifSitePeage');
    linkSubMenuGestionNotePerceptionBanque = $('#linkSubMenuGestionNotePerceptionBanque');
    linkSubMenuGestionTauxVignette = $('#linkSubMenuGestionTauxVignette');

    //Menu Gestion Déclaration

    linkMenuDeclaration = $('#linkMenuDeclaration');
    linkSubMenuRetraitDeclaration = $('#linkSubMenuRetraitDeclaration');
    linkSubMenuGestionDepotDeclaration = $('#linkSubMenuGestionDepotDeclaration');
    linkSubMenuRegistreDepotDeclaration = $('#linkSubMenuRegistreDepotDeclaration');
    linkSubMenuRegistreRetraitDeclaration = $('#linkSubMenuRegistreRetraitDeclaration');
    linkSubMenuRegistreRetraitDeclarationVignette = $('#linkSubMenuRegistreRetraitDeclarationVignette');
    linkSubMenuExonerationVignette = $('#linkSubMenuExonerationVignette');
    linkSubMenuCorrectionNoteTaxation = $('#linkSubMenuCorrectionNoteTaxation');

    // Menu Recouvrement

    linkMenuRecouvrement = $('#linkMenuRecouvrement');
    linkSubMenuMed = $('#linkSubMenuMed');
    linkSubMenuMep = $('#linkSubMenuMep');
    linkSubMenuNoteTaxation = $('#linkSubMenuNoteTaxation');

    //Menu Péage

    linkSubMenuGestionPrevisionCredit = $('#linkSubMenuGestionPrevisionCredit');
    linkSubMenuGestionExemptes = $('#linkSubMenuGestionExemptes');
    linkSubMenuRegistreTicketAnnules = $('#linkSubMenuRegistreTicketAnnules');
    linkSubMenuRegistreCommandeVoucher = $('#linkSubMenuRegistreCommandeVoucher');

    // Menu Poursuites

    linkMenuPoursuite = $('#linkMenuPoursuite');
    linkSubMenuMiseEnDemeure = $('#linkSubMenuMiseEnDemeure');
    linkSubMenuInvitationApayer = $('#linkSubMenuInvitationApayer');
    linkSubMenuInvitationServiceNonPaiement = $('#linkSubMenuInvitationServiceNonPaiement');
    linkSubMenuAmr = $('#linkSubMenuAmr');
    linkSubMenuContrainte = $('#linkSubMenuContrainte');
    linkSubMenuCommandement = $('#linkSubMenuCommandement');
    linkSubMenuAtd = $('#linkSubMenuAtd');
    linkSubMenuFichePriseCharge = $('#linkSubMenuFichePriseCharge');
    linkSubMenuBonApayer = $('#linkSubMenuBonApayer');

    //Menu Carte

    linkMenuGestionCarte = $('#linkMenuGestionCarte');
    linkSubMenuEditerCarte = $('#linkSubMenuEditerCarte');
    linkSubMenuRgistreCartes = $('#linkSubMenuRgistreCartes');

    //Menu Fractionnement

    linkMenuFractionnement = $('#linkMenuFractionnement');
    linkSubMenuEditerDemandeFractionnement = $('#linkSubMenuEditerDemandeFractionnement');
    linkSubMenuRegistreDemandeFractionnement = $('#linkSubMenuRegistreDemandeFractionnement');
    linkSubMenuOrdonnancementFractionnement = $('#linkSubMenuOrdonnancementFractionnement');

    //Menu Assignation budgetaire
    linkMenuAssignation = $('#linkMenuAssignation');
    linkSubMenuEditionAssignationBudgetaire = $('#linkSubMenuEditionAssignationBudgetaire');
    linkSubMenuRegistre = $('#linkSubMenuRegistre');

    //Menu Gestion banque

    linkMenuGestionBanque = $('#linkMenuGestionBanque');
    linkSubMenuEditerBanque = $('#linkSubMenuEditerBanque');
    linkSubMenuCompteBancaire = $('#linkSubMenuCompteBancaire');
    linkSubMenuAffecterComptePaiemen = $('#linkSubMenuAffecterComptePaiemen');
    linkSubMenuStatistique = $('#linkSubMenuStatistique');
    linkSubMenuTauxChange = $('#linkSubMenuTauxChange');
    linkSubMenuAssocierAccountImpot = $('#linkSubMenuAssocierAccountImpot');

    // Menu Site banque

    linkMenuSiteBanque = $('#linkMenuSiteBanque');
    linkSubMenuEditionSite = $('#linkSubMenuEditionSite');
    linkSubMenuAffectationSiteBanue = $('#linkSubMenuAffectationSiteBanue');
    linkSubMenuGestionBanqueSite = $('#linkSubMenuGestionBanqueSite');
    linkSubMenuEditerDivision = $('#linkSubMenuEditerDivision');


    //Menu Gestion des pénalités

    linkMenuGestionPenalite = $('#linkMenuGestionPenalite');
    linkSubMenuTypePenalite = $('#linkSubMenuTypePenalite');
    linkSubMenuEditionPenalite = $('#linkSubMenuEditionPenalite');
    linkSubMenuParamTauxPenalite = $('#linkSubMenuParamTauxPenalite');

    //Menu Mise à jour Echeance Paiement

    linkMenuMajEcheance = $('#linkMenuMajEcheance');
    linkSubMenuMajEcheance = $('#linkSubMenuMajEcheance');

    //Menu Contôle

    linkMenuControle = $('#linkMenuControle');
    linkSubMenuControleOrdonancement = $('#linkSubMenuControleOrdonancement');
    linkSubMenuOrdonancementControle = $('#linkSubMenuOrdonancementControle');

    //Menu Complément

    linkMenuGestionComplement = $('#linkMenuGestionComplement');
    linkSubMenuAffecterComplementTypeBien = $('#linkSubMenuAffecterComplementTypeBien');
    linkSubMenuAffecterComplementFormeJuridique = $('#linkSubMenuAffecterComplementFormeJuridique');
    linkMenuExoneration = $('#linkMenuExoneration'),
            linkSubMenuGestionBienEx = $('#linkSubMenuGestionBienEx'),
            linkSubMenuRegistrePersonne = $('#linkSubMenuRegistrePersonne'),
            linkSubMenuRegistreCommandeCarte = $('#linkSubMenuRegistreCommandeCarte');

}

function applyAccess() {

    if (controlAccess('MOD_ID')) {
        applySubmenuRepertoire();
    }

    if (controlAccess('MOD_TAXATION')) {
        applySubmenuTaxation();
    }

    if (controlAccess('MOD_ORDONNANCEMENT')) {
        applySubmenuOrdonnancement();
    }

    if (controlAccess('MOD_PAIEMENT')) {
        applySubmenuPaiement();
    }

    if (controlAccess('MOD_APUREMENT')) {
        applySubmenuApurement();
    }

    if (controlAccess('MOD_ACQUIT_LIBERATOIRE')) {
        applySubmenuAcquitLiberatoire();
    }

    if (controlAccess('MOD_CONTENTIEUX')) {
        applySubmenuContentieux();
    }

    if (controlAccess('MOD_GESTION_UTILISATEUR')) {
        applySubmenuGestionUtilisateur();
    }

    if (controlAccess('MOD_GESTION_ARTICLE_BUDGETAIRE')) {
        applySubmenuGestionArticleBudgetaire();
    }

    if (controlAccess('MOD_DECLARATION')) {
        applySubmenuDeclaration();
    }

    if (controlAccess('MOD_RECOUVREMENT')) {
        applySubmenuRecouvrement();
    }

    if (controlAccess('MOD_POURSUITE')) {
        applySubmenuPoursuites();
    }

    if (controlAccess('MOD_CARTE')) {
        applySubmenuGestionCarte();
    }

    if (controlAccess('MOD_FRACTIONNEMENT')) {
        applySubmenuFractionnement();
    }

    if (controlAccess('MOD_GESTION_BANQUE')) {
        applySubmenuGestionBanque();
    }

    if (controlAccess('MOD_PEAGE')) {
        applySubmenuGestionPeage();
    }

    if (controlAccess('MOD_ASSIGNATION')) {
        applySubmenuAssignation();
    }

    if (controlAccess('MOD_SITE')) {
        applySubmenuSiteBanque();
    }

    if (controlAccess('MOD_PENALITE')) {
        applySubmenuGestionPenalite();
    }

    if (controlAccess('WVD00000000000000022020')) {
        applySubmenuComplement();
    }
//    if (controlAccess('WVD00000000000000022020')) {
//        applySubmenuGestionControle();
//    }

    /*if (controlAccess('MOD_PENALITE')) {
     applySubmenuMajEcheancePaiement();
     }*/

    applyMenuExoneration();

}

function applySubmenuRepertoire() {

    var showMenu = 0;

    if (controlAccess('SET_VALIDATE_ASSUJETTIS')) {
        linkSubMenuValiderAssujettis.show();
        showMenu++;
    }

    if (controlAccess('EDIT_PERSONNE')) {
        linkSubMenuIdentification.show();
        showMenu++;
    }

    if (controlAccess('REGISTRE_PERSONNE')) {
        linkSubMenuRegistreAssujetis.show();
        showMenu++;
    }

    if (controlAccess('EDIT_BIEN')) {
        linkSubMenuGestionBien.show();
        showMenu++;
    }
    
    if (controlAccess('GEST_BIEN_AUTOMOBILE')) {
        linkSubMenuGestionBienAutomobile.show();
        showMenu++;
    }
    
    if (controlAccess('CORRECTION_BIEN_AUTOMOBILE_VIGNETTE')) {
        linkSubMenuCorrectionBienAutomobileVignette.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuRepertoire.show();
        showMenu++;
    }

}

function applyMenuExoneration() {

    var showMenu = 0;

    if (controlAccess('VIEW_EXONERATION')) {
        linkSubMenuGestionBienEx.show();
        linkSubMenuRegistrePersonne.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuExoneration.show();
        showMenu++;
    }

}

function applySubmenuTaxation() {

    var showMenu = 0;

    if (controlAccess('SET_TAXATION_NON_FISCALE')) {
        linkSubMenuTaxationOrdinaire.show();
        showMenu++;
    }

    if (controlAccess('CLOTURE_TAXATION')) {
        linkSubMenuClotureTaxation.show();
        showMenu++;
    }

    if (controlAccess('LISTE_NOTE_CALCUL_CLOTURE')) {
        linkSubMenuRegistreTaxation.show();
        showMenu++;
    }

    if (controlAccess('VIEW_NC_REJETE_CLOTURE')) {
        linkSubMenuRegistreTaxationRejetee.show();
        showMenu++;
    }

    if (controlAccess('SET_TAXATION_FRAIS_CERTIFICATION')) {
        linkSubMenuTaxationFraisCertification.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuTaxation.show();
    }
}

function applySubmenuOrdonnancement() {

    var showMenu = 0;

    if (controlAccess('EDITER_NOTE_PERCEPTION')) {
        linkSubMenuOrdonnancement.show();
        showMenu++;
    }

    if (controlAccess('LISTER_NOTE_PERCEP')) {
        linkSubMenuRegistreNotePerception.show();
        showMenu++;
    }

    if (controlAccess('VIEW_NC_REJETE')) {
        linkSubMenuRegistreTaxationRejeteeOrdonnancement.show();
        showMenu++;
    }

    if (controlAccess('REGISTER_SP_MINIER')) {
        linkSubMenuRegistreSousProvisionMinier.show();
        showMenu++;
    }
    
    if (controlAccess('MODIFY_NOTE_PERCEPTION')) {
        linkSubMenuCorrectionNotePerception.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuOrdonnancement.show();
    }
}

function applySubmenuPaiement() {

    var showMenu = 0;


    if (controlAccess('EDITER_PAYEMENT')) {
        linkSubMenuEditerPaiement.show();
        showMenu++;
    }

    if (controlAccess('REGISTRE_PAYEMENT')) {
        linkSubMenuRegistrePaiement.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuPaiement.show();
    }
}

function applySubmenuApurement() {

    var showMenu = 0;

    if (controlAccess('EDITER_APPUREMENT')) {
        linkSubMenuApurementAdmininistratif.show();
        showMenu++;
    }

    if (controlAccess('SET_APUREMENT_COMPTABLE')) {
        linkSubMenuApurementCompatable.show();
        showMenu++;
    }

    if (controlAccess('REGISTRE_APUREMENT')) {
        linkSubMenuRegistreApurement.show();
        showMenu++;
    }

    if (controlAccess('VIEW_APUREMENT_REJETE')) {
        linkSubMenuRegistreApurementRejete.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuApurement.show();
    }
}

function applySubmenuAcquitLiberatoire() {

    var showMenu = 0;

    if (controlAccess('EDITER_ACQUIT')) {
        linkSubMenuEditerAcquitLiberatoire.show();
        showMenu++;
    }

    if (controlAccess('REEGISTRE_ACQUIT')) {
        linkSubMenuRegistreAcquitLiberatoire.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuAcquitLiberatoire.show();
    }
}

function applySubmenuContentieux() {

    var showMenu = 0;

    if (controlAccess('EDITER_RECLAMATION')) {
        linkSubMenuEditerReclamation.show();
        showMenu++;
    }

    if (controlAccess('REEGISTRE_RECLAMATION')) {
        linkSubMenuRegistreReclamation.show();
        showMenu++;
    }

    if (controlAccess('REGISTRE_DECISION')) {
        linkSubMenuDecisionJuridictionnelle.show();
        showMenu++;
    }

    if (controlAccess('EDITER_RECOURS')) {
        linkSubMenuEditerRecours.show();
        showMenu++;
    }
    if (controlAccess('REGISTRE_RECOURS')) {
        linkSubMenuRegistreRecours.show();
        showMenu++;
    }


    if (showMenu > 0) {
        linkMenuContentieux.show();
    }
}

function applySubmenuGestionCarte() {

    var showMenu = 0;

    if (controlAccess('EDIT_CARTE_MOTO')) {
        linkSubMenuEditerCarte.show();
        showMenu++;
    }

    if (controlAccess('VIEW_CARTE')) {
        linkSubMenuRgistreCartes.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuGestionCarte.show();
        showMenu++;
    }

}

function applySubmenuGestionPeage() {

    var showMenu = 0;

    if (controlAccess('ASSUJETTISSEMENT_AUTOMOBILE')) {
        linkSubMenuAssujettissementAutomobile.show();
        showMenu++;
    }

    if (controlAccess('GEST_PROVISION_CREDIT')) {
        linkSubMenuGestionPrevisionCredit.show();
        showMenu++;
    }

    if (controlAccess('REGISTRE_SOUS_PROVISION')) {
        linkSubMenuRegistreSousProvision.show();
        showMenu++;
    }

    if (controlAccess('REGISTRE_CREDIT')) {
        linkSubMenuRegistreCredit.show();
        showMenu++;
    }

    if (controlAccess('REGISTRE_EXEMPTE')) {
        linkSubMenuGestionExemptes.show();
        showMenu++;
    }

    if (controlAccess('REGISTRE_TICKET')) {
        linkSubMenuRegistreTicket.show();
        linkSubMenuRegistreTicketAnnules.show();
        showMenu++;
    }

    if (controlAccess('REGISTRE_COMMANDE_VOUCHER')) {
        linkSubMenuRegistreCommandeVoucher.show();
        showMenu++;
    }

    linkSubMenuRegistreCommandeCarte.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RCMC');
        window.location = 'registre-commande-carte';
    });

    if (controlAccess('GESTION_REMISE_PREPAIEMENT_PEAGE')) {
        linkSubMenuGestionRemisePrepaiementPeage.show();
        showMenu++;
    }

    if (controlAccess('PRISE_COMPTE_EXISTANT_BON_PREPAIEMNT')) {
        linkSubMenuGestionExistantPrepaiementPeage.show();
        showMenu++;
    }

    if (controlAccess('REGISTRE_COMMANDE_CARTE')) {
        linkSubMenuRegistreCommandeCarte.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuPeage.show();
        showMenu++;
    }

}

function applySubmenuGestionUtilisateur() {

    var showMenu = 0;

    if (controlAccess('EDITER_FONCTION')) {
        linkSubMenuGestionFonction.show();
        showMenu++;
    }
    if (controlAccess('EDITER_UTILISATEUR')) {
        linkSubMenuGestionUtilisateur.show();
        showMenu++;
    }
    if (controlAccess('EDITER_GROUPE_UTILISATEUR')) {
        linkSubMenuGestionGroupe.show();
        showMenu++;
    }
    if (controlAccess('EDITER_DROIT')) {
        linkSubMenuGestionDroit.show();
        showMenu++;
    }
    if (controlAccess('GESTIONNAIRE_ASSUJETTIS')) {
        linkSubMenuGestionnaireAssujettis.show();
        showMenu++;
    }

    if (controlAccess('REGISTRE_USER')) {
        linkSubMenuRegistreUtilisateurs.show();
        showMenu++;
    }

    if (controlAccess('REG_ADD_DROIT_SUPP')) {

        linkSubMenuGestionDroitSuppUser.show();
        showMenu++;
    }


    if (controlAccess('GESTION_DROIT_USER')) {
        linkSubMenuGestionUser.show();
        showMenu++;
    }
    if (controlAccess('WVD00000000000000052020')) {
        linkSubMenuAffecterUtilisateurDivision.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuGestionUtilisateur.show();
        showMenu++;
    }

}

function applySubmenuRecouvrement() {

    var showMenu = 0;

    if (controlAccess('EDITION_MED_DECLAR')) {
        linkSubMenuMed.show();
        showMenu++;
    }

    if (controlAccess('EDITION_MED_PAIEM')) {
        linkSubMenuMep.show();
        linkSubMenuNoteTaxation.show();
        showMenu++;
    }

    if (controlAccess('SET_DEPOT_DECLARATION')) {
        linkSubMenuGestionDepotDeclaration.show();
        showMenu++;
    }

    if (controlAccess('VIEW_LIST_DEPOT_DECLARATION')) {
        linkSubMenuRegistreDepotDeclaration.show();
        showMenu++;
    }


    if (showMenu > 0) {
        linkMenuRecouvrement.show();
        showMenu++;
    }

}

function applySubmenuPoursuites() {

    var showMenu = 0;

    if (controlAccess('EDITION_MED_DECLAR')) {
        linkSubMenuMiseEnDemeure.show();
        showMenu++;
    }
    
    if (controlAccess('REGISTRE_INVITATION_A_PAYER')) {
        linkSubMenuInvitationApayer.show();
        showMenu++;
    }

    if (controlAccess('EDITION_INVITATION_SERVICE_NP')) {
        linkSubMenuInvitationServiceNonPaiement.show();
        showMenu++;
    }

    if (controlAccess('EDITION_MED_PAIEM')) {
        linkSubMenuMiseEnDemeure.show();
        //linkSubMenuMep2.show();
        showMenu++;
    }

    if (controlAccess('LISTE_AMR') || controlAccess('LISTE_AMR2')) {
        linkSubMenuAmr.show();
        showMenu++;
    }

    if (controlAccess('VIEW_REGISTRE_CONTRAINTE_CMD')) {
        linkSubMenuContrainte.show();
        showMenu++;
    }

    if (controlAccess('VIEW_REGISTRE_CONTRAINTE_CMD')) {
        linkSubMenuCommandement.show();
        showMenu++;
    }

    if (controlAccess('VIEW_ATD') || controlAccess('PRINT _ATD')) {
        linkSubMenuAtd.show();
        showMenu++;
    }

    if (controlAccess('VIEW_PRISE_CHARGE')) {
        linkSubMenuFichePriseCharge.show();
        showMenu++;
    }

    if (controlAccess('VIEW_BON_PAYER')) {
        linkSubMenuBonApayer.show();
        showMenu++;
    }
    
    if (controlAccess('VIEW_REGISTER_RELANCE')) {
        linkSubMenuRelance.show();
        showMenu++;
    }
    
    if (controlAccess('VIEW_REGISTER_AMR_RELANCE')) {
        linkSubMenuAmrRelance.show();
        showMenu++;
    }

    if (controlAccess('GEST_ROLE_AND_EXTRAIT_ROLE')) {
        linkSubMenuEditionRole.show();
        linkSubMenuRegisterRole.show();
        linkSubMenuOrdonnancementExtraitRole.show();
        linkSubMenuRegisterExtraitRole.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuPoursuite.show();
        showMenu++;
    }

}

function applySubmenuDeclaration() {

    var showMenu = 0;

    if (controlAccess('EDIT_DECLARATION')) {
        linkSubMenuRetraitDeclaration.show();
        showMenu++;
    }

    if (controlAccess('VIEW_LISTE_DECLARATION')) {
        linkSubMenuRegistreRetraitDeclaration.show();
        showMenu++;
    }
    
    if (controlAccess('VIEW_LISTE_DECLARATION_VIGNETTE')) {
        linkSubMenuRegistreRetraitDeclarationVignette.show();
        showMenu++;
    }
    
    if (controlAccess('GEST_EXONERATION_VIGNETTE')) {
        linkSubMenuExonerationVignette.show();
        showMenu++;
    }

    if (controlAccess('CORRECTION_NOTE_TAXATION')) {
        linkSubMenuCorrectionNoteTaxation.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuDeclaration.show();
        showMenu++;
    }

}

function applySubmenuGestionArticleBudgetaire() {

    var showMenu = 0;

    if (controlAccess('VIEW_EXERCICE_FISCAL')) {

        linkSubMenuGestionExerciceFiscal.show();
        linkSubMenuAffecterArticleBudgetaireBanque.show();
        linkSubMenuGestionServiceAssiette.show();
        linkSubMenuGestionFaitGenerateur.show();
        linkSubMenuGestionArticleBudgetaire.show();
        linkSubMenuRegistreArticleBudgetaire.show();
        linkSubMenuRegistreProrogationDateLegale.show();
        linkSubMenuAffecterArticleBudgetaireBanque.show();
        linkSubMenuGestionPeriodicite.show();

        linkSubMenuGestionServiceAssiette.show();
        linkSubMenuGestionFaitGenerateur.show();
        linkSubMenuDocumentOfficial.show();

        showMenu++;
    }

    if (controlAccess('GEST_NOTE_PERCEPTION_BANQUE')) {
        linkSubMenuGestionNotePerceptionBanque.show();
        showMenu++;
    }
    
    if (controlAccess('CONFIG_TAUX_VIGNETTE')) {
        linkSubMenuGestionTauxVignette.show();
        showMenu++;
    }
    
    if (controlAccess('CONFIG_TARIF_PEAGE')) {
        linkSubMenuRegistreTarifSitePeage.show();
        showMenu++;
    }

    if (controlAccess('EDIT_TARIF')) {
        linkSubMenuGestionTarif.show();
        showMenu++;
    }



    if (showMenu > 0) {
        linkMenuGestionArticleBudgetaire.show();
        showMenu++;
    }

}

function applySubmenuGestionBanque() {

    var showMenu = 0;

    if (controlAccess('EDIT_BANQUE')) {
        linkSubMenuEditerBanque.show();
        showMenu++;
    }

    if (controlAccess('EDIT_COMPTE_BANCAIRE')) {
        linkSubMenuCompteBancaire.show();
        showMenu++;
    }

    if (controlAccess('AFFECT_COMPTE_PAYMENT')) {
        linkSubMenuAffecterComptePaiemen.show();
        showMenu++;
    }
    
    if (controlAccess('ASSOCIATE_ACCOUNT_AB')) {
        linkSubMenuAssocierAccountImpot.show();
        showMenu++;
    }


    if (showMenu > 0) {
        linkMenuGestionBanque.show();
        showMenu++;
    }

}

function applySubmenuFractionnement() {

    var showMenu = 0;

    if (controlAccess('EDIT_DEMAND_FRACTIONNEMENT')) {
        linkSubMenuEditerDemandeFractionnement.show();
        showMenu++;
    }

    if (controlAccess('REGISTER_DEMAND_FRACTIONNEMENT')) {
        linkSubMenuRegistreDemandeFractionnement.show();
        showMenu++;
    }

    if (controlAccess('ORDONNANCE_DEMAND_FRACTIONNEMENT')) {
        linkSubMenuOrdonnancementFractionnement.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuFractionnement.show();
        showMenu++;
    }

}

function applySubmenuAssignation() {

    var showMenu = 0;

    if (controlAccess('SET_ASSIGNATION_BUDGETAIRE')) {
        linkSubMenuEditionAssignationBudgetaire.show();
        showMenu++;
    }

    if (controlAccess('VIEW_ASSIGNATION_BUDGETAIRE')) {
        linkSubMenuRegistre.show();
        showMenu++;
    }



    if (showMenu > 0) {
        linkMenuAssignation.show();
        showMenu++;
    }
    if (showMenu > 0) {
        linkSubMenuStatistique.show();
        showMenu++;
    }

}

function applySubmenuGestionPenalite() {

    var showMenu = 0;
    showMenu++;

    linkSubMenuTypePenalite.show();
    linkSubMenuEditionPenalite.show();
    linkSubMenuParamTauxPenalite.show();


    if (showMenu > 0) {
        linkMenuGestionPenalite.show();
        showMenu++;
    }

}

function applySubmenuMajEcheancePaiement() {

    var showMenu = 0;
    showMenu++;

    if (showMenu > 0) {
        linkMenuMajEcheance.show();
        linkSubMenuMajEcheance.show();
        showMenu++;
    }

}

function applySubmenuSiteBanque() {

    var showMenu = 0;

    if (controlAccess('EDIT_SITE')) {
        linkSubMenuEditionSite.show();
        showMenu++;
    }

    if (controlAccess('AFFECT_SITE_BANK')) {
        linkSubMenuAffectationSiteBanue.show();
        showMenu++;
    }

    if (controlAccess('WVD00000000000000052020')) {
        linkSubMenuEditerDivision.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuSiteBanque.show();
        showMenu++;
    }

}

function applySubmenuComplement() {

    var showMenu = 0;

    if (controlAccess('WVD00000000000000032020')) {
        linkSubMenuAffecterComplementFormeJuridique.show();
        showMenu++;
    }

    if (controlAccess('WVD00000000000000042020')) {
        linkSubMenuAffecterComplementTypeBien.show();
        showMenu++;
    }

    if (showMenu > 0) {
        linkMenuGestionComplement.show();
        showMenu++;
    }

}

//function applySubmenuGestionControle() {
//
//    var showMenu = 0;
//    showMenu++;
//
//    linkSubMenuControleOrdonancement.show();
//    linkSubMenuOrdonancementControle.show();
//
//
//    if (showMenu > 0) {
//        linkMenuControle.show();
//        showMenu++;
//    }
//
//}

function removeActiveMenu() {

    linkMenuDahsboard.removeClass('active');
    linkMenuRepertoire.removeClass('active');
    linkMenuTaxation.removeClass('active');
    linkMenuOrdonnancement.removeClass('active');
    linkMenuPaiement.removeClass('active');
    linkMenuApurement.removeClass('active');
    linkMenuAcquitLiberatoire.removeClass('active');
    linkSubMenuEditerReclamation.removeClass('active');
    linkSubMenuRegistreReclamation.removeClass('active');
    linkSubMenuEditerRecours.removeClass('active');
    linkSubMenuDecisionJuridictionnelle.removeClass('active');
    linkMenuGestionUtilisateur.removeClass('active');
    linkMenuGestionArticleBudgetaire.removeClass('active');
    linkSubMenuRegistreDepotDeclaration.removeClass('active');
    linkSubMenuGestionnaireAssujettis.removeClass('active');
    linkMenuGestionCarte.removeClass('active');
    linkSubMenuRegistreUtilisateurs.removeClass('active');
    linkSubMenuGestionUser.removeClass('active');
    linkMenuFractionnement.removeClass('active');
    linkMenuAssignation.removeClass('active');
    linkSubMenuEditerDemandeFractionnement.removeClass('active');
    linkSubMenuRegistreDemandeFractionnement.removeClass('active');
    linkSubMenuOrdonnancementFractionnement.removeClass('active');
    linkSubMenuDocumentOfficial.removeClass('active');
    linkMenuGestionComplement.removeClass('active');
    linkSubMenuTauxChange.removeClass('active');
    linkMenuExoneration.removeClass('active');

}

function updateUserPassword() {

    if (inputCurrentPassword.val() === '' || inputNewPassword.val() === '' || inputConfirmNewPassword.val() === '') {
        alertify.alert('Veuillez remplir tous les champs.');
        return;
    }

    if (inputNewPassword.val() !== inputConfirmNewPassword.val()) {
        alertify.alert('Le mot de passe de confirmation doit être identique au nouveau mot de passe');
        return;
    }

    if (inputCurrentPassword.val() === inputNewPassword.val()) {
        alertify.alert('L\'actuel et le nouveau mot de passe sont identiques.');
        return;
    }

    if (inputNewPassword.val().length < 6) {
        alertify.alert('Le nouveau mot de passe doit avoir 6 caractères minimum');
        return;
    }

    alertify.confirm('Voulez-vous confirmer cette modification ?', function ()
    {
        $.ajax({
            type: 'POST',
            url: 'connexion_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'idUser': userData.idUser,
                'currentPassword': inputCurrentPassword.val(),
                'newPassword': inputNewPassword.val(),
                'operation': 'updatePassword'
            },
            beforeSend: function () {
                modalUpdateUserPassword.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Modification du mot de passe...</h5>'});
            },
            success: function (response)
            {

                setTimeout(function () {

                    modalUpdateUserPassword.unblock();

                    switch (response) {

                        case '1' :
                            alertify.alert('Modification effectuée avec succès.');
                            setTimeout(function () {
                                logout();
                            }, 1500);
                            break;
                        case '3' :
                            alertify.alert('Le mot de passe actuel est invalide. Veuillez ressayer.');
                            break;
                        case '0' :
                            alertify.alert('Echec de la modification du mot de passe. Veuillez ressayer.');
                            break;
                        case '-1':
                            showResponseError();
                            break;
                        default :
                            showResponseError();
                    }
                    ;
                }
                , 1000);

            },
            complete: function () {

            },
            error: function () {
                modalUpdateUserPassword.unblock();
                showResponseError();
            }

        });

    }, function () {
    });

}

function getSignatureAgent() {


    if (!controlAccess('SET_ADD_SIGNATURE')) {
        divApercuSignature.hide();
        return;
    }


    $.ajax({
        type: 'POST',
        url: 'connexion_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'idUser': userData.idUser,
            'operation': 'getSignatureAgent'
        },
        beforeSend: function () {
            imgSignatureAgent.attr('src', '');
            $('#spanLoading').html('<img width="50" src="assets/images/loading.gif" alt="" />');
        },
        success: function (response)
        {

            $('#spanLoading').html('');

            if (response == '0') {
                imgSignatureAgent.attr('style', 'display:none');
            } else {
                imgSignatureAgent.attr('src', response);
                imgSignatureAgent.attr('style', 'display:inline');
            }

        },
        complete: function () {

        },
        error: function () {
            showResponseError();
        }
    });
}

function saveSignatureAgent() {

    var base64 = imgSignatureAgent.attr('src');

    $.ajax({
        type: 'POST',
        url: 'connexion_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'idUser': userData.idUser,
            'signature': base64,
            'operation': 'saveSignatureAgent'
        },
        beforeSend: function () {

        },
        success: function (response)
        {
            if (response == '0') {
                alertify.alert('Echec de l\'enregistrement de la signature');
            }
        },
        complete: function () {

        },
        error: function () {
            showResponseError();
        }
    });
}
