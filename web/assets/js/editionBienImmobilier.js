/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var cmbTypeBien;

var
        divComplement,
        divPanelComplement;

var
        inputIntitule,
        inputDescription,
        inputDateAcquissition,
        inputAdresse;

var
        btnAjouterAdresse,
        btnEnregistrer, btnEnregistrer2, BtnEditerTypeBien;

var
        tempTypeBienList = [],
        tempComplementList = [],
        tarifList = [],
        tempUsageBienList = [];

var eaList = [];

var
        codeOfPersonne,
        nameOfPersonne,
        selectedCodeAP = '',
        idOfBien = '',
        codeOfTypeBien = '';

var labelAssujettiName;

var IdDivCmbCommuneBien, cmbCommuneBien, cmbUsageBien, cmbCategorieBien, cmbQuartierBien, IdDivCmbQuartierBien, IdDivCmbCategorieBien;

var BtnUsageBien, modalEditerUsageBien;
var codeRangCurrent;

var spnCommuneId,
        spnQuartierId,
        spnRangId,
        checkBoxAssociateBienWithIF;

var checkBox;
var rangExist;
var isModify;

var cmbTypeBienSelected;

$(function () {


    mainNavigationLabel.text('REPERTOIRE');
    secondNavigationLabel.text('Edition d\'un bien immobilier');

    inputIntitule = $('#inputIntitule');
    inputDescription = $('#inputDescription');
    inputDateAcquissition = $('#inputDateAcquissition');
    inputAdresse = $('#inputAdresse');

    labelAssujettiName = $('#labelAssujettiName');

    isModify = false;

    modalAdressesPersonne = $('#modalAdressesPersonne');
    modalEditerTypeBien = $('#modalEditerTypeBien');


    BtnEditerTypeBien = $('#BtnEditerTypeBien');
    IdDivCmbCommuneBien = $('#IdDivCmbCommuneBien');
    IdDivCmbQuartierBien = $('#IdDivCmbQuartierBien');
    IdDivCmbCategorieBien = $('#IdDivCmbCategorieBien');
    cmbCommuneBien = $('#cmbCommuneBien');
    cmbUsageBien = $('#cmbUsageBien');
    cmbCategorieBien = $('#cmbCategorieBien');
    BtnUsageBien = $('#BtnUsageBien');
    modalEditerUsageBien = $('#modalEditerUsageBien');
    cmbQuartierBien = $('#cmbQuartierBien');

    spnCommuneId = $('#spnCommuneId');
    spnQuartierId = $('#spnQuartierId');
    spnRangId = $('#spnRangId');
    checkBoxAssociateBienWithIF = $('#checkBoxAssociateBienWithIF');

    checkBox = document.getElementById("checkBoxAssociateBienWithIF");

    inputDescription.val('');
    inputAdresse.val('');

    IdDivCmbCommuneBien.attr('style', 'display:none');
    IdDivCmbQuartierBien.attr('style', 'display:none');
    IdDivCmbCategorieBien.attr('style', 'display:none');

    var urlId = getUrlParameter('id');
    var urlName = getUrlParameter('nom');

    if (jQuery.type(urlId) !== 'undefined' && jQuery.type(urlName) !== 'undefined') {

        codeOfPersonne = atob(urlId);
        nameOfPersonne = atob(urlName);

        var codeBien = getUrlParameter('codeBien');
        if (jQuery.type(codeBien) !== 'undefined') {
            isModify = true;
            codeBien = atob(codeBien);
            secondNavigationLabel.text('Répertoire des biens -> Modification d\'un bien');
            idOfBien = codeBien.split(';')[0];
            codeOfTypeBien = codeBien.split(';')[1];
        } else {
            isModify = false;
            idOfBien = '';
            codeOfTypeBien = '';
        }
    }

    btnAjouterAdresse = $('#btnAjouterAdresse');
    btnAjouterAdresse.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        codePersonne = codeOfPersonne;
        nomPersonne = nameOfPersonne;

        loadAdressesOfPersonne();

        modalAdressesPersonne.modal('show');

    });

    btnEnregistrer = $('#btnEnregistrer');
    btnEnregistrer2 = $('#btnEnregistrer2');

    btnEnregistrer.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();

    });

    btnEnregistrer2.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();
    });

    cmbTypeBien = $('#cmbTypeBien');
    cmbTypeBien.on('change', function (e) {

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbTypeBien.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner une nature de bien valide');
            return;

        } else {

            cmbTypeBienSelected = isModify == false ? cmbTypeBien.val() : codeOfTypeBien;

            var codeTypeBien = cmbTypeBien.val();
            loadTypeComplementBiens(codeTypeBien);

            switch (cmbTypeBienSelected) {

                case 'TB00000031' : //CITERNE/TANK
                case 'TB00000035' : //CONTAINEUR
                case 'TB00000033' : //DEPOT
                case 'TB00000034' : //EMETTEUR/RECEPTEUR
                case 'TB00000032' : //ENTREPOT
                case 'TB00000030' : //HANGAR
                case 'TB00000023' : //ANTENNE DE COMMUNICATION


                    IdDivCmbCommuneBien.attr('style', 'display:none');
                    IdDivCmbQuartierBien.attr('style', 'display:none');
                    IdDivCmbCategorieBien.attr('style', 'display:none');

                    rangExist = false;

                    break;

                case 'TB00000028' : //TERRAIN

                    IdDivCmbCommuneBien.attr('style', 'display:none');
                    IdDivCmbQuartierBien.attr('style', 'display:none');
                    IdDivCmbCategorieBien.attr('style', 'display:block');

                    rangExist = true;

                    loadRangs();

                    break;

                case 'TB00000026' : //AP
                case 'TB00000027' : //AT
                case 'TB00000025' : //VILLA
                case 'TB00000029' : //ETAGE (ET)

                    if (checkBox.checked) {

                        IdDivCmbCommuneBien.attr('style', 'display:block');
                        IdDivCmbQuartierBien.attr('style', 'display:block');
                        IdDivCmbCategorieBien.attr('style', 'display:block');

                        rangExist = true;

                        loadRangs();

                    } else {

                        IdDivCmbCommuneBien.attr('style', 'display:none');
                        IdDivCmbQuartierBien.attr('style', 'display:none');
                        IdDivCmbCategorieBien.attr('style', 'display:none');

                        rangExist = false;

                    }

                    break;

            }

        }
    });

    cmbCommuneBien.on('change', function (e) {

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbCommuneBien.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner une commune valide');
            return;

        } else {

            for (var i = 0; i < eaList.length; i++) {

                if (eaList[i].code == cmbCommuneBien.val()) {

                    var listQuartier = eaList[i].quartierList;

                    var dataQuartier = '<option value ="0">-- Sélectionner --</option>';

                    for (var j = 0; j < listQuartier.length; j++) {

                        if (listQuartier[j].communeCode == cmbCommuneBien.val()) {

                            dataQuartier += '<option value =' + listQuartier[j].quartierCode + '>' + listQuartier[j].quartierName + '</option>';
                        }
                    }
                    cmbQuartierBien.html(empty);
                    cmbQuartierBien.html(dataQuartier);

                }
            }

        }

        //var codeTypeBien = cmbTypeBien.val();
        //loadTypeComplementBiens(codeTypeBien);
    });

    cmbQuartierBien.on('change', function (e) {

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbQuartierBien.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un quartier valide');
            return;

        } else {
        }
    });

    BtnEditerTypeBien.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        displayNatureBienInfo();

        modalEditerTypeBien.modal('show');
    });

    BtnUsageBien.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        displayUsageBienInfo();

        modalEditerUsageBien.modal('show');
    });

    divComplement = $('#divComplement');
    divPanelComplement = $('#divPanelComplement');

    labelAssujettiName.text(nameOfPersonne.toUpperCase());
    ID_TYPE = 2;
    initData();

});


function initData() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'initData',
            'codeDivision': userData.divisionCode,
            'codeService': userData.serviceCode,
            'type': 2
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }
            if (response == '0') {
                $.unblockUI();
                alertify.alert('Aucun type bien trouvé.');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                tempTypeBienList = [];

                var usageBienList = result.usageBienList;
                var typeBienList = result.typeBienList;

                eaList = result.eaList;

                var categorieList = result.categorieList;

                var dataTypeBien = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < typeBienList.length; i++) {

                    var typeBien = new Object();
                    typeBien.codeTypeBien = typeBienList[i].codeTypeBien;
                    typeBien.libelleTypeBien = typeBienList[i].libelleTypeBien;
                    typeBien.estContractuel = typeBienList[i].estContractuel;
                    tempTypeBienList.push(typeBien);

                    dataTypeBien += '<option value =' + typeBienList[i].codeTypeBien + '>' + typeBienList[i].libelleTypeBien + '</option>';
                }

                cmbTypeBien.html(dataTypeBien);

                if (idOfBien != '') {
                    cmbTypeBien.val(codeOfTypeBien);
                    loadTypeComplementBiens(codeOfTypeBien);
                }

                tempUsageBienList = [];

                var dataUsageBien = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < usageBienList.length; i++) {

                    var usageBien = new Object();
                    usageBien.id = usageBienList[i].id;
                    usageBien.intitule = usageBienList[i].intitule;

                    tempUsageBienList.push(usageBien);

                    dataUsageBien += '<option value =' + usageBienList[i].id + '>' + usageBienList[i].intitule + '</option>';
                }

                cmbUsageBien.html(dataUsageBien);

                var dataCommuneBien = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < eaList.length; i++) {

                    dataCommuneBien += '<option value =' + eaList[i].code + '>' + eaList[i].communeComposite + '</option>';
                }

                cmbCommuneBien.html(dataCommuneBien);

                /*var dataCategorieBien = '<option value ="0">-- Sélectionner --</option>';
                 
                 for (var i = 0; i < categorieList.length; i++) {
                 
                 dataCategorieBien += '<option value =' + categorieList[i].code + '>' + categorieList[i].intitule + '</option>';
                 }
                 
                 cmbCategorieBien.html(dataCategorieBien);*/

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadTypeBien() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTypeBienByService',
            'codeService': userData.serviceCode,
            'type': 2
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }
            if (response == '0') {
                $.unblockUI();
                alertify.alert('Aucun type bien retrouvé.');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                tempTypeBienList = [];

                var dataTypeBien = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < result.length; i++) {

                    var typeBien = new Object();
                    typeBien.codeTypeBien = result[i].codeTypeBien;
                    typeBien.libelleTypeBien = result[i].libelleTypeBien;
                    typeBien.estContractuel = result[i].estContractuel;
                    tempTypeBienList.push(typeBien);

                    dataTypeBien += '<option value =' + result[i].codeTypeBien + '>' + result[i].libelleTypeBien + '</option>';
                }

                cmbTypeBien.html(dataTypeBien);

                if (idOfBien != '') {
                    cmbTypeBien.val(codeOfTypeBien);
                    loadTypeComplementBiens(codeOfTypeBien);
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadUsageBien() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadUsageBien'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }
            if (response == '0') {
                $.unblockUI();
                alertify.alert('Aucun usage bien trouvé.');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                tempUsageBienList = [];

                var dataUsageBien = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < result.length; i++) {

                    var usageBien = new Object();
                    usageBien.id = result[i].id;
                    usageBien.intitule = result[i].intitule;

                    tempUsageBienList.push(usageBien);

                    dataUsageBien += '<option value =' + result[i].id + '>' + result[i].intitule + '</option>';
                }

                cmbUsageBien.html(dataUsageBien);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

var numberBien;

function loadTypeComplementBiens(codeTypeBien) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTypeComplementBien',
            'codeTypeBien': codeTypeBien,
            'codePersonne': codeOfPersonne,
            'idBien': idOfBien
        },
        beforeSend: function () {
            divPanelComplement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                divPanelComplement.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                divPanelComplement.unblock();

                var result = $.parseJSON(JSON.stringify(response));
                numberBien = response.numberOfBien;

                if (numberBien < 10) {
                    numberBien = "0" + numberBien;
                }
                
                inputIntitule.val(response.nomePersonne + ' : ' + $('#cmbTypeBien option:selected').text().toUpperCase() + " - " + numberBien);

                var typeComplementBienList = result.typeComplementBienList;

                var dataTypeComplementBien = '';
                for (var i = 0; i < typeComplementBienList.length; i++) {
                    dataTypeComplementBien += typeComplementBienList[i].inputValue;
                }
                divComplement.html(dataTypeComplementBien);

                if (result.bien != '') {

                    var dataBien = result.bien;
                    var complementList = dataBien.complementList;

                    inputIntitule.val(dataBien.intituleBien);
                    inputDescription.val(dataBien.descriptionBien);
                    inputDateAcquissition.val(dataBien.dateAcquisition);

                    //cmbCategorieBien.val(dataBien.codeCategorie);
                    cmbUsageBien.val(dataBien.codeUsage);
                    cmbCommuneBien.val(dataBien.codeCommune);
                    codeRangCurrent = dataBien.codeCategorie;

                    for (var i = 0; i < eaList.length; i++) {

                        if (eaList[i].code == cmbCommuneBien.val()) {

                            var listQuartier = eaList[i].quartierList;

                            var dataQuartier = '<option value ="0">-- Sélectionner --</option>';

                            for (var j = 0; j < listQuartier.length; j++) {

                                if (listQuartier[j].communeCode == cmbCommuneBien.val()) {

                                    dataQuartier += '<option value =' + listQuartier[j].quartierCode + '>' + listQuartier[j].quartierName + '</option>';
                                }
                            }
                            cmbQuartierBien.html(empty);
                            cmbQuartierBien.html(dataQuartier);
                            cmbQuartierBien.val(dataBien.codeQuartier);

                        }
                    }

                    getTarifsByQuartier(dataBien.codeQuartier);

                    selectedCodeAP = dataBien.codeAdressePersonne;
                    inputAdresse.val(dataBien.chaineAdressePersonne.toUpperCase());

                    var length = typeComplementBienList.length;

                    for (var i = 0; i < length; i++) {
                        var idControl = "#IDENT_" + i;
                        var name = $(idControl).attr("name");
                        var splitName = name.split('-');
                        var code = splitName[1];
                        var requiered = splitName[2];
                        for (var j = 0; j < complementList.length; j++) {
                            if (complementList[j].codeTypeComplementBien == code) {
                                $(idControl).val(complementList[j].valeur);
                                $(idControl).attr('name', complementList[j].id + '-' + code + '-' + requiered);
                                var uniteDevise = complementList[j].uniteDevise;
                                if (uniteDevise != '') {
                                    if (uniteDevise.length > 5) {
                                        $(idControl + '-u').val(uniteDevise);
                                    } else {
                                        $(idControl + '-d').val(uniteDevise);
                                    }
                                }
                            }
                        }
                    }
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            divPanelComplement.unblock();
            showResponseError();
        }

    });
}

function getSelectedAdressePersonneData() {

    inputAdresse.val(selectedAdressePersonne.chaine);
    selectedCodeAP = selectedAdressePersonne.codeAP;
}

function checkFields() {

    if (cmbTypeBien.val() === '0') {
        alertify.alert('Veuillez d\'abord sélectionner la nature du bien');
        return;
    }

    if (cmbUsageBien.val() === '0') {
        alertify.alert('Veuillez d\'abord sélectionner l\'usage du bien');
        return;
    }

    if (rangExist) {

        if (checkBox.checked) {

            if (cmbTypeBien.val() == 'TB00000028') {

                if (cmbCategorieBien.val() === '0') {
                    alertify.alert('Veuillez d\'abord sélectionner la localité ou rang du bien');
                    return;
                }

            } else {

                if (cmbCommuneBien.val() === '0') {
                    alertify.alert('Veuillez d\'abord sélectionner la commune du bien');
                    return;
                }

                if (cmbQuartierBien.val() === '0') {
                    alertify.alert('Veuillez d\'abord sélectionner le quartier du bien');
                    return;
                }

                if (cmbCategorieBien.val() === '0') {
                    alertify.alert('Veuillez d\'abord sélectionner la localité ou rang du bien');
                    return;
                }
            }


        }
    }



    if (inputIntitule.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir la dénomination du bien');
        return;
    }

    /*if (inputDateAcquissition.val().trim() === '') {
     alertify.alert('Veuillez d\'abord saisir la date d\'acquisition du bien');
     return;
     }*/

    if (inputAdresse.val().trim() === '') {
        alertify.alert('Veuillez d\'abord indiquer l\'adresse du bien');
        return;
    }

    var passe = true;

    if ($("#divComplement") != null && $("#divComplement").length > 0) {

        var count = 0;
        tempComplementList = [];

        $("#divComplement").children().each(function () {

            var idControl = "#IDENT_" + count;
            var value = $(this).find(idControl).val();
            var name = $(this).find(idControl).attr("name");
            var placeholder = $(this).find(idControl).attr("placeholder");

            var splitName = name.split('-');
            var id = splitName[0];
            var code = splitName[1];
            var requiered = splitName[2];

            var deviseId = $(idControl + "-d");
            var uniteId = $(idControl + "-u");

            var hasDevise = (deviseId.val() == undefined) ? false : true;
            var hasUnite = (uniteId.val() == undefined) ? false : true;

            var uniteDevise = '';

            if (requiered === '1') {

                if (value === '0') {
                    var val = parseFloat(value);
                    if (val < 0) {
                        passe = false;
                        alertify.alert(placeholder + ' ne doit pas contenir une valeur inférieure à zéro');
                        return;
                    }
                }

                if (value.trim() === '' || value === '0') {
                    passe = false;
                    alertify.alert(placeholder + ' est obligatoire.');
                    return;

                } else {

                    if (hasDevise) {

                        uniteDevise = deviseId.val();
                        if (uniteDevise == '0') {
                            passe = false;
                            alertify.alert('La devise du complément ' + placeholder + ' est obligatoire.');
                            return;
                        }

                    } else {

                        if (hasUnite) {
                            uniteDevise = uniteId.val();
                            if (uniteDevise == '0') {
                                passe = false;
                                alertify.alert('L\'unité du complément ' + placeholder + ' est obligatoire.');
                                return;
                            }
                        }
                    }
                }
            } else {

                if (hasDevise) {
                    uniteDevise = deviseId.val();
                    if (uniteDevise == '0') {
                        uniteDevise = '';
                    }

                } else {

                    if (hasUnite) {
                        uniteDevise = uniteId.val();
                        if (uniteDevise == '0') {
                            uniteDevise = '';
                        }
                    }
                }
            }

            count++;
            var complement = new Object();
            complement.id = id;
            complement.code = code;
            complement.valeur = value;
            complement.uniteDevise = uniteDevise;
            tempComplementList.push(complement);
        });
    }

    if (passe) {
        alertify.confirm('Etes-vous sûre de vouloir enregistrer ce bien immobilier ?', function () {
            saveBien();
        });
    }
}

function saveBien() {

    var complementsBien = JSON.stringify(tempComplementList);

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveBienImmobilier',
            'idBien': idOfBien,
            'codeTypeBien': cmbTypeBien.val(),
            'codePersonne': codeOfPersonne,
            'intituleBien': inputIntitule.val(),
            'codeUsage': cmbUsageBien.val(),
            'codeCommune': cmbCommuneBien.val(),
            'codeQuartier': cmbQuartierBien.val(),
            'codeCategorie': cmbCategorieBien.val(),
            'dateAcquisition': inputDateAcquissition.val(),
            'codeAP': selectedCodeAP,
            'idUser': userData.idUser,
            'complementBiens': complementsBien
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du bien ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('L\'enregistrement du bien s\'est effectué avec succès.');

                    setTimeout(function () {
                        window.location = 'gestion-bien?id=' + btoa(codeOfPersonne);
                    }
                    , 1500);

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement du bien.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }
    });
}

function getTarifsByQuartier(code) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTarifByQuartier',
            'codeQuartier': code,
            'codeNature': isModify == false ? cmbTypeBien.val() : codeOfTypeBien
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Récupération des tarifs en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }
            if (response == '0') {
                $.unblockUI();
                cmbCategorieBien.html(empty);
                codeRangCurrent = '';
                alertify.alert('Il n\' y a aucune localité associée au quartier : ' + '<span style="font-weight:bold">' + $('#cmbQuartierBien option:selected').text().toUpperCase() + '</span>');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                cmbTypeBienSelected = isModify == false ? cmbTypeBien.val() : codeOfTypeBien;

                switch (cmbTypeBienSelected) {

                    case 'TB00000031' : //CITERNE/TANK
                    case 'TB00000035' : //CONTAINEUR
                    case 'TB00000033' : //DEPOT
                    case 'TB00000034' : //EMETTEUR/RECEPTEUR
                    case 'TB00000032' : //ENTREPOT
                    case 'TB00000030' : //HANGAR
                    case 'TB00000023' : //ANTENNE DE COMMUNICATION


                        IdDivCmbCommuneBien.attr('style', 'display:none');
                        IdDivCmbQuartierBien.attr('style', 'display:none');
                        IdDivCmbCategorieBien.attr('style', 'display:none');

                        rangExist = false;

                        break;

                    case 'TB00000028' : //TERRAIN

                        IdDivCmbCommuneBien.attr('style', 'display:none');
                        IdDivCmbQuartierBien.attr('style', 'display:none');
                        IdDivCmbCategorieBien.attr('style', 'display:block');

                        rangExist = true;

                        break;

                    case 'TB00000026' : //AP
                    case 'TB00000027' : //AT
                    case 'TB00000025' : //VILLA
                    case 'TB00000029' : //ETAGE (ET)

                        alertify.confirm('Lors de la modification de ce bien s\'il sera associé à l\'impôt foncier, veuillez cliqier sur le bouton (OK) ?', function () {

                            checkBox.checked = true;
                            document.getElementById("checkBoxAssociateBienWithIF").checked = true;

                            if (checkBox.checked) {

                                IdDivCmbCommuneBien.attr('style', 'display:block');
                                IdDivCmbQuartierBien.attr('style', 'display:block');
                                IdDivCmbCategorieBien.attr('style', 'display:block');

                                rangExist = true;

                            }

                        }, function () {

                            IdDivCmbCommuneBien.attr('style', 'display:none');
                            IdDivCmbQuartierBien.attr('style', 'display:none');
                            IdDivCmbCategorieBien.attr('style', 'display:none');

                            rangExist = false;
                        });





                        break;

                }

                tarifList = $.parseJSON(JSON.stringify(response));

                var dataCategorieBien = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < tarifList.length; i++) {


                    dataCategorieBien += '<option value =' + tarifList[i].code + '>' + tarifList[i].intitule + '</option>';

                }
                cmbCategorieBien.html(empty);
                cmbCategorieBien.html(dataCategorieBien);
                cmbCategorieBien.val(codeRangCurrent);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function checkFieldsRequired() {

    checkBox = document.getElementById("checkBoxAssociateBienWithIF");

    if (checkBox.checked) {

        spnCommuneId.attr('style', 'color:red');
        spnCommuneId.html('*');
        spnQuartierId.attr('style', 'color:red');
        spnQuartierId.html('*');
        spnRangId.attr('style', 'color:red');
        spnRangId.html('*');

        IdDivCmbCommuneBien.attr('style', 'display:block');
        IdDivCmbQuartierBien.attr('style', 'display:block');
        IdDivCmbCategorieBien.attr('style', 'display:block');


    } else {

        spnCommuneId.html('');
        spnQuartierId.html('');
        spnRangId.html('');

        IdDivCmbCommuneBien.attr('style', 'display:none');
        IdDivCmbQuartierBien.attr('style', 'display:none');
        IdDivCmbCategorieBien.attr('style', 'display:none');
    }

    //alert(checkBox.checked);
}

function loadRangs() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTarifByQuartier',
            'codeQuartier': cmbQuartierBien.val(),
            'codeNature': isModify == false ? cmbTypeBien.val() : codeOfTypeBien
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Récupération des tarifs en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }
            if (response == '0') {
                $.unblockUI();
                cmbCategorieBien.html(empty);
                codeRangCurrent = '';
                alertify.alert('Il n\' y a aucune localité associée au quartier : ' + '<span style="font-weight:bold">' + $('#cmbQuartierBien option:selected').text().toUpperCase() + '</span>');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                tarifList = $.parseJSON(JSON.stringify(response));

                var dataCategorieBien = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < tarifList.length; i++) {


                    dataCategorieBien += '<option value =' + tarifList[i].code + '>' + tarifList[i].intitule + '</option>';

                }
                cmbCategorieBien.html(empty);
                cmbCategorieBien.html(dataCategorieBien);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}


