/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var medList = [];
var mepList = [];
var tableDefaillants;
var tableDefaillantPaiement;
var btnSimpleSearch;
var assujettiModal;
var assujettiCodeValue;
var codeAssujetti;
var typeAssujetti;
var adresseId;
var responsibleObject = new Object();
var medDocumentPrint;
var medObj = new Object();
var mepObj = new Object();
var isAvancedSearch;
var modalRechercheAvanceeByArticle;
var btnShowAdvancedSerachModal;
var btnAdvencedSearchByArticle;

var codeTypeDoc = 'MEP';
var codeDoc = undefined;

var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var selectAB, selectAnnee_1, selectMois_1;

var lblArticleBudgetaire, lblPeriodeDeclaration;

var isAdvance;
var btnAdvencedSearch;
var modalRechercheAvanceeModelTwo, btnShowAdvancedSerachModal;

var lblService, lblSite, lblDateDebut, lblDateFin;

var modalAccuserReception;

var medInfoModal, medPrintInviteService,
        inputDateHeureInvitation, btnPrintInvitation;

var idIconBtnOperation,
        btnPrintMedOrMep,
        valDateEcheance,
        valDateReception,
        valDateCreate,
        valMedOrMep,
        lblMedOrMep,
        divInfoMed,
        valExercice,
        valReference,
        lblReference;

var echeanceMedExist;

var dateDebuts, dateFins, codeService, codeSite;
var tempDefaillantPaiementList = [];
var inputDateDebut, inputdateLast, selectService, selectSite;

$(function () {

    medDocumentPrint = '';

    mainNavigationLabel.text('RECOUVREMENT');

    switch (getRegisterType()) {
        case 'MED':
            secondNavigationLabel.text('Registre des défaillants en déclaration des notes de taxation');
            break;
        case 'MEP':
            secondNavigationLabel.text('Registre des défaillants au paiement des notes de perception');
            break;
    }

    removeActiveMenu();
    linkMenuContentieux.addClass('active');

    tableDefaillants = $('#tableDefaillants');
    btnSimpleSearch = $('#btnSimpleSearch');
    assujettiModal = $('#assujettiModal');
    assujettiCodeValue = $('#assujettiCodeValue');
    modalRechercheAvanceeByArticle = $('#modalRechercheAvanceeByArticle');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    btnAdvencedSearchByArticle = $('#btnAdvencedSearchByArticle');

    selectAB = $('#selectAB');
    selectAnnee_1 = $('#selectAnnee_1');
    selectMois_1 = $('#selectMois_1');

    lblService = $('#lblService');
    lblArticleBudgetaire = $('#lblArticleBudgetaire');
    lblPeriodeDeclaration = $('#lblPeriodeDeclaration');
    isAdvance = $('#isAdvance');
    tableDefaillantPaiement = $('#tableDefaillantPaiement');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    modalRechercheAvanceeModelTwo = $('#modalRechercheAvanceeModelTwo');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');

    //inputDateHeureInvitation = $('#inputDateHeureInvitation');
    medPrintInviteService = $('#medPrintInviteService');
    btnPrintInvitation = $('#btnPrintInvitation');

    lblService = $('#lblService');
    lblSite = $('#lblSite');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    modalAccuserReception = $('#modalAccuserReception');
    medInfoModal = $('#medInfoModal');

    idIconBtnOperation = $('#idIconBtnOperation');
    btnPrintMedOrMep = $('#btnPrintMedOrMep');

    valDateEcheance = $('#valDateEcheance');
    valDateReception = $('#valDateReception');
    valDateCreate = $('#valDateCreate');
    valMedOrMep = $('#valMedOrMep');
    lblMedOrMep = $('#lblMedOrMep');
    divInfoMed = $('#divInfoMed');

    valExercice = $('#valExercice');
    valReference = $('#valReference');
    lblReference = $('#lblReference');

    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');
    selectService = $('#selectService');
    selectSite = $('#selectSite');

    btnSimpleSearch.on('click', function (e) {
        e.preventDefault();
        assujettiModal.modal('show');
    });

    btnPrintMedOrMep.on('click', function (e) {
        e.preventDefault();
        printMedOrMep(medDocumentPrint);
    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeModelTwo.modal('show');
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAvancedSearch = '1';
        assujettiCodeValue.val(empty);
        assujettiCodeValue.attr('style', 'font-weight:normal;width: 380px');
        loadMep(isAvancedSearch);
    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalRechercheAvanceeByArticle.modal('show');
    });

    btnAdvencedSearchByArticle.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        isAvancedSearch = '1';
        assujettiCodeValue.val(empty);
        assujettiCodeValue.attr('style', 'font-weight:normal;width: 380px');
        loadMed(isAvancedSearch);
    });

    btnPrintInvitation.click(function (e) {
        e.preventDefault();
        var inputDateHeureInvitation = $('#inputDateHeureInvitation');
        if (inputDateHeureInvitation.val().trim() == '') {
            alertify.alert('Veuillez saisir la date et l\'heure de l\'invitation');
            return;
        }
        mepObj.dateHeureInvitation = inputDateHeureInvitation.val();

        $('#medPrintInviteService').modal('hide');

        saveAndPrintMep(mepObj);
    });


    printDefaillantDeclaration(empty);
    printDefaillantPaiement(empty);
    getDefaillantPaiement();

    $('.rapport-defaillant-paiement').click(function (e) {
        e.preventDefault();
        alertify.confirm('Etes-vous sûre de vouloir imprimer le rapport des défaillants au paiement ?', function () {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            if (tempDefaillantPaiementList.length != 0) {
                printDataDefaillantPaiement();
            } else {
                alertify.alert('Aucune donnée n\'est disponnible pour imprimer ce rapport.');
            }
        });
    });

});

function loadMed(typeSearch) {

    switch (typeSearch) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':
            var valuePeriode = '';

            switch (codePeriodicite) {
                case 'PR0032015' :// Mensuelle
                    valuePeriode = $('#selectMois_1 option:selected').text() + '-' + selectAnnee_1.val();
                    break;
                case 'PR0042015' : // Annuelle
                    valuePeriode = selectAnnee_1.val();
                    break;
            }

            lblPeriodeDeclaration.text(valuePeriode);
            lblPeriodeDeclaration.attr('title', valuePeriode);

            lblService.text($('#selectService_ option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService_ option:selected').text().toUpperCase());

            lblArticleBudgetaire.text($('#selectAB option:selected').text().toUpperCase());
            lblArticleBudgetaire.attr('title', $('#selectAB option:selected').text().toUpperCase());

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'articleBudgetaireCode': codeABSelected,
            'codePeriodicite': codePeriodicite,
            'periodValueYear': selectAnnee_1.val(),
            'periodValueMonth': selectMois_1.val(),
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': typeSearch,
            'operation': 'loadDefaillantDeclaration'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                switch (typeSearch) {
                    case '0':
                        printDefaillantDeclaration('');
                        alertify.alert('Cet assujetti : ' + assujettiCodeValue.val() + ' n\'a pas de période en retard de déclaration.');
                        break;
                    case '1':
                        printDefaillantDeclaration('');
                        alertify.alert('Aucune période en retard de déclaration ne corresponde au critère de recherche fournis.');
                        break;
                }

            } else {
                medList = JSON.parse(JSON.stringify(response));
                modalRechercheAvanceeByArticle.modal('hide');
                printDefaillantDeclaration(medList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printDefaillantPaiement(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:left">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left">NOTE DE PERCEPTION</th>';
    tableContent += '<th style="text-align:center">DATE ORDO.</th>';
    tableContent += '<th style="text-align:center">DATE ECHEANCE</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var amount = formatNumber(result[i].amountNp, result[i].deviseNp);
        var buttonPrintMep = '';

        switch (result[i].printExist) {
            case '0':
                buttonPrintMep = '<button class="btn btn-warning" onclick="callPrintAndSaveMep(\'' + result[i].numeroNp
                        + '\',\'' + result[i].assujettiCode + '\',\'' + result[i].assujettiName + '\',\'' + result[i].adresseCode
                        + '\',\'' + result[i].dateOrdonnancement
                        + '\',\'' + result[i].exerciceNp
                        + '\',\'' + result[i].printExist + '\',\'' + result[i].dateEcheanceNp
                        + '\',\'' + result[i].numeroMed + '\',\'' + amount + '\',\'' + result[i].numeroNc + '\',\'' + result[i].amountNp + '\',\'' + result[i].articleBudgetaireCode + '\',\'' + result[i].nbreMoisRetard + '\')"><i class="fa fa-print"></i></button>';
                break;
            case '1':
                buttonPrintMep = '<button class="btn btn-warning" onclick="printDocument(\'' + result[i].numeroMed + '\')"><i class="fa fa-list"></i></button>';
                break;
        }

        var data = new Object();
        data.exerciceNp = result[i].exerciceNp;
        data.assujettiNameComposite = result[i].assujettiNameComposite;
        data.numeroNp = result[i].numeroNp;
        data.dateOrdonnancement = result[i].dateOrdonnancement;
        data.dateEcheanceNp = result[i].dateEcheanceNp;
        data.articleBudgetaireName = result[i].articleBudgetaireName;
        data.amount = result[i].amountNp + ' ' + result[i].deviseNp;
        tempDefaillantPaiementList.push(data);

        tableContent += '<tr>';
        tableContent += '<td style="text-align:center;width:8%;vertical-align:middle">' + result[i].exerciceNp + '</td>';
        tableContent += '<td style="text-align:left;width:30%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:9%;vertical-align:middle">' + result[i].numeroNp + '</td>';
        tableContent += '<td style="text-align:center;width:9%;vertical-align:middle">' + result[i].dateOrdonnancement + '</td>';
        tableContent += '<td style="text-align:center;width:9%;color:red;vertical-align:middle;font-weight:bold">' + result[i].dateEcheanceNp + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].articleBudgetaireName.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold">' + amount + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonPrintMep + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableDefaillantPaiement.html(tableContent);
    tableDefaillantPaiement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 5,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 5
    });

}

function printDefaillantDeclaration(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:center">NTD</th>';
    tableContent += '<th style="text-align:left">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:center">ECHEANCE DECLARATION</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var buttonPrintMed = '';

        switch (result[i].printExist) {
            case '0':
                buttonPrintMed = '<button class="btn btn-warning" onclick="callPrintAndSaveMed(\'' + result[i].periodeDeclarationId
                        + '\',\'' + result[i].assujettiCode
                        + '\',\'' + result[i].assujettiName + '\',\'' + result[i].adresseCode
                        + '\',\'' + result[i].adresseName + '\',\'' + result[i].echeanceDeclaration
                        + '\',\'' + result[i].articleBudgetaireCode + '\',\'' + result[i].articleBudgetaireName
                        + '\',\'' + result[i].amountPeriodeDeclaration + '\',\'' + result[i].printExist
                        + '\',\'' + result[i].periodeDeclaration
                        + '\',\'' + result[i].numeroMed + '\')"><i class="fa fa-print"></i></button>';
                break;
            case '1':

                buttonPrintMed = '<button class="btn btn-warning" onclick="printDocument(\'' + result[i].numeroMed + '\')"><i class="fa fa-list"></i></button>';
                break;
        }



        tableContent += '<tr>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + result[i].periodeDeclaration + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + result[i].userName + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].articleBudgetaireName.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle;color:red;font-weight:bold">' + result[i].echeanceDeclaration + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonPrintMed + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableDefaillants.html(tableContent);
    tableDefaillants.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 5,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 5,
        dom: 'Bfrtip', columnDefs: [
            {"visible": false, "targets": 2}
        ], order: [[2, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="5">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

}

function callPrintAndSaveMep(numeroNp, assujettiCode, assujettiName, adresseCode, dateOrdonnancement,
        exerciceNp, printExist, dateEcheanceNp, numeroMed, amountNp1, numeroNc, amountNp2, articleCode, nbreMoisRetard) {

    var value = '<span style="font-weight:bold"> ' + numeroNp + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir générer une invitation de service pour cette note de perception n° ' + value + ' ? ', function () {

        mepObj = new Object();

        mepObj.numeroNp = numeroNp;
        mepObj.assujettiCode = assujettiCode;
        mepObj.assujettiName = assujettiName;
        mepObj.adresseCode = adresseCode;
        mepObj.dateOrdonnancement = dateOrdonnancement;
        mepObj.exerciceNp = exerciceNp;
        mepObj.printExist = printExist;
        mepObj.dateEcheanceNp = dateEcheanceNp;
        mepObj.numeroMed = numeroMed;
        mepObj.amountNp1 = amountNp1;
        mepObj.amountNp2 = amountNp2;
        mepObj.numeroNc = numeroNc;
        mepObj.articleBudgetaireCode = articleCode;
        mepObj.nbreMoisRetard = nbreMoisRetard;

        medPrintInviteService.modal('show');

        //saveAndPrintMep(mepObj);
    });
}

function callPrintAndSaveMed(periodeID, assujettiCode, assujettiName,
        adresseCode, adresseName, echeanceDeclaration, articleBudgetaireCode,
        articleBudgetaireName, amountPeriodeDeclaration, printExist, periodeDeclaration, numeroMed) {

    alertify.confirm('Etes-vous sûre de vouloir imprimer une mise en demeure pour cette période de déclaration ? ', function () {

        medObj = new Object();

        medObj.periodeDeclarationId = periodeID;
        medObj.exercice = periodeDeclaration;
        medObj.assujettiCode = assujettiCode;
        medObj.assujettiName = assujettiName;
        medObj.adresseCode = adresseCode;
        medObj.adresseName = adresseName;
        medObj.echeance = echeanceDeclaration;
        medObj.articleBudgetaireCode = articleBudgetaireCode;
        medObj.articleBudgetaireName = articleBudgetaireName;
        medObj.amountPeriodeDeclaration = amountPeriodeDeclaration;
        medObj.printExist = printExist;
        medObj.numeroMed = numeroMed;

        saveAndPrintMed(medObj);
    });

}


function getSelectedAssujetiData() {
    assujettiCodeValue.val(selectAssujettiData.nomComplet);
    assujettiCodeValue.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;
    isAvancedSearch = '0';

    switch (getRegisterType()) {
        case 'MED':
            loadMed(isAvancedSearch);
            break;
        case 'MEP':
            loadMep(isAvancedSearch);
            break;
    }

}

function loadDefaillantPaiement(typeSearch) {


    switch (typeSearch) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            lblSite.text($('#selectSite option:selected').text().toUpperCase());
            lblSite.attr('title', $('#selectSite option:selected').text().toUpperCase());

            lblService.text($('#selectService option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblDateDebut.text(dateDebuts);
            lblDateDebut.attr('title', dateDebuts);

            lblDateFin.text(dateFins);
            lblDateFin.attr('title', dateFins);

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': codeService,
            'codeSite': codeSite,
            'dateDebut': dateDebuts,
            'dateFin': dateFins,
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': typeSearch,
            'operation': 'loadDefaillantPaiement'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                printDefaillantPaiement('');
                alertify.alert('Aucune note de perception en retard de paiement.');
            } else {
                mepList = JSON.parse(JSON.stringify(response));
                printDefaillantPaiement(mepList);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function saveAndPrintMed(medObj) {


    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'periodeDeclarationId': medObj.periodeDeclarationId,
            'articleBudgetaireCode': medObj.articleBudgetaireCode,
            'assujettiCode': medObj.assujettiCode,
            'echeanceDeclaration': medObj.echeance,
            'periodeDeclaration': medObj.exercice,
            'adresseCode': medObj.adresseCode,
            'articleBudgetaireName': medObj.articleBudgetaireName,
            'assujettiName': medObj.assujettiName,
            'adresseName': medObj.adresseName,
            'amountPeriodeDeclarationToString': medObj.amountPeriodeDeclarationToString,
            'amountPeriodeDeclaration': medObj.amountPeriodeDeclaration,
            'idUser': userData.idUser,
            'printExist': medObj.printExist,
            'numeroMed': medObj.numeroMed,
            'dateHeureInvitation': medObj.dateHeureInvitation,
            'operation': 'printMed'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else {

                setTimeout(function () {

                    $.unblockUI();
                    setDocumentContent(response);

                    setTimeout(function () {
                    }, 2000);

                    loadMed(isAvancedSearch);

                    window.open('visualisation-document', '_blank');
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function saveAndPrintMep(mepObj) {

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'numeroNp': mepObj.numeroNp,
            'assujettiCode': mepObj.assujettiCode,
            'assujettiName': mepObj.assujettiName,
            'adresseCode': mepObj.adresseCode,
            'dateOrdonnancement': mepObj.dateOrdonnancement,
            'articleBudgetaireCode': mepObj.articleBudgetaireCode,
            'exerciceNp': mepObj.exerciceNp,
            'printExist': mepObj.printExist,
            'dateEcheanceNp': mepObj.dateEcheanceNp,
            'numeroMed': mepObj.numeroMed,
            'amountNp1': mepObj.amountNp1,
            'amountNp2': mepObj.amountNp2,
            'numeroNc': mepObj.numeroNc,
            'dateHeureInvitation': mepObj.dateHeureInvitation,
            'nbreMoisRetard': mepObj.nbreMoisRetard,
            'idUser': userData.idUser,
            'operation': 'printMep'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else {

                setTimeout(function () {

                    $.unblockUI();
                    setDocumentContent(response);

                    setTimeout(function () {
                    }, 2000);

                    loadMep(isAvancedSearch);

                    window.open('visualisation-document', '_blank');
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function printDocument(medId) {

    var msg = '';

    switch (getRegisterType()) {

        case 'MED':
            msg = 'Etes-vous sûre de vouloir imprimer une mise en demeure de déclaration ? ';

            for (var i = 0; i < medList.length; i++) {

                if (medList[i].numeroMed === medId) {

                    medDocumentPrint = medList[i].documentPrint;
                }
            }

            alertify.confirm(msg, function () {

                setTimeout(function () {
                    setDocumentContent(medDocumentPrint);

                    setTimeout(function () {
                    }, 2000);
                    window.open('visualisation-document', '_blank');
                }
                , 1);
            });
            break;
        case 'MEP':

            msg = 'Etes-vous sûre de vouloir ré-imprimer l\'invitation de service pour cette note de perception ? ';

            for (var i = 0; i < mepList.length; i++) {

                if (mepList[i].numeroMed === medId) {

                    medDocumentPrint = mepList[i].documentPrint;
                }
            }

            alertify.confirm(msg, function () {

                setTimeout(function () {
                    setDocumentContent(medDocumentPrint);

                    setTimeout(function () {
                    }, 2000);
                    window.open('visualisation-document', '_blank');
                }
                , 1);
            });
            break;
    }
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function printMedOrMep(document) {

    var msg = '';

    switch (getRegisterType()) {
        case 'MED':
            msg = 'Etes-vous sûre de vouloir imprimer une mise en demeure de déclaration ? ';
            break;
        case 'MEP':
            msg = 'Etes-vous sûre de vouloir imprimer une mise en demeure au paiement ? ';
            break;
    }

    alertify.confirm(msg, function () {

        setTimeout(function () {
            setDocumentContent(document);

            setTimeout(function () {
            }, 2000);
            window.open('visualisation-document', '_blank');
        }
        , 1);
    });

}

function refrechDataAfterAccuserReception() {

    switch (getRegisterType()) {
        case 'MED':
            loadMed(isAvancedSearch);
            break;
        case 'MEP':
            loadMep(isAvancedSearch);
            break;
    }
}

function getDefaillantPaiement() {

    var date = new Date();
    var day = dateFormat(date.getDate());
    var month = dateFormat(date.getMonth() + 1);
    var year = date.getFullYear();

    var dateDuJour = day + "-" + month + "-" + year;

    dateDebuts = dateDuJour;
    dateFins = dateDuJour;
    codeService = userData.serviceCode;
    codeSite = userData.SiteCode;

    loadDefaillantPaiement(1);

}

function loadMep(typeSearch) {

    switch (typeSearch) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        
        case '1':

            lblSite.text($('#selectService option:selected').text().toUpperCase());
            lblSite.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblService.text($('#selectService option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblDateDebut.text(inputDateDebut.val());
            lblDateDebut.attr('title', inputDateDebut.val());

            lblDateFin.text(inputdateLast.val());
            lblDateFin.attr('title', inputdateLast.val());

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': selectService.val(),
            'codeSite': selectSite.val(),
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': typeSearch,
            'operation': 'loadDefaillantPaiement'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                switch (typeSearch) {
                    case '0':
                        printDefaillantPaiement('');
                        alertify.alert('Cet assujetti : ' + assujettiCodeValue.val() + ' n\'a pas de notes de perception en retard de paiement.');
                        //return;
                        break;
                    case '1':
                        printDefaillantPaiement('');
                        alertify.alert('Aucune note de perception en retard de paiement ne corresponde au critère de recherche fournis.');
                        //return;
                        break;
                }


            } else {
                mepList = JSON.parse(JSON.stringify(response));
                modalRechercheAvanceeModelTwo.modal('hide');
                printDefaillantPaiement(mepList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printDataDefaillantPaiement() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    if (tempDefaillantPaiementList.length > 0) {

        if (isAvancedSearch == 1) {
            docTitle = 'REGISTRE DES DEFAILLANTS AU PAIEMENT';
            if (selectSite.val() == '*' && selectService.val() == '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 300;
            } else if (selectSite.val() !== '*' && selectService.val() == '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSITE : ' + $('#selectSite option:selected').text();
                position = 300;
                hauteur = 160;
            } else if (selectSite.val() == '*' && selectService.val() !== '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSERVICE : ' + $('#selectService option:selected').text();
                position = 300;
                hauteur = 160;
            } else {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSITE : ' + $('#selectSite option:selected').text();
                docTitle += '\nSERVICE : ' + $('#selectService option:selected').text();
                position = 300;
                hauteur = 170;
            }
        } else {
            var critere = assujettiCodeValue.val().toUpperCase();
            docTitle = 'REGISTRE DES DEFAILLANTS AU PAIEMENT : ' + critere;
            position = 300;
        }

    } else {
        docTitle = 'REGISTRE DES DEFAILLANTS AU PAIEMENT';
        position = 440;
    }

    var columns = [
        {title: "EXERCICE", dataKey: "exerciceNp"},
        {title: "ASSUJETTI", dataKey: "assujettiNameComposite"},
        {title: "NOTE DE PERCEPTION", dataKey: "numeroNp"},
        {title: "DATE ORDONNANCEMENT", dataKey: "dateOrdonnancement"},
        {title: "DATE ECHEANCE", dataKey: "dateEcheanceNp"},
        {title: "ARTICLE BUDGETAIRE", dataKey: "articleBudgetaireName"},
        {title: "MONTANT DÛ", dataKey: "amount"}];

    var rows = tempDefaillantPaiementList;
    var pageContent = function (data) {

        doc.setFontSize(10);
        doc.text("REPUBLIQUE DEMOCRATIQUE DU CONGO", 40, 25);
        doc.setFontSize(9);
        doc.text("PROVINCE DU HAUT-KATANGA", 40, 40);
        doc.setFontSize(10);
        doc.text(docTitle, position, 140);
        doc.addImage(docImage, 'PNG', 50, 45, 75, 75);

        doc.setFontSize(8);
        doc.text("Imprimé le " + day + '/' + month + '/' + year + ' à ' + hh + ':' + mm + ':' + ss + ' avec E-RECETTES 1.0', 590, 25);
        doc.text("Par : " + userData.nomComplet, 590, 35);
        doc.text("Page : " + data.pageCount, 590, 45);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: {
            exerciceNp: {columnWidth: 90, fontSize: 8, overflow: 'linebreak'},
            assujettiNameComposite: {columnWidth: 115, overflow: 'linebreak', fontSize: 8},
            numeroNp: {columnWidth: 115, overflow: 'linebreak', fontSize: 8},
            dateOrdonnancement: {columnWidth: 130, overflow: 'linebreak', fontSize: 8},
            dateEcheanceNp: {columnWidth: 90, overflow: 'linebreak', fontSize: 8},
            articleBudgetaireName: {columnWidth: 130, overflow: 'linebreak', fontSize: 8},
            amount: {columnWidth: 100, overflow: 'linebreak', fontSize: 8}
        },
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 10 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');
//    doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(sumCDF), 540, finalY + 40);
//    doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD), 540, finalY + 60);
    window.open(doc.output('bloburl'), '_blank');
}
