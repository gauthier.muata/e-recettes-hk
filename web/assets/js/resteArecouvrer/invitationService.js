/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var medList = [], mepList = [];
var tableDefaillants, tableDefaillantPaiement;
var assujettiModal, modalRechercheAvanceeByArticle;
var assujettiCodeValue, codeAssujetti, typeAssujetti;
var adresseId, medDocumentPrint, isAvancedSearch;
var responsibleObject = {}, medObj = {}, mepObj = {};
var btnSimpleSearch, btnShowAdvancedSerachModal, btnAdvencedSearchByArticle;
var codeMedCurrent;
var typeMed;
var codeTypeDoc = 'INVITATION_SERVICE';
var codeDoc = undefined;

var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var selectAB, selectAnnee_1, selectMois_1;

var lblService, lblArticleBudgetaire, lblPeriodeDeclaration;

var isAdvance;
var btnAdvencedSearch;
var modalRechercheAvanceeModelTwo, btnShowAdvancedSerachModal;

var lblService, lblSite, lblDateDebut, lblDateFin;

var modalAccuserReception;

var medInfoModal;

var idIconBtnOperation,
        btnPrintMedOrMep,
        valDateEcheance,
        valDateReception,
        valDateCreate,
        valMedOrMep,
        lblMedOrMep,
        divInfoMed,
        valExercice,
        valReference,
        lblReference;

var echeanceMedExist;

var dateDebuts, dateFins, codeService,
        codeSite;

var modalGenerateNoteTaxationOfInvitationService, cmbBanqueNoteTation, cmbCompteBancaireBanqueNoteTation, btnValidateChoiceBanque;
var medSelected;
var cmbCompteBancaireBanqueNoteTationPenalite;
var banqueList = [];

$(function () {

    medDocumentPrint = '';

    mainNavigationLabel.text('POURSUITES');

    secondNavigationLabel.text('Registre des invitations de service');

    //removeActiveMenu();
    //linkMenuContentieux.addClass('active');

    tableDefaillants = $('#tableDefaillants');
    btnSimpleSearch = $('#btnSimpleSearch');
    assujettiModal = $('#assujettiModal');
    assujettiCodeValue = $('#assujettiCodeValue');
    modalRechercheAvanceeByArticle = $('#modalRechercheAvanceeByArticle');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    btnAdvencedSearchByArticle = $('#btnAdvencedSearchByArticle');

    selectAB = $('#selectAB');
    selectAnnee_1 = $('#selectAnnee_1');
    selectMois_1 = $('#selectMois_1');

    lblService = $('#lblService');
    lblArticleBudgetaire = $('#lblArticleBudgetaire');
    lblPeriodeDeclaration = $('#lblPeriodeDeclaration');
    isAdvance = $('#isAdvance');
    tableDefaillantPaiement = $('#tableDefaillantPaiement');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    modalRechercheAvanceeModelTwo = $('#modalRechercheAvanceeModelTwo');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');
    cmbCompteBancaireBanqueNoteTationPenalite = $('#cmbCompteBancaireBanqueNoteTationPenalite');

    lblService = $('#lblService');
    lblSite = $('#lblSite');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    modalAccuserReception = $('#modalAccuserReception');
    medInfoModal = $('#medInfoModal');

    idIconBtnOperation = $('#idIconBtnOperation');
    btnPrintMedOrMep = $('#btnPrintMedOrMep');

    valDateEcheance = $('#valDateEcheance');
    valDateReception = $('#valDateReception');
    valDateCreate = $('#valDateCreate');
    valMedOrMep = $('#valMedOrMep');
    lblMedOrMep = $('#lblMedOrMep');
    divInfoMed = $('#divInfoMed');

    valExercice = $('#valExercice');
    valReference = $('#valReference');
    lblReference = $('#lblReference');

    modalGenerateNoteTaxationOfInvitationService = $('#modalGenerateNoteTaxationOfInvitationService');
    cmbBanqueNoteTation = $('#cmbBanqueNoteTation');
    cmbCompteBancaireBanqueNoteTation = $('#cmbCompteBancaireBanqueNoteTation');
    btnValidateChoiceBanque = $('#btnValidateChoiceBanque');


    btnValidateChoiceBanque.on('click', function (e) {
        e.preventDefault();


        if (cmbBanqueNoteTation.val() == '0') {
            alertify.alert('Veuillez sélectionner une banque');
            return;
        }

        if (cmbCompteBancaireBanqueNoteTation.val() == '0') {
            alertify.alert('Veuillez sélectionner un compte bancaire pour la note de taxation principale');
            return;
        }
        
        if (cmbCompteBancaireBanqueNoteTationPenalite.val() == '0') {
            alertify.alert('Veuillez sélectionner un compte bancaire pour la note de taxation pénalié');
            return;
        }

        generateNoteTaxationPrincipalAndPenalite();

    });


    cmbBanqueNoteTation.on('change', function () {

        if (cmbBanqueNoteTation.val() !== '0') {


            for (var i = 0; i < banqueList.length; i++) {

                if (banqueList[i].codeBanque == cmbBanqueNoteTation.val()) {

                    var dataCompteBancaire = '';
                    dataCompteBancaire += '<option value="0">--</option>';


                    var listompteBancaire = JSON.parse(banqueList[i].compteBancaireList);

                    for (var j = 0; j < listompteBancaire.length; j++) {
                        dataCompteBancaire += '<option value="' + listompteBancaire[j].codeCompteBancaire + '">' + listompteBancaire[j].libelleCompteBancaire + '</option>';

                    }

                    cmbCompteBancaireBanqueNoteTation.html(dataCompteBancaire);
                    cmbCompteBancaireBanqueNoteTationPenalite.html(dataCompteBancaire);
                }
            }

        } else {
            alertify.alert('Veuillez sélectionner une banque');
            return;
        }

    });


    btnSimpleSearch.on('click', function (e) {
        e.preventDefault();
        assujettiModal.modal('show');
    });

    btnPrintMedOrMep.on('click', function (e) {
        e.preventDefault();
        printMedOrMep(medDocumentPrint);
    });



    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeModelTwo.modal('show');
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAvancedSearch = '1';
        assujettiCodeValue.val(empty);
        assujettiCodeValue.attr('style', 'font-weight:normal;width: 380px');
        loadMep(isAvancedSearch);
    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalRechercheAvanceeByArticle.modal('show');
    });

    btnAdvencedSearchByArticle.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        isAvancedSearch = '1';
        assujettiCodeValue.val(empty);
        assujettiCodeValue.attr('style', 'font-weight:normal;width: 380px');
        loadDefaillantDeclaration(isAvancedSearch);
    });

    setRegisterType('INVITATION_SERVICE');
    initDataBank();
    printInvitationService(empty);
    //loadDefaillantDeclaration('1');

});

function loadDefaillantDeclaration(typeSearch) {

    switch (typeSearch) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':
            var valuePeriode = '';

            switch (codePeriodicite) {
                case 'PR0032015' :// Mensuelle
                    valuePeriode = $('#selectMois_1 option:selected').text() + '-' + selectAnnee_1.val();
                    break;
                case 'PR0042015' : // Annuelle
                    valuePeriode = selectAnnee_1.val();
                    break;
            }

            lblPeriodeDeclaration.text(valuePeriode);
            lblPeriodeDeclaration.attr('title', valuePeriode);

            lblService.text($('#selectService_ option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService_ option:selected').text().toUpperCase());

            lblArticleBudgetaire.text($('#selectAB option:selected').text().toUpperCase());
            lblArticleBudgetaire.attr('title', $('#selectAB option:selected').text().toUpperCase());

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'articleBudgetaireCode': codeABSelected,
            'codePeriodicite': codePeriodicite,
            'periodValueYear': selectAnnee_1.val(),
            'periodValueMonth': selectMois_1.val(),
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': typeSearch,
            'codeSite': $('#selectService_ ').val(),
            'operation': 'loadInvitationService'

        },
        beforeSend: function () {

            if (typeSearch == '0') {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
            } else {
                modalRechercheAvanceeByArticle.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
            }

        },
        success: function (response)
        {


            if (typeSearch == '0') {
                $.unblockUI();
            } else {
                modalRechercheAvanceeByArticle.unblock();
            }

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                switch (typeSearch) {
                    case '0':
                        printInvitationService('');
                        alertify.alert('Ce contribuable : ' + assujettiCodeValue.val() + ' n\'a pas d\'invitation de service.');
                        break;
                    case '1':
                        printInvitationService('');
                        alertify.alert('Aucune invitation de service ne correspond au critère de recherche fournis.');
                        break;
                }

            } else {
                medList = JSON.parse(JSON.stringify(response));
                modalRechercheAvanceeByArticle.modal('hide');
                printInvitationService(medList);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (typeSearch == '0') {
                $.unblockUI();
            } else {
                modalRechercheAvanceeByArticle.unblock();
            }
            showResponseError();
        }
    });
}

function printDefaillantPaiement(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:left">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left">NOTE DE PERCEPTION</th>';
    tableContent += '<th style="text-align:center">DATE ORDO.</th>';
    tableContent += '<th style="text-align:center">DATE ECHEANCE</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var amount = formatNumber(result[i].amountNp, result[i].deviseNp);
        var buttonPrintMep = '';

        switch (result[i].printExist) {
            case '0':
                buttonPrintMep = '<button class="btn btn-warning" onclick="callPrintAndSaveMep(\'' + result[i].numeroNp
                        + '\',\'' + result[i].assujettiCode + '\',\'' + result[i].assujettiName + '\',\'' + result[i].adresseCode
                        + '\',\'' + result[i].dateOrdonnancement
                        + '\',\'' + result[i].exerciceNp
                        + '\',\'' + result[i].printExist + '\',\'' + result[i].dateEcheanceNp
                        + '\',\'' + result[i].numeroMed + '\',\'' + amount + '\',\'' + result[i].numeroNc + '\',\'' + result[i].amountNp + '\')"><i class="fa fa-print"></i></button>';
                break;
            case '1':
                buttonPrintMep = '<button class="btn btn-warning" onclick="printDocument(\'' + result[i].numeroMed + '\')"><i class="fa fa-list"></i></button>';
                break;
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:center;width:8%;vertical-align:middle">' + result[i].exerciceNp + '</td>';
        tableContent += '<td style="text-align:left;width:30%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:9%;vertical-align:middle">' + result[i].numeroNp + '</td>';
        tableContent += '<td style="text-align:center;width:9%;vertical-align:middle">' + result[i].dateOrdonnancement + '</td>';
        tableContent += '<td style="text-align:center;width:9%;color:red;vertical-align:middle;font-weight:bold">' + result[i].dateEcheanceNp + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].articleBudgetaireName.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold">' + amount + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonPrintMep + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableDefaillantPaiement.html(tableContent);
    tableDefaillantPaiement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 5,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 5
    });

}

function printInvitationService(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">NUMERO</th>';
    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:left">CONTRIBUABLE</th>';
    tableContent += '<th style="text-align:left">BIEN</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:left">ECHEANCE DOCUMENT</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';


    var sumCDF = 0;
    var sumUSD = 0;

    for (var i = 0; i < result.length; i++) {

        var btnGenerateNoteTaxationPenalite = '';

        var InfoTaxationPenalite = '';

        if (result[i].relanceExist == '0' && result[i].ntPenaliteExiste == '0') {
            InfoTaxationPenalite = '<hr/>' + 'NOTES TAXATION EXISTE : ' + '<span style="font-weight:bold">' + 'NON' + '</span>';
            btnGenerateNoteTaxationPenalite = '<br/><button class="btn btn-primary" onclick="callModalGenerateNoteTaxationPenalite(\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')"><i class="fa fa-check-circle"></i>&nbsp;Générer Notes taxations</button>'

        }

        if (result[i].ntPenaliteExiste == '1') {
            InfoTaxationPenalite = '<hr/>' + 'NOTES TAXATION EXISTE : ' + '<span style="font-weight:bold">' + 'OUI' + '</span>';
        }



        var buttonPrintRelance = '';
        var txtBtnRelance = 'Génerer une relance';

        if (result[i].nextStap == '1') {

            buttonPrintRelance = '<button class="btn btn-success" onclick="callPrintAndSaveRelance(\'' + result[i].periodeDeclarationId
                    + '\',\'' + result[i].assujettiCode
                    + '\',\'' + result[i].assujettiName + '\',\'' + result[i].adresseCode
                    + '\',\'' + result[i].adresseName + '\',\'' + result[i].echeanceDeclaration
                    + '\',\'' + result[i].articleBudgetaireCode + '\',\'' + result[i].articleBudgetaireName
                    + '\',\'' + result[i].amountPeriodeDeclaration + '\',\'' + result[i].printExist
                    + '\',\'' + result[i].periodeDeclaration
                    + '\',\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')">' + txtBtnRelance + ' <i class="fa fa-check-circle"></i></button>';

        }

        if (result[i].relanceExist == '1') {
            var txtBtnRelance = 'Ré-imprimer la relance';
            buttonPrintRelance = '<button class="btn btn-warning" onclick="printDocumentRelance(\'' + result[i].numeroMedRelance + '\',\'' + result[i].numeroDocument + '\')">' + txtBtnRelance + ' <i class="fa fa-print"></i></button>';
        }

        var statePaymentPrincipale = '';
        var statePaymentPenalite = '';

        switch (result[i].estPayer) {
            case '1':
                statePaymentPrincipale = '<br/>' + '<span style="color:green">' + 'payé'.toUpperCase() + '</span>';
                statePaymentPenalite = '<br/>' + '<span style="color:green">' + 'payé'.toUpperCase() + '</span>';
                break;
            case '-1':
                statePaymentPrincipale = '<br/>' + '<span style="color:green">' + 'payé'.toUpperCase() + '</span>';
                statePaymentPenalite = '<br/>' + '<span style="color:green">' + 'non payé'.toUpperCase() + '</span>';
                break;
            case '0':
                statePaymentPrincipale = '<br/>' + '<span style="color:green">' + 'non payé'.toUpperCase() + '</span>';
                statePaymentPenalite = '<br/>' + '<span style="color:green">' + 'non payé'.toUpperCase() + '</span>';
                break;
        }


        var textBtn = '';

        if (result[i].accuserExist === '1') {
            textBtn = 'Ré-imprimer IS';
        } else {
            textBtn = 'Accuser réception';
        }

        var color = '';
        if (result[i].nextStap === '1') {
            color = 'red';
        } else {
            color = 'green';
        }



        var buttonPrintInvitationService = '<button class="btn btn-default" onclick="accuserReception(\'' + result[i].accuserExist + '\',\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')">' + textBtn + ' <i class="fa fa-print"></i></button>';

        var btnOperation = buttonPrintInvitationService + '<br/><br/>' + buttonPrintRelance + '<br/><br/>' + btnGenerateNoteTaxationPenalite;

        var receptionInfo = 'DATE RECEPTION : ' + '<span style="font-weight:bold">' + result[i].receptionIS + '</span>';
        var echeanceInfo = 'DATE ECHEANCE : ' + '<span style="font-weight:bold;color:' + color + '">' + result[i].echeanceIS + '</span>';

        var dateDocumentInfo = receptionInfo + '<hr/>' + echeanceInfo;

        if (result[i].isImmobilier === '1') {

            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + result[i].usageName + '</span>';
            var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + result[i].communeName + '</span>';
            var quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + result[i].quartiereName + '</span>';

            var bienInfo = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';

            var adresseInfo = result[i].adresseBien.toUpperCase();

            descriptionBien = bienInfo + '<br/><br/>' + natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<hr/>' + adresseInfo;

        } else {
            var bienInfo2 = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';
            var natureInfo2 = 'Genre : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
            var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
            var adresseInfo2 = result[i].adresseBien.toUpperCase();

            descriptionBien = bienInfo2 + '<br/><br/>' + natureInfo2 + '<br/>' + categorieInfo2 + '<hr/>' + adresseInfo2;
        }

        //var principalDuInfo = 'PRINCIPAL DÛ : ' + '<span style="font-weight:bold;color:green">' + formatNumber(result[i].amountPeriodeDeclaration, result[i].devisePeriodeDeclaration) + statePaymentPrincipale + '</span>';
        var principalDuInfo = 'PRINCIPAL DÛ : ' + '<span style="font-weight:bold;color:green">' + formatNumber(result[i].amountPeriodeDeclaration, result[i].devisePeriodeDeclaration) + statePaymentPrincipale + '</span>';
        var penaliteDuInfo = 'PENALITE DÛ : ' + '<span style="font-weight:bold;color:red">' + formatNumber(result[i].penaliteDu, result[i].devisePeriodeDeclaration) + statePaymentPenalite + '</span>';
        //var penaliteDuInfo = 'PENALITE DÛ : ' + '<span style="font-weight:bold;color:red">' + formatNumber(result[i].penaliteDu, result[i].devisePeriodeDeclaration) + statePaymentPenalite + '</span>';
        var amountDuInfo = principalDuInfo + '<hr/>' + penaliteDuInfo;

        if (result[i].devisePeriodeDeclaration === 'CDF') {
            sumCDF += (result[i].amountPeriodeDeclaration + result[i].amountPenaliteMed);
        } else if (result[i].devisePeriodeDeclaration === 'USD') {
            sumUSD += (result[i].amountPeriodeDeclaration + result[i].amountPenaliteMed);
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + result[i].numeroDocument + InfoTaxationPenalite + '</td>';
        tableContent += '<td style="text-align:center;width:8%;vertical-align:middle">' + result[i].periodeDeclaration + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + descriptionBien + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].articleBudgetaireName.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + amountDuInfo + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + dateDocumentInfo + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + btnOperation + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="5" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';


    tableDefaillants.html(tableContent);
    tableDefaillants.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 25,
        dom: 'Bfrtip', columnDefs: [
            {"visible": false, "targets": 4}
        ], order: [[4, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(4, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        },
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(5).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD'));
        }
    });

}

function callPrintAndSaveMep(numeroNp, assujettiCode, assujettiName, adresseCode, dateOrdonnancement,
        exerciceNp, printExist, dateEcheanceNp, numeroMed, amountNp1, numeroNc, amountNp2) {


    alertify.confirm('Etes-vous sûre de vouloir imprimer une mise en demeure pour cette note de perception ? ', function () {

        mepObj = new Object();

        mepObj.numeroNp = numeroNp;
        mepObj.assujettiCode = assujettiCode;
        mepObj.assujettiName = assujettiName;
        mepObj.adresseCode = adresseCode;
        mepObj.dateOrdonnancement = dateOrdonnancement;
        mepObj.exerciceNp = exerciceNp;
        mepObj.printExist = printExist;
        mepObj.dateEcheanceNp = dateEcheanceNp;
        mepObj.numeroMed = numeroMed;
        mepObj.amountNp1 = amountNp1;
        mepObj.amountNp2 = amountNp2;
        mepObj.numeroNc = numeroNc;

        saveAndPrintMep(mepObj);
    });
}

function callPrintAndSaveRelance(periodeID, assujettiCode, assujettiName,
        adresseCode, adresseName, echeanceDeclaration, articleBudgetaireCode,
        articleBudgetaireName, amountPeriodeDeclaration, printExist, periodeDeclaration, numeroMed, numeroDocument) {

    var value = '<span style="font-weight:bold">' + numeroDocument + '</span>';
    alertify.confirm('Etes-vous sûre de vouloir générer la relance pour cette invitation de service n° ' + value + ' ?', function () {

        medObj = new Object();

        medObj.periodeDeclarationId = periodeID;
        medObj.exercice = periodeDeclaration;
        medObj.assujettiCode = assujettiCode;
        medObj.assujettiName = assujettiName;
        medObj.adresseCode = adresseCode;
        medObj.adresseName = adresseName;
        medObj.echeance = echeanceDeclaration;
        medObj.articleBudgetaireCode = articleBudgetaireCode;
        medObj.articleBudgetaireName = articleBudgetaireName;
        medObj.amountPeriodeDeclaration = amountPeriodeDeclaration;
        medObj.printExist = printExist;
        medObj.numeroMed = numeroMed;

        saveAndPrintRelance(medObj);
    });

}


function getSelectedAssujetiData() {
    assujettiCodeValue.val(selectAssujettiData.nomComplet);
    assujettiCodeValue.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;
    isAvancedSearch = '0';

    switch (getRegisterType()) {
        case 'SERVICE':
        case 'INVITATION_SERVICE':
            loadDefaillantDeclaration(isAvancedSearch);
            break;
        case 'MEP':
            loadMep(isAvancedSearch);
            break;
    }

}

function loadDefaillantPaiement(typeSearch) {


    switch (typeSearch) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            lblSite.text($('#selectService option:selected').text().toUpperCase());
            lblSite.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblService.text($('#selectService option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblDateDebut.text(dateDebuts);
            lblDateDebut.attr('title', dateDebuts);

            lblDateFin.text(dateFins);
            lblDateFin.attr('title', dateFins);

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': codeService,
            'codeSite': codeSite,
            'dateDebut': dateDebuts,
            'dateFin': dateFins,
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': typeSearch,
            'operation': 'loadDefaillantPaiement'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                printDefaillantPaiement('');
                alertify.alert('Aucune note de perception en retard de paiement.');
            } else {
                mepList = JSON.parse(JSON.stringify(response));
                printDefaillantPaiement(mepList);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function saveAndPrintRelance(medObj) {


    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'periodeDeclarationId': medObj.periodeDeclarationId,
            'articleBudgetaireCode': medObj.articleBudgetaireCode,
            'assujettiCode': medObj.assujettiCode,
            'echeanceDeclaration': medObj.echeance,
            'periodeDeclaration': medObj.exercice,
            'adresseCode': medObj.adresseCode,
            'articleBudgetaireName': medObj.articleBudgetaireName,
            'assujettiName': medObj.assujettiName,
            'adresseName': medObj.adresseName,
            'amountPeriodeDeclarationToString': medObj.amountPeriodeDeclarationToString,
            'amountPeriodeDeclaration': medObj.amountPeriodeDeclaration,
            'idUser': userData.idUser,
            'printExist': medObj.printExist,
            'numeroMed': medObj.numeroMed,
            'operation': 'printRelance'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else {

                setTimeout(function () {

                    $.unblockUI();
                    setDocumentContent(response);

                    setTimeout(function () {
                    }, 2000);

                    loadDefaillantDeclaration(isAvancedSearch);

                    window.open('visualisation-document', '_blank');
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function saveAndPrintMep(mepObj) {

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'numeroNp': mepObj.numeroNp,
            'assujettiCode': mepObj.assujettiCode,
            'assujettiName': mepObj.assujettiName,
            'adresseCode': mepObj.adresseCode,
            'dateOrdonnancement': mepObj.dateOrdonnancement,
            'articleBudgetaireCode': mepObj.articleBudgetaireCode,
            'exerciceNp': mepObj.exerciceNp,
            'printExist': mepObj.printExist,
            'dateEcheanceNp': mepObj.dateEcheanceNp,
            'numeroMed': mepObj.numeroMed,
            'amountNp1': mepObj.amountNp1,
            'amountNp2': mepObj.amountNp2,
            'numeroNc': mepObj.numeroNc,
            'idUser': userData.idUser,
            'operation': 'printMep'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else {

                setTimeout(function () {

                    $.unblockUI();
                    setDocumentContent(response);

                    setTimeout(function () {
                    }, 2000);

                    loadMep(isAvancedSearch);

                    window.open('visualisation-document', '_blank');
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function printDocumentRelance(medId, numeroReference) {

    var value = '<span style="font-weight:bold">' + numeroReference + '</span>';

    alertify.confirm('Etes-vous sûr de vouloir ré-imprimer la relance pour l\'invitation de service n° ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'numero': medId,
                'operation': 'printNoteTaxation'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    $.unblockUI();
                    alertify.alert('Aucune relance trouvée correspondant à cette invitation de service : ' + value);
                    return;
                }

                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });
    });
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function printMedOrMep(document) {

    var msg = '';

    switch (getRegisterType()) {
        case 'SERVICE':
            msg = 'Etes-vous sûre de vouloir imprimer une relance ? ';
            break;
        case 'MEP':
            msg = 'Etes-vous sûre de vouloir imprimer une mise en demeure au paiement ? ';
            break;
    }

    alertify.confirm(msg, function () {

        setTimeout(function () {
            setDocumentContent(document);

            setTimeout(function () {
            }, 2000);
            window.open('visualisation-document', '_blank');
        }
        , 1);
    });

}

function refrechDataAfterAccuserReception() {

    switch (typeMed) {
        case 'SERVICE':
        case 'INVITATION_SERVICE':
            loadDefaillantDeclaration(isAvancedSearch);
            break;
        case 'MEP':
            loadMep(isAvancedSearch);
            break;
    }
}

function getDefaillantPaiement() {

    var date = new Date();
    var day = dateFormat(date.getDate());
    var month = dateFormat(date.getMonth() + 1);
    var year = date.getFullYear();

    var dateDuJour = day + "-" + month + "-" + year;

    dateDebuts = dateDuJour;
    dateFins = dateDuJour;
    codeService = userData.serviceCode;
    codeSite = userData.SiteCode;

    loadDefaillantPaiement(1);

}

function loadMep(typeSearch) {

    switch (typeSearch) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            lblSite.text($('#selectService option:selected').text().toUpperCase());
            lblSite.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblService.text($('#selectService option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblDateDebut.text(inputDateDebut.val());
            lblDateDebut.attr('title', inputDateDebut.val());

            lblDateFin.text(inputdateLast.val());
            lblDateFin.attr('title', inputdateLast.val());

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': selectService.val(),
            'codeSite': selectSite.val(),
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': typeSearch,
            'operation': 'loadDefaillantPaiement'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                switch (typeSearch) {
                    case '0':
                        printDefaillantPaiement('');
                        alertify.alert('Cet assujetti : ' + assujettiCodeValue.val() + ' n\'a pas de notes de perception en retard de paiement.');
                        //return;
                        break;
                    case '1':
                        printDefaillantPaiement('');
                        alertify.alert('Aucune note de perception en retard de paiement ne corresponde au critère de recherche fournis.');
                        //return;
                        break;
                }


            } else {
                mepList = JSON.parse(JSON.stringify(response));
                modalRechercheAvanceeModelTwo.modal('hide');
                printDefaillantPaiement(mepList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function accuserReception(accuserExist, medID, numeroDocument) {

    for (var i = 0; i < medList.length; i++) {

        if (medList[i].numeroMed === medID) {
            codeMedCurrent = medList[i].numeroMed;
            typeMed = 'INVITATION_SERVICE';
            codeTypeDoc = typeMed;
        }
    }

    if (accuserExist == '0') {

        initAccuseReceptionUI(codeMedCurrent, typeMed);
        modalAccuserReception.modal('show');

    } else {
        var value = '<span style= "font-weight: bold">' + numeroDocument + '</span>';

        var msg = 'Etes-vous sûre de vouloir réimprimer l\'invitation de service n° ' + value + ' ?';

        alertify.confirm(msg, function () {

            $.ajax({
                type: 'POST',
                url: 'declaration_servlet',
                dataType: 'text',
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                crossDomain: true,
                data: {
                    'numero': medID,
                    'operation': 'printNoteTaxation'
                },
                beforeSend: function () {

                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

                },
                success: function (response)
                {

                    $.unblockUI();

                    if (response == '-1') {
                        $.unblockUI();
                        showResponseError();
                        return;
                    }

                    if (response == '0') {
                        $.unblockUI();
                        alertify.alert('Aucune invitation de service trouvée correspondant à ce numéro : ' + value);
                        return;
                    }

                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');
                },
                complete: function () {

                },
                error: function (xhr, status, error) {
                    $.unblockUI();
                    showResponseError();
                }

            });
        });
    }
}

function callModalGenerateNoteTaxationPenalite(med, numeroDocument) {

    var value = '<span style="font-weight:bold">' + numeroDocument + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir générer les notes de principale et de pénalité pour cette invitation de service n° ' + value + ' ?', function () {

        medSelected = med;

        var dataBanque = '';
        dataBanque += '<option value="0">--</option>';

        for (var i = 0; i < banqueList.length; i++) {
            dataBanque += '<option value="' + banqueList[i].codeBanque + '">' + banqueList[i].libelleBanque + '</option>';

        }

        cmbBanqueNoteTation.html(dataBanque);

        modalGenerateNoteTaxationOfInvitationService.modal('show');
    });
}


function generateNoteTaxationPrincipalAndPenalite() {

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'Json',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'userId': userData.idUser,
            'numeroMed': medSelected,
            'codeBanque': cmbBanqueNoteTation.val(),
            'codeCompteBancaire': cmbCompteBancaireBanqueNoteTation.val(),
            'codeCompteBancairePenalite': cmbCompteBancaireBanqueNoteTationPenalite.val(),
            'operation': 'saveNoteTaxationPrincipalAndPenalite'

        },
        beforeSend: function () {
            modalGenerateNoteTaxationOfInvitationService.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Génération des notes de taxation en cours ...</h5>'});
        },
        success: function (response)
        {

            modalGenerateNoteTaxationOfInvitationService.unblock();

            if (response == '-1' | response == '0') {

                showResponseError();
                return;

            } else {

                var result = JSON.parse(JSON.stringify(response));

                setDocumentContent(result.ntPenalitePrincipal);
                window.open('visualisation-document', '_blank');

                setDocumentContent(result.ntPenalitePenalite);
                window.open('visualisation-document', '_blank');

                modalGenerateNoteTaxationOfInvitationService.modal('hide');

                loadDefaillantDeclaration(isAvancedSearch);

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            modalGenerateNoteTaxationOfInvitationService.unblock();
            showResponseError();
        }

    });
}

function initDataBank() {

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'initDataBank'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            banqueList = JSON.parse(JSON.stringify(response));

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}
