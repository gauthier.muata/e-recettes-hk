/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var typeResearchRegistreRetraitDeclaration;

var responsibleObject = new Object();

var inputResearchRetraitDeclar;

var btnResearchRetraitDeclaration, btnAdvencedSearch;

var btnCallModalSearchAvandedRetraitDeclaration;

var btnSelectArticleBudgetaire;

var articleBudgetaireModal;

var tableRegistreRetraitDeclaration;

var listRetraitDeclaration;

var chkbFilterTeleDeclaration, loadOnlyDeclarationOnline;

var assujettiModal;

var isAdvance, lblSite, lblService, labelType, lblDateDebut, lblDateFin;

var modalReviewAmountPenalite, spnAmountPenaliteInit, cmbTauxRemise, spnAmountPenaliteRemise, btnConfirmeRemisePenalite, spnMonthLate, inputObservationRemiseTaux;
var isAdvanced;
var onClickBtndvancedSearch;
var tempDefaillantPaiementNoteTaxationList = [];

$(function () {

    loadOnlyDeclarationOnline = '0';
    onClickBtndvancedSearch = '0';

    mainNavigationLabel.text('RECOUVREMENT');
    secondNavigationLabel.text('Registre des défaillants au paiement des notes de taxation');

    removeActiveMenu();
    linkSubMenuRegistreRetraitDeclaration.addClass('active');


    tableRegistreRetraitDeclaration = $('#tableRegistreRetraitDeclaration');

    btnCallModalSearchAvandedRetraitDeclaration = $('#btnCallModalSearchAvandedRetraitDeclaration');

    btnSelectArticleBudgetaire = $('#btnSelectArticleBudgetaire');
    articleBudgetaireModal = $('#articleBudgetaireModal');

    btnResearchRetraitDeclaration = $('#btnResearchRetraitDeclaration');
    btnAdvencedSearch = $('#btnAdvencedSearch');

    inputResearchRetraitDeclar = $('#inputResearchRetraitDeclar');

    typeResearchRegistreRetraitDeclaration = $('#typeResearchRegistreRetraitDeclaration');
    chkbFilterTeleDeclaration = $('#chkbFilterTeleDeclaration');
    assujettiModal = $('#assujettiModal');

    isAdvance = $('#isAdvance');
    lblSite = $('#lblSite');
    lblService = $('#lblService');
    labelType = $('#labelType');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    modalReviewAmountPenalite = $('#modalReviewAmountPenalite');
    spnAmountPenaliteInit = $('#spnAmountPenaliteInit');
    cmbTauxRemise = $('#cmbTauxRemise');
    spnAmountPenaliteRemise = $('#spnAmountPenaliteRemise');
    btnConfirmeRemisePenalite = $('#btnConfirmeRemisePenalite');
    spnMonthLate = $('#spnMonthLate');
    inputObservationRemiseTaux = $('#inputObservationRemiseTaux');

    btnCallModalSearchAvandedRetraitDeclaration.on('click', function (e) {
        e.preventDefault();
        onClickBtndvancedSearch = '1';
        modalRechercheAvanceeNC.modal('show');
    });


    btnResearchRetraitDeclaration.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAdvance.attr('style', 'display: none');
        isAdvanced = '0';

        switch (typeResearchRegistreRetraitDeclaration.val()) {
            case '1':
                assujettiModal.modal('show');
                break;
            case '2':

                if (inputResearchRetraitDeclar.val() === empty) {
                    alertify.alert('Veuillez fournir le numéro de la déclaration avant de lancer la recherche');
                    return;
                } else {
                    loadRetraitDeclaration();
                }

                break;
        }


    });

    typeResearchRegistreRetraitDeclaration.on('change', function (e) {

        inputResearchRetraitDeclar.val(empty);

        switch (typeResearchRegistreRetraitDeclaration.val()) {
            case '1':
                inputResearchRetraitDeclar.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
                inputResearchRetraitDeclar.attr('readonly', true);
                inputResearchRetraitDeclar.attr('style', 'font-weight:normal;width: 380px');
                break;
            case '2':
                inputResearchRetraitDeclar.attr('readonly', false);
                inputResearchRetraitDeclar.attr('placeholder', 'Veuillez saisir le numéro déclaration');
                inputResearchRetraitDeclar.attr('style', 'font-weight:normal;width: 380px');
                break;
        }
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAdvanced = '1';
        isAdvance.attr('style', 'display: block');
        declarationByAdvancedSearch();
    })

    loadRetraitDeclarationTable('');

    chkbFilterTeleDeclaration.attr('style', 'display: inline');

    //btnAdvencedSearch.trigger('click');

    $('.rapport-defaillant-paiement-nt').click(function (e) {
        e.preventDefault();
        alertify.confirm('Etes-vous sûre de vouloir imprimer le rapport des défaillants de paiement de note de taxation ?', function () {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            if (tempDefaillantPaiementNoteTaxationList.length != 0) {
                printDataDefaillantPaiementNT();
            } else {
                alertify.alert('Aucune donnée n\'est disponnible pour imprimer ce rapport.');
            }
        });
    })

});

//chkbFilterTeleDeclaration

function handleClick(cb) {

    switch (cb.checked) {
        case true:
            loadOnlyDeclarationOnline = '1';
            break;
        case false:
            loadOnlyDeclarationOnline = '0';
            break;
    }
}

function loadRetraitDeclaration() {

    if (inputDateDebut.val() === '') {
        alertify.alert('Veuillez d\'abord indiquer la date de début de la recherche.');
        return;
    }

    if (inputdateLast.val() === '') {
        alertify.alert('Veuillez d\'abord indiquer la date fin de la recherche.');
        return;
    }

    //var viewAllService = controlAccess('VIEW_ALL_SERVICES_NC');
    var viewAllService = controlAccess('SEARCH_FOR_ALL_SERVICES');
    var viewAllSite = controlAccess('VIEW_ALL_SITE');

    registerType = getRegisterType();

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': typeResearchRegistreRetraitDeclaration.val() === '1' ? responsibleObject.codeResponsible : inputResearchRetraitDeclar.val(),
            'typeSearch': typeResearchRegistreRetraitDeclaration.val(),
            'typeRegister': registerType,
            'allService': viewAllService,
            'codeSite': viewAllSite == true ? '*' : userData.SiteCode,
            'codeService': viewAllService == true ? '*' : userData.serviceCode,
            'userId': userData.idUser,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'operation': 'searchRetraitDeclarationDefaillant'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'})
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    alertify.alert('Aucune donnée ne corresponde au critère de recherche fournie.');
                    return;
                } else {
                    listRetraitDeclaration = null;
                    listRetraitDeclaration = JSON.parse(JSON.stringify(response));
                    loadRetraitDeclarationTable(listRetraitDeclaration);
                }
            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getSelectedAssujetiData() {

    inputResearchRetraitDeclar.val(selectAssujettiData.nomComplet);
    inputResearchRetraitDeclar.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    loadRetraitDeclaration();

}

function loadRetraitDeclarationTable(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    //tableContent += '<th style="text-align:left;display:none"scope="col">Déclaration</th>';
    tableContent += '<th style="text-align:left">NOTE TAXATION</th>';
    tableContent += '<th style="text-align:left"scope="col">BIEN</th>';
    tableContent += '<th style="text-align:left"scope="col">CONTRIBUABLE</th>';
    tableContent += '<th style="text-align:left"scope="col">IMPÔT</th>';
    tableContent += '<th style="text-align:left"scope="col">DATE RETRAIT</th>';
    tableContent += '<th style="text-align:left"scope="col">ECHEANCE PAIEMENT</th>';
    tableContent += '<th style="text-align:right"scope="col">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:left"scope="col">INFOS BANQUE</th>';
    tableContent += '<th style="text-align:center">INVITATION EXISTE ?</th>';
    tableContent += '<th style="text-align:left;width:8%"scope="col"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var sumCDF = 0;
    var sumUSD = 0;

    for (var i = 0; i < result.length; i++) {

        var firstLineAB = '';

        if (result[i].libelleArticleBudgetaire.length > 250) {
            firstLineAB = result[i].libelleArticleBudgetaire.substring(0, 250).toUpperCase() + ' ...';
        } else {
            firstLineAB = result[i].libelleArticleBudgetaire.toUpperCase();
        }

        var buttonOperation = '';
        var descriptionBien = empty;

        var btnPrintNoteTaxation = '&nbsp;<button class="btn btn-warning" title="Cliquez ici pour réimprimer la note de taxation" onclick="printNoteTaxation(\'' + result[i].numeroDepotDeclaration + '\')"><i class="fa fa-print"></i>&nbsp;R&eacute;imprimer la note</button>';
        var btnCallModalDsiplayRemise = '';

        var bienTitle = '';
        if (result[i].isImmobilier == '1') {

            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + result[i].usageName + '</span>';
            var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + result[i].communeName + '</span>';
            var quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + result[i].quartierName + '</span>';

            var bienInfo = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';

            var adresseInfo = result[i].adresseBien.toUpperCase();
            bienTitle = result[i].intituleBien + '\n' + 'Nature : ' + result[i].libelleTypeBien + '\n' + 'Usage : ' + result[i].usageName + '\n' + 'Catégorie : ' + result[i].tarifName + '\n' + 'Commune : ' + result[i].communeName + '\n' + adresseInfo;
            descriptionBien = bienInfo + '<br/><br/>' + natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<hr/>' + adresseInfo;


        } else {
            var bienInfo2 = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';
            var natureInfo2 = 'Genre : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
            var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
            var adresseInfo2 = '';

            if (!isUndefined(result[i].adresseBien)) {
                adresseInfo2 = '<hr/>' + result[i].adresseBien.toUpperCase();
            }

            descriptionBien = bienInfo2 + '<br/><br/>' + natureInfo2 + '<br/>' + categorieInfo2 + adresseInfo2;
            bienTitle = result[i].descriptionBien;
        }

        var assujettiInfo = '<span style="font-weight:bold">' + result[i].nom + '</span>';

        var numeroDeclarationInfo = '<span style="font-weight:bold">' + result[i].numeroDepotDeclaration + '</span>';

        var color = 'green';

        var btnPrintInvitationPaiement = '';

        var valueCheck = 'checked';

        if (result[i].medExist == '1') {
            btnPrintInvitationPaiement = '<br/><br/><button class="btn btn-success" title="Cliquez ici pour imprimer l\'invitation à payer" onclick="printInvitationPaiement(\'' + result[i].numeroMed + '\',\'' + result[i].medId + '\')"><i class="fa fa-print"></i>&nbsp;Imprimer invitation</button>';
        } else {
            valueCheck = '';
            btnPrintInvitationPaiement = '<br/><br/><button class="btn btn-success" title="Cliquez ici pour créer et imprimer l\'invitation à payer" onclick="savePrintInvitationPaiement(\'' + result[i].idRetraitDeclaration + '\',\'' + result[i].numeroDepotDeclaration + '\')"><i class="fa fa-check-circle"></i>&nbsp;Lancer invitation</button>';
        }

        var siteNameInfo = '';

        if (result[i].siteName !== '') {
            siteNameInfo = '<br/><br/>' + 'ENTITE : ' + '<span style="font-weight:bold">' + result[i].siteName + '</span>';
        }

        if (result[i].echeanceDepasse == '1') {
            color = 'red';
        }

        buttonOperation = btnPrintNoteTaxation + btnCallModalDsiplayRemise + btnPrintInvitationPaiement;

        var statePayment = '', etatPay = '';

        if (result[i].bordereauExist == '1') {

            var bordereauDate = '<br/>' + '(Payer le : ' + '<span style="color:black">' + result[i].bordereauDate + '</span>' + ')';

            if (result[i].payerHorsEcheance == '1') {
                statePayment = '<hr/>' + '<span style="color:orange">' + 'payé hors échéance'.toUpperCase() + '</span>' + bordereauDate;
                etatPay = '\nPayé hors échéance'.toUpperCase();
            }

        } else {
            statePayment = '<hr/>' + '<span style="color:red">' + 'non payé'.toUpperCase() + '</span>';
            etatPay = '\nNon payé'.toUpperCase();
        }

        var bank = 'Banque : ' + '<span style="font-weight:bold">' + result[i].banqueName + '</span>';
        var accountBank = 'Compte bancaire : ' + '<br/><br/><span style="font-weight:bold">' + result[i].compteBancaireName + '</span>';

        var bankInfo = bank + '<br/><br/>' + accountBank;

        var data = new Object();
        data.numeroDeclarationInfo = result[i].numeroDeclarationInfo;
        data.bien = result[i].bienTitle;
        data.libelleArticleBudgetaire = result[i].libelleArticleBudgetaire + ' Période : ' + result[i].IntitulePeriodicite;
        data.dateCreate = result[i].dateCreate;
        data.dateEcheance = result[i].dateEcheance;
        data.amount = result[i].montantPercu + ' ' + result[i].devise + etatPay;
        data.banque = 'Banque : ' + result[i].banqueName + ' Compte bancaire : ' + result[i].compteBancaireName;
        tempDefaillantPaiementNoteTaxationList.push(data);

        if (result[i].devise === 'CDF') {
            sumCDF += result[i].montantPercu;
        } else if (result[i].devise === 'USD') {
            sumUSD += result[i].montantPercu;
        }

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;width:8%;">' + numeroDeclarationInfo + '</td>';
        tableContent += '<td style="vertical-align:middle;width:20%">' + descriptionBien + '</td>';
        tableContent += '<td style="vertical-align:middle;width:16%">' + assujettiInfo + '</td>';
        tableContent += '<td style="vertical-align:middle;width:11%"><span style="font-weight:bold;"></span>' + firstLineAB + ' <hr/>' + 'Période : ' + '<span style="font-weight:bold">' + result[i].IntitulePeriodicite.toUpperCase() + siteNameInfo + '</span>' + '</td>'
        tableContent += '<td style="vertical-align:middle;width:8%">' + result[i].dateCreate + '</td>';
        tableContent += '<td style="vertical-align:middle;width:8%;font-weight:bold;color:' + color + '">' + result[i].dateEcheance + '</td>';
        tableContent += '<td style="vertical-align:middle;width:8%;text-align:right;font-weight:bold">' + formatNumber(result[i].montantPercu, result[i].devise) + statePayment + '</td>';
        tableContent += '<td style="vertical-align:middle;width:12%;text-align:left">' + bankInfo + '</td>';

        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" id="checkboxRelance" disabled ' + valueCheck + '></td>';

        tableContent += '<td style="vertical-align:middle;width:7%;text-align:center">' + buttonOperation + '</td>';

        tableContent += '</tr>';
    }

    tableContent += '</tbody>';
    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="6" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableRegistreRetraitDeclaration.html(tableContent);
    tableRegistreRetraitDeclaration.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste des d&eacute;ffaillants ici ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: true,
        filter: false,
        pageLength: 50,
        columnDefs: [
            {"visible": false, "targets": 2}
        ],
        order: [[2, 'asc']],
        //,lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 25,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(6).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD'));
        },
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6" class="group"><td colspan="10">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

}

function callModalSavePayment(idRetrait, callNextStap) {
    alert('callModalSavePayment : ' + idRetrait);
}

function callModalDisplayDocument(idRetrait, callNextStap) {

    for (var i = 0; i < listRetraitDeclaration.length; i++) {

        if (listRetraitDeclaration[i].idRetraitDeclaration == idRetrait) {
            reprintRecepisse(listRetraitDeclaration[i].numeroDepotDeclaration);
        }
    }


}

function declarationByAdvancedSearch() {

    if (inputDateDebut.val() === '') {
        alertify.alert('Veuillez d\'abord indiquer la date de début de la recherche.');
        return;
    }

    if (inputdateLast.val() === '') {
        alertify.alert('Veuillez d\'abord indiquer la date fin de la recherche.');
        return;
    }
    registerType = getRegisterType();

    switch (selectSite.val()) {

        case '*':
            lblSite.text('TOUS');
            lblSite.attr('title', 'TOUS');
            break;
        default:
            lblSite.text($('#selectSite option:selected').text().toUpperCase());
            lblSite.attr('title', $('#selectSite option:selected').text().toUpperCase());
            break;
    }

    switch (selectService.val()) {

        case '*':
            lblService.text('TOUS');
            lblService.attr('title', 'TOUS');
            break;
        default:
            lblService.text($('#selectService option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService option:selected').text().toUpperCase());
            break;
    }

    if (loadOnlyDeclarationOnline === '0') {
        labelType.text('NORMALE');
        labelType.attr('title', 'NORMALE');
    } else {
        labelType.text('EN LIGNE');
        labelType.attr('title', 'EN LIGNE');
    }

    lblDateDebut.text(inputDateDebut.val());
    lblDateDebut.attr('title', inputDateDebut.val());

    lblDateFin.text(inputdateLast.val());
    lblDateFin.attr('title', inputdateLast.val());

    var viewAllService = controlAccess('SEARCH_FOR_ALL_SERVICES');
    var viewAllSite = controlAccess('VIEW_ALL_SITE');


    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'SiteCode': selectSite.val(),//viewAllSite == true ? '*' : selectSite.val(),
            'serviceCode': selectService.val(),//viewAllService == true ? '*' : selectService.val(),
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'declarationOnlineOnly': loadOnlyDeclarationOnline,
            'typeRegister': registerType,
            'codeAgent': userData.idUser,
            'operation': 'SearchAvancedRetraitDeclarationDefaillant'
        },
        beforeSend: function () {
            if (onClickBtndvancedSearch == '0') {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
            } else {
                modalRechercheAvanceeNC.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
            }
        },
        success: function (response)
        {
            setTimeout(function () {
                modalRechercheAvanceeNC.unblock();

                if (onClickBtndvancedSearch == '0') {
                    $.unblockUI();
                } else {
                    modalRechercheAvanceeNC.unblock();
                }

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    //modalRechercheAvanceeNC.modal('hide');
                    listRetraitDeclaration = '';
                    loadRetraitDeclarationTable('');
                    alertify.alert('Aucune donnée ne corresponde au critère de recherche fournie.');
                    return;
                } else {
                    modalRechercheAvanceeNC.modal('hide');
                    listRetraitDeclaration = null;
                    listRetraitDeclaration = JSON.parse(JSON.stringify(response));
                    loadRetraitDeclarationTable(listRetraitDeclaration);
                }
            });
        },
        complete: function () {
        },
        error: function () {
            if (onClickBtndvancedSearch == '0') {
                $.unblockUI();
            } else {
                modalRechercheAvanceeNC.unblock();
            }
            showResponseError();
        }

    });
}

function reprintRecepisse(declaration) {

    if (declaration == '') {
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codeDepotDeclaration': declaration,
            'operation': 'reprintRecepisse'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            if (response == '0') {
                $.unblockUI();
                alertify.alert('Document non trouvé.');
                return;
            }

            setTimeout(function () {
                $.unblockUI();
                setDocumentContent(response);
                window.open('visualisation-document', '_blank');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printNoteTaxation(numero) {
    var value = '<span style="font-weight:bold">' + numero + "</span>";
    alertify.confirm('Etes-vous sûre de vouloir imprimer la note de taxation n° ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'numero': numero,
                'typeDoc': 'NT2',
                'operation': 'printNoteTaxation'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

            },
            success: function (response)
            {

                if (response == '-1') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    $.unblockUI();
                    alertify.alert('Aucune note de taxation trouvée correspondant à ce numéro : ' + numero);
                    return;
                }

                setTimeout(function () {
                    $.unblockUI();
                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');

                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });

    });

}

function savePrintInvitationPaiement(id, numero) {

    var valueNumero = '<span style="font-weight:bold">' + numero + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir imprimer l\'invitation à payer pour la note de taxation n° ' + valueNumero + ' ?', function () {
        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'id': id,
                'idUser': userData.idUser,
                'noteTaxation': numero,
                'operation': 'saveInvitationApayer'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Génération de l\'invitation en cours ...</h5>'});

            },
            success: function (response)
            {

                if (response == '-1' | response == '0') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                setTimeout(function () {
                    $.unblockUI();
                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');

                }
                , 1);

                if (isAdvanced == '1') {
                    btnAdvencedSearch.trigger('click');
                } else {
                    btnResearchRetraitDeclaration.trigger('click');
                }
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });
    });

}

function callModalDsiplayRemise(amountRemise, tauxRemise, observationRemise, devise, amount, moisRetard) {

    spnAmountPenaliteInit.html(formatNumber(amount, devise));
    cmbTauxRemise.val(tauxRemise);
    cmbTauxRemise.attr('disabled', true);
    spnAmountPenaliteRemise.html(formatNumber(amountRemise, devise));

    btnConfirmeRemisePenalite.attr('style', 'display:none');
    spnMonthLate.html(moisRetard);
    inputObservationRemiseTaux.val(observationRemise);

    modalReviewAmountPenalite.modal('show');
}

function printInvitationPaiement(med, id) {


    var value = '<span style="font-weight:bold">' + med + '</span>';

    alertify.confirm('Etes-vous sûr de vouloir ré-imprimer la relance pour l\'invitation à payer n° ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'numero': id,
                'typeDoc': 'INVITATION_PAIEMENT',
                'operation': 'printNoteTaxation'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression de l\'invitation en cours ...</h5>'});

            },
            success: function (response)
            {

                if (response == '-1') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    $.unblockUI();
                    alertify.alert('Aucune relance trouvée correspondant à cette invitation à payer : ' + value);
                    return;
                }

                setTimeout(function () {
                    $.unblockUI();
                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');

                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });
    });


}

function printDataDefaillantPaiementNT() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    if (tempDefaillantPaiementNoteTaxationList.length > 0) {

        if (isAdvanced == 1) {
            docTitle = 'REGISTRE DES DEFAILLANTS AU PAIEMENT DE NOTE DE TAXATION';
            if (selectSite.val() == '*' && selectService.val() == '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 300;
            } else if (selectSite.val() !== '*' && selectService.val() == '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSITE : ' + $('#selectSite option:selected').text();
                position = 300;
                hauteur = 160;
            } else if (selectSite.val() == '*' && selectService.val() !== '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSERVICE : ' + $('#selectService option:selected').text();
                position = 300;
                hauteur = 160;
            } else {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSITE : ' + $('#selectSite option:selected').text();
                docTitle += '\nSERVICE : ' + $('#selectService option:selected').text();
                position = 300;
                hauteur = 170;
            }
        } else {
            var critere = assujettiCodeValue.val().toUpperCase();
            docTitle = 'REGISTRE DES DEFAILLANTS AU PAIEMENT DE NOTE DE TAXATION : ' + critere;
            position = 300;
        }

    } else {
        docTitle = 'REGISTRE DES DEFAILLANTS AU PAIEMENT DE NOTE DE TAXATION';
        position = 250;
    }

    var columns = [
        {title: "NOTE TAXATION", dataKey: "numeroDeclarationInfo"},
        {title: "BIEN", dataKey: "bien"},
        {title: "ARTICLE BUDGETAIRE", dataKey: "libelleArticleBudgetaire"},
        {title: "DATE RETRAIT", dataKey: "dateCreate"},
        {title: "ECHEANCE PAIEMENT", dataKey: "dateEcheance"},
        {title: "MONTANT DÛ", dataKey: "amount"},
        {title: "BANQUE", dataKey: "banque"}];

    var rows = tempDefaillantPaiementNoteTaxationList;
    var pageContent = function (data) {

        doc.setFontSize(10);
        doc.text("REPUBLIQUE DEMOCRATIQUE DU CONGO", 40, 25);
        doc.setFontSize(9);
        doc.text("PROVINCE DU HAUT-KATANGA", 40, 40);
        doc.setFontSize(10);
        doc.text(docTitle, position, 140);
        doc.addImage(docImage, 'PNG', 50, 45, 75, 75);

        doc.setFontSize(8);
        doc.text("Imprimé le " + day + '/' + month + '/' + year + ' à ' + hh + ':' + mm + ':' + ss + ' avec E-RECETTES 1.0', 590, 25);
        doc.text("Par : " + userData.nomComplet, 590, 35);
        doc.text("Page : " + data.pageCount, 590, 45);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: {
            numeroDeclarationInfo: {columnWidth: 90, fontSize: 8, overflow: 'linebreak'},
            bien: {columnWidth: 115, overflow: 'linebreak', fontSize: 8},
            libelleArticleBudgetaire: {columnWidth: 115, overflow: 'linebreak', fontSize: 8},
            dateCreate: {columnWidth: 100, overflow: 'linebreak', fontSize: 8},
            dateEcheance: {columnWidth: 110, overflow: 'linebreak', fontSize: 8},
            amount: {columnWidth: 100, overflow: 'linebreak', fontSize: 8},
            banque: {columnWidth: 140, overflow: 'linebreak', fontSize: 8}
        },
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 10 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');
//    doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(sumCDF), 540, finalY + 40);
//    doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD), 540, finalY + 60);
    window.open(doc.output('bloburl'), '_blank');
}
