/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var modalSearchDocumentPaiement;
var typeDocument;
var selectTypeDocument;
var referenceDocument;
var btnSearchEditPaiement;
var panelInformationNote;

var lblLegalForm, lblNameResponsible, lblAddress;

var notePerceptionList = [];
var amrList = [];
var bonApayerList = [];
var declarationList = [];

var lblTypeTitrePerception;
var lblNotePerception;
var lblArticleBudgetaire;

var footerPanelPaiementInfo, panelDeclarationInfo;

var lblNetAPayer, lblMontantPayer, lblSolde, selectBankPaid, selectAccountBankPaid, selectDevise, inputDateEmission;

var codePersonne, compteBancaire, deviseCode, typeDocument, net, surplus;

var btnValiderPaiement1, btnValiderPaiement2;
var datePicker1;
var datePicker2;
var inputMontantPercu, inputBordereau, documentPaid;

var lblDocumentPaid1, lblDocumentPaid2;
var tablePeriodeDeclaration;
var labelTotalAmount;
var idValueMessagePaiement1, idValueMessagePaiement2;

$(function () {

    mainNavigationLabel.text('PAIEMENT');
    secondNavigationLabel.text('Edition d\'un paiement');

    removeActiveMenu();
    linkSubMenuEditerPaiement.addClass('active');

    typeDocument = '';

    modalSearchDocumentPaiement = $('#modalSearchDocumentPaiement');
    selectTypeDocument = $('#selectTypeDocument');
    referenceDocument = $('#referenceDocument');
    btnSearchEditPaiement = $('#btnSearchEditPaiement');
    panelInformationNote = $('#panelInformationNote');
    labelTotalAmount = $('#labelTotalAmount');

    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');

    lblTypeTitrePerception = $('#lblTypeTitrePerception');
    lblNotePerception = $('#lblNotePerception');
    lblArticleBudgetaire = $('#lblArticleBudgetaire');

    lblNetAPayer = $('#lblNetAPayer');
    lblMontantPayer = $('#lblMontantPayer');
    lblSolde = $('#lblSolde');
    selectBankPaid = $('#selectBankPaid');
    selectAccountBankPaid = $('#selectAccountBankPaid');
    selectDevise = $('#selectDevise');

    lblDocumentPaid1 = $('#lblDocumentPaid1');
    lblDocumentPaid2 = $('#lblDocumentPaid2');
    idValueMessagePaiement1 = $('#idValueMessagePaiement1');
    idValueMessagePaiement2 = $('#idValueMessagePaiement2');


    btnValiderPaiement1 = $('#btnValiderPaiement1');
    btnValiderPaiement2 = $('#btnValiderPaiement2');

    inputDateEmission = $('#inputDateEmission');
    inputMontantPercu = $('#inputMontantPercu');
    inputBordereau = $('#inputBordereau');

    datePicker1 = $("#datePicker1");
    datePicker2 = $("#datePicker2");
    tablePeriodeDeclaration = $("#tablePeriodeDeclaration");

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePicker1.datepicker("setDate", new Date());
    datePicker2.datepicker("setDate", new Date());

    btnValiderPaiement1.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkValuePaiementJournal();
    });

    btnValiderPaiement2.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        checkValuePaiementJournal();
    });

    footerPanelPaiementInfo = $('#footerPanelPaiementInfo');
    panelDeclarationInfo = $('#panelDeclarationInfo');

    btnSearchEditPaiement.on('click', function () {
        switchDocumentByType(typeDocument);
    });

    selectTypeDocument.on('change', function () {
        typeDocument = selectTypeDocument.val();
    });

    loadModalTypePaiment();

});

function loadModalTypePaiment() {
    modalSearchDocumentPaiement.modal('show');
}

function switchDocumentByType(type) {

    if (selectTypeDocument.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner un document valide avant de rechercher et afficher les informations du titre de perception');
        return;
    }

    if (referenceDocument.val() == empty) {
        alertify.alert('Veuillez d\'abord fournir la référence document à payer');
        return;
    }

    if (typeDocument == '') {
        typeDocument = selectTypeDocument.val();
    } else {
        typeDocument = type;
    }

    switch (typeDocument) {
        case 'AMR':
            loadAmrByNumero(referenceDocument.val());
            break;
        case 'BP':
            loadBonAPayerByNumero(referenceDocument.val());
            break;
        case 'DEC':
            loadDeclarationByCode(referenceDocument.val());
            break;
        case 'NP':
            loadNotePerceptionByNumero(referenceDocument.val());
            break;
        case 'NPCV':
            loadCommandeVoucherByNumero(referenceDocument.val());
            break;
    }

}

function loadNotePerceptionByNumero(numero) {

    var value = '<span style="font-weight:bold">' + numero + '</span>';

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'referenceDocument': numero,
            'siteCode': userData.SiteCode,
            'operation': 'loadNotePerception'

        },
        beforeSend: function () {
            modalSearchDocumentPaiement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1') {
                
                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();
                showResponseError();
                return;
            
            } else if (response == '0') {
                
                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();
                alertify.alert('Cette note de perception n° ' + value + ' n\'existe pas dans le système');

            } else if (response == '3') {
                
                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();
                alertify.alert('Vous n\'êtes pas autoriser à payer cette note de perception dans votre banque');

            } else if (response == '4') {
                modalSearchDocumentPaiement.unblock();
                alertify.alert('Vous n\'êtes pas autoriser à payer cette note de perception car elle a des notres fractionnées.');

            } else {
                setTimeout(function () {

                    modalSearchDocumentPaiement.unblock();
                    notePerceptionList = JSON.parse(JSON.stringify(response));
                    modalSearchDocumentPaiement.modal('hide');

                    panelInformationNote.attr('style', 'visibility: visible');
                    displayTitrePerceptionInfo(notePerceptionList);
                }
                , 1);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            panelInformationNote.attr('spanelInformationNotetyle', 'visibility: hidden');
            modalSearchDocumentPaiement.block();
            showResponseError();
        }

    });
}

function loadCommandeVoucherByNumero(numero) {

    var value = '<span style="font-weight:bold">' + numero + '</span>';

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'referenceDocument': numero,
            'codeSite': userData.SiteCode,
            'operation': 'loadCommandeVoucherByNumero'

        },
        beforeSend: function () {
            modalSearchDocumentPaiement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1') {
                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();
                showResponseError();
                return;

            } else if (response == '0') {

                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();
                alertify.alert('Cette note de paiement n° ' + value + ' n\'existe pas dans le système');

            } else if (response == '3') {

                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();
                alertify.alert('Vous n\'êtes pas autoriser à payer cette note de paiement dans votre banque');

            } else {

                setTimeout(function () {

                    modalSearchDocumentPaiement.unblock();
                    notePerceptionList = JSON.parse(JSON.stringify(response));
                    modalSearchDocumentPaiement.modal('hide');

                    panelInformationNote.attr('style', 'visibility: visible');
                    displayTitrePerceptionInfo(notePerceptionList);
                }
                , 1);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            panelInformationNote.attr('spanelInformationNotetyle', 'visibility: hidden');
            modalSearchDocumentPaiement.block();
            showResponseError();
        }

    });
}

function loadAmrByNumero(numero) {

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'referenceDocument': numero,
            'siteCode': userData.SiteCode,
            'operation': 'loadAmr'

        },
        beforeSend: function () {
            modalSearchDocumentPaiement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1') {
                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();
                showResponseError();
                return;
            } else if (response == '0') {
                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();

                alertify.alert('La référence de l\'AMR fourni n\'existe pas dans le système');

            } else if (response == '3') {
                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();

                alertify.alert('Vous n\'êtes pas autoriser à payer cet AMR');


            } else if (response == '4') {
                modalSearchDocumentPaiement.unblock();
                alertify.alert('Vous n\'êtes pas autoriser à payer cet AMR car il a des titres de perception fils.');

            } else if (response == '5') {
                modalSearchDocumentPaiement.unblock();
                alertify.alert('Vous n\'êtes pas autoriser à payer cet AMR car il est en contentieux');

            } else {
                setTimeout(function () {

                    modalSearchDocumentPaiement.unblock();
                    notePerceptionList = JSON.parse(JSON.stringify(response));
                    modalSearchDocumentPaiement.modal('hide');

                    panelInformationNote.attr('style', 'visibility: visible');
                    displayTitrePerceptionInfo(notePerceptionList);
                }
                , 1);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            panelInformationNote.attr('spanelInformationNotetyle', 'visibility: hidden');
            modalSearchDocumentPaiement.block();
            showResponseError();
        }

    });
}

function loadBonAPayerByNumero(numero) {

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'referenceDocument': numero,
            'siteCode': userData.SiteCode,
            'operation': 'loadBonApayer'

        },
        beforeSend: function () {
            modalSearchDocumentPaiement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1') {
                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();
                showResponseError();
                return;
            } else if (response == '0') {
                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();

                alertify.alert('La référence du Bon à payer fourni n\'existe pas dans le système');

            } else if (response == '3') {
                panelInformationNote.attr('style', 'visibility: hidden');
                modalSearchDocumentPaiement.unblock();

                alertify.alert('Vous n\'êtes pas autoriser à payer ce Bon à payer');


            } else if (response == '4') {
                modalSearchDocumentPaiement.unblock();
                alertify.alert('Vous n\'êtes pas autoriser à payer ce Bon à payer car il a des titres de perception fils.');

            } else if (response == '5') {
                modalSearchDocumentPaiement.unblock();
                alertify.alert('Vous n\'êtes pas autoriser à payer ce Bon à payer car il est en contentieux');

            } else {
                setTimeout(function () {

                    modalSearchDocumentPaiement.unblock();
                    notePerceptionList = JSON.parse(JSON.stringify(response));
                    modalSearchDocumentPaiement.modal('hide');

                    panelInformationNote.attr('style', 'visibility: visible');
                    displayTitrePerceptionInfo(notePerceptionList);
                }
                , 1);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            panelInformationNote.attr('spanelInformationNotetyle', 'visibility: hidden');
            modalSearchDocumentPaiement.block();
            showResponseError();
        }

    });
}

function loadDeclarationByCode(code) {

    var value = '<span style="font-weight:bold">' + code + '</span>';

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'referenceDocument': code,
            'userId': userData.idUser,
            'siteCode': userData.SiteCode,
            'operation': 'loadDeclaration'

        },
        beforeSend: function () {
            modalSearchDocumentPaiement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            modalSearchDocumentPaiement.unblock();
            
            if (response == '-1') {
                
                panelInformationNote.attr('style', 'visibility: hidden');
                showResponseError();
                return;
            
            } else if (response == '0') {
                
                panelInformationNote.attr('style', 'visibility: hidden');
                //modalSearchDocumentPaiement.unblock();
                alertify.alert('Cette note de taxation n° ' + value + ' n\'existe pas dans le système');

            } else if (response == '3') {

                panelInformationNote.attr('style', 'visibility: hidden');
                alertify.alert('Vous n\'êtes pas autoriser à percevoir cette note de taxation dans votre banque');

            } else if (response == '-0') {

                panelInformationNote.attr('style', 'visibility: hidden');
                alertify.alert('Impossible de payer cette note de taxation car elle est désactivée dans le système');


            } else if (response == '2') {

                panelInformationNote.attr('style', 'visibility: hidden');
                alertify.alert('Impossible de payer cette note de taxation car elle possède des notes d\'échelonnements');


            } else {

                setTimeout(function () {

                    modalSearchDocumentPaiement.unblock();
                    declarationList = JSON.parse(JSON.stringify(response));
                    modalSearchDocumentPaiement.modal('hide');

                    panelInformationNote.attr('style', 'visibility: visible');
                    displayTitrePerceptionInfo(declarationList);
                }
                , 1);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            panelInformationNote.attr('spanelInformationNotetyle', 'visibility: hidden');
            modalSearchDocumentPaiement.block();
            showResponseError();
        }

    });
}

function displayTitrePerceptionInfo(document) {

    lblLegalForm.html(document.typePersonne);
    lblNameResponsible.html(document.assujettiName);
    lblAddress.html(document.adressePersonne);

    codePersonne = document.assujettiCode;
    compteBancaire = document.CompteBancaireCode;
    deviseCode = document.deviseCode;
    typeDocument = document.typeDocumentCode;
    net = parseFloat(document.netAPayer);

    lblTypeTitrePerception.html(document.typeDocumentLibelle);
    lblNotePerception.html(document.referenceDocument);
    documentPaid = document.referenceDocument;
    lblArticleBudgetaire.html(document.articleName.toUpperCase());

    lblNetAPayer.html(formatNumber(document.netAPayer, document.deviseCode));
    inputMontantPercu.val(empty);
    //inputMontantPercu.val(formatNumber(document.netAPayer, document.deviseCode));
    lblMontantPayer.html(formatNumber(document.payer, document.deviseCode));
    lblSolde.html(formatNumber(document.solde, document.deviseCode));

    var dataBank;
    var banqueUserList;
    var dataAccountBank;
    var accountBankList;

    switch (document.typeDocumentCode) {

        case 'NP':
        case 'NPCV':

            footerPanelPaiementInfo.attr('style', 'visibility: visible');
            panelDeclarationInfo.attr('style', 'visibility: hidden');

            lblDocumentPaid1.attr('style', 'display: block');
            lblDocumentPaid2.attr('style', 'display: block');

            dataBank;
            banqueUserList = JSON.parse(userData.banqueUserList);

            for (var i = 0; i < banqueUserList.length; i++) {
                if (document.banqueCode == banqueUserList[i].banqueCode) {
                    dataBank += '<option value="' + banqueUserList[i].banqueCode + '" >' + banqueUserList[i].banqueName + '</option>';
                }
            }

            selectBankPaid.html(dataBank);
            selectBankPaid.val(document.banqueCode)

            dataAccountBank;
            accountBankList = JSON.parse(userData.accountList);

            for (var i = 0; i < accountBankList.length; i++) {
                if (document.CompteBancaireCode == accountBankList[i].accountCode) {
                    dataAccountBank += '<option value="' + accountBankList[i].accountCode + '" >' + accountBankList[i].accountName + '</option>';
                }
            }

            selectAccountBankPaid.html(dataAccountBank);
            selectAccountBankPaid.val(document.CompteBancaireCode);

            if (selectDevise.val() == document.deviseCode) {
                selectDevise.val(document.deviseCode);
            }

            break;

        case 'AMR':
        case 'BP':

            footerPanelPaiementInfo.attr('style', 'visibility: visible');
            panelDeclarationInfo.attr('style', 'visibility: hidden');

            lblDocumentPaid1.attr('style', 'display: block');
            lblDocumentPaid2.attr('style', 'display: block');

            dataBank;
            banqueUserList = JSON.parse(document.BanqueList);

            for (var i = 0; i < banqueUserList.length; i++) {

                dataBank += '<option value="' + banqueUserList[i].banqueCode + '" >' + banqueUserList[i].banqueName + '</option>';

            }

            selectBankPaid.html(dataBank);
            //selectBankPaid.val(document.banqueCode)

            dataAccountBank;
            accountBankList = JSON.parse(document.CompteBancaireList);

            for (var i = 0; i < accountBankList.length; i++) {

                dataAccountBank += '<option value="' + accountBankList[i].CompteBancaireCode + '" >' + accountBankList[i].CompteBancaireLibelle + '</option>';

            }

            selectAccountBankPaid.html(dataAccountBank);
            //selectAccountBankPaid.val(document.CompteBancaireCode);

            /*if (selectDevise.val() == document.deviseCode) {
             selectDevise.val(document.deviseCode);
             }*/

            selectDevise.val(document.deviseCode);

            break;
        case 'DEC':

            footerPanelPaiementInfo.attr('style', 'visibility: hidden');
            panelDeclarationInfo.attr('style', 'visibility: visible');

            dataBank;
            banqueUserList = JSON.parse(userData.banqueUserList);

            for (var i = 0; i < banqueUserList.length; i++) {
                if (document.banqueCode == banqueUserList[i].banqueCode) {
                    dataBank += '<option value="' + banqueUserList[i].banqueCode + '" >' + banqueUserList[i].banqueName + '</option>';
                }
            }

            selectBankPaid.html(dataBank);
            selectBankPaid.val(document.banqueCode)

            dataAccountBank;
            accountBankList = JSON.parse(userData.accountList);

            for (var i = 0; i < accountBankList.length; i++) {
                if (document.CompteBancaireCode == accountBankList[i].accountCode) {
                    dataAccountBank += '<option value="' + accountBankList[i].accountCode + '" >' + accountBankList[i].accountName + '</option>';
                }
            }

            selectAccountBankPaid.html(dataAccountBank);
            selectAccountBankPaid.val(document.CompteBancaireCode);

            selectDevise.val(document.deviseCode);
            //selectDevise.attr('disabled', true);
            selectDevise.attr('disabled', false);

            labelTotalAmount.html(formatNumber(document.netAPayer, document.deviseCode));
            printPeriodeDeclaration(JSON.parse(document.detailDeclarationJsonList), selectDevise.val());
            break;
    }


    if (document.paiementExisting == '1') {

        inputBordereau.val(document.bordereau);
        inputDateEmission.val(document.dateBordereau);
        var amount = formatNumber(document.montantPaye, document.deviseCode);

        inputMontantPercu.attr('type', 'text');
        inputMontantPercu.val(amount);

        inputBordereau.attr('readonly', true);
        inputDateEmission.attr('readonly', true);
        inputMontantPercu.attr('readonly', true);

        btnValiderPaiement1.attr('disabled', 'true');
        btnValiderPaiement2.attr('disabled', 'true');

        switch (document.typeDocumentCode) {
            case 'NP':
            case 'NPCV':
                lblDocumentPaid1.attr('style', 'display: block');
                idValueMessagePaiement1.html(document.messagePaiement);
                lblDocumentPaid2.attr('style', 'display: none');

                break;
            case 'DEC':
                lblDocumentPaid1.attr('style', 'display: none');
                lblDocumentPaid2.attr('style', 'display: block');

                idValueMessagePaiement2.html(document.messagePaiement);

                selectAccountBankPaid.val(document.compteBancaireBordereau);
                selectAccountBankPaid.attr('disabled', true);

                btnValiderPaiement2.attr('style', 'visibility: hidden');

                break;
        }




    } else {
        lblDocumentPaid1.html(empty);
        lblDocumentPaid2.html(empty);
        idValueMessagePaiement1.html(empty);
    }

}

var amountPaid;

function checkValuePaiementJournal() {

    if (inputMontantPercu.val() == empty) {

        alertify.alert('Veuillez d\'abord saisir le montant à percevoir');
        return;

    } else if (!$.isNumeric(inputMontantPercu.val())) {

        alertify.alert('Le montant perçu fourni n\'est pas un nombre');
        return;

    } else {

        amountPaid = parseFloat(inputMontantPercu.val());

        if (amountPaid < net) {

            alertify.alert('Vous ne pouvez payer un montant inférieur au net à payer du titre de perception');
            return;

        }
    }

    if (typeDocument == 'DEC') {

        if (selectAccountBankPaid.val() === null || selectAccountBankPaid.val() === 'null') {
            alertify.alert('Veuillez d\'abord sélectionner le compte bancaire de ce paiement');
            return;
        }
    }

    if (inputBordereau.val() === "") {
        alertify.alert('Veuillez d\'abord fournir la référence de ce paiement');
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir enregistrer ce paiement ? ', function () {

        switch (typeDocument) {
            case 'AMR':
            case 'BP':
            case 'NP':
                savePaiementJournal();
                break;
            case 'DEC':
                savePaiementDeclaration();
                break;
            case 'NPCV':
                savePaiementCommandeVoucher();
                break;
        }
    });

}

function savePaiementDeclaration() {

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'},
        crossDomain: false,
        data: {
            'referenceDocument': documentPaid,
            'referencePaiement': inputBordereau.val(),
            'datePaiement': inputDateEmission.val(),
            'userId': userData.idUser,
            'CompteBancaireCode': selectAccountBankPaid.val(),
            'amountPaid': amountPaid,
            'operation': 'saveDeclaration'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >L\'enregistrement du paiement en cours...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '700') {
                $.unblockUI();
                alertify.alert('Ce bordereau de paiement : ' + inputBordereau.val() + ' est déjà lié au compte bancaire : ' + $('#selectAccountBankPaid option:selected').text());
                return;
            } else {
                setTimeout(function () {
                    $.unblockUI();
                    alertify.alert('L\'enregistrement du paiement s\'est effectué avec succès.');
                    setTimeout(function () {
                        window.location = 'edition-paiement';
                    }, 3000);
                }
                , 1);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function savePaiementCommandeVoucher() {

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'},
        crossDomain: false,
        data: {
            'referenceDocument': documentPaid,
            'referencePaiement': inputBordereau.val(),
            'userId': userData.idUser,
            'operation': 'savePaiementCommandeVoucher'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >L\'enregistrement du paiement en cours...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '700') {
                $.unblockUI();
                alertify.alert('Ce bordereau de paiement : ' + inputBordereau.val() + ' est déjà lié au compte bancaire : ' + $('#selectAccountBankPaid option:selected').text());
                return;
            } else {
                setTimeout(function () {
                    $.unblockUI();
                    alertify.alert('L\'enregistrement du paiement s\'est effectué avec succès.');
                    setTimeout(function () {
                        window.location = 'edition-paiement';
                    }, 3000);
                }
                , 1);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function savePaiementJournal() {

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'},
        crossDomain: false,
        data: {
            'referenceDocument': documentPaid,
            'referencePaiement': inputBordereau.val(),
            'typeDocumentCode': typeDocument,
            'datePaiement': inputDateEmission.val(),
            'accountBank': selectAccountBankPaid.val(),
            'bankCode': selectBankPaid.val(),
            'montant': inputMontantPercu.val(),
            'userId': userData.idUser,
            'operation': 'saveJournal'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >L\'enregistrement du paiement en cours...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '700') {
                $.unblockUI();
                alertify.alert('Ce bordereau de paiement : ' + inputBordereau.val() + ' est déjà lié au compte bancaire : ' + $('#selectAccountBankPaid option:selected').text());
                return;
            } else {
                setTimeout(function () {
                    $.unblockUI();
                    alertify.alert('L\'enregistrement du paiement s\'est effectué avec succès.');
                    setTimeout(function () {
                        window.location = 'edition-paiement';
                    }, 3000);
                }
                , 1);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printPeriodeDeclaration(periodList, devise) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">PERIODE DECLARATION</th>';
    tableContent += '<th style="text-align:left">BIEN DECLARATION</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';
    for (var i = 0; i < periodList.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;width:15%">' + periodList[i].periodeDeclaration + '</td>';
        tableContent += '<td style="vertical-align:middle;width:70%">' + periodList[i].bienDeclaration + '</td>';
        tableContent += '<td style="text-align:right;vertical-align:middle;width:15%">' + formatNumber(periodList[i].netAPayer, devise) + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tablePeriodeDeclaration.html(tableContent);
    tablePeriodeDeclaration.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "", loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        paging: false,
        pageLength: 5,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

