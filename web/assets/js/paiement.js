/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var inputResearchValue;
var inputResearchType;
var codeResearch;


var codeBanqueSelct;
var btnAdvencedSearch;
var btnSimpleSearch;

var btnShowAdvancedSerachModal;

var divbtnValideApurement, btnValiderApurement;
var tableJournal, journalList;

var savedOperation;
var from;
var tempPaiementList = [];
var sumUSD = 0, sumCDF = 0;
var cmbBureau;

$(function () {
    from = 0;
    number = 7;
    inputResearchValue = $('#ResearchValue');
    inputResearchType = $('#ResearchType');

    codeResearch = inputResearchType.val();

    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    divbtnValideApurement = $('#divbtnValideApurement');

    tableJournal = $('#tableJournal');

    inputResearchType.on('change', function (e) {
        codeResearch = inputResearchType.val();

        if (codeResearch === "1") {
            inputResearchValue.attr('placeholder', 'Le nom de l\'assujetti');
            inputResearchValue.val('');
        } else {
            inputResearchValue.attr('placeholder', 'Numero de la note de perception');
            inputResearchValue.val('');
        }
    });

    btnSimpleSearch = $('#btnSimpleSearch');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    btnShowModalAdvencedRechearch = $('#btnRechercheAvancee');
    cmbBureau = $('.cmbBureau');

    btnShowAdvancedSerachModal.click(function (e) {
        e.preventDefault();
        from = 1;
        modalRechercheAvancee.modal('show');
    });

    $('.rapport-apurement').click(function (e) {

        e.preventDefault();
        alertify.confirm('Etes-vous sûre de vouloir imprimer le rapport des ordonnancés recouvrés ?', function () {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            if (tempPaiementList.length != 0) {
                printDataPaiement();
            } else {
                alertify.alert('Aucune donnée n\'est disponnible pour imprimer ce rapport.');
            }
        });
    });

});

function jourrnalSearching(page, value) {

    var siteValueList = [];
    var UsersSiteList = JSON.parse(userData.siteUserList);

    if (!controlAccess('VIEW_ALL_SITE')) {
        for (var i = 0; i < UsersSiteList.length; i++) {
            siteValueList.push(UsersSiteList[i]);
        }
    } else {
        siteValueList = 0;
    }

    var inputDateDeb = inputDateDebut.val();
    var inputDateF = inputdateLast.val();
    var operation;

    var advancedSearch = value;

    if (value === 1) {
        //operation = "searchPayment";
        operation = "searchPaymentFromAdministration";
        savedOperation = 1;
    } else {
        operation = "searchPaymentAdvenced";
        savedOperation = 2;
        modalRechercheAvancee.modal('hide');
    }

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'typeSearch': codeResearch,
            'valueSearch': inputResearchValue.val().trim(),
            'operation': operation,
            'page': page,
            'advancedSearch': advancedSearch,
            'fromResearch': '0',
            'siteValue': cmbBureau.val(),
            'siteValueList': (JSON.stringify(siteValueList)),
            'banqueValue': selectBanque.val(),
            'compteBancaireValue': selectCompteBancaire.val(),
            'periodeDebutValue': inputDateDeb,
            'periodeFinValue': inputDateF
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {

                loadJournalTable('', '');
                showResponseError();

                return;

            } else if (response == '0') {

                loadJournalTable('', '');
                alertify.alert('Il n\'y a pas des paiements disponible en attente d\'apurement');
                return;

                if (savedOperation == 1) {

                    if (inputResearchValue.val() !== empty) {

                        switch (codeResearch) {
                            case '1':
                                alertify.alert('Cet assujetti n\'est possède pas de titres de perception à l\'apurement provisoire');
                                return;
                                break;
                            case '2':
                                alertify.alert('Ce titre de perception ne pas en attente à l\'apurement provisoire');
                                return;
                                break;
                        }

                    }

                } else {

                    switch (page) {
                        case 1:
                            number = 7;
                            //alertify.alert('Aucun paiement est en attente d\'un apurement administratif');
                            return;
                            break;
                        case 2:
                            number = 7;
                            //alertify.alert('Aucun paiement est en attente d\'un apurement comptable');
                            return;
                            break;
                        case 3:
                            number = 9;
                            //alertify.alert('Aucun paiement est en attente d\'un apurement');
                            return;
                            break;
                    }
                }

            } else {
                setTimeout(function () {
                    journalList = null;
                    journalList = JSON.parse(JSON.stringify(response));
                    numberRows = journalList.length;

                    loadJournalTable(journalList, page);
                }
                , 1);
            }



        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadJournalTable(result, page) {

    var header = '';
    header += '<thead style="background-color:#0085c7;color:white" id="headerTable">';
    header += '<tr>';
    if (page === 1 || page === 2) {
        header += '<th></th>';
    }
    header += '<th>DATE PAIEMENT</th>';
    header += '<th>DOCUMENT</th>';
    header += '<th>NTD</th>';
    header += '<th>ASSUJETTI</th>';
    header += '<th>REFERENCE DOCUMENT</th>';
    header += '<th>BORDEREAU</th>';
    header += '<th>DATE BORDEREAU</th>';
    header += '<th>BANQUE</th>';
    header += '<th>COMPTE BANCAIRE</th>';
    header += '<th>MONTANT PAYE</th>';

    if (page !== 2) {
        header += '<th>ETAT</th>';
    }

    header += '<th>AGENT PAIEMENT</th>';

    if (page === 2 || page === 3) {
        header += '<th>NUMERO RELEVE</th>';
        header += '<th>DATE RELEVE</th>';
    }

    if (page === 1 || page === 2) {
        header += '<th></th>';
    }

    if (page === 3 || page === 4) {
        header += '<th></th>';
        header += '<th style="display:none"></th>';
    }

    if (page === 3) {
        number = 7;
    } else {
        number = 8;
    }

    header += '</tr>';
    header += '</thead>';

    var data = '';

    data += '<tbody id="bodyTable">';
    sumCDF = 0, sumUSD = 0;
    tempPaiementList = [];


    sumCDF = 0;
    sumUSD = 0;
    var number = 0;

    for (var i = 0; i < result.length; i++) {

        if (result[i].devise === 'CDF') {
            sumCDF += result[i].montantPaye;
        } else if (result[i].devise === 'USD') {
            sumUSD += result[i].montantPaye;
        }

        number += 1;

        var paiement = new Object();
        paiement.datePaiement = result[i].datePaiement;
        paiement.document = result[i].document;
        paiement.dateOrdonnancement = result[i].dateOrdonnancement;
        paiement.requerant = result[i].requerant;
        paiement.reference = result[i].reference;
        paiement.bordereau = result[i].bordereau;
        paiement.dateBordereau = result[i].dateBordereau;
        paiement.penalites = 0;
        paiement.montantPayePenalite = 0;
        paiement.montantApure = result[i].montantApure + ' ' + result[i].devise;
        paiement.montantPaye = result[i].montantPaye + ' ' + result[i].devise;
        paiement.datePaiement = result[i].datePaiement;
        paiement.agentPaiement = result[i].agentPaiement;
        paiement.numeroReleveBancaire = result[i].numeroReleveBancaire;
        paiement.dateReleveBancaire = result[i].dateReleveBancaire;
        paiement.intituleArticleBudgetaire = result[i].intituleArticleBudgetaire;
        tempPaiementList.push(paiement);

        data += '<tr>';

        if (page === 1 || page === 2) {
            data += '<td><input class="form-check-input position-static" type="checkbox" id="CheckboxAppurement" value="option1" aria-label="..."></td>';
        }

        data += '<td style="vertical-align:middle">' + result[i].datePaiement + '</td>';
        data += '<td style="vertical-align:middle">' + result[i].document + '</td>';
        data += '<td style="vertical-align:middle">' + result[i].userName + '</td>';
        data += '<td style="vertical-align:middle">' + result[i].requerant + '</td>';
        data += '<td style="vertical-align:middle">' + result[i].reference + '</td>';
        data += '<td style="vertical-align:middle">' + result[i].bordereau + '</td>';
        data += '<td style="vertical-align:middle">' + result[i].dateBordereau + '</td>';
        data += '<td style="vertical-align:middle">' + result[i].banque + '</td>';
        data += '<td style="vertical-align:middle">' + result[i].compteBancaire + '</td>';
        data += '<td style="text-align:right;vertical-align:middle">' + formatNumber(result[i].montantPaye, result[i].devise) + '</td>';

        if (page !== 2) {
            data += '<td style="vertical-align:middle">' + result[i].etatPaiement + '</td>';
        }

        data += '<td style="vertical-align:middle">' + result[i].agentPaiement.toUpperCase() + '</td>';

        if (page === 2 || page === 3) {

            data += '<td style="vertical-align:middle">' + result[i].numeroReleveBancaire + '</td>';
            data += '<td style="vertical-align:middle">' + result[i].dateReleveBancaire + '</td>';
        }

        if (page === 1) {

            data += '<td style="vertical-align:middle"> <a style="margin-right:3px" onclick="administratifApurementValidation(\'' + result[i].codeJournal + '\',\'' + result[i].reference + '\',\'' + userData.idUser + '\',\''
                    + result[i].montantApure + '\')" class="btn btn-success"><i class="fa fa-check"></i></a></td>';
        }

        if (page === 2) {
            data += '<td style="vertical-align:middle"> <a style="margin-right:3px" onclick="ComptableApurementValidation(\'' + result[i].codeJournal + '\')" class="btn btn-success"><i class="fa fa-check"></i></a></td>';
        }

        if (page === 3 || page === 4) {
            data += '<td style="vertical-align:middle"> <a style="color:blue;text-decoration: underline;cursor: pointer" onclick="getObservation(\'' + result[i].codeJournal + '\')">Observation</a></td>';
            data += '<td id="td_observation_' + result[i].codeJournal + '" style="display:none;vertical-align:middle">' + result[i].observation + '</td>';
        }

        data += '</tr>';
    }

    data += '</tbody>';
    data += '<tfoot>';

    data += '<tr><th colspan="9" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    data += '</tfoot>';

    var TableContent = header + data;

    tableJournal.html(TableContent);

    if (result.length > 0 && (page === 1 || page === 2)) {
    }

    tableJournal.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste ici ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: true,
        pageLength: 25,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 25,
        columnDefs: [
            {"visible": false, "targets": number}
        ],
        order: [[number, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(number, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="14">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        },
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(9).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD') +
                    '<hr/>' +
                    'TOTAL PAIEMENT : ' +number);
        }
    });
}

function getObservation(codeJournal) {

    var id = 'td_observation_' + codeJournal;
    var control = $('#' + id);

    $('#textObservApurement').text(control.html())
    $('#modalObservationApurement').modal('show');

}

function printDataPaiement() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position = 300, hauteur = 180;

    if (tempPaiementList.length > 0) {

        if (from == 1) {
            docTitle = 'ORDONNANCEMENTS RECOUVRES\n\n     BUREAU DE RECOUVREMENT\n';

            if (cmbBureau.val() == '*' && selectService.val() == '*') {
                docTitle += ' (Pour la période du ' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 300;
            } else if (cmbBureau.val() !== '*' && selectService.val() == '*') {
                docTitle += ' (Pour la période du ' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 300;
                hauteur = 180;
            } else if (cmbBureau.val() == '*' && selectService.val() !== '*') {
                docTitle += ' (Pour la période du' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 300;
                hauteur = 180;
            } else {
                docTitle += ' (Pour la période du ' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 300;
                hauteur = 180;
            }
        } else {
            var critere = inputResearchValue.val().toUpperCase();
            if (critere == '') {
                docTitle = 'ORDONNANCEMENTS RECOUVRES\n\n     BUREAU DE RECOUVREMENT\n';
                docTitle += ' (Pour la période du ' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
            } else {
                docTitle = 'ORDONNANCEMENTS RECOUVRES\n\n    BUREAU DE RECOUVREMENT\n : ' + critere;
                docTitle += ' (Pour la période du ' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
            }


            position = 300;
        }

    } else {
        docTitle = 'ORDONNANCEMENTS RECOUVRES\n\n     BUREAU DE RECOUVREMENT\n\n';
        position = 440;
    }

    var columns = [
        {title: "DATE ORD.", dataKey: "dateOrdonnancement"},
        {title: "ACTE GENERATEUR", dataKey: "intituleArticleBudgetaire"},
        {title: "CONTRIBUABLES", dataKey: "requerant"},
        {title: "TYPE TITRE", dataKey: "document"},
        {title: "N° TAXATION", dataKey: "reference"},
        {title: "MONTANT", dataKey: "montantPaye"},
        {title: "PENALITES", dataKey: "penalites"},
        {title: "DATE PAYE", dataKey: "datePaiement"},
        {title: "MONT. REC.", dataKey: "montantApure"},
        {title: "MONT. REC. PEN.", dataKey: "montantPayePenalite"}];

    var rows = tempPaiementList;
    var pageContent = function (data) {

        var bureau = $('.cmbBureau option:selected').text();
        setDocParams(doc, docTitle, position, day, month, year, hh, mm, ss, data, bureau);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 8},
        columnStyles: {
            dateOrdonnancement: {columnWidth: 55, fontSize: 8, overflow: 'linebreak'},
            intituleArticleBudgetaire: {columnWidth: 115, overflow: 'linebreak', fontSize: 8},
            requerant: {columnWidth: 140, overflow: 'linebreak', fontSize: 8},
            document: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            reference: {columnWidth: 73, overflow: 'linebreak', fontSize: 8},
            montantPaye: {columnWidth: 60, overflow: 'linebreak', fontSize: 8},
            penalites: {columnWidth: 55, overflow: 'linebreak', fontSize: 8},
            datePaiement: {columnWidth: 55, overflow: 'linebreak', fontSize: 8},
            montantApure: {columnWidth: 75, overflow: 'linebreak', fontSize: 8},
            montantPayePenalite: {columnWidth: 80, overflow: 'linebreak', fontSize: 8}
        },
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 10 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');
    doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(sumCDF), 540, finalY + 40);
    doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD), 540, finalY + 60);
    window.open(doc.output('bloburl'), '_blank');
}
