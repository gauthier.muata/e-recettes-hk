


var tableBienAutomobile, btnChangeBien, btnCancel, tableBienAutomobileSelected;

var bienAutomobileListSelected = [];
var bienAutomobileListOthers = [];

var valueUrlIdBien,
        valueUrlContribuable,
        valueUrlNoteTaxation,
        valueUrlExercice;

var nameeBienCurrent;

$(function () {


    mainNavigationLabel.text('DECLARATION');
    secondNavigationLabel.text('Remplacement des biens automobile dans la taxation');
    removeActiveMenu();

    tableBienAutomobile = $('#tableBienAutomobile');
    btnChangeBien = $('#btnChangeBien');
    btnCancel = $('#btnCancel');
    tableBienAutomobileSelected = $('#tableBienAutomobileSelected');

    var urlIdBien = getUrlParameter('id');
    var urlContribuable = getUrlParameter('contribuable');
    var urlExercice = getUrlParameter('exercice');
    var urlNoteTaxation = getUrlParameter('taxation');

    valueUrlIdBien = atob(urlIdBien);
    valueUrlContribuable = atob(urlContribuable);
    valueUrlExercice = atob(urlExercice);
    valueUrlNoteTaxation = atob(urlNoteTaxation);

    loadBienAutomobileByIDBien();


})

function loadBienAutomobileByIDBien() {
    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBienAutomobileByIDBien',
            'codeBien': valueUrlIdBien,
            'codePersonne': valueUrlContribuable
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des biens en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                showResponseError();
                return;

            } else if (response == '0') {

                printTableBienAutomobile('');

            } else {

                bienAutomobileListSelected = $.parseJSON(JSON.stringify(response));
                printTableBienAutomobile(bienAutomobileListSelected);
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadBienAutomobileNotTaxed() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBienAutomobileNotTaxed',
            'codePersonne': valueUrlContribuable
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des biens en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                printTableBienAutomobileOther('');
                showResponseError();
                return;

            } else if (response == '0') {

                printTableBienAutomobileOther('');
                alertify.alert('Désolé le contribuable ne possède pas d\'autres biens automobile pour procéder au remplacement');

            } else {

                bienAutomobileListOthers = $.parseJSON(JSON.stringify(response));
                printTableBienAutomobileOther(bienAutomobileListOthers);
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            printTableBienAutomobileOther('');
            showResponseError();
        }

    });
}

function printTableBienAutomobile(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';

    tableContent += '<th style="text-align:center;width:5%">№</th>';
    tableContent += '<th style="text-align:left;width:20%">GENRE</th>';
    tableContent += '<th style="text-align:left;width:20%">DENOMINATION</th>';
    tableContent += '<th style="text-align:left;width:20%">DESCRIPTION</th>';
    tableContent += '<th style="text-align:left;width:10%">№ PLAQUE</th>';
    tableContent += '<th style="text-align:left;width:10%">№ CHASSIS</th>';
    tableContent += '<th style="text-align:right;width:10%">TAUX VIGNETTE</th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var count = 1;

    for (var i = 0; i < result.length; i++) {

        nameeBienCurrent = '<span style="font-weight:bold">' + result[i].denomination + '</span>';
        valueUrlNoteTaxation = result[i].noteTaxation;

        var categorieInfo = 'CATEGORIE : ' + '<span style="font-weight:bold">' + result[i].nameTarifBien + '</span>';
        var typeInfo = 'MODELE : ' + '<span style="font-weight:bold">' + result[i].modeleName + '</span>';
        var marqueInfo = 'MARQUE : ' + '<span style="font-weight:bold">' + result[i].marqueName + '</span>';
        var puissanceInfo = 'PUISSANCE FISCALE : ' + '<span style="font-weight:bold">' + result[i].puissanceFiscal + ' ' + result[i].nameUnitePuissance + '</span>';
        var anneeCirculationInfo = 'ANNEE DE MISE EN CIRCULATION : ' + '<span style="font-weight:bold">' + result[i].anneeCirculation + '</span>';
        var dateCirculationInfo = 'DATE DE MISE EN CIRCULATION : ' + '<span style="font-weight:bold">' + result[i].dateMiseCirculation1 + '</span>';

        var descriptionInfo = typeInfo + '<br/>' + marqueInfo + '<br/>' + categorieInfo + '<br/>' + puissanceInfo + '<br/>' + dateCirculationInfo + '<br/>' + anneeCirculationInfo;

        var denomination = 'DENOMINATION : ' + '<span style="font-weight:bold">' + result[i].denomination + '</span>';
        var adresseInfo = 'ADRESSE : ' + '<span style="font-weight:bold">' + result[i].nameAdresse + '</span>';

        var denominationInfo = denomination + '<br/><br/>' + adresseInfo;

        tableContent += '<tr id="row_' + result[i].codeBien + '">';

        //tableContent += '<td style="text-align:center;width:5%;vertical-align:middle"><input  type="checkbox" ' + valueDisabled + 'id="checkbox_' + result[i].codeBien + '" onclick="checkingAllRows(\'' + checkAllRows + '\',\'' + result[i].tauxVignette + '\',\'' + result[i].devise + '\')" /></th>';

        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + count + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].nameTypeBien + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + denominationInfo + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + descriptionInfo + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].plaque + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].chassis + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold;font-size:18px">' + formatNumber(result[i].tauxVignette, result[i].devise) + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableBienAutomobileSelected.html(tableContent);

    tableBienAutomobileSelected.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des biens automobile est vide",
            search: "Filtrer la liste des biens automobile ici",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"}
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false, ordering: false,
        pageLength: 50,
        columnDefs: [
            {"visible": false, "targets": 1}
        ],
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        /*select: {
         style: 'os',
         blurable: true
         },*/
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(1, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    loadBienAutomobileNotTaxed();

}

function printTableBienAutomobileOther(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';

    tableContent += '<th style="text-align:center;width:5%">№</th>';
    tableContent += '<th style="text-align:left;width:20%">GENRE</th>';
    tableContent += '<th style="text-align:left;width:20%">DENOMINATION</th>';
    tableContent += '<th style="text-align:left;width:20%">DESCRIPTION</th>';
    tableContent += '<th style="text-align:left;width:10%">№ PLAQUE</th>';
    tableContent += '<th style="text-align:left;width:10%">№ CHASSIS</th>';
    tableContent += '<th style="text-align:right;width:10%">TAUX VIGNETTE</th>';
    tableContent += '<th style="text-align:center;width:10%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var count = 0;
    var totalBien = result.length;

    for (var i = 0; i < result.length; i++) {

        count++;

        var btnChangeBienAutomobile = '<button class="btn btn-success" onclick="changeBienAutomobile(\'' + result[i].codeBien + '\',\'' + result[i].denomination + '\')"><i class="fa fa-check-circle"></i> Remplacer</button>';

        var categorieInfo = 'CATEGORIE : ' + '<span style="font-weight:bold">' + result[i].nameTarifBien + '</span>';
        var typeInfo = 'MODELE : ' + '<span style="font-weight:bold">' + result[i].modeleName + '</span>';
        var marqueInfo = 'MARQUE : ' + '<span style="font-weight:bold">' + result[i].marqueName + '</span>';
        var puissanceInfo = 'PUISSANCE FISCALE : ' + '<span style="font-weight:bold">' + result[i].puissanceFiscal + ' ' + result[i].nameUnitePuissance + '</span>';
        var anneeCirculationInfo = 'ANNEE DE MISE EN CIRCULATION : ' + '<span style="font-weight:bold">' + result[i].anneeCirculation + '</span>';
        var dateCirculationInfo = 'DATE DE MISE EN CIRCULATION : ' + '<span style="font-weight:bold">' + result[i].dateMiseCirculation1 + '</span>';

        var descriptionInfo = typeInfo + '<br/>' + marqueInfo + '<br/>' + categorieInfo + '<br/>' + puissanceInfo + '<br/>' + dateCirculationInfo + '<br/>' + anneeCirculationInfo;

        var denomination = 'DENOMINATION : ' + '<span style="font-weight:bold">' + result[i].denomination + '</span>';
        var adresseInfo = 'ADRESSE : ' + '<span style="font-weight:bold">' + result[i].nameAdresse + '</span>';

        var denominationInfo = denomination + '<br/><br/>' + adresseInfo;

        tableContent += '<tr id="row_' + result[i].codeBien + '">';

        //tableContent += '<td style="text-align:center;width:5%;vertical-align:middle"><input  type="checkbox" ' + valueDisabled + 'id="checkbox_' + result[i].codeBien + '" onclick="checkingAllRows(\'' + checkAllRows + '\',\'' + result[i].tauxVignette + '\',\'' + result[i].devise + '\')" /></th>';

        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + count + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].nameTypeBien + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + denominationInfo + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + descriptionInfo + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].plaque + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].chassis + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold;font-size:18px">' + formatNumber(result[i].tauxVignette, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + btnChangeBienAutomobile + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="3" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL: </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableBienAutomobile.html(tableContent);

    tableBienAutomobile.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des biens automobile est vide",
            search: "Filtrer la liste des biens automobile ici",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"}
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false, ordering: false,
        pageLength: 50,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(3).footer()).html(totalBien);
        },
        columnDefs: [
            {"visible": false, "targets": 1}
        ],
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        /*select: {
         style: 'os',
         blurable: true
         },*/
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(1, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

}

function changeBienAutomobile(codeBien, nameBien) {

    var value = '<span style="font-weight:bold">' + nameBien.toUpperCase() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir remplacer ce bien automobile : ' + nameeBienCurrent + ' <br/>' + 'par celui-ci : ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'assujetissement_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'changeBienAutomobileTaxation',
                'codeOldBien': valueUrlIdBien,
                'codeNewBien': codeBien,
                'noteTaxation': valueUrlNoteTaxation,
                'pdOld': valueUrlExercice,
                'codePersonne': valueUrlContribuable
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Remplacement bien en cours ...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;


                } else if (response == '2') {

                    alertify.alert('Ce bien : ' + value + ' ne possède pas l\'exercice fiscal courant');
                    return;

                } else {

                    alertify.alert('Le remplacement du bien s\'est effectué avec succès.');

                    var noteObj = new Object();
                    noteObj.code = valueUrlNoteTaxation;

                    //sessionStorage.setItem('objDataNote', CryptoJS.AES.encrypt(JSON.stringify(noteObj), CYPHER_KEY).toString());
                    sessionStorage.setItem('objDataNote', JSON.stringify(noteObj));

                    setTimeout(function () {
                        window.location = 'registre-retrait-declaration-vignette';
                    }, 3000);

                }

            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });

    });

}

