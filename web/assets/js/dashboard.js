/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tableaudebord;
var faitGenerateurList;
var cmbAssietteService;
var btnfermer;
var dateDebut, dateCloture;
var cmdTypeTBB;



$(function () {

    mainNavigationLabel.text('ERECETTES 1.0');

    secondNavigationLabel.text('Tableau de bord');
    removeActiveMenu();
    linkMenuDahsboard.addClass('active');

    tableaudebord = $('#tableaudebord');
//    displaydashbord('');

    btnfermer = $('#btnfermer');
    btnfermer.click(function (e) {
        e.preventDefault();
        alertify.alert('Veuillez sélectionner un ministere.');

    });
    dateDebut = $('#dateDebut');
    dateCloture = $('#dateCloture');
    cmdTypeTBB = $('#cmdTypeTBB');

    cmbAssietteService = $('#cmbAssietteService');
    cmbAssietteService.on('change', function () {

    });

    loadServicesAssieteByCode();

});


function loadServicesAssieteByCode(code) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'load_services_assiettes',
            'code': code
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            var serviceList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner un service d\'assiètte ---</option>';

            for (var i = 0; i < serviceList.length; i++) {
                data += '<option value="' + serviceList[i].code + '">' + serviceList[i].intitule + '</option>';
            }
            cmbAssietteService.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}
function loadTaxationPonctuelle(operation, message_excepting) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': operation,
            'userId': userData.idUser,
            'annee': annee,
            'intitule': inputTitre.val(),
            'dateDebut': dateDebut.val(),
            'dateFin': dateCloture.val()
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>' + message_excepting + '</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            var serviceList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner un service d\'assiètte ---</option>';

            for (var i = 0; i < serviceList.length; i++) {
                data += '<option value="' + serviceList[i].code + '">' + serviceList[i].intitule + '</option>';
            }
            cmdTypeTBB.html(data);

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

