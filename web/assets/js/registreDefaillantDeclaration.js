/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var medList = [], mepList = [];
var tableDefaillants, tableDefaillantPaiement;
var btnSimpleSearch;
var assujettiModal;
var assujettiCodeValue;
var codeAssujetti, typeAssujetti;
var adresseId;
var responsibleObject = null;
var medDocumentPrint;
var medObj = new Object(), mepObj = new Object();
var isAvancedSearch;
var modalRechercheAvanceeByArticle;
var btnShowAdvancedSerachModal;
var btnAdvencedSearchByArticle;

var codeTypeDoc = 'MEP';
var codeDoc = undefined;

var tempDocumentList = [];
var periodeDeclarationIDList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var selectAB, selectService_, selectAnnee_1, selectMois_1;

var lblArticleBudgetaire, lblPeriodeDeclaration;

var isAdvance;
var btnAdvencedSearch, btnPrintInvitation;
var modalRechercheAvanceeModelTwo, btnShowAdvancedSerachModal;
var lblService, lblSite, lblDateDebut, lblDateFin;
var modalAccuserReception, medInfoModal;

var idIconBtnOperation,
        btnPrintMedOrMep,
        valDateEcheance,
        valDateReception,
        valDateCreate,
        valMedOrMep,
        lblMedOrMep,
        divInfoMed,
        valExercice,
        valReference,
        lblReference;

var echeanceMedExist;

var dateDebuts, dateFins, codeService, codeSite;
var tempDefaillantDecList = [];
var sumCDF = 0, sumUSD = 0;
var btnGenerateIS, btnGenerateRelance;

var amountTotalPrincipal = 0, amountTotalPenalite = 0;
var RelanceExisting;
var researchType;
var medObj2 = new Object();
var isRelance;
var assujettiNameSelected;
var modalGetAddEntiteAdministrative;
var codeKatanga = '00000000000427472016';

var selectAvenue, btnAddTypeAvenue,
        selectQuartier, btnAddTypeQuartier,
        selectCommune, btnAddTypeCommune;

var valueSearch;
var typeEASelected;
var frequence = 0;
var isChangeDayInvitation;
var inputNewDateHeureInvitation, btnChangeDayInvitation, modalChangeDayInvitation;
var medIdSeelcted;
var selectPeriode;
var isMonthSelected;
var yearCurrent;

$(function () {

    valueSearch = '';
    typeEASelected = '';
    isChangeDayInvitation = false;

    medDocumentPrint = '';
    isRelance = false;

    mainNavigationLabel.text('RECOUVREMENT');

    switch (getRegisterType()) {
        case 'SERVICE':
            secondNavigationLabel.text('Registre des défaillants en déclaration');
            break;
        case 'MEP':
            secondNavigationLabel.text('Registre des défaillants au paiement');
            break;
    }

    removeActiveMenu();
    linkMenuContentieux.addClass('active');

    tableDefaillants = $('#tableDefaillants');
    btnSimpleSearch = $('#btnSimpleSearch');
    assujettiModal = $('#assujettiModal');
    assujettiCodeValue = $('#assujettiCodeValue');
    modalRechercheAvanceeByArticle = $('#modalRechercheAvanceeByArticle');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    btnAdvencedSearchByArticle = $('#btnAdvencedSearchByArticle');

    selectAB = $('#selectAB');
    selectService_ = $('#selectService_');
    selectAnnee_1 = $('#selectAnnee_1');
    selectMois_1 = $('#selectMois_1');

    lblService = $('#lblService');
    lblArticleBudgetaire = $('#lblArticleBudgetaire');
    lblPeriodeDeclaration = $('#lblPeriodeDeclaration');
    isAdvance = $('#isAdvance');
    tableDefaillantPaiement = $('#tableDefaillantPaiement');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    modalRechercheAvanceeModelTwo = $('#modalRechercheAvanceeModelTwo');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');

    btnGenerateIS = $('#btnGenerateIS');
    btnGenerateRelance = $('#btnGenerateRelance');
    researchType = $('#researchType');
    modalGetAddEntiteAdministrative = $('#modalGetAddEntiteAdministrative');

    selectAvenue = $('#selectAvenue');
    btnAddTypeAvenue = $('#btnAddTypeAvenue');
    selectQuartier = $('#selectQuartier');
    btnAddTypeQuartier = $('#selectQuartier');
    selectCommune = $('#selectCommune');
    btnAddTypeCommune = $('#btnAddTypeCommune');

    inputNewDateHeureInvitation = $('#inputNewDateHeureInvitation');
    btnChangeDayInvitation = $('#btnChangeDayInvitation');
    modalChangeDayInvitation = $('#modalChangeDayInvitation');
    selectPeriode = $('#selectPeriode');

    btnChangeDayInvitation.on('click', function (e) {

        e.preventDefault();

        if (inputNewDateHeureInvitation.val().trim() == '') {

            alertify.alert('Veuillez saisir la nouvelle date et l\'heure de l\'invitation');
            return;

        } else {

            alertify.confirm('Etes-vous sûre de vouloir changer la date et heure de l\'invitation ?', function () {
                changeDayInvitation();
            });
        }
    });

    btnGenerateIS.on('click', function (e) {
        e.preventDefault();

        if (RelanceExisting == '0') {
            callPrintAndSaveMedV2();
        } else {
            printDocumentV2();
        }
    });

    selectAB.on('change', function (e) {

        if (selectAB.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un impôt valide');
            return;

        } else {

            selectPeriode.attr('disabled', false);

            if (selectAB.val() == '00000000000002282020') {
                isMonthSelected = '1';
                loadMois();
            } else {
                isMonthSelected = '0';
                loadAnnee();
            }
        }

    });

    researchType.on('change', function (e) {

        assujettiCodeValue.val('');

        if (researchType.val() == '1') {
            responsibleObject = null;
            assujettiCodeValue.attr('placeholder', 'Veuillez saisir le nom du contribuable');
            assujettiCodeValue.attr('readOnly', false);

        } else if (researchType.val() == '2') {

            responsibleObject = null;
            assujettiCodeValue.attr('placeholder', 'Veuillez indiquer une avenue');
            assujettiCodeValue.attr('readOnly', false);


        } else if (researchType.val() == '3') {

            responsibleObject = null;
            assujettiCodeValue.attr('placeholder', 'Veuillez indiquer un quartier');
            assujettiCodeValue.attr('readOnly', false);

        } else if (researchType.val() == '4') {

            responsibleObject = null;
            assujettiCodeValue.attr('placeholder', 'Veuillez indiquer une commune');
            assujettiCodeValue.attr('readOnly', false);

        } else {

            assujettiCodeValue.attr('placeholder', 'La recherche se fera sur tous les contribuables');
            assujettiCodeValue.attr('readOnly', true);
        }
    });

    btnGenerateRelance.on('click', function (e) {
        e.preventDefault();

        if (RelanceExisting == '0') {
            callPrintAndSaveRelanceV2();
        } else {
            printDocumentV2();
        }

    });

    btnPrintInvitation = $('#btnPrintInvitation');

    btnPrintInvitation.click(function (e) {
        e.preventDefault();
        var inputDateHeureInvitation = $('#inputDateHeureInvitation');
        if (inputDateHeureInvitation.val().trim() == '') {
            alertify.alert('Veuillez saisir la date et l\'heure de l\'invitation');
            return;
        }

        if (isRelance) {

            medObj2.dateHeureInvitation = inputDateHeureInvitation.val();
            $('#medPrintInviteService').modal('hide');
            saveAndPrintRelance(medObj2);

        } else {
            medObj.dateHeureInvitation = inputDateHeureInvitation.val();
            $('#medPrintInviteService').modal('hide');
            saveAndPrintInviteService(medObj);
        }

    });

    lblService = $('#lblService');
    lblSite = $('#lblSite');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    modalAccuserReception = $('#modalAccuserReception');
    medInfoModal = $('#medInfoModal');

    idIconBtnOperation = $('#idIconBtnOperation');
    btnPrintMedOrMep = $('#btnPrintMedOrMep');

    valDateEcheance = $('#valDateEcheance');
    valDateReception = $('#valDateReception');
    valDateCreate = $('#valDateCreate');
    valMedOrMep = $('#valMedOrMep');
    lblMedOrMep = $('#lblMedOrMep');
    divInfoMed = $('#divInfoMed');

    valExercice = $('#valExercice');
    valReference = $('#valReference');
    lblReference = $('#lblReference');

    btnSimpleSearch.on('click', function (e) {

        e.preventDefault();

        if (researchType.val() == '1') {

            if (responsibleObject == null || responsibleObject.codeResponsible == '') {
                alertify.alert('Veuillez d\'abord rechercher et sélectionner un contribuable avant de lancer la recherche');
                return;
            }

        } else if (researchType.val() == '2' || researchType.val() == '3' || researchType.val() == '4') {

            if (valueSearch == '') {
                var value = '<span style="font-weight:bold">' + $('#researchType option:selected').text().toUpperCase() + '</span>';
                alertify.alert('Veuillez d\'abord sélectionner une entité administrative :' + value + ' valide');
                return;
            } else {
                responsibleObject = new Object();
                isAvancedSearch = '0';
                responsibleObject.codeResponsible = '*';
            }

        } else {
            responsibleObject = new Object();
            isAvancedSearch = '0';
            responsibleObject.codeResponsible = '*';
        }

        if (selectAB.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un impôt avant de lancer la recherche');
            return;
        }

        isAvancedSearch = '0';
        codeABSelected = selectAB.val();

        switch (getRegisterType()) {
            case 'SERVICE':
                loadDefaillantDeclaration(isAvancedSearch);
                break;
            case 'MEP':
                loadMep(isAvancedSearch);
                break;
        }
        //assujettiModal.modal('show');
    });

    btnPrintMedOrMep.on('click', function (e) {
        e.preventDefault();
        printMedOrMep(medDocumentPrint);
    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeModelTwo.modal('show');
    });



    /*assujettiCodeValue.on('change', function (e) {
     alert('OK');
     });*/

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAvancedSearch = '1';
        assujettiCodeValue.val(empty);
        assujettiCodeValue.attr('style', 'font-weight:normal;width: 380px');
        loadMep(isAvancedSearch);
    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalRechercheAvanceeByArticle.modal('show');
    });

    btnAdvencedSearchByArticle.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        isAvancedSearch = '1';
        assujettiCodeValue.val(empty);
        assujettiCodeValue.attr('style', 'font-weight:normal;width: 380px');
        loadDefaillantDeclaration(isAvancedSearch);
    });

    printDefaillantDeclaration(empty);

    //getDefaillantDeclaration();

    $('.rapport-defaillant1').click(function (e) {

        e.preventDefault();

        alertify.confirm('Etes-vous sûre de vouloir imprimer le rapport des défaillants déclaration ?', function () {

            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }

            if (tempDefaillantDecList.length != 0) {
                printDataDefaillantDeclaration();
            } else {
                alertify.alert('Aucune donnée n\'est disponible pour imprimer ce rapport');
            }
        });
    });
});

function loadDefaillantDeclaration(typeSearch) {

    switch (typeSearch) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':
            var valuePeriode = '';

            switch (codePeriodicite) {
                case 'PR0032015' :// Mensuelle
                    valuePeriode = $('#selectMois_1 option:selected').text() + '-' + selectAnnee_1.val();
                    break;
                case 'PR0042015' : // Annuelle
                    valuePeriode = selectAnnee_1.val();
                    break;
            }

            lblPeriodeDeclaration.text(valuePeriode);
            lblPeriodeDeclaration.attr('title', valuePeriode);

            lblService.text($('#selectService_ option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService_ option:selected').text().toUpperCase());

            lblArticleBudgetaire.text($('#selectAB option:selected').text().toUpperCase());
            lblArticleBudgetaire.attr('title', $('#selectAB option:selected').text().toUpperCase());

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'articleBudgetaireCode': codeABSelected,
            'codePeriodicite': codePeriodicite,
            'periodValue': selectPeriode.val(),
            'isMonth': isMonthSelected,
            'yearCurrentValue': yearCurrent,
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': typeSearch,
            'valueSearch': valueSearch,
            'choiceSearch': researchType.val(),
            'codeSite': userData.SiteCode,
            'operation': 'loadDefaillantDeclaration'

        },
        beforeSend: function () {

            if (typeSearch == '0') {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
            } else {
                modalRechercheAvanceeByArticle.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
            }
        },
        success: function (response)
        {
            if (typeSearch == '0') {
                $.unblockUI();
            } else {
                modalRechercheAvanceeByArticle.unblock();
            }

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                switch (typeSearch) {
                    case '0':
                        switch (responsibleObject.codeResponsible) {
                            case '*':
                                printDefaillantDeclarationForAllPerson('');
                                alertify.alert('Aucun contribuable  n\'a pas de période en retard de déclaration pour l\'impôt : ' + $('#selectAB option:selected').text());
                                break;
                            default:
                                printDefaillantDeclaration('');
                                alertify.alert('Ce contribuable : ' + assujettiCodeValue.val() + ' n\'a pas de période en retard de déclaration pour l\'impôt : ' + $('#selectAB option:selected').text());
                                break;
                        }

                        break;
                    case '1':
                        switch (responsibleObject.codeResponsible) {
                            case '*':
                                printDefaillantDeclarationForAllPerson('');
                                break;
                            default:
                                printDefaillantDeclaration('');
                                break;
                        }
                        alertify.alert('Aucune période en retard de déclaration ne corresponde au critère de recherche fournis.');
                        break;
                }

            } else {
                medList = JSON.parse(JSON.stringify(response));
                modalRechercheAvanceeByArticle.modal('hide');

                switch (responsibleObject.codeResponsible) {
                    case '*':
                        printDefaillantDeclarationForAllPerson(medList);
                        break;
                    default:
                        printDefaillantDeclaration(medList);
                        break;
                }

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //$.unblockUI();
            if (typeSearch == '0') {
                $.unblockUI();
            } else {
                modalRechercheAvanceeByArticle.unblock();
            }
            showResponseError();
        }
    });
}

function printDefaillantPaiement(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:left">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left">NOTE DE PERCEPTION</th>';
    tableContent += '<th style="text-align:center">DATE ORDO.</th>';
    tableContent += '<th style="text-align:center">DATE ECHEANCE</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var amount = formatNumber(result[i].amountNp, result[i].deviseNp);
        var buttonPrintMep = '';

        switch (result[i].printExist) {
            case '0':
                buttonPrintMep = '<button class="btn btn-warning" onclick="callPrintAndSaveMep(\'' + result[i].numeroNp
                        + '\',\'' + result[i].assujettiCode + '\',\'' + result[i].assujettiName + '\',\'' + result[i].adresseCode
                        + '\',\'' + result[i].dateOrdonnancement
                        + '\',\'' + result[i].exerciceNp
                        + '\',\'' + result[i].printExist + '\',\'' + result[i].dateEcheanceNp
                        + '\',\'' + result[i].numeroMed + '\',\'' + amount + '\',\'' + result[i].numeroNc + '\',\'' + result[i].amountNp + '\')"><i class="fa fa-print"></i></button>';
                break;
            case '1':
                buttonPrintMep = '<button class="btn btn-warning" onclick="printDocument(\'' + result[i].numeroMed + '\')"><i class="fa fa-list"></i></button>';
                break;
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:center;width:8%;vertical-align:middle">' + result[i].exerciceNp + '</td>';
        tableContent += '<td style="text-align:left;width:30%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:9%;vertical-align:middle">' + result[i].numeroNp + '</td>';
        tableContent += '<td style="text-align:center;width:9%;vertical-align:middle">' + result[i].dateOrdonnancement + '</td>';
        tableContent += '<td style="text-align:center;width:9%;color:red;vertical-align:middle;font-weight:bold">' + result[i].dateEcheanceNp + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].articleBudgetaireName.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold">' + amount + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonPrintMep + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableDefaillantPaiement.html(tableContent);
    tableDefaillantPaiement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 5,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 5
    });

}

function printDefaillantDeclaration(result) {

    //console.log(result)

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:left">CONTRIBUABLE</th>';
    tableContent += '<th style="text-align:left">BIEN</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:left">IMPÔT</th>';
    tableContent += '<th style="text-align:center">ECHEANCE DECLARATION</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    sumCDF = 0;
    sumUSD = 0;
    frequence = 0;

    var numberRowsPD = result.length;
    periodeDeclarationIDList = [];

    for (var i = 0; i < result.length; i++) {

        if (result[i].amountPeriodeDeclaration > 0 && result[i].devisePeriodeDeclaration !== '') {

            var objPD = new Object();

            objPD.id = result[i].periodeDeclarationId;
            objPD.principalDu = result[i].amountPeriodeDeclaration;
            frequence += 1;

            var buttonPrintMed = '';
            var buttonPrintRelance = '';
            var resteMois;
            var penaliteDu;

            if (parseInt(result[i].moisRetard) > 0) {

                resteMois = parseInt(result[i].moisRetard - 1)

                var penaliteDuFirstMonth = (((result[i].amountPeriodeDeclaration * 25) / 100) * 1);

                var penaliteDuOtherMonth = (((result[i].amountPeriodeDeclaration * 2) / 100) * resteMois);

                penaliteDu = (penaliteDuFirstMonth + penaliteDuOtherMonth);

            } else {

                penaliteDu = (((result[i].amountPeriodeDeclaration * 25) / 100) * result[i].moisRetard);
            }

            objPD.penaliteDu = penaliteDu;

            periodeDeclarationIDList.push(objPD);

            if (result[i].devisePeriodeDeclaration === 'CDF') {
                sumCDF += (result[i].amountPeriodeDeclaration + penaliteDu);
            } else if (result[i].devisePeriodeDeclaration === 'USD') {
                sumUSD += (result[i].amountPeriodeDeclaration + penaliteDu);
            }

            var txtBtnRelance = 'Générer une relance';
            txtBtnRelance = '';


            RelanceExisting = result[i].printExist;

            switch (result[i].printExist) {

                case '0':

                    if (result[i].relanceExist == '0') {

                        if (controlAccess('PRINT_RELANCE_BEFORE_IS')) {

                            buttonPrintMed = '<button class="btn btn-warning" onclick="callPrintAndSaveRelance(\'' + result[i].periodeDeclarationId
                                    + '\',\'' + result[i].assujettiCode
                                    + '\',\'' + result[i].assujettiName + '\',\'' + result[i].adresseCode
                                    + '\',\'' + result[i].adresseName + '\',\'' + result[i].echeanceDeclaration
                                    + '\',\'' + result[i].articleBudgetaireCode + '\',\'' + result[i].articleBudgetaireName
                                    + '\',\'' + result[i].amountPeriodeDeclaration + '\',\'' + result[i].printExist
                                    + '\',\'' + result[i].periodeDeclaration
                                    + '\',\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')"> Générer une relance <i class="fa fa-print"></i></button>'

                        } else {
                            buttonPrintMed = '<button class="btn btn-warning" onclick="callPrintAndSaveMed(\'' + result[i].periodeDeclarationId
                                    + '\',\'' + result[i].assujettiCode
                                    + '\',\'' + result[i].assujettiName + '\',\'' + result[i].adresseCode
                                    + '\',\'' + result[i].adresseName + '\',\'' + result[i].echeanceDeclaration
                                    + '\',\'' + result[i].articleBudgetaireCode + '\',\'' + result[i].articleBudgetaireName
                                    + '\',\'' + result[i].amountPeriodeDeclaration + '\',\'' + result[i].printExist
                                    + '\',\'' + result[i].periodeDeclaration
                                    + '\',\'' + result[i].numeroMed + '\',\'' + penaliteDu + '\')"> Générer une IS <i class="fa fa-print"></i></button>';
                        }

                    } else {
                        buttonPrintMed = '';
                    }
                    break;
                case '1':
                    buttonPrintMed = '';
                    //buttonPrintMed = '<button class="btn btn-default" onclick="printDocument(\'' + result[i].numeroDocument + '\',\'' + result[i].numeroMed + '\')"> Ré-imprimer IS <i class="fa fa-print"></i></button>';
                    break;
            }

            var bienTitle = '';

            if (result[i].isImmobilier === '1') {

                var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
                var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + result[i].usageName + '</span>';
                var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
                var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + result[i].communeName + '</span>';
                var quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + result[i].quartierName + '</span>';
                var periodiciteInfo = 'Périodicité : ' + '<span style="font-weight:bold">' + result[i].periodiciteName + '</span>';

                var bienInfo = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';

                var adresseInfo = result[i].adresseBien.toUpperCase();

                descriptionBien = bienInfo + '<br/><br/>' + natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<br/><br/>' + periodiciteInfo + '<hr/>' + adresseInfo;
                bienTitle = result[i].intituleBien + '\n' + 'Nature : ' + result[i].libelleTypeBien + '\n' + 'Usage : ' +
                        result[i].usageName + '\n' + 'Catégorie : ' + result[i].tarifName + '\n' + 'Commune : ' +
                        result[i].communeName.replace('(Ville : <span style=\'font-weight:bold\'>', '').replace('</span>)', '') + '\n' + adresseInfo;

            } else {

                var bienInfo2 = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';
                var natureInfo2 = 'Genre : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
                var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
                var adresseInfo2 = result[i].adresseBien.toUpperCase();

                descriptionBien = bienInfo2 + '<br/><br/>' + natureInfo2 + '<br/>' + categorieInfo2 + '<hr/>' + adresseInfo2;
                bienTitle = result[i].descriptionBien;
            }

            var data = new Object();
            data.periodeDeclaration = result[i].periodeDeclaration;
            data.assujettiNameComposite = result[i].assujettiName + '\n\n' + result[i].adresseName;
            data.bien = bienTitle;
            data.amountDu = result[i].amountPeriodeDeclaration + ' ' + result[i].devisePeriodeDeclaration + '\n\nPénalité dûe: ' + penaliteDu + ' ' + result[i].devisePeriodeDeclaration + '\n\nNbre. mois de retard: ' + result[i].moisRetard;
            data.penalitedue = result[i].articleBudgetaireName;
            data.articleBudgetaireName = result[i].articleBudgetaireName;
            data.echeanceDeclaration = result[i].echeanceDeclaration;
            tempDefaillantDecList.push(data);

            amountTotalPrincipal += result[i].amountPeriodeDeclaration;
            amountTotalPenalite += penaliteDu;

            var principalDuInfo = 'PRINCIPAL DÛ : ' + '<span style="font-weight:bold;color:green">' + formatNumber(result[i].amountPeriodeDeclaration, result[i].devisePeriodeDeclaration) + '</span>';
            var penaliteDuInfo = 'PENALITE DÛE : ' + '<span style="font-weight:bold;color:red">' + formatNumber(penaliteDu, result[i].devisePeriodeDeclaration) + '</span>';
            var monthLaterInfo = 'NBRE. MOIS RETARD : ' + '<span style="font-weight:bold;color:red">' + result[i].moisRetard + '</span>';
            var amountDuInfo = principalDuInfo + '<hr/>' + penaliteDuInfo + '<hr/>' + monthLaterInfo;

            buttonPrintMed = '';
            buttonPrintRelance = '';

            tableContent += '<tr>';
            tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + result[i].periodeDeclaration + '</td>';
            tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
            tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + descriptionBien + '</td>';
            tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + amountDuInfo + '</td>';
            tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].articleBudgetaireName.toUpperCase() + '</td>';
            tableContent += '<td style="text-align:center;width:10%;vertical-align:middle;color:red;font-weight:bold">' + result[i].echeanceDeclaration + '</td>';
            tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonPrintMed + buttonPrintRelance + '</td>';
            tableContent += '</tr>';
            tableContent += '<hr/>';

        }
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="3" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableDefaillants.html(tableContent);
    tableDefaillants.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste des biens ici ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: true,
        lengthChange: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 25,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(3).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD') +
                    '<hr/>' +
                    'TOTAL BIEN : ' + frequence
                    );
        },
        dom: 'Bfrtip', columnDefs: [
            {"visible": false, "targets": 4}
        ], order: [[4, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(4, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="7">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    if (numberRowsPD > 0) {

        if (controlAccess('PRINT_RELANCE_BEFORE_IS')) {
            btnGenerateIS.attr('style', 'display:none');
            btnGenerateRelance.attr('style', 'display:inline');
        } else {
            btnGenerateIS.attr('style', 'display:inline');
            btnGenerateRelance.attr('style', 'display:none');
        }

        btnGenerateIS.attr('disabled', false);
        btnGenerateRelance.attr('disabled', false);

    } else {

        if (controlAccess('PRINT_RELANCE_BEFORE_IS')) {
            btnGenerateIS.attr('style', 'display:none');
            btnGenerateRelance.attr('style', 'display:inline');
        } else {
            btnGenerateIS.attr('style', 'display:inline');
            btnGenerateRelance.attr('style', 'display:none');
        }

        btnGenerateIS.attr('disabled', true);
        btnGenerateRelance.attr('disabled', true);
    }
}

function printDefaillantDeclarationForAllPerson(result) {

    //console.log(result)

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:left">CONTRIBUABLE</th>';
    tableContent += '<th style="text-align:left">BIEN</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:left">IMPÔT</th>';
    tableContent += '<th style="text-align:center">ECHEANCE DECLARATION</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    sumCDF = 0;
    sumUSD = 0;
    frequence = 0;

    var numberRowsPD = result.length;

    for (var i = 0; i < result.length; i++) {

        if (result[i].amountPeriodeDeclaration > 0 && result[i].devisePeriodeDeclaration !== '') {

            var objPD = new Object();

            objPD.id = result[i].periodeDeclarationId;
            objPD.principalDu = result[i].amountPeriodeDeclaration;

            frequence += 1;

            var buttonPrintMed = '';
            var buttonPrintRelance = '';
            var resteMois;
            var penaliteDu;

            if (parseInt(result[i].moisRetard) > 0) {

                resteMois = parseInt(result[i].moisRetard - 1)

                var penaliteDuFirstMonth = (((result[i].amountPeriodeDeclaration * 25) / 100) * 1);

                var penaliteDuOtherMonth = (((result[i].amountPeriodeDeclaration * 2) / 100) * resteMois);

                penaliteDu = (penaliteDuFirstMonth + penaliteDuOtherMonth);

            } else {

                penaliteDu = (((result[i].amountPeriodeDeclaration * 25) / 100) * result[i].moisRetard);
            }

            objPD.penaliteDu = penaliteDu;

            periodeDeclarationIDList.push(objPD);

            if (result[i].devisePeriodeDeclaration === 'CDF') {
                sumCDF += (result[i].amountPeriodeDeclaration + penaliteDu);
            } else if (result[i].devisePeriodeDeclaration === 'USD') {
                sumUSD += (result[i].amountPeriodeDeclaration + penaliteDu);
            }

            var txtBtnRelance = 'Générer une relance';
            txtBtnRelance = '';

            RelanceExisting = result[i].printExist;

            var bienTitle = '';

            if (result[i].isImmobilier === '1') {

                var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
                var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + result[i].usageName + '</span>';
                var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
                var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + result[i].communeName + '</span>';
                var quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + result[i].quartierName + '</span>';
                var periodiciteInfo = 'Périodicité : ' + '<span style="font-weight:bold">' + result[i].periodiciteName + '</span>';

                var bienInfo = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';

                var adresseInfo = result[i].adresseBien.toUpperCase();

                descriptionBien = bienInfo + '<br/><br/>' + natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<br/><br/>' + periodiciteInfo + '<hr/>' + adresseInfo;
                bienTitle = result[i].intituleBien + '\n' + 'Nature : ' + result[i].libelleTypeBien + '\n' + 'Usage : ' +
                        result[i].usageName + '\n' + 'Catégorie : ' + result[i].tarifName + '\n' + 'Commune : ' +
                        result[i].communeName.replace('(Ville : <span style=\'font-weight:bold\'>', '').replace('</span>)', '') + '\n' + adresseInfo;

            } else {

                var bienInfo2 = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';
                var natureInfo2 = 'Genre : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
                var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
                var adresseInfo2 = result[i].adresseBien.toUpperCase();

                descriptionBien = bienInfo2 + '<br/><br/>' + natureInfo2 + '<br/>' + categorieInfo2 + '<hr/>' + adresseInfo2;
                bienTitle = result[i].descriptionBien;
            }

            var data = new Object();
            data.periodeDeclaration = result[i].periodeDeclaration;
            data.assujettiNameComposite = result[i].assujettiName + '\n\n' + result[i].adresseName;
            data.bien = bienTitle;
            data.amountDu = result[i].amountPeriodeDeclaration + ' ' + result[i].devisePeriodeDeclaration + '\n\nPénalité dûe: ' + penaliteDu + ' ' + result[i].devisePeriodeDeclaration + '\n\nNbre. mois de retard: ' + result[i].moisRetard;
            data.penalitedue = result[i].articleBudgetaireName;
            data.articleBudgetaireName = result[i].articleBudgetaireName;
            data.echeanceDeclaration = result[i].echeanceDeclaration;
            tempDefaillantDecList.push(data);

            amountTotalPrincipal += result[i].amountPeriodeDeclaration;
            amountTotalPenalite += penaliteDu;

            var principalDuInfo = 'PRINCIPAL DÛ : ' + '<span style="font-weight:bold;color:green">' + formatNumber(result[i].amountPeriodeDeclaration, result[i].devisePeriodeDeclaration) + '</span>';
            var penaliteDuInfo = 'PENALITE DÛE : ' + '<span style="font-weight:bold;color:red">' + formatNumber(penaliteDu, result[i].devisePeriodeDeclaration) + '</span>';
            var monthLaterInfo = 'NBRE. MOIS RETARD : ' + '<span style="font-weight:bold;color:red">' + result[i].moisRetard + '</span>';
            var amountDuInfo = principalDuInfo + '<hr/>' + penaliteDuInfo + '<hr/>' + monthLaterInfo;

            var nameAssujettiWithButton = '';

            if (researchType.val() == '*' || researchType.val() == '2' 
                    || researchType.val() == '3' || researchType.val() == '3') {

                var btnAction = '';
                var btnUpdateDayInvitation = '';

                if (controlAccess('PRINT_RELANCE_BEFORE_IS')) {

                    if (RelanceExisting == '0') {
                        btnAction = '<br/><br/><button class="btn btn-success"  onclick="callPrintAndSaveRelanceV3(\'' + result[i].assujettiCode + '\',\'' + result[i].idBien + '\',\'' + result[i].abCode + '\',\'' + result[i].printExist + '\',\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')"> Générer Relance pour toutes ces p&eacute;riodes <i class="fa fa-print"></i></button>';
                    } else {

                        if (result[i].articleBudgetaireCode == '00000000000002282020' || result[i].articleBudgetaireCode == '00000000000002292020'
                                || result[i].articleBudgetaireCode == '00000000000002352021'
                                || result[i].articleBudgetaireCode == '00000000000002362021') {

                            btnUpdateDayInvitation = '';
                            //btnUpdateDayInvitation = '&nbsp;&nbsp;<button class="btn btn-success"  onclick="callModalChangeDayInvitation(\'' + result[i].numeroMed + '\')"> Modifier jour invitation <i class="fa fa-edit"></i></button>';

                        } else {
                            btnUpdateDayInvitation = '';
                        }

                        btnAction = '<br/><br/><button class="btn btn-warning"  onclick="callPrintAndSaveRelanceV3(\'' + result[i].assujettiCode + '\',\'' + result[i].idBien + '\',\'' + result[i].abCode + '\',\'' + result[i].printExist + '\',\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')"> Ré-imprimer la Relance <i class="fa fa-print"></i></button>' + btnUpdateDayInvitation;

                    }

                } else {
                    if (RelanceExisting == '0') {
                        btnAction = '<br/><br/><button class="btn btn-success"  onclick="callPrintAndSaveMedV3(\'' + result[i].assujettiCode + '\',\'' + result[i].idBien + '\',\'' + result[i].abCode + '\',\'' + result[i].printExist + '\',\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')"> Générer Invitation de service pour toutes ces p&eacute;riodes <i class="fa fa-print"></i></button>';
                    } else {
                        btnAction = '<br/><br/><button class="btn btn-warning"  onclick="callPrintAndSaveMedV3(\'' + result[i].assujettiCode + '\',\'' + result[i].idBien + '\',\'' + result[i].abCode + '\',\'' + result[i].printExist + '\',\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')"> Ré-imprimer invitation service <i class="fa fa-print"></i></button>';
                    }
                }

                //buttonPrintMed = '';
                //buttonPrintRelance = '';

                nameAssujettiWithButton = result[i].assujettiNameComposite + '' + btnAction
            } else {
                nameAssujettiWithButton = result[i].assujettiNameComposite;
            }
            tableContent += '<tr>';
            tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + result[i].periodeDeclaration + '</td>';
            //tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
            tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + nameAssujettiWithButton + '</td>';
            tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + descriptionBien + '</td>';
            tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + amountDuInfo + '</td>';
            tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].articleBudgetaireName.toUpperCase() + '</td>';
            tableContent += '<td style="text-align:center;width:10%;vertical-align:middle;color:red;font-weight:bold">' + result[i].echeanceDeclaration + '</td>';
            tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonPrintMed + buttonPrintRelance + '</td>';
            tableContent += '</tr>';
            tableContent += '<hr/>';

        }
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="3" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableDefaillants.html(tableContent);
    tableDefaillants.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste des biens ici ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: true,
        lengthChange: false,
        pageLength: 50,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 25,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(3).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD') +
                    '<hr/>' +
                    'TOTAL BIEN : ' + frequence
                    );
        },
        dom: 'Bfrtip', columnDefs: [
            {"visible": false, "targets": 1}
        ], order: [[1, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(1, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="7">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    switch (researchType.val()) {
        case '1':

            if (numberRowsPD > 0) {

                if (controlAccess('PRINT_RELANCE_BEFORE_IS')) {
                    btnGenerateIS.attr('style', 'display:none');
                    btnGenerateRelance.attr('style', 'display:inline');
                } else {
                    btnGenerateIS.attr('style', 'display:inline');
                    btnGenerateRelance.attr('style', 'display:none');
                }

                btnGenerateIS.attr('disabled', false);
                btnGenerateRelance.attr('disabled', false);

            } else {

                if (controlAccess('PRINT_RELANCE_BEFORE_IS')) {
                    btnGenerateIS.attr('style', 'display:none');
                    btnGenerateRelance.attr('style', 'display:inline');
                } else {
                    btnGenerateIS.attr('style', 'display:inline');
                    btnGenerateRelance.attr('style', 'display:none');
                }

                btnGenerateIS.attr('disabled', true);
                btnGenerateRelance.attr('disabled', true);
            }
            break;

        default:

            btnGenerateIS.attr('style', 'display:none');
            btnGenerateRelance.attr('style', 'display:none');
            break;
    }

}

function callPrintAndSaveMep(numeroNp, assujettiCode, assujettiName, adresseCode, dateOrdonnancement,
        exerciceNp, printExist, dateEcheanceNp, numeroMed, amountNp1, numeroNc, amountNp2) {

    alertify.confirm('Etes-vous sûre de vouloir imprimer une invitation de service ? ', function () {

        mepObj = new Object();

        mepObj.numeroNp = numeroNp;
        mepObj.assujettiCode = assujettiCode;
        mepObj.assujettiName = assujettiName;
        mepObj.adresseCode = adresseCode;
        mepObj.dateOrdonnancement = dateOrdonnancement;
        mepObj.exerciceNp = exerciceNp;
        mepObj.printExist = printExist;
        mepObj.dateEcheanceNp = dateEcheanceNp;
        mepObj.numeroMed = numeroMed;
        mepObj.amountNp1 = amountNp1;
        mepObj.amountNp2 = amountNp2;
        mepObj.numeroNc = numeroNc;

        saveAndPrintMep(mepObj);
    });
}

function callPrintAndSaveMed(periodeID, assujettiCode, assujettiName,
        adresseCode, adresseName, echeanceDeclaration, articleBudgetaireCode,
        articleBudgetaireName, amountPeriodeDeclaration, printExist, periodeDeclaration, numeroMed, penaliteDu) {

    medObj = new Object();

    medObj.periodeDeclarationId = periodeID;
    medObj.exercice = periodeDeclaration;
    medObj.assujettiCode = assujettiCode;
    medObj.assujettiName = assujettiName;
    medObj.adresseCode = adresseCode;
    medObj.adresseName = adresseName;
    medObj.echeance = echeanceDeclaration;
    medObj.articleBudgetaireCode = articleBudgetaireCode;
    medObj.articleBudgetaireName = articleBudgetaireName;
    medObj.amountPeriodeDeclaration = amountPeriodeDeclaration;
    medObj.printExist = printExist;
    medObj.numeroMed = numeroMed;
    medObj.penaliteDu = penaliteDu;
    medObj.typeMed = 'SERVICE';
    medObj.numberPd = selectPeriode.val();

    $('#medPrintInviteService').modal('show');
}

function callPrintAndSaveMedV2() {

    medObj = new Object();

    medObj.periodeDeclarationId = medList[0].periodeDeclarationId;
    medObj.exercice = null;
    medObj.assujettiCode = medList[0].assujettiCode;
    medObj.assujettiName = medList[0].assujettiName;
    medObj.adresseCode = medList[0].adresseCode;
    medObj.adresseName = medList[0].adresseName;
    medObj.echeance = medList[0].echeanceDeclaration;
    medObj.articleBudgetaireCode = selectAB.val();
    medObj.articleBudgetaireName = $('#selectAB option:selected').text().toUpperCase();
    medObj.amountPeriodeDeclaration = amountTotalPrincipal;
    medObj.printExist = medList[0].printExist;
    medObj.numeroMed = medList[0].numeroMed;
    medObj.penaliteDu = amountTotalPenalite;
    medObj.typeMed = 'SERVICE';
    medObj.numberPd = selectPeriode.val();
    medObj.periodeDeclarationList = JSON.stringify(periodeDeclarationIDList)

    $('#medPrintInviteService').modal('show');
}


function getSelectedAssujetiData() {
    assujettiCodeValue.val(selectAssujettiData.nomComplet);
    assujettiCodeValue.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;
    isAvancedSearch = '0';

    printDefaillantDeclarationForAllPerson('');
    printDefaillantDeclaration('');

}

function loadDefaillantPaiement(typeSearch) {


    switch (typeSearch) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            lblSite.text($('#selectService option:selected').text().toUpperCase());
            lblSite.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblService.text($('#selectService option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblDateDebut.text(dateDebuts);
            lblDateDebut.attr('title', dateDebuts);

            lblDateFin.text(dateFins);
            lblDateFin.attr('title', dateFins);

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': codeService,
            'codeSite': codeSite,
            'dateDebut': dateDebuts,
            'dateFin': dateFins,
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': typeSearch,
            'operation': 'loadDefaillantPaiement'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                printDefaillantPaiement('');
                alertify.alert('Aucune note de perception en retard de paiement.');
            } else {
                mepList = JSON.parse(JSON.stringify(response));
                printDefaillantPaiement(mepList);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function saveAndPrintInviteService(medObj) {

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'periodeDeclarationId': medObj.periodeDeclarationId,
            'articleBudgetaireCode': medObj.articleBudgetaireCode,
            'assujettiCode': medObj.assujettiCode,
            'echeanceDeclaration': medObj.echeance,
            'periodeDeclaration': medObj.exercice,
            'adresseCode': medObj.adresseCode,
            'articleBudgetaireName': medObj.articleBudgetaireName,
            'assujettiName': medObj.assujettiName,
            'adresseName': medObj.adresseName,
            'amountPeriodeDeclarationToString': medObj.amountPeriodeDeclarationToString,
            'amountPeriodeDeclaration': medObj.amountPeriodeDeclaration,
            'penaliteDu': medObj.penaliteDu,
            'idUser': userData.idUser,
            'printExist': medObj.printExist,
            'numeroMed': medObj.numeroMed,
            'dateHeureInvitation': medObj.dateHeureInvitation,
            'periodeDeclarationList': medObj.periodeDeclarationList,
            'operation': 'printInviteService'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else {

                setTimeout(function () {

                    $.unblockUI();
                    setDocumentContent(response);

                    setTimeout(function () {
                    }, 2000);

                    loadDefaillantDeclaration(isAvancedSearch);

                    window.open('visualisation-document', '_blank');
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function saveAndPrintMep(mepObj) {

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'numeroNp': mepObj.numeroNp,
            'assujettiCode': mepObj.assujettiCode,
            'assujettiName': mepObj.assujettiName,
            'adresseCode': mepObj.adresseCode,
            'dateOrdonnancement': mepObj.dateOrdonnancement,
            'articleBudgetaireCode': mepObj.articleBudgetaireCode,
            'exerciceNp': mepObj.exerciceNp,
            'printExist': mepObj.printExist,
            'dateEcheanceNp': mepObj.dateEcheanceNp,
            'numeroMed': mepObj.numeroMed,
            'amountNp1': mepObj.amountNp1,
            'amountNp2': mepObj.amountNp2,
            'numeroNc': mepObj.numeroNc,
            'idUser': userData.idUser,
            'operation': 'printMep'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else {

                setTimeout(function () {

                    $.unblockUI();
                    setDocumentContent(response);

                    setTimeout(function () {
                    }, 2000);

                    loadMep(isAvancedSearch);

                    window.open('visualisation-document', '_blank');
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function printDocument(numeroReference, medId) {

    var value = '<span style="font-weight:bold">' + numeroReference + '</span>';
    alertify.confirm('Etes-vous sûre de vouloir ré-imprimer l\'invitation de service n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'numero': medId,
                'operation': 'printNoteTaxation'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ré-impression en cours ...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    $.unblockUI();
                    alertify.alert('Aucune invitation de service trouvée correspondant à ce numéro : ' + value);
                    return;
                }

                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });

    });
}

function printDocumentV2() {

    var numeroReference = medList[0].numeroDocument;
    var medId = medList[0].numeroMed;

    var value = '<span style="font-weight:bold">' + numeroReference + '</span>';
    alertify.confirm('Etes-vous sûre de vouloir ré-imprimer l\'invitation de service n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'numero': medId,
                'typeDoc': 'SERVICE',
                'operation': 'printNoteTaxation'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ré-impression en cours ...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    $.unblockUI();
                    alertify.alert('Aucune invitation de service trouvée correspondant à ce numéro : ' + value);
                    return;
                }

                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });

    });
}

function printDocumentV3(medId, numeroReference) {

    //var numeroReference = medList[0].numeroDocument;
    //var medId = medList[0].numeroMed;

    var value = '<span style="font-weight:bold">' + numeroReference + '</span>';
    alertify.confirm('Etes-vous sûre de vouloir ré-imprimer l\'invitation de service n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'numero': medId,
                'typeDoc': 'SERVICE',
                'operation': 'printNoteTaxation'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ré-impression en cours ...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    $.unblockUI();
                    alertify.alert('Aucune invitation de service trouvée correspondant à ce numéro : ' + value);
                    return;
                }

                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });

    });
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function printMedOrMep(document) {

    var msg = '';

    switch (getRegisterType()) {
        case 'SERVICE':
            msg = 'Etes-vous sûre de vouloir imprimer une invitation de service? ';
            break;
        case 'MEP':
            msg = 'Etes-vous sûre de vouloir imprimer une mise en demeure au paiement ? ';
            break;
    }

    alertify.confirm(msg, function () {

        setTimeout(function () {
            setDocumentContent(document);

            setTimeout(function () {
            }, 2000);
            window.open('visualisation-document', '_blank');
        }
        , 1);
    });

}

function refrechDataAfterAccuserReception() {

    switch (getRegisterType()) {
        case 'SERVICE':
            loadDefaillantDeclaration(isAvancedSearch);
            break;
        case 'MEP':
            loadMep(isAvancedSearch);
            break;
    }
}

function getDefaillantDeclaration() {

    var date = new Date();
    var day = dateFormat(date.getDate());
    var month = dateFormat(date.getMonth() + 1);
    var year = date.getFullYear();

    var dateDuJour = day + "-" + month + "-" + year;

    dateDebuts = dateDuJour;
    dateFins = dateDuJour;
    codeService = userData.serviceCode;
    codeSite = userData.SiteCode;

    loadDefaillantDeclaration('1');
}

function printDataDefaillantDeclaration() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    if (tempDefaillantDecList.length > 0) {

        if (isAvancedSearch == 1) {
            docTitle = 'REGISTRE DES DEFAILLANTS DECALRATION';
            if (selectAB.val() == '*' && selectService_.val() == '*') {
                docTitle += ' (Du ' + selectMois_1.val() + ' au ' + selectAnnee_1.val() + ')';
                position = 300;
            } else if (selectAB.val() !== '*' && selectService_.val() == '*') {
                docTitle += ' (' + selectMois_1.val() + ' au ' + selectAnnee_1.val() + ')';
                docTitle += '\nSITE : ' + $('#selectService_ option:selected').text();
                position = 300;
                hauteur = 160;
            } else if (selectAB.val() == '*' && selectService_.val() !== '*') {
                docTitle += ' (Du ' + selectMois_1.val() + ' au ' + selectAnnee_1.val() + ')';
                docTitle += '\nSERVICE : ' + $('#selectService_ option:selected').text();
                position = 300;
                hauteur = 160;
            } else {
                docTitle += ' (Du ' + selectMois_1.val() + ' au ' + selectAnnee_1.val() + ')';
                docTitle += '\nARTICLE BUDGETAIRE : ' + $('#selectAB option:selected').text();
                docTitle += '\nSERVICE : ' + $('#selectService_ option:selected').text();
                position = 300;
                hauteur = 170;
            }
        } else {
            var critere = assujettiCodeValue.val().toUpperCase();
            docTitle = 'REGISTRE DES DEFAILLANTS DECALRATION : ' + critere;
            position = 300;
        }

    } else {
        docTitle = 'REGISTRE DES DEFAILLANTS DECALRATION';
        position = 440;
    }

    var columns = [
        {title: "EXERCICE", dataKey: "periodeDeclaration"},
        {title: "CONTRIBUABLE", dataKey: "assujettiNameComposite"},
        {title: "BIEN", dataKey: "bien"},
        {title: "MONTANT PRINCIPAL DÛ", dataKey: "amountDu"},
        {title: "ECHEANCE DECLARATION", dataKey: "echeanceDeclaration"}];

    var rows = tempDefaillantDecList;
    var pageContent = function (data) {

        doc.setFontSize(10);
        doc.text("REPUBLIQUE DEMOCRATIQUE DU CONGO", 40, 25);
        doc.setFontSize(9);
        doc.text("PROVINCE DU HAUT-KATANGA", 40, 40);
        doc.setFontSize(10);
        doc.text(docTitle, position, 140);

        doc.setFontSize(8);
        doc.text("Imprimé le " + day + '/' + month + '/' + year + ' à ' + hh + ':' + mm + ':' + ss + ' avec E-RECETTES 1.0', 590, 25);
        doc.text("Par : " + userData.nomComplet, 590, 35);
        doc.text("Page : " + data.pageCount, 590, 45);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: {
            periodeDeclaration: {columnWidth: 60, fontSize: 8, overflow: 'linebreak'},
            assujettiNameComposite: {columnWidth: 205, overflow: 'linebreak', fontSize: 8},
            bien: {columnWidth: 250, overflow: 'linebreak', fontSize: 8},
            amountDu: {columnWidth: 120, overflow: 'linebreak', fontSize: 8},
            echeanceDeclaration: {columnWidth: 130, overflow: 'linebreak', fontSize: 8}
        },
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 10 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');
    doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(sumCDF), 540, finalY + 40);
    doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD), 540, finalY + 60);
    window.open(doc.output('bloburl'), '_blank');
}

function callPrintAndSaveRelance(periodeID, assujettiCode, assujettiName,
        adresseCode, adresseName, echeanceDeclaration, articleBudgetaireCode,
        articleBudgetaireName, amountPeriodeDeclaration, printExist, periodeDeclaration, numeroMed, numeroDocument) {

    var value = '<span style="font-weight:bold">' + assujettiName + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir générer la relance pour ce contribuable ' + value + ' ?', function () {

        medObj = new Object();

        medObj.periodeDeclarationId = periodeID;
        medObj.exercice = periodeDeclaration;
        medObj.assujettiCode = assujettiCode;
        medObj.assujettiName = assujettiName;
        medObj.adresseCode = adresseCode;
        medObj.adresseName = adresseName;
        medObj.echeance = echeanceDeclaration;
        medObj.articleBudgetaireCode = articleBudgetaireCode;
        medObj.articleBudgetaireName = articleBudgetaireName;
        medObj.amountPeriodeDeclaration = amountPeriodeDeclaration;
        medObj.printExist = printExist;
        medObj.numeroMed = numeroMed;

        saveAndPrintRelance(medObj);
    });

}


function callPrintAndSaveRelanceV2() {

    var value = '<span style="font-weight:bold">' + medList[0].assujettiName + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir générer la relance pour ce contribuable ' + value + ' ?', function () {

        medObj = new Object();

        medObj.periodeDeclarationId = medList[0].periodeDeclarationId;
        medObj.exercice = null;
        medObj.assujettiCode = medList[0].assujettiCode;
        medObj.assujettiName = medList[0].assujettiName;
        medObj.adresseCode = medList[0].adresseCode;
        medObj.adresseName = medList[0].adresseName;
        medObj.echeance = medList[0].echeanceDeclaration;
        medObj.articleBudgetaireCode = selectAB.val();
        medObj.articleBudgetaireName = $('#selectAB option:selected').text().toUpperCase();
        medObj.amountPeriodeDeclaration = amountTotalPrincipal;
        medObj.printExist = medList[0].printExist;
        medObj.numeroMed = medList[0].numeroMed;
        medObj.penaliteDu = amountTotalPenalite;
        medObj.numberPd = selectPeriode.val();
        medObj.periodeDeclarationList = JSON.stringify(periodeDeclarationIDList)

        saveAndPrintRelance(medObj);
    });

}

function callPrintAndSaveMedV3(assujCode, bienCode, abCode, printExist, numeroMed, referenceDocu) {

    if (printExist == '1') {

        printDocumentV3(numeroMed, referenceDocu);

    } else {

        medObj = new Object();

        var periodeDeclarationIDList2 = [];
        var amountTotalPenalite2 = 0
        var amountTotalPrincipal2 = 0;

        for (var i = 0; i < medList.length; i++) {

            if (medList[i].assujettiCode == assujCode && medList[i].idBien == bienCode && medList[i].abCode == abCode) {

                var resteMois;
                var penaliteDu;

                if (parseInt(medList[i].moisRetard) > 0) {

                    resteMois = parseInt(medList[i].moisRetard - 1)

                    var penaliteDuFirstMonth = (((medList[i].amountPeriodeDeclaration * 25) / 100) * 1);

                    var penaliteDuOtherMonth = (((medList[i].amountPeriodeDeclaration * 2) / 100) * resteMois);

                    penaliteDu = (penaliteDuFirstMonth + penaliteDuOtherMonth);

                } else {

                    penaliteDu = (((medList[i].amountPeriodeDeclaration * 25) / 100) * medList[i].moisRetard);
                }

                amountTotalPenalite2 += penaliteDu;
                amountTotalPrincipal2 += medList[i].amountPeriodeDeclaration;

                medObj.periodeDeclarationId = medList[i].periodeDeclarationId;
                medObj.exercice = null;
                medObj.assujettiCode = medList[i].assujettiCode;
                medObj.assujettiName = medList[i].assujettiName;
                medObj.adresseCode = medList[i].adresseCode;
                medObj.adresseName = medList[i].adresseName;
                medObj.echeance = medList[i].echeanceDeclaration;
                medObj.articleBudgetaireCode = selectAB.val();
                medObj.articleBudgetaireName = $('#selectAB option:selected').text().toUpperCase();
                medObj.printExist = medList[i].printExist;
                medObj.numeroMed = medList[i].numeroMed;
                medObj.numberPd = selectPeriode.val();
                medObj.typeMed = 'SERVICE';

                var objPD = new Object();

                objPD.id = medList[i].periodeDeclarationId;
                objPD.principalDu = medList[i].amountPeriodeDeclaration;
                objPD.penaliteDu = penaliteDu;

                periodeDeclarationIDList2.push(objPD);
            }
        }

        medObj.amountPeriodeDeclaration = amountTotalPrincipal2;
        medObj.penaliteDu = amountTotalPenalite2;
        medObj.periodeDeclarationList = JSON.stringify(periodeDeclarationIDList2);
    }

    $('#medPrintInviteService').modal('show');
}

function callPrintAndSaveRelanceV3(assujCode, bienCode, abCode, printExist, numeroMed, referenceDocu) {

    if (printExist == '1') {
        printDocumentV3(numeroMed, referenceDocu);
    } else {

        medObj2 = new Object();
        var periodeDeclarationIDList2 = [];
        var amountTotalPenalite2 = 0
        var amountTotalPrincipal2 = 0;

        for (var i = 0; i < medList.length; i++) {

            if (medList[i].assujettiCode == assujCode && medList[i].idBien == bienCode && medList[i].abCode == abCode) {

                var resteMois;
                var penaliteDu;

                if (parseInt(medList[i].moisRetard) > 0) {

                    resteMois = parseInt(medList[i].moisRetard - 1)

                    var penaliteDuFirstMonth = (((medList[i].amountPeriodeDeclaration * 25) / 100) * 1);

                    var penaliteDuOtherMonth = (((medList[i].amountPeriodeDeclaration * 2) / 100) * resteMois);

                    penaliteDu = (penaliteDuFirstMonth + penaliteDuOtherMonth);

                } else {

                    penaliteDu = (((medList[i].amountPeriodeDeclaration * 25) / 100) * medList[i].moisRetard);
                }

                amountTotalPenalite2 += penaliteDu;
                amountTotalPrincipal2 += medList[i].amountPeriodeDeclaration;

                medObj2 = new Object();

                medObj2.periodeDeclarationId = medList[i].periodeDeclarationId;
                medObj2.exercice = null;
                medObj2.assujettiCode = medList[i].assujettiCode;
                medObj2.assujettiName = medList[i].assujettiName;
                medObj2.adresseCode = medList[i].adresseCode;
                medObj2.adresseName = medList[i].adresseName;
                medObj2.echeance = medList[i].echeanceDeclaration;
                medObj2.articleBudgetaireCode = selectAB.val();
                medObj2.articleBudgetaireName = $('#selectAB option:selected').text().toUpperCase();
                medObj2.printExist = medList[i].printExist;
                medObj2.numeroMed = medList[i].numeroMed;
                medObj2.numberPd = selectPeriode.val();
                var objPD = new Object();

                objPD.id = medList[i].periodeDeclarationId;
                objPD.principalDu = medList[i].amountPeriodeDeclaration;
                objPD.penaliteDu = penaliteDu;

                periodeDeclarationIDList2.push(objPD);
            }
        }

        medObj2.amountPeriodeDeclaration = amountTotalPrincipal2;
        medObj2.penaliteDu = amountTotalPenalite2;
        medObj2.periodeDeclarationList = JSON.stringify(periodeDeclarationIDList2);

        assujettiNameSelected = '<span style="font-weight:bold">' + medList[0].assujettiName + '</span>';

        switch (abCode) {

            case '00000000000002282020':
            case '00000000000002352021':
            case '00000000000002362021':
            case '00000000000002292020':

                alertify.confirm('Etes-vous sûre de vouloir générer la relance pour ce contribuable ' + assujettiNameSelected + ' ?', function () {
                    isRelance = true;
                    $('#medPrintInviteService').modal('show');
                });

                break;

            default:

                isRelance = false;
                alertify.confirm('Etes-vous sûre de vouloir générer la relance pour ce contribuable ' + assujettiNameSelected + ' ?', function () {
                    saveAndPrintRelance(medObj2);
                });
                break;
        }
    }
}

function printDocumentRelance(medId, numeroReference) {

    var value = '<span style="font-weight:bold">' + numeroReference + '</span>';

    alertify.confirm('Etes-vous sûr de vouloir ré-imprimer la relance n° ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'numero': medId,
                'operation': 'printNoteTaxation'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    $.unblockUI();
                    alertify.alert('Aucune relance trouvée' + value);
                    return;
                }

                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });
    });
}

function loadModalSeachPersonne() {

    if (researchType.val() == '1') {

        assujettiModal.modal('show');

    } else if (researchType.val() == '2' || researchType.val() == '3' || researchType.val() == '4') {

        loadEntityByCode(codeKatanga, true);
        modalGetAddEntiteAdministrative.modal('show');
    }

}

function saveAndPrintRelance(medObj) {


    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'periodeDeclarationId': medObj.periodeDeclarationId,
            'articleBudgetaireCode': medObj.articleBudgetaireCode,
            'assujettiCode': medObj.assujettiCode,
            'echeanceDeclaration': medObj.echeance,
            'periodeDeclaration': medObj.exercice,
            'adresseCode': medObj.adresseCode,
            'articleBudgetaireName': medObj.articleBudgetaireName,
            'assujettiName': medObj.assujettiName,
            'adresseName': medObj.adresseName,
            'amountPeriodeDeclarationToString': medObj.amountPeriodeDeclarationToString,
            'amountPeriodeDeclaration': medObj.amountPeriodeDeclaration,
            'penaliteDu': medObj.penaliteDu,
            'idUser': userData.idUser,
            'printExist': medObj.printExist,
            'numeroMed': medObj.numeroMed,
            'periodeDeclarationList': medObj.periodeDeclarationList,
            'dateHeureInvitation': medObj.dateHeureInvitation,
            'numberPd': medObj.numberPd,
            'operation': 'printRelance'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else {

                setTimeout(function () {

                    $.unblockUI();
                    setDocumentContent(response);

                    setTimeout(function () {
                    }, 2000);

                    periodeDeclarationIDList = [];
                    periodeDeclarationIDList2 = [];

                    loadDefaillantDeclaration(isAvancedSearch);

                    window.open('visualisation-document', '_blank');
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function callModalChangeDayInvitation(medId) {

    medIdSeelcted = medId;

    inputNewDateHeureInvitation.val('');
    $('#modalChangeDayInvitation').modal('show');
}

function changeDayInvitation() {

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'numeroMed': medIdSeelcted,
            'newDateHeureInvitation': inputNewDateHeureInvitation.val(),
            'userId': userData.idUser,
            'operation': 'changeDayInvitation'
        },
        beforeSend: function () {
            modalChangeDayInvitation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Changement en cours ...</h5>'});
        },
        success: function (response)
        {

            modalChangeDayInvitation.unblock();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            } else {

                setTimeout(function () {
                    alertify.alert('Le changement de la date et heure de l\'invitation s\'est effectué avec succès');
                    $('#modalChangeDayInvitation').modal('hide');
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr) {
            modalChangeDayInvitation.unblock();
            showResponseError();
        }

    });

}

function loadMois() {

    selectPeriode.html('');

    var date = new Date();
    var mois = date.getMonth() + 1;

    var dataMois = '<option value ="0"> ' + "--" + '</option>';

    dataMois += '<option value =1> Janvier </option>';
    dataMois += '<option value =2> Février </option>';
    dataMois += '<option value =3> Mars </option>';
    dataMois += '<option value =4> Avril </option>';
    dataMois += '<option value =5> Mai </option>';
    dataMois += '<option value =6> Juin </option>';
    dataMois += '<option value =7> Juillet </option>';
    dataMois += '<option value =8> Aôut </option>';
    dataMois += '<option value =9> Septembre </option>';
    dataMois += '<option value =10> Octobre </option>';
    dataMois += '<option value =11> Novembre </option>';
    dataMois += '<option value =12> Décembre </option>';

    dataMois += '<option value ="13"> ' + "TOUS LES MOIS" + '</option>';

    selectPeriode.html(dataMois.toUpperCase());
    selectPeriode.val(mois - 1);
}

function loadAnnee() {

    selectPeriode.html('');

    var anneeDebut = 2015;
    var date = new Date();
    var anneeFin = date.getFullYear() + 5;

    var dataAnnee = '<option value ="0"> ' + "--" + '</option>';

    while (anneeDebut <= anneeFin) {
        dataAnnee += '<option value =' + anneeDebut + '> ' + anneeDebut + '</option>';
        anneeDebut++;
    }

    dataAnnee += '<option value ="2050"> ' + "TOUTES LES ANNEES" + '</option>';

    yearCurrent = date.getFullYear();

    selectPeriode.html(dataAnnee);
    selectPeriode.val(date.getFullYear());
}
