/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var inputSite, inputAdresseSite, btnSelectAdresse, inputResponsableSite, btnSearchResponsable, btnSaveSite, tableSites;
var modalAdresses, inputLibelle, btnRechercherAdresses, btnAddEntite, tableAdresses, inputNumber, btnValiderAdresse;
var agentModal, btnCallModalSearchResponsable, inputValueResearchAgent, tableResultSearchAgent, btnSelectAgent;

var siteList = [];
var codeSiteSelected;
var codeResponsableSelected;
var codeAdresseSiteSelected;
var btnCleanFields;
var numeroAdress;
var codeProvince,
        codeVille,
        codeDistrict,
        codeCommune,
        codeQuartier,
        codeAvenue,
        idAdresse;

var selectTypeSite;

var divisionList = [];

var cmbDivisionBureau, inputSigleBureau, cmdSelectTypeEntite;

var codeDivisionBureau, codeTypeEntite, codeTypeSite;

$(function () {

    mainNavigationLabel.text('GESTION BUREAU');
    secondNavigationLabel.text('Edition d\'un bureau');

    removeActiveMenu();
    linkMenuSiteBanque.addClass('active');

    codeSiteSelected = empty;
    codeResponsableSelected = empty;
    codeAdresseSiteSelected = empty;

    inputSite = $("#inputSite");
    inputAdresseSite = $("#inputAdresseSite");
    btnSelectAdresse = $("#btnSelectAdresse");
    inputResponsableSite = $("#inputResponsableSite");
    btnSearchResponsable = $("#btnSearchResponsable");
    btnSaveSite = $("#btnSaveSite");
    tableSites = $("#tableSites");

    modalAdresses = $("#modalAdresses");
    inputLibelle = $("#inputLibelle");
    btnRechercherAdresses = $("#btnRechercherAdresses");
    btnAddEntite = $("#btnAddEntite");
    tableAdresses = $("#tableAdresses");
    inputNumber = $("#inputNumber");
    btnValiderAdresse = $("#btnValiderAdresse");
    btnCleanFields = $("#btnCleanFields");
    selectTypeSite = $("#selectTypeSite");


    agentModal = $("#agentModal");
    btnCallModalSearchResponsable = $("#btnCallModalSearchResponsable");
    inputValueResearchAgent = $("#inputValueResearchAgent");
    tableResultSearchAgent = $("#tableResultSearchAgent");
    btnSelectAgent = $("#btnSelectAgent");

    cmbDivisionBureau = $("#cmbDivisionBureau");
    inputSigleBureau = $("#inputSigleBureau");
    cmdSelectTypeEntite = $("#cmdSelectTypeEntite");

    codeDivisionBureau = "0";

    btnSelectAdresse.on('click', function (e) {

        e.preventDefault();

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalAdresses.modal('show');
    });

    btnSaveSite.on('click', function (e) {
        e.preventDefault();
        checkFields();
    });

    btnCallModalSearchResponsable.on('click', function (e) {
        e.preventDefault();
        agentModal.modal('show');
    });

    btnCleanFields.on('click', function (e) {
        e.preventDefault();
        resetFields();
    });

    cmbDivisionBureau.on('change', function () {
        codeDivisionBureau = cmbDivisionBureau.val();
    });

    cmdSelectTypeEntite.on('change', function () {
        codeTypeEntite = cmdSelectTypeEntite.val();
    });

    selectTypeSite.on('change', function () {
        codeTypeSite = selectTypeSite.val();
//        if (codeTypeSite == '2') {
//            cmbDivisionBureau.attr('disabled', true);
//            codeDivisionBureau = "0";
//            cmbDivisionBureau.val("0");
//        } else {
//            cmbDivisionBureau.attr('disabled', false);
//        }
    });

    loadSites();
    dataDivision();
});

function loadSites() {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'userId': userData.idUser,
            'operation': 'loadSite'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des sites en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1') {
                    printTableSite(empty);
                    showResponseError();
                    return;
                } else if (response == '0') {
                    printTableSite(empty);
                    alertify.alert('Aucun site n\'est disponible actuellement.');
                    return;

                } else {
                    siteList = JSON.parse(JSON.stringify(response));
                    printTableSite(siteList);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printTableSite(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:30%">BUREAU</th>';
    tableContent += '<th style="text-align:left;width:30%">ADRESSE</th>';
    tableContent += '<th style="text-align:left;width:15%">RESPONSABLE</th>';
    tableContent += '<th style="text-align:left;width:20%">DIVISION</th>';
    //tableContent += '<th style="text-align:left;width:10%">TYPE SITE</th>';
    //tableContent += '<th style="text-align:left;width:10%">TYPE ENTITE</th>';
    tableContent += '<th style="text-align:center;width:5%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {
        var typeSite = '';
        var typeEntite = '';
        var divisionIntitule = '';

        if (result[i].typeSite === true) {

            typeSite = 'EST SITE PEGAGE';
        } else {
            typeSite = 'EST SITE STANDARD';
        }

        if (result[i].typeEntite == '1') {

            typeEntite = 'COMMUNE';
        } else if (result[i].typeEntite == '2') {
            typeEntite = 'LOCALITE';
        }

        if (result[i].intituleDivision == '') {
            divisionIntitule = '------';
        } else {
            divisionIntitule = result[i].intituleDivision + ' (' + result[i].sigle + ')';
        }

        var btnEditSite = '<button class="btn btn-success" onclick="editSite(\'' + result[i].code + '\')"><i class="fa fa-edit"></i></button>';
        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:30%">' + result[i].intitule + '</td>';
        tableContent += '<td style="text-align:left;width:30%">' + result[i].adresseName + '</td>';
        tableContent += '<td style="text-align:left;width:15%">' + result[i].responsableName + '</td>';
        tableContent += '<td style="text-align:left;width:20%">' + divisionIntitule + '</td>';
        //tableContent += '<td style="text-align:left;width:10%">' + typeEntite + '</td>';
        //tableContent += '<td style="text-align:left;width:10%">' + typeSite + '</td>';
        tableContent += '<td style="text-align:center;width:5%">' + btnEditSite + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableSites.html(tableContent);

    tableSites.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste des bureaux ici",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: true,
        pageLength: 20,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function editSite(code) {

    for (var i = 0; i < siteList.length; i++) {

        if (siteList[i].code == code) {
            alertify.confirm('Voulez-vous modifier ce bureau ?', function () {
                codeSiteSelected = code;
                codeResponsableSelected = siteList[i].responsableCode;
                idAdresse = siteList[i].adresseCode;
                inputSite.val(siteList[i].intitule);
                inputAdresseSite.val(siteList[i].adresseName);
                inputResponsableSite.val(siteList[i].responsableName);
                inputSigleBureau.val(siteList[i].sigle);
                cmbDivisionBureau.val(siteList[i].codeDivision)

                if (siteList[i].typeSite === true) {
                    selectTypeSite.val('2');
                } else {
                    selectTypeSite.val('1');
                }               
                
                if (siteList[i].typeEntite == null || siteList[i].typeEntite == '') {
                    cmdSelectTypeEntite.val('0');
                }else{
                   cmdSelectTypeEntite.val(siteList[i].typeEntite); 
                }

            });
            break;
        }
    }
}

function checkFields() {

    if (inputSite.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir l\'intitulé du bureau');
        return;
    }

    if (inputSigleBureau.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir le sigle du bureau');
        return;
    }

    if (idAdresse == empty) {
        alertify.alert('Veuillez d\'abord rechercher et sélectionner l\'adresse du bureau');
        return;
    }

    /*if (codeResponsableSelected == empty) {
        alertify.alert('Veuillez d\'abord rechercher et sélectionner le responsable du bureau');
        return;
    }*/

    if (selectTypeSite.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner le type du bureau');
        return;
    }
    
    if (selectTypeSite.val() == '1') {
        if (cmbDivisionBureau.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner la division du bureau');
            return;
        }
    }

    /*if (cmdSelectTypeEntite.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner le type entité');
        return;
    }*/

    alertify.confirm('Etes-vous sûre de vouloir enregistrer ce bureau ?', function () {
        saveSite();
    });
}

function saveSite() {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'code': codeSiteSelected,
            'intitule': inputSite.val(),
            'responsable': codeResponsableSelected,
            'numero': numeroAdress,
            'idAdresse': idAdresse,
            'codeAvenue': codeAvenue,
            'codeQuartier': codeQuartier,
            'codeCommune': codeCommune,
            'codeDistrict': codeDistrict,
            'codeVille': codeVille,
            'codeProvince': codeProvince,
            'typeSite': selectTypeSite.val(),
            'codeDivision': codeDivisionBureau,
            'sigle': inputSigleBureau.val(),
            'typeEntite': codeTypeEntite,
            'operation': 'editSite'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement bureau en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {
                    showResponseError();
                    return;

                } else {
                    alertify.alert('L\'enregistrement du site s\'est effectué avec succès');
                    resetFields();
                    loadSites();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function resetFields() {

    codeSiteSelected = empty;
    codeResponsableSelected = empty;
    codeAdresseSiteSelected = empty;

    inputSite.val(empty);
    inputAdresseSite.val(empty);
    inputResponsableSite.val(empty);
    inputSigleBureau.val(empty);
    selectTypeSite.val('0');
    cmdSelectTypeEntite.val('0');
    cmbDivisionBureau.val('0');
}

function getSelectedPersonneData() {
    codeResponsableSelected = selectPersonneData.code;
    inputResponsableSite.val(selectPersonneData.nomComplet);
}

function getSelectedAdresseData() {

    numeroAdress = adresse.numero;

    codeAvenue = adresse.codeAvenue;
    codeQuartier = adresse.codeQuartier;
    codeCommune = adresse.codeCommune;
    codeDistrict = adresse.codeDistrict;
    codeVille = adresse.codeVille;
    codeProvince = adresse.codeProvince;

    inputAdresseSite.val(adresse.chaine + ' N° ' + adresse.numero);
}

function dataDivision() {
    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'userId': userData.idUser,
            'operation': 'getDivision'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucune données retrouvée.');
                return;
            }

            divisionList = $.parseJSON(JSON.stringify(response));

            var data = '<option value="0">--- Sélectionner une division ---</option>';

            for (var i = 0; i < divisionList.length; i++) {
                data += '<option value="' + divisionList[i].codeDivision + '">' + divisionList[i].intituleDivision + '</option>';
            }
            cmbDivisionBureau.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}


