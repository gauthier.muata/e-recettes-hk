/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var tableEditionDivision;

var btnEnregisterDivision;

var cmbEntiteAdminist;

var inputIntituleDivision, inputSigleDivision;

var codeDivision = '';

var checkUpdate = '';

var codeEntite = '';

var divisionList = [];

$(function () {

    mainNavigationLabel.text('DIVISION');
    secondNavigationLabel.text('Edition de division');

    removeActiveMenu();
    linkSubMenuEditerDivision.addClass('active');

    tableEditionDivision = $('#tableEditionDivision');

    btnEnregisterDivision = $('#btnEnregisterDivision');

    cmbEntiteAdminist = $('#cmbEntiteAdminist');

    inputIntituleDivision = $('#inputIntituleDivision');
    inputSigleDivision = $('#inputSigleDivision');

    cmbEntiteAdminist.on('change', function () {
        codeEntite = cmbEntiteAdminist.val();
    });

    btnEnregisterDivision.click(function (e) {
        e.preventDefault();
        saveDivision();
    });



    loadVille();
    loadDivision()
});


function printDivision(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="text-align:left;width:40%" scope="col"> INTITULE </th>';
    header += '<th style="text-align:left;width:40%" scope="col"> VILLE </th>';
    header += '<th style="text-align:left;width:10%" scope="col"> SIGLE </th>';
    header += '<th scope="col"style="width:5%; display:none"></th>';
    header += '<th scope="col"></th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < result.length; i++) {
        body += '<tr>';
        body += '<td>' + result[i].intitule + '</td>';
        body += '<td>' + result[i].intituleVille + '</td>';
        body += '<td>' + result[i].sigle + '</td>';
        body += '<td style="vertical-align:middle;text-align:center">' + '<a onclick="updateDivision(\'' + result[i].code + '\')" class="btn btn-success" title="Modifier la division"><i class="fa fa-edit"></i></a>&nbsp;'
                + '<a onclick="removeDivision(\'' + result[i].code + '\')" class="btn btn-danger" title="Désactiver la division"><i class="fa fa-trash-o"></i></a></td>'
        body += '<td hidden="true">' + result[i].code + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableEditionDivision.html(tableContent);
    tableEditionDivision.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function loadDivision() {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadDivision',
            'codeEntite': codeEntite
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                printDivision('');
                alertify.alert('Aucune données retrouvée.');
                return;
            }

            divisionList = $.parseJSON(JSON.stringify(response));
            codeEntite = '';
            printDivision(divisionList);


        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadVille() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadEntiteAdministrativeBytype',
            'codeType': 4
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucune données retrouvée.');
                return;
            }

            var entiteList = $.parseJSON(JSON.stringify(response));

            var data = '<option value="0">--- Sélectionner une entité ---</option>';

            for (var i = 0; i < entiteList.length; i++) {
                data += '<option value="' + entiteList[i].codeEntite + '">' + entiteList[i].intituleEntite.toUpperCase() + '</option>';
            }
            cmbEntiteAdminist.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function saveDivision() {

    if (checkUpdate != '2') {
        if (inputIntituleDivision.val() == '' || inputIntituleDivision.val() == 'null' || inputIntituleDivision.val() == '0') {
            alertify.alert('Veuillez saisir un intitulé.');
            return;
        }
        if (cmbEntiteAdminist.val() == '' || cmbEntiteAdminist.val() == 'null' || cmbEntiteAdminist.val() == '0') {
            alertify.alert('Veuillez selectionner une ville.');
            return;
        }
        var action;
        if (codeDivision == '') {

            action = 'enregistrer ?';
        } else {
            action = 'modifier ?';
        }
    } else {
        action = 'désactiver ?';
    }


    var message = 'Etes-vous sûrs de vouloir ' + action;

    alertify.confirm(message, function () {

        $.ajax({
            type: 'POST',
            url: 'site_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'saveDivision',
                'code': codeDivision,
                'intitule': inputIntituleDivision.val(),
                'checkUpdate': checkUpdate,
                'sigle': inputSigleDivision.val(),
                'codeEntite': codeEntite
            },
            beforeSend: function () {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Opération en cours...</h5>'});
            },
            success: function (response)
            {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                }
                if (response == '0') {
                    alertify.alert('Aucune données retrouvée.');
                    return;
                }

                if (response == '1') {
                    alertify.alert('Opération effectuée avec succès.');
                    claenDataDivision();
                }
                loadDivision();
            },
            complete: function () {
            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });

    });
}

function updateDivision(code) {

    if (divisionList.length > 0) {
        for (var i = 0; i < divisionList.length; i++) {
            if (divisionList[i].code == code) {
                alertify.confirm('Etes-vous sûrs de vouloir modifier cette division ?', function () {
                    inputIntituleDivision.val(divisionList[i].intitule);
                    inputSigleDivision.val(divisionList[i].sigle);
                    cmbEntiteAdminist.val(divisionList[i].codeEntite);
                    codeDivision = divisionList[i].code;
                    codeEntite = divisionList[i].codeEntite;
                    checkUpdate = 1;
                    btnEnregisterDivision.html('<i class="fa fa-save"></i> Modifier');
                    saveDivision();
                });
                break;
            }
        }
    }


}

function removeDivision(code) {
        checkUpdate = 2;
        codeDivision = code;
        saveDivision();
}

function claenDataDivision() {
    codeDivision = '';
    codeEntite = '';
    cmbEntiteAdminist.val('0');
    inputIntituleDivision.val('');
    inputSigleDivision.val('');
    btnEnregisterDivision.html('<i class="fa fa-save"></i> Enregister');
}


