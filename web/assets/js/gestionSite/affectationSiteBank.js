/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var selectSite, selectBanque, btnAddBankSite, tableSiteBanque;
var siteList, banqueList, banqueSiteList;

$(function () {

    mainNavigationLabel.text('SITE BANQUE');
    secondNavigationLabel.text('Affectation de banques aux sites');

    removeActiveMenu();
    linkMenuSiteBanque.addClass('active');

    selectSite = $("#selectSite");
    selectBanque = $("#selectBanque");
    btnAddBankSite = $("#btnAddBankSite");
    tableSiteBanque = $("#tableSiteBanque");

    btnAddBankSite.on('click', function (e) {

        e.preventDefault();

        if (selectSite.val() === '0') {
            alertify.alert('Veuillez d\'abord sélectionner le site');
            return;
        }

        if (selectBanque.val() === '0') {
            alertify.alert('Veuillez d\'abord sélectionner la banque');
            return;
        }

        alertify.confirm('Etes-vous sûr de vouloir ajouter la banque : ' + $('#selectBanque option:selected').text() + ' ?', function () {
            addBankToSite();
        });
    });

    selectSite.on('change', function (e) {

        if (selectSite.val() === '0') {

            alertify.alert('Veuillez sélectionner un site valide');
            return;

        } else {

            for (var i = 0; i < siteList.length; i++) {

                if (siteList[i].siteCode == selectSite.val()) {

                    switch (siteList[i].bankSiteExist) {
                        case '0':
                            banqueSiteList = '';
                            printTableBanqueSite(empty);
                            break;
                        case '1':
                            banqueSiteList = JSON.parse(siteList[i].bankSiteList);
                            printTableBanqueSite(banqueSiteList);
                            break;
                    }

                }
            }
        }
    });

    loadBanqueSites();
    printTableBanqueSite(empty);

});

function loadBanqueSites() {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'userId': userData.idUser,
            'operation': 'loadDataBanqueSite'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {
                    showResponseError();
                    return;
                } else {

                    var data = JSON.parse(JSON.stringify(response));
                    siteList = JSON.parse(data.siteList);
                    banqueList = JSON.parse(data.banqueList);

                    constituateSelectSite(siteList);
                    constituateSelectBanque(banqueList);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function constituateSelectSite(result) {

    var dataSite = '';

    dataSite += '<option value="0">--</option>';

    for (var i = 0; i < result.length; i++) {
        dataSite += '<option value="' + result[i].siteCode + '">' + result[i].siteName + '</option>';
    }
    selectSite.html(dataSite);
}

function constituateSelectBanque(result) {

    var dataBanque = '';

    dataBanque += '<option value="0">--</option>';

    for (var i = 0; i < result.length; i++) {
        dataBanque += '<option value="' + result[i].codeBanque + '">' + result[i].libelleBanque + '</option>';
    }
    selectBanque.html(dataBanque);
}

function printTableBanqueSite(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:75%">BANQUE</th>';
    tableContent += '<th style="text-align:center;width:15%">STATUS</th>';
    tableContent += '<th style="text-align:center;width:10%"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var btnAction = '';

        var btnActivate = '<button class="btn btn-success" onclick="editAffactationBankSite(\'' + result[i].banqueSiteId + '\',\'' + result[i].status + '\',\'' + result[i].bankName + '\')"><i class="fa fa-check-circle"></i>&nbsp;Activer</button>';
        var btnDesactivate = '<button class="btn btn-danger" onclick="editAffactationBankSite(\'' + result[i].banqueSiteId + '\',\'' + result[i].status + '\',\'' + result[i].bankName + '\')"><i class="fa fa-close"></i>&nbsp;D&eacute;sactiver</button>';

        var statusName = '';

        if (result[i].status == true) {
            statusName = 'Activée';
            btnAction = btnDesactivate;
        } else {
            statusName = 'Désactivée';
            btnAction = btnActivate;
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:75%">' + result[i].bankName + '</td>';
        tableContent += '<td style="text-align:center;width:15%">' + statusName.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:center;width:10%">' + btnAction + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableSiteBanque.html(tableContent);

    tableSiteBanque.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune banque affecter à ce site",
            search: "Rechercher par Banque _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 8,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function addBankToSite() {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'siteCode': selectSite.val(),
            'bankCode': selectBanque.val(),
            'operation': 'addBankToSite'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>L\'ajout de banque est en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {
                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('La banqe : ' + $('#selectBanque option:selected').text() + ' est déjà affecter dans ce site.');
                    return;
                } else {
                    alertify.alert('L\'ajout de la banqe : ' + $('#selectBanque option:selected').text() + ' s\est effectué avec succès');
                    loadBanqueSites();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function editAffactationBankSite(id, status, bank) {

    var msg = '';
    var valueState;

    switch (status) {
        case 'true':
            valueState = 0;
            msg = 'Etes-vous sûre de vouloir désactiver la banque : ' + bank + ' affactée à ce site ?';
            break;
        case 'false':
            valueState = 1;
            msg = 'Etes-vous sûre de vouloir activer la banque : ' + bank + ' affactée à ce site ?';
            break;
    }

    alertify.confirm(msg, function () {
        updateAffactationBankSite(id, valueState);
    });

}

function updateAffactationBankSite(id, status) {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': id,
            'status': status,
            'operation': 'update'
        },
        beforeSend: function () {

            switch (status) {
                case 0:
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>La désactivation de la banque en cours ...</h5>'});
                    break;
                case 1:
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>L\'activation de la banque en cours ...</h5>'});
                    break;
            }

        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {
                    showResponseError();
                    return;

                } else {

                    switch (status) {
                        case 0:
                            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>La désactivation s\'est effectuée avec succès.</h5>'});
                            break;
                        case 1:
                            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>L\'activation s\'est effectuée avec succès</h5>'});
                            break;
                    }

                    setTimeout(function () {
                        loadBanqueSites();
                        printTableBanqueSite(empty);
                    }, 4000);

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}




