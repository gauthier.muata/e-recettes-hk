/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var notePerceptionEdit, selectBanqueEdit, PlageDebutNotePerceptionEdit, PlageFinNotePerceptionEdit, btnSaveEdit;
var tableNotePerceptionBanque, modalMiseAJourNotePerceptionBanque, notePerceptionSelected, selectBanqueSelected;
var PlageDebutNotePerceptionSelected, PlageFinNotePerceptionSelected, btnDeleteNoteSelected, btnSaveSelected;

var idRowSelected;
var notePerceptionBankList = [];
var bankDataList = [];
var selectAction, divNpID, divBanqueNpID, divPlageNpID, divVilleNpID;

var selectVilleNPEdit,
        selectVilleNPSelected;
var btnDisplayAllNote, btnDisplayAllNoteNotUse;
var btnSaveNpBankBatch;

$(function () {

    idRowSelected = empty;

    mainNavigationLabel.text('PARAMETRAGE');
    secondNavigationLabel.text('Gestion des notes de perception banque');
    removeActiveMenu();

    notePerceptionEdit = $('#notePerceptionEdit');
    selectBanqueEdit = $('#selectBanqueEdit');
    PlageDebutNotePerceptionEdit = $('#PlageDebutNotePerceptionEdit');
    PlageFinNotePerceptionEdit = $('#PlageFinNotePerceptionEdit');
    btnSaveEdit = $('#btnSaveEdit');
    tableNotePerceptionBanque = $('#tableNotePerceptionBanque');
    modalMiseAJourNotePerceptionBanque = $('#modalMiseAJourNotePerceptionBanque');
    notePerceptionSelected = $('#notePerceptionSelected');
    selectBanqueSelected = $('#selectBanqueSelected');
    PlageDebutNotePerceptionSelected = $('#PlageDebutNotePerceptionSelected');
    PlageFinNotePerceptionSelected = $('#PlageFinNotePerceptionSelected');
    btnDeleteNoteSelected = $('#btnDeleteNoteSelected');
    btnSaveSelected = $('#btnSaveSelected');

    selectAction = $('#selectAction');
    divNpID = $('#divNpID');
    divBanqueNpID = $('#divBanqueNpID');
    divPlageNpID = $('#divPlageNpID');
    divVilleNpID = $('#divVilleNpID');

    selectVilleNPEdit = $('#selectVilleNPEdit');
    selectVilleNPSelected = $('#selectVilleNPSelected');
    btnDisplayAllNote = $('#btnDisplayAllNote');
    btnDisplayAllNoteNotUse = $('#btnDisplayAllNoteNotUse');
    btnSaveNpBankBatch = $('#btnSaveNpBankBatch');

    selectAction.on('change', function (e) {

        if (selectAction.val() == '') {

            alertify.alert('Veuillez d\'abord sélectionner une action valide avant de poursuivre');

            divNpID.attr('style', 'display:none');
            divBanqueNpID.attr('style', 'display:none');
            divPlageNpID.attr('style', 'display:none');
            divVilleNpID.attr('style', 'display:none');
            btnDeleteNoteSelected.attr('style', 'display:none');

            return;

        } else {

            switch (selectAction.val()) {

                case '1': //Modifier la note de perception

                    divNpID.attr('style', 'display:block');
                    divBanqueNpID.attr('style', 'display:none');
                    divPlageNpID.attr('style', 'display:none');
                    divVilleNpID.attr('style', 'display:none');
                    btnDeleteNoteSelected.attr('style', 'display:none');
                    btnSaveSelected.attr('style', 'display:inline');
                    break;
                case '2': // Modifier la banque
                    divNpID.attr('style', 'display:none');
                    divBanqueNpID.attr('style', 'display:block');
                    divPlageNpID.attr('style', 'display:none');
                    btnDeleteNoteSelected.attr('style', 'display:none');
                    divVilleNpID.attr('style', 'display:none');
                    btnSaveSelected.attr('style', 'display:inline');
                    break;
                case '3': // Modifier la plage de la note
                    divNpID.attr('style', 'display:none');
                    divBanqueNpID.attr('style', 'display:none');
                    btnDeleteNoteSelected.attr('style', 'display:none');
                    divPlageNpID.attr('style', 'display:block');
                    divVilleNpID.attr('style', 'display:block');
                    btnSaveSelected.attr('style', 'display:inline');
                    break;
                case '4': // Supprimer la note
                    divNpID.attr('style', 'display:none');
                    divBanqueNpID.attr('style', 'display:none');
                    divPlageNpID.attr('style', 'display:none');
                    divVilleNpID.attr('style', 'display:none');
                    btnDeleteNoteSelected.attr('style', 'display:inline');
                    btnSaveSelected.attr('style', 'display:none');
                    break;
                case '5': // Supprimer la ville
                    divNpID.attr('style', 'display:none');
                    divBanqueNpID.attr('style', 'display:none');
                    divPlageNpID.attr('style', 'display:none');
                    divVilleNpID.attr('style', 'display:inline');
                    btnDeleteNoteSelected.attr('style', 'display:none');
                    btnSaveSelected.attr('style', 'display:inline');
                    break;
            }
        }
    });

    btnSaveNpBankBatch.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectBanqueEdit.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner la banque d\'affectation');
            selectBanqueEdit.focus();
            return;
        }

        if (selectVilleNPEdit.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner la ville de la banque d\'affectation');
            selectVilleNPEdit.focus();
            return;
        }

        if (PlageDebutNotePerceptionEdit.val() == '') {
            alertify.alert('Veuillez d\'abord fournir la plage de début de la note de perception');
            PlageDebutNotePerceptionEdit.focus();
            return;
        }

        if (PlageFinNotePerceptionEdit.val() == '') {
            alertify.alert('Veuillez d\'abord fournir la plage de fin de la note de perception');
            PlageFinNotePerceptionEdit.focus();
            return;

        }

        var value1 = '<span style="font-weight:bold">' + PlageDebutNotePerceptionEdit.val().toUpperCase().trim() + '</span>';
        var value2 = '<span style="font-weight:bold">' + PlageFinNotePerceptionEdit.val().toUpperCase().trim() + '</span>';

        alertify.confirm('Etes-vous sûre de vouloir enregistrer en lot les notes de perception allant de la plage de ' + value1 + ' à ' + value2 + ' ?', function () {
            saveNotePerceptionBankBatch();
        });

    });

    btnDisplayAllNote.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir afficher toutes les notes de perception banque utilisées ?', function () {
            loadNotePerceptionBank();
        });


    });

    btnDisplayAllNoteNotUse.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir afficher toutes les notes de perception banque non-utilisées ?', function () {
            loadNotePerceptionBankNotUse();
        });


    });

    btnDeleteNoteSelected.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var valueNp = '<span style="font-weight:bold">' + notePerceptionSelected.val() + '</span>';

        alertify.confirm('Etes-vous sûre de vouloir supprimer la note de perception n°' + valueNp + ' ?', function () {
            deleteNotePerceptionBank();
        });

    });

    btnSaveEdit.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();

    });

    btnSaveSelected.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectAction.val() == '') {

            alertify.alert('Veuillez d\'abord sélectionner une action valide avant de poursuivre');

            return;

        } else {

            var valueNp = '<span style="font-weight:bold">' + notePerceptionSelected.val() + '</span>';

            switch (selectAction.val()) {

                case '1': //Modifier la note de perception

                    if (notePerceptionSelected.val() == empty) {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de perception');
                        notePerceptionSelected.focus();
                        return;
                    } else {

                        alertify.confirm('Etes-vous sûre de vouloir modifier la note de perception n°' + valueNp + ' ?', function () {
                            modifyNotePerception();
                        });

                    }

                    break;
                case '2': // Modifier la banque

                    if (selectBanqueSelected.val() == '0') {
                        alertify.alert('Veuillez d\'abord sélectionner une banque d\'affectation');
                        selectBanqueSelected.focus();
                        return;
                    } else {

                        alertify.confirm('Etes-vous sûre de vouloir modifier la banque de la note de perception n°' + valueNp + ' ?', function () {
                            modifyBankNotePerception();
                        });

                    }

                    break;
                case '3': // Modifier la plage de la note

                    if (PlageDebutNotePerceptionSelected.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir la plage de début de la note de perception');
                        PlageDebutNotePerceptionSelected.focus();
                        return;
                    }

                    if (PlageFinNotePerceptionSelected.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir la plage de fin de la note de perception');
                        PlageFinNotePerceptionSelected.focus();
                        return;

                    }

                    alertify.confirm('Etes-vous sûre de vouloir modifier la plage de la note de perception n°' + valueNp + ' ?', function () {
                        modifyPlageNotePerception();
                    });

                    break;
                case '5':

                    if (selectVilleNPSelected.val() == '0') {
                        alertify.alert('Veuillez d\'abord sélectionner la ville de la banque d\'affectation');
                        selectVilleNPSelected.focus();
                        return;
                    }

                    alertify.confirm('Etes-vous sûre de vouloir modifier la ville de la note de perception n°' + valueNp + ' ?', function () {
                        modifyVilleNotePerception();
                    });

                    break;
            }
        }

    });

    loadingDataBank();
    loadFirstRowNotePerceptionBank();

});

function deleteNotePerceptionBank() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idRowSelected,
            'userId': userData.idUser,
            'operation': 'deleteNotePerceptionBank'
        },
        beforeSend: function () {
            modalMiseAJourNotePerceptionBanque.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalMiseAJourNotePerceptionBanque.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('La suppression de la note de perception s\'est effectuée avec succès');
                    modalMiseAJourNotePerceptionBanque.modal('hide');
                    loadFirstRowNotePerceptionBank();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalMiseAJourNotePerceptionBanque.unblock();
            showResponseError();
        }
    });

}

function resetFields() {

    idRowSelected = '';

    notePerceptionEdit.val('');
    //selectBanqueEdit.val('0');
    //PlageDebutNotePerceptionEdit.val('');
    //PlageFinNotePerceptionEdit.val('');

    notePerceptionSelected.val('');
    ///selectBanqueSelected.val('0');
    //PlageDebutNotePerceptionSelected.val('');
    //PlageFinNotePerceptionSelected.val('');

    modalMiseAJourNotePerceptionBanque.modal('hide');
}

function checkFields() {

    if (notePerceptionEdit.val() == empty) {
        alertify.alert('Veuillez d\'abord fournir le numéro de la note de perception');
        notePerceptionEdit.focus();
        return;
    }

    if (selectBanqueEdit.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner la banque d\'affectation');
        selectBanqueEdit.focus();
        return;
    }

    if (selectVilleNPEdit.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner la ville de la banque d\'affectation');
        selectVilleNPEdit.focus();
        return;
    }

    if (PlageDebutNotePerceptionEdit.val() == '') {
        alertify.alert('Veuillez d\'abord fournir la plage de début de la note de perception');
        PlageDebutNotePerceptionEdit.focus();
        return;
    }

    if (PlageFinNotePerceptionEdit.val() == '') {
        alertify.alert('Veuillez d\'abord fournir la plage de fin de la note de perception');
        PlageFinNotePerceptionEdit.focus();
        return;

    }

    var value = '<span style="font-weight:bold">' + notePerceptionEdit.val().toUpperCase().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir enregistrer la note de perception n°' + value + ' ?', function () {
        saveNotePerceptionBankForEdit();
    });
}

function checkFields2() {

    if (notePerceptionSelected.val() == empty) {
        alertify.alert('Veuillez d\'abord fournir le numéro de la note de perception');
        notePerceptionSelected.focus();
        return;
    }

    if (selectBanqueSelected.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner une banque d\'affectation');
        selectBanqueSelected.focus();
        return;
    }

    if (PlageDebutNotePerceptionSelected.val() == '') {
        alertify.alert('Veuillez d\'abord fournir la plage de début de la note de perception');
        PlageDebutNotePerceptionSelected.focus();
        return;
    }

    if (PlageFinNotePerceptionSelected.val() == '') {
        alertify.alert('Veuillez d\'abord fournir la plage de fin de la note de perception');
        PlageFinNotePerceptionSelected.focus();
        return;

    }

    var value = '<span style="font-weight:bold">' + notePerceptionSelected.val().toUpperCase().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir modifier la note de perception n°' + value + ' ?', function () {
        saveNotePerceptionBankForModify();
    });
}

function saveNotePerceptionBankForEdit() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'text',
        crossDomain: false,
        data: {
            'notePerception': notePerceptionEdit.val(),
            'banque': selectBanqueEdit.val(),
            'plageDebut': PlageDebutNotePerceptionEdit.val(),
            'plageFin': PlageFinNotePerceptionEdit.val(),
            'ville': selectVilleNPEdit.val(),
            'userId': userData.idUser,
            'operation': 'saveNotePerceptionBankForEdit'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    var value = '<span style="font-weight:bold">' + notePerceptionEdit.val().toUpperCase().trim() + '</span>';
                    alertify.alert('Désolé, cette note de perception : ' + value + ', est déjà enregsitrée dans le système !');
                    return;

                } else {

                    alertify.alert('L\'enregistrement de la note de perception s\'est effectué avec succès');
                    resetFields();
                    loadFirstRowNotePerceptionBank();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function saveNotePerceptionBankBatch() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'text',
        crossDomain: false,
        data: {
            'banque': selectBanqueEdit.val(),
            'plageDebut': PlageDebutNotePerceptionEdit.val(),
            'plageFin': PlageFinNotePerceptionEdit.val(),
            'ville': selectVilleNPEdit.val(),
            'userId': userData.idUser,
            'operation': 'saveNotePerceptionBankBatch'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    alertify.alert('L\'enregistrement de la note de perception s\'est effectué avec succès');
                    PlageDebutNotePerceptionEdit.val('');
                    PlageFinNotePerceptionEdit.val('');
                    loadFirstRowNotePerceptionBank();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function modifyNotePerception() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idRowSelected,
            'notePerception': notePerceptionSelected.val(),
            'userId': userData.idUser,
            'operation': 'modifyNotePerception'
        },
        beforeSend: function () {
            modalMiseAJourNotePerceptionBanque.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modifification en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalMiseAJourNotePerceptionBanque.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    var value = '<span style="font-weight:bold">' + notePerceptionSelected.val().toUpperCase().trim() + '</span>';
                    alertify.alert('Désolé, cette note de perception : ' + value + ', est déjà enregsitrée dans le système !');
                    return;

                } else {

                    alertify.alert('La modification de la note de perception s\'est effectuée avec succès');
                    notePerceptionSelected.val('');
                    modalMiseAJourNotePerceptionBanque.modal('hide');
                    loadFirstRowNotePerceptionBank();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalMiseAJourNotePerceptionBanque.unblock();
            showResponseError();
        }
    });
}

function modifyBankNotePerception() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idRowSelected,
            'banque': selectBanqueSelected.val(),
            'userId': userData.idUser,
            'operation': 'modifyBankNotePerception'
        },
        beforeSend: function () {
            modalMiseAJourNotePerceptionBanque.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modifification en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalMiseAJourNotePerceptionBanque.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    alertify.alert('La modification de la banque s\'est effectuée avec succès');
                    selectBanqueSelected.val('0');
                    modalMiseAJourNotePerceptionBanque.modal('hide');
                    loadFirstRowNotePerceptionBank();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalMiseAJourNotePerceptionBanque.unblock();
            showResponseError();
        }
    });
}

function modifyPlageNotePerception() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idRowSelected,
            'plageDebut': PlageDebutNotePerceptionSelected.val(),
            'plageFin': PlageFinNotePerceptionSelected.val(),
            'userId': userData.idUser,
            'operation': 'modifyPlageNotePerception'
        },
        beforeSend: function () {
            modalMiseAJourNotePerceptionBanque.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modifification en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalMiseAJourNotePerceptionBanque.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    alertify.alert('La modification de la plage de la note de perception s\'est effectuée avec succès');
                    PlageDebutNotePerceptionSelected.val('');
                    PlageFinNotePerceptionSelected.val('');
                    modalMiseAJourNotePerceptionBanque.modal('hide');
                    loadFirstRowNotePerceptionBank();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalMiseAJourNotePerceptionBanque.unblock();
            showResponseError();
        }
    });
}

function modifyVilleNotePerception() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idRowSelected,
            'ville': selectVilleNPSelected.val(),
            'userId': userData.idUser,
            'operation': 'modifyVilleNotePerception'
        },
        beforeSend: function () {
            modalMiseAJourNotePerceptionBanque.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modifification en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalMiseAJourNotePerceptionBanque.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    alertify.alert('La modification de la ville de la note de perception s\'est effectuée avec succès');
                    selectVilleNPSelected.val('0');
                    modalMiseAJourNotePerceptionBanque.modal('hide');
                    loadFirstRowNotePerceptionBank();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalMiseAJourNotePerceptionBanque.unblock();
            showResponseError();
        }
    });
}


function printTableNotePerceptionBank(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center;width:5%">N°</th>';
    tableContent += '<th style="text-align:left;width:15%">NOTE DE PERCEPTION</th>';
    tableContent += '<th style="text-align:left;width:20%;font-weight:bold">BANQUE</th>';
    tableContent += '<th style="text-align:left;width:15%;font-weight:bold">VILLE</th>';
    tableContent += '<th style="text-align:center;width:15%">NOTE DE TAXATION</th>';
    tableContent += '<th style="text-align:left;width:20%">PLAGE DE LA NOTE</th>';
    tableContent += '<th style="text-align:left;width:25%">INFOS UTILISATION DE LA NOTE</th>';
    tableContent += '<th style="text-align:center;width:5%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var count = 0;
    var countNpUsing = 0;
    var countNpNotUsing = 0;
    var totalNp = result.length;

    for (var i = 0; i < result.length; i++) {

        count++;

        var btnEditNpBanque = '<button class="btn btn-success" onclick="editNotePerceptionBanque(\'' + result[i].id + '\',\'' + result[i].notePerception + '\')"><i class="fa fa-edit"></i></button>';

        var color = '';

        if (result[i].estUtilise == 0) {
            color = 'green';
            countNpNotUsing++;
        } else {
            color = 'red';
            countNpUsing++;
            btnEditNpBanque = '';
        }

        var plageDebut = 'DE : ' + '<span style="font-weight:bold">' + result[i].plageDebut + '</span>';
        var plageFin = 'A : ' + '<span style="font-weight:bold">' + result[i].plageFin + '</span>';

        var plageNpInfo = plageDebut + ' ' + plageFin;

        var dateUtilisationInfo = 'Date utilisation : ' + '<span style="font-weight:bold">' + 'N/A' + '</span>';
        var stateTxtUtilisationInfo = 'Etat : ' + '<span style="font-weight:bold">' + 'NON UTILISEE' + '</span>';

        var nameAgentMaj = '';

        if (result[i].nameAgentMaj !== '') {
            nameAgentMaj = '<span style="font-weight:bold;color:green">' + result[i].nameAgentMaj + '</span>';
        }


        //var nbreImpressionInfo = 'Agent utilisation : ' + '<span style="font-weight:bold">' + nameAgentMaj + '</span>';

        if (result[i].estUtilise > 0) {

            dateUtilisationInfo = 'Date utilisation : ' + '<span style="font-weight:bold;color:red">' + result[i].dateUtilisation + '</span>';
            stateTxtUtilisationInfo = 'Etat : ' + '<span style="font-weight:bold">' + 'DEJA UTILISEE' + '</span>';
            nbreImpressionInfo = 'Nombre impression : ' + '<span style="font-weight:bold">' + result[i].nbreImpression + '</span>';
        }

        //var infoNpUtilisation = dateUtilisationInfo + '<br/>' + stateTxtUtilisationInfo + '<br/>' + nbreImpressionInfo;
        var infoNpUtilisation = dateUtilisationInfo + '<br/>' + stateTxtUtilisationInfo + '<br/><br/>' + nameAgentMaj;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + count + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle;font-weight:bold;color:' + color + '">' + result[i].notePerception + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle;font-weight:bold">' + result[i].banqueName + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle;font-weight:bold">' + result[i].villeName + '</td>';
        tableContent += '<td style="text-align:center;width:15%;vertical-align:middle;font-weight:bold">' + result[i].noteTaxation + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + plageNpInfo + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + infoNpUtilisation + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + btnEditNpBanque + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="3" style="text-align:right;font-size:16px;vertical-align:middle">RESUME : </th><th style="text-align:right;font-size:16px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableNotePerceptionBanque.html(tableContent);

    tableNotePerceptionBanque.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des remises est vide",
            search: "Filtrer la liste des notes ici",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"}
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false, ordering: false,
        pageLength: 50,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(3).footer()).html(
                    'TOTAL NOTE UTILISE : ' + countNpUsing +
                    '<hr/>' +
                    'TOTAL NOTE NON UTILISE : ' + countNpNotUsing +
                    '<hr/>' +
                    'TOTAL GENERAL : ' + totalNp
                    );
        },
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        select: {
            style: 'os',
            blurable: true
        },
        columnDefs: [
            {"visible": false, "targets": 2}
        ],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6;;font-weight:bold" class="group"><td colspan="11">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });
}

function editNotePerceptionBanque(id, np) {

    var value = '<span style="font-weight:bold">' + np.toUpperCase().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir afficher les informations de la note de perception n°' + value + ' ?', function () {

        var bankDataList = JSON.parse(userData.banqueUserList);

        var dataBanque;
        dataBanque += '<option value="0">--</option>';

        for (var i = 0; i < bankDataList.length; i++) {
            dataBanque += '<option value="' + bankDataList[i].banqueCode + '">' + bankDataList[i].banqueName.toUpperCase() + '</option>';
        }

        selectBanqueSelected.html(dataBanque);

        divNpID.attr('style', 'display:none');
        divBanqueNpID.attr('style', 'display:none');
        divPlageNpID.attr('style', 'display:none');
        btnDeleteNoteSelected.attr('style', 'display:none');
        btnSaveSelected.attr('style', 'display:inline');

        for (var i = 0; i < notePerceptionBankList.length; i++) {

            if (notePerceptionBankList[i].id == id) {

                idRowSelected = id;

                notePerceptionSelected.val(notePerceptionBankList[i].notePerception);
                selectBanqueSelected.val(notePerceptionBankList[i].banqueCode);
                PlageDebutNotePerceptionSelected.val(notePerceptionBankList[i].plageDebut);
                PlageFinNotePerceptionSelected.val(notePerceptionBankList[i].plageFin);
                selectVilleNPSelected.val(notePerceptionBankList[i].villeCode);

                modalMiseAJourNotePerceptionBanque.modal('show');

                break;
            }
        }
    });


}

function loadingDataBank() {

    bankDataList = JSON.parse(userData.banqueUserList);

    var dataBanque;

    dataBanque += '<option value="0">--</option>';

    for (var i = 0; i < bankDataList.length; i++) {
        dataBanque += '<option value="' + bankDataList[i].banqueCode + '">' + bankDataList[i].banqueName.toUpperCase() + '</option>';
    }

    selectBanqueEdit.html(dataBanque);

}

function loadNotePerceptionBank() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadNotePerceptionBank'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {


            $.unblockUI();

            if (response == '-1') {
                printTableNotePerceptionBank('');
                showResponseError();
                return;

            } else {

                notePerceptionBankList = JSON.parse(JSON.stringify(response));

                printTableNotePerceptionBank(notePerceptionBankList);
            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function loadNotePerceptionBankNotUse() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadNotePerceptionBankNotUse'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {


            $.unblockUI();

            if (response == '-1') {
                printTableNotePerceptionBank('');
                showResponseError();
                return;

            } else {

                notePerceptionBankList = JSON.parse(JSON.stringify(response));

                printTableNotePerceptionBank(notePerceptionBankList);
            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function getSelectedAssujetiData() {
    codePersonneSelected = selectAssujettiData.code;
    societeNameEdit.val(selectAssujettiData.nomComplet + ' (Adresse : ' + selectAssujettiData.adresse.toUpperCase() + ')');
}

function saveRemisePeageForModidification() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idRowSelected,
            'codePersonne': codePersonneSelected,
            'codeTarif': selectCategorieSelected.val(),
            'taux': tarifSelected.val(),
            'modePaiement': selectModePaiementSelected.val(),
            'codeSite': selectSiteSelected.val(),
            'userId': userData.idUser,
            'devise': selectDeviseSelected.val(),
            'operation': 'saveNotePerceptionBankForEdit'
        },
        beforeSend: function () {
            modalMiseAJourInfo.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalMiseAJourInfo.unblock();

                if (response == '-1' | response == '0') {
                    showResponseError();
                    return;

                } else {
                    alertify.alert('La modification s\'est effectuée avec succès');
                    modalMiseAJourInfo.modal('hide');
                    resetFields();
                    loadNotePerceptionBank();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalMiseAJourInfo.unblock();
            showResponseError();
        }
    });
}

function loadFirstRowNotePerceptionBank() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadFirstRowNotePerceptionBank'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {


            $.unblockUI();

            if (response == '-1') {
                printTableNotePerceptionBank('');
                showResponseError();
                return;

            } else {

                notePerceptionBankList = JSON.parse(JSON.stringify(response));
                printTableNotePerceptionBank(notePerceptionBankList);
            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}


