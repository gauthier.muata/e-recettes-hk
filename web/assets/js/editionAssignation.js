/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var isReady = false;

var data, action, iEdit, codeSecteur, montantGlobal, assignationId = 0;

var inputMontantTotal, cmbMinistere, cmbService, cmbFiscal, cmbDevise;

var tableSecteurDactivite, tableArticleBudgetaire;

var faitGenerateurList = [], articleBudgetaireList = [], _assignationList;

var DEFAULT_MONEY_CODE = 'CDF';

var btnChargeDonnees, btnSaveAssignation, btnActionDialAssign, btnJoindreDocument;

var divMontantGlobal, divDeviseMonnaie;

var modalMinistereService;

$(function () {

    mainNavigationLabel.text('ASSIGNATION BUDGETAIRE');

    secondNavigationLabel.text('Edition d\'une assignation budgétaire');

    iEdit = $('#iEdit');

    inputMontantTotal = $('#inputMontantTotal');
    formatting($('#inputMontantTotal'));

    divMontantGlobal = $('#divMontantGlobal');
    divDeviseMonnaie = $('#divDeviseMonnaie');

    cmbMinistere = $('#cmbMinistere');
    cmbMinistere.on('change', function () {
        loadFaitGenerateur("");
        var code = cmbMinistere.val();
        if (code == "0") {
            cmbService.html("");
            return;
        }
        loadService(code);
    });

    cmbService = $('#cmbService');
    cmbService.on('change', function () {
        var code = cmbService.val();
        if (code == "0") {
            printFaitGenerateur("");
            return;
        }
        loadFaitGenerateur(code);
    });

    cmbFiscal = $('#cmbFiscal');
    cmbFiscal.on('change', function () {
        var exercice = cmbFiscal.val();
        loadAssignation(exercice);
    })

    cmbDevise = $('#cmbDevise');
    cmbDevise.on('change', function () {
        var code = cmbDevise.val();
        if (code == "0") {
            $('#thMontant').html('Montant');
        } else {
            $('#thMontant').html('Montant (' + code + ')');
        }
    });

    iEdit.click(function (e) {
        e.preventDefault();

        if (action == 'CONTINUE') {
            action = 'EDIT'
            inputMontantTotal.attr('disabled', false);
            btnActionDialAssign.html('Modifier');
            btnActionDialAssign.removeClass('btn-primary');
            btnActionDialAssign.addClass('btn-warning');
        } else {
            action = 'CONTINUE';
            inputMontantTotal.attr('disabled', true);
            btnActionDialAssign.html('Continuer');
            btnActionDialAssign.removeClass('btn-warning');
            btnActionDialAssign.addClass('btn-primary');
        }

    });

    modalMinistereService = $('#modalMinistereService');

    btnChargeDonnees = $('#btnChargeDonnees');
    btnChargeDonnees.click(function (e) {
        e.preventDefault();
        if (isReady) {
            modalMinistereService.modal('show');
        } else {
            $('#modalAssignation').modal('show');
        }
    });

    btnSaveAssignation = $('#btnSaveAssignation');
    btnSaveAssignation.click(function (e) {
        e.preventDefault();

        if (checkFields()) {
            saveDetailsAssignationBudgetaire();
        }
    });

    btnActionDialAssign = $('#btnActionDialAssign');
    btnActionDialAssign.click(function (e) {
        e.preventDefault();


        if (action == 'SAVE' || action == 'EDIT') {
            if (inputMontantTotal.val() == '' || inputMontantTotal.val() == 0) {
                alertify.alert('Veuillez saisir le montant total');
            }
            saveAssignationBudgetaire();
        } else {
            $('#inputFiscal').val($('#cmbFiscal option:selected').text());
            $('#inputDevise').val($('#cmbDevise option:selected').text());
            $('#modalAssignation').modal('hide');
        }

        if (!isReady) {
            isReady = true;
            btnChargeDonnees.trigger('click');
        }
    });

    btnJoindreDocument = $('#btnJoindreDocument');
    btnJoindreDocument.on('click', function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (archivesDepotDeclar == '') {
            initUpload('', 'codeTypeDoc');
        } else {
            initUpload(archivesDepotDeclar, codeTypeDoc);
        }

    });

    tableSecteurDactivite = $('#tableSecteurDactivite');

    tableArticleBudgetaire = $('#tableArticleBudgetaire');

    loadExercice();

    loadDevise();

    loadMinistere();

    printFaitGenerateur('');

    printArticleBudgetaire('');

//    $('#modalAssignation').modal('show');

});

function send(operation) {

    $.ajax({
        type: 'POST',
        url: 'assignationBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: data,
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            callback(JSON.parse(JSON.stringify(response)), operation);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function callback(response, operation) {

    var content;
    switch (operation) {
        case 'loadDevise':
            content = '<option value="0">--- Sélectionner ---</option>';
            for (var i = 0; i < response.length; i++) {
                content += '<option value="' + response[i].code + '">' + response[i].intitule + '</option>';
            }
            cmbDevise.html(content);

            cmbDevise.val(DEFAULT_MONEY_CODE);
            cmbDevise.attr('disabled', true);
            break;
        case 'loadMinistere':
            content = '<option value="0">--- Sélectionner ---</option>';
            for (var i = 0; i < response.length; i++) {
                content += '<option value="' + response[i].code + '">' + response[i].intitule + '</option>';
            }
            cmbMinistere.html(content);
            break;
        case 'loadServices':
            content = '<option value="0">--- Sélectionner ---</option>';
            for (var i = 0; i < response.length; i++) {
                content += '<option value="' + response[i].code + '">' + response[i].intitule + '</option>';
            }
            cmbService.html(content);
            break;
        case 'loadFaitGenerateurs':
            faitGenerateurList = response;
            printFaitGenerateur(response);
            break;
        case 'loadArticleBudgetaire':
            codeSecteur = cmbService.val();
            articleBudgetaireList = response == '0' ? [] : response;
            printArticleBudgetaire();
            break;
        case 'saveAssignationBudgetaire':
            if (response == '0') {
                alertify.alert('Echec lors de l\'enregistrement de l\'assignation!');
                return;
            }

            assignationId = +response;
            $('#inputFiscal').val($('#cmbFiscal option:selected').text());
            $('#inputDevise').val($('#cmbDevise option:selected').text());
            $('#modalAssignation').modal('hide');

            break;
        case 'loadAssignByExercice':
            btnActionDialAssign.show();
            divMontantGlobal.attr('style', 'display:block');
            divDeviseMonnaie.attr('style', 'display:block')
            if (response == '0') {
                assignationId = 0;
                inputMontantTotal.val('');
                inputMontantTotal.attr('disabled', false);
                btnActionDialAssign.html('Enregistrer');
                if (btnActionDialAssign.hasClass('btn-warning')) {
                    btnActionDialAssign.removeClass('btn-warning');
                    btnActionDialAssign.addClass('btn-primary');
                }
                iEdit.attr('style', 'display:none');
                action = 'SAVE';
            } else {
                var data = JSON.parse(JSON.stringify(response));
                assignationId = data.id;
                inputMontantTotal.val(data.montantGlobal);
                inputMontantTotal.attr('disabled', true);
                btnActionDialAssign.html('Continuer');
                iEdit.attr('style', 'float:right;position:fixed;margin-left:10px;display:inline');
                action = 'CONTINUE';
            }
            break;
        case 'saveDetailsAssignationBudgetaire':
            if (response == '1') {
                alertify.alert('Enregistrement effectué avec succès !');
                setTimeout(function () {
                    location.reload(true);
                }
                , 3000);
            } else {
                alertify.alert('Echec lors de l\'enregistrement de l\'assignation!');
            }
            break;
    }

}

function loadAssignation(exercice) {
    data = {};
    data.operation = 'loadAssignByExercice';
    data.exerciceFiscal = exercice;
    send(data.operation);
}

function loadExercice() {

    var date = new Date();
    var annee = date.getFullYear();

    var dataAnnee = '<option value =' + annee + '> ' + annee + '</option>';
    annee = annee + 1;
    dataAnnee += '<option value =' + annee + '> ' + annee + '</option>';
    cmbFiscal.html(dataAnnee);

    loadAssignation(cmbFiscal.val());
}

function loadDevise() {
    data = {};
    data.operation = 'loadDevise';
    send(data.operation);
}

function loadMinistere() {
    data = {};
    data.operation = 'loadMinistere';
    send(data.operation);
}

function loadService(codeMinistere) {
    data = {};
    data.operation = 'loadServices';
    data.codeMinistere = codeMinistere;
    send(data.operation);
}

function loadFaitGenerateur(codeService) {
    data = {};
    data.operation = 'loadFaitGenerateurs';
    data.codeService = codeService;
    send(data.operation);
}

function loadArticleBudgetaire(code) {

    modalMinistereService.modal('hide');

    $('#lblMinistere').html($('#cmbMinistere option:selected').text());
    $('#lblSecteurDactivite').html($('#cmbService option:selected').text());
    $('#lblFaitGenerateur').html(getSelectedFaitGenerateur(code));

    data = {};
    data.operation = 'loadArticleBudgetaire';
    data.codeAG = code;
    send(data.operation);
}

function getSelectedFaitGenerateur(code) {
    for (var i = 0; i < faitGenerateurList.length; i++) {
        if (faitGenerateurList[i].code == code) {
            return faitGenerateurList[i].intitule;
        }
    }
    return '';
}

function checkFields() {

    montantGlobal = 0;

    if (articleBudgetaireList.length == 0 || articleBudgetaireList == null) {
        alertify.alert('Aucun article budgétaire n\'est disponible pour faire l\'assignation bugétaire.\nVeuillez d\'abord charger les données.');
        return false;
    }

    if (cmbDevise.val() == '0') {
        alertify.alert('Veuillez choisir une devise');
        return false;
    }

    var numberChecked = 0;

    for (var i = 0; i < _assignationList.length; i++) {

        var isChecked = _assignationList[i].checked;
        var montant = _assignationList[i].montant;
        var intitule = _assignationList[i].intitule;

        if (isChecked) {
            numberChecked++;
            if (montant == 0 || montant == '' || montant == null) {
                alertify.alert('Veuillez renseigner le montant de l\'article : ' + intitule);
                return false;
            }
            montantGlobal += montant;
        }
    }

    if (numberChecked == 0) {
        alertify.alert('Veuillez côcher les articles budgétaires à prendre en compte dans l\'assignation.');
        return;
    }

    return true;

}

function saveAssignationBudgetaire() {

    alertify.confirm('Etes-vous sûre de vouloir enregistrer ?', function () {
        data = {};
        data.id = assignationId;
        data.exercice = cmbFiscal.val();
        data.montantGlobal = replaceAll(inputMontantTotal.val(), ',', '');
        data.devise = cmbDevise.val();
        data.idUser = userData.idUser;
        data.operation = 'saveAssignationBudgetaire';
        send(data.operation);
    });
}

function saveDetailsAssignationBudgetaire() {

    alertify.confirm('Etes-vous sûre de vouloir enregistrer ?', function () {
        data = {};
        data.assignationId = assignationId;
        data.idUser = userData.idUser;
        data.devise = cmbDevise.val();
        data.details = JSON.stringify(filterAssignationList());
        data.operation = 'saveDetailsAssignationBudgetaire';
        send(data.operation);
    });
}

function getAmount(code) {
    for (var i = 0; i < _assignationList.length; i++) {
        if (_assignationList[i].code == code) {
            var montant = ($('#input_' + code).val());
            //montant = +montant.replace(',','');
            montant = +replaceAll(montant, ',', '');
            _assignationList[i].montant = montant;
            var checkId = 'check_' + code;
            if (montant == 0 || montant == '') {
                _assignationList[i].checked = false;
                document.getElementById(checkId).checked = false;
            } else {
                if (document.getElementById(checkId).checked == false) {
                    _assignationList[i].checked = true;
                    document.getElementById(checkId).checked = true;
                }
            }
            break;
        }
    }
}

function getChoose(code) {
    for (var i = 0; i < _assignationList.length; i++) {
        if (_assignationList[i].code == code) {
            var isChecked = document.getElementById('check_' + code).checked;
            _assignationList[i].checked = isChecked;
            if (!isChecked) {
                _assignationList[i].montant = 0;
                $('#input_' + code).val('');
            }
            break;
        }
    }
}

function printFaitGenerateur(faitGenerateurList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> Fait générateur </th>';
    header += '<th scope="col"> Forme juridique </th>';
    header += '<th ></th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < faitGenerateurList.length; i++) {

        body += '<tr>';
        body += '<td>' + faitGenerateurList[i].intitule + '</td>';
        body += '<td>' + faitGenerateurList[i].intituleForme + '</td>';
        body += '<td style="vertical-align:middle;text-align:center"><a onclick="loadArticleBudgetaire(\'' + faitGenerateurList[i].code + '\')" class="btn btn-success"><i class="fa fa-check"></i></a></td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableSecteurDactivite.html(tableContent);
    var dtSecteurDactivite = tableSecteurDactivite.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 3,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    $('#tableSecteurDactivite tbody').on('click', 'tr', function () {
        var data = dtSecteurDactivite.row(this).data();
    });
}

function printArticleBudgetaire() {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:10%" scope="col"> Code officiel</th>';
    header += '<th scope="col"> Article budégtaire </th>';
    header += '<th scope="col" id="thMontant"> Montant (' + DEFAULT_MONEY_CODE + ')</th>';
    header += '<th style="width:10px;text-align:center" scope="col" id="thMontant"> Choix </th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < articleBudgetaireList.length; i++) {

        var codeAB = articleBudgetaireList[i].code;

        body += '<tr >';
        body += '<td>' + articleBudgetaireList[i].codeofficiel + '</td>';
        body += '<td>' + articleBudgetaireList[i].intitule + '</td>';
        body += '<td><input onchange=getAmount(\'' + codeAB + '\') style="width:100%;text-align:right" type="text" class="inputMont form-control" id="input_' + codeAB + '"></td>';
        body += '<td><input onchange=getChoose(\'' + codeAB + '\') style="width:100%;text-align:center" type="checkbox" class="form-control" id="check_' + codeAB + '"></td>';
        body += '</tr>';

        var detailAssignation = {};
        detailAssignation.code = codeAB;
        detailAssignation.intitule = articleBudgetaireList[i].intitule;
        detailAssignation.montant = 0;
        detailAssignation.checked = false;
        _assignationList.push(detailAssignation);
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableArticleBudgetaire.html(tableContent);

    formatting($('.inputMont'));

    var dtArticleBudgetaire = tableArticleBudgetaire.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher un article _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
//        select: {
//            style: 'os',
//            blurable: true
//        },
        datalength: 3
    });
    $('#tableArticleBudgetaire tbody').on('click', 'tr', function () {
        var data = dtArticleBudgetaire.row(this).data();
    });
}

function filterAssignationList() {
    var list = [];
    for (var i = 0; i < _assignationList.length; i++) {
        if (_assignationList[i].montant != 0 && _assignationList[i].montant != '') {
            list.push(_assignationList[i]);
        }
    }
    return list;
}

_assignationList = [];
