/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnSaveRemiseSelected,
        btnDeleteRemise,
        tarifSelected,
        selectModePaiementSelected,
        selectCategorieSelected,
        societeNameSelectd,
        modalMiseAJourInfo,
        tableConfigTaux,
        btnSaveRemiseEdit,
        selectModePaiementEdit,
        tarifEdit,
        selectCategorieEdit,
        btnSearchSociete;

var selectSiteEdit,
        selectSiteSelected;

var societeNameEdit, selectCategorieEdit;

var codePersonneSelected, idRowSelected;
var resultDataSite;
var configTauxExerciceList;

var axeList, categorieList;

var newCategorieRemiseModal,
        selectNewCategorie,
        btnValidateINewCategorieRemise;

var selectImpotEdit, selectExerciceEdit, selectFormeJuridiqueEdit, selectCategorieEdit, selectNatureEdit,
        selectLocaliteEdit, tauxEdit, selectUniteEdit, checkBoxMultiplyVB, btnSaveConfigEdit, tableConfigTaux;

var selectImpotEdit2, selectExerciceEdit2, selectFormeJuridiqueEdit2, selectCategorieEdit2, selectNatureEdit2,
        selectLocaliteEdit2, tauxEdit2, selectUniteEdit2, checkBoxMultiplyVB2;

var modalEditConfigTauxExercice;
var uniteList;

var btnSave, btnDelete;

$(function () {

    idRowSelected = empty;

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('Configuration de taux pour exercices anterieurs');
    removeActiveMenu();

    btnAddTarifSite = $('#btnAddTarifSite');
    btnSaveRemiseSelected = $('#btnSaveRemiseSelected');
    btnDeleteRemise = $('#btnDeleteRemise');
    tarifSelected = $('#tarifSelected');
    selectModePaiementSelected = $('#selectModePaiementSelected');
    selectCategorieSelected = $('#selectCategorieSelected');
    societeNameSelectd = $('#societeNameSelectd');
    modalMiseAJourInfo = $('#modalMiseAJourInfo');

    tableConfigTaux = $('#tableConfigTaux');

    btnSaveRemiseEdit = $('#btnSaveRemiseEdit');
    selectModePaiementEdit = $('#selectModePaiementEdit');
    tarifEdit = $('#tarifEdit');
    selectCategorieEdit = $('#selectCategorieEdit');
    btnSearchSociete = $('#btnSearchSociete');

    selectSiteEdit = $('#selectSiteEdit');
    selectSiteSelected = $('#selectSiteSelected');

    societeNameEdit = $('#societeNameEdit');
    selectCategorieEdit = $('#selectCategorieEdit');

    newCategorieRemiseModal = $('#newCategorieRemiseModal');
    selectNewCategorie = $('#selectNewCategorie');
    btnValidateINewCategorieRemise = $('#btnValidateINewCategorieRemise');


    selectImpotEdit = $('#selectImpotEdit');
    selectExerciceEdit = $('#selectExerciceEdit');
    selectFormeJuridiqueEdit = $('#selectFormeJuridiqueEdit');
    selectCategorieEdit = $('#selectCategorieEdit');
    selectNatureEdit = $('#selectNatureEdit');
    selectLocaliteEdit = $('#selectLocaliteEdit');
    tauxEdit = $('#tauxEdit');
    selectUniteEdit = $('#selectUniteEdit');
    checkBoxMultiplyVB = $('#checkBoxMultiplyVB');
    btnSaveConfigEdit = $('#btnSaveConfigEdit');
    tableConfigTaux = $('#tableConfigTaux');
    modalEditConfigTauxExercice = $('#modalEditConfigTauxExercice');

    selectImpotEdit2 = $('#selectImpotEdit2');
    selectExerciceEdit2 = $('#selectExerciceEdit2');
    selectFormeJuridiqueEdit2 = $('#selectFormeJuridiqueEdit2');
    selectCategorieEdit2 = $('#selectCategorieEdit2');
    selectNatureEdit2 = $('#selectNatureEdit2');
    selectLocaliteEdit2 = $('#selectLocaliteEdit2');
    tauxEdit2 = $('#tauxEdit2');
    selectUniteEdit2 = $('#selectUniteEdit2');
    checkBoxMultiplyVB2 = $('#checkBoxMultiplyVB2');

    btnSave = $('#btnSave');
    btnDelete = $('#btnDelete');

    btnSearchSociete.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        assujettiModal.modal('show');
    });

    btnSaveConfigEdit.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectImpotEdit.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un impot valide');
            return;
        }

        if (selectExerciceEdit.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un exercice valide');
            return;
        }

        if (selectFormeJuridiqueEdit.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner la forme juridique valide');
            return;
        }

        if (selectCategorieEdit.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner la catégorie de la nature valide');
            return;
        }

        if (selectNatureEdit.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner la nature valide');
            return;
        }

        if (selectLocaliteEdit.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner le rang ou localité valide');
            return;
        }

        if (tauxEdit.val() == '') {
            alertify.alert('Veuillez d\'abord saisir le taux valide');
            return;
        } else if (tauxEdit.val() <= 0) {
            alertify.alert('Le taux saisi n\'est doit pas être inférieur ou égale à zéro');
            return;
        }

        if (selectUniteEdit.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner une unité valide');
            return;
        }

        alertify.confirm('Etes-vous sûr de vouloir enregistrer ces informations ?', function () {
            saveConfigTauxExercice();
        });

    });

    btnSave.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectImpotEdit2.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un impot valide');
            return;
        }

        if (selectExerciceEdit2.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un exercice valide');
            return;
        }

        if (selectFormeJuridiqueEdit2.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner la forme juridique valide');
            return;
        }

        if (selectCategorieEdit2.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner la catégorie de la nature valide');
            return;
        }

        if (selectNatureEdit2.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner la nature valide');
            return;
        }

        if (selectLocaliteEdit2.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner le rang ou localité valide');
            return;
        }

        if (tauxEdit2.val() == '') {
            alertify.alert('Veuillez d\'abord saisir le taux valide');
            return;
        } else if (tauxEdit2.val() <= 0) {
            alertify.alert('Le taux saisi n\'est doit pas être inférieur ou égale à zéro');
            return;
        }

        if (selectUniteEdit2.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner une unité valide');
            return;
        }

        alertify.confirm('Etes-vous sûr de vouloir modifier ces informations ?', function () {
            saveConfigTauxExercice2();
        });

    });

    selectNatureEdit.on('change', function (e) {

        if (selectNatureEdit.val() !== '0' && selectImpotEdit.val() !== '0') {

            loadTarifByImpotAndNature();

        } else {

            selectLocaliteEdit.html('0');
            selectNatureEdit.attr('disabled', true);
            alertify.alert('Veuillez sélectionner une catégorie valide');
            return;
        }

    });

    selectCategorieEdit.on('change', function (e) {

        if (selectCategorieEdit.val() !== '0') {


            $.ajax({
                type: 'POST',
                url: 'articleBudgetaire_servlet',
                dataType: 'JSON',
                crossDomain: false,
                data: {
                    'categorie': selectCategorieEdit.val(),
                    'operation': 'loadTypeBienByCategorie'
                },
                beforeSend: function () {
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
                },
                success: function (response)
                {

                    setTimeout(function () {

                        $.unblockUI();

                        if (response == '-1') {

                            printTableConfigTauxExercice(empty);
                            showResponseError();
                            return;

                        } else if (response == '0') {

                            selectNatureEdit.html('');
                            selectNatureEdit.attr('disabled', true);
                            alertify.alert('Cette catégorie n\'a pas de natures');
                            return;

                        } else {

                            var result = JSON.parse(JSON.stringify(response));

                            var dataNature;
                            dataNature += '<option value="0">--</option>';

                            for (var i = 0; i < result.length; i++) {
                                dataNature += '<option value="' + result[i].codeTypeBien + '">' + result[i].nameTypeBien + '</option>';
                            }

                            selectNatureEdit.html(dataNature);
                            selectNatureEdit.attr('disabled', false);
                        }
                    }
                    , 1);
                },
                complete: function () {
                },
                error: function () {
                    selectNatureEdit.html('');
                    selectNatureEdit.attr('disabled', true);
                    $.unblockUI();
                    showResponseError();
                }
            });

        } else {

            selectNatureEdit.html('');
            selectNatureEdit.attr('disabled', true);
            alertify.alert('Veuillez sélectionner une catégorie valide');

        }
    });

    btnSaveRemiseEdit.on('click', function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();


    });


    btnSaveRemiseSelected.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields2()();

    });

    btnDelete.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        deleteConfigTauxExercice();
    });

    initDataConfigTauxExerciceAnterieur();
    loadConfigTauxExercice();
    printTableConfigTauxExercice('');
});

function deleteConfigTauxExercice() {

    alertify.confirm('Etes-vous sûr de vouloir supprimer ces informations ?', function () {
        $.ajax({
            type: 'POST',
            url: 'articleBudgetaire_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'id': idRowSelected,
                'userId': userData.idUser,
                'operation': 'deleteConfigTauxExercice'
            },
            beforeSend: function () {
                modalMiseAJourInfo.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
            },
            success: function (response)
            {

                setTimeout(function () {

                    modalMiseAJourInfo.unblock();

                    if (response == '-1' | response == '0') {

                        showResponseError();
                        return;

                    } else {
                        alertify.alert('La suppression s\'est effectuée avec succès');
                        resetFields();
                        modalEditConfigTauxExercice.modal('hide');
                        loadConfigTauxExercice();
                    }
                }
                , 1);
            },
            complete: function () {
            },
            error: function () {
                modalMiseAJourInfo.unblock();
                showResponseError();
            }
        });
    });
}

function resetFields() {

    idRowSelected = empty;
    codePersonneSelected = empty
    societeNameEdit.val(empty);
    selectSiteEdit.val('0');
    selectCategorieEdit.val('0');
    selectModePaiementEdit.val('0');
    tarifEdit.val(empty);
}

function checkFields() {

    if (societeNameEdit.val() == empty) {
        alertify.alert('Veuillez d\'abord rechercher et sélectionner une société');
        societeNameEdit.focus();
        return;
    }

    if (selectSiteEdit.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner un axe péage');
        selectSiteEdit.focus();
        return;
    }

    if (selectCategorieEdit.val() == '0') {
        alertify.alert('Veuillez d\'abord saisir sélectionner la catégorie');
        selectCategorieEdit.focus();
        return;
    }

    if (selectModePaiementEdit.val() == '0') {
        alertify.alert('Veuillez d\'abord saisir sélectionner le mode de paiement');
        selectModePaiementEdit.focus();
        return;

    } else if (selectModePaiementEdit.val() == '2' && selectCategorieEdit.val() !== '*') {

        alertify.alert('Pour le mode paiement (CREDIT), vous devez sélectionner la catégorie (TOUTES LES CATEGORIES)');
        selectModePaiementEdit.focus();
        return;

    }

    if (tarifEdit.val() == '') {
        alertify.alert('Veuillez d\'abord saisir le tarif');
        tarifEdit.focus();
        return;
    } else if (tarifEdit.val() < 0) {
        alertify.alert('Désolé, le tarif ne doit pas être inférieur à zéro');
        tarifEdit.focus();
        return;
    }

    alertify.confirm('Etes-vous sûr de vouloir enregistrer ces informations ?', function () {
        saveRemisePeageForEdit();
    });
}

function checkFields2() {

    if (societeNameSelectd.val() == empty) {
        alertify.alert('Veuillez d\'abord rechercher et sélectionner une société');
        societeNameEdit.focus();
        return;
    }

    if (selectSiteSelected.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner un poste de péage');
        selectSiteSelected.focus();
        return;
    }

    if (selectCategorieSelected.val() == '0') {
        alertify.alert('Veuillez d\'abord saisir sélectionner la catégorie');
        selectCategorieSelected.focus();
        return;
    }

    if (selectModePaiementSelected.val() == '0') {
        alertify.alert('Veuillez d\'abord saisir sélectionner le mode de paiement');
        selectModePaiementSelected.focus();
        return;
    } else if (selectModePaiementSelected.val() == '2' && selectCategorieSelected.val() !== '*') {

        alertify.alert('Pour le mode paiement (CREDIT), vous devez sélectionner la catégorie (TOUTES LES CATEGORIES)');
        selectCategorieSelected.focus();
        return;

    }

    if (tarifSelected.val() == '') {
        alertify.alert('Veuillez d\'abord saisir le tarif');
        tarifSelected.focus();
        return;
    } else if (tarifSelected.val() < 0) {
        alertify.alert('Désolé, le tarif ne doit pas être inférieur à zéro');
        tarifSelected.focus();
        return;
    }

    alertify.confirm('Etes-vous sûr de vouloir modifier ces informations ?', function () {
        saveRemisePeageForModidification();
    });
}

var idRowSelected = '';

function saveConfigTauxExercice() {

    var checkBox = '0';
    if (document.getElementById("checkBoxMultiplyVB").checked) {
        checkBox = '1';
    }

    idRowSelected = '';

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idRowSelected,
            'codeImpot': selectImpotEdit.val(),
            'codeTarif': selectLocaliteEdit.val(),
            'taux': tauxEdit.val(),
            'codeExercice': selectExerciceEdit.val(),
            'codeFormeJuridique': selectFormeJuridiqueEdit.val(),
            'codeBase': selectUniteEdit.val(),
            'codeNature': selectNatureEdit.val(),
            'multiplyVB': checkBox,
            'userId': userData.idUser,
            'operation': 'saveConfigTauxExercice'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('L\'enregistrement s\'est effectué avec succès');
                    tauxEdit.val('');
                    loadConfigTauxExercice();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function saveConfigTauxExercice2() {

    var checkBox = '0';
    if (document.getElementById("checkBoxMultiplyVB2").checked) {
        checkBox = '1';
    }

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idRowSelected,
            'codeImpot': selectImpotEdit2.val(),
            'codeTarif': selectLocaliteEdit2.val(),
            'taux': tauxEdit2.val(),
            'codeExercice': selectExerciceEdit2.val(),
            'codeFormeJuridique': selectFormeJuridiqueEdit2.val(),
            'codeBase': selectUniteEdit2.val(),
            'codeNature': selectNatureEdit2.val(),
            'multiplyVB': checkBox,
            'userId': userData.idUser,
            'operation': 'saveConfigTauxExercice'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('La modification des informations s\'est effectuée avec succès');
                    modalEditConfigTauxExercice.modal('hide');
                    loadConfigTauxExercice();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadConfigTauxExercice() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadConfigTauxExercice'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1') {

                    printTableConfigTauxExercice(empty);
                    showResponseError();
                    return;

                } else if (response == '0') {

                    printTableConfigTauxExercice(empty);

                } else {

                    configTauxExerciceList = JSON.parse(JSON.stringify(response));
                    printTableConfigTauxExercice(configTauxExerciceList);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printTableConfigTauxExercice(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:10%">EXERCICE</th>';
    tableContent += '<th style="text-align:left;width:25%">IMPOT</th>';
    tableContent += '<th style="text-align:left;width:20%">NATURE</th>';
    tableContent += '<th style="text-align:left;width:15%">RANG</th>';
    tableContent += '<th style="text-align:left;width:15%">FORME JURIDIQUE</th>';
    tableContent += '<th style="text-align:right;width:15%">TAUX</th>';
    tableContent += '<th style="text-align:left;width:10%">BASE</th>';
    tableContent += '<th style="text-align:center;width:5%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var btnEditConfigTaux = '<button class="btn btn-success" onclick="editConfigTauxExercice(\'' + result[i].id + '\')"><i class="fa fa-edit"></i></button>';
        var amount = '<span style="font-weight:bold">' + formatNumber(result[i].taux, result[i].devise) + '</span>';
        var valueMultiply = result[i].multiplierVB == '1' ? 'OUI' : 'NON';
        var multiplyInfo = 'à multiplier par la valeur de base : ' + '<span style="font-weight:bold">' + valueMultiply + '</span>';

        var amountInfo = amount + '<br/>' + multiplyInfo;

        tableContent += '<tr>';

        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].annee + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].nameAb + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle;font-weight:bold">' + result[i].nameNature + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].nameTarif + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + result[i].nameFj + '</td>';
        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + amountInfo + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + result[i].nameUnite + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + btnEditConfigTaux + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableConfigTaux.html(tableContent);

    tableConfigTaux.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des remises est vide",
            search: "Filtrer la liste ici _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        select: {
            style: 'os',
            blurable: true
        },
        columnDefs: [
            {"visible": false, "targets": 0}
        ],
        order: [[0, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(0, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });
}

function editConfigTauxExercice(id) {

    alertify.confirm('Etes-vous sûr de vouloir afficher ces informations ?', function () {

        for (var i = 0; i < configTauxExerciceList.length; i++) {

            if (configTauxExerciceList[i].id == id) {

                idRowSelected = id;

                var codeAb = configTauxExerciceList[i].codeAb;
                var annee = configTauxExerciceList[i].annee;
                var codeFj = configTauxExerciceList[i].codeFj;
                var codeNature = configTauxExerciceList[i].codeNature;

                var codeTarif = configTauxExerciceList[i].codeTarif;
                var taux = configTauxExerciceList[i].taux;
                var codeUnite = configTauxExerciceList[i].codeUnite;

                checkBoxMultiplyVB2.checked = configTauxExerciceList[i].multiplierVB == '1' ? true : false;

                var dataAB;
                dataAB += '<option value="0">--</option>';

                for (var i = 0; i < abList.length; i++) {
                    dataAB += '<option value="' + abList[i].abCode + '">' + abList[i].abName + '</option>';
                }

                selectImpotEdit2.html(dataAB);

                var dataFJ;
                dataFJ += '<option value="0">--</option>';

                for (var j = 0; j < formeJuridiqueList.length; j++) {
                    dataFJ += '<option value="' + formeJuridiqueList[j].codeformeJ + '">' + formeJuridiqueList[j].nameformeJ + '</option>';
                }

                selectFormeJuridiqueEdit2.html(dataFJ);

                var dataUnite;
                dataUnite += '<option value="0">--</option>';

                for (var k = 0; k < uniteList.length; k++) {
                    dataUnite += '<option value="' + uniteList[k].uniteCode + '">' + uniteList[k].uniteName + '</option>';
                }

                selectUniteEdit2.html(dataUnite);

                selectImpotEdit2.val(codeAb);
                selectExerciceEdit2.val(annee);
                selectFormeJuridiqueEdit2.val(codeFj);

                switch (codeAb) {

                    case '00000000000002302020': //VIGNETTE:
                        selectCategorieEdit2.val('1');
                        break;
                    case '00000000000002272020': //IMMOBILIER:
                    case '00000000000002292020':
                    case '00000000000002362021':
                    case '00000000000002352021':
                    case '00000000000002282020':

                        selectCategorieEdit2.val('2');
                        break;
                    case '00000000000002312021':  //ICM
                        selectCategorieEdit2.val('3');
                        break;
                    default:
                        selectCategorieEdit2.val('0');
                        break;
                }

                $.ajax({
                    type: 'POST',
                    url: 'articleBudgetaire_servlet',
                    dataType: 'JSON',
                    crossDomain: false,
                    data: {
                        'categorie': selectCategorieEdit2.val(),
                        'operation': 'loadTypeBienByCategorie'
                    },
                    beforeSend: function () {
                        $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
                    },
                    success: function (response)
                    {

                        setTimeout(function () {

                            $.unblockUI();

                            if (response == '-1') {

                                printTableConfigTauxExercice(empty);
                                showResponseError();
                                return;

                            } else {

                                var result = JSON.parse(JSON.stringify(response));

                                var dataNature;
                                dataNature += '<option value="0">--</option>';

                                for (var l = 0; l < result.length; l++) {
                                    dataNature += '<option value="' + result[l].codeTypeBien + '">' + result[l].nameTypeBien + '</option>';
                                }

                                selectNatureEdit2.html(dataNature);
                                selectNatureEdit2.val(codeNature);
                            }
                        }
                        , 1);

                        $.ajax({
                            type: 'POST',
                            url: 'assujetissement_servlet',
                            dataType: 'JSON',
                            headers: {
                                'Access-Control-Allow-Origin': '*'
                            },
                            crossDomain: true,
                            data: {
                                'operation': 'loadTarifByImpotAndNature',
                                'codeAb': codeAb,
                                'codeNature': codeNature
                            },
                            beforeSend: function () {
                                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Récupération des tarifs en cours ...</h5>'});
                            },
                            success: function (response)
                            {

                                if (response == '-1') {
                                    $.unblockUI();
                                    selectLocaliteEdit2.html(empty);
                                    showResponseError();
                                    return;
                                }

                                setTimeout(function () {

                                    $.unblockUI();

                                    var tarifList = $.parseJSON(JSON.stringify(response));
                                    var dataCategorieBien = '<option value ="0">--</option>';

                                    for (var m = 0; m < tarifList.length; m++) {

                                        dataCategorieBien += '<option value =' + tarifList[m].code + '>' + tarifList[m].intitule + '</option>';

                                    }
                                    selectLocaliteEdit2.html(dataCategorieBien);
                                    selectLocaliteEdit2.val(codeTarif);
                                }
                                , 1);
                            },
                            complete: function () {

                            },
                            error: function (xhr, status, error) {
                                //alert(xhr.status);
                                $.unblockUI();
                                selectLocaliteEdit2.html(empty);
                                showResponseError();
                            }

                        });


                        tauxEdit2.val(taux);
                        selectUniteEdit2.val(codeUnite);

                    },
                    complete: function () {
                    },
                    error: function () {
                        selectNatureEdit2.html('');
                        $.unblockUI();
                        showResponseError();
                    }
                });

                modalEditConfigTauxExercice.modal('show');

                break;
            }
        }
    });


}

var tarifList,
        abList,
        formeJuridiqueList,
        natureList;

function initDataConfigTauxExerciceAnterieur() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'userId': userData.idUser,
            'operation': 'initDataConfigTauxExerciceAnterieur'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' | response == '0') {

                showResponseError();
                return;

            } else {

                abList = JSON.parse(response.abList);
                formeJuridiqueList = JSON.parse(response.formeJuridiqueList);
                uniteList = JSON.parse(response.uniteList);

                var dataAB;
                dataAB += '<option value="0">--</option>';

                for (var i = 0; i < abList.length; i++) {
                    dataAB += '<option value="' + abList[i].abCode + '">' + abList[i].abName + '</option>';
                }

                selectImpotEdit.html(dataAB);

                var dataFJ;
                dataFJ += '<option value="0">--</option>';

                for (var i = 0; i < formeJuridiqueList.length; i++) {
                    dataFJ += '<option value="' + formeJuridiqueList[i].codeformeJ + '">' + formeJuridiqueList[i].nameformeJ + '</option>';
                }

                selectFormeJuridiqueEdit.html(dataFJ);

                var dataUnite;
                dataUnite += '<option value="0">--</option>';

                for (var i = 0; i < uniteList.length; i++) {
                    dataUnite += '<option value="' + uniteList[i].uniteCode + '">' + uniteList[i].uniteName + '</option>';
                }

                selectUniteEdit.html(dataUnite);

            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function loadTarifByImpotAndNature() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTarifByImpotAndNature',
            'codeAb': selectImpotEdit.val(),
            'codeNature': selectNatureEdit.val()
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Récupération des tarifs en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                selectLocaliteEdit.html(empty);
                selectLocaliteEdit.attr('disabled', true);
                showResponseError();
                return;
            }
            if (response == '0') {
                $.unblockUI();
                selectLocaliteEdit.html(empty);
                selectLocaliteEdit.attr('disabled', true);
                alertify.alert('Il n\' y a des rangs disponible');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var tarifList = $.parseJSON(JSON.stringify(response));
                var dataCategorieBien = '<option value ="0">--</option>';

                for (var i = 0; i < tarifList.length; i++) {

                    dataCategorieBien += '<option value =' + tarifList[i].code + '>' + tarifList[i].intitule + '</option>';

                }
                selectLocaliteEdit.attr('disabled', false);
                selectLocaliteEdit.html(dataCategorieBien);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            //alert(xhr.status);
            $.unblockUI();
            selectLocaliteEdit.html(empty);
            selectLocaliteEdit.attr('disabled', true);
            showResponseError();
        }

    });
}