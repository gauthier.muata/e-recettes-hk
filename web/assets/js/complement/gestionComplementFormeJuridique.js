/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var tableComplementFormeJuridique, modalComplementFormeJuridique,
        tableResultComplementForme;
var complementObjetList = [];

var cmbFormeJuridique;

var btnAffecterFormeJuridique, btnValiderAffectationForme,
        btnLauchSearchArticleComplementForme;

var codeComplementForme = '';

var codeComplementType;

var listTypeComplement;

var inputValueResearchComplementForme;

$(function () {

    mainNavigationLabel.text('GESTION DES COMPLEMENTS');
    secondNavigationLabel.text('affecter un complément à une forme juridique');

    removeActiveMenu();

    tableComplementFormeJuridique = $('#tableComplementFormeJuridique');
    tableResultComplementForme = $('#tableResultComplementForme');
    
    modalComplementFormeJuridique = $('#modalComplementFormeJuridique');

    cmbFormeJuridique = $('#cmbFormeJuridique');
    
    btnAffecterFormeJuridique = $('#btnAffecterFormeJuridique');
    btnValiderAffectationForme = $('#btnValiderAffectationForme');
    btnLauchSearchArticleComplementForme = $('#btnLauchSearchArticleComplementForme');
    
    inputValueResearchComplementForme = $('#inputValueResearchComplementForme');

    cmbFormeJuridique.on('change', function () {
        codeComplementForme = cmbFormeJuridique.val();
        getComplementFormeByType(codeComplementForme);
    });
    
     btnLauchSearchArticleComplementForme.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
//        if (inputValueResearchComplementForme.val() == '' || inputValueResearchComplementForme.val() == 'null') {
//            alertify.alert('Veuillez fournir un critère de recherche.');
//            return;
//        }        
        getCompleTypeByIntitule();
    });
    
     btnAffecterFormeJuridique.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        if (codeComplementForme == '' || codeComplementForme == 'null') {
            alertify.alert('Veuillez sélectionner une forme juridique.');
            return;
        }
        
        getCompleTypeByIntitule();
        
        modalComplementFormeJuridique.modal('show');
    });
    
     btnValiderAffectationForme.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        var listComplements = preparerAffectationComplement(); 
        
        if (listComplements.length >0){
            affecterComplementForme(JSON.stringify(listComplements));
//            cmbFormeJuridique.trigger('change'); 
//            getComplementFormeByType(codeComplementForme);
        }else{
             alertify.alert('Veuillez sélectionner au moins un complément.');
            return;
        }
        
        

    });

    getFormeJuridique();
    printComplementForme('');
});


function getFormeJuridique() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadFormeJuridique'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else {
                    var dataFormeJuridique = $.parseJSON(JSON.stringify(response));

                    var dataFormeJuriques = '<option value ="0">-- Sélectionner --</option>';
                    for (var i = 0; i < dataFormeJuridique.length; i++) {
                        dataFormeJuriques += '<option value =' + dataFormeJuridique[i].codeFormeJuridique + '>' + dataFormeJuridique[i].libelleFormeJuridique + '</option>';
                    }
                    cmbFormeJuridique.html(dataFormeJuriques);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getComplementFormeByType(codeComplement) {

    $.ajax({
        type: 'POST',
        url: 'complement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getComplementForme',
            'codeComplement': codeComplement
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {

                var result = $.parseJSON(JSON.stringify(response));

                if (result.length > 0) {
                    printComplementForme(result);
                } else {
                    printComplementForme('');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}


function printComplementForme(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="text-align:center;width:80%" scope="col"> TYPE COMPLEMENT </th>';
    header += '<th scope="col"style="width:5%; display:none"></th>';
    header += '<th hidden="true" scope="col">Code</th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < result.length; i++) {

        body += '<td style="text-align:left">' + result[i].intitule + '</td>';
        body += '<td style="vertical-align:middle;">'
                + '<a onclick="removeComplementForme(\'' + result[i].code + '\')" class="btn btn-danger" title="Désactiver ce complément"><i class="fa fa-trash-o"></i></a></td>'
        body += '<td hidden="true">' + result[i].code + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableComplementFormeJuridique.html(tableContent);
    var dttableComplementFormeJuridique = tableComplementFormeJuridique.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function getCompleTypeByIntitule() {

    $.ajax({
        type: 'POST',
        url: 'complement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getComplementType',
            'intituleArticle': inputValueResearchComplementForme.val(),
            'codeFormeJuridique': codeComplementForme
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                printTypeComplement('');
            }

            listTypeComplement = $.parseJSON(JSON.stringify(response));
            if (listTypeComplement.length > 0) {
                printTypeComplement(listTypeComplement);
            } else {
                printTypeComplement('');
            }
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printTypeComplement(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center;width:45%"scope="col">INTITULE</th>';
    tableContent += '<th style="text-align:center;width:35%"scope="col">INTITULE CATEGORIE</th>';
    tableContent += '<th style="width:5%;text-align:center"scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"> Code article </th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';



    for (var i = 0; i < result.length; i++) {

        var firstLineCp = '';

        if (result[i].intitule.length > 70) {
            firstLineCp = result[i].intitule.substring(0, 70).toUpperCase() + ' ...';
        } else {
            firstLineCp = result[i].intitule.toUpperCase();
        }

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;title="' + result[i].intitule.toUpperCase() + '"><span style="font-weight:bold;"></span>' + firstLineCp.toUpperCase() + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + result[i].intituleCategorie + '</td>';
        tableContent += '<td style="text-align:center"><center><input onclick="checkComplement(\'' + result[i].code + '\')" type="checkbox" id="checkBoxSelectComplement' + result[i].code + '" name="checkBoxSelectComplement' + result[i].code + '"></center></td>';
        tableContent += '<td hidden="true">' + result[i].code + '</td>';
        tableContent += '</tr>';

    }
    tableContent += '</tbody>';
    tableResultComplementForme.html(tableContent);

    var myDataTable = tableResultComplementForme.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: true,
        pageLength: 10,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

}

function preparerAffectationComplement() {

    for (var i = 0; i < listTypeComplement.length; i++) {

        var selectComplement = '#checkBoxSelectComplement' + listTypeComplement[i].code;

        var select = $(selectComplement);

        if (select.is(':checked')) {

            var complementObjet = new Object();

            complementObjet.code = listTypeComplement[i].code;
            complementObjet.codeComplementForme = codeComplementForme;
            
            complementObjetList.push(complementObjet);

        }

    }

    return complementObjetList;
}

function affecterComplementForme(complementObjetList) {


    $.ajax({
        type: 'POST',
        url: 'complement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'affecterComplementFormeJuridique',
            'complementObjetList': complementObjetList
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du dépôt déclaration en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'opération effectuée avec succès.');
                  complementObjetList = [];
                  getComplementFormeByType(codeComplementForme);
                   modalComplementFormeJuridique.modal('hide');
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'affectation du complément.');
                     complementObjetList = [];
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function removeComplementForme(code) {

    alertify.confirm('Etes-vous sûrs de vouloir désactiver ?', function () {
        codeComplementType = code;
        desactiverComplement();
    });

}


function desactiverComplement() {


    $.ajax({
        type: 'POST',
        url: 'complement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'desactiverComplement',
            'codeTypeComplement': codeComplementType
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Désactivation du complément en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'opération effectuée avec succès.');
                  complementObjetList = [];
                  getComplementFormeByType(codeComplementForme);
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'affectation du complément.');
                     complementObjetList = [];
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}