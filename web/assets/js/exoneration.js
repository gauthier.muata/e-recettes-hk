/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tempBienList = [],
        tempArticleList = [],
        bienList = [],
        articlesList = [],
        tempPeriodeDeclarationList = [],
        listPeriode = [],
        listTaux = [];

var codeResponsible = '',
        codeTypeAssjetti = '',
        nameResponsible = '',
        selectedBien = '',
        yes4Assujetti = true, //Pour afficher les biens non loués
        fkPersonne,
        bienId,
        abBiens,
        isTypeMobilier,
        typePage,
        lblNameAssujettiModal, lblAdress,
        lblNoteCalcul,
        lblAgentTaxateur,
        lblAgentValidateur,
        lblAgentOrdonnateur,
        archiveContent = '',
        redressementObj = null,
        periodeDeclarationId,
        periodeDeclarationName,
        codeAssujettiSave,
        dateAcquisitionBien,
        namePropriete,
        adressePropriete,
        checkOperation = false,
        assujettissementIDSlected,
        assujettissementId,
        beginPeriodValue,
        communeCodeBienSelected,
        codeNatureBienSelected,
        clickExist,
        stamp_;

var tabBiens = $('#tabBiens'),
        tabArticles = $('#tabArticles'),
        assujettiModal = $('#assujettiModal'),
        lblLegalForm = $('#lblLegalForm'),
        lblNameResponsible = $('#lblNameResponsible'),
        lblAddress = $('#lblAddress'),
        tableBiens = $('#tableBiens'),
        tableArticles = $('#tableArticles'),
        tablePeriodeDeclaration = $('#tablePeriodeDeclaration'),
        modalPeriodeDeclaration = $('#modalPeriodeDeclaration'),
        lbl1 = $('#lbl1'),
        lbl2 = $('#lbl2'),
        lbl3 = $('#lbl3'),
        modalInfoMiseEnQuarantaine = $('#modalInfoMiseEnQuarantaine'),
        modalPermutationBien = $('#modalPermutationBien'),
        valuePeriode2 = $('#valuePeriode2'),
        valueDate2 = $('#valueDate2'),
        valueAgent2 = $('#valueAgent2'),
        valueObservation = $('#valueObservation'),
        valuePeriode1 = $('#valuePeriode1'),
        valueAgent1 = $('#valueAgent1'),
        modalMiseEnQuarantaine = $('#modalMiseEnQuarantaine'),
        textaeraObservation = $('#textaeraObservation'),
        btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable'),
        inputObservationRedressement = $('#inputObservationRedressement'),
        newBase = $('#newBase'),
        spnBaseActuelle = $('#spnBaseActuelle'),
        modalRedressement = $('#modalRedressement'),
        cmbUnite = $('#cmbUnite'),
        btnIRL = $("#btnIRL"),
        btnIF = $("#btnIF"),
        btnRL = $("#btnRL"),
        btnVIGNETTE = $("#btnVIGNETTE"),
        btnICM = $("#btnICM"),
        btnPUBLICITE = $("#btnPUBLICITE"),
        btnRL22M = $("#btnRL22M"),
        btnRL22A = $("#btnRL22A"),
        modalAddPeriodeDeclarationAssujettissement = $("#modalAddPeriodeDeclarationAssujettissement"),
        modalDetailDeclaration = $('#modalDetailDeclaration'),
        btnVoirDeclaration = $('#btnVoirDeclaration'),
        spnPDDepart = $("#spnPDDepart"),
        spnBienAssujSelected = $("#spnBienAssujSelected"),
        tableNewPDAssuj = $("#tableNewPDAssuj"),
        btnAjouterPD = $("#btnAjouterPD"),
        btnPasserAB2 = $('#btnPasserAB2'),
        btnPasserAB21 = $('#btnPasserAB21');

$(function () {

    clickExist = false;
    inputObservationRedressement.val(empty);

    var urlCode = getUrlParameter('id');
    if (jQuery.type(urlCode) !== 'undefined') {
        codeResponsible = atob(urlCode);
        refreshDataAssujetti();
        loadBiens(codeResponsible, '0');
    }

    tabBiens.tab('show');

    btnCallModalSearchResponsable.click(function (e) {
        e.preventDefault();
        yes4Assujetti = true;
        allSearchAssujetti = '1';
        resetSearchAssujetti();
        assujettiModal.modal('show');
    });

    btnAjouterPD.attr('style', 'display:none');

    //vérifier si le bien est de type immobilier
    isTypeMobilier = false;

    btnPasserAB2.click(function (e) {

        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        setBienData(JSON.stringify(tempBienList));

        selectedBien = '';

    });

    btnPasserAB21.click(function (e) {

        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        if (!controlAccess('2004')) {
            alertify.alert('Vous n\'avez pas le droit de faire un assujettissement.');
            return;
        }

        setBienData(JSON.stringify(tempBienList));

        selectedBien = '';

    });

    printBien('');
    printArticles('');

    var paramGuideValue = JSON.parse(getTourParam());

    if (paramGuideValue === 1) {

        Tour.run([
            {
                element: $('#btnCallModalSearchResponsable'),
                content: '<p style="font-size:17px;color:black">Commencer par rechercher un assujetti.</p>',
                language: 'fr',
                position: 'right',
                close: 'false'}
        ]);
    } else {
        Tour.run();
    }

    switch (userData.isSitePeage) {
        case true:
            tabArticles.attr('style', 'display:none');
            break;
        case false:
            tabArticles.attr('style', 'display:inline');
            break;

    }

});

function getSelectedAssujetiData() {

    if (yes4Assujetti == true) {

        codeResponsible = selectAssujettiData.code;
        codeTypeAssjetti = selectAssujettiData.codeForme;
        nameResponsible = selectAssujettiData.nomComplet;

        codeLocataireBien = codeResponsible;
        codePermuteBien = codeResponsible;
        codeAssujettiSave = codeResponsible;

        lblNameResponsible.html(nameResponsible);
        lblLegalForm.html(selectAssujettiData.categorie);
        lblAddress.html(selectAssujettiData.adresse);

        lbl1.show();
        lbl2.show();
        lbl3.show();

        setAssujettiData(JSON.stringify(selectAssujettiData));
        loadBiens(codeResponsible, '0');

    } else {

        if (codeResponsible == selectAssujettiData.code) {
            alertify.alert('Vous ne pouvez pas louer vos propres biens.');
            return;
        }

        codeProprietaireBien = selectAssujettiData.code;
        lblNameProprietaire.html(selectAssujettiData.nomComplet + (' (Propriétaire)'));
        lblAddressProprietaire.html(selectAssujettiData.adresse);

        lblNameProprietairePerm.html(selectAssujettiData.nomComplet + (' (Propriétaire)'));
        lblAddressProprietairePerm.html(selectAssujettiData.adresse);

        loadBiens(codeProprietaireBien, '1');
    }

}

function refreshDataAssujetti() {

    var jsonAssujetti = getAssujettiData();
    var assujettiObj = JSON.parse(jsonAssujetti);

    codeResponsible = assujettiObj.code;
    nameResponsible = assujettiObj.nomComplet;

    lblNameResponsible.html(nameResponsible);
    lblLegalForm.html(assujettiObj.categorie);
    lblAddress.html(assujettiObj.adresse);

    lbl1.show();
    lbl2.show();
    lbl3.show();
}

function loadBiens(personneCode, forLocation) {

    fkPersonne = personneCode;

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBiensPersonne',
            'codePersonne': personneCode,
            'forLocation': forLocation,
            'codeService': userData.serviceCode,
            'cmbTarif': '*',
            'estExonere': true,
            'articleB': AB_EXONERATION
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response) {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();
                var assujettissementList = $.parseJSON(JSON.stringify(response));
                bienList = $.parseJSON(JSON.stringify(assujettissementList.listBiens));
                var articleList = $.parseJSON(JSON.stringify(assujettissementList.listArticleBudgetaires));
                articlesList = articleList;

                if (yes4Assujetti == true) {
                    printBien(bienList);
                    printArticles(articleList);
                } else {

                    var dataBiens = printOtherBien(bienList);

                    if (checkOperation) {
                        if (bienList.length === 0) {
                            alertify.alert('L\assujetti ' + selectAssujettiData.nomComplet + ' n\'a pas des biens à permuter.');
                            return;
                        }
                        cmbPermutationBien.html(dataBiens);
                        modalPermutationBien.modal('show');

                    } else {

                        if (bienList.length === 0) {
                            alertify.alert('L\assujetti ' + selectAssujettiData.nomComplet + ' n\'a pas des biens à mettre en location.');
                            return;
                        }
                        cmbLocationBien.html(dataBiens);
                        modalLocationBien.modal('show');
                    }
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printBien(bienList) {

    tempBienList = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" style="width:20%"> BIEN </th>';
    header += '<th scope="col" style="width:30%"> DESCRIPTION </th>';
    header += '<th scope="col" style="width:30%"> ADRESSE </th>';
    header += '<th scope="col" style="width:10%"> PROPRIETAIRE </th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < bienList.length; i++) {

        var proprietaire = bienList[i].proprietaire;

        var descriptionBien = empty;

        if (bienList[i].isImmobilier === '1') {

            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + bienList[i].usageName + '</span>';
            var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + bienList[i].communeName + '</span>';

            var quartierInfo = 'Quartier :';

            if (bienList[i].communeName !== empty) {
                quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + bienList[i].quartierName + '</span>';
            }

            var complementInfo = bienList[i].complement;

            descriptionBien = natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<hr/>' + complementInfo;


        } else {

            if (bienList[i].type == 1) {

                var genreInfo = 'Genre : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
                var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + bienList[i].complement;
            } else if (bienList[i].type == 3) {

                var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
                var categorieInfo2 = 'Destination : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + bienList[i].complement;
            } else if (bienList[i].type == 4) {

                var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
                var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + bienList[i].complement;
            } else {

                descriptionBien = bienList[i].descriptionBien + '<hr/>' + bienList[i].complement;
            }
        }

        body += '<tr>';
        body += '<td style="vertical-align:middle;">' + bienList[i].intituleBien + '</td>';
        body += '<td style="vertical-align:middle;">' + descriptionBien + '</td>';
        body += '<td style="vertical-align:middle;">' + bienList[i].chaineAdresse.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;">' + bienList[i].responsable.toUpperCase() + '</td>';
        body += '</tr>';

        var bien = new Object();

        bien.idBien = bienList[i].idBien;
        bien.intituleBien = bienList[i].intituleBien;
        bien.chaineAdresse = bienList[i].chaineAdresse;
        bien.codeAP = bienList[i].codeAP;
        bien.dateAcquisition = bienList[i].dateAcquisition;
        bien.codeTypeBien = bienList[i].codeTypeBien;
        bien.communeCode = bienList[i].communeCode;
        bien.natureCode = bienList[i].codeTypeBien;
        tempBienList.push(bien);
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableBiens.html(tableContent);
    var dtBien = tableBiens.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste ici _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false, ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 20
    });

    $('#tableBiens tbody').on('click', 'tr', function () {
        var data = dtBien.row(this).data();
        selectedBien = data[5];
        codeNatureBienSelected = data[8];

        switch (data[6]) {
            case 1:
            case 2:
                isTypeMobilier = false;
                break;

            default:
                isTypeMobilier = true;
                communeCodeBienSelected = data[7];
                break;
        }
        clickExist = true;
    });
}

function printArticles(articleList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" hidden="true"> Id </th>';
    header += '<th scope="col" hidden="true"> Code </th>';
    header += '<th scope="col" style="width:300px"> SECTEUR </th>';
    header += '<th scope="col" style="width:30px"> BIEN </th>';
    header += '<th scope="col" style="width:400px"> IMPÔT </th>';
    header += '<th scope="col" style="width:100px"> PERIODICITE </th>';
    header += '<th scope="col" style="width:100px"> ETAT </th>';
    header += '<th scope="col" style="width:150px"> </th>';
    header += '</tr></thead>';
    tempArticleList = [];
    var body = '<tbody id="tbodyArticles">';
    var firstLineAB = '';

    for (var i = 0; i < articleList.length; i++) {

        var article = {};

        firstLineAB = articleList[i].intituleAB;

        if (articleList[i].intituleTarif.toUpperCase() != 'TOUT' && articleList[i].intituleTarif.toUpperCase() != 'UNIQUE') {
            firstLineAB += ' : ' + articleList[i].intituleTarif;
        }

        if (firstLineAB.length > 340) {
            firstLineAB = firstLineAB.substring(0, 340) + ' ...';
        }
        article.idAssujettissement = articleList[i].idAssujettissement;
        article.codeAB = articleList[i].codeAB;
        article.intituleAB = articleList[i].intituleAB.toUpperCase();
        article.valeurBase = articleList[i].valeurBase;
        article.unite = articleList[i].unite;
        article.duree = articleList[i].duree;
        article.dateDebut = articleList[i].dateDebut;
        article.dateFin = articleList[i].dateFin;
        article.dateFin2 = articleList[i].dateFin2;
        article.reconduction = articleList[i].reconduction;
        article.nombreJour = articleList[i].nombreJour;
        article.nombreJourLimite = articleList[i].nombreJourLimite;
        article.nombreJourLimitePaiement = articleList[i].nbreJourLegalePaiement;
        article.echeanceLegale = articleList[i].echeanceLegale;
        article.periodeEcheance = articleList[i].periodeEcheance;
        article.codePeriodiciteAB = articleList[i].codePeriodiciteAB;
        article.listPeriodesDeclarations = articleList[i].listPeriodesDeclarations;
        article.montantDu = formatNumber(articleList[i].montant, articleList[i].devise);
        article.intituleTarif = articleList[i].intituleTarif;
        article.libellePeriodiciteAB = articleList[i].libellePeriodiciteAB;
        article.waittingClosing = articleList[i].waittingClosing;

        tempArticleList.push(article);
        var etat = 'VALIDE';
        var etatColor = 'black';

        if (articleList[i].etat == 0) {
            etat = 'ANNULE';
            etatColor = 'red';
        }

        body += '<tr>';
        body += '<td hidden="true">' + articleList[i].idAssujettissement + '</td>';
        body += '<td hidden="true">' + articleList[i].codeAB + '</td>';
        body += '<td style="width:20%;vertical-align:middle">' + articleList[i].secteurActivite.toUpperCase() + '</td>';
        body += '<td style="text-align:left;width:25%;vertical-align:middle"><span style="font-weight:bold">' + articleList[i].intituleBien.toUpperCase() + '</span><br/><br/>' + articleList[i].adresseBien.toUpperCase() + '</td>';

        if (articleList[i].isManyAB) {
            body += '<td style="width:30%;vertical-align:middle" title="Plusieurs"><a href="#3" style="font-weight:bold;color:blue;text-decoration:underline">Voir la liste</a></td>';
        } else {
            body += '<td style="width:30%;vertical-align:middle" title="' + articleList[i].intituleAB + '"><span style="font-weight:bold;">' + firstLineAB.toUpperCase() + '</td>';
        }

        var valueX = '';

        switch (articleList[i].unite) {
            case 'USD':
            case 'CDF':
                valueX = formatNumber(articleList[i].valeurBase, articleList[i].unite);
                break;
            default:
                valueX = articleList[i].valeurBase + ' ' + articleList[i].uniteValeurBase;
                break;
        }

        var redressementAssuj = '';

        var valeurBaseInfo = '<hr/>VALEUR DE BASE : <span style="font-weight:bold;color:green">' + valueX + '</span>' + redressementAssuj;

        body += '<td style="width:10%;vertical-align:middle"> ' + articleList[i].libellePeriodiciteAB + valeurBaseInfo + '</td>';
        body += '<td style="width:8%;vertical-align:middle;color:' + etatColor + '"> ' + etat + '</td>';

        var taxButton = '<a  onclick="gotoTaxation(\'' + articleList[i].idAssujettissement + '\',' + articleList[i].isManyAB + ')" class="btn btn-success" title="Passer à la taxation"><i class="fa fa-arrow-right"></i></a>&nbsp;';
        var generateButton = '<a onclick="generatePeriode(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-success" title="Générer période"><i class="fa fa-plus-circle"></i></a>&nbsp;';
        var deleteButton = '<a onclick="removeAssujettissement(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-danger" title="Annuler cet assujettissement"><i class="fa fa-close"></i></a>&nbsp';

        if (!controlAccess('SET_TAXATION_FISCALE')) {
            taxButton = '';
        }
        if (!controlAccess('3011')) {
            generateButton = '';
        }
        if (!controlAccess('3012')) {
            deleteButton = '';
        }

        if (articleList[i].etat == 1) {
            body += '<td style="text-align:center;width:17%;vertical-align:middle">'
                    + '' + deleteButton + ''
                    + '<a onclick="openPeriodes(\'' + articleList[i].idAssujettissement + '\',\'' + articleList[i].idBien + '\')" class="btn btn-primary" title="Cliquez pour exonérer ce bien"><i class="fa fa-list"></i>&nbsp;&nbsp;Exonérer</a>';
            +'' + generateButton + ''
                    + '' + taxButton + '</td>';
        } else {
            body += '<td style="text-align:center;width:10%;vertical-align:middle">'
                    + '<a onclick="openPeriodes(\'' + articleList[i].idAssujettissement + '\',\'' + articleList[i].idBien + '\')" class="btn btn-primary" title="Cliquez pour exonérer ce bien"><i class="fa fa-list"></i>&nbsp;&nbsp;Exonérer</a></td>';
        }

        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableArticles.html(tableContent);
    var dtArticles = tableArticles.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste ici  _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false, ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os', blurable: true
        },
        datalength: 3
    });
    $('#tableArticles tbody').on('click', 'tr', function () {
        var data = dtArticles.row(this).data();
    });
}

function printOtherBien(bienList) {

    var dataBien = '<option value ="0">-- Sélectionner --</option>';
    for (var i = 0; i < bienList.length; i++) {
        dataBien += '<option value =' + bienList[i].idBien + '>' + bienList[i].intituleBien + '</option>';
    }

    return dataBien;
}

function generatePeriode(idAssujettissement) {

    alertify.confirm('Etes-vous sûre de vouloir générer une période supplémentaire ?', function () {

        var dateFin = '', nombreJour = '', nombreJourLimite = '', nombreJourLimitePaiement = '', echeanceLegale = '', codePeriodiciteAB = '', periodeEcheance = '';

        for (var i = 0; i < tempArticleList.length; i++) {

            if (tempArticleList[i].idAssujettissement == idAssujettissement) {

                dateFin = tempArticleList[i].dateFin2;
                nombreJour = tempArticleList[i].nombreJour;
                nombreJourLimite = tempArticleList[i].nombreJourLimite;
                echeanceLegale = tempArticleList[i].echeanceLegale;
                periodeEcheance = tempArticleList[i].periodeEcheance;
                codePeriodiciteAB = tempArticleList[i].codePeriodiciteAB;
                nombreJourLimitePaiement = tempArticleList[i].nombreJourLimitePaiement;
                break;
            }
        }

        createPeriodeDeclarationIfNotExist(1, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance);
    });
}

function gotoTaxation(idAssujettissement, isMany) {

    alertify.confirm('Etes-vous sûre de vouloir passer à la déclaration proprement dite ?', function () {

        var dateFin = '', nombreJour = '', nombreJourLimite = '', nombreJourLimitePaiement = '', echeanceLegale = '', codePeriodiciteAB = '', periodeEcheance = '';

        for (var i = 0; i < tempArticleList.length; i++) {

            if (tempArticleList[i].idAssujettissement == idAssujettissement) {

                dateFin = tempArticleList[i].dateFin2;
                nombreJour = tempArticleList[i].nombreJour;
                nombreJourLimite = tempArticleList[i].nombreJourLimite;
                echeanceLegale = tempArticleList[i].echeanceLegale;
                periodeEcheance = tempArticleList[i].periodeEcheance;
                codePeriodiciteAB = tempArticleList[i].codePeriodiciteAB;
                nombreJourLimitePaiement = tempArticleList[i].nombreJourLimitePaiement;
                break;
            }
        }

        createPeriodeDeclarationIfNotExist(0, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance, isMany);
    });
}

var tempPeriodeDeclaration;

function openPeriodes(idAssujettissement, idBien) {

    assujettissementId = idAssujettissement;
    bienId = idBien;

    getPeriodeDeclarationBien(idAssujettissement, idBien);
}

function removeAssujettissement(idAssujettissement) {

    alertify.confirm('Etes-vous sûre de vouloir retirer cet acte générateur du bien ?', function () {
        desactivateAssujettissement(idAssujettissement);
    });
}

function createPeriodeDeclarationIfNotExist(isGenerate, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance, isMany) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true, data: {
            'operation': 'createPeriodeDeclaration',
            'isGenerate': isGenerate,
            'idAssujettissement': idAssujettissement,
            'dateFin': dateFin,
            'nombreJour': nombreJour,
            'nombreJourLimite': nombreJourLimite,
            'nbreJourLegalePaiement': nombreJourLimitePaiement,
            'echeanceLegale': echeanceLegale, 'codePeriodiciteAB': codePeriodiciteAB,
            'periodeEcheance': periodeEcheance
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Veuillez patientier...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1' && isGenerate == 0) {

                    if (isMany) {
                        setAssujettiData(JSON.stringify(selectAssujettiData));
                    }

                    window.location = isMany ? 'taxation-repetitive_multiple?assuj=' + btoa(idAssujettissement) : 'taxation-repetitive?id=' + btoa(idAssujettissement);

                } else if (response == '1' && isGenerate == 1) {

                    alertify.alert('Des périodes supplémnetaires sont générées avec succès');
                    loadBiens(codeResponsible, '0');

                } else if (response == '0') {

                    alertify.alert('Echec lors du passage à la déclaration');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function desactivateAssujettissement(idAssujettissement) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'desactivateAssujettissement',
            'idAssujettissement': idAssujettissement
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Annulation de l\'assujettissement en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('l\'assujettissement est annulé avec succès.');

                    setTimeout(function () {

                        loadBiens(codeResponsible, '0');
                    }
                    , 1000);

                } else if (response == '0') {
                    alertify.alert('Echec lors de l\'annulation de l\'assujettissement');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function resetSearchAssujetti() {

    inputValueResearchAssujetti.val('');
    printAssujettiTable('');
}

function disabledBien(idAcquisition, proprietaire) {

    alertify.confirm('Etes-vous sûre de vouloir rétirer ce bien ?', function () {

        $.ajax({
            type: 'POST', url: 'assujetissement_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'},
            crossDomain: true,
            data: {
                'operation': 'desactivateBienAcquisitionV2', 'idAcquisition': idAcquisition
            },
            beforeSend: function () {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Rétrait du bien en cours ...</h5>'});
            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1') {
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    if (response == '1') {

                        alertify.alert('Le rétrait du bien s\'est effectué avec succès.');
                        loadBiens(codeResponsible, '0');

                    } else if (response == '0') {
                        alertify.alert('Echec lors du rétrait du bien');
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });
}

function loadModalMiseEnQuarantaine(periodeId, periodeName) {

    periodeDeclarationId = periodeId;
    periodeDeclarationName = periodeName;
    textaeraObservation.val(empty);
    valuePeriode1.html(periodeName);
    valueAgent1.html(userData.nomComplet);
    modalMiseEnQuarantaine.modal('show');
}

function miseEnQuarantaine() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'miseQuarantaine',
            'periodeId': periodeDeclarationId,
            'observation': textaeraObservation.val(),
            'userId': userData.idUser
        },
        beforeSend: function () {

            modalMiseEnQuarantaine.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>La mise en quarantaine en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                modalMiseEnQuarantaine.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalMiseEnQuarantaine.unblock();
                modalMiseEnQuarantaine.modal('hide');

                alertify.alert('La mise en quarantaine s\'est effectuée avec succès.');

                loadBiens(codeAssujettiSave, '1');

                modalPeriodeDeclaration.modal('hide');

            }
            , 1);
        },
        error: function (xhr, status, error) {
            modalMiseEnQuarantaine.unblock();
            showResponseError();
        }
    });
}
var resultNewPeriodeList;

var checkExist;

function setExoneration(periodeId, montantExonere) {

    var objet = new Object();
    var idAssujettissement = '', idBien = '';

    alertify.confirm('Etes-vous sûr d\'exonérer ce bien pour cette période ?', function () {
        
        stamp_ === undefined ? stamp_ = null : stamp_;

        $.each(articlesList, function (index, item, array) {
            if (assujettissementId === item.idAssujettissement) {

                objet.montant = montantExonere;
                objet.idAssujettissement = item.idAssujettissement;
                objet.devise = item.devise;
                objet.fkAB = item.codeAB;
                objet.fkBien = item.idBien;
                objet.fkSite = userData.SiteCode;
                objet.fkPersonne = fkPersonne;
                objet.idUser = userData.idUser;
                objet.fkPeriode = periodeId;
                objet.observation = "Exonération";
                objet.base64 = stamp_;

                idAssujettissement = item.idAssujettissement;
                idBien = item.idBien;
            }
        });

        saveExonerationData(objet, idAssujettissement, idBien);
    });

}
function saveExonerationData(objet, idAssujettissement, idBien) {

    $.ajax({
        type: 'POST',
        url: 'exonerations_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveExoneration',
            'objet': JSON.stringify(objet)
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Traitement en cours ...</h5>'});
        },
        success: function (response) {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('L\'exonération n\'a pas abouti. Veuillez réessayer ou contacter l\'administrateur.');
                return;
            }

            alertify.alert('Le bien sélectionné a été exonéré pour cette période avec succcès.');

            getTaux(idAssujettissement, idBien);

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getPeriodeDeclarationBien(idAssujettissement, idBien) {

    getTaux(idAssujettissement, idBien);
}

function getTaux(idAssujettissement, idBien) {

    $.ajax({
        type: 'POST',
        url: 'exonerations_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getTauxAssujetti',
            'fkPersonne': fkPersonne,
            'fkBien': idBien,
            'fkSite': userData.SiteCode,
            'fkAgent': userData.idUser
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Traitement en cours ...</h5>'});
        },
        success: function (data) {

            $.unblockUI();

            if (data == '-1') {
                showResponseError();
                return;
            }
            $.each(data, function (index, item) {
                listPeriode = item.listPeriode;
                listTaux = item.listTaux;
            });

            for (var i = 0; i < tempArticleList.length; i++) {

                if (tempArticleList[i].idAssujettissement == idAssujettissement) {

                    var header = empty, body = empty;

                    tempPeriodeDeclaration = tempArticleList[i].listPeriodesDeclarations;

                    header += '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>';
                    header += '<th style="text-align:left">EXERCICE</th>';
                    header += '<th style="text-aLign:left">ECHEANCE DECLARATION</th>';
                    header += '<th style="text-aLign:left">MONTANT EXONERE</th>';
                    header += '<th style="text-aLign:left">DATE EXONERATION</th>';
                    header += '<th style="text-aLign:left">DOCUMENT</th>';
                    header += '<th></th>';
                    header += '</tr></thead>';

                    for (var j = 0; j < tempPeriodeDeclaration.length; j++) {

                        var periodeDeclaration = tempPeriodeDeclaration[j];
                        var colorExoneree = '', dateExoneration = '', tauxExoneration = 0, montantExonere = '',
                                btnExonerer = '', idExoneration;

                        if (data != '0' && listTaux.length != 0) {
                            $.each(listTaux, function (index, item) {
                                if (idBien === item.fkBien) {
                                    montantExonere = item.tauxCumule + ' ' + item.devise;
                                    tauxExoneration = item.tauxCumule;
                                }
                            });
                        }

                        btnExonerer = '&nbsp;<button type="button" class="btn btn-success" title="Cliquez ici pour exonérer" onclick="setExoneration(\'' + periodeDeclaration.periodeID + '\',\'' + tauxExoneration + '\')"><i class="fa fa-check"></i></button>';

                        if (listPeriode != '0' && listPeriode.length != 0) {
                            $.each(listPeriode, function (index, item) {
                                if (periodeDeclaration.periodeID === item.idPeriode && idBien === item.idBien) {
                                    btnExonerer = '&nbsp;<span>D\351jà exonérée</span>';
                                    colorExoneree = 'style = "background-color: #ddd"';
                                    dateExoneration = item.dateExoneration;
                                    montantExonere = item.montantExonere + ' ' + item.devise;
                                    idExoneration = item.id;
                                }
                            });
                        }

                        var colorEcheance = 'color: black';

                        switch (periodeDeclaration.estPenalise) {
                            case "1":
                                colorEcheance = 'color: red';
                                break;
                            case "0":
                                colorEcheance = 'color: black';
                                break;
                        }
                        body += '<tr ' + colorExoneree + '>';
                        body += '<td style="text-align:left;vertical-align:middle">' + periodeDeclaration.periode + '</td>';
                        body += '<td style="text-align:left;vertical-align:middle' + colorEcheance + '">' + periodeDeclaration.dateLimite + '</td>';
                        body += '<td style="text-align:left;vertical-align:middle">' + montantExonere + '</td>';
                        body += '<td style="text-align:left;vertical-align:middle">' + dateExoneration + '</td>';
                        if (dateExoneration == '') {
                            body += '<td><input type ="file" onclick="uploadPreuvePaiement(\'' + idExoneration + '\')" onchange="uploadStamp(this)" name="document" accept="image/png, image/jpeg"></td>';
                        } else {
                            body += '<td></td>';
                        }
                        body += '<td style="text-align:centervertical-align:middle">' + btnExonerer + '</td>';
                    }
                    body += '</tr></tbody>';

                    var tableContent = header + body;
                    tablePeriodeDeclaration.html(tableContent);
                    tablePeriodeDeclaration.DataTable({
                        language: {
                            processing: "Traitement en cours...",
                            track: "Rechercher&nbsp;:",
                            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                            infoPostFix: "",
                            loadingRecords: "Chargement en cours...",
                            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
                            search: "Rechercher par N° document _INPUT_  ",
                            paginate: {
                                first: "Premier",
                                previous: "Pr&eacute;c&eacute;dent",
                                next: "Suivant",
                                last: "Dernier"
                            },
                            aria: {
                                sortAscending: ": activer pour trier la colonne par ordre croissant",
                                sortDescending: ": activer pour trier la colonne par ordre décroissant"
                            }
                        },
                        info: false,
                        destroy: true,
                        searching: false,
                        paging: true,
                        order: [[0, 'asc']],
                        lengthChange: false,
                        tracking: false,
                        ordering: false,
                        pageLength: 5,
                        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
                        select: {
                            style: 'os',
                            blurable: true
                        }, datalength: 3
                    });
                }
            }

            modalPeriodeDeclaration.modal('show');

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function uploadStamp(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var stamp = e.target.result;
            stamp_ = stamp;
        };
        reader.readAsDataURL(input.files[0]);
    }

}