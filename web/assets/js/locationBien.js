/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalLocationBien;

var
        codeLocataireBien,
        codeProprietaireBien;

var
        lblNameProprietaire,
        lblAddressProprietaire;

var cmbLocationBien;

var
        inputReference,
        inputActeNotarie,
        inputDateActeNotarie,
        inputDateAcquisition;

var btnSaveLocateBien;

$(function () {

    modalLocationBien = $('#modalLocationBien');
    lblNameProprietaire = $('#lblNameProprietaire');
    lblAddressProprietaire = $('#lblAddressProprietaire');

    cmbLocationBien = $('#cmbLocationBien');
    inputReference = $('#inputReference');
    inputActeNotarie = $('#inputActeNotarie');
    inputDateActeNotarie = $('#inputDateActeNotarie');
    inputDateAcquisition = $('#inputDateAcquisition');

    btnSaveLocateBien = $('#btnSaveLocateBien');
    btnSaveLocateBien.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbLocationBien.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner le bien à louer');
            return;

        }

        if (inputDateAcquisition.val() === '') {
            alertify.alert('Veuillez d\'abord indiquer la date du début de contrat de ce bien.');
            return;
        }

        var value = '<span style="font-weight:bold">' + $('#cmbLocationBien option:selected').text().toUpperCase() + '</span>';

        alertify.confirm('Etes-vous sûre de vouloir louer ce bien dénommé : ' + value + ' ?', function () {
            saveLocateBien();
        });

    });

});

function saveLocateBien() {

    if (cmbLocationBien.val() === '0') {
        alertify.alert('Veuillez d\'abord sélectionner le bien à louer.');
        return;
    }

    if (inputDateAcquisition.val() === '') {
        alertify.alert('Veuillez d\'abord indiquer la date du début de contrat de ce bien.');
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveBienLocation',
            'codePersonne': codeLocataireBien,
            'idBien': cmbLocationBien.val(),
            'dateAcquisition': inputDateAcquisition.val(),
            'referenceContrat': inputReference.val(),
            'numActeNotarie': inputActeNotarie.val(),
            'dateActeNotarie': inputDateActeNotarie.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de l\'assujetti ...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '1') {

                alertify.alert('Le bien a été loué avec succès.');

                setTimeout(function () {
                    window.location = 'gestion-bien?id=' + btoa(codeLocataireBien);
                }
                , 1500);

            } else if (response == '0') {
                alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement du bien.');
            } else {
                showResponseError();
            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });

}
