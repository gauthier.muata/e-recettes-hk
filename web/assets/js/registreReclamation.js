/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var typeResearch, datePickerDebut, datePickerFin, dateReclamation;
var isAdvance, isAdvancedSearch, typeSearch, typeTraitement;
var btnSearch, btnCallModalSearchAvanded, btnShowAccuserReception, btnValiderReclamation, btnValiderTraitement;
var btnAdvancedSearch, btnVisualiserLettreReclamation, btnPrintBP;
var lblNameAssujetti, lblTypeAssujetti, lblAdresseAssujetti, lblDateValidation;
var inputResearchReclamation, inputdateLast, inputDateDebut, inputDegreveAmount;
var lblTypeReclamation, lblReferenceDocument, lblDemandeSurcis, lblSurcisAccorde, divRadio;
var lblDateDebut, lblDateFin, lblNbArchuiveDecisionAdminstrative;
var lblMereMontantDu, lblBonAPayer3, lblNameAssujetti3, lblAdress3;

var inputNumeroEnregGreffe, inputDateDepotCourrier, inputDateAudience, textMotifRecours;
var btnCreerRecours;
var lblNameAssujettiRecours, lblTypeAssujettiRecours, lblAdresseAssujettiRecours;

var fkDocument, codeAssujetti, assujettiCode;

var tableReclamation, tableTitreAMR, tableBPChild;
var selectTypeReclamation, selectDecision, checkContentieux;
var typeReclamation = '*';
var modalReclamationAdvancedSearch, modalTraitementDecision, modalDemandeRecoursJ;
var stateTraitement = ''

var divMontantDu, divRestPayer, formRadioButton;

var modalDetailReclaration, modalBonAPayerChild;

var tempReclamationList = [];
var tempDetailReclamationList = [];
var documentList = [];

var idReclamation = '';

var reclamationDocumentAttach = '';

$(function () {

    mainNavigationLabel.text('CONTENTIEUX');
    secondNavigationLabel.text('Registre des réclamations');

    removeActiveMenu();
    linkMenuContentieux.addClass('active');

    typeResearch = $('#typeResearch');

    isAdvance = $('#isAdvance');
    isAdvancedSearch = $('#isAdvancedSearch');

    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    lblNameAssujetti = $('#lblNameAssujetti');
    lblTypeAssujetti = $('#lblTypeAssujetti');
    lblAdresseAssujetti = $('#lblAdresseAssujetti');
    lblDateValidation = $('#lblDateValidation');
    lblTypeReclamation = $('#lblTypeReclamation');
    lblReferenceDocument = $('#lblReferenceDocument');
    lblDemandeSurcis = $('#lblDemandeSurcis');
    lblSurcisAccorde = $('#lblSurcisAccorde');
    lblNbArchuiveDecisionAdminstrative = $('#lblNbArchuiveDecisionAdminstrative');

    inputNumeroEnregGreffe = $('#inputNumeroEnregGreffe');
    inputDateDepotCourrier = $('#inputDateDepotCourrier');
    inputDateAudience = $('#inputDateAudience');
    textMotifRecours = $('#textMotifRecours');
    lblNameAssujettiRecours = $('#lblNameAssujettiRecours');
    lblTypeAssujettiRecours = $('#lblTypeAssujettiRecours');
    lblAdresseAssujettiRecours = $('#lblAdresseAssujettiRecours');

    lblMereMontantDu = $('#lblMereMontantDu');
    lblBonAPayer3 = $('#lblBonAPayer3');
    lblNameAssujetti3 = $('#lblNameAssujetti3');
    lblAdress3 = $('#lblAdress3');

    btnCallModalSearchAvanded = $('#btnCallModalSearchAvanded');
    btnShowAccuserReception = $('#btnShowAccuserReception');
    btnValiderReclamation = $('#btnValiderReclamation');
    btnValiderTraitement = $('#btnValiderTraitement');
    btnAdvancedSearch = $('#btnAdvancedSearch');
    btnSearch = $('#btnSearch');
    btnPrintBP = $('#btnPrintBP');

    btnCreerRecours = $('#btnCreerRecours');

    btnVisualiserLettreReclamation = $('#btnVisualiserLettreReclamation');

    selectTypeReclamation = $('#selectTypeReclamation');
    selectDecision = $('#selectDecision');

    inputResearchReclamation = $('#inputResearchReclamation');
    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');
    inputDegreveAmount = $('#inputDegreveAmount');


    datePickerDebut = $("#datePicker1");
    datePickerFin = $("#datePicker2");

    tableReclamation = $('#tableReclamation');
    tableTitreAMR = $('#tableTitreAMR');
    tableBPChild = $('#tableBPChild');

    modalDetailReclaration = $('#modalDetailReclaration');
    modalReclamationAdvancedSearch = $('#modalReclamationAdvancedSearch');
    modalTraitementDecision = $('#modalTraitementDecision');
    modalDemandeRecoursJ = $('#modalDemandeRecoursJ');

    divRadio = $('#divRadio');
    divMontantDu = $('#divMontantDu');
    divRestPayer = $('#divRestPayer');
    formRadioButton = $('#formRadioButton');

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerDebut.datepicker("setDate", new Date());
    datePickerFin.datepicker("setDate", new Date());


    $('.date').datepicker({
        multidate: true
    });

    btnSearch.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var critere = typeResearch.val();

        if (critere == '1') {
            assujettiModal.modal('show');
        } else {
            isAdvancedSearch = '0';
            isAdvance.attr('style', 'display: none');
            loadReclamation();
        }
    });

    btnAdvancedSearch.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAdvancedSearch = '1';
        codeAssujetti = '';
        loadReclamation();
        modalReclamationAdvancedSearch.modal('hide');


    });

    typeResearch.on('change', function () {

        inputResearchReclamation.val('');

        tempReclamationList.length = 0;

        printReclamation(tempReclamationList);

        typeSearchDocument = typeResearch.val();
        if (typeSearchDocument == '1') {
            inputResearchReclamation.attr('placeholder', 'Assujetti');
            inputResearchReclamation.attr('disabled', 'true');
            assujettiModal.modal('show');
        } else if (typeSearchDocument == '2') {
            inputResearchReclamation.attr('placeholder', 'Référence du courrier');
            inputResearchReclamation.removeAttr('disabled');
        } else if (typeSearchDocument == '3') {
            inputResearchReclamation.attr('placeholder', 'Titre d\'AMR');
            inputResearchReclamation.removeAttr('disabled');
        }
    });

    selectTypeReclamation.on('change', function () {

        typeReclamation = selectTypeReclamation.val();

    });

    btnVisualiserLettreReclamation.click(function (e) {

        e.preventDefault();
        initPrintData(reclamationDocumentAttach);

    });

    btnValiderReclamation.click(function (e) {
        e.preventDefault();

        if (stateTraitement == '') {
            alertify.alert('Veuilez d\'abord choisir la décision du traitement.');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir valider cette réclamation ?', function () {
            validerReclamation();
        });


    });

    $('input[name=radioTraiter]').on('change', function (e) {
        stateTraitement = $('input[name=radioTraiter]:checked').val();
    });

    btnShowAccuserReception.click(function (e) {
        e.preventDefault();
        initAccuseReceptionUI(idReclamation, 'AMR');
    });

    btnCallModalSearchAvanded.on('click', function (e) {
        e.preventDefault();
        modalReclamationAdvancedSearch.modal('show');
    });

    selectDecision.on('change', function () {

        inputDegreveAmount.val('');

        if (selectDecision.val() == '1') {

            inputDegreveAmount.removeAttr('disabled');
            formRadioButton.attr('style', 'display:none');
            divMontantDu.attr('style', 'display:inline');
            divRestPayer.attr('style', 'display:inline');
        }
        else if (selectDecision.val() == '2') {

            inputDegreveAmount.attr('disabled', true);
            formRadioButton.attr('style', 'display:none');
            divMontantDu.attr('style', 'display:none');
            divRestPayer.attr('style', 'display:none');
        }
        else if (selectDecision.val() == '3') {

            inputDegreveAmount.attr('disabled', true);
            formRadioButton.attr('style', 'display:none');
            divMontantDu.attr('style', 'display:none');
            divRestPayer.attr('style', 'display:none');
        }
        else if (selectDecision.val() == '4') {

            inputDegreveAmount.attr('disabled', true);
            formRadioButton.attr('style', 'display:none');
            divMontantDu.attr('style', 'display:none');
            divRestPayer.attr('style', 'display:none');

        } else {
            formRadioButton.attr('style', 'display:inline');
            inputDegreveAmount.attr('disabled', true);
            divMontantDu.attr('style', 'display:none');
            divRestPayer.attr('style', 'display:none');

            var checkControlRadio = $("#radioDegreveTotal");

            checkControlRadio.change(function (e) {
                e.preventDefault();

                inputDegreveAmount.attr('disabled', true);
                inputDegreveAmount.val('');
                divMontantDu.attr('style', 'display:none');
                divRestPayer.attr('style', 'display:none');
            });

            var checkControlRadioPartiel = $("#radioDegrevePartiel");
            checkControlRadioPartiel.change(function (e) {
                e.preventDefault();

                inputDegreveAmount.removeAttr('disabled');
                inputDegreveAmount.val('');
                divMontantDu.attr('style', 'display:inline');
                divRestPayer.attr('style', 'display:inline');
            });
        }
    });

    btnPrintBP.click(function (e) {

        e.preventDefault();

        alertify.confirm('Etes-vous sûre de vouloir imprimer ce bon à payer ?', function () {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            printBonAPayer(fkDocument, 'BP');
        });

    });

    btnCreerRecours.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputNumeroEnregGreffe.val().trim() == '') {
            alertify.alert('Veuillez d\'abord saisir le numéro d\'enregistrement du greffier.');
            return;
        }

        if (inputDateDepotCourrier.val().trim() == '') {
            alertify.alert('Veuillez d\'abord choisir la date du dépôt du courrier');
            return;
        }

        if (inputDateAudience.val().trim() == '') {
            alertify.alert('Veuillez d\'abord choisir la ou les dates de l\'audience.');
            return;
        }
        if (textMotifRecours.val().trim() == '') {
            alertify.alert('Veuillez d\'abord saisir le motif du recours.');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir créer le recours juridictionnel ?', function () {

            saveRecoursJuridictionnel();

        });

    });

//      btnDisplayObservation.click(function (e) {
//        
//        e.preventDefault();
//
//        if (checkSession()) {
//            showSessionExpiredMessage();
//            return;
//        }
//
//        observationListData = tempReclamationList[0].observList;
//        numberFolderSelected = tempReclamationList[0].reclamationId;
//        displayObservation(observationListData);
//
//    });

//     btnJoindreDecisionAdmin.click(function (e) {
//
//        e.preventDefault();
//
//        if (archivesDecisionAdmin == '') {
//            initUpload(codeDoc, codeTypeDoc);
//        } else {
//            initUpload(archivesDecisionAdmin, codeTypeDoc);
//        }
//    });

    printReclamation('');

});


function loadReclamation() {

    var dateDebut;
    var dateLast;

    var valueSearch = inputResearchReclamation.val().trim();

    if (typeResearch.val() === '1') {
        valueSearch = codeAssujetti;
    }

    if (isAdvancedSearch == '1') {
        dateDebut = inputDateDebut.val();
        dateLast = inputdateLast.val();
    } else {
        dateDebut = '';
        dateLast = '';
    }


    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadReclamation',
            'valueSearch': valueSearch,
            'typeSearch': typeResearch.val(),
            'dateDebut': dateDebut,
            'dateFin': dateLast,
            'isAdvancedSearch': isAdvancedSearch,
            'typeReclamation': typeReclamation,
            'idUser': userData.idUser,
//            'codeService': JSON.stringify(JSON.parse(userData.allServiceList)),
//            'codeSite': JSON.stringify(JSON.parse(userData.siteUserList))


        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                printReclamation('');
                return;
            }

            setTimeout(function () {
                var allReclamationList = JSON.parse(JSON.stringify(response));
                printReclamation(allReclamationList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });

}

function printReclamation(reclamationList) {

    tempReclamationList = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col">TYPE RECLAMATION</th>';
    header += '<th scope="col">NUMERO COURRIER</th>';
    header += '<th scope="col">ASSUJETTI</th>';
    header += '<th scope="col">TYPE ASSUJETTI</th>';
    header += '<th scope="col">DATE CREATION</th>';
    header += '<th scope="col">DATE VALIDATION</th>';
    header += '<th scope="col">ETAT</th>';
    header += '<th></th>';
    header += '</tr></thead>';

    var body = '<tbody >';

    for (var i = 0; i < reclamationList.length; i++) {

        var type, etat;
        var action;
        var actionRecours;

        type = 'recouvrement';

        action = '<a onclick="openDetailReclamation(\'' + reclamationList[i].reclamationId + '\',\'' + reclamationList[i].etat + '\',\'' + reclamationList[i].documentPayer + '\')" class="btn btn-warning"><i class="fa fa-list"></i></a>';
        actionRecours = '<a style="margin-rigth:3px" onclick="demanderRecours(\'' + reclamationList[i].reclamationId + '\',\'' + reclamationList[i].etat + '\',\'' + reclamationList[i].delaisRecoursDepasse + '\')" class="btn btn-primary"><i class="fa fa-anchor"></i></a>';

        switch (reclamationList[i].etat) {
            case 3:
                etat = 'REJETE';
                break;
            case 1:
                etat = 'EN ATTENTE DE TRAITEMENT';
                if (reclamationList[i].reclamationTraiter == '3') {
                    etat = 'TRAITE';
                } else if (reclamationList[i].reclamationTraiter == '2') {
                    etat = 'RECOURS ENCOURS';
                } else if (reclamationList[i].reclamationTraiter == '1') {
                    etat = 'RECOURS TRAITE';
                }
                break;
            case 2:
                etat = 'EN ATTENTE DE VALIDATION';

                break;
            case 0:
                etat = 'EN ATTENTE DE PAIEMENT';
                break;
            case 4:
                if (reclamationList[i].reclamationTraiter == '3') {
                    etat = 'TRAITE';
                } else if (reclamationList[i].reclamationTraiter == '2') {
                    etat = 'RECOURS ENCOURS';
                } else if (reclamationList[i].reclamationTraiter == '1') {
                    etat = 'RECOURS TRAITE';
                }
        }

        var reclamation = {};
        reclamation.reclamationId = reclamationList[i].reclamationId;
        reclamation.typeReclamation = reclamationList[i].typeReclamation;
        reclamation.assujettiCode = reclamationList[i].assujettiCode;
        reclamation.assujettiName = reclamationList[i].assujettiName;
        reclamation.legalFormName = reclamationList[i].legalFormName;
        reclamation.adresseName = reclamationList[i].adresseName;
        reclamation.dateCreate = reclamationList[i].dateCreate;
        reclamation.dateReceptionCourrier = reclamationList[i].dateReceptionCourrier;
        reclamation.etat = reclamationList[i].etat;
        reclamation.observation = reclamationList[i].observation;
        reclamation.detailsReclamation = reclamationList[i].detailsReclamation;
        reclamation.recourExist = reclamationList[i].recourExist;
        reclamation.numeroEnregistrementGreffe = reclamationList[i].numeroEnregistrementGreffe;
        reclamation.dateAudience = reclamationList[i].dateAudience;
        reclamation.dateDepotRecours = reclamationList[i].dateDepotRecours;
        reclamation.motifRecours = reclamationList[i].motifRecours;
        reclamation.observList = reclamationList[i].observationList;
        reclamation.etatReclamation = reclamationList[i].etatReclamation;
        reclamation.documentPayer = reclamationList[i].documentPayer;
        reclamation.reclamationTraiter = reclamationList[i].reclamationTraiter;
        reclamation.CodeDecisionJuridictionnel = reclamationList[i].CodeDecisionJuridictionnel;
        assujettiCode = reclamationList[i].assujettiCode;

        tempReclamationList.push(reclamation);

        body += '<tr>';
        body += '<td style="text-align:left;width:15%;vertical-align:middle;">' + type.toUpperCase() + '</td>';
        body += '<td style="text-align:left;width:10%;vertical-align:middle;">' + reclamationList[i].referenceCourrierReclamation + '</td>';
        body += '<td style="text-align:left;width:23%;vertical-align:middle" title="' + reclamationList[i].assujettiName + '"><span style="font-weight:bold;">' + reclamationList[i].assujettiName + '</span><br/><br/>' + reclamationList[i].adresseName + '</td>';
        body += '<td style="text-align:left;width:12%;vertical-align:middle;">' + reclamationList[i].legalFormName + '</td>';
        body += '<td style="text-align:left;width:10%;vertical-align:middle;">' + reclamationList[i].dateCreate + '</td>';
        body += '<td style="text-align:center;width:12%;vertical-align:middle;">' + reclamationList[i].dateReceptionCourrier + '</td>';
        body += '<td style="text-align:left;width:10%;vertical-align:middle;">' + etat + '</td>';

        if (reclamationList[i].reclamationTraiter == '3') {
            if (reclamationList[i].CodeDecisionJuridictionnel == '1') {
                body += '<td style="text-align:center;width:9%;vertical-align:middle;"><center>'
                        + '' + action + ''
                        + '' + actionRecours +
                        '</center></td>';
                body += '</tr>';
            } else {
                body += '<td style="text-align:center;width:9%;vertical-align:middle;"><center>'
                        + action + '</center></td>';
                body += '</tr>';
            }

        } else {
            body += '<td style="text-align:center;width:9%;vertical-align:middle;"><center>'
                    + action + '</center></td>';
            body += '</tr>';
        }
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableReclamation.html(tableContent);
    tableReclamation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}

function getSelectedAssujetiData() {
    isAdvancedSearch = '0';
    typeSearch = '1';

    isAdvance.attr('style', 'display: none');

    inputResearchReclamation.val(selectAssujettiData.nomComplet);
    codeAssujetti = selectAssujettiData.code;

    assujettiModal.modal('hide');

    loadReclamation();

}

function resetData() {
    codeAssujetti = '';

    inputResearchReclamation.val('');

    inputDateDebut.val('');
    inputdateLast.val('');

    printReclamation('');
}

function openDetailReclamation(reclamationId, etatReclamation, documentPayer) {


    for (var j = 0; j < tempReclamationList.length; j++) {

        if (tempReclamationList[j].reclamationId == reclamationId) {

            idReclamation = reclamationId;
            dateReclamation = tempReclamationList[j].dateCreate;

            lblNameAssujetti.html(tempReclamationList[j].assujettiName);
            lblTypeAssujetti.html(tempReclamationList[j].legalFormName);
            lblAdresseAssujetti.html(tempReclamationList[j].adresseName);

            divRadio.attr('style', 'display:none');
            btnValiderReclamation.attr('style', 'display:none');
            btnPrintBP.attr('style', 'display:none');

            if (tempReclamationList[j].etat == '2') {
                // En attente de validation
                divRadio.attr('style', 'display:block');
                btnValiderReclamation.attr('style', 'display:iniline');
                btnPrintBP.attr('style', 'display:none');
            } else if (tempReclamationList[j].etat == '1') {
                // En attente de traitement

                btnValiderReclamation.attr('style', 'display:none');

                if (tempReclamationList[j].reclamationTraiter == '3') {
                    if (tempReclamationList[j].CodeDecisionJuridictionnel == '1') {
                        btnPrintBP.attr('style', 'display:inline');
                    } else {
                        btnPrintBP.attr('style', 'display:none');
                    }
                } else {
                    btnPrintBP.attr('style', 'display:none');
                }

//                if (tempReclamationList[j].reclamationTraiter == '2') {
//                    btnPrintBP.attr('style', 'display:none');
//                } else {
//                    btnPrintBP.attr('style', 'display:inline');
//                }

            } else if (tempReclamationList[j].etat == '0') {
                //En attente de paiement
                btnValiderReclamation.attr('style', 'display:none');
                btnPrintBP.attr('style', 'display:inline');
            } else if (tempReclamationList[j].etat == '4') {
                //Traitement juridictionnel  
                if (tempReclamationList[j].CodeDecisionJuridictionnel == '1') {
                    btnValiderReclamation.attr('style', 'display:none');
                    btnPrintBP.attr('style', 'display:inline');
                } else {
                    btnValiderReclamation.attr('style', 'display:none');
                    btnPrintBP.attr('style', 'display:none');
                }

            } else {
                //Rjeter
                divRadio.attr('style', 'display:none');
                btnValiderReclamation.attr('style', 'display:none');
                btnPrintBP.attr('style', 'display:none');
            }


            modalDetailReclaration.modal('show');
            printDetailsReclamation(tempReclamationList[j].detailsReclamation, etatReclamation, documentPayer);

            break;
        }
    }

}

function printDetailsReclamation(declarationList, etatReclamation, documentPayer) {

    tempDetailReclamationList = [];
    var detailsReclamation = JSON.parse(declarationList);

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"><center>EXERCICE</center></th>';
    header += '<th scope="col">SERVICE D\'ASSIETTE</th>';
    header += '<th scope="col">ACTE GENERATEUR</th>';
    header += '<th scope="col"><center>TYPE DOCUMENT</center></th>';
    header += '<th scope="col">DECISION ADMINISTRATIVE</th>';
    header += '<th scope="col">DECISION JURIDICTIONNELLE</th>';
    header += '<th scope="col"><center>DATE TRAITEMENT</center></th>';
    header += '<th scope="col" style="text-align:right">MONTANT DÛ</th>';
    header += '<th scope="col" style="text-align:right">MONTANT CONTESTE</th>';
    header += '<th scope="col" style="text-align:right">20% DE LA PARTIE NON CONTESTEE</th>';
//    if (etatReclamation == '2') {
//        header += '<th scope="col">ACCORDER SURCIS</th>';
//    } else {
//        header += '<th scope="col" hidden="true">ACCORDER SURCIS</th>';
//    }

    if (detailsReclamation.length > 0) {
        lblDateValidation.html(detailsReclamation[0].dateDecisionSurcis);
    } else {
        lblDateValidation.html('');
    }

    header += '<th></th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyReclamation">';

    for (var i = 0; i < detailsReclamation.length; i++) {

//        if (detailsReclamation[i].pourcentageMontantNonContester == '0') {
//            btnPrintBP.attr('style', 'display:none');
//        } else {
//            btnPrintBP.attr('style', 'display:inline');
//        }

        var detail = {};
        detail.detailReclamationId = detailsReclamation[i].detailReclamationId;
        detail.hasSurcis = '0';
        detail.demandeSurcis = detailsReclamation[i].avecSurcis;
        detail.motif = detailsReclamation[i].motif;
        detail.referenceDocument = detailsReclamation[i].referenceDocument;
        detail.avecSurcis = detailsReclamation[i].avecSurcis;
        detail.decisionSurcis = detailsReclamation[i].decisionSurcis;
        detail.typeDocument = detailsReclamation[i].typeDocument;
        detail.reclamationPayer = detailsReclamation[i].reclamationPayer;
        detail.amount = detailsReclamation[i].amount;
        detail.exerciceFiscal = detailsReclamation[i].exerciceFiscal;
        detail.montantContester = detailsReclamation[i].montantContester;
        detail.pourcentageMontantNonContester = detailsReclamation[i].pourcentageMontantNonContester;
        detail.documentImprimer = detailsReclamation[i].documentImprimer;
        detail.devise = detailsReclamation[i].devise;
        detail.libelleArticleBudgetaire = detailsReclamation[i].libelleArticleBudgetaire;
        detail.decisionAdministrative = detailsReclamation[i].decisionAdministrative;
        detail.CodeDecisionJuridictionnel = detailsReclamation[i].CodeDecisionJuridictionnel;
        reclamationDocumentAttach = detailsReclamation[i].declarationDocumentList;
        fkDocument = detailsReclamation[i].referenceDocument;
        tempDetailReclamationList.push(detail);

        body += '<tr>';
        body += '<td style="width:14%;vertical-align:middle"><center>' + detailsReclamation[i].exerciceFiscal + '</center></td>';
        body += '<td style="width:20%;vertical-align:middle">' + detailsReclamation[i].libelleService + '</td>';
        body += '<td style="vertical-align:middle">' + detailsReclamation[i].libelleArticleBudgetaire + '</td>';
        body += '<td style="width:12%;vertical-align:middle"><center><span >' + detailsReclamation[i].referenceDocument + '</span><br/><br/><span style="font-size:13px;font-weight:bold;">' + '(' + detailsReclamation[i].typeDocument + ')' + '</span></center></td>';
        body += '<td style="width:8%;vertical-align:middle">' + detailsReclamation[i].decisionAdministrative + '</td>';
        body += '<td style="width:8%;vertical-align:middle">' + detailsReclamation[i].decisionJuridique + '</td>';
        body += '<td style="width:10%;vertical-align:middle"><center>' + detailsReclamation[i].dateTraitement + '</center></td>';
        body += '<td style="width:11%;text-align:right;vertical-align:middle">' + formatNumber(detailsReclamation[i].amount, detailsReclamation[i].devise) + '</td>';
        body += '<td style="width:11%;text-align:right;vertical-align:middle">' + formatNumber(detailsReclamation[i].montantContester, detailsReclamation[i].devise) + '</td>';
        body += '<td style="width:11%;text-align:right;vertical-align:middle">' + formatNumber(detailsReclamation[i].pourcentageMontantNonContester, detailsReclamation[i].devise) + '</td>';
        if (etatReclamation == '2') {
//            if (detailsReclamation[i].avecSurcis == '1') {
////                body += '<td style="width:5%;vertical-align:middle"><center><input type="checkbox" id="checkBoxSurcis_' + detailsReclamation[i].detailReclamationId + '" name="checkBoxSurcis_' + detailsReclamation[i].detailReclamationId + '"></center></td>';
//            } else {
//                body += '<td style="width:5%;vertical-align:middle"></td>';
//            }


            body += '<td style="vertical-align:middle"></td>';
        } else {
//            var demandeSurcis = detailsReclamation[i].avecSurcis == '1' ? 'Oui' : 'Non';
//            body += '<td style="width:5%;vertical-align:middle" hidden="true">' + demandeSurcis + '</td>';

            if (documentPayer == '1') {
                body += '<td style="vertical-align:middle"><a onclick="showModalTraitementDecision(\'' + detailsReclamation[i].detailReclamationId + '\')" class="btn btn-success" title="traitement"><i class="fa fa-check-circle"></i></a></td>';
            } else {
                body += '<td style="vertical-align:middle"></td>';
            }

        }
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableTitreAMR.html(tableContent);
    tableTitreAMR.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier", previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        order: [[2, 'asc']],
        pageLength: 5,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 5,
        columnDefs: [
            {"visible": false, "targets": 2}
        ],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="11">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

}

function refrechDataAfterValidate() {

    idReclamation = '';
    stateTraitement = '';

    if (isAdvancedSearch == '0') {
        btnSearch.trigger('click');
    } else {
        btnAdvencedSearch.trigger('click');
    }

    modalDetailReclaration.modal('hide');
}

function validerReclamation() {

    documentList = JSON.stringify(tempDetailReclamationList);

    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'validerReclamation',
            'userId': userData.idUser,
            'valueState': stateTraitement,
            'reclamationId': idReclamation,
            'codeAssujetti': assujettiCode,
            'fkDocument': fkDocument,
            'documentList': documentList

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            if (response == '1') {

                alertify.alert('La réclamation est validée avec succès.');

                idReclamation = '';
                stateTraitement = '';

                if (isAdvancedSearch == '0') {
                    //btnSearch.trigger('click');
                    isAdvancedSearch = '0';
                    isAdvance.attr('style', 'display: none');

                } else {
                    //btnAdvancedSearch.trigger('click');
                    isAdvancedSearch = '1';
                    codeAssujetti = '';
//                    btnSearchAssujettis.trigger('click');
                }

                loadReclamation();

                modalDetailReclaration.modal('hide');

            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });

}

function showModalTraitementDecision(detailReclamatioId) {

    var documentRef, degrevValue, typeDocument,
            detailReclamationId, typeRemiseGracieuse,
            documentArchive, amountDegre, idDecision;
    var amount, devise, fivePercent;
    var avecSurcis, dateDecisionSurcis;
    var montantConteste;
    var libelleAB;

    var lblMontantDu = $('#lblMontantDu');
    var lblRestePayer = $('#lblRestePayer');

    var lblPartieMontantConteste = $('#lblPartieMontantConteste');
    var lblCinqPourcentPartieConteste = $('#lblCinqPourcentPartieConteste');

    var dataDecision = '<option value="1">Dégrèvement partiel</option>'
            + '<option value="2">Dégrèvement total</option>'
    /*+ '<option value="3">Rejet réclamation</option>'
     /*+ '<option value="4">Admission en non-valeur</option>'
     + '<option value="5">Remise gracieuse</option>';*/

    selectDecision.html(dataDecision);

    for (var i = 0; i < tempDetailReclamationList.length; i++) {

        if (tempDetailReclamationList[i].detailReclamationId == detailReclamatioId) {

            amount = tempDetailReclamationList[i].amount;
            fivePercent = tempDetailReclamationList[i].pourcentageMontantNonContester;
            montantConteste = tempDetailReclamationList[i].montantContester;
            codeTypeDocumentSelected = tempDetailReclamationList[i].referenceDocument;
            libelleAB = tempDetailReclamationList[i].libelleArticleBudgetaire;
            typeDocument = tempDetailReclamationList[i].typeDocument;
            detailReclamationId = tempDetailReclamationList[i].detailReclamationId;
            devise = tempDetailReclamationList[i].devise;
            documentArchive = '';
            lblMontantDu.html(formatNumber(amount, devise));
            typeTraitement = 1;

            lblPartieMontantConteste.html(formatNumber(montantConteste, devise));
            lblCinqPourcentPartieConteste.html(formatNumber(fivePercent, devise));

            lblRestePayer.html(formatNumber((parseFloat(amount) - parseFloat(fivePercent)), devise));
//            avecSurcis = tempDetailReclamationList[i].decisionSurcis;
//            dateDecisionSurcis = tempDetailReclamationList[i].dateDecisionSurcis;
            break;
        }
    }

    modalTraitementDecision.modal('show');

    if (selectDecision.val() == '5') {
        formRadioButton.attr('style', 'display:inline');
    } else {
        formRadioButton.attr('style', 'display:none');
    }

    inputDegreveAmount.change(function () {

        if (degrevValue >= 0) {
            if (degrevValue > montantConteste) {
                alertify.alert('Le montant dégrevé est superieur à ' + formatNumber(montantConteste, devise) + '. Veuillez saisir un montant valide.');
                inputDegreveAmount.val('');
                return;
            } else {
                amountDegre = parseFloat(amount) - parseFloat(degrevValue) - parseFloat(fivePercent);
                lblRestePayer.html(formatNumber(amountDegre, devise));
            }
        } else {
            amountDegre = parseFloat(amount) - parseFloat(fivePercent);
            lblRestePayer.html(formatNumber(amountDegre, devise));
        }
    });



    btnValiderTraitement.click(function (e) {
        e.preventDefault();

        var selectedDecision = selectDecision.val();
        var selectState = $('input[name=radioValider]:checked').val();

        if (inputDegreveAmount.val() == '') {
            amountDegre = 0;
        } else {
            degrevValue = parseFloat(inputDegreveAmount.val());
            amountDegre = amount - degrevValue;
        }

        if (parseInt(amountDegre) != 0 && parseInt(inputDegreveAmount.val()) > amount) {
            alertify.alert('Le montant dégrevé est superieur au montant dû. Veuillez saisir un montant valide.');
            return;
        }

        switch (selectedDecision) {
            case "1" :
                if (inputDegreveAmount.val() == '') {
                    alertify.alert('Aucun montant dégrevé est fourni. Veuillez saisir un montant.');
                }
                break;
//            case "5" :
//                if (selectState == 0 && inputDegreveAmount.val() == '') {
//                    alertify.alert('Aucun montant dégrevé est fourni. Veuillez saisir un montant.');
//                }
//                break;
        }

        switch (selectedDecision) {

//            case "5" :
//                if (selectState == 1) {
//                    typeRemiseGracieuse = 2;
//                } else {
//                    typeRemiseGracieuse = 1;
//                }
//                if (typeRemiseGracieuse == 1 && amountDegre == 0) {
//                    typeRemiseGracieuse = 2;
//                }
//                break;
            case "1" :
                if (amountDegre == 0) {
                    selectedDecision = '2';
                }
                break;
        }


        alertify.confirm('Etes-vous sûr de vouloir valider ce traitement ?', function () {

            traitementDecision(
                    selectedDecision,
                    typeDocument,
                    detailReclamationId,
//                    typeRemiseGracieuse,
                    documentArchive,
                    amountDegre,
                    fivePercent,
                    amount,
                    libelleAB
//                    avecSurcis,
//                    dateDecisionSurcis
                    );
        });

    });
}

function traitementDecision(
        idDecision,
        typeDocument,
        detailReclamationId,
//        typeRemiseGracieuse,
        documentArchive,
        amountDegre,
        fivePercent,
        amount,
        libelleAB
//        avecSurcis,
//        dateDecisionSurcis
        ) {

    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'traitementDecision',
            'decisionId': idDecision,
            'userId': userData.idUser,
            'typeDocument': typeDocument,
            'dateCreateReclamation': dateReclamation,
            'detailReclamationId': detailReclamationId,
            'archives': documentArchive,
            'amountDegre': amountDegre,
            'amount': amount,
            'pourcentageMontantNonContester': fivePercent,
            'typeTraitement': typeTraitement,
            'fkDocument': codeTypeDocumentSelected,
            'codeAssujetti': assujettiCode,
            'reclamationId': idReclamation,
            'libelleArticleBudgetaire': libelleAB
//            'observationDocument': textObservDoc.val()

        },
        beforeSend: function () {
            modalTraitementDecision.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Traitement en cours...</h5>'});
        },
        success: function (response)
        {
            modalTraitementDecision.unblock();

            if (response == '0') {
                showResponseError();
                return;
            }

            if (response == '1') {
                alertify.alert('Le traitement de la décision s\'est effectué avec success!');

                setTimeout(function () {
                    modalTraitementDecision.modal('hide');
                    modalDetailReclaration.modal('hide');

                    idReclamation = '';
                    stateTraitement = '';

                    if (isAdvancedSearch == '0') {
                        btnSearch.trigger('click');
                    } else {
                        btnAdvancedSearch.trigger('click');
                    }
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalTraitementDecision.unblockUI();
            showResponseError();
        }
    });
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesDecisionAdmin = getUploadedData();

    nombre = JSON.parse(archivesDecisionAdmin).length;

    switch (nombre) {
        case 0:
            lblNbArchuiveDecisionAdminstrative.text('');
            break;
        case 1:
            lblNbArchuiveDecisionAdminstrative.html('1 document');
            break;
        default:
            lblNbArchuiveDecisionAdminstrative.html(nombre + ' documents');
    }
}

function printBonAPayer(numeroDocument, typeDoc) {


    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'documentReference': numeroDocument,
            'typeDocument': typeDoc,
            'operation': 'printDocument'
        },
        beforeSend: function () {
            modalDetailReclaration.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            modalDetailReclaration.unblock();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            setTimeout(function () {
                modalDetailReclaration.unblock();
                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
                setTimeout(function () {
                }, 2000);
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            modalDetailReclaration.unblock();
            showResponseError();
        }

    });

}

//function loadArchiveLR() {
//
//    $.ajax({
//        type: 'POST',
//        url: 'contentieux_servlet',
//        dataType: 'JSON',
//        headers: {
//            'Access-Control-Allow-Origin': '*'
//        },
//        crossDomain: false,
//        data: {
//            'operation': 'loadArchiveLR',
//            'reclamationId': idReclamation
//        },
//        beforeSend: function () {
//        },
//        success: function (response)
//        {
//            if (response == '-1') {
//                showResponseError();
//                return;
//            }
//
//            setTimeout(function () {
//
//                if (response == '0') {
//                    alertify.alert('Aucun Archive retrouvé!');
//                } else {
//
//                    dataArchive = JSON.parse(JSON.stringify(response));
//                    initPrintData(JSON.stringify(dataArchive));
//                }
//            }
//            , 1);
//        },
//        error: function (jqXHR, textStatus, errorThrown) {
//            showResponseError();
//        }
//    });
//}

function demanderRecours(reclamationId, etat, delaisRecoursDepasse) {

    idReclamation = reclamationId;

    if (delaisRecoursDepasse == '1') {
        alertify.alert('Le délais légale pour introduire le recours est dépassé.');
        return;
    }

    inputNumeroEnregGreffe.val('');
    inputDateDepotCourrier.val('');
    inputDateAudience.val('');
    textMotifRecours.val('');

    for (var i = 0; i < tempReclamationList.length; i++) {

        if (tempReclamationList[i].reclamationId == reclamationId) {

            idReclamation = reclamationId;
            dateReclamation = tempReclamationList[i].dateCreate;

            lblNameAssujettiRecours.html(tempReclamationList[i].assujettiName);
            lblTypeAssujettiRecours.html(tempReclamationList[i].legalFormName);
            lblAdresseAssujettiRecours.html(tempReclamationList[i].adresseName);

        }
        break;
    }

    modalDemandeRecoursJ.modal('show');

}

function displayObservation(observList) {


    observList = JSON.parse(observList);
    var table = '';

    if (observList[0] !== '0') {

        lblNumberFolder.text('NUMERO RECLAMATION :');
        lblValueNumberFolder.html(numberFolderSelected);

        table += '<table class="table table-bordered table-hover">';

        table += '<thead style="background-color:#0085c7;color:white">';
        table += '<tr>';
        table += '<td> TYPE </td>';
        table += '<td> CONTENU OBSERVATION </td>';
        table += '</tr>';
        table += '</thead>';

        table += '<tbody>';

        if (observList[0].obsCreateExists == '1') {

            table += '<tr>';

            table += '<td style="text-align:left;width:20%;vertical-align:middle;font-weight:bold"> Observation fournie à la création </td>';
            table += '<td style="text-align:left;width:80%;vertical-align:middle"> ' + observList[0].obsCreateValues + ' </td>';

            table += '</tr>';
        }

        if (observList[0].obsValidateExistsFirstLevel == '1') {

            table += '<tr>';

            table += '<td style="text-align:left;width:20%;vertical-align:middle;font-weight:bold"> Observation fournie à la validation </td>';
            table += '<td style="text-align:left;width:80%;vertical-align:middle"> ' + observList[0].obsValidateFirstLevel + ' </td>';

            table += '</tr>';
        }

        /*if (observList[0].obsValidateExistsSecondLevel == '1') {
         
         table += '<tr>';
         
         table += '<td> Observation fournie à la création </td>';
         table += '<td> ' + observList[0].obsValidateSecondLevel + ' </td>';
         
         table += '</tr>';
         }*/

        table += '</tbody>';
        table += '</table>';

        divInfoObservationContent.html(table);
        modalObservationDisplay.modal('show');
    } else {

        lblNumberFolder.text(empty);
        lblValueNumberFolder.html(empty);
        divInfoObservationContent.html(empty);

        alertify.alert('Ce dossier n\'a pas d\'observation.');

    }
}

function saveRecoursJuridictionnel() {

    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveRecouursJuridictionnel',
            'userId': userData.idUser,
            'motif': textMotifRecours.val(),
            'reclamationId': idReclamation,
            'numeroEnregistrementGreffe': inputNumeroEnregGreffe.val(),
            'dateDepotRecours': inputDateDepotCourrier.val(),
            'dateAudience': inputDateAudience.val()
        },
        beforeSend: function () {
            modalDemandeRecoursJ.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Création du recours en cours...</h5>'});
        },
        success: function (response)
        {
            modalDemandeRecoursJ.unblock();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            if (response == '1') {

                alertify.alert('Le recours juridictionnel est créé avec succès.');

                idReclamation = '';

                modalDemandeRecoursJ.modal('hide');

                if (isAdvancedSearch == '1') {
                    btnAdvancedSearch.trigger('click');
                } else {
                    btnSearch.trigger('click');
                }

            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalDemandeRecoursJ.unblock();
            showResponseError();
        }
    });

}


