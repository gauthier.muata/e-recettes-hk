/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var idRowSelected;
var codeContribuableSelected;
var codeAdrPricipalContribuableSelected;
var nameContribuableSelected;
var fJContribuableSelected;
var codeBienSelected;
var selectedCodeAP;
var codeAdressePersonneBienSelected;
var idAssujettissement;

var contribuableNameEdit, btnSearchContribuable, selectGenreEdit, selectCategorieEdit, denominationEdit, MarqueEdit;
var TypeEdit, NumeroPlaqueEdit, NumeroChassisEdit, PuissanceFiscalEdit, UnitePuissanceFiscalEdit, AnneeCirculationEdit;
var AdresseEdit, btnAjouterAdresseEdit, btnSaveBien, tableBienAutomobile;

var modalMiseAJourBienAutomobile, selectGenreSelected, selectCategorieSelected, denominationSelected, MarqueSelected;
var TypeSelected, NumeroPlaqueSelected, NumeroChassisSelected, PuissanceFiscalSelected, UnitePuissanceFiscalSelected;
var AnneeCirculationSelected, AdresseSelected, btnAjouterAdresseSelected, btnDeleteBienSelected, btnSaveBienSelected;
var modalAdressesPersonne;
var ExerciceEdit;

var bienAutomobileList = [];
var periodeDeclarationList = [];
var bienAutomobileSelectedList = [];
var bienAutomobileSelectedForTaxationList = [];
var categorieBienListGlobal = [];
var btnDeleteBien, btnDeleteAllBienNotTaxed;
var checkExist;
var objBienSelected = {};
var spnNbreBien;
var DateMiseCirculation;

var newGenreModal,
        inputNewGenre,
        btnNewGenre,
        newMarqueModal,
        inputNewMarque,
        btnNewMarque,
        newModeleModal,
        inputNewModele,
        btnNewModele;

var btnAfficherAllBien;

var plageCategorieList = [];
var plageCategorieListVU = [];
var valueDenomination;
var adressePersonneListX = [];
var adressIsCorrect;
var btnNettoyer;

var changeADR;

var btnTaxationAllBien, idDivCheck, checkBoxSelectAllBien;
var tempPeriodeDeclarationList = [];
var modalAddPeriodeDeclarationAssujettissement, spnPDDepart, spnBienAssujSelected, tableNewPDAssuj;
var periodeDeclarationModal, selectBanque, selectCompteBancaire, selectPeridoeDecl, btnValidateTaxation, selectDevise;

var modalDisplayResumeTotalTaxationBatch,
        spnNbreBienSelected,
        spnAmountGlobal,
        btnValidateTaxation2;

var typeVehicule;
var btnDisplayCotation;

var tableCotation, modalDisplayCotation, spnTotalGen;
var spnNameContribuable;

$(function () {

    idRowSelected = empty;
    codeBienSelected = empty;
    codeAdressePersonneBienSelected = empty;
    changeADR = false;

    adressIsCorrect = false;

    mainNavigationLabel.text('REPERTOIRE');
    secondNavigationLabel.text('Gestion des biens automobile');
    removeActiveMenu();

    contribuableNameEdit = $('#contribuableNameEdit');
    btnSearchContribuable = $('#btnSearchContribuable');
    selectGenreEdit = $('#selectGenreEdit');
    selectCategorieEdit = $('#selectCategorieEdit');
    denominationEdit = $('#denominationEdit');
    MarqueEdit = $('#MarqueEdit');
    TypeEdit = $('#TypeEdit');
    NumeroPlaqueEdit = $('#NumeroPlaqueEdit');
    NumeroChassisEdit = $('#NumeroChassisEdit');
    PuissanceFiscalEdit = $('#PuissanceFiscalEdit');
    UnitePuissanceFiscalEdit = $('#UnitePuissanceFiscalEdit');
    AnneeCirculationEdit = $('#AnneeCirculationEdit');
    AdresseEdit = $('#AdresseEdit');
    btnAjouterAdresseEdit = $('#btnAjouterAdresseEdit');
    btnSaveBien = $('#btnSaveBien');
    tableBienAutomobile = $('#tableBienAutomobile');
    ExerciceEdit = $('#ExerciceEdit');

    modalMiseAJourBienAutomobile = $('#modalMiseAJourBienAutomobile');
    selectGenreSelected = $('#selectGenreSelected');
    selectCategorieSelected = $('#selectCategorieSelected');
    denominationSelected = $('#denominationSelected');
    MarqueSelected = $('#MarqueSelected');
    TypeSelected = $('#TypeSelected');
    NumeroPlaqueSelected = $('#NumeroPlaqueSelected');
    NumeroChassisSelected = $('#NumeroChassisSelected');
    PuissanceFiscalSelected = $('#PuissanceFiscalSelected');
    UnitePuissanceFiscalSelected = $('#UnitePuissanceFiscalSelected');
    AnneeCirculationSelected = $('#AnneeCirculationSelected');
    AdresseSelected = $('#AdresseSelected');
    btnAjouterAdresseSelected = $('#btnAjouterAdresseSelected');
    btnDeleteBienSelected = $('#btnDeleteBienSelected');
    btnSaveBienSelected = $('#btnSaveBienSelected');
    modalAdressesPersonne = $('#modalAdressesPersonne');
    btnDeleteBien = $('#btnDeleteBien');
    btnDeleteAllBienNotTaxed = $('#btnDeleteAllBienNotTaxed');
    spnNbreBien = $('#spnNbreBien');
    DateMiseCirculation = $('#DateMiseCirculation');

    newGenreModal = $('#newGenreModal');
    inputNewGenre = $('#inputNewGenre');
    btnNewGenre = $('#btnNewGenre');

    newMarqueModal = $('#newMarqueModal');
    inputNewMarque = $('#inputNewMarque');
    btnNewMarque = $('#btnNewMarque');

    newModeleModal = $('#newModeleModal');
    inputNewModele = $('#inputNewModele');
    btnNewModele = $('#btnNewModele');
    btnNettoyer = $('#btnNettoyer');
    btnAfficherAllBien = $('#btnAfficherAllBien');

    btnTaxationAllBien = $('#btnTaxationAllBien');
    idDivCheck = $('#idDivCheck');
    checkBoxSelectAllBien = $('#checkBoxSelectAllBien');

    idDivCheck.attr('style', 'display:none');
    btnTaxationAllBien.attr('style', 'display:none');

    periodeDeclarationModal = $('#periodeDeclarationModal');
    selectBanque = $('#selectBanque');
    selectCompteBancaire = $('#selectCompteBancaire');
    selectPeridoeDecl = $('#selectPeridoeDecl');
    btnValidateTaxation = $('#btnValidateTaxation');
    selectDevise = $('#selectDevise');

    modalAddPeriodeDeclarationAssujettissement = $("#modalAddPeriodeDeclarationAssujettissement");
    spnPDDepart = $("#spnPDDepart");
    spnBienAssujSelected = $("#spnBienAssujSelected");
    tableNewPDAssuj = $("#tableNewPDAssuj");
    btnAjouterPD = $("#btnAjouterPD");

    btnAjouterPD.attr('style', 'display:none');

    modalDisplayResumeTotalTaxationBatch = $("#modalDisplayResumeTotalTaxationBatch");
    spnNbreBienSelected = $("#spnNbreBienSelected");
    spnAmountGlobal = $("#spnAmountGlobal");
    btnValidateTaxation2 = $("#btnValidateTaxation2");
    typeVehicule = $("#typeVehicule");
    btnDisplayCotation = $("#btnDisplayCotation");

    tableCotation = $("#tableCotation");
    modalDisplayCotation = $("#modalDisplayCotation");
    spnTotalGen = $("#spnTotalGen");
    spnNameContribuable = $("#spnNameContribuable");

    btnDisplayCotation.click(function (e) {

        e.preventDefault();

        alertify.confirm('Etes-vous sûre de vouloir afficher la cotation de ce(s) bien(s) automobile(s) ?', function () {
            getCotationBienAutomobileByContribuable();
        });

    });

    btnAjouterPD.click(function (e) {

        e.preventDefault();

        alertify.confirm('Etes-vous sûre de vouloir ajouter cette période de déclaration pour ce bien automobile ?', function () {
            saveAddNewPriodeDeclaration();
        });

    });

    btnValidateTaxation2.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalDisplayResumeTotalTaxationBatch.modal('hide');
        periodeDeclarationModal.modal('show');

    });

    btnValidateTaxation.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectBanque.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;

        }

        if (selectCompteBancaire.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un compete bancaire valide');
            return;

        }

        var value = '<span style="font-weight:bold">' + nameContribuableSelected.toUpperCase() + '</span>';

        alertify.confirm('Etes-vous sûre de vouloir enregistrer la taxation du contribuable : ' + value + ' ?', function () {

            taxationBatchVignette();
        });


    });

    checkBoxSelectAllBien.on('click', function (e) {

        var checkControl = document.getElementById("checkBoxSelectAllBien");

        var checked = false;

        if (checkControl.checked) {
            checked = true;
        }

        if (checked) {

            alertify.confirm('Etes-vous sûre de vouloir sélectionner tous les biens du contribuable ?', function () {
                printTableBienAutomobileV2(bienAutomobileList, checked);
                btnTaxationAllBien.attr('style', 'display:block');
            });

        } else {

            alertify.confirm('Etes-vous sûre de vouloir désélectionner tous les biens du contribuable ?', function () {
                bienAutomobileSelectedForTaxationList = [];
                printTableBienAutomobileV2(bienAutomobileList, checked);
                btnTaxationAllBien.attr('style', 'display:none');
            });
        }

    });

    selectBanque.change(function (e) {

        if (selectDevise.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;

        }

        if (selectBanque.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;
        } else {
            loadingAccountBankData(AB_VIGNETTE, selectBanque.val(), selectDevise.val());
        }

    });

    btnTaxationAllBien.on('click', function (e) {

        e.preventDefault();

        if (bienAutomobileSelectedForTaxationList.length == 0) {

            alertify.alert('Veuillez d\'abord sélectionner un bien automobile avant de procéder à la taxation');
            return;

        } else {

            loadingBankData();

            var amountTotal = 0;
            var devsie = '';
            var nbreBien = 0;

            for (var i = 0; i < bienAutomobileSelectedForTaxationList.length; i++) {

                amountTotal += bienAutomobileSelectedForTaxationList[i].montant;
                nbreBien++;
                devsie = bienAutomobileSelectedForTaxationList[i].devise;
            }

            totalBienSelected = nbreBien

            spnNbreBienSelected.html(totalBienSelected);
            spnAmountGlobal.html(formatNumber(amountTotal, devsie));

            modalDisplayResumeTotalTaxationBatch.modal('show');
        }



    });


    btnAfficherAllBien.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir afficher tous les biens automobiles de ce contribuable ?', function () {

            loadBienAutomobileByContribuable();
        });
    });

    btnNettoyer.on('click', function (e) {

        e.preventDefault();

        alertify.confirm('Etes-vous sûre de vouloir nettoyer les champs de saisie ?', function () {

            resetFields();
        });
    });

    btnDeleteBien.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir supprimer ce bien automobile : ' + valueDenomination + ' ?', function () {

            deleteBienAutomobile();
        });
    });

    btnDeleteAllBienNotTaxed.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir désactiver les biens automobile non-taxés de contribuable ?', function () {

            deleteAllBienAutomobileNotTaxed();
        });
    });

    btnNewGenre.on('click', function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputNewGenre.val() == '') {

            alertify.alert('Veuillez d\'abord fournir le nouveau genre de véhicule');
            return;

        } else {

            alertify.confirm('Etes-vous sûre de vouloir ajouter ce nouveau genre de véhicule ?', function () {
                addNewGenreVehicule();
            });

        }

    });

    btnNewMarque.on('click', function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputNewMarque.val() == '') {

            alertify.alert('Veuillez d\'abord fournir la nouvelle marque de véhicule');
            return;

        } else {

            alertify.confirm('Etes-vous sûre de vouloir ajouter cette nouvelle marque de véhicule ?', function () {
                addNewMarqueVehicule();
            });

        }

    });

    btnNewModele.on('click', function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputNewModele.val() == '') {

            alertify.alert('Veuillez d\'abord fournir le nouveau modèle de véhicule');
            return;

        } else {

            alertify.confirm('Etes-vous sûre de vouloir ajouter ce nouveau modèle de véhicule ?', function () {
                addNewModeleVehicule();
            });

        }

    });

    btnSaveBien.on('click', function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();
    });

    btnSearchContribuable.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        assujettiModal.modal('show');
    });

    typeVehicule.on('change', function (e) {

        //if (typeVehicule.val() !== 'OUI') {

        UnitePuissanceFiscalEdit.val('0');
        PuissanceFiscalEdit.val('');
        selectCategorieEdit.val('0');
        //}
    });

    PuissanceFiscalEdit.on('change', function (e) {

        if (PuissanceFiscalEdit.val() !== '') {

            if (parseFloat(PuissanceFiscalEdit.val()) <= 0) {

                alertify.alert('La puissance fiscale n\'est doit pas être inférieure ou égale à zéro');
                return;

            } else {

                for (var i = 0; i < plageCategorieList.length; i++) {

                    if (plageCategorieList[i].codeUnite == UnitePuissanceFiscalEdit.val()) {

                        if (selectGenreEdit.val() == 'TB00000037' || selectGenreEdit.val() == 'TB00000039') {

                            if (plageCategorieList[i].plageMin <= PuissanceFiscalEdit.val()
                                    && plageCategorieList[i].plageMax >= PuissanceFiscalEdit.val()
                                    && plageCategorieList[i].codeGenre == selectGenreEdit.val()) {

                                selectCategorieEdit.val(plageCategorieList[i].codeTarif);

                                break;
                            }

                        } else {

                            if (typeVehicule.val() == 'OUI') {


                                for (var j = 0; j < plageCategorieListVU.length; j++) {

                                    if (plageCategorieListVU[j].plageMin <= PuissanceFiscalEdit.val()
                                            && plageCategorieListVU[j].plageMax >= PuissanceFiscalEdit.val()) {

                                        selectCategorieEdit.val(plageCategorieListVU[j].codeTarif);

                                        break;

                                    }
                                }

                            } else {

                                if (plageCategorieList[i].plageMin <= PuissanceFiscalEdit.val()
                                        && plageCategorieList[i].plageMax >= PuissanceFiscalEdit.val()) {

                                    selectCategorieEdit.val(plageCategorieList[i].codeTarif);
                                    selectCategorieEdit.attr('disabled', true);
                                    break;
                                }
                            }


                        }


                    }

                }
            }

        }
    });

    UnitePuissanceFiscalEdit.on('change', function (e) {

        if (UnitePuissanceFiscalEdit.val() == '0') {

            PuissanceFiscalEdit.val('');
            PuissanceFiscalEdit.attr('disabled', true);
            selectCategorieEdit.val('0');
            alertify.alert('Veuillez d\'abord sélectionner une unité de puissance fiscale valide');
            return;

        } else {

            PuissanceFiscalEdit.val('');
            selectCategorieEdit.val('0');
            PuissanceFiscalEdit.attr('disabled', false);
        }
    });

    selectGenreEdit.on('change', function (e) {

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectGenreEdit.val() == 'new') {

            inputNewGenre.val('');
            newGenreModal.modal('show');
            return;
        }

        if (codeContribuableSelected == '' || codeContribuableSelected == null) {

            selectGenreEdit.val('0');
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un contribuable');
            return;

        } else {

            if (selectGenreEdit.val() == '0') {

                alertify.alert('Veuillez d\'abord sélectionner un genre valide');
                return;

            } else {
                var codeTypeBien = selectGenreEdit.val();
                loadTypeComplementBiens(codeTypeBien);
            }
        }
    });

    btnAjouterAdresseEdit.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        codePersonne = codeContribuableSelected;
        nomPersonne = nameContribuableSelected;

        loadAdressesOfPersonne();

        modalAdressesPersonne.modal('show');

    });

    MarqueEdit.on('change', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (MarqueEdit.val() == 'new') {
            inputNewMarque.val('');
            newMarqueModal.modal('show');
        }

    });

    TypeEdit.on('change', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (TypeEdit.val() == 'new') {
            inputNewModele.val('');
            newModeleModal.modal('show');
        }

    });

    initInfosConnexeBienAutomobile();
    printTableBienAutomobile('');

});

function deleteBienAutomobile() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'codeBien': codeBienSelected,
            'codePersonne': codeContribuableSelected,
            'userId': userData.idUser,
            'operation': 'deleteBienAutomobile'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('La suppression du bien dénommé : ' + valueDenomination + ' s\'est effectuée avec succès');
                    resetFields();
                    btnDeleteBien.attr('style', 'display:none');
                    //btnDeleteAllBienNotTaxed.attr('style', 'display:none');

                    codeAdressePersonneBienSelected = '';
                    selectedCodeAP = '';
                    AdresseEdit.val('');
                    AdresseEdit.attr('style', 'font-weight:normal');

                    //loadBienAutomobileByContribuable();
                    loadFiveLastBienAutomobileByContribuable();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function deleteAllBienAutomobileNotTaxed() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'codePersonne': codeContribuableSelected,
            'userId': userData.idUser,
            'operation': 'deleteAllBienAutomobileNotTaxed'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('La désactivation de bien(s) automobile non-taxé s\'est effectuée avec succès');
                    resetFields();
                    btnDeleteBien.attr('style', 'display:none');
                    btnDeleteAllBienNotTaxed.attr('style', 'display:none');

                    codeAdressePersonneBienSelected = '';
                    selectedCodeAP = '';
                    AdresseEdit.val('');
                    AdresseEdit.attr('style', 'font-weight:normal');

                    //loadBienAutomobileByContribuable();
                    loadFiveLastBienAutomobileByContribuable();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function resetFields() {

    idRowSelected = '';
    codeBienSelected = '';

    //codeAdressePersonneBienSelected = '';
    //selectedCodeAP = '';

    selectGenreEdit.val('0');
    selectCategorieEdit.val('0');
    denominationEdit.val('');
    MarqueEdit.val('0');
    TypeEdit.val('0');

    NumeroPlaqueEdit.val('');
    NumeroChassisEdit.val('');
    PuissanceFiscalEdit.val('');
    UnitePuissanceFiscalEdit.val('0');
    AnneeCirculationEdit.val('');
    DateMiseCirculation.val('');
    denominationEdit.attr('style', 'font-weight:normal');
    typeVehicule.val('0');

}

function printTableBienAutomobile(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col"></th>';
    tableContent += '<th style="text-align:center;width:5%">№</th>';
    tableContent += '<th style="text-align:left;width:20%">GENRE</th>';
    tableContent += '<th style="text-align:left;width:20%">DENOMINATION</th>';
    tableContent += '<th style="text-align:left;width:20%">DESCRIPTION</th>';
    tableContent += '<th style="text-align:left;width:10%">№ PLAQUE</th>';
    tableContent += '<th style="text-align:left;width:10%">№ CHASSIS</th>';
    tableContent += '<th style="text-align:right;width:10%">TAUX VIGNETTE</th>';
    tableContent += '<th style="text-align:center;width:10%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var count = 0;
    var totalBien = result.length;

    var checkControl = document.getElementById("checkBoxSelectAllBien");

    var checkAllRows = false;

    if (checkControl.checked) {
        checkAllRows = true;
    }

    totalBienSelected = 0;

    if (result.length > 0) {
        btnDisplayCotation.attr('style', 'display:block');
    } else {
        btnDisplayCotation.attr('style', 'display:none');
    }

    for (var i = 0; i < result.length; i++) {

        count++;

        var btnEditBienAutomobile = '<button class="btn btn-success" onclick="editBienAutomobile(\'' + result[i].codeBien + '\',\'' + result[i].denomination + '\')"><i class="fa fa-edit"></i> Modifier</button>';

        var btnAddPeriodeDeclarationBien = '';

        if (controlAccess('ADD_NEW_PERIODE_DECLARATION_ASSUJ')) {
            btnAddPeriodeDeclarationBien = '<br/><br/><a onclick="addPeriodeDeclarationBien(\'' + result[i].bienName + '\',\'' + result[i].idAssujettissement + '\')" class="btn btn-warning" title="Ajouter Exercice"><i class="fa fa-plus-circle"></i>&nbsp;Ajouter exercice</a>';
        }

        var inputSelected = '<hr/><br/>' + '<input  height="40px" weight="40px" type="checkbox" id="checkbox_' + result[i].codeBien + '" onclick="getBienSelected(\'' + result[i].codeBien + '\')"><span style="font-weight:bold;color:red">Cochez ici pour taxer</span></input>';

        var declaredInfo = '';
        var valueDisabled = 'disabled="true"';

        if (result[i].isDeclared == '0') {
            inputSelected = '';
            declaredInfo = 'EST DECLARE : ' + '<span style="font-weight:bold;color:red">' + 'NON' + '</span>';
        } else {
            totalBienSelected++;
            inputSelected = '';
            //valueDisabled = 'disabled="true"';
            declaredInfo = 'EST DECLARE : ' + '<span style="font-weight:bold;color:green">' + 'OUI' + '</span>';
        }

        var categorieInfo = 'CATEGORIE : ' + '<span style="font-weight:bold">' + result[i].nameTarifBien + '</span>';
        var typeInfo = 'MODELE : ' + '<span style="font-weight:bold">' + result[i].modeleName + '</span>';
        var marqueInfo = 'MARQUE : ' + '<span style="font-weight:bold">' + result[i].marqueName + '</span>';
        var puissanceInfo = 'PUISSANCE FISCALE : ' + '<span style="font-weight:bold">' + result[i].puissanceFiscal + ' ' + result[i].nameUnitePuissance + '</span>';
        var anneeCirculationInfo = 'ANNEE DE MISE EN CIRCULATION : ' + '<span style="font-weight:bold">' + result[i].anneeCirculation + '</span>';
        var dateCirculationInfo = 'DATE DE MISE EN CIRCULATION : ' + '<span style="font-weight:bold">' + result[i].dateMiseCirculation1 + '</span>';

        var descriptionInfo = typeInfo + '<br/>' + marqueInfo + '<br/>' + categorieInfo + '<br/>' + puissanceInfo + '<br/>' + dateCirculationInfo + '<br/>' + anneeCirculationInfo;

        var denomination = 'DENOMINATION : ' + '<span style="font-weight:bold">' + result[i].denomination + '</span>';
        var adresseInfo = 'ADRESSE : ' + '<span style="font-weight:bold">' + result[i].nameAdresse + '</span>';

        var denominationInfo = denomination + '<br/><br/>' + adresseInfo + '<br/><br/>' + declaredInfo;

        tableContent += '<tr id="row_' + result[i].codeBien + '">';

        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle"><input  type="checkbox" ' + valueDisabled + 'id="checkbox_' + result[i].codeBien + '" onclick="checkingAllRows(\'' + checkAllRows + '\',\'' + result[i].tauxVignette + '\',\'' + result[i].devise + '\')" /></th>';

        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + count + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].nameTypeBien + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + denominationInfo + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + descriptionInfo + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].plaque + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].chassis + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold;font-size:18px">' + formatNumber(result[i].tauxVignette, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + btnEditBienAutomobile + inputSelected + btnAddPeriodeDeclarationBien + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';
    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="3" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL: </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableBienAutomobile.html(tableContent);

    tableBienAutomobile.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des biens automobile est vide",
            search: "Filtrer la liste des biens automobile ici",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"}
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false, ordering: false,
        pageLength: 50,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(3).footer()).html(totalBien);
        },
        columnDefs: [
            {"visible": false, "targets": 2}
        ],
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        /*select: {
         style: 'os',
         blurable: true
         },*/
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    if (totalBienSelected == totalBien) {

        idDivCheck.attr('style', 'display:none');
        btnTaxationAllBien.attr('style', 'display:none');

    } else {

        if (controlAccess('SET_TAXATION_VIGNETTE')) {

            if (totalBien > 0) {

                idDivCheck.attr('style', 'display:block');

            } else {
                idDivCheck.attr('style', 'display:none');

            }
        } else {
            idDivCheck.attr('style', 'display:none');
        }
    }

}

function editBienAutomobile(code, denomination) {

    valueDenomination = '<span style="font-weight:bold">' + denomination + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir afficher les informations de ce bien automobile : ' + valueDenomination + ' ?', function () {


        for (var i = 0; i < bienAutomobileList.length; i++) {

            if (bienAutomobileList[i].codeBien == code) {

                codeBienSelected = code;
                codeAdressePersonneBienSelected = bienAutomobileList[i].codeAdressePersonneBien;
                selectedCodeAP = codeAdressePersonneBienSelected;

                codeContribuableSelected = bienAutomobileList[i].codePersonne;

                selectGenreEdit.val(bienAutomobileList[i].codeTypeBien);
                //selectCategorieEdit.val(bienAutomobileList[i].codeTarifBien);
                denominationEdit.val(bienAutomobileList[i].denomination);

                denominationEdit.attr('style', 'font-weight:bold');

                MarqueEdit.val(bienAutomobileList[i].marquecode == '' ? '0' : bienAutomobileList[i].marquecode);
                TypeEdit.val(bienAutomobileList[i].modelecode == '' ? '0' : bienAutomobileList[i].modelecode);

                DateMiseCirculation.val(bienAutomobileList[i].dateMiseCirculation2);

                NumeroPlaqueEdit.val(bienAutomobileList[i].plaque);
                NumeroChassisEdit.val(bienAutomobileList[i].chassis);
                PuissanceFiscalEdit.val(bienAutomobileList[i].puissanceFiscal);
                UnitePuissanceFiscalEdit.val(bienAutomobileList[i].codeUnitePuissance);
                AnneeCirculationEdit.val(bienAutomobileList[i].anneeCirculation);
                AdresseEdit.val(bienAutomobileList[i].nameAdresse);
                AdresseEdit.attr('style', 'font-weight:bold');

                btnSaveBien.attr('disabled', false);
                PuissanceFiscalEdit.attr('disabled', false);

                typeVehicule.val(bienAutomobileList[i].estVu);

                if (PuissanceFiscalEdit.val() !== '') {

                    for (var j = 0; j < plageCategorieList.length; j++) {

                        if (plageCategorieList[j].codeUnite == UnitePuissanceFiscalEdit.val()) {



                            if (bienAutomobileList[i].codeTypeBien == 'TB00000037' || bienAutomobileList[i].codeTypeBien == 'TB00000039') {

                                if (plageCategorieList[j].plageMin <= PuissanceFiscalEdit.val()
                                        && plageCategorieList[j].plageMax >= PuissanceFiscalEdit.val()
                                        && plageCategorieList[j].codeGenre == bienAutomobileList[i].codeTypeBien) {

                                    selectCategorieEdit.val(plageCategorieList[j].codeTarif);
                                    typeVehicule.val('OUI');
                                    
                                    break;
                                }

                            } else {

                                if (plageCategorieList[j].plageMin <= PuissanceFiscalEdit.val()
                                        && plageCategorieList[j].plageMax >= PuissanceFiscalEdit.val()) {

                                    selectCategorieEdit.val(plageCategorieList[j].codeTarif);
                                    typeVehicule.val('NON');
                                    break;
                                }

                                /*if (typeVehicule.val() == 'OUI') {
                                 
                                 
                                 for (var j = 0; j < plageCategorieListVU.length; j++) {
                                 
                                 if (plageCategorieListVU[j].plageMin <= PuissanceFiscalEdit.val()
                                 && plageCategorieListVU[j].plageMax >= PuissanceFiscalEdit.val()) {
                                 
                                 selectCategorieEdit.val(plageCategorieListVU[j].codeTarif);
                                 
                                 break;
                                 
                                 }
                                 }
                                 
                                 } else {
                                 
                                 if (plageCategorieList[i].plageMin <= PuissanceFiscalEdit.val()
                                 && plageCategorieList[i].plageMax >= PuissanceFiscalEdit.val()
                                 && plageCategorieList[i].codeTarif == bienAutomobileList[i].codeTarifBien) {
                                 
                                 selectCategorieEdit.val(plageCategorieList[i].codeTarif);
                                 break;
                                 }
                                 }*/
                            }
                        }

                    }
                }



                if (bienAutomobileList[i].taxationExist == false) {

                    if (controlAccess('DELETE_BIEN_AUTOMOBILE')) {
                        btnDeleteBien.attr('style', 'display:inline');
                    } else {
                        btnDeleteBien.attr('style', 'display:none');
                    }

                    if (controlAccess('DELETE_ALL_BIEN_AUTOMOBILE_NOT_TAXED')) {
                        btnDeleteAllBienNotTaxed.attr('style', 'display:inline');
                    } else {
                        btnDeleteAllBienNotTaxed.attr('style', 'display:none');
                    }


                } else {
                    btnDeleteBien.attr('style', 'display:none');
                    btnDeleteAllBienNotTaxed.attr('style', 'display:none');
                }

                break;
            }
        }
    });


}

function getSelectedAssujetiData() {

    codeContribuableSelected = selectAssujettiData.code;
    fJContribuableSelected = selectAssujettiData.codeForme;
    nameContribuableSelected = selectAssujettiData.nomComplet;
    codeAdrPricipalContribuableSelected = selectAssujettiData.codeAdressePersonne;

    contribuableNameEdit.val(selectAssujettiData.nomComplet + ' (Adresse : ' + selectAssujettiData.adresse.toUpperCase() + ')');
    contribuableNameEdit.attr('style', 'font-weight:bold');

    btnAjouterAdresseEdit.attr('disabled', false);
    btnAfficherAllBien.attr('disabled', false);

    //loadBienAutomobileByContribuable();
    loadFiveLastBienAutomobileByContribuable();

    selectedCodeAP = '';
    codeAdressePersonneBienSelected = '';

    AdresseEdit.val(selectAssujettiData.adresse.toUpperCase());
    AdresseEdit.attr('style', 'font-weight:bold');

    selectedCodeAP = selectAssujettiData.codeAdressePersonne;
    codeAdressePersonneBienSelected = selectedCodeAP;

    btnSaveBien.attr('disabled', false);

    idDivCheck.attr('style', 'display:none');
    btnTaxationAllBien.attr('style', 'display:none');
    checkBoxSelectAllBien.attr('checked', false);
}

function getSelectedAdressePersonneData() {

    AdresseEdit.val(selectedAdressePersonne.chaine);
    AdresseEdit.attr('style', 'font-weight:bold');
    selectedCodeAP = selectedAdressePersonne.codeAP;
    codeAdressePersonneBienSelected = selectedCodeAP;

    btnSaveBien.attr('disabled', false);
}

function initInfosConnexeBienAutomobile() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTypeBienByService',
            'codeService': userData.serviceCode,
            'type': 1
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {
                $.unblockUI();
                alertify.alert('Aucun genre de biens automobile trouvé.');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                var typeBienList = result.typeBienList;
                var categorieBienList = result.categorieBienList;
                categorieBienListGlobal = result.categorieBienList;
                
                var marqueVehiculeList = result.marqueVehiculeList;
                var modeleVehiculeList = result.modeleVehiculeList;

                plageCategorieList = result.plageCategorieList;
                plageCategorieListVU = result.plageCategorieListVU;

                tempTypeBienList = [];
                tempCategorieBienList = [];

                var dataTypeBien = '<option value ="0">--</option>';

                for (var i = 0; i < typeBienList.length; i++) {

                    var typeBien = new Object();
                    typeBien.codeTypeBien = typeBienList[i].codeTypeBien;
                    typeBien.libelleTypeBien = typeBienList[i].libelleTypeBien;
                    typeBien.estContractuel = typeBienList[i].estContractuel;
                    tempTypeBienList.push(typeBien);

                    dataTypeBien += '<option style="font-weight: bold" value =' + typeBienList[i].codeTypeBien + '>' + typeBienList[i].libelleTypeBien + '</option>';
                }

                dataTypeBien += '<option value="new" style="font-weight:bold;font-style: italic;color:green;font-size:16px">+Ajouter un nouveau genre</option>';

                selectGenreEdit.html(dataTypeBien);
                selectGenreSelected.html(dataTypeBien);

                var dataCategorieBien = '<option value ="0">--</option>';

                for (var i = 0; i < categorieBienList.length; i++) {

                    var categorieBien = new Object();
                    typeBien.tarifCode = categorieBienList[i].tarifCode;
                    typeBien.tarifName = categorieBienList[i].tarifName;

                    tempCategorieBienList.push(categorieBien);

                    dataCategorieBien += '<option style="font-weight: bold" value =' + categorieBienList[i].tarifCode + '>' + categorieBienList[i].tarifName + '</option>';
                }

                selectCategorieEdit.html(dataCategorieBien);
                selectCategorieSelected.html(dataCategorieBien);

                var dataMarqueVehicule = '<option value ="0">--</option>';

                for (var i = 0; i < marqueVehiculeList.length; i++) {
                    dataMarqueVehicule += '<option value =' + marqueVehiculeList[i].marqueCode + '>' + marqueVehiculeList[i].marqueName.toUpperCase() + '</option>';
                }

                dataMarqueVehicule += '<option value="new" style="font-weight:bold;font-style: italic;color:green;font-size:16px">+Ajouter une nouvelle marque</option>';

                MarqueEdit.html(dataMarqueVehicule);

                var dataModeleVehicule = '<option value="0">--</option>';

                for (var i = 0; i < modeleVehiculeList.length; i++) {
                    dataModeleVehicule += '<option style="font-weight: bold" value =' + modeleVehiculeList[i].modeleCode + '>' + modeleVehiculeList[i].modeleName.toUpperCase() + '</option>';
                }

                dataModeleVehicule += '<option value="new" style="font-weight:bold;font-style: italic;color:green;font-size:16px">+Ajouter un nouveau modèle</option>';

                TypeEdit.html(dataModeleVehicule)

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadTypeComplementBiens(codeTypeBien) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTypeComplementBien',
            'codeTypeBien': codeTypeBien,
            'codePersonne': codeContribuableSelected,
            'idBien': codeBienSelected
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {

                var numberBien = response.numberOfBien;

                if (numberBien < 10) {
                    numberBien = "0" + numberBien;
                }

                denominationEdit.val(response.nomePersonne + ' : ' + $('#selectGenreEdit option:selected').text().toUpperCase() + " - " + numberBien);
                denominationEdit.attr('style', 'font-weight:bold');
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function checkFields() {

    if (selectCategorieEdit.val() == '') {

        alertify.alert('Veuillez d\'abord sélectionner la catégorie');
        return;
    }

    if (MarqueEdit.val() == '0') {

        alertify.alert('Veuillez d\'abord sélectionner la marque');
        return;
    }

    if (TypeEdit.val() == '0') {

        alertify.alert('Veuillez d\'abord sélectionner le modèle');
        return;
    }

    if (NumeroPlaqueEdit.val() == '') {

        alertify.alert('Veuillez d\'abord fournir le numéro de plaque');
        return;
    }

    if (NumeroChassisEdit.val() == '') {

        alertify.alert('Veuillez d\'abord fournir le numéro de chassis');
        return;
    }

    if (DateMiseCirculation.val() == '' || DateMiseCirculation.val() == null) {

        alertify.alert('Veuillez d\'abord fournir la date de mise en circulation');
        return;
    }

    if (PuissanceFiscalEdit.val() == '') {

        alertify.alert('Veuillez d\'abord fournir la puissance fiscale');
        return;
    }

    if (UnitePuissanceFiscalEdit.val() == '') {

        alertify.alert('Veuillez d\'abord sélectionner l\'unité de la puissance fiscale');
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir enregistrer ce bien automobile ?', function () {
        saveBienAutomobile();
    });
}

function saveBienAutomobile() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveBienAutomobile',
            'codeGenre': selectGenreEdit.val(),
            'codePersonne': codeContribuableSelected,
            'denomination': denominationEdit.val(),
            'marque': MarqueEdit.val().toUpperCase(),
            'type': TypeEdit.val().toUpperCase(),
            'plaque': NumeroPlaqueEdit.val().toUpperCase(),
            'chassis': NumeroChassisEdit.val().toUpperCase(),
            'puissanceFiscale': PuissanceFiscalEdit.val(),
            'unitePuissance': UnitePuissanceFiscalEdit.val(),
            'exercice': ExerciceEdit.val(),
            'anneMiseCirculation': AnneeCirculationEdit.val(),
            'dateMiseCirculation': DateMiseCirculation.val(),
            'codeAP': selectedCodeAP,
            'categorie': selectCategorieEdit.val(),
            'fjPersonne': fJContribuableSelected,
            'idUser': userData.idUser,
            'codeBien': codeBienSelected
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' || response == '0') {

                showResponseError();
                return;

            } else if (response == '2') {

                var valuePlaque = '<span style="font-weight:bold">' + NumeroPlaqueEdit.val().toUpperCase() + '</span>';
                alertify.alert('Ce № plaque : ' + valuePlaque + ' est déjà associé à un bien automobile');
                return;

            } else if (response == '3') {

                var valueChassis = '<span style="font-weight:bold">' + NumeroChassisEdit.val().toUpperCase() + '</span>';
                alertify.alert('Ce № chassis : ' + valueChassis + ' est déjà associé à un bien automobile');
                return;

            } else {

                valueDenomination = '<span style="font-weight:bold">' + denominationEdit.val().toUpperCase() + '</span>';

                alertify.alert('L\'Enregistrement du bien automobile dénommé : ' + valueDenomination + ' s\'est effectué avec succès.');
                resetFields();
                //loadBienAutomobileByContribuable();
                loadFiveLastBienAutomobileByContribuable();
            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadFiveLastBienAutomobileByContribuable() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadFiveLastBienAutomobileByContribuable',
            'codePersonne': codeContribuableSelected
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des biens en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                showResponseError();
                return;

            } else if (response == '0') {

                printTableBienAutomobile('');

            } else {

                bienAutomobileList = $.parseJSON(JSON.stringify(response));
                printTableBienAutomobile(bienAutomobileList);

                //btnDeleteAllBienNotTaxed.attr('disabled', false);

                if (controlAccess('DELETE_ALL_BIEN_AUTOMOBILE_NOT_TAXED')) {
                    btnDeleteAllBienNotTaxed.attr('style', 'display:inline');
                } else {
                    btnDeleteAllBienNotTaxed.attr('style', 'display:none');
                }

            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function getCotationBienAutomobileByContribuable() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getCotationBienAutomobileByContribuable',
            'codePersonne': codeContribuableSelected,
            'codeFormeJ': fJContribuableSelected
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement de la cotation en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                printTableCotationBienAutomobile('');
                showResponseError();
                return;

            } else if (response == '0') {

                printTableCotationBienAutomobile('');

            } else {

                var result = $.parseJSON(JSON.stringify(response));
                printTableCotationBienAutomobile(result);
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            printTableCotationBienAutomobile('');
            showResponseError();
        }

    });
}

function loadBienAutomobileByContribuable() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBienAutomobileByContribuable',
            'codePersonne': codeContribuableSelected
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des biens en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                showResponseError();
                return;

            } else if (response == '0') {

                printTableBienAutomobile('');

            } else {

                bienAutomobileList = $.parseJSON(JSON.stringify(response));
                printTableBienAutomobile(bienAutomobileList);
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function getBienSelected(code) {

    var row = $("#row_" + code);
    var checkBox = document.getElementById("checkbox_" + code);

    if (checkBox.checked) {

        row.attr('style', 'background-color:#daf2c9;color:black');

        checkExist = true;

        for (var j = 0; j < bienAutomobileList.length; j++) {

            if (bienAutomobileList[j].codeBien == code) {

                objBienSelected = {};
                objBienSelected.codeBien = code;
                bienAutomobileSelectedList.push(objBienSelected);
            }
        }

        if (bienAutomobileSelectedList.length == 0) {
            spnNbreBien.html('');
        } else if (bienAutomobileSelectedList.length == 1) {
            spnNbreBien.html('de ce ' + '<span style="font-weight: bold;font-size: 18px">' + bienAutomobileSelectedList.length + '</span>' + ' bien');
        } else if (bienAutomobileSelectedList.length > 1) {
            spnNbreBien.html('de ces ' + '<span style="font-weight: bold;font-size: 18px">' + bienAutomobileSelectedList.length + '</span>' + ' biens');
        }

        btnDeleteBien.attr('disabled', false);
        //btnDeleteAllBienNotTaxed.attr('disabled', false);
        btnDeleteAllBienNotTaxed.attr('style', 'display:none');

    } else {

        row.removeAttr('style');

        for (var i = 0; i < bienAutomobileSelectedList.length; i++) {

            if (bienAutomobileSelectedList[i].codeBien == code) {

                bienAutomobileSelectedList.splice(i, 1);
                break;
            }
        }

        if (bienAutomobileSelectedList.length == 0) {
            checkExist = false;

            if (controlAccess('DELETE_BIEN_AUTOMOBILE')) {
                btnDeleteBien.attr('disabled', true);
            } else {
                btnDeleteBien.attr('disabled', false);
            }

            if (controlAccess('DELETE_ALL_BIEN_AUTOMOBILE_NOT_TAXED')) {
                //btnDeleteAllBienNotTaxed.attr('disabled', true);
                btnDeleteAllBienNotTaxed.attr('style', 'display:inline');
            } else {
                //btnDeleteAllBienNotTaxed.attr('disabled', false);
                btnDeleteAllBienNotTaxed.attr('style', 'display:none');
            }


        } else {
            checkExist = true;
            btnDeleteBien.attr('disabled', false);
            //btnDeleteAllBienNotTaxed.attr('disabled', false);
            btnDeleteAllBienNotTaxed.attr('style', 'display:none');
        }

        if (bienAutomobileSelectedList.length == 0) {
            spnNbreBien.html('');
        } else if (bienAutomobileSelectedList.length == 1) {
            spnNbreBien.html('de ce ' + '<span style="font-weight: bold;font-size: 18px">' + bienAutomobileSelectedList.length + '</span>' + ' bien');
        } else if (bienAutomobileSelectedList.length > 1) {
            spnNbreBien.html('de ces ' + '<span style="font-weight: bold;font-size: 18px">' + bienAutomobileSelectedList.length + '</span>' + ' biens');
        }

    }
}

function addNewGenreVehicule() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'genre': inputNewGenre.val().toUpperCase().trim(),
            'userId': userData.idUser,
            'operation': 'addNewGenreVehicule'
        },
        beforeSend: function () {
            newGenreModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ajout du nouveau genre en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                newGenreModal.unblock();

                var value = '<span style="font-weight:bold">' + inputNewGenre.val().toUpperCase().trim() + '</span>';

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Ce genre de véhicule : ' + value + ' existe déjà dans le système');
                    return;

                } else {
                    alertify.alert('L\'ajout du nouveau genre de véhicule : ' + value + ' s\'est effectué avec succès');
                    newGenreModal.modal('hide');
                    initListGenreBienAutomobile();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            newGenreModal.unblock();
            showResponseError();
        }
    });

}

function addNewMarqueVehicule() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'marque': inputNewMarque.val().toUpperCase().trim(),
            'userId': userData.idUser,
            'operation': 'addNewMarqueVehicule'
        },
        beforeSend: function () {
            newMarqueModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ajout de la nouvelle marque en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                newMarqueModal.unblock();

                var value = '<span style="font-weight:bold">' + inputNewMarque.val().toUpperCase().trim() + '</span>';

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Ce genre de véhicule : ' + value + ' existe déjà dans le système');
                    return;

                } else {
                    alertify.alert('L\'ajout de la nouvelle marque de véhicule : ' + value + ' s\'est effectué avec succès');
                    newMarqueModal.modal('hide');
                    initListMarqueBienAutomobile();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            newMarqueModal.unblock();
            showResponseError();
        }
    });

}

function addNewModeleVehicule() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'modele': inputNewModele.val().toUpperCase().trim(),
            'userId': userData.idUser,
            'operation': 'addNewModeleVehicule'
        },
        beforeSend: function () {
            newModeleModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ajout du nouveau modèle en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                newModeleModal.unblock();

                var value = '<span style="font-weight:bold">' + inputNewModele.val().toUpperCase().trim() + '</span>';

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Ce modèle de véhicule : ' + value + ' existe déjà dans le système');
                    return;

                } else {
                    alertify.alert('L\'ajout du nouveau modèle de véhicule : ' + value + ' s\'est effectué avec succès');
                    newModeleModal.modal('hide');
                    initListModeleBienAutomobile();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            newModeleModal.unblock();
            showResponseError();
        }
    });

}

function loadAdressesOfPersonneV2() {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAdressesPersonne',
            'codePersonne': codeContribuableSelected
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {
                adressePersonneListX = $.parseJSON(JSON.stringify(response));
                changeADR = true;
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function initListModeleBienAutomobile() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTypeBienByService',
            'codeService': userData.serviceCode,
            'type': 1
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {
                $.unblockUI();
                alertify.alert('Aucun genre de biens automobile trouvé.');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));
                var modeleVehiculeList = result.modeleVehiculeList;

                var dataModeleVehicule = '<option value="0">--</option>';

                for (var i = 0; i < modeleVehiculeList.length; i++) {
                    dataModeleVehicule += '<option style="font-weight: bold" value =' + modeleVehiculeList[i].modeleCode + '>' + modeleVehiculeList[i].modeleName.toUpperCase() + '</option>';
                }

                dataModeleVehicule += '<option value="new" style="font-weight:bold;font-style: italic;color:green;font-size:16px">+Ajouter un nouveau modèle</option>';

                TypeEdit.html(dataModeleVehicule)

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function initListMarqueBienAutomobile() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTypeBienByService',
            'codeService': userData.serviceCode,
            'type': 1
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {
                $.unblockUI();
                alertify.alert('Aucun genre de biens automobile trouvé.');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));
                var marqueVehiculeList = result.marqueVehiculeList;


                var dataMarqueVehicule = '<option value ="0">--</option>';

                for (var i = 0; i < marqueVehiculeList.length; i++) {
                    dataMarqueVehicule += '<option value =' + marqueVehiculeList[i].marqueCode + '>' + marqueVehiculeList[i].marqueName.toUpperCase() + '</option>';
                }

                dataMarqueVehicule += '<option value="new" style="font-weight:bold;font-style: italic;color:green;font-size:16px">+Ajouter une nouvelle marque</option>';

                MarqueEdit.html(dataMarqueVehicule);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function initListGenreBienAutomobile() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTypeBienByService',
            'codeService': userData.serviceCode,
            'type': 1
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {
                $.unblockUI();
                alertify.alert('Aucun genre de biens automobile trouvé.');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                var typeBienList = result.typeBienList;


                var dataTypeBien = '<option value ="0">--</option>';

                for (var i = 0; i < typeBienList.length; i++) {

                    var typeBien = new Object();
                    typeBien.codeTypeBien = typeBienList[i].codeTypeBien;
                    typeBien.libelleTypeBien = typeBienList[i].libelleTypeBien;
                    typeBien.estContractuel = typeBienList[i].estContractuel;
                    tempTypeBienList.push(typeBien);

                    dataTypeBien += '<option style="font-weight: bold" value =' + typeBienList[i].codeTypeBien + '>' + typeBienList[i].libelleTypeBien + '</option>';
                }

                dataTypeBien += '<option value="new" style="font-weight:bold;font-style: italic;color:green;font-size:16px">+Ajouter un nouveau genre</option>';

                selectGenreEdit.html(dataTypeBien);
                selectGenreSelected.html(dataTypeBien);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function checkingAllRows(codeBien, montant, devise) {

    var checkBox = document.getElementById("checkbox_" + codeBien);
    var row = $("#row_" + codeBien);

    if (checkBox.checked) {

        var bienSelected = new Object();

        bienSelected.codeBien = codeBien;
        bienSelected.montant = parseFloat(montant);
        bienSelected.devise = devise;

        bienAutomobileSelectedForTaxationList.push(bienSelected);

        totalBienSelected++;

        if (bienAutomobileSelectedForTaxationList.length > 0) {
            //if (bienAutomobileSelectedForTaxationList.length == 1) {

            //idDivCheck.attr('style', 'display:block');
            btnTaxationAllBien.attr('style', 'display:block');
            //checkBoxSelectAllBien.attr('checked', true);

        }

    } else {

        for (var i = 0; i < bienAutomobileSelectedForTaxationList.length; i++) {

            if (bienAutomobileSelectedForTaxationList[i].codeBien == codeBien) {

                bienAutomobileSelectedForTaxationList.splice(i, 1);

                if (bienAutomobileSelectedForTaxationList.length == 0) {

                    idDivCheck.attr('style', 'display:none');
                    btnTaxationAllBien.attr('style', 'display:none');
                    checkBoxSelectAllBien.attr('checked', false);
                    totalBienSelected = 0;


                } else {
                    totalBienSelected--;
                }

                row.removeAttr('style');
            }
        }
    }

}

var totalBienSelected = 0;
var totalBienTaxed = 0;

function printTableBienAutomobileV2(result, check) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col"></th>';
    tableContent += '<th style="text-align:center;width:5%">№</th>';
    tableContent += '<th style="text-align:left;width:20%">GENRE</th>';
    tableContent += '<th style="text-align:left;width:20%">DENOMINATION</th>';
    tableContent += '<th style="text-align:left;width:20%">DESCRIPTION</th>';
    tableContent += '<th style="text-align:left;width:10%">№ PLAQUE</th>';
    tableContent += '<th style="text-align:left;width:10%">№ CHASSIS</th>';
    tableContent += '<th style="text-align:right;width:10%">TAUX VIGNETTE</th>';
    tableContent += '<th style="text-align:center;width:10%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var count = 0;
    var totalBien = result.length;

    totalBienSelected = 0;
    totalBienTaxed = 0;
    var taxationNotExist = 0;

    for (var i = 0; i < result.length; i++) {

        count++;

        var rowChecked = '';
        var valueDisabled = '';
        //var valueDisabled = 'disabled="true"';
        var valueBackground = '';

        var btnEditBienAutomobile = '<button class="btn btn-success" onclick="editBienAutomobile(\'' + result[i].codeBien + '\',\'' + result[i].denomination + '\')"><i class="fa fa-edit"></i> Modifier</button>';

        var btnAddPeriodeDeclarationBien = '';

        if (controlAccess('ADD_NEW_PERIODE_DECLARATION_ASSUJ')) {
            btnAddPeriodeDeclarationBien = '<br/><br/><a onclick="addPeriodeDeclarationBien(\'' + result[i].bienName + '\',\'' + result[i].idAssujettissement + '\')" class="btn btn-warning" title="Ajouter Exercice"><i class="fa fa-plus-circle"></i>&nbsp;Ajouter exercice</a>';
        }

        var inputSelected = '<hr/><br/>' + '<input  height="40px" weight="40px" type="checkbox" id="checkbox_' + result[i].codeBien + '" onclick="getBienSelected(\'' + result[i].codeBien + '\')"><span style="font-weight:bold;color:red">Cochez ici pour taxer</span></input>';

        var declaredInfo = '';

        if (result[i].isDeclared == '0') {

            if (check == true) {
                valueBackground = 'background-color:#daf2c9';
                //valueBackground = 'background-color:#FFA500';
                rowChecked = 'checked';
                valueDisabled = '';
                totalBienSelected++;

                var bienSelected = new Object();

                bienSelected.codeBien = result[i].codeBien;
                bienSelected.montant = result[i].tauxVignette;
                bienSelected.devise = result[i].devise;

                bienAutomobileSelectedForTaxationList.push(bienSelected);

            }

            inputSelected = '';
            declaredInfo = 'EST DECLARE : ' + '<span style="font-weight:bold;color:red">' + 'NON' + '</span>';
        } else {
            valueDisabled = 'disabled="true"';
            taxationNotExist = 1;
            totalBienTaxed++;
            //rowChecked = '';
            inputSelected = '';
            declaredInfo = 'EST DECLARE : ' + '<span style="font-weight:bold;color:green">' + 'OUI' + '</span>';
        }

        var categorieInfo = 'CATEGORIE : ' + '<span style="font-weight:bold">' + result[i].nameTarifBien + '</span>';
        var typeInfo = 'MODELE : ' + '<span style="font-weight:bold">' + result[i].modeleName + '</span>';
        var marqueInfo = 'MARQUE : ' + '<span style="font-weight:bold">' + result[i].marqueName + '</span>';
        var puissanceInfo = 'PUISSANCE FISCALE : ' + '<span style="font-weight:bold">' + result[i].puissanceFiscal + ' ' + result[i].nameUnitePuissance + '</span>';
        var anneeCirculationInfo = 'ANNEE DE MISE EN CIRCULATION : ' + '<span style="font-weight:bold">' + result[i].anneeCirculation + '</span>';
        var dateCirculationInfo = 'DATE DE MISE EN CIRCULATION : ' + '<span style="font-weight:bold">' + result[i].dateMiseCirculation1 + '</span>';

        var descriptionInfo = typeInfo + '<br/>' + marqueInfo + '<br/>' + categorieInfo + '<br/>' + puissanceInfo + '<br/>' + dateCirculationInfo + '<br/>' + anneeCirculationInfo;

        var denomination = 'DENOMINATION : ' + '<span style="font-weight:bold">' + result[i].denomination + '</span>';
        var adresseInfo = 'ADRESSE : ' + '<span style="font-weight:bold">' + result[i].nameAdresse + '</span>';

        var denominationInfo = denomination + '<br/><br/>' + adresseInfo + '<br/><br/>' + declaredInfo;

        tableContent += '<tr id="row_' + result[i].codeBien + '">';

        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle;' + valueBackground + '"><input  type="checkbox" ' + valueDisabled + ' ' + rowChecked + ' id="checkbox_' + result[i].codeBien + '" onclick="checkingAllRows(\'' + result[i].codeBien + '\',\'' + result[i].tauxVignette + '\',\'' + result[i].devise + '\')" /></th>';

        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + count + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].nameTypeBien + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + denominationInfo + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + descriptionInfo + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].plaque + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].chassis + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold;font-size:18px">' + formatNumber(result[i].tauxVignette, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + btnEditBienAutomobile + inputSelected + btnAddPeriodeDeclarationBien + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="3" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL: </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableBienAutomobile.html(tableContent);

    tableBienAutomobile.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des biens automobile est vide",
            search: "Filtrer la liste des biens automobile ici",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"}
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false, ordering: false,
        pageLength: 50,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(3).footer()).html(
                    'SELECTION : ' + totalBienSelected +
                    '<hr/>' +
                    'DEJA TAXE(S) : ' + totalBienTaxed +
                    '<hr/>' +
                    'BIEN(S) : ' + totalBien
                    );
        },
        columnDefs: [
            {"visible": false, "targets": 2}
        ],
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        /*select: {
         style: 'os',
         blurable: true
         },*/
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    if (totalBienSelected == totalBien && taxationNotExist == 1) {

        idDivCheck.attr('style', 'display:none');
        btnTaxationAllBien.attr('style', 'display:none');

    } else {

        if (controlAccess('SET_TAXATION_VIGNETTE')) {

            if (totalBien > 0) {

                idDivCheck.attr('style', 'display:block');

            } else {
                idDivCheck.attr('style', 'display:none');

            }
        } else {
            idDivCheck.attr('style', 'display:none');
        }
    }

}

function loadingBankData() {

    var dataBank = empty;
    var defautlValue = '--';
    dataBank += '<option value="0">' + defautlValue + '</option>';

    var banqueUserList = JSON.parse(userData.banqueUserList);

    for (var i = 0; i < banqueUserList.length; i++) {

        dataBank += '<option value="' + banqueUserList[i].banqueCode + '" >' + banqueUserList[i].banqueName + '</option>';

    }

    selectBanque.html(dataBank);

    //periodeDeclarationModal.modal('show');
}

function loadingAccountBankData(abCode, bankCode, deviseCode) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadingAccountBankDataV2',
            'codeAb': abCode,
            'codeDevise': deviseCode,
            'codeBanque': bankCode
        },
        beforeSend: function () {
            periodeDeclarationModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                periodeDeclarationModal.unblock();

                if (response == '-1') {

                    showResponseError();
                    return;

                } else {

                    var dataAccountBank = empty;
                    var defautlValue = '--';

                    dataAccountBank += '<option value="0">' + defautlValue + '</option>';

                    accountBankList = JSON.parse(JSON.stringify(response));

                    for (var i = 0; i < accountBankList.length; i++) {

                        dataAccountBank += '<option value="' + accountBankList[i].accountCode + '" >' + accountBankList[i].accountName + '</option>';

                    }

                    selectCompteBancaire.html(dataAccountBank);

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            periodeDeclarationModal.unblock();
            showResponseError();
        }
    });
}

function loadingPeriodeDeclaration() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadingPeriodeDeclaration',
            'codePersonne': codeContribuableSelected,
            'codeListBiens': JSON.stringify(bienAutomobileSelectedForTaxationList)
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();

                if (response == '-1') {

                    showResponseError();
                    return;

                } else {

                    /* periodeDeclarationList = $.parseJSON(JSON.stringify(response));
                     
                     var dataPD = empty;
                     var defautlValue = '--';
                     
                     dataPD += '<option value="0">' + defautlValue + '</option>';
                     
                     for (var i = 0; i < periodeDeclarationList.length; i++) {
                     
                     
                     dataPD += '<option value="' + periodeDeclarationList[i].code + '" >' + periodeDeclarationList[i].periode.toUpperCase() + '</option>';
                     }
                     
                     selectPeridoeDecl.html(dataPD);
                     loadingBankData();*/

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function taxationBatchVignette() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'codePersonne': codeContribuableSelected,
            'listBiens': JSON.stringify(bienAutomobileSelectedForTaxationList),
            'userId': userData.idUser,
            'codeSite': userData.SiteCode,
            'codePeriode': selectPeridoeDecl.val(),
            'devise': selectDevise.val(),
            'codeAB': AB_VIGNETTE,
            'requerant': nameContribuableSelected,
            'selectCompteBancaire': selectCompteBancaire.val(),
            'selectBanque': selectBanque.val(),
            'operation': 'taxationBatchVignette'
        },
        beforeSend: function () {
            periodeDeclarationModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Taxation en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                periodeDeclarationModal.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    alertify.alert('L\'enregistrement de la taxation s\'est effectué avec succès.');

                    setDocumentContent(response);

                    window.open('visualisation-document', '_blank');

                    selectDevise.val('0');
                    selectBanque.val('0');
                    selectCompteBancaire.val('0');
                    selectPeridoeDecl.val('2021');
                    bienAutomobileSelectedForTaxationList = [];

                    periodeDeclarationModal.modal('hide');

                    loadFiveLastBienAutomobileByContribuable();

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            periodeDeclarationModal.unblock();
            showResponseError();
        }
    });

}

function printTableNewPeriodeDeclaration(periodeList, beginValue) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="text-align:center"> PERIODE </th>';
    header += '<th style="text-align:left"> ECHEANCE DECLARATION </th>';
    header += '<th style="text-align:center"> COCHEZ </th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    tempPeriodeDeclarationList = [];

    for (var i = 0; i < periodeList.length; i++) {

        var readOnly = periodeList.length === 0 ? 'hidden' : 'checkbox';

        body += '<tr id="row_' + periodeList[i].ordrePeriode + '">';
        body += '<td style="text-align:center;width:10%;vertical-align:middle">' + periodeList[i].periode + '</td>';
        body += '<td style="text-align:left;width:20%;vertical-align:middle">' + periodeList[i].dateLimite + '</td>';
        body += '<td style="text-align:center;width:6%;vertical-align:middle"><input  type="' + readOnly + '" id="checkbox_' + periodeList[i].ordrePeriode + '" onclick="setNewPeriodeDeclarationSelected(\'' + periodeList[i].ordrePeriode + '\')" /></th>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableNewPDAssuj.html(tableContent);

    tableNewPDAssuj.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 12,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });

    spnPDDepart.html(beginValue);
    modalAddPeriodeDeclarationAssujettissement.modal('show');
}

function saveAddNewPriodeDeclaration() {

    var debut = '', fin = '', value = '';

    if (tempPeriodeDeclarationList.length == 1) {

        value = '';
        debut = tempPeriodeDeclarationList[0].dateDebut;
        fin = tempPeriodeDeclarationList[0].dateFin;

    } else if (tempPeriodeDeclarationList.length > 1) {

        for (var i = 0; i < tempPeriodeDeclarationList.length; i++) {

            if (value == '') {

                value = tempPeriodeDeclarationList[i].ordrePeriode;

                debut = tempPeriodeDeclarationList[i].dateDebut;
                fin = tempPeriodeDeclarationList[i].dateFin;

            } else {

                if (tempPeriodeDeclarationList[i].ordrePeriode < value) {

                    value = tempPeriodeDeclarationList[i].ordrePeriode;
                    debut = tempPeriodeDeclarationList[i].dateDebut;
                    fin = tempPeriodeDeclarationList[i].dateFin;
                }
            }
        }
    }

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveAddNewPriodeDeclaration',
            'codeAssuj': idAssujettissement,
            'dateDebut': debut,
            'dateFin': fin,
            'periodeList': JSON.stringify(tempPeriodeDeclarationList),
            'userId': userData.idUser
        },
        beforeSend: function () {

            modalAddPeriodeDeclarationAssujettissement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ajout des périodes en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                modalAddPeriodeDeclarationAssujettissement.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalAddPeriodeDeclarationAssujettissement.unblock();

                alertify.alert('L\'ajout de période de déclaration s\'est effectué avec succès.');
                modalAddPeriodeDeclarationAssujettissement.modal('hide');

                loadBiens(codeResponsible, '0');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalAddPeriodeDeclarationAssujettissement.unblock();
            showResponseError();
        }
    });
}

function getDatePeriode(ordre, debut) {

    var result = '';
    var x = '';

    for (var i = 0; i < tempPeriodeDeclarationList.length; i++) {

        if (ordre == tempPeriodeDeclarationList[i].ordrePeriode) {
            if (debut == true) {
                result = tempPeriodeDeclarationList[i].dateDebut;
            } else {
                result = tempPeriodeDeclarationList[i].dateFin;
            }
            break;
        }
    }
    return result;
}

function addPeriodeDeclarationBien(bienName, code) {

    idAssujettissement = code;

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getNewPeriodeDeclarationBien',
            'codeAssuj': code
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));
                beginPeriodValue = result[0].beginPeriod;

                resultNewPeriodeList = result[0].periodeList;

                printTableNewPeriodeDeclaration(resultNewPeriodeList, beginPeriodValue);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });

    spnBienAssujSelected.html(bienName);
}

function setNewPeriodeDeclarationSelected(id) {

    var checkBox = document.getElementById("checkbox_" + id);
    var row = $("#row_" + id);

    if (checkBox.checked) {

        row.attr('style', 'background-color:#daf2c9');
        checkExist = true;

        btnAjouterPD.attr('style', 'display:block');

        for (var i = 0; i < resultNewPeriodeList.length; i++) {

            if (resultNewPeriodeList[i].ordrePeriode == id) {

                var periode = new Object();

                periode.dateDebut = resultNewPeriodeList[i].dateDebut;
                periode.dateFin = resultNewPeriodeList[i].dateFin;
                periode.periode = resultNewPeriodeList[i].periode;
                periode.dateLimite = resultNewPeriodeList[i].dateLimite;
                periode.ordrePeriode = resultNewPeriodeList[i].ordrePeriode;
                periode.dateLimitePaiement = resultNewPeriodeList[i].dateLimitePaiement;

                tempPeriodeDeclarationList.push(periode);
            }
        }
    } else {


        row.removeAttr('style');

        for (var k = 0; k < tempPeriodeDeclarationList.length; k++) {

            if (tempPeriodeDeclarationList[k].ordrePeriode == id) {

                tempPeriodeDeclarationList.splice(k, 1);
                break;
            }
        }

        if (tempPeriodeDeclarationList.length > 0) {
            checkExist = true;
            btnAjouterPD.attr('style', 'display:block');
        } else {
            checkExist = false;
            btnAjouterPD.attr('style', 'display:none');

        }
    }


}

function printTableCotationBienAutomobile(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<th style="text-align:center;width:5%">№</th>';
    tableContent += '<th style="text-align:left;width:15%">CATEGORIE</th>';
    tableContent += '<th style="text-align:right;width:10%">TAUX</th>';
    tableContent += '<th style="text-align:center;width:15%">NOMBRE</th>';
    tableContent += '<th style="text-align:right;width:15%">TOTAL DU</th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var count = 0;
    var totalGeneral = 0;

    for (var i = 0; i < result.length; i++) {

        count++;
        totalGeneral += result[i].MONTANT;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + count + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle;font-weight:bold">' + result[i].TARIF + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold">' + formatNumber(result[i].TAUX, 'USD') + '</td>';
        tableContent += '<td style="text-align:center;width:15%;vertical-align:middle;font-weight:bold;font-size:15px">' + result[i].NOMBRE + '</td>';
        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle;font-weight:bold;font-size:16px;color:green">' + formatNumber(result[i].MONTANT, 'USD') + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableCotation.html(tableContent);
    tableCotation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des biens automobile est vide",
            search: "Filtrer la liste de la cotation ici",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"}
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false, ordering: false,
        pageLength: 50,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });

    if (result.length > 0) {

        spnNameContribuable.html(nameContribuableSelected);
        spnTotalGen.html(formatNumber(totalGeneral, 'USD'));
        modalDisplayCotation.modal('show');

    } else {

        spnTotalGen.html('');
        spnNameContribuable.html('');
        modalDisplayCotation.modal('hide');
    }

}

