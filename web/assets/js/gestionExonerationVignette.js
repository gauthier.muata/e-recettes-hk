/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var contribuableNameEdit,
        btnSearchContribuable,
        selectTypExoneration,
        btnSaveExoneration,
        btnDeleteExoneration,
        tableBienExonerationVignette,
        assujettiModal,
        btnTaxerExoneration,
        inputExonerationAccorder;

var typeExo;
var nameContribuableSelected, codeContribuableSelected, fJContribuableSelected;
var bienAutomobileList = [];

var periodeDeclarationModal, selectPD, btnValidateTaxationExo;

var listObjBienTaxe = [];

var selectBanqueExo, selectCompteBancaireExo;

$(function () {

    typeExo = 0;

    mainNavigationLabel.text('REPERTOIRE');
    secondNavigationLabel.text('Gestion des exonérations de vignette');
    removeActiveMenu();

    contribuableNameEdit = $('#contribuableNameEdit');
    btnSearchContribuable = $('#btnSearchContribuable');


    selectTypExoneration = $('#selectTypExoneration');
    btnSaveExoneration = $('#btnSaveExoneration');
    btnDeleteExoneration = $('#btnDeleteExoneration');
    tableBienExonerationVignette = $('#tableBienExonerationVignette');
    inputExonerationAccorder = $('#inputExonerationAccorder');
    assujettiModal = $('#assujettiModal');
    btnTaxerExoneration = $('#btnTaxerExoneration');

    periodeDeclarationModal = $('#periodeDeclarationModal');
    selectPD = $('#selectPD');
    btnValidateTaxationExo = $('#btnValidateTaxationExo');

    selectCompteBancaireExo = $('#selectCompteBancaireExo');
    selectBanqueExo = $('#selectBanqueExo');

    selectBanqueExo.change(function (e) {

        if (selectBanqueExo.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;
        } else {
            loadingAccountBankData(AB_VIGNETTE, selectBanqueExo.val(), 'USD');
        }

    });

    btnValidateTaxationExo.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectBanqueExo.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;

        }

        if (selectCompteBancaireExo.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un compete bancaire valide');
            return;

        }

        var value = '<span style="font-weight:bold">' + nameContribuableSelected.toUpperCase() + '</span>';

        alertify.confirm('Etes-vous sûre de vouloir enregistrer la taxation du contribuable : ' + value + ' ?', function () {

            taxationExonerationVignette();
        });

    });

    btnTaxerExoneration.on('click', function (e) {

        e.preventDefault();

        selectBanqueExo.val('0');
        selectCompteBancaireExo.val('0');

        loadingBankData();

        periodeDeclarationModal.modal('show');
    });

    btnSearchContribuable.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        codeFormeJ_X = '03';
        assujettiModal.modal('show');
    });

    btnDeleteExoneration.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var value = '<span style="font-weight:bold">' + nameContribuableSelected.toUpperCase() + '</span>';

        alertify.confirm('Etes-vous sûre de vouloir supprimer l\'exonération du contribuable : ' + value + ' ?', function () {

            deleteAccordExonerationVignette();
        });
    });

    btnSaveExoneration.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectTypExoneration.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un type exonération valide');
            return;

        } else if (selectTypExoneration.val() == typeExo) {

            var value = '<span style="font-weight:bold">' + $('#selectTypExoneration option:selected').text() + '</span>';

            alertify.alert('DESOLE ! Ce type Exonération : ' + value + ' est déjà accorder au contribuable sélectionner');
            return;

        } else {

            var value = '<span style="font-weight:bold">' + nameContribuableSelected.toUpperCase() + '</span>';

            alertify.confirm('Etes-vous sûre de vouloir accorder l\'exonération au contribuable : ' + value + ' ?', function () {

                saveAccordExonerationVignette();
            });
        }


    });

    printTableBienAutomobile('');

});

function deleteAccordExonerationVignette() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'codePersonne': codeContribuableSelected,
            'userId': userData.idUser,
            'operation': 'deleteAccordExonerationVignette'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('La suppression de l\'exonération s\'est effectuée avec succès.');
                    typeExo = 0;
                    inputExonerationAccorder.val('Exonération accordée : ');
                    btnDeleteExoneration.attr('disabled', true);
                    btnTaxerExoneration.attr('disabled', true);

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function printTableBienAutomobile(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center;width:5%">№</th>';
    tableContent += '<th style="text-align:left;width:20%">GENRE</th>';
    tableContent += '<th style="text-align:left;width:20%">DENOMINATION</th>';
    tableContent += '<th style="text-align:left;width:20%">DESCRIPTION</th>';
    tableContent += '<th style="text-align:left;width:10%">№ PLAQUE</th>';
    tableContent += '<th style="text-align:left;width:10%">№ CHASSIS</th>';
    tableContent += '<th style="text-align:right;width:10%">TAUX VIGNETTE</th>';
    //tableContent += '<th style="text-align:center;width:10%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var count = 0;
    var totalBien = result.length;

    for (var i = 0; i < result.length; i++) {

        var objBien = new Object();

        objBien.codeBien = result[i].codeBien;
        listObjBienTaxe.push(objBien);

        count++;

        //var btnEditBienAutomobile = '<button class="btn btn-success" onclick="editBienAutomobile(\'' + result[i].codeBien + '\',\'' + result[i].denomination + '\')"><i class="fa fa-edit"></i> Modifier</button>';
        var btnEditBienAutomobile = '';
        var inputSelected = '<hr/><br/>' + '<input  height="40px" weight="40px" type="checkbox" id="checkbox_' + result[i].codeBien + '" onclick="getBienSelected(\'' + result[i].codeBien + '\')"><span style="font-weight:bold;color:red">Cochez ici pour taxer</span></input>';

        var declaredInfo = '';

        if (result[i].isDeclared == '0') {
            inputSelected = '';
            declaredInfo = 'EST DECLARE : ' + '<span style="font-weight:bold;color:red">' + 'NON' + '</span>';
        } else {
            inputSelected = '';
            declaredInfo = 'EST DECLARE : ' + '<span style="font-weight:bold;color:green">' + 'OUI' + '</span>';
        }

        var categorieInfo = 'CATEGORIE : ' + '<span style="font-weight:bold">' + result[i].nameTarifBien + '</span>';
        var typeInfo = 'MODELE : ' + '<span style="font-weight:bold">' + result[i].modeleName + '</span>';
        var marqueInfo = 'MARQUE : ' + '<span style="font-weight:bold">' + result[i].marqueName + '</span>';
        var puissanceInfo = 'PUISSANCE FISCALE : ' + '<span style="font-weight:bold">' + result[i].puissanceFiscal + ' ' + result[i].nameUnitePuissance + '</span>';
        var anneeCirculationInfo = 'ANNEE DE MISE EN CIRCULATION : ' + '<span style="font-weight:bold">' + result[i].anneeCirculation + '</span>';
        var dateCirculationInfo = 'DATE DE MISE EN CIRCULATION : ' + '<span style="font-weight:bold">' + result[i].dateMiseCirculation1 + '</span>';

        var descriptionInfo = typeInfo + '<br/>' + marqueInfo + '<br/>' + categorieInfo + '<br/>' + puissanceInfo + '<br/>' + dateCirculationInfo + '<br/>' + anneeCirculationInfo;

        var denomination = 'DENOMINATION : ' + '<span style="font-weight:bold">' + result[i].denomination + '</span>';
        var adresseInfo = 'ADRESSE : ' + '<span style="font-weight:bold">' + result[i].nameAdresse + '</span>';

        var denominationInfo = denomination + '<br/><br/>' + adresseInfo + '<br/><br/>' + declaredInfo;

        tableContent += '<tr id="row_' + result[i].codeBien + '">';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + count + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].nameTypeBien + '</td>';
        tableContent += '<td style="text-align:left;width:40%;vertical-align:middle">' + denominationInfo + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + descriptionInfo + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].plaque + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].chassis + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold;font-size:18px">' + formatNumber(result[i].tauxVignette, result[i].devise) + '</td>';
        //tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + btnEditBienAutomobile + inputSelected + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="2" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL: </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableBienExonerationVignette.html(tableContent);

    tableBienExonerationVignette.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des biens automobile est vide",
            search: "Filtrer la liste des biens automobile ici",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"}
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false, ordering: false,
        pageLength: 50,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(2).footer()).html(totalBien);
        },
        columnDefs: [
            {"visible": false, "targets": 1}
        ],
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(1, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });
}

function getSelectedAssujetiData() {

    codeContribuableSelected = selectAssujettiData.code;
    fJContribuableSelected = selectAssujettiData.codeForme;
    nameContribuableSelected = selectAssujettiData.nomComplet;
    codeAdrPricipalContribuableSelected = selectAssujettiData.codeAdressePersonne;

    contribuableNameEdit.val(selectAssujettiData.nomComplet + ' (Adresse : ' + selectAssujettiData.adresse.toUpperCase() + ')');
    contribuableNameEdit.attr('style', 'font-weight:bold');

    inputExonerationAccorder.val('Exonération accordée : ' + selectAssujettiData.exonerationName);
    inputExonerationAccorder.attr('style', 'font-weight:bold');

    typeExo = selectAssujettiData.exonerationType;

    if (controlAccess('ACCORD_EXO_VIGNETTE')) {
        btnSaveExoneration.attr('disabled', false);
    }

    if (controlAccess('DELETE_EXO_VIGNETTE')) {

        if (typeExo !== '0') {
            btnDeleteExoneration.attr('disabled', false);
        } else {
            btnDeleteExoneration.attr('disabled', true);
        }

    }

    if (controlAccess('SET_TAXATION_EXO_VIGNETTE')) {

        if (typeExo !== '0') {
            btnTaxerExoneration.attr('disabled', false);
        } else {
            btnTaxerExoneration.attr('disabled', true);
        }

    }

    loadBienAutomobileByContribuable();

}

function saveAccordExonerationVignette() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveAccordExonerationVignette',
            'codePersonne': codeContribuableSelected,
            'typeExoneration': selectTypExoneration.val(),
            'typeExoCurrent': typeExo,
            'idUser': userData.idUser
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' || response == '0') {

                showResponseError();
                return;

            } else {

                alertify.alert('L\'Enregistrement de l\'exonération s\'est effectué avec succès.');
                typeExo = selectTypExoneration.val();

                //inputExonerationAccorder.val('EXONERATION ACCORDEE : ' + $('#selectTypExoneration option:selected').text());
                inputExonerationAccorder.val('Exonération accordée : ' + $('#selectTypExoneration option:selected').text());
                inputExonerationAccorder.attr('style', 'font-weight:bold');

                btnDeleteExoneration.attr('disabled', false);
                btnTaxerExoneration.attr('disabled', false);

            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadBienAutomobileByContribuable() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBienAutomobileByContribuable',
            'codePersonne': codeContribuableSelected
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des biens en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                showResponseError();
                return;

            } else if (response == '0') {

                printTableBienAutomobile('');

            } else {

                bienAutomobileList = $.parseJSON(JSON.stringify(response));
                printTableBienAutomobile(bienAutomobileList);
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadPeriodeDeclarationVignette() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBienAutomobileByContribuable',
            'codePersonne': codeContribuableSelected
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des biens en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                showResponseError();
                return;

            } else if (response == '0') {

                printTableBienAutomobile('');

            } else {

                bienAutomobileList = $.parseJSON(JSON.stringify(response));
                printTableBienAutomobile(bienAutomobileList);
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function taxationExonerationVignette() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'codePersonne': codeContribuableSelected,
            'listBiens': JSON.stringify(listObjBienTaxe),
            'userId': userData.idUser,
            'codeSite': userData.SiteCode,
            'typeExoneration': typeExo,
            'codeAB': AB_VIGNETTE,
            'requerant': nameContribuableSelected,
            'selectCompteBancaireExo': selectCompteBancaireExo.val(),
            'selectBanqueExo': selectBanqueExo.val(),
            'operation': 'taxationExonerationVignette'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Taxation exonération en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {
                    
                    var value = '<span style="font-weight:bold">' + nameContribuableSelected.toUpperCase() + '</span>';
                    alertify.alert('Il existe déjà une taxation Exoneration pour le contribuable : ' + value);
                    return;

                } else {

                    alertify.alert('La taxation de l\'exonération s\'est effectuée avec succès.');

                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');

                    loadBienAutomobileByContribuable();

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function loadingBankData() {


    var dataBank = empty;
    var defautlValue = '--';
    dataBank += '<option value="0">' + defautlValue + '</option>';

    var banqueUserList = JSON.parse(userData.banqueUserList);

    for (var i = 0; i < banqueUserList.length; i++) {

        dataBank += '<option value="' + banqueUserList[i].banqueCode + '" >' + banqueUserList[i].banqueName + '</option>';

    }

    selectBanqueExo.html(dataBank);
}

function loadingAccountBankData(abCode, bankCode, deviseCode) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadingAccountBankDataV2',
            'codeAb': abCode,
            'codeDevise': deviseCode,
            'codeBanque': bankCode
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();

                if (response == '-1') {

                    showResponseError();
                    return;

                } else {

                    var dataAccountBank = empty;
                    var defautlValue = '--';

                    dataAccountBank += '<option value="0">' + defautlValue + '</option>';

                    accountBankList = JSON.parse(JSON.stringify(response));

                    for (var i = 0; i < accountBankList.length; i++) {

                        dataAccountBank += '<option value="' + accountBankList[i].accountCode + '" >' + accountBankList[i].accountName + '</option>';

                    }

                    selectCompteBancaireExo.html(dataAccountBank);

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }
    });
}


