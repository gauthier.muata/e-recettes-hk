/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var cmbFormeJuridique;
var modalAdresses;
var
        divComplement,
        divPanelComplement;
var
        tableAdresses,
        tableAdressesPersonne;
var labelSelectedAdresse, lblNif, labelInfo;
var
        inputNIF,
        inputNom,
        inputPostNom,
        inputPrenom,
        inputLibelle,
        inputNumber,
        inputTelephone,
        inputEmail,
        checkboxTeleDeclaration;
var
        btnAjouterAdresse,
        btnRechercherAdresses,
        btnRechercherAdresses2,
        btnValiderAdresse,
        btnEnregistrer;
var adresse, personneCode;
var divNinfInfo;
var selectStateDeclaration;
var validNumber, initError = false;
var
        tempAdresseList = [],
        tempComplementList = [],
        suppressAdresseList = [];

var complementValue;
var complements = [];

var isSave;
var valueResponseCheckingEmail;
var valueResponseCheckingPhone;
var btnCallRegister;

var assujettiModal1, inputValueResearchAssujetti, btnLauchSearchAssujetti, spanModalSearchAssuj, tableResultSearchAssujeti, btnSelectAssujeti;
var idNameSeach, idNameSeach2;
var nameSelected;

var btnChangeTypePersonne;
var resultPersonne = {};
var codeFormeNew = '';

$(function () {

    isSave = true;
    codeFormeNew = '';

    valueResponseCheckingEmail = 0;
    valueResponseCheckingPhone = 0;

    mainNavigationLabel.text('REPERTOIRE');
    secondNavigationLabel.text('Identification d\'un contribuable');
    removeActiveMenu();
    linkMenuRepertoire.addClass('active');
    inputNIF = $('#inputNIF');
    inputNom = $('#inputNom');
    inputPostNom = $('#inputPostNom');
    inputPrenom = $('#inputPrenom');
    inputLibelle = $('#inputLibelle');
    inputNumber = $('#inputNumber');
    inputTelephone = $('#inputTelephone');
    inputEmail = $('#inputEmail');
    checkboxTeleDeclaration = $('#checkboxTeleDeclaration');
    lblNif = $('#lblNif');
    labelInfo = $('#labelInfo');
    labelInfo.html('');
    divComplement = $('#divComplement');
    divPanelComplement = $('#divPanelComplement');
    divNinfInfo = $('#divNinfInfo');
    modalAdresses = $('#modalAdresses');
    tableAdresses = $('#tableAdresses');
    tableAdressesPersonne = $('#tableAdressesPersonne');
    labelSelectedAdresse = $('#labelSelectedAdresse');
    cmbFormeJuridique = $('#cmbFormeJuridique');
    btnAjouterAdresse = $('#btnAjouterAdresse');
    btnRechercherAdresses = $('#btnRechercherAdresses');
    btnRechercherAdresses2 = $('#btnRechercherAdresses2');
    btnValiderAdresse = $('#btnValiderAdresse');
    btnEnregistrer = $('#btnEnregistrer');
    btnCallRegister = $('#btnCallRegister');

    assujettiModal1 = $('#assujettiModal1');
    inputValueResearchAssujetti = $('#inputValueResearchAssujetti');
    btnLauchSearchAssujetti = $('#btnLauchSearchAssujetti');
    spanModalSearchAssuj = $('#spanModalSearchAssuj');
    tableResultSearchAssujeti = $('#tableResultSearchAssujeti');
    btnSelectAssujeti = $('#btnSelectAssujeti');
    idNameSeach = $('#idNameSeach');
    idNameSeach2 = $('#idNameSeach2');
    btnChangeTypePersonne = $('#btnChangeTypePersonne');

    personneCode = '';
    loadFormeJuridique();

    var input = document.querySelector('#inputTelephone');
    var iti = intlTelInput(input, {
        initialCountry: "cd",
        utilsScript: "assets/intl-tel-input/build/js/utils.js"
    });

    inputTelephone.on('blur', function (event) {

        var num = iti.getNumber().substring(1);
        validNumber = iti.isValidNumber();

        if (!validNumber) {
            inputTelephone.parents('.form-group').addClass('has-error');
            if (!initError) {
                var error = "<div class='error-message error-text'>Ce numéro de téléphone est invalide.</div>";
                inputTelephone.parents('.form-group .col-sm-8').append(error);
                initError = true;
            }

            btnEnregistrer.attr('disabled', 'true').css('cursor', 'not-allowed');

        } else {
            inputTelephone.parents('.form-group').removeClass('has-error');
            btnEnregistrer.removeAttr('disabled').css('cursor', 'pointer');

        }
        console.log(num);
        $(this).val(num);
    })

    var urlCode = getUrlParameter('id');

    if (jQuery.type(urlCode) !== 'undefined') {
        personneCode = atob(urlCode);
        isSave = false;
        //getInfosPersonne(urlCode);
        getInfosPersonne(personneCode);
    }

    cmbFormeJuridique.on('change', function (e) {
        var codeFormeJuridique = cmbFormeJuridique.val();
        if (codeFormeJuridique !== '') {
            switch (codeFormeJuridique) {
                case '04':
                case '08':
                case '09':
                    divNinfInfo.attr('style', 'display : none');
                    break;
                default:
                    divNinfInfo.attr('style', 'display : block');
                    break;
            }
        }
        loadComplementFormes(codeFormeJuridique);
        loadComplements(codeFormeJuridique);
    });

    btnChangeTypePersonne.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var nameContribuable = '<span style="font-weight:bold">' + resultPersonne.nom.toUpperCase() + '</span>';

        alertify.confirm('Voulez-vous changer la forme juridique du contribuable : ' + nameContribuable + ' ?', function () {
           changeFormeJuridique();
        });

    });

    btnAjouterAdresse.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }


        $('#inputLibelle').val('');
        inputNumber.attr('style', 'visibility: hidden');
        btnValiderAdresse.attr('style', 'visibility: hidden');
        printAdresse('');
        $('#labelSelectedAdresse').text('');
        $('#inputNumber').val('');
        $('#modalAdresses').modal('show');
    });

    btnRechercherAdresses2.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadAdresses();
    });

    btnCallRegister.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Voulez-vous revenir au répertoire des contribuables ?', function () {
            window.location = 'repertoire-assujetti';
        });
    });

    inputLibelle.keypress(function (e) {
        if (e.keyCode === 13) {
            btnRechercherAdresses2.trigger('click');
        }
    });

    btnValiderAdresse.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        validateAdresse();
    });

    btnEnregistrer.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        checkFields();
    });

    inputNIF.focusout(function () {

        if (inputNIF.val() !== empty) {

            if (isSave) {

                switch (checkNifValue(inputNIF.val().trim())) {

                    case '-1':

                        showResponseError();

                        break;

                    case '1':

                        alertify.alert('Désoler, ce NIF : ' + inputNIF.val() + ' est déjà lié à un autre assujetti');

                        break;
                }
            }

        }
    });

    inputTelephone.focusout(function () {

        if (inputTelephone.val() !== empty) {

            if (isSave) {
                checkMailOrPhoneValue(inputTelephone.val().trim(), 2);
            }
        }
    });

    inputEmail.focusout(function () {

        if (inputEmail.val() !== empty) {

            if (isValidEmailAddress(inputEmail.val()) === false) {

                alertify.alert('Veuillez d\'abord saisir une adresse mail valide pour cet assujetti');
                return;

            } else {

                if (isSave) {
                    checkMailOrPhoneValue(inputEmail.val().trim(), 1);
                }
            }

        }
    });

    inputNom.on('change', function (e) {

        if (inputNom.val() !== empty) {
            loadPersonnesLikeCurrentName();
        }
    });
});

function loadPersonnesLikeCurrentName() {

    nameSelected = inputNom.val().toUpperCase();
    idNameSeach2.html(inputNom.val().toUpperCase());

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'nameLike': nameSelected,
            'codeForme': cmbFormeJuridique.val(),
            'operation': 'loadPersonnesLikeCurrentName'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'})
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                if (result.length > 0) {

                    var valueName = '<span style="font-weight:bold">' + nameSelected + '</span>';
                    idNameSeach.html(valueName);

                    printAssujettiTable(result);
                    assujettiModal1.modal('show');
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadFormeJuridique() {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadFormeJuridique'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'})
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                var result = $.parseJSON(JSON.stringify(response));
                var dataFormeJuridique = '<option value ="0">-- Sélectionner --</option>';
                for (var i = 0; i < result.length; i++) {

                    dataFormeJuridique += '<option value =' + result[i].codeFormeJuridique + '>' + result[i].libelleFormeJuridique + '</option>';
                }

                cmbFormeJuridique.html(dataFormeJuridique);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadComplementFormes(codeFormeJuridique) {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadComplementForme',
            'codeFormeJuridique': codeFormeJuridique
        },
        beforeSend: function () {

            divPanelComplement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                divPanelComplement.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                divPanelComplement.unblock();
                var result = $.parseJSON(JSON.stringify(response));
                var dataComplementForme = '';
                for (var i = 0; i < result.length; i++) {

                    dataComplementForme += result[i].inputValue;
                }

                divComplement.html(dataComplementForme);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            divPanelComplement.unblock();
            showResponseError();
        }

    });
}

function loadComplements(codeFormeJuridique) {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadComplementByFormeJuridique',
            'codeFormeJuridique': codeFormeJuridique
        },
        beforeSend: function () {

            divPanelComplement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                divPanelComplement.unblock();
                showResponseError();
                return;
            }

            divPanelComplement.unblock();
            complements = $.parseJSON(JSON.stringify(response));


        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            divPanelComplement.unblock();
            showResponseError();
        }

    });
}

function loadAdresses() {

    if (inputLibelle.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir une adresse.');
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAdresses',
            'libelle': inputLibelle.val(),
        },
        beforeSend: function () {

            modalAdresses.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                modalAdresses.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalAdresses.unblock();
                var adresseList = $.parseJSON(JSON.stringify(response));
                if (adresseList.length > 0) {
                    inputNumber.attr('style', 'visibility: visible');
                    btnValiderAdresse.attr('style', 'visibility: visible');
                    printAdresse(adresseList);
                } else {
                    inputNumber.attr('style', 'visibility: hidden');
                    btnValiderAdresse.attr('style', 'visibility: hidden');
                    printAdresse('');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalAdresses.unblock();
            showResponseError();
        }

    });
}

function printAdresse(adresseList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> PROVINCE </th>';
    header += '<th scope="col"> VILLE </th>';
//    header += '<th scope="col"> District </th>';
    header += '<th scope="col"> COMMUNE </th>';
    header += '<th scope="col"> QUARTIER </th>';
    header += '<th scope="col"> AVENUE </th>';
    header += '<th hidden="true" scope="col"> Code avenue </th>';
    header += '</tr></thead>';
    var body = '<tbody id="tbodyAdresses">';
    for (var i = 0; i < adresseList.length; i++) {
        if (adresseList[i].province == undefined || adresseList[i].province == 'undefined'
                || adresseList[i].province == null || adresseList[i].province == 'null') {
            continue;
        }
        body += '<tr>';
        body += '<td>' + adresseList[i].province.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].ville.toUpperCase() + '</td>';
//        body += '<td>' + adresseList[i].district.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].commune.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].quartier.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].avenue.toUpperCase() + '</td>';
        body += '<td hidden="true">' + adresseList[i].codeAvenue + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableAdresses.html(tableContent);
    var dtAdresse = tableAdresses.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    $('#tableAdresses tbody').on('click', 'tr', function () {
        var data = dtAdresse.row(this).data();
        adresse = new Object();
        adresse.code = data[5];
        adresse.chaine = data[0].toUpperCase() + '--> V/' + data[1].toUpperCase() + ' - C/' + data[2].toUpperCase() + ' - Q/' + data[3].toUpperCase() + ' - Av. ' + data[4].toUpperCase();
        adresse.numero = '';
        adresse.defaut = '';
        adresse.codeAP = '';
        adresse.etat = '1';
        labelSelectedAdresse.html(adresse.chaine);
    });
}

function validateAdresse() {

    if (labelSelectedAdresse.text().trim() === '') {
        alertify.alert('Veuillez d\'abord sélectionner une adresse.');
        return;
    }

    if (inputNumber.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir un numéro de l\'adresse en cours de sélectionner.');
        return;
    }

    var numero = inputNumber.val();
    var exist = ifAdressExists(adresse.code, numero);
    if (exist === true) {
        alertify.alert('Cette adresse a déjà été ajoutée dans la liste des adresses.');
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir confirmer cette adresse ?', function () {
        labelInfo.html('L\'adresse par défaut est mise en surbrillance');
        adresse.numero = numero;
        if (tempAdresseList.length === 0) {
            adresse.defaut = 'Oui';
        } else {
            adresse.defaut = '';
        }
        tempAdresseList.push(adresse);
        printAdressePersonne(tempAdresseList);
        $('#labelSelectedAdresse').text('');
        $('#inputNumber').val('');
    });
}

function printAdressePersonne(tempAdresseList) {

    var header = '<thead><tr>';
    header += '<th scope="col"> ADRESSES ASSUJETTI </th>';
    header += '<th hidden="true" scope="col"> Principale </th>';
    header += '<th width="82" scope="col"></th>';
    header += '<th hidden="true" scope="col"> Code </th>';
    header += '<th hidden="true" scope="col"> Numero </th>';
    header += '<th hidden="true" scope="col"> ID Adresse </th>';
    header += '</tr></thead>';
    var body = '<tbody id="tbodyAdressesPersonne">';
    for (var i = 0; i < tempAdresseList.length; i++) {

        var numero = ' n° ' + tempAdresseList[i].numero;
        if (tempAdresseList[i].codeAP !== '') {
            numero = '';
        }

        var style = '', hidden = '';
        if (tempAdresseList[i].defaut === 'Oui') {
            style = 'style="background-color: #c1e2b3;color:black"';
            hidden = 'style="visibility: hidden"';
        }

        body += '<tr ' + style + '>';
        body += '<td>' + tempAdresseList[i].chaine.toUpperCase() + numero + '</td>';
        body += '<td hidden="true">' + tempAdresseList[i].defaut + '</td>';
        body += '<td><a ' + hidden + 'style="margin-right:3px" onclick="setDefaultAdresse(\'' + tempAdresseList[i].code + '\',\'' + tempAdresseList[i].numero + '\')" class="btn btn-success"><i class="fa fa-check"></i></a><a onclick="removeAdresse(\'' + tempAdresseList[i].code + '\',\'' + tempAdresseList[i].numero + '\')" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>';
        body += '<td hidden="true">' + tempAdresseList[i].code + '</td>';
        body += '<td hidden="true">' + tempAdresseList[i].numero + '</td>';
        body += '<td hidden="true">' + tempAdresseList[i].idAdresse + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableAdressesPersonne.html(tableContent);
    tableAdressesPersonne.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}

function ifAdressExists(code, numero) {

    for (var i = 0; i < tempAdresseList.length; i++) {
        if (tempAdresseList[i].code === code && tempAdresseList[i].numero === numero) {
            return true;
        }
    }
    return false;
}

function ifDefaultAdressExists() {

    for (var i = 0; i < tempAdresseList.length; i++) {
        if (tempAdresseList[i].defaut === 'Oui') {
            return true;
        }
    }
    return false;
}

function setDefaultAdresse(code, numero) {

    var adresseFormatted = getAdresseFormatted(code, numero);
    alertify.confirm('Etes-vous sûre de vouloir définir cette adresse : <br/>' + adresseFormatted + ' comme adresse principale ?', function () {
        for (var i = 0; i < tempAdresseList.length; i++) {
            if (tempAdresseList[i].code === code && tempAdresseList[i].numero === numero) {
                tempAdresseList[i].defaut = 'Oui';
            } else {
                tempAdresseList[i].defaut = '';
            }
        }
        printAdressePersonne(tempAdresseList);
    });
}

function removeAdresse(code, numero) {

    for (var i = 0; i < tempAdresseList.length; i++) {
        if (tempAdresseList[i].code === code && tempAdresseList[i].numero === numero) {

            alertify.confirm('Etes-vous sûre de vouloir rétirer cette adresse : <br/>' + tempAdresseList[i].chaine + ' n° ' + tempAdresseList[i].numero + ' ?', function () {

                if (tempAdresseList[i].codeAP !== '') {
                    adresse = new Object();
                    adresse.code = tempAdresseList[i].code;
                    adresse.chaine = tempAdresseList[i].chaine;
                    adresse.numero = tempAdresseList[i].numero;
                    adresse.defaut = tempAdresseList[i].defaut;
                    adresse.codeAP = tempAdresseList[i].codeAP;
                    adresse.etat = '';
                    suppressAdresseList.push(adresse);
                }

                tempAdresseList.splice(i, 1);
                printAdressePersonne(tempAdresseList);
                if (tempAdresseList.length == 0) {
                    tableAdressesPersonne.remove();
                }
            });
            break;
        }
    }

}

function getAdresseFormatted(code, numero) {

    var adresseFormatted = '';
    for (var i = 0; i < tempAdresseList.length; i++) {
        if (tempAdresseList[i].code === code && tempAdresseList[i].numero === numero) {

            if (tempAdresseList[i].codeAP === '') {
                adresseFormatted = tempAdresseList[i].chaine + ' n° ' + tempAdresseList[i].numero;
            } else {
                adresseFormatted = tempAdresseList[i].chaine;
            }

        }
    }
    return adresseFormatted;
}

function checkFields() {

    if (cmbFormeJuridique.val() === '0') {
        alertify.alert('Veuillez d\'abord sélectionner le type du contribuable');
        return;
    }

    if (inputNom.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir le nom ou la raison sociale du contribuable');
        return;
    }

    if (tempAdresseList.length === 0) {
        alertify.alert('Veuillez sélectionner au moins une adresse.');
        return;
    } else {
        var exist = ifDefaultAdressExists();
        if (exist === false) {
            alertify.alert('Veuillez spécifier l\'adresse par principale du contribuable');
            return;
        }
    }

    if (valueResponseCheckingEmail == 1) {

        alertify.alert('Désoler, cette adresse : ' + inputEmail.val() + ' est déjà liée à un autre contribuable');
        return;

    }

    if (valueResponseCheckingPhone == 1) {

        alertify.alert('Désoler, ce téléphone : ' + inputTelephone.val() + ' est déjà lié à un autre contribuable');
        return;
    }

    selectStateDeclaration = 1;

    var runSave = true;

    if ($("#divComplement") != null && $("#divComplement").length > 0) {
        var count = 0;
        tempComplementList = [];
        $("#divComplement").children().each(function () {
            var id = "#IDENT_" + count;
            var value = $(this).find(id).val();
            var name = $(this).find(id).attr("name");
            var placeholder = $(this).find(id).attr("placeholder");
            var splitName = name.split('-');
            var id = splitName[0];
            var code = splitName[1];
            var requiered = splitName[2];

            if (requiered === '1') {

                if (value.trim() === '' || value === '0') {
                    alertify.alert(placeholder + ' est obligatoire.');
                    return;

                } else {

                    if (complements.length > 0) {

                        if (cmbFormeJuridique.val() == '03') {

                            switch (placeholder) {

                                case 'RCCM':

                                    for (var i = 0; i < complements.length; i++) {

                                        if (complements[i].value == value.trim()) {
                                            runSave = false;
                                            alertify.alert('Désoler, ce RCCM : ' + value.trim() + ' est déjà utilisé par une autre entreprise');
                                            return;
                                        } else {
                                            runSave = true;
                                        }
                                    }

                                    break;

                                case 'ID.NAT':

                                    for (var j = 0; j < complements.length; j++) {

                                        if (complements[j].value == value.trim()) {
                                            runSave = false;
                                            alertify.alert('Désoler, cet ID. NAT : ' + value.trim() + ' est déjà utilisé par une autre entreprise');
                                            return;
                                        } else {
                                            runSave = true;
                                        }
                                    }

                                    break;
                            }
                        }
                    }
                }
            }
            count++;
            var complement = new Object();
            complement.id = id;
            complement.code = code;
            complement.valeur = value;
            tempComplementList.push(complement);
        });
    }

    if (runSave) {
        alertify.confirm('Etes-vous sûre de vouloir enregistrer ce contribuable ?', function () {
            saveAssujetti();
        });
    }


}

function checkNifValue(nif) {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'checkNifValue',
            'value': nif,
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification du NIF en cours ...</h5>'})

        },
        success: function (response)
        {
            $.unblockUI();
            return response;
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function checkMailOrPhoneValue(mailOrPhone, typeSearch) {
    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'checkMailOrPhoneValue',
            'value': mailOrPhone,
            'type': typeSearch,
        },
        beforeSend: function () {

            switch (typeSearch) {
                case 1:
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification de l\'adresse mail en cours ...</h5>'})
                    break;
                case 2:
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification du numéro de téléphone en cours ...</h5>'})
                    break;
            }
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;

            } else if (response == '1') {

                if (typeSearch == 1) {

                    valueResponseCheckingEmail = 1;

                    alertify.alert('Désoler, cette adresse : ' + inputEmail.val() + ' est déjà liée à un autre contribuable');
                    return;

                } else {

                    valueResponseCheckingPhone = 1;

                    alertify.alert('Désoler, ce téléphone : ' + inputTelephone.val() + ' est déjà lié à un autre contribuable');
                    return;
                }

            } else {

                if (typeSearch == 1) {

                    valueResponseCheckingEmail = 0;

                } else {

                    valueResponseCheckingPhone = 0;

                }
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}


function saveAssujetti() {

    completeListAdresse();
    var adresses = JSON.stringify(tempAdresseList);
    var complements = JSON.stringify(tempComplementList);

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveAssujetti',
            'codePersonne': personneCode,
            'codeFormeJuridique': cmbFormeJuridique.val(),
            'nif': inputNIF.val(),
            'nom': inputNom.val(),
            'postNom': inputPostNom.val(),
            'prenom': inputPrenom.val(),
            'telephone': inputTelephone.val(),
            'email': inputEmail.val().trim(),
            'idUser': userData.idUser,
            'adresses': adresses,
            'selectStateDeclaration': selectStateDeclaration,
            'complements': complements
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du contribuable...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                if (response == '1') {
                    alertify.alert('L\'enregistrement du contribuable s\'est effectué avec succès.');
                    if (personneCode === '') {
                        resetFields();
                    }

                } else if (response == '5') {
                    alertify.alert('Ce numéro de téléphone existe.');
                } else if (response == '6') {
                    alertify.alert('Cette adresse mail existe.');
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement du contribuable');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function resetFields() {

    cmbFormeJuridique.val(0);
    inputNIF.val(empty);
    inputNom.val(empty);
    inputPostNom.val(empty);
    inputPrenom.val(empty);
    inputTelephone.val(empty);
    inputEmail.val(empty);
    tableAdressesPersonne.remove();
    divComplement.html(empty);
    tempAdresseList = [];
    tempComplementList = [];
    suppressAdresseList = [];
}

function getInfosPersonne(codePersonne) {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadInfoAssujetti',
            'codePersonne': codePersonne
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                btnChangeTypePersonne.attr('style', 'display:none');
                showResponseError();
                return;
            }

            setTimeout(function () {

                //var result = JSON.parse(JSON.stringify(response));
                resultPersonne = JSON.parse(JSON.stringify(response));
                cmbFormeJuridique.val(resultPersonne.codeFormeJuridique);
                cmbFormeJuridique.attr('disabled', 'true');
                inputNIF.val(resultPersonne.nif);
                inputNom.val(resultPersonne.nom);
                inputPostNom.val(resultPersonne.postNom);
                inputPrenom.val(resultPersonne.prenom);
                inputTelephone.val(resultPersonne.telephone);
                inputEmail.val(resultPersonne.email);

                if (controlAccess('CHANGE_TYPE_PERSONNE')) {
                    btnChangeTypePersonne.attr('style', 'display:block;float: right; margin-right: 5px');
                } else {
                    btnChangeTypePersonne.attr('style', 'display:none');
                }

                if (resultPersonne.teleDelarationIsExiste) {

                    checkboxTeleDeclaration.attr('checked', "true");

                } else {
                    checkboxTeleDeclaration.removeAttr('checked');
                }

                tempAdresseList = [];
                
                var adressesPersonneList = $.parseJSON(resultPersonne.adresses);
                
                for (var i = 0; i < adressesPersonneList.length; i++) {
                    adresse = new Object();
                    adresse.code = adressesPersonneList[i].code;
                    adresse.chaine = adressesPersonneList[i].chaine;
                    adresse.numero = adressesPersonneList[i].numero;
                    adresse.defaut = adressesPersonneList[i].defaut;
                    adresse.codeAP = adressesPersonneList[i].codeAP;
                    adresse.etat = '1';
                    tempAdresseList.push(adresse);
                }

                printAdressePersonne(tempAdresseList);

                var complementsPersonneList = $.parseJSON(resultPersonne.complements);
                var dataComplementForme = '';
                for (var i = 0; i < complementsPersonneList.length; i++) {
                    dataComplementForme += complementsPersonneList[i].inputValue;
                }
                divComplement.html(dataComplementForme);
                $.unblockUI();
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            btnChangeTypePersonne.attr('style', 'display:none');
            showResponseError();
        }

    });
}

function completeListAdresse() {

    if (suppressAdresseList.length > 0) {

        for (var i = 0; i < suppressAdresseList.length; i++) {

            adresse = new Object();
            adresse.code = suppressAdresseList[i].code;
            adresse.chaine = suppressAdresseList[i].chaine;
            adresse.numero = suppressAdresseList[i].numero;
            adresse.defaut = suppressAdresseList[i].defaut;
            adresse.codeAP = suppressAdresseList[i].codeAP;
            adresse.etat = suppressAdresseList[i].etat;
            tempAdresseList.push(adresse);
        }
    }

}

function printAssujettiTable(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">NTD</th>';
    tableContent += '<th scope="col">ASSUJETTI</th>';
    tableContent += '<th scope="col">TYPE</th>';
    tableContent += '<th scope="col">ADRESSE</th>';
    tableContent += '<th hidden="true" scope="col">Code assujetti</th>';
    tableContent += '<th hidden="true" scope="col">Code forme</th>';
    tableContent += '<th hidden="true" scope="col">Code adresse</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td>' + data[i].codeNTD + '</td>';
        tableContent += '<td>' + data[i].nomCompletAssujetti + '</td>';
        tableContent += '<td>' + data[i].libelleFormeJuridique + '</td>';
        tableContent += '<td>' + data[i].adresseAssujetti + '</td>';
        tableContent += '<td hidden="true">' + data[i].code + '</td>';
        tableContent += '<td hidden="true">' + data[i].codeFormeJuridique + '</td>';
        tableContent += '<td hidden="true">' + data[i].adresseId + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultSearchAssujeti.html(tableContent);

    var myDt = tableResultSearchAssujeti.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste ici ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 50,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableResultSearchAssujeti tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        selectAssujettiData = new Object();
        selectAssujettiData.code = myDt.row(this).data()[4];
        selectAssujettiData.codeAdresse = myDt.row(this).data()[6];
        selectAssujettiData.codeForme = myDt.row(this).data()[5];
        selectAssujettiData.nif = myDt.row(this).data()[0];
        selectAssujettiData.nomComplet = myDt.row(this).data()[1].toUpperCase();
        selectAssujettiData.categorie = myDt.row(this).data()[2].toUpperCase();
        selectAssujettiData.adresse = myDt.row(this).data()[3].toUpperCase();

        //alert(selectAssujettiData.nif);

    });
}

function changeFormeJuridique() {

    codeFormeNew = resultPersonne.codeFormeJuridique == '03' ? '04' : '03';

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'changeFormeJuridique',
            'codePersonne': personneCode,
            'codeForme': codeFormeNew
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Changement en cours ...</h5>'})
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' || response == '0') {

                showResponseError();
                return;

            } else {

                alertify.alert('Le changement de la forme juridique du contribuable en cours s\'est effectué avec succès.');
                getInfosPersonne(personneCode);
                //window.location = 'identification?id=' + btoa(personneCode);
            }


        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}
