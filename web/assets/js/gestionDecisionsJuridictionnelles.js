/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var lblNameResponsible, lblAddress, lblNifResponsible, lblLegalForm;
var lbl1, lbl2, lbl3, lbl4;

var typeResearchRecours;

var btnSearchRecours, modalRecoursAdvancedSearch, selectTypeRecours;

var btnCallModalSearchResponsable, btnEnregistrerRecours, btnJoindreArchiveRecours;
var btnSearchAndAddDocument, btnAdvancedSearch;

var inputNumeroDocument, inputResearchRecours, btnCallModalSearchAvandedRecours;
var inputdateLast, inputDateDebut;
var datePickerDebut, datePickerFin;

var tableRecours;

var assujettiModal;
var codeResponsible = ''
var codeFormeJuridique = '';
var adresseId = '';

var isAdvanceJuridictionnelle;

var lblDateDebut, lblDateFin;

var tableRegistreDecisionJudictionnelle;

var typeRecours = '*';

var tempRecoursLists = [];
var tempDetailRecoursLists = [];

$(function () {

    mainNavigationLabel.text('CONTENTIEUX');
    secondNavigationLabel.text('Registre des recours juridictionnelles');

    removeActiveMenu();
    linkMenuContentieux.addClass('active');

    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    lblNifResponsible = $('#lblNifResponsible');
    lblLegalForm = $('#lblLegalForm');

    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');
    lbl3 = $('#lbl3');
    lbl4 = $('#lbl4');

    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    modalRecoursAdvancedSearch = $('#modalRecoursAdvancedSearch');

    btnCallModalSearchAvandedRecours = $('#btnCallModalSearchAvandedRecours');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');
    btnEnregistrerRecours = $('#btnEnregistrerRecours');
    btnJoindreArchiveRecours = $('#btnJoindreArchiveRecours');
    btnSearchAndAddDocument = $('#btnSearchAndAddDocument');

    btnSearchRecours = $('#btnSearchRecours');
    btnAdvancedSearch = $('#btnAdvancedSearch');

    inputNumeroDocument = $('#inputNumeroDocument');
    inputResearchRecours = $('#inputResearchRecours');

    inputdateLast = $('#inputdateLast');
    inputDateDebut = $('#inputDateDebut');

    typeResearchRecours = $('#typeResearchRecours');
    selectTypeRecours = $('#selectTypeRecours');

    isAdvanceJuridictionnelle = $('#isAdvanceJuridictionnelle');

    datePickerDebut = $('#datePickerDebut');
    datePickerFin = $('#datePickerFin');

    assujettiModal = $('#assujettiModal');

    tableRecours = $('#tableRecours');
    tableRegistreDecisionJudictionnelle = $('#tableRegistreDecisionJudictionnelle');

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerDebut.datepicker("setDate", new Date());
    datePickerFin.datepicker("setDate", new Date());


    $('.date').datepicker({
        multidate: true
    });

   

    btnSearchRecours.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var critere = typeResearchRecours.val();

        if (critere == '1') {
            assujettiModal.modal('show');
        } else {
            isAdvancedSearch = '0';
            isAdvanceJuridictionnelle.attr('style', 'display: none');
            loadRecours();
        }
    });

    btnCallModalSearchAvandedRecours.on('click', function (e) {
        e.preventDefault();
        modalRecoursAdvancedSearch.modal('show');
    });

    selectTypeRecours.on('change', function () {

        typeRecours = selectTypeRecours.val();

    });

    btnAdvancedSearch.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAdvancedSearch = '1';
        codeAssujetti = '';

        if (inputDateDebut.val() == '' || inputdateLast.val() == '') {
            alertify.alert('Veuillez d\'abord sélectionner la date du début et celle de la fin.');
            return;
        } else {
            loadRecours();
            inputdateLast.val('');
            inputDateDebut.val('');
            modalRecoursAdvancedSearch.modal('hide');           
        }

    });

    typeResearchRecours.on('change', function () {

        inputResearchRecours.val('');

        tempRecoursLists.length = 0;

        printRecours(tempRecoursLists);

        typeSearchDocument = typeResearchRecours.val();
        if (typeSearchDocument == '1') {
            inputResearchRecours.attr('placeholder', 'Assujetti');
            inputResearchRecours.attr('disabled', 'true');
            assujettiModal.modal('show');
        } else if (typeSearchDocument == '2') {
            inputResearchRecours.attr('placeholder', 'Référence du courrier');
            inputResearchRecours.removeAttr('disabled');
        } else if (typeSearchDocument == '3') {
            inputResearchRecours.attr('placeholder', 'Titre de Réclamation');
            inputResearchRecours.removeAttr('disabled');
        }
    });



    printRecours('');
});

function getSelectedAssujetiData() {
    isAdvancedSearch = '0';
    typeRecours = '1';

    isAdvanceJuridictionnelle.attr('style', 'display: none');

    inputResearchRecours.val(selectAssujettiData.nomComplet);
    codeAssujetti = selectAssujettiData.code;

    assujettiModal.modal('hide');

    loadRecours();
}

function resetData() {
    codeAssujetti = '';

    inputResearchRecours.val('');

    inputDateDebut.val('');
    inputdateLast.val('');

    loadRecours('');
}

function loadRecours() {

    var dateDebut;
    var dateLast;

    var valueSearch = inputResearchRecours.val().trim();

    if (typeResearchRecours.val() === '1') {
        valueSearch = codeAssujetti;
    }

    if (isAdvancedSearch == '1') {
        dateDebut = inputDateDebut.val();
        dateLast = inputdateLast.val();
    } else {
        dateDebut = '';
        dateLast = '';
    }


    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadRecoursJuridictionnel',
            'valueSearch': valueSearch,
            'typeSearch': typeResearchRecours.val(),
            'dateDebut': dateDebut,
            'dateFin': dateLast,
            'isAdvancedSearch': isAdvancedSearch,
            'typeReclamation': typeRecours,
            'TypePage': '0',
            'idUser': userData.idUser
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                printRecours('');
                return;
            }

            setTimeout(function () {
                var allRecoursList = JSON.parse(JSON.stringify(response));
                printRecours(allRecoursList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });

}

function printRecours(recoursList) {

    tempRecoursLists = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col">TYPE RECOURS</th>';
    header += '<th scope="col">NUMERO COURRIER</th>';
    header += '<th scope="col">ASSUJETTI</th>';
    header += '<th scope="col">TYPE ASSUJETTI</th>';
    header += '<th scope="col">DATE CREATION</th>';
    header += '<th scope="col">DATE DEPOT RECOURS</th>';
    header += '<th scope="col">DATE AUDIENCE</th>';
    header += '<th scope="col">ETAT</th>';
//    header += '<th></th>';
    header += '</tr></thead>';

    var body = '<tbody >';

    for (var i = 0; i < recoursList.length; i++) {

        var type, etat;
        var action;

        type = 'recouvrement';

//        action = '<a onclick="openDetailReclamation(\'' + recoursList[i].reclamationId + '\',\'' + recoursList[i].etat + '\',\'' + recoursList[i].documentPayer + '\')" class="btn btn-warning"><i class="fa fa-list"></i></a>';

        switch (recoursList[i].etat) {
            case 3:
                etat = 'REJETE';
                break;
            case 1:
                etat = 'EN ATTENTE DE TRAITEMENT';
                break;
            case 2:
                etat = 'TRAITE';
                break;
        }

        var recours = {};
        recours.reclamationId = recoursList[i].reclamationId;
        recours.assujettiCode = recoursList[i].assujettiCode;
        recours.assujettiName = recoursList[i].assujettiName;
        recours.legalFormName = recoursList[i].legalFormName;
        recours.adresseName = recoursList[i].adresseName;
        recours.dateCreate = recoursList[i].dateCreate;
        recours.dateReceptionCourrier = recoursList[i].dateReceptionCourrier;
        recours.etat = recoursList[i].etat;
        recours.numeroEnregistrementGreffe = recoursList[i].numeroEnregistrementGreffe;
        recours.dateAudience = recoursList[i].dateAudience;
        recours.dateDepotRecours = recoursList[i].dateDepotRecours;
        assujettiCode = recoursList[i].assujettiCode;

        tempRecoursLists.push(recours);

        body += '<tr>';
        body += '<td style="text-align:left;width:12%;vertical-align:middle;">' + type.toUpperCase() + '</td>';
        body += '<td style="text-align:left;width:10%;vertical-align:middle;">' + recoursList[i].referenceCourrierReclamation + '</td>';
        body += '<td style="text-align:left;width:25%;vertical-align:middle" title="' + recoursList[i].assujettiName + '"><span style="font-weight:bold;">' + recoursList[i].assujettiName + '</span><br/><br/>' + recoursList[i].adresseName + '</td>';
        body += '<td style="text-align:left;width:12%;vertical-align:middle;">' + recoursList[i].legalFormName + '</td>';
        body += '<td style="text-align:left;width:10%;vertical-align:middle;">' + recoursList[i].dateCreate + '</td>';
        body += '<td style="text-align:center;width:12%;vertical-align:middle;">' + recoursList[i].dateDepotRecours + '</td>';
        body += '<td style="text-align:center;width:12%;vertical-align:middle;">' + recoursList[i].dateAudience + '</td>';
        body += '<td style="text-align:left;width:10%;vertical-align:middle;">' + etat + '</td>';
//        body += '<td style="text-align:center;width:7%;vertical-align:middle;"><center>' + action + '</center></td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableRegistreDecisionJudictionnelle.html(tableContent);
    tableRegistreDecisionJudictionnelle.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}
