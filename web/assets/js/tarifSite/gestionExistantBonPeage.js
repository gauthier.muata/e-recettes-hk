

var societeNameEdit, btnSearchSociete, selectSiteEdit, selectCategorieEdit, tarifEdit, nombreBonRemis,
        montantTotal, selectBanque, selectCompteBancaire, btnSave, tableData;

var codePersonneSelected;
var codeSiteSelected, deviseSelected;
var tauxNormal, tauxRemise;
var spnDevise, spnDevise2;
var montantTot;
var datePaiement;
var selectModePaiement;

$(function () {

    mainNavigationLabel.text('PEAGE');
    secondNavigationLabel.text('Gestion de prise en compte de bons de pré-paiement péage');

    removeActiveMenu();
    linkMenuOrdonnancement.addClass('active');

    societeNameEdit = $('#societeNameEdit');
    btnSearchSociete = $('#btnSearchSociete');
    selectSiteEdit = $('#selectSiteEdit');
    selectCategorieEdit = $('#selectCategorieEdit');
    tarifEdit = $('#tarifEdit');
    nombreBonRemis = $('#nombreBonRemis');
    montantTotal = $('#montantTotal');
    selectBanque = $('#selectBanque');
    selectCompteBancaire = $('#selectCompteBancaire');
    btnSave = $('#btnSave');
    tableData = $('#tableData');
    spnDevise = $('#spnDevise');
    spnDevise2 = $('#spnDevise2');
    datePaiement = $('#datePaiement');
    selectModePaiement = $('#selectModePaiement');

    tauxNormal = 0;
    montantTot = 0;
    tauxRemise = 0;

    nombreBonRemis.on('change', function (e) {

        if (nombreBonRemis.val() <= 0) {

            montantTot = 0;
            montantTotal.val('');
            spnDevise2.html('');

            btnSave.attr('disabled', true);
            alertify.alert('Le nombre de bon remis ne doit pas être inférireur à zéro');

            return;

        } else {

            montantTot = parseFloat(nombreBonRemis.val() * tauxRemise);
            montantTotal.val(formatNumberOnly(montantTot));
            spnDevise2.html(' En (' + deviseSelected + ')');

            btnSave.attr('disabled', false);
        }

    });

    btnSave.on('click', function (e) {

        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        if(selectModePaiement.val() == '0'){
            
            alertify.alert('Veuillez d\'abord sélectionner le mode de paiement de la commande');
            return;
            
        }
        
        if (datePaiement.val() == '' || datePaiement.val() == null) {

            alertify.alert('Veuillez d\'abord indiquer la date du paiement');
            return;

        }

        alertify.confirm('Etes-vous sûre de vouloir enregistrer ces informations ?', function () {
            save();
        });

    });

    btnSearchSociete.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        assujettiModal.modal('show');
    });

    selectSiteEdit.on('change', function (e) {

        if (selectSiteEdit.val() == '0') {
            selectCategorieEdit.val('0');
            selectCategorieEdit.attr('disabled', true);
            alertify.alert('Veuillez d\'abord sélectionner un axe de péage valide');
            return;
        } else {
            selectCategorieEdit.attr('disabled', false);
            selectCategorieEdit.val('0');
        }
    });

    selectCategorieEdit.on('change', function (e) {

        if (selectCategorieEdit.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner une catégorie valdie');
            return;
        } else {

            nombreBonRemis.val('');
            tarifEdit.val('');
            spnDevise.html('');

            montantTotal.val('');
            spnDevise2.html('');
            getInfoRemise();
        }
    });

    initDataRemise();

});

function initDataRemise() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'userId': userData.idUser,
            'operation': 'initDataRemise'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {


            $.unblockUI();

            if (response == '-1' | response == '0') {

                showResponseError();
                return;

            } else {

                axeList = JSON.parse(response.axePeageList);
                categorieList = JSON.parse(response.tarifList);

                var dataAxe;
                dataAxe += '<option value="0">--</option>';

                for (var i = 0; i < axeList.length; i++) {
                    dataAxe += '<option value="' + axeList[i].axeCode + '">' + axeList[i].axeName + '</option>';
                }

                selectSiteEdit.html(dataAxe);

                var dataTarif;
                dataTarif += '<option value="0">--</option>';

                for (var i = 0; i < categorieList.length; i++) {
                    dataTarif += '<option value="' + categorieList[i].tarifCode + '">' + categorieList[i].tarifName + '</option>';
                }

                //dataTarif += '<option value="new" style="font-style: italic;font-weight: bold;color:green">+ Ajouter une cat&eacute;gorie</option>';

                selectCategorieEdit.html(dataTarif);

            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function getInfoRemise() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'codePersonne': codePersonneSelected,
            'codeTarif': selectCategorieEdit.val(),
            'codeAxe': selectSiteEdit.val(),
            'operation': 'getInfoRemise'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                tarifEdit.val('');
                codeSiteSelected = '';
                spnDevise.html('');
                deviseSelected = '';
                tauxRemise = '';
                tauxNormal = '';

                showResponseError();
                return;

            } else if (response == '2') {

                tarifEdit.val('');
                codeSiteSelected = '';
                spnDevise.html('');
                deviseSelected = '';
                tauxRemise = '';
                tauxNormal = '';

                var value = '<span style="font-weight:bold">' + $('#selectSiteEdit option:selected').text().toUpperCase() + '</span>';

                alertify.alert('Veuillez d\'abord configurer le tarif normal d\'une course dans l\'axe : ' + value + ' avant de continuer');
                return;

            } else if (response == '3') {
                
                var value = '<span style="font-weight:bold">' + $('#selectSiteEdit option:selected').text().toUpperCase() + '</span>';
                var valueCateg = '<span style="font-weight:bold">' + $('#selectCategorieEdit option:selected').text().toUpperCase() + '</span>';
                alertify.alert('Veuillez d\'abord configurer la catégorie : ' +valueCateg + ' <br/>' + 'sur un poste de péage de l\'axe : ' + value + ' avant de continuer');
                return;
            } else {

                tarifEdit.val(formatNumberOnly(response.tauxRemise));
                codeSiteSelected = response.codeSite;
                tauxNormal = response.tauxNormal;
                tauxRemise = response.tauxRemise;
                //tauxRemise = response.percentRemise == 0 ? response.tauxRemise : response.percentRemise;
                deviseSelected = response.devise;
                spnDevise.html(' En (' + response.devise + ')');

            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function getSelectedAssujetiData() {
    codePersonneSelected = selectAssujettiData.code;
    societeNameEdit.val(selectAssujettiData.nomComplet + ' (Adresse : ' + selectAssujettiData.adresse.toUpperCase() + ')');

    selectSiteEdit.attr('disabled', false);
}

function save() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'codePersonne': codePersonneSelected,
            'codeTarif': selectCategorieEdit.val(),
            'codeAxe': selectSiteEdit.val(),
            'compteBancaire': selectCompteBancaire.val(),
            'qte': nombreBonRemis.val(),
            'amount': montantTot,
            'devise': deviseSelected,
            'tauxNormal': tauxNormal,
            'tauxRemise': tauxRemise,
            'userId': userData.idUser,
            'codeSite': codeSiteSelected,
            'datePaiement': datePaiement.val(),
            'modePaiement': selectModePaiement.val(),
            'operation': 'saveCommandeV2'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;

            } else {

                alertify.alert('L\'enregistrement s\'est effectué avec succès');

                setTimeout(function () {
                    window.location = 'gestion-existant-bons-sous-provision';
                }, 3000);
            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

