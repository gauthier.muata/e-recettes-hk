/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnAddTarifSite;
var modalUpdateTarifSite;
var tableTaifSite;

var tarifSiteList = [];

var tarifSiteID, selectSiteProvenance, selectSiteDestination, selectTarif, tauxTarif, selectDevise;

var codeAccount;

var btnDeleteTarifSite, btnSaveTarifSite;

$(function () {

    tarifSiteID = empty;

    mainNavigationLabel.text('Gestion des tarifs sites péage');
    secondNavigationLabel.text('Affectation tarif sur un axe péage ');
    removeActiveMenu();

    btnAddTarifSite = $('#btnAddTarifSite');
    modalUpdateTarifSite = $('#modalUpdateTarifSite');
    tableTaifSite = $('#tableTaifSite');

    selectSiteProvenance = $('#selectSiteProvenance');
    selectSiteDestination = $('#selectSiteDestination');
    selectTarif = $('#selectTarif');
    tauxTarif = $('#tauxTarif');
    btnDeleteTarifSite = $('#btnDeleteTarifSite');
    btnSaveTarifSite = $('#btnSaveTarifSite');
    selectDevise = $('#selectDevise');

    btnAddTarifSite.on('click', function (e) {
        e.preventDefault();

        tarifSiteID = empty;
        selectSiteProvenance.val(empty);
        selectSiteDestination.val(empty);
        selectTarif.val(empty);
        tauxTarif.val(empty);

        btnDeleteTarifSite.attr('style', 'display:none');
        modalUpdateTarifSite.modal('show');

    });

    btnSaveTarifSite.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();

    });

    btnDeleteTarifSite.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        deleteTarifSite();

    });

    initDataTarifSite();

});

function deleteTarifSite() {

    alertify.confirm('Etes-vous sûr de vouloir supprimer ce tarif ?', function () {
        $.ajax({
            type: 'POST',
            url: 'articleBudgetaire_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'id': tarifSiteID,
                'userId': userData.idUser,
                'operation': 'deleteTarifSite'
            },
            beforeSend: function () {
                modalUpdateTarifSite.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression tarif en cours ...</h5>'});
            },
            success: function (response)
            {

                setTimeout(function () {

                    modalUpdateTarifSite.unblock();

                    if (response == '-1' | response == '0') {

                        showResponseError();
                        return;

                    } else {
                        alertify.alert('La suppression du tarif s\'est effectuée avec succès');
                        resetFields();
                        modalUpdateTarifSite.modal('hide');
                        loadTarifSites();
                    }
                }
                , 1);
            },
            complete: function () {
            },
            error: function () {
                modalUpdateTarifSite.unblock();
                showResponseError();
            }
        });
    });
}

function resetFields() {

    tarifSiteID = empty;
    selectSiteProvenance.val(empty);
    selectSiteDestination.val(empty);
    selectTarif.val(empty);
    selectDevise.val('0');
    tauxTarif.val(empty);
}

function checkFields() {

    if (selectSiteProvenance.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner le site de provenance');
        selectSiteProvenance.focus();
        return;
    }

    if (selectSiteDestination.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner le site de destination');
        selectSiteDestination.focus();
        return;
    }

    if (selectTarif.val() == '0') {
        alertify.alert('Veuillez d\'abord saisir sélectionner le tarif');
        selectTarif.focus();
        return;
    }

    if (tauxTarif.val() == '') {
        alertify.alert('Veuillez d\'abord saisir le taux du tarif');
        tauxTarif.focus();
        return;
    } else if (tauxTarif.val() < 0) {
        alertify.alert('Désolé, le taux du tarif ne doit pas être inférieur à zéro');
        tauxTarif.focus();
        return;
    }

    if (selectDevise.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner la devise');
        selectDevise.focus();
        return;
    }

    alertify.confirm('Etes-vous sûr de vouloir enregistrer ce tarif ?', function () {
        saveTarifSite();
    });
}

function saveTarifSite() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': tarifSiteID,
            'siteProvenance': selectSiteProvenance.val(),
            'siteDestination': selectSiteDestination.val(),
            'tarif': selectTarif.val(),
            'taux': tauxTarif.val(),
            'devise': selectDevise.val(),
            'userId': userData.idUser,
            'operation': 'saveTarifSite'
        },
        beforeSend: function () {
            modalUpdateTarifSite.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du tarif en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalUpdateTarifSite.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('L\'enregistrement du tarif s\'est effectué avec succès');
                    resetFields();
                    modalUpdateTarifSite.modal('hide');
                    loadTarifSites();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalUpdateTarifSite.unblock();
            showResponseError();
        }
    });
}

function loadTarifSites() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadTarifSites'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1') {

                    printTableTarifSite(empty);
                    showResponseError();
                    return;

                } else if (response == '0') {

                    printTableTarifSite(empty);

                } else {

                    tarifSiteList = JSON.parse(JSON.stringify(response));
                    printTableTarifSite(tarifSiteList);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printTableTarifSite(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:25%">SITE DE PROVENANCE</th>';
    tableContent += '<th style="text-align:left;width:25%">SITE DE DESTINATION</th>';
    tableContent += '<th style="text-align:left;width:30%">TARIF</th>';
    tableContent += '<th style="text-align:right;width:15%">MONTANT</th>';
    tableContent += '<th style="text-align:center;width:5%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var btnEditTarifSite = '<button class="btn btn-success" onclick="editTarifSite(\'' + result[i].id + '\')"><i class="fa fa-edit"></i></button>';


        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:25%">' + result[i].siteProvenanaceName + '</td>';
        tableContent += '<td style="text-align:left;width:25%">' + result[i].siteDestinantionName + '</td>';
        tableContent += '<td style="text-align:left;width:30%">' + result[i].tarifName + '</td>';
        tableContent += '<td style="text-align:right;width:15%">' + formatNumber(result[i].taux, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:center;width:5%">' + btnEditTarifSite + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableTaifSite.html(tableContent);

    tableTaifSite.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des articles budgétaires est vide",
            search: "Rechercher par intitulé _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}

function editTarifSite(id) {

    for (var i = 0; i < tarifSiteList.length; i++) {

        if (tarifSiteList[i].id == id) {

            tarifSiteID = id;

            selectSiteProvenance.val(tarifSiteList[i].siteProvenanaceCode);
            selectSiteDestination.val(tarifSiteList[i].siteDestinantionCode);
            selectTarif.val(tarifSiteList[i].tarifCode);
            tauxTarif.val(tarifSiteList[i].taux);
            selectDevise.val(tarifSiteList[i].devise);

            btnDeleteTarifSite.attr('style', 'display:inline');
            modalUpdateTarifSite.modal('show');

            break;
        }
    }

}

function initDataTarifSite() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'userId': userData.idUser,
            'operation': 'initDataTarifSite'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {


            $.unblockUI();

            if (response == '-1' | response == '0') {

                showResponseError();
                return;

            } else {

                var siteProvenantList = JSON.parse(response.siteProvenanceList);
                var dataSiteA;
                dataSiteA += '<option value="0">--</option>';

                for (var i = 0; i < siteProvenantList.length; i++) {
                    dataSiteA += '<option value="' + siteProvenantList[i].siteCode + '">' + siteProvenantList[i].siteName + '</option>';
                }

                selectSiteProvenance.html(dataSiteA);

                var siteDestinationList = JSON.parse(response.siteDestinationList);
                var dataSiteB;
                dataSiteB += '<option value="0">--</option>';

                for (var i = 0; i < siteProvenantList.length; i++) {
                    dataSiteB += '<option value="' + siteDestinationList[i].siteCode + '">' + siteDestinationList[i].siteName + '</option>';
                }

                selectSiteDestination.html(dataSiteB);

                var tarifList = JSON.parse(response.tarifList);
                var dataTarif;
                dataTarif += '<option value="0">--</option>';

                for (var i = 0; i < tarifList.length; i++) {
                    dataTarif += '<option value="' + tarifList[i].tarifCode + '">' + tarifList[i].tarifName + '</option>';
                }

                selectTarif.html(dataTarif);

                loadTarifSites();
            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}