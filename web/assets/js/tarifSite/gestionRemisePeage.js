/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnSaveRemiseSelected,
        btnDeleteRemise,
        tarifSelected,
        selectModePaiementSelected,
        selectCategorieSelected,
        societeNameSelectd,
        modalMiseAJourInfo,
        tableRemise,
        btnSaveRemiseEdit,
        selectModePaiementEdit,
        tarifEdit,
        selectCategorieEdit,
        btnSearchSociete;

var selectSiteEdit,
        selectSiteSelected;

var societeNameEdit, selectCategorieEdit;

var codePersonneSelected, idRowSelected;
var resultDataSite;
var remisePeageList;

var axeList, categorieList;

var newCategorieRemiseModal,
        selectNewCategorie,
        btnValidateINewCategorieRemise;

var spnDeviseTarif;
var selectDeviseEdit,
        selectDeviseSelected;

$(function () {

    idRowSelected = empty;

    mainNavigationLabel.text('Gestion des allegements sur la taxe de péage');
    secondNavigationLabel.text('Remise de pré-paiement et crédit');
    removeActiveMenu();

    btnAddTarifSite = $('#btnAddTarifSite');
    btnSaveRemiseSelected = $('#btnSaveRemiseSelected');
    btnDeleteRemise = $('#btnDeleteRemise');
    tarifSelected = $('#tarifSelected');
    selectModePaiementSelected = $('#selectModePaiementSelected');
    selectCategorieSelected = $('#selectCategorieSelected');
    societeNameSelectd = $('#societeNameSelectd');
    modalMiseAJourInfo = $('#modalMiseAJourInfo');

    tableRemise = $('#tableRemise');

    btnSaveRemiseEdit = $('#btnSaveRemiseEdit');
    selectModePaiementEdit = $('#selectModePaiementEdit');
    tarifEdit = $('#tarifEdit');
    selectCategorieEdit = $('#selectCategorieEdit');
    btnSearchSociete = $('#btnSearchSociete');

    selectSiteEdit = $('#selectSiteEdit');
    selectSiteSelected = $('#selectSiteSelected');

    societeNameEdit = $('#societeNameEdit');
    selectCategorieEdit = $('#selectCategorieEdit');

    selectDeviseEdit = $('#selectDeviseEdit');
    selectDeviseSelected = $('#selectDeviseSelected');

    newCategorieRemiseModal = $('#newCategorieRemiseModal');
    selectNewCategorie = $('#selectNewCategorie');
    btnValidateINewCategorieRemise = $('#btnValidateINewCategorieRemise');
    spnDeviseTarif = $('#spnDeviseTarif');

    btnSearchSociete.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        assujettiModal.modal('show');
    });

    selectDeviseEdit.on('change', function (e) {

        if (selectDeviseEdit.val() == '0') {
            spnDeviseTarif.html('');
            alertify.alert('Veuillez d\'abord sélectionner une devise valide');
        } else {
            spnDeviseTarif.html('(' + selectDeviseEdit.val() + ')');
        }
    });

    selectCategorieEdit.on('change', function (e) {

        if (selectCategorieEdit.val() == 'new') {

            alertify.confirm('Etes-vous sûr de vouloir ajouter une catégorie parmis les catégories de remise ?', function () {

                loadNewCategorieRemise();

            });
        }
    });

    btnSaveRemiseEdit.on('click', function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();


    });

    btnValidateINewCategorieRemise.on('click', function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var categ = '<span style="font-weight:bold">' + $('#selectNewCategorie option:selected').text() + '</span>';

        alertify.confirm('Etes-vous sûr de vouloir ajouter : ' + categ + ' parmis les catégiories de remise ?', function () {
            addNewCategorieRemise();
        });


    });

    btnSaveRemiseSelected.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields2()();

    });

    btnDeleteRemise.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        deleteRemisePeage();

    });

    initDataRemise();
    printTableRemisePeage('');
});

function deleteRemisePeage() {

    alertify.confirm('Etes-vous sûr de vouloir supprimer ces informations ?', function () {
        $.ajax({
            type: 'POST',
            url: 'peage_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'id': idRowSelected,
                'userId': userData.idUser,
                'operation': 'deleteRemisePeage'
            },
            beforeSend: function () {
                modalMiseAJourInfo.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
            },
            success: function (response)
            {

                setTimeout(function () {

                    modalMiseAJourInfo.unblock();

                    if (response == '-1' | response == '0') {

                        showResponseError();
                        return;

                    } else {
                        alertify.alert('La suppression s\'est effectuée avec succès');
                        resetFields();
                        modalMiseAJourInfo.modal('hide');
                        loadRemisePeage();
                    }
                }
                , 1);
            },
            complete: function () {
            },
            error: function () {
                modalMiseAJourInfo.unblock();
                showResponseError();
            }
        });
    });
}

function resetFields() {

    idRowSelected = '';
    codePersonneSelected = '';
    societeNameEdit.val('');
    selectSiteEdit.val('0');
    selectCategorieEdit.val('0');
    selectModePaiementEdit.val('0');
    selectDeviseEdit.val('0');
    selectDeviseSelected.val('0');
    spnDeviseTarif.html('');
    tarifEdit.val('');
}

function checkFields() {

    if (societeNameEdit.val() == empty) {
        alertify.alert('Veuillez d\'abord rechercher et sélectionner une société');
        societeNameEdit.focus();
        return;
    }

    if (selectSiteEdit.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner un axe péage');
        selectSiteEdit.focus();
        return;
    }

    if (selectCategorieEdit.val() == '0') {
        alertify.alert('Veuillez d\'abord saisir sélectionner la catégorie');
        selectCategorieEdit.focus();
        return;
    }

    if (selectModePaiementEdit.val() == '0') {
        alertify.alert('Veuillez d\'abord saisir sélectionner le mode de paiement');
        selectModePaiementEdit.focus();
        return;

    } /*else if (selectModePaiementEdit.val() == '2' && selectCategorieEdit.val() !== '*') {
     
     alertify.alert('Pour le mode paiement (CREDIT), vous devez sélectionner la catégorie (TOUTES LES CATEGORIES)');
     selectModePaiementEdit.focus();
     return;
     
     }*/

    if (tarifEdit.val() == '') {
        alertify.alert('Veuillez d\'abord saisir le tarif');
        tarifEdit.focus();
        return;
    } else if (tarifEdit.val() < 0) {
        alertify.alert('Désolé, le tarif ne doit pas être inférieur à zéro');
        tarifEdit.focus();
        return;
    }

    if (selectDeviseEdit.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner une devise valide');
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir enregistrer ces informations ?', function () {
        saveRemisePeageForEdit();
    });
}

function checkFields2() {

    if (societeNameSelectd.val() == empty) {
        alertify.alert('Veuillez d\'abord rechercher et sélectionner une société');
        societeNameEdit.focus();
        return;
    }

    if (selectSiteSelected.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner un poste de péage');
        selectSiteSelected.focus();
        return;
    }

    if (selectCategorieSelected.val() == '0') {
        alertify.alert('Veuillez d\'abord saisir sélectionner la catégorie');
        selectCategorieSelected.focus();
        return;
    }

    if (selectModePaiementSelected.val() == '0') {
        alertify.alert('Veuillez d\'abord saisir sélectionner le mode de paiement');
        selectModePaiementSelected.focus();
        return;
    } /*else if (selectModePaiementSelected.val() == '2' && selectCategorieSelected.val() !== '*') {
     
     alertify.alert('Pour le mode paiement (CREDIT), vous devez sélectionner la catégorie (TOUTES LES CATEGORIES)');
     selectCategorieSelected.focus();
     return;
     
     }*/

    if (tarifSelected.val() == '') {
        alertify.alert('Veuillez d\'abord saisir le tarif');
        tarifSelected.focus();
        return;
    } else if (tarifSelected.val() < 0) {
        alertify.alert('Désolé, le tarif ne doit pas être inférieur à zéro');
        tarifSelected.focus();
        return;
    }

    if (selectDeviseSelected.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner une devise valide');
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir modifier ces informations ?', function () {
        saveRemisePeageForModidification();
    });
}

function saveRemisePeageForEdit() {

    idRowSelected = '';

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idRowSelected,
            'codePersonne': codePersonneSelected,
            'codeTarif': selectCategorieEdit.val(),
            'taux': tarifEdit.val() == '*' ? null : tarifEdit.val(),
            'modePaiement': selectModePaiementEdit.val(),
            'codeSite': selectSiteEdit.val(),
            'userId': userData.idUser,
            'devise': selectDeviseEdit.val(),
            'operation': 'saveRemisePeageForEdit'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('L\'enregistrement s\'est effectué avec succès');
                    resetFields();
                    loadRemisePeage();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadRemisePeage() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadRemisePeage'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1') {

                    printTableRemisePeage(empty);
                    showResponseError();
                    return;

                } else if (response == '0') {

                    printTableRemisePeage(empty);

                } else {

                    remisePeageList = JSON.parse(JSON.stringify(response));
                    printTableRemisePeage(remisePeageList);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printTableRemisePeage(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:30%">SOCIETE</th>';
    tableContent += '<th style="text-align:left;width:20%">CATEGORIE</th>';
    tableContent += '<th style="text-align:left;width:15%">AXE PEAGE</th>';
    tableContent += '<th style="text-align:right;width:15%">TARIF</th>';
    tableContent += '<th style="text-align:left;width:15%">MODE DE PERCEPTION</th>';
    tableContent += '<th style="text-align:center;width:5%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var btnEditRemisePeage = '<button class="btn btn-success" onclick="editRemisePeage(\'' + result[i].id + '\')"><i class="fa fa-edit"></i></button>';

        tableContent += '<tr>';
        //tableContent += '<td style="text-align:left;width:30%;vertical-align:middle">' + result[i].namePersonne + '<br/><br/>' + result[i].adressePersonne + '</td>';
        tableContent += '<td style="text-align:left;width:30%;vertical-align:middle;font-weight:bold">' + result[i].namePersonne + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].nameTarif + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + result[i].nameSite + '</td>';

        /*if (result[i].modePaiement == 'PRE-PAIEMENT') {
         tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + formatNumber(result[i].taux, 'USD') + '</td>';
         } else {
         tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + formatNumberOnly(result[i].taux) + '%' + '</td>';
         }*/

        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + formatNumber(result[i].taux, result[i].devise) + '</td>';

        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + result[i].modePaiement + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + btnEditRemisePeage + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableRemise.html(tableContent);

    tableRemise.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des remises est vide",
            search: "Filtrer la liste ici _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        select: {
            style: 'os',
            blurable: true
        }
    });
}

function editRemisePeage(id) {

    alertify.confirm('Etes-vous sûr de vouloir afficher ces informations ?', function () {

        for (var i = 0; i < remisePeageList.length; i++) {

            if (remisePeageList[i].id == id) {

                idRowSelected = id;
                codePersonneSelected = remisePeageList[i].codePersonne;

                societeNameSelectd.val(remisePeageList[i].namePersonne);
                selectDeviseSelected.val(remisePeageList[i].devise);

                var dataSite;
                dataSite += '<option value="0">--</option>';

                for (var l = 0; l < axeList.length; l++) {
                    dataSite += '<option value="' + axeList[l].axeCode + '">' + axeList[l].axeName + '</option>';
                }

                selectSiteSelected.html(dataSite);
                selectSiteSelected.val(remisePeageList[i].codeSite);

                var dataTarif;
                dataTarif += '<option value="0">--</option>';

                for (var k = 0; k < categorieList.length; k++) {
                    dataTarif += '<option value="' + categorieList[k].tarifCode + '">' + categorieList[k].tarifName + '</option>';
                }

                dataTarif += '<option value="*"">+ Toutes les cat&eacute;gories</option>';

                selectCategorieSelected.html(dataTarif);

                selectCategorieSelected.val(remisePeageList[i].codeTarif);
                selectModePaiementSelected.val(remisePeageList[i].modePaiementCode);
                tarifSelected.val(remisePeageList[i].taux);

                modalMiseAJourInfo.modal('show');

                break;
            }
        }
    });


}

function initDataRemise() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'userId': userData.idUser,
            'operation': 'initDataRemise'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {


            $.unblockUI();

            if (response == '-1' | response == '0') {

                showResponseError();
                return;

            } else {

                axeList = JSON.parse(response.axePeageList);
                categorieList = JSON.parse(response.tarifList);

                var dataAxe;
                dataAxe += '<option value="0">--</option>';

                for (var i = 0; i < axeList.length; i++) {
                    dataAxe += '<option value="' + axeList[i].axeCode + '">' + axeList[i].axeName + '</option>';
                }

                selectSiteEdit.html(dataAxe);

                var dataTarif;
                dataTarif += '<option value="0">--</option>';

                for (var i = 0; i < categorieList.length; i++) {
                    dataTarif += '<option value="' + categorieList[i].tarifCode + '">' + categorieList[i].tarifName + '</option>';
                }

                dataTarif += '<option value="*"">+ Toutes les cat&eacute;gories</option>';
                dataTarif += '<option value="new" style="font-style: italic;font-weight: bold;color:green">+ Ajouter une cat&eacute;gorie</option>';

                selectCategorieEdit.html(dataTarif);

                loadRemisePeage();
            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function getSelectedAssujetiData() {
    codePersonneSelected = selectAssujettiData.code;
    societeNameEdit.val(selectAssujettiData.nomComplet + ' (Adresse : ' + selectAssujettiData.adresse.toUpperCase() + ')');
}

function saveRemisePeageForModidification() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idRowSelected,
            'codePersonne': codePersonneSelected,
            'codeTarif': selectCategorieSelected.val(),
            'taux': tarifSelected.val(),
            'modePaiement': selectModePaiementSelected.val(),
            'codeSite': selectSiteSelected.val(),
            'userId': userData.idUser,
            'devise': selectDeviseSelected.val(),
            'operation': 'saveRemisePeageForEdit'
        },
        beforeSend: function () {
            modalMiseAJourInfo.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalMiseAJourInfo.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('La modification s\'est effectuée avec succès');
                    modalMiseAJourInfo.modal('hide');
                    resetFields();
                    loadRemisePeage();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalMiseAJourInfo.unblock();
            showResponseError();
        }
    });
}

function loadNewCategorieRemise() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadNewCategorieRemise'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' || response == '0') {
                    showResponseError();
                    return;
                } else {

                    var result = JSON.parse(JSON.stringify(response));

                    var dataTarif;
                    dataTarif += '<option value="0">--</option>';

                    for (var i = 0; i < result.length; i++) {
                        dataTarif += '<option value="' + result[i].tarifCode + '">' + result[i].tarifName + '</option>';
                    }

                    selectNewCategorie.html(dataTarif);
                    newCategorieRemiseModal.modal('show');

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function addNewCategorieRemise() {

    var categ = '<span style="font-weight:bold">' + $('#selectNewCategorie option:selected').text() + '</span>';

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'Text',
        crossDomain: false,
        data: {
            'operation': 'addNewCategorieRemise',
            'codeTarif': selectNewCategorie.val()
        },
        beforeSend: function () {
            newCategorieRemiseModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ajout de la catégorie en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                newCategorieRemiseModal.unblock();

                if (response == '-1' || response == '0') {
                    showResponseError();
                    return;

                } else {

                    alertify.alert('L\'ajout  de la catégorie : ' + categ + ' s\'est effectuée avec succès');

                    initDataRemise();
                    newCategorieRemiseModal.modal('hide');

                }
            }
            , 2);
        },
        complete: function () {
        },
        error: function () {
            newCategorieRemiseModal.unblock();
            showResponseError();
        }
    });
}