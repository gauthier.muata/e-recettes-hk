/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var modalCorrectionNoteTaxation,
        selectTypeCorrection,
        divItem1,
        numeroNoteDeleted,
        divItem2,
        selectBankAccountBank,
        selectNewAccountBank,
        divItem3,
        amountNote,
        numeroNoteUpdate1,
        numeroNoteUpdate2,
        btnValidateAction;

var divItem4,
        numeroNoteUpdate3,
        selectDevise;

$(function () {

    mainNavigationLabel.text('DECLARATION');
    secondNavigationLabel.text('Correction de la note de taxation');

    removeActiveMenu();
    linkSubMenuEditerPaiement.addClass('active');

    modalCorrectionNoteTaxation = $('#modalCorrectionNoteTaxation');
    selectTypeCorrection = $('#selectTypeCorrection');
    divItem1 = $('#divItem1');
    numeroNoteDeleted = $('#numeroNoteDeleted');
    divItem2 = $('#divItem2');
    selectBankAccountBank = $('#selectBankAccountBank');
    selectNewAccountBank = $('#selectNewAccountBank');
    divItem3 = $('#divItem3');
    btnValidateAction = $('#btnValidateAction');
    amountNote = $('#amountNote');
    numeroNoteUpdate1 = $('#numeroNoteUpdate1');
    numeroNoteUpdate2 = $('#numeroNoteUpdate2');

    divItem4 = $('#divItem4');
    numeroNoteUpdate3 = $('#numeroNoteUpdate3');
    selectDevise = $('#selectDevise');


    selectBankAccountBank.change(function (e) {

        e.preventDefault();

        if (selectBankAccountBank.val() !== '0') {
            loadingAccountBankData(selectBankAccountBank.val());
        } else {

            selectNewAccountBank.val('0');
            selectNewAccountBank.attr('disabled', true);
            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;
        }

    });

    selectTypeCorrection.on('change', function (e) {

        if (selectTypeCorrection.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un type de correction valide');
            return;

        } else {

            switch (selectTypeCorrection.val()) {

                case '1' : //Suppression de la note

                    divItem1.attr('style', 'display:inline');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');

                    break;
                case '2' : //Modifier le compte bancaire de la note

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:inline');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');

                    loadingBankData();
                    break;
                case '3' : //Modifier le montant de la note
                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:inline');
                    divItem4.attr('style', 'display:none');
                    break;

                case '4' : //Modifier la devsie de la note
                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:inline');
                    break;
            }
        }
    });

    btnValidateAction.on('click', function (e) {

        e.preventDefault();

        if (selectTypeCorrection.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un type de correction valide');
            return;

        } else {

            switch (selectTypeCorrection.val()) {

                case '1' : //Suppression de la note

                    if (numeroNoteDeleted.val() == empty) {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de taxarion à supprimer');
                        return;
                    } else {
                        deleteNoteTaxarion();
                    }

                    break;
                case '2' : //Modifier le compte bancaire de la note

                    if (numeroNoteUpdate1.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de taxation');
                        return;
                    }

                    if (selectBankAccountBank.val() == '0') {
                        alertify.alert('Veuillez d\'abord sélectionner une banque valide');
                        return;
                    }

                    if (selectNewAccountBank.val() == '0') {
                        alertify.alert('Veuillez d\'abord sélectionner un compte bancaire valide');
                        return;
                    }

                    updateAccountBank();

                    break;

                case '3' : //Modifier le montant de la note

                    if (numeroNoteUpdate2.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de taxation');
                        return;
                    }

                    if (amountNote.val() == '') {

                        alertify.alert('Veuillez d\'abord fournir le nouveau montant de la note');
                        return;

                    } else if (amountNote.val() <= 0) {

                        alertify.alert('Le nouveau montant de la note ne doit pas être inférieur à zéro');
                        return;
                    }

                    updateAmountNoteTaxation();
                    break;

                case '4': //Modifier la devise de la note

                    if (numeroNoteUpdate3.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de taxation');
                        return;
                    }

                    if (selectDevise.val() == '0') {
                        alertify.alert('Veuillez d\'abord sélectionner la nouvelle devise de la note de taxation');
                        return;
                    }

                    updateDeviseNoteTaxation();
                    break;
            }
        }
    });

    modalCorrectionNoteTaxation.modal('show');

})

function deleteNoteTaxarion() {

    var value = '<span style="font-weight:bold">' + numeroNoteDeleted.val().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir supprimer cette note de taxarion n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numero': numeroNoteDeleted.val().trim(),
                'userId': userData.idUser,
                'operation': 'deleteNoteTaxarion'

            },
            beforeSend: function () {
                modalCorrectionNoteTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionNoteTaxation.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette note de taxation n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else if (response == '3') {

                    alertify.alert('SUPPRESSION IMPOSSIBLE. <br/><br/> Cette note de taxation n° ' + value + ' est déjà payée à la banque !');
                    return;

                } else {

                    alertify.alert('La supression de la note de taxation n° ' + value + ' s\'est effectuée avec succès');
                    numeroNoteDeleted.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionNoteTaxation.unblock();
                showResponseError();
            }

        });
    });

}

function updateAccountBank() {

    var value = '<span style="font-weight:bold">' + numeroNoteUpdate1.val().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir modifier le compte bancaire cette note de taxarion n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numero': numeroNoteUpdate1.val().trim(),
                'userId': userData.idUser,
                'compteBancaire': selectNewAccountBank.val(),
                'banque': selectBankAccountBank.val(),
                'operation': 'updateAccountBank'

            },
            beforeSend: function () {
                modalCorrectionNoteTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionNoteTaxation.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette note de taxation n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else if (response == '3') {

                    alertify.alert('MODIFICATION IMPOSSIBLE. <br/><br/> Cette note de taxation n° ' + value + ' est déjà payée à la banque !');
                    return;

                } else {

                    alertify.alert('La modification du compte bancaire de la note de taxation n° ' + value + ' s\'est effectuée avec succès');
                    numeroNoteUpdate1.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionNoteTaxation.unblock();
                showResponseError();
            }

        });
    });

}

function loadingBankData() {


    var dataBank = empty;
    var defautlValue = '--';
    dataBank += '<option value="0">' + defautlValue + '</option>';

    var banqueUserList = JSON.parse(userData.banqueUserList);

    for (var i = 0; i < banqueUserList.length; i++) {

        dataBank += '<option value="' + banqueUserList[i].banqueCode + '" >' + banqueUserList[i].banqueName + '</option>';

    }

    selectBankAccountBank.html(dataBank);
}

function loadingAccountBankData(bankCode) {

    var dataAccountBank = empty;
    var defautlValue = '--';

    dataAccountBank += '<option value="0">' + defautlValue + '</option>';

    var accountBankList = JSON.parse(userData.accountList);

    for (var i = 0; i < accountBankList.length; i++) {
        if (bankCode == accountBankList[i].acountBanqueCode && (accountBankList[i].typeCompteCode == 2 | accountBankList[i].typeCompteCode == 3)) {
            dataAccountBank += '<option value="' + accountBankList[i].accountCode + '" >' + accountBankList[i].accountName + '</option>';
        }
    }

    selectNewAccountBank.html(dataAccountBank);
    selectNewAccountBank.attr('disabled', false);
}

function updateAmountNoteTaxation() {

    var value = '<span style="font-weight:bold">' + numeroNoteUpdate2.val().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir modifier le montant cette note de taxarion n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numero': numeroNoteUpdate2.val().trim(),
                'userId': userData.idUser,
                'amountNote': amountNote.val(),
                'operation': 'updateAmountNoteTaxation'

            },
            beforeSend: function () {
                modalCorrectionNoteTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionNoteTaxation.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette note de taxation n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else if (response == '3') {

                    alertify.alert('MODIFICATION IMPOSSIBLE. <br/><br/> Cette note de taxation n° ' + value + ' est déjà payée à la banque !');
                    return;

                } else {

                    alertify.alert('La modification du montant de la note de taxation n° ' + value + ' s\'est effectuée avec succès');
                    amountNote.val('');
                    numeroNoteUpdate2.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionNoteTaxation.unblock();
                showResponseError();
            }

        });
    });

}

function updateDeviseNoteTaxation() {

    var value = '<span style="font-weight:bold">' + numeroNoteUpdate3.val().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir modifier la devise de la note de taxarion n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numero': numeroNoteUpdate3.val().trim(),
                'userId': userData.idUser,
                'deviseNote': selectDevise.val(),
                'operation': 'updateDeviseNoteTaxation'

            },
            beforeSend: function () {
                modalCorrectionNoteTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionNoteTaxation.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette note de taxation n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else if (response == '3') {

                    alertify.alert('MODIFICATION IMPOSSIBLE. <br/><br/> Cette note de taxation n° ' + value + ' est déjà payée à la banque !');
                    return;

                } else {

                    alertify.alert('La modification de devise de la note de taxation n° ' + value + ' s\'est effectuée avec succès');
                    selectDevise.val('0');
                    numeroNoteUpdate3.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionNoteTaxation.unblock();
                showResponseError();
            }

        });
    });

}

