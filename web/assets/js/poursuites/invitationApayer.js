/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var miseEnDemeureList = [];

var selectTypeSearch;
var valueTypeSearch;
var selectTypeMiseEnDemeure;

var selectTypeSearchID;
var selectTypeMiseEnDemeureID;
var selectStateMedID;
var btnSimpleSearch;
var isAdvanced;
var onClickBtnAdvancedSearch;

var responsibleObject = new Object();
var tableMiseEnDemeure;
var modalAccuserReception;

var codeTypeDoc = 'INVITATION_PAIEMENT';
var codeDoc = undefined;

var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var modalRechercheAvanceeMed;
var selectService;
var selectStateMed;

var inputDateDebutMed;
var inputdateLastMed;

var datePickerMed1;
var datePickerMed2;

var btnShowAdvancedSerachModal;
var btnAdvencedSearchMed;
var codeMedCurrent;

var isAdvance;

var lblService, lblTypeMed, lblStateMed, lblDateDebut, lblDateFin;

var modalFichePriseEnCharge;

var periodeIDSelected;
var fromCall;

var penalitieList = [];

var amountPenalite;
var amountPenaliteFormat;
var medIDSelected;
var numberRows;
var deviseSelected;
var dateDebuts, dateFins;
var divSelectTypeMiseEnDemeure;
var modalInfoComplementAmr, inputMotifAmr, inputDetailMotifAmr, btnValider;

var medSelected, numeroDocumentSelected,noteTaxationSelected;
var idDivServiceMed;

$(function () {

    fromCall = '0';
    numberRows = empty;
    onClickBtnAdvancedSearch = '0';

    mainNavigationLabel.text('POURSUITES');
    secondNavigationLabel.text('Registre des invitations à payer');
    removeActiveMenu();
    linkMenuContentieux.addClass('active');

    selectTypeSearch = $('#selectTypeSearch');
    valueTypeSearch = $('#valueTypeSearch');
    selectTypeMiseEnDemeure = $('#selectTypeMiseEnDemeure');
    btnSimpleSearch = $('#btnSimpleSearch');
    tableMiseEnDemeure = $('#tableMiseEnDemeure');
    modalAccuserReception = $('#modalAccuserReception');

    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');

    modalRechercheAvanceeMed = $('#modalRechercheAvanceeMed');
    selectService = $('#selectService');
    selectStateMed = $('#selectStateMed');

    inputDateDebutMed = $('#inputDateDebutMed');
    inputdateLastMed = $('#inputdateLastMed');

    datePickerMed1 = $('#datePickerMed1');
    datePickerMed2 = $('#datePickerMed2');
    btnAdvencedSearchMed = $('#btnAdvencedSearchMed');
    isAdvance = $('#isAdvance');

    lblService = $('#lblService');
    lblTypeMed = $('#lblTypeMed');
    lblStateMed = $('#lblStateMed');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    divSelectTypeMiseEnDemeure = $('#divSelectTypeMiseEnDemeure');

    divSelectTypeMiseEnDemeure.attr('style', 'display:none');

    modalFichePriseEnCharge = $('#modalFichePriseEnCharge');
    idDivServiceMed = $('#idDivServiceMed');

    //idDivServiceMed.attr('style','display:none');

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerMed1.datepicker("setDate", new Date());
    datePickerMed2.datepicker("setDate", new Date());

    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');

    modalInfoComplementAmr = $('#modalInfoComplementAmr');
    inputMotifAmr = $('#inputMotifAmr');
    inputDetailMotifAmr = $('#inputDetailMotifAmr');
    btnValider = $('#btnValider');

    inputDetailMotifAmr.val(empty);

    selectTypeMiseEnDemeure.on('change', function (e) {

        selectTypeMiseEnDemeureID = selectTypeMiseEnDemeure.val();

        if (selectTypeMiseEnDemeureID === '0') {

            alertify.alert('Veuillez sélectionner un type d\'une invitation à payer valide.');
            return;

        }
    });

    selectStateMed.on('change', function (e) {
        selectStateMedID = selectStateMed.val();
    });

    selectTypeSearch.on('change', function (e) {

        selectTypeSearchID = selectTypeSearch.val();
        valueTypeSearch.val(empty);

        switch (selectTypeSearchID) {
            case '1':
                valueTypeSearch.attr('readonly', true);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom du contribuable');
                break;
            case '2':
                valueTypeSearch.attr('readonly', false);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro de l\'invitation à payer');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');

                break;
        }
    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        onClickBtnAdvancedSearch = '1';
        modalRechercheAvanceeMed.modal('show');
    });

    btnValider.on('click', function (e) {
        e.preventDefault();
        validateInfoComplementAmr();
    });

    btnAdvencedSearchMed.on('click', function (e) {
        e.preventDefault();
        isAdvanced = '1';

        valueTypeSearch.val(empty);
        valueTypeSearch.attr('readonly', true);
        valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom du contribuable');
        loadInvitationApayer(isAdvanced);
    });

    btnSimpleSearch.on('click', function (e) {
        e.preventDefault();
        isAdvanced = '0';
        switch (selectTypeSearchID) {
            case '1':
                assujettiModal.modal('show');
                break;
            case '2':

                if (valueTypeSearch.val() === empty) {

                    alertify.alert('Veuillez saisir le numéro de l\'invitation à payer avant de lancer la recheche');
                    return;

                } else {
                    loadInvitationApayer(isAdvanced);
                }

                break;
        }

    });

    selectTypeSearchID = selectTypeSearch.val();
    selectTypeMiseEnDemeureID = selectTypeMiseEnDemeure.val();
    selectStateMedID = selectStateMed.val();

    //btnAdvencedSearchMed.trigger('click');

    printMiseEnDemeure(empty);

});

function loadInvitationApayer(isAdvanced) {

    switch (isAdvanced) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            //lblTypeMed.text($('#selectTypeMiseEnDemeure option:selected').text().toUpperCase());
            //lblTypeMed.attr('title', $('#selectTypeMiseEnDemeure option:selected').text().toUpperCase());

            lblStateMed.text($('#selectStateMed option:selected').text().toUpperCase());
            lblStateMed.attr('title', $('#selectStateMed option:selected').text().toUpperCase());

            if (selectService.val() === '*') {
                lblService.text('TOUS');
                lblService.attr('title', 'TOUS');
            } else {
                lblService.text($('#selectSite option:selected').text().toUpperCase());
                lblService.attr('title', $('#selectSite option:selected').text().toUpperCase());
            }

            lblDateDebut.text(inputDateDebut.val());
            lblDateDebut.attr('title', inputDateDebut.val());

            lblDateFin.text(inputdateLast.val());
            lblDateFin.attr('title', inputdateLast.val());

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': selectSite.val(),
            //'codeTypeMiseEnDemeure': selectTypeMiseEnDemeureID,
            'codeStateMiseEnDemeure': selectStateMedID,
            'dateDebut': inputDateDebutMed.val(),
            'dateFin': inputdateLastMed.val(),
            'valueSearch': selectTypeSearchID === '1' ? responsibleObject.codeResponsible : valueTypeSearch.val(),
            'advancedSearch': isAdvanced,
            'typeSearch': selectTypeSearchID,
            'codeSiteRec': userData.SiteCode,
            'operation': 'loadInvitationApayer'

        },
        beforeSend: function () {

            switch (isAdvanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
                case '1':
                    if (onClickBtnAdvancedSearch == '0') {
                        $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    } else {
                        modalRechercheAvanceeMed.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    }

                    break;
            }
        },
        success: function (response)
        {

            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':

                    if (onClickBtnAdvancedSearch == '0') {
                        $.unblockUI();
                    } else {
                        modalRechercheAvanceeMed.unblock();
                    }

                    break;
            }

            if (response == '-1') {
                printMiseEnDemeure('');
                showResponseError();
                return;
            } else if (response == '0') {

                if (isAdvanced === '1') {
                    modalRechercheAvanceeMed.modal('hide');
                    printMiseEnDemeure('');
                } else {
                    printMiseEnDemeure('');
                    alertify.alert('Aucune invitation à payer ne correspond au critère de recherches fournis');
                }

                switch (isAdvanced) {
                    case '0':
                        switch (selectTypeSearchID) {
                            case '0':
                                printMiseEnDemeure('');
                                alertify.alert('Ce contribuable : ' + valueTypeSearch.val() + ' n\'a pas d\'invitation à payer');
                                break;
                            case '1':
                                printMiseEnDemeure('');
                                alertify.alert('Ce numéro : ' + valueTypeSearch.val() + ' ne pas une référence valide d\'une invitation à payer');
                                break;
                        }
                        break;
                    case '1':
                        alertify.alert('Aucune invitation à payer ne correspond au critère de recherche fournis');
                        break;
                }
            } else {

                miseEnDemeureList = JSON.parse(JSON.stringify(response));

                if (isAdvanced === '1') {
                    modalRechercheAvanceeMed.modal('hide');
                }

                printMiseEnDemeure(miseEnDemeureList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    if (onClickBtnAdvancedSearch == '0') {
                        $.unblockUI();
                    } else {
                        modalRechercheAvanceeMed.unblock();
                    }
                    break;
            }
            showResponseError();
        }

    });
}

function getSelectedAssujetiData() {

    valueTypeSearch.val(selectAssujettiData.nomComplet);
    valueTypeSearch.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    loadInvitationApayer(isAdvanced);

}

function printMiseEnDemeure(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">NUMERO</th>';

    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:left">NTD</th>';
    tableContent += '<th style="text-align:left">CONTRIBUABLE</th>';
    tableContent += '<th style="text-align:left">DATE RECEPTION</th>';
    tableContent += '<th style="text-align:left">DATE ECHEANCE</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:center">AMR EXISTE ?</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var sumCDF = 0;
    var sumUSD = 0;

    for (var i = 0; i < result.length; i++) {

        var amount = 'PRINCIPAL : ' + '<span style="font-weight:bold">' + formatNumber(result[i].amountMed, result[i].deviseMed) + '</span>';
        var amountPenalite = 'PENALITE : ' + '<span style="font-weight:bold">' + formatNumber(result[i].amountPenaliteMed, result[i].deviseMed) + '</span>';

        var noteTaxationIAInfo = '';

        if (result[i].noteTaxationIA !== '') {
            noteTaxationIAInfo = '<hr/>' + 'NOTE DE TAXATION : ' + '<span style="font-weight:bold;color:green">' + result[i].noteTaxationIA + '</span>';
        }


        var btnAccuserReception = '';

        //var btnPrintInvitation = '&nbsp;<button class="btn btn-warning" onclick="printInvitationApayer(\'' + result[i].numeroMed + '\')"><i class="fa fa-print"></i></button>'
        var btnCallNextStap = '';
        var echeanceMedExist = '1';

        var btnText = '&nbsp;Ré-imprimer IA';

        if (result[i].dateEcheanceMed === empty) {
            echeanceMedExist = '0';
            btnText = '&nbsp;Accuser Réception IA';
        }

        btnAccuserReception = '<button class="btn btn-warning" onclick="accuserReception(\'' + echeanceMedExist + '\',\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')"><i class="fa fa-print"></i>' + btnText + '</button>';

        var color = 'green';

        var valueCheck = 'checked';

        if (result[i].activateNextStap === '1') {
            color = 'red';
            btnCallNextStap = '<br/><br/><button class="btn btn-primary" onclick="callModalInfoComplementAmr(\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\',\'' + result[i].noteTaxationIA + '\')"><i class="fa fa-check-circle"></i>&nbsp;Générer AMR</button>'
        }

        if (result[i].amrExist === '0') {
            valueCheck = '';
        } else {
            btnCallNextStap = '';
        }

        var statePayment = '';

        if (result[i].estPayer == '1') {
            statePayment = '<span style="color:green">' + 'payé'.toUpperCase() + '</span>';
        } else {
            statePayment = '<span style="color:red">' + 'non payé'.toUpperCase() + '</span>';
        }


        var btnGenerateNoteTaxationPenalite = '';

        if (result[i].amrExist == '0' && result[i].ntPenaliteExiste == '0') {

            btnGenerateNoteTaxationPenalite = '<br/><br/><button class="btn btn-default" onclick="callModalGenerateNoteTaxationPenalite(\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')"><i class="fa fa-check-circle"></i>&nbsp;Générer NTP</button>'

        }

        var btnOperation = btnAccuserReception + ' ' + btnCallNextStap + ' ' + btnGenerateNoteTaxationPenalite;

        if (result[i].deviseMed === 'CDF') {
            sumCDF += (result[i].amountMed + result[i].amountPenaliteMed);
        } else if (result[i].deviseMed === 'USD') {
            sumUSD += (result[i].amountMed + result[i].amountPenaliteMed);
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:7%;vertical-align:middle">' + result[i].numeroDocument + '</td>';
        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle">' + result[i].exerciceMed + '</td>';
        tableContent += '<td style="text-align:left;width:14%;vertical-align:middle">' + result[i].articleBudgetaireName + '<hr/>' + 'Période déclaration : ' + '<span style="font-weight:bold">' + result[i].referenceName + '</span>' + noteTaxationIAInfo + '</td>';
        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle">' + result[i].userName + '</td>';
        tableContent += '<td style="text-align:left;width:22%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:8%;vertical-align:middle">' + result[i].dateReceptionMed + '</td>';
        tableContent += '<td style="text-align:left;width:8%;font-weight:bold;vertical-align:middle;color:' + color + '">' + result[i].dateEcheanceMed + '</td>';
        tableContent += '<td style="text-align:right;width:14%;vertical-align:middle;font-weight:bold">' + amount + '<hr/>' + amountPenalite + '<hr/>' + statePayment + '</td>';
        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" disabled ' + valueCheck + '></td>';
        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle">' + btnOperation + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="7" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableMiseEnDemeure.html(tableContent);
    tableMiseEnDemeure.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 25,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(7).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD'));
        }
    });

}

function accuserReception(echeanceMedExist, medID, numeroDocument) {

    var typeMed = '';
    var refDocument;

    for (var i = 0; i < miseEnDemeureList.length; i++) {

        if (miseEnDemeureList[i].numeroMed === medID) {
            codeMedCurrent = miseEnDemeureList[i].numeroMed;
            refDocument = miseEnDemeureList[i].numeroDocument;
            typeMed = miseEnDemeureList[i].typeMed;
            codeTypeDoc = typeMed;
        }
    }

    if (echeanceMedExist == '0') {

        initAccuseReceptionUI(codeMedCurrent, typeMed);
        modalAccuserReception.modal('show');

    } else {

        var value = '<span style="font-weight:bold">' + numeroDocument + '</span>';
        var msg = 'Etes-vous sûre de vouloir ré-imprimer l\'invitation à payer n° ' + value + ' ?';

        alertify.confirm(msg, function () {

            $.ajax({
                type: 'POST',
                url: 'declaration_servlet',
                dataType: 'text',
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                crossDomain: true,
                data: {
                    'numero': medID,
                    'typeDoc': 'INVITATION_PAIEMENT',
                    'operation': 'printNoteTaxation'
                },
                beforeSend: function () {

                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

                },
                success: function (response)
                {

                    $.unblockUI();

                    if (response == '-1') {

                        $.unblockUI();
                        showResponseError();
                        return;
                    }

                    if (response == '0') {

                        $.unblockUI();
                        alertify.alert('Aucune invitation à payer trouvée correspondant à ce numéro : ' + value);
                        return;

                    }

                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');

                },
                complete: function () {
                },
                error: function (xhr, status, error) {
                    $.unblockUI();
                    showResponseError();
                }

            });
        });

    }
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function refrechDataAfterAccuserReception() {
    loadInvitationApayer(isAdvanced);
}


function callModalInfoComplementAmr(code, numeroDocument, noteTaxation) {

    alertify.confirm('Etes-vous sûre de vouloir fournir les informations complémentaire pour la création de l\'AMR ?', function () {

        medSelected = code;
        numeroDocumentSelected = numeroDocument;
        noteTaxationSelected = noteTaxation;
        
        inputMotifAmr.val(empty);
        inputDetailMotifAmr.val(empty);

        modalInfoComplementAmr.modal('show');

    });
}

function validateInfoComplementAmr() {

    if (inputMotifAmr.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir le motif de la création de l\'AMR');
        return;
    }

    if (inputDetailMotifAmr.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir les détails du motif de la création de l\'AMR');
        return;
    }
    var value = '<span style="font-weight:bold">' + numeroDocumentSelected + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir générer l\'AMR pour cette invitation à payer n° ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'poursuites_servlet',
            dataType: 'Text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'userId': userData.idUser,
                'numeroMed': medSelected,
                'valueMotif': inputMotifAmr.val(),
                'valueDetailMotif': inputDetailMotifAmr.val(),
                'noteTaxation': noteTaxationSelected,
                'operation': 'saveAndPrintAmr'

            },
            beforeSend: function () {
                modalInfoComplementAmr.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Génération de l\'AMR  en cours ...</h5>'});
            },
            success: function (response)
            {

                modalInfoComplementAmr.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    setTimeout(function () {
                        setDocumentContent(response);

                        setTimeout(function () {
                        }, 2000);
                        window.open('visualisation-document', '_blank');
                    }
                    , 1);
                    modalInfoComplementAmr.modal('hide');
                    loadInvitationApayer(isAdvanced);
                }


            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalInfoComplementAmr.unblock();
                showResponseError();
            }

        });
    });
}

function generateNoteTaxationPenalite(med) {

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'Json',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'userId': userData.idUser,
            'numeroMed': med,
            'operation': 'saveNoteTaxationPenalite'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Génération des notes de taxation de pénalités  en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' | response == '0') {

                showResponseError();
                return;

            } else {

                var result = JSON.parse(JSON.stringify(response));

                setDocumentContent(result.ntPenaliteProvince);
                window.open('visualisation-document', '_blank');

                setDocumentContent(result.ntPenaliteDrhKat);
                window.open('visualisation-document', '_blank');

                loadInvitationApayer(isAdvanced);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function callModalGenerateNoteTaxationPenalite(med, numeroDocument) {

    var value = '<span style="font-weight:bold">' + numeroDocument + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir générer les deux notes de pénalité pour cette invitation à payer n° ' + value + ' ?', function () {
        generateNoteTaxationPenalite(med);
    });
}
