/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var amrList = [];
var documentList = [];

var selectTypeSearch;
var selectTypeSearchID;
var valueTypeSearch;

var btnSimpleSearch;
var btnShowAdvancedSerachModal;
var isAdvance;
var tableAMR;
var isAdvanced;

var responsibleObject = new Object();
var modalAccuserReception;

var assujettiModal;
var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var modalRechercheAvanceeAmr;

var selectService, selectTypeAmr, selectStateAmr;
var inputDateStartAmr;
var inputdateEndAmr;

var selectServiceID;

var selectTypeAmrID, selectStateAmrID;

var dateStartPickerAmr;
var dateEndPickerAmr;

var btnAdvencedSearchAmr;

var codeAmrCurrent, amrDocumentPrint

var lblService, lblTypeAmr, lblStateAmr, lblDateDebut, lblDateFin;

var documentListModal, selectDocument, btnPrintDocument;
var modalComplementContrainteAndCommandement, cmbAgentHuissier, inputParquet, btnValiderComplementInfo;

var amrSelected;
var numeroMedSelected;
var numeroDocumentSelected;
var nombreMoisRetard;

$(function () {

    mainNavigationLabel.text('POURSUITES');
    secondNavigationLabel.text('Registre des avis de mise en recouvrement');
    removeActiveMenu();
    linkMenuContentieux.addClass('active');

    selectTypeSearch = $('#selectTypeSearch');
    valueTypeSearch = $('#valueTypeSearch');
    btnSimpleSearch = $('#btnSimpleSearch');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    tableAMR = $('#tableAMR');
    assujettiModal = $('#assujettiModal');
    modalAccuserReception = $('#modalAccuserReception');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');
    modalRechercheAvanceeAmr = $('#modalRechercheAvanceeAmr');

    selectService = $('#selectService');
    selectTypeAmr = $('#selectTypeAmr');
    selectStateAmr = $('#selectStateAmr');
    inputDateStartAmr = $('#inputDateStartAmr');
    inputdateEndAmr = $('#inputdateEndAmr');

    dateStartPickerAmr = $('#dateStartPickerAmr');
    dateEndPickerAmr = $('#dateEndPickerAmr');
    btnAdvencedSearchAmr = $('#btnAdvencedSearchAmr');

    isAdvance = $('#isAdvance');

    lblService = $('#lblService');
    lblTypeAmr = $('#lblTypeAmr');
    lblStateAmr = $('#lblStateAmr');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    documentListModal = $('#documentListModal');
    selectDocument = $('#selectDocument');
    btnPrintDocument = $('#btnPrintDocument');

    modalComplementContrainteAndCommandement = $('#modalComplementContrainteAndCommandement');
    cmbAgentHuissier = $('#cmbAgentHuissier');
    inputParquet = $('#inputParquet');
    btnValiderComplementInfo = $('#btnValiderComplementInfo');

    selectTypeSearch.on('change', function (e) {
        selectTypeSearchID = selectTypeSearch.val();
    });

    selectStateAmr.on('change', function (e) {

        if (selectStateAmr.val() === '1') {
            selectStateAmrID = '*';
        } else {
            selectStateAmrID = selectStateAmr.val();
        }

    });

    selectTypeAmr.on('change', function (e) {

        if (selectTypeAmr.val() === '1') {
            selectTypeAmrID = '*';
        } else {
            selectTypeAmrID = selectTypeAmr.val();
        }

    });

    selectService.on('change', function (e) {
        selectServiceID = selectService.val();
    });


    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    dateStartPickerAmr.datepicker("setDate", new Date());
    dateEndPickerAmr.datepicker("setDate", new Date());


    selectTypeSearch.on('change', function (e) {

        selectTypeSearchID = selectTypeSearch.val();
        valueTypeSearch.val(empty);

        switch (selectTypeSearchID) {
            case '1':
                valueTypeSearch.attr('readonly', false);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro AMR');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;
            case '2':
                valueTypeSearch.attr('readonly', true);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;
        }
    });

    btnValiderComplementInfo.on('click', function (e) {
        e.preventDefault();

        if (cmbAgentHuissier.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un agent huissier');
            return;
        }

        if (inputParquet.val() == '') {
            alertify.alert('Veuillez d\'abord saisir le parquet de grande instance');
            return;
        }

        callGenerateContrainte();
    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeAmr.modal('show');
    });

    btnPrintDocument.on('click', function (e) {
        e.preventDefault();
        printDocument();
    });

    btnAdvencedSearchAmr.on('click', function (e) {
        e.preventDefault();

        isAdvanced = '1';

        valueTypeSearch.val(empty);
        valueTypeSearch.attr('readonly', true);
        valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom du contribuable');
        valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
        loadAmr(isAdvanced);
    });

    btnSimpleSearch.on('click', function (e) {
        e.preventDefault();
        isAdvanced = '0';
        switch (selectTypeSearchID) {

            case '1':
                if (valueTypeSearch.val() === empty) {
                    alertify.alert('Veuillez saisir le numéro AMR');
                    return;
                } else {
                    loadAmr(isAdvanced);
                }
                break;
            case '2':
                assujettiModal.modal('show');
                break;
        }

    });

    selectTypeSearchID = selectTypeSearch.val();
    selectStateAmrID = selectStateAmr.val();
    selectTypeAmrID = selectTypeAmr.val();
    selectServiceID = selectService.val();

    getAgentHuissier();

    printAmr(empty);

});

function printDocument() {

    var typeDoc = '';
    var msg = '';

    for (var i = 0; i < documentList.length; i++) {

        if (documentList[i].referenceDocument == selectDocument.val()) {

            typeDoc = documentList[i].typeDocument;
            msg = 'Impression du document : ' + $('#selectDocument option:selected').text().toUpperCase() + '...';
        }
    }

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'referenceDocument': selectDocument.val(),
            'userId': userData.idUser,
            'typeDocument': typeDoc,
            'operation': 'printDocument'

        },
        beforeSend: function () {
            documentListModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>' + msg + '</h5>'});

        },
        success: function (response)
        {

            documentListModal.unblock();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            } else {

                setTimeout(function () {
                    setDocumentContent(response);

                    setTimeout(function () {
                    }, 2000);
                    window.open('visualisation-document', '_blank');
                }
                , 1);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            documentListModal.unblock();
            showResponseError();
        }
    });
}

function loadAmr(isAdvanced) {

    switch (isAdvanced) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            //lblTypeAmr.text($('#selectTypeAmr option:selected').text().toUpperCase());
            //lblTypeAmr.attr('title', $('#selectTypeAmr option:selected').text().toUpperCase());

            lblStateAmr.text($('#selectStateAmr option:selected').text().toUpperCase());
            lblStateAmr.attr('title', $('#selectStateAmr option:selected').text().toUpperCase());

            if (selectService.val() === '*') {
                lblService.text('TOUS');
                lblService.attr('title', 'TOUS');
            } else {
                lblService.text($('#selectSite option:selected').text().toUpperCase());
                lblService.attr('title', $('#selectSite option:selected').text().toUpperCase());
            }

            lblDateDebut.text(inputDateDebut.val());
            lblDateDebut.attr('title', inputDateDebut.val());

            lblDateFin.text(inputdateLast.val());
            lblDateFin.attr('title', inputdateLast.val());

            isAdvance.attr('style', 'display: block');
            break;
    }

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': selectSite.val(),
            //'codeTypeAmr': selectTypeAmrID,
            'codeStateAmr': selectStateAmrID,
            'dateDebut': inputDateStartAmr.val(),
            'dateFin': inputdateEndAmr.val(),
            'valueSearch': selectTypeSearchID === '2' ? responsibleObject.codeResponsible : valueTypeSearch.val(),
            'advancedSearch': isAdvanced,
            'typeSearch': selectTypeSearchID,
            'operation': 'loadAmr'

        },
        beforeSend: function () {
            switch (isAdvanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
                case '1':
                    modalRechercheAvanceeAmr.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
            }

        },
        success: function (response)
        {

            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeAmr.unblock();
                    break;
            }

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                if (isAdvanced === '1') {
                    modalRechercheAvanceeAmr.modal('hide');
                }

                switch (isAdvanced) {
                    case '0':
                        switch (selectTypeSearchID) {
                            case '4':
                                printAmr(empty);
                                alertify.alert('Ce contribuable : ' + valueTypeSearch.val() + ' n\'a pas d\'avis de mise en demeure (AMR)');
                                break;
                            default:
                                printAmr(empty);
                                alertify.alert('Ce numéro : ' + valueTypeSearch.val() + ' ne pas une référence valide d\'un mise en recouvrement (AMR)');
                                break;
                        }
                        break;
                    case '1':
                        alertify.alert('Aucun avis de mise en recouvrement ne correspond au critère de recherche fournis');
                        break;
                }
            } else {

                amrList = JSON.parse(JSON.stringify(response));

                if (isAdvanced === '1') {
                    modalRechercheAvanceeAmr.modal('hide');
                }
                printAmr(amrList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeAmr.unblock();
                    break;
            }
            showResponseError();
        }

    });
}

function printAmr(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">NUMERO</th>';
    tableContent += '<th style="text-align:left">CONTRIBUABLE</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:left">DATE RECEPTION</th>';
    tableContent += '<th style="text-align:left">DATE ECHEANCE</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ CUMULE</th>';
    tableContent += '<th style="text-align:center">EST EN POURSUITE ?</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var sumCDF = 0;
    var sumUSD = 0;

    for (var i = 0; i < result.length; i++) {

        var amountPrincipal = formatNumber(result[i].amountPrincipalAmr, result[i].deviseAmr);
        var amountPenalite1 = formatNumber((result[i].amountPenalite / 2), result[i].deviseAmr);
        var amountPenalite2 = formatNumber((result[i].amountPenalite / 2), result[i].deviseAmr);

        var amountPrincipalCumule = formatNumber((result[i].amountPrincipalAmr + result[i].amountPenalite), result[i].deviseAmr);

        if (result[i].deviseAmr === 'CDF') {
            sumCDF += (result[i].amountPrincipalAmr + result[i].amountPenalite);
        } else if (result[i].deviseAmr === 'USD') {
            sumUSD += (result[i].amountPrincipalAmr + result[i].amountPenalite);
        }

        var buttonAmr = '';
        var buttonCallNextStap = '';
        var buttonCallGenerateContrainte = '';
        var valueCheck = 'checked';


        if (result[i].poursuiteExist === '0') {
            valueCheck = '';
            buttonCallGenerateContrainte = '&nbsp;<button class="btn btn-primary" onclick="callModalGenerateContrainteAndCommandement(\'' + result[i].numeroAmr + '\',\'' + result[i].nombreMoisRetard + '\',\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')"><i class="fa fa-list"></i></button>'
        }

        if (result[i].dateLimiteAmr === empty) {

            echeanceMedExist = '1';
        } else {
            echeanceMedExist = '2';
        }

        var size1 = '';
        var size2 = '';
        var color = '';

        var colorEcheance = '';

        if (result[i].activateNextStap == '1') {
            colorEcheance = 'red';
        }

        if (result[i].activateNextStap === '1') {

            size1 = '20%';
            size2 = '8%';
            color = 'red';
            colorEcheance = 'red';

            buttonCallNextStap = buttonCallGenerateContrainte;

        } else {
            size1 = '25%';
            size2 = '5%';
            color = 'green';
            colorEcheance = 'green';
        }

        buttonAmr = '<button class="btn btn-warning" onclick="accuserReception(\'' + echeanceMedExist + '\',\'' + result[i].numeroAmr + '\')"><i class="fa fa-print"></i></button>';

        var articleName = '<span style="font-weight:bold">' + result[i].articleBudgetaireName + '</span>';
        var periodeName = 'Période Déclaration : ' + '<span style="font-weight:bold">' + result[i].periodeDeclarationName + '</span>';

        var articleBudgetaireInfo = articleName + '<hr/>' + periodeName;

        var amountPrincipalInfo = 'Montant principal : ' + '<span style="font-weight:bold">' + amountPrincipal + '</span>';
        var amountPenalite1Info = 'Montant pénalité 50% (PROVINCE) : ' + '<span style="font-weight:bold">' + amountPenalite1 + '</span>';
        var amountPenalite2Info = 'Montant pénalité 50% (DRHKAT) : ' + '<span style="font-weight:bold">' + amountPenalite2 + '</span>';

        var montantDuInfo = amountPrincipalInfo + '<hr/>' + amountPenalite1Info + '<hr/>' + amountPenalite2Info;

        var statePayment;

        if (result[i].paiementExist == '1') {


            buttonCallNextStap = empty;

            statePayment = '<span style="font-weight:bold;color:green">' + "payé".toUpperCase() + '</span>';
        } else {
            statePayment = '<span style="font-weight:bold;color:red">' + "non payé".toUpperCase() + '</span>';
        }

        var amountPrincipalCumuleInfo = '<span style="font-weight:bold">' + amountPrincipalCumule + '<hr/>' + statePayment + '</span>';

        var btnOperation = buttonAmr + ' ' + buttonCallNextStap;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:8%;vertical-align:middle">' + result[i].numeroDocument + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + articleBudgetaireInfo + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + result[i].dateReceptionAmr + '</td>';
        tableContent += '<td style="text-align:left;width:10%;font-weight:bold;vertical-align:middle;color:' + colorEcheance + '">' + result[i].dateLimiteAmr + '</td>';
        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle;font-weight:normal">' + montantDuInfo + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:normal">' + amountPrincipalCumuleInfo + '</td>';
        tableContent += '<td style="text-align:center;width:8%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" id="checkboxEnPoursuite" disabled ' + valueCheck + '></td>';

        tableContent += '<td style="text-align:center;width:' + size2 + ';vertical-align:middle">' + btnOperation + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="6" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableAMR.html(tableContent);
    tableAMR.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 25,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(6).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD'));
        }
    });

}

function accuserReception(echeanceMedExist, amrID) {

    var typeAmr = '';
    var amount = '';
    var numberMonth = 0;
    var refDocument;

    for (var i = 0; i < amrList.length; i++) {

        if (amrList[i].numeroAmr === amrID) {
            codeAmrCurrent = amrList[i].numeroAmr;
            amrDocumentPrint = amrList[i].documentPrint;
            typeAmr = amrList[i].typeAmr;
            codeTypeDoc = typeAmr;
            amount = amrList[i].amountPrincipalAmr;
            numberMonth = amrList[i].nombreMoisRetard;
            refDocument = amrList[i].numeroDocument;

        }
    }

    if (echeanceMedExist == '1') {

        initAccuseReceptionUI(codeAmrCurrent, typeAmr);

        modalAccuserReception.modal('show');

    } else {

        var value = '<span style="font-weight:bold">' + refDocument + '</span>';
        var msg = 'Etes-vous sûre de vouloir ré-imprimer l\'AMR n° ' + value + ' ?';

        alertify.confirm(msg, function () {

            $.ajax({
                type: 'POST',
                //url: 'poursuites_servlet',
                url: 'declaration_servlet',
                dataType: 'text',
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                crossDomain: false,
                data: {
                    'numero': amrID,
                    //'numeroAmr': amrID,
                    //'userId': userData.idUser,
                    'operation': 'printNoteTaxation'

                },
                beforeSend: function () {
                    switch (isAdvanced) {
                        case '0':
                            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                            break;
                        case '1':
                            modalRechercheAvanceeAmr.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                            break;
                    }

                },
                success: function (response)
                {

                    switch (isAdvanced) {
                        case '0':
                            $.unblockUI();
                            break;
                        case '1':
                            modalRechercheAvanceeAmr.unblock();
                            break;
                    }

                    if (response == '-1' || response == '0') {
                        showResponseError();
                        return;
                    } else {

                        setTimeout(function () {

                            setDocumentContent(response);

                            loadAmr(isAdvanced);

                            setTimeout(function () {
                            }, 2000);
                            window.open('visualisation-document', '_blank');
                        }
                        , 1);
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    switch (isAdvanced) {
                        case '0':
                            $.unblockUI();
                            break;
                        case '1':
                            modalRechercheAvanceeAmr.unblock();
                            break;
                    }
                    showResponseError();
                }

            });



        });


    }
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function refrechDataAfterAccuserReception() {
    loadAmr(isAdvanced);
}

function callGenerateContrainte() {

    var value = '<span style="font-weight:bold">' + numeroDocumentSelected + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir générer la contrainte et commandemenr pour cet AMR n° ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'poursuites_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numeroAmr': amrSelected,
                'userId': userData.idUser,
                'numeroMed': numeroMedSelected,
                'codeAgentHuissier': $('#cmbAgentHuissier option:selected').text(),
                'parquetName': inputParquet.val(),
                'monthLater': nombreMoisRetard,
                'operation': 'generateContrainte'
            },
            beforeSend: function () {
                modalComplementContrainteAndCommandement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Génération de la contrainte et commandement...</h5>'});

            },
            success: function (response)
            {

                modalComplementContrainteAndCommandement.unblock();

                if (response == '-1' || response == '0') {
                    showResponseError();
                    return;
                } else {

                    var document = JSON.parse(JSON.stringify(response));

                    setTimeout(function () {
                        setDocumentContent(document.contrainteHtml);

                        setTimeout(function () {
                        }, 1000);
                        window.open('visualisation--document', '_blank');
                    }
                    , 1);

                    setTimeout(function () {
                        setDocumentContent(document.commandementHtml);

                        setTimeout(function () {
                        }, 1000);
                        window.open('visualisation-document', '_blank');
                    }
                    , 1);

                    modalComplementContrainteAndCommandement.modal('hide');

                    loadAmr(isAdvanced);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalComplementContrainteAndCommandement.unblock();
                showResponseError();
            }
        });
    });

}

function callModalGenerateContrainteAndCommandement(amrID, monthLater, numeroMed, numeroDocument) {

    amrSelected = amrID;
    nombreMoisRetard = monthLater;
    numeroMedSelected = numeroMed;
    numeroDocumentSelected = numeroDocument;

    alertify.confirm('Etes-vous sûre de vouloir poursuivre cette opération ?', function () {
        cmbAgentHuissier.val('0');
        inputParquet.val(empty);
        modalComplementContrainteAndCommandement.modal('show');
    });

}

function getSelectedAssujetiData() {

    valueTypeSearch.val(selectAssujettiData.nomComplet);
    valueTypeSearch.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    loadAmr(isAdvanced);

}

function getAgentHuissier() {


    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': 'loadAgentHuissier'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            } else {

                var dataAgent = '';

                var data = JSON.parse(JSON.stringify(response));

                dataAgent += '<option value="0">--</option>';

                for (var i = 0; i < data.length; i++) {
                    dataAgent += '<option value="' + data[i].code + '">' + data[i].nom + '</option>';

                }
                cmbAgentHuissier.html(dataAgent);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });

}