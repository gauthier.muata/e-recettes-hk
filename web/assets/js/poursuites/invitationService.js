/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var miseEnDemeureList = [];

var selectTypeSearch;
var valueTypeSearch;
var selectTypeMiseEnDemeure;

var selectTypeSearchID;
var selectTypeMiseEnDemeureID;
var selectStateMedID;
var btnSimpleSearch;
var isAdvanced;
var onClickBtnAdvancedSearch;

var responsibleObject = new Object();
var tableMiseEnDemeure;
var modalAccuserReception;

var codeTypeDoc = 'INVITATION_SERVICE';
var codeDoc = undefined;

var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var modalRechercheAvanceeMed;
var selectService;
var selectStateMed;

var inputDateDebutMed;
var inputdateLastMed;

var datePickerMed1;
var datePickerMed2;

var btnShowAdvancedSerachModal;
var btnAdvencedSearchMed;
var codeMedCurrent;

var isAdvance;

var lblService, lblTypeMed, lblStateMed, lblDateDebut, lblDateFin;

var modalFichePriseEnCharge;

var periodeIDSelected;
var fromCall;

var penalitieList = [];

var amountPenalite;
var amountPenaliteFormat;
var medIDSelected;
var numberRows;
var deviseSelected;
var dateDebuts, dateFins;
var divSelectTypeMiseEnDemeure;
var modalInfoComplementAmr, inputMotifAmr, inputDetailMotifAmr, btnValider;

var medSelected;

$(function () {

    fromCall = '0';
    numberRows = empty;
    onClickBtnAdvancedSearch = '0';

    mainNavigationLabel.text('POURSUITES');
    secondNavigationLabel.text('Registre des invitations de service');
    removeActiveMenu();
    linkMenuContentieux.addClass('active');

    selectTypeSearch = $('#selectTypeSearch');
    valueTypeSearch = $('#valueTypeSearch');
    selectTypeMiseEnDemeure = $('#selectTypeMiseEnDemeure');
    btnSimpleSearch = $('#btnSimpleSearch');
    tableMiseEnDemeure = $('#tableMiseEnDemeure');
    modalAccuserReception = $('#modalAccuserReception');

    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');

    modalRechercheAvanceeMed = $('#modalRechercheAvanceeMed');
    selectService = $('#selectService');
    selectStateMed = $('#selectStateMed');

    inputDateDebutMed = $('#inputDateDebutMed');
    inputdateLastMed = $('#inputdateLastMed');

    datePickerMed1 = $('#datePickerMed1');
    datePickerMed2 = $('#datePickerMed2');
    btnAdvencedSearchMed = $('#btnAdvencedSearchMed');
    isAdvance = $('#isAdvance');

    lblService = $('#lblService');
    lblTypeMed = $('#lblTypeMed');
    lblStateMed = $('#lblStateMed');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    divSelectTypeMiseEnDemeure = $('#divSelectTypeMiseEnDemeure');

    divSelectTypeMiseEnDemeure.attr('style', 'display:none');

    modalFichePriseEnCharge = $('#modalFichePriseEnCharge');

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerMed1.datepicker("setDate", new Date());
    datePickerMed2.datepicker("setDate", new Date());

    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');

    modalInfoComplementAmr = $('#modalInfoComplementAmr');
    inputMotifAmr = $('#inputMotifAmr');
    inputDetailMotifAmr = $('#inputDetailMotifAmr');
    btnValider = $('#btnValider');

    inputDetailMotifAmr.val(empty);

    selectTypeMiseEnDemeure.on('change', function (e) {

        selectTypeMiseEnDemeureID = selectTypeMiseEnDemeure.val();

        if (selectTypeMiseEnDemeureID === '0') {

            alertify.alert('Veuillez sélectionner un type de mise en demeure valide.');
            return;

        }
    });

    selectStateMed.on('change', function (e) {
        selectStateMedID = selectStateMed.val();
    });

    selectTypeSearch.on('change', function (e) {

        selectTypeSearchID = selectTypeSearch.val();
        valueTypeSearch.val(empty);

        switch (selectTypeSearchID) {
            case '1':
                valueTypeSearch.attr('readonly', true);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
                break;
            case '2':
                valueTypeSearch.attr('readonly', false);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro de l\'invitation à payer');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');

                break;
        }
    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        onClickBtnAdvancedSearch = '1';
        modalRechercheAvanceeMed.modal('show');
    });

    btnValider.on('click', function (e) {
        e.preventDefault();
        validateInfoComplementAmr();
    });

    btnAdvencedSearchMed.on('click', function (e) {
        e.preventDefault();
        isAdvanced = '1';

        valueTypeSearch.val(empty);
        valueTypeSearch.attr('readonly', true);
        valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
        loadInvitationService(isAdvanced);
    });

    btnSimpleSearch.on('click', function (e) {
        e.preventDefault();
        isAdvanced = '0';
        switch (selectTypeSearchID) {
            case '1':
                assujettiModal.modal('show');
                break;
            case '2':

                if (valueTypeSearch.val() === empty) {

                    alertify.alert('Veuillez saisir le numéro de l\'invitation à payer avant de lancer la recheche');
                    return;

                } else {
                    loadInvitationService(isAdvanced);
                }

                break;
        }

    });

    selectTypeSearchID = selectTypeSearch.val();
    selectTypeMiseEnDemeureID = selectTypeMiseEnDemeure.val();
    selectStateMedID = selectStateMed.val();

    //btnAdvencedSearchMed.trigger('click');

    printMiseEnDemeure(empty);

});

function loadInvitationService(isAdvanced) {

    switch (isAdvanced) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            //lblTypeMed.text($('#selectTypeMiseEnDemeure option:selected').text().toUpperCase());
            //lblTypeMed.attr('title', $('#selectTypeMiseEnDemeure option:selected').text().toUpperCase());

            lblStateMed.text($('#selectStateMed option:selected').text().toUpperCase());
            lblStateMed.attr('title', $('#selectStateMed option:selected').text().toUpperCase());

            if (selectService.val() === '*') {
                lblService.text('TOUS');
                lblService.attr('title', 'TOUS');
            } else {
                lblService.text($('#selectService option:selected').text().toUpperCase());
                lblService.attr('title', $('#selectService option:selected').text().toUpperCase());
            }

            lblDateDebut.text(inputDateDebut.val());
            lblDateDebut.attr('title', inputDateDebut.val());

            lblDateFin.text(inputdateLast.val());
            lblDateFin.attr('title', inputdateLast.val());

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': selectService.val(),
            //'codeTypeMiseEnDemeure': selectTypeMiseEnDemeureID,
            'codeStateMiseEnDemeure': selectStateMedID,
            'dateDebut': inputDateDebutMed.val(),
            'dateFin': inputdateLastMed.val(),
            'valueSearch': selectTypeSearchID === '1' ? responsibleObject.codeResponsible : valueTypeSearch.val(),
            'advancedSearch': isAdvanced,
            'typeSearch': selectTypeSearchID,
            'operation': 'loadInvitationService'

        },
        beforeSend: function () {

            switch (isAdvanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
                case '1':
                    if (onClickBtnAdvancedSearch == '0') {
                        $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    } else {
                        modalRechercheAvanceeMed.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    }

                    break;
            }
        },
        success: function (response)
        {

            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':

                    if (onClickBtnAdvancedSearch == '0') {
                        $.unblockUI();
                    } else {
                        modalRechercheAvanceeMed.unblock();
                    }

                    break;
            }

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                if (isAdvanced === '1') {
                    modalRechercheAvanceeMed.modal('hide');
                }

                switch (isAdvanced) {
                    case '0':
                        switch (selectTypeSearchID) {
                            case '0':
                                printMiseEnDemeure('');
                                alertify.alert('Cet assujetti : ' + valueTypeSearch.val() + ' n\'a pas d\'invitation à payer');
                                break;
                            case '1':
                                printMiseEnDemeure('');
                                alertify.alert('Ce numéro : ' + valueTypeSearch.val() + ' ne pas une référence valide d\'une invitation à payer');
                                break;
                        }
                        break;
                    case '1':
                        alertify.alert('Aucune invitation à payer ne correspond au critère de recherche fournis');
                        break;
                }
            } else {

                miseEnDemeureList = JSON.parse(JSON.stringify(response));

                if (isAdvanced === '1') {
                    modalRechercheAvanceeMed.modal('hide');
                }

                printMiseEnDemeure(miseEnDemeureList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    if (onClickBtnAdvancedSearch == '0') {
                        $.unblockUI();
                    } else {
                        modalRechercheAvanceeMed.unblock();
                    }
                    break;
            }
            showResponseError();
        }

    });
}

function getSelectedAssujetiData() {

    valueTypeSearch.val(selectAssujettiData.nomComplet);
    valueTypeSearch.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    loadInvitationService(isAdvanced);

}

function printMiseEnDemeure(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">NUMERO</th>';

    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:left">NTD</th>';
    tableContent += '<th style="text-align:left">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left">DATE RECEPTION</th>';
    tableContent += '<th style="text-align:left">DATE ECHEANCE</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:right">PENALITE DÛ</th>';
    tableContent += '<th style="text-align:center">AMR EXISTE ?</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var amount = formatNumber(result[i].amountMed, result[i].deviseMed);
        var amountPenalite = formatNumber(result[i].amountPenaliteMed, result[i].deviseMed);

        var btnAccuserReception = '';

        //var btnPrintInvitation = '&nbsp;<button class="btn btn-warning" onclick="printInvitationApayer(\'' + result[i].numeroMed + '\')"><i class="fa fa-print"></i></button>'
        var btnCallNextStap = '';
        var echeanceMedExist = '1';

        if (result[i].dateEcheanceMed === empty) {
            echeanceMedExist = '0';
        }

        btnAccuserReception = '<button class="btn btn-warning" onclick="accuserReception(\'' + echeanceMedExist + '\',\'' + result[i].numeroMed + '\')"><i class="fa fa-print"></i></button>';

        var color = 'green';

        var valueCheck = 'checked';

        if (result[i].activateNextStap === '1') {
            color = 'red';
            btnCallNextStap = '&nbsp;<button class="btn btn-primary" onclick="callModalInfoComplementAmr(\'' + result[i].numeroMed + '\')"><i class="fa fa-check-circle"></i></button>'
        }

        if (result[i].amrExist === '0') {
            valueCheck = '';
        } else {
            btnCallNextStap = '';
        }

        var btnOperation = btnAccuserReception + btnCallNextStap;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:7%;vertical-align:middle">' + result[i].numeroDocument + '</td>';
        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle">' + result[i].exerciceMed + '</td>';
        tableContent += '<td style="text-align:left;width:14%;vertical-align:middle">' + result[i].articleBudgetaireName + '<hr/>' + 'Période : ' + result[i].referenceName + '</td>';
        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle">' + result[i].userName + '</td>';
        tableContent += '<td style="text-align:left;width:19%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:8%;vertical-align:middle">' + result[i].dateReceptionMed + '</td>';
        tableContent += '<td style="text-align:left;width:8%;font-weight:bold;vertical-align:middle;color:' + color + '">' + result[i].dateEcheanceMed + '</td>';
        tableContent += '<td style="text-align:right;width:8%;vertical-align:middle;font-weight:bold">' + amount + '</td>';
        tableContent += '<td style="text-align:right;width:8%;vertical-align:middle;font-weight:bold">' + amountPenalite + '</td>';
        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" disabled ' + valueCheck + '></td>';
        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle">' + btnOperation + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableMiseEnDemeure.html(tableContent);
    tableMiseEnDemeure.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 4
    });

}

function accuserReception(echeanceMedExist, medID) {

    var typeMed = '';

    for (var i = 0; i < miseEnDemeureList.length; i++) {

        if (miseEnDemeureList[i].numeroMed === medID) {
            codeMedCurrent = miseEnDemeureList[i].numeroMed;
            medDocumentPrint = miseEnDemeureList[i].documentPrint;
            typeMed = miseEnDemeureList[i].typeMed;
            codeTypeDoc = typeMed;
        }
    }

    if (echeanceMedExist == '0') {

        initAccuseReceptionUI(codeMedCurrent, typeMed);
        modalAccuserReception.modal('show');

    } else {

        var msg = 'Etes-vous sûre de vouloir réimprimer l\'invitation à payer ?';

        alertify.confirm(msg, function () {

            setTimeout(function () {
                setDocumentContent(medDocumentPrint);

                setTimeout(function () {
                }, 2000);
                window.open('visualisation-document', '_blank');
            }
            , 1);
        });
    }
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function refrechDataAfterAccuserReception() {
    loadInvitationService(isAdvanced);
}


function callModalInfoComplementAmr(code) {

    alertify.confirm('Etes-vous sûre de vouloir fournir les informations complémentaire pour la création de l\'AMR ?', function () {

        medSelected = code;

        inputMotifAmr.val(empty);
        inputDetailMotifAmr.val(empty);

        modalInfoComplementAmr.modal('show');

    });
}

function validateInfoComplementAmr() {

    if (inputMotifAmr.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir le motif de la création de l\'AMR');
        return;
    }

    if (inputDetailMotifAmr.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir les détails du motif de la création de l\'AMR');
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir générer l\'AMR pour cette invitation à payer n° ' + medSelected + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'poursuites_servlet',
            dataType: 'Text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'userId': userData.idUser,
                'numeroMed': medSelected,
                'valueMotif': inputMotifAmr.val(),
                'valueDetailMotif': inputDetailMotifAmr.val(),
                'operation': 'saveAndPrintAmr'

            },
            beforeSend: function () {
                modalInfoComplementAmr.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Génération de l\'AMR  en cours ...</h5>'});
            },
            success: function (response)
            {

                modalInfoComplementAmr.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    setTimeout(function () {
                        setDocumentContent(response);

                        setTimeout(function () {
                        }, 2000);
                        window.open('visualisation-document', '_blank');
                    }
                    , 1);
                    modalInfoComplementAmr.modal('hide');
                    loadInvitationService(isAdvanced);
                }


            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalInfoComplementAmr.unblock();
                showResponseError();
            }

        });
    });
}