/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var commandementList = [];
var responsibleObject = new Object();

var btnSimpleSearch;
var selectTypeSearch;
var selectTypeSearchID;
var valueTypeSearch;

var modalAccuserReception;

var assujettiModal;
var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var dateStartPickerAmr;
var dateEndPickerAmr;
var btnAdvencedSearchAmr;

var lblService, lblTypeContrainte, lblStateAmr, lblDateDebut, lblDateFin;
var selectServiceID;

var btnShowAdvancedSerachModal;
var tableCommandement;

var codeCommandementCurrent;
var commandementDocumentPrint = '';
var bonApayerDocumentPrint = '';

var documentListModal, selectDocument, btnPrintDocument;

var documentHtmlPrint;

var labelType, lblStateAmr, selectStateAmr;
var isAdvance;
var fromModalCall;
var contrainteSelected;
var redevableId;

var lblTypeAmr;

$(function () {

    mainNavigationLabel.text('POURSUITES');
    secondNavigationLabel.text('Registre des commandements');
    removeActiveMenu();
    linkMenuContentieux.addClass('active');


    btnSimpleSearch = $('#btnSimpleSearch');
    selectTypeSearch = $('#selectTypeSearch');
    valueTypeSearch = $('#valueTypeSearch');

    modalAccuserReception = $('#modalAccuserReception');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');
    assujettiModal = $('#assujettiModal');

    selectService = $('#selectService');
    selectTypeAmr = $('#selectTypeAmr');
    selectStateAmr = $('#selectStateAmr');
    inputDateStartAmr = $('#inputDateStartAmr');
    inputdateEndAmr = $('#inputdateEndAmr');

    dateStartPickerAmr = $('#dateStartPickerAmr');
    dateEndPickerAmr = $('#dateEndPickerAmr');
    btnAdvencedSearchAmr = $('#btnAdvencedSearchAmr');

    modalRechercheAvanceeAmr = $('#modalRechercheAvanceeAmr');
    lblTypeAmr = $('#lblTypeAmr');

    isAdvance = $('#isAdvance');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');

    lblService = $('#lblService');
    lblTypeContrainte = $('#lblTypeContrainte');
    lblStateAmr = $('#lblStateAmr');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    labelType = $('#labelType');
    lblStateAmr = $('#lblStateAmr');

    tableCommandement = $('#tableCommandement');

    documentListModal = $('#documentListModal');
    selectDocument = $('#selectDocument');
    btnPrintDocument = $('#btnPrintDocument');

    selectService.on('change', function (e) {
        selectServiceID = selectService.val();
    });


    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    dateStartPickerAmr.datepicker("setDate", new Date());
    dateEndPickerAmr.datepicker("setDate", new Date());

    selectTypeSearch.on('change', function (e) {

        selectTypeSearchID = selectTypeSearch.val();
        valueTypeSearch.val(empty);

        switch (selectTypeSearchID) {
            case '1':
                valueTypeSearch.attr('readonly', false);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro de la contrainte');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;
            case '2':
                valueTypeSearch.attr('readonly', true);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;
        }


    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeAmr.modal('show');
    });

    btnSimpleSearch.on('click', function (e) {
        e.preventDefault();
        isAdvanced = '0';
        switch (selectTypeSearchID) {

            case '1':
                if (valueTypeSearch.val() === empty) {
                    alertify.alert('Veuillez saisir le numéro du commandement');
                    return;
                } else {
                    loadCommandement(isAdvanced);
                }

                break;
            case '2':
                fromModalCall = '1';
                assujettiModal.modal('show');
                break;
            default:
                loadCommandement(isAdvanced);
                break;
        }

    });

    btnAdvencedSearchAmr.on('click', function (e) {
        e.preventDefault();

        isAdvanced = '1';
        selectTypeSearch.val('1');
        valueTypeSearch.val(empty);
        valueTypeSearch.attr('readonly', false);
        valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro du commandement');
        valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
        loadCommandement(isAdvanced);
    });


    lblTypeAmr.attr('style', 'display:none');
    selectTypeAmr.attr('style', 'display:none');
    lblTypeContrainte.attr('style', 'display:none');

    lblStateAmr.text('Type :');

    var dataselectStateAmr = '';

    selectStateAmr.html(empty);

    dataselectStateAmr += '<option value="1">Tous</option>';
    dataselectStateAmr += '<option value="2">Echus</option>';
    dataselectStateAmr += '<option value="3">Non Echus</option>';

    selectStateAmr.html(dataselectStateAmr);

    selectTypeSearchID = selectTypeSearch.val();
    printCommandement(empty);

});

function loadCommandement(isAdvanced) {

    switch (isAdvanced) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            labelType.text($('#selectStateAmr option:selected').text().toUpperCase());
            labelType.attr('title', $('#selectStateAmr option:selected').text().toUpperCase());

            if (selectService.val() === '*') {
                lblService.text('TOUS');
                lblService.attr('title', 'TOUS');
            } else {
                lblService.text($('#selectService option:selected').text().toUpperCase());
                lblService.attr('title', $('#selectService option:selected').text().toUpperCase());
            }

            lblDateDebut.text(inputDateDebut.val());
            lblDateDebut.attr('title', inputDateDebut.val());

            lblDateFin.text(inputdateLast.val());
            lblDateFin.attr('title', inputdateLast.val());

            isAdvance.attr('style', 'display: block');
            break;
    }

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': selectServiceID,
            'codeTypeContrainte': selectStateAmr.val(),
            'dateDebut': inputDateStartAmr.val(),
            'dateFin': inputdateEndAmr.val(),
            'valueSearch': selectTypeSearchID === '2' ? responsibleObject.codeResponsible : valueTypeSearch.val(),
            'advancedSearch': isAdvanced,
            'typeSearch': selectTypeSearchID,
            'operation': 'loadCommandement'
        },
        beforeSend: function () {
            switch (isAdvanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
                case '1':
                    modalRechercheAvanceeAmr.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
            }

        },
        success: function (response)
        {

            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeAmr.unblock();
                    break;
            }

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                if (isAdvanced === '1') {
                    modalRechercheAvanceeAmr.modal('hide');
                }

                switch (isAdvanced) {
                    case '0':
                        switch (selectTypeSearchID) {
                            case '2':
                                printCommandement(empty);
                                alertify.alert('Cet assujetti : ' + valueTypeSearch.val() + ' n\'a pas de commandement');
                                break;
                            default:
                                printCommandement(empty);
                                alertify.alert('Ce numéro : ' + valueTypeSearch.val() + ' ne pas une référence valide d\'un commandement');
                                break;
                        }
                        break;
                    case '1':
                        printCommandement(empty);
                        alertify.alert('Aucun commandement ne correspond au critère de recherche fournis');
                        break;
                }
            } else {

                commandementList = JSON.parse(JSON.stringify(response));

                if (isAdvanced === '1') {
                    modalRechercheAvanceeAmr.modal('hide');
                }
                printCommandement(commandementList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeAmr.unblock();
                    break;
            }
            showResponseError();
        }

    });
}


function getSelectedAssujetiData() {

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    switch (fromModalCall) {

        case '1':

            valueTypeSearch.val(selectAssujettiData.nomComplet);
            valueTypeSearch.attr('style', 'font-weight:bold;width: 380px');

            loadCommandement(isAdvanced);

            break;

        case '2':

            if (selectAssujettiData.code === redevableId) {

                alertify.alert('Déoler, le redevable n\'est doit pas être lui-même tier détenteur.');
            } else {
                saveAtd(selectAssujettiData.code);
            }

            break;
    }


}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function refrechDataAfterAccuserReception() {
    loadCommandement(isAdvanced);
}

function printCommandement(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">NUMERO</th>';
    tableContent += '<th style="text-align:left">INFOS CONTRAINTE</th>';

    tableContent += '<th style="text-align:left">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';

    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';

    tableContent += '<th style="text-align:left">DATE RECEPTION</th>';
    tableContent += '<th style="text-align:left">DATE ECHEANCE</th>';

    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';
    
    var sumCDF = 0;
    var sumUSD = 0;
    
    for (var i = 0; i < result.length; i++) {

        var amountPoursuivi = formatNumber(result[i].amountPoursuivi, result[i].devise);
        var amountFraisPoursuite = formatNumber(result[i].fraisPoursuite, result[i].devise);
        var amountTotal = formatNumber((result[i].amountPoursuivi + result[i].fraisPoursuite), result[i].devise);
        
        if (result[i].devise === 'CDF') {
            sumCDF += (result[i].amountPoursuivi + result[i].fraisPoursuite);
        } else if (result[i].devise === 'USD') {
            sumUSD += (result[i].amountPoursuivi + result[i].fraisPoursuite);
        }

        var btnPrintCommandement = '<button class="btn btn-warning" onclick="printCommandementDoucment(\'' + result[i].numeroCommandement + '\',\'' + result[i].numeroDocument + '\')"><i class="fa fa-print"></i></button>';

        var articleName = '<span style="font-weight:bold">' + result[i].faitGenerateur + '</span>';
        var periodeName = 'Période Déclaration : ' + '<span style="font-weight:bold">' + result[i].periodeDeclaration + '</span>';

        var articleBudgetaireInfo = articleName + '<hr/>' + periodeName;

        var contrainteIDInfo = 'ID : ' + '<span style="font-weight:bold">' + result[i].numeroContrainte + '</span>';
        var contrainteDateInfo = 'Date émission : ' + '<span style="font-weight:bold">' + result[i].dateEmissionContrainte + '</span>';

        var contrainteInfo = contrainteIDInfo + '<hr/>' + contrainteDateInfo;

        var montantPoursuiviInfo = 'Montant poursuivi : ' + '<span style="font-weight:bold">' + amountPoursuivi + '</span>';
        var fraisPoursuiteInfo = 'Frais de poursuite : ' + '<span style="font-weight:bold">' + amountFraisPoursuite + '</span>';
        var amountTotalInfo = 'TOTAL DÛ : ' + '<span style="font-weight:bold;color:red">' + amountTotal + '</span>';

        var montantDuInfo = montantPoursuiviInfo + '<hr/>' + fraisPoursuiteInfo + '<hr/>' + amountTotalInfo;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + result[i].numeroDocument + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + contrainteInfo + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + articleBudgetaireInfo + '</td>';

        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + montantDuInfo + '</td>';

        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + result[i].dateReceptionCommandement + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].dateEcheanceCommandement + '</td>';

        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + btnPrintCommandement + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    
    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="4" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';
    
    
    tableCommandement.html(tableContent);
    tableCommandement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 25,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(4).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD'));
        }
    });

}

function printCommandementDoucment(commandementID, numeroDocument) {

    var value = '<span style="font-weight:bold">' + numeroDocument + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir imprimer le commandement n° ' + value + ' ?', function () {


        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numero': commandementID,
                'operation': 'printNoteTaxation'

            },
            beforeSend: function () {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression du commandement en cours...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1' || response == '0') {
                    showResponseError();
                    return;
                } else {

                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.unblockUI();
                showResponseError();
            }
        });

    });
}

function saveAtd(tierDetenteur) {

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'numeroContrainte': contrainteSelected,
            'userId': userData.idUser,
            'tierDetenteur': tierDetenteur,
            'operation': 'generateAtd'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Génération en cours de l\'Atd...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            } else {

                loadCommandement(isAdvanced);

                setDocumentContent(response);
                window.open('visualisation-document', '_blank');

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function callGenerateAtd(contrainteID, assujettiCode) {

    contrainteSelected = contrainteID;
    redevableId = assujettiCode;

    alertify.confirm('Etes-vous sûre de vouloir générer l\'Atd pour ce commandement ?', function () {

        fromModalCall = '2';
        assujettiModal.modal('show');
    });
}

