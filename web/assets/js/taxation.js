/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var inputValueResearchResponsible, inputValueResearchBudgetArticle;
var inputRate, inputBaseCalcul, inputBaseCalculOnCasePourcent, inputQuantity, inputAmount, labelTotalAmount,
        inputMinimum, inputMaximum;

var cmbResearchType, cmbTarif;

var associatedExist;

var lblLegalForm,
        lblNifResponsible,
        lblNameResponsible,
        lblAddress,
        lblGeneratorAct,
        lblBudgetArticle,
        lblExampleBase,
        lblUniteBase, lblUniteBase2, lblUnityBaseCalcul;

var btnResearchResponsible, btnResearchBudgetArticle,
        btnAddBudgetArticle, btnSaveTaxation, btnRemoveTaxation, btnCallModalSearchResponsable, btnAssociateBien, btnCallModalInfoSuppTaxeVoirieConcentre;

var btnSelectedResponsible, btnSelectedBudgetArticle, btnValidateInfoBase;

var tbodyTaxation, tableResponsibles, tableBudgetArticles, tableTaxationBudgetArticles, tableTaxation;

var responsibleModal, budgetArticleModal;

var modalTransactionnel,
        inputMontanTransactionnel,
        lblTransactionnelValue,
        btnValidateTTransactionnel;

var infoResponsible, infoTaxationBudgetArticle, infosBaseCalculModal, tarifBudgetArticleModal;

var dataArrayBudgetArticles, dataTarifs = null;

var codeTarif, codeFormeJuridique, libelleTarif, libelleArticleBudgetaire, codeResponsible, adresseId, tarifName;
var typeTaux, tauxPalier, tauxAppliquer, montant, montantDu, devise, codeBdgetArticle, codeBdgetArticle2,
        baseCalculOnCaseIsPalier, uniteBudgetArticle, uniteBudgetArticleCode = '';
var isPalier, isTransactionnel;
var multiplier = '';
var amount = 0;
var accept_N_ArticleToPanier = '';

var infoBudgetArticle = {};

var transacMinimum, transacMaximum, codeServiceAB;

var listTaxationArticle = [];
var responsibleObject = new Object();
var objetDetailNoteCalcul = new Object();
var erase;

var typeTaxation;

var lbl1, lbl2, lbl3, lbl4;

var lblZoneBien, lblSelectedBien, codeBien;
var accepted;

var dataBienTaxationList = [];
var taxationWithBien;

var divTableBienTaxation, tableBienTaxation;
var codeDoc = undefined;
var codeTypeDocs = 'TD00000035';
var checkBoxJoinDocument;
var archivages;

var btnCallModalInfoSuppTaxeVoirieConcentre,
        modalComplementInfoTaxe,
        inputTransporteur,
        inputNatureProduit,
        inputNumeroPlaque,
        cmbModePaiement,
        btnConfirmation;

var complementInfoTaxeObj = {};
var complementInfoTaxeList = [];

var modalSubventionProvinciale,
        inputMontanSubvention,
        cmbDeviseMontantSubvention,
        btnValidateSubvention;

$(function () {

    mainNavigationLabel.text('TAXATION');
    secondNavigationLabel.text('Taxation non fiscale');

    typeTaxation = 'TO';

    associatedExist = false;
    taxationWithBien = false;

    removeActiveMenu();
    linkMenuTaxation.addClass('active');

    accepted = false;

    erase = '0';
    isPalier = 0;
    transacMinimum = 0;
    transacMaximum = 0;
    isTransactionnel = 0;
    codeBdgetArticle = '';
    codeResponsible = '';
    tableResponsibles = $('#tableResponsibles');
    tableBudgetArticles = $('#tableBudgetArticles');
    tableTaxationBudgetArticles = $('#tableTaxation');

    inputValueResearchBudgetArticle = $('#inputValueResearchBudgetArticle');
    inputRate = $('#inputRate');
    inputBaseCalcul = $('#inputBaseCalcul');
    inputQuantity = $('#inputQuantity');
    inputAmount = $('#inputAmount');
    labelTotalAmount = $('#labelTotalAmount');


    inputMinimum = $('#inputMinimum');
    inputMaximum = $('#inputMaximum');
    inputBaseCalculOnCasePourcent = $('#inputBaseCalculOnCasePourcent');

    cmbResearchType = $('#cmbResearchType');
    cmbTarif = $('#cmbTarif');

    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    lblNifResponsible = $('#lblNifResponsible');
    lblGeneratorAct = $('#lblGeneratorAct');
    lblBudgetArticle = $('#lblBudgetArticle');
    lblExampleBase = $('#lblExampleBase');
    lblUniteBase = $('#lblUniteBase');
    lblUniteBase2 = $('#lblUniteBase2');
    lblUnityBaseCalcul = $('#lblUnityBaseCalcul');

    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');
    lbl3 = $('#lbl3');
    lbl4 = $('#lbl4');

    btnResearchBudgetArticle = $('#btnResearchBudgetArticle');
    btnAddBudgetArticle = $('#btnAddBudgetArticle');
    btnSaveTaxation = $('#btnSaveTaxation');
    btnRemoveTaxation = $('#btnRemoveTaxation');
    btnSelectedBudgetArticle = $('#btnSelectedBudgetArticle');
    btnSelectedResponsible = $('#btnSelectedResponsible');
    btnSearchBudgetArticle = $('#btnSearchBudgetArticle');
    btnSelectedTarif = $('#btnSelectedTarif');
    btnValidateInfoBase = $('#btnValidateInfoBase');
    btnAssociateBien = $('#btnAssociateBien');
    btnCallModalInfoSuppTaxeVoirieConcentre = $('#btnCallModalInfoSuppTaxeVoirieConcentre');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');
    responsibleModal = $('#responsibleModal');
    budgetArticleModal = $('#budgetArticleModal');
    infosBaseCalculModal = $('#infosBaseCalculModal');
    tarifBudgetArticleModal = $('#tarifBudgetArticleModal');

    modalTransactionnel = $('#modalTransactionnel');
    inputMontanTransactionnel = $('#inputMontanTransactionnel');
    lblTransactionnelValue = $('#lblTransactionnelValue');
    btnValidateTTransactionnel = $('#btnValidateTTransactionnel');

    lblZoneBien = $('#lblZoneBien');
    lblSelectedBien = $('#lblSelectedBien');

    divTableBienTaxation = $('#divTableBienTaxation');
    tableBienTaxation = $('#tableBienTaxation');

    lblZoneBien.attr('style', 'display: none');

    checkBoxJoinDocument = $('#checkBoxJoinDocument');

    btnCallModalInfoSuppTaxeVoirieConcentre = $('#btnCallModalInfoSuppTaxeVoirieConcentre');
    modalComplementInfoTaxe = $('#modalComplementInfoTaxe');
    inputTransporteur = $('#inputTransporteur');
    inputNatureProduit = $('#inputNatureProduit');
    inputNumeroPlaque = $('#inputNumeroPlaque');
    cmbModePaiement = $('#cmbModePaiement');
    btnConfirmation = $('#btnConfirmation');


    modalSubventionProvinciale = $('#modalSubventionProvinciale');
    inputMontanSubvention = $('#inputMontanSubvention');
    cmbDeviseMontantSubvention = $('#cmbDeviseMontantSubvention');
    btnValidateSubvention = $('#btnValidateSubvention');

    cmbTarif.on('change', function (e) {

        if (cmbTarif.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un tarif valide');
            return;
        }

        codeTarif = cmbTarif.val();
        tarifName = $('#cmbTarif option:selected').text().toUpperCase();
    });

    inputBaseCalcul.on('change', function (e) {
        calculateBaseCalcul('B');
    });

    inputQuantity.on('change', function (e) {
        calculateBaseCalcul('Q');
    });

    btnCallModalInfoSuppTaxeVoirieConcentre.on('click', function (e) {

        e.preventDefault();

        if (complementInfoTaxeList.length == 0) {

            inputTransporteur.val('');
            inputNatureProduit.val('');
            inputNumeroPlaque.val('');
            cmbModePaiement.val('0');

        } else {

            inputTransporteur.val(complementInfoTaxeObj.transpoteur);
            inputNatureProduit.val(complementInfoTaxeObj.natureProduit);
            inputNumeroPlaque.val(complementInfoTaxeObj.numeroPlaque);
            cmbModePaiement.val(complementInfoTaxeObj.cmdModePaiement);

        }

        modalComplementInfoTaxe.modal('show');

    });

    btnConfirmation.on('click', function (e) {

        e.preventDefault();

        if (inputTransporteur.val() == '') {

            alertify.alert('Veuillez d\'abord fournir le nom du transiteur');
            return;
        }

        if (inputNatureProduit.val() == '') {

            alertify.alert('Veuillez d\'abord fournir la nature du produit transporté');
            return;
        }

        if (inputNumeroPlaque.val() == '') {

            alertify.alert('Veuillez d\'abord fournir le numéro plaque véhicule/Wagon');
            return;
        }

        if (cmbModePaiement.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner le mode de paiement');
            return;
        }

        alertify.confirm('Etes-vous sûr de vouloir confirmer l\'ajout de ce complément d\'informations ?', function () {

            complementInfoTaxeObj = {};
            complementInfoTaxeList = [];

            complementInfoTaxeObj.transpoteur = inputTransporteur.val();
            complementInfoTaxeObj.natureProduit = inputNatureProduit.val();
            complementInfoTaxeObj.numeroPlaque = inputNumeroPlaque.val();
            complementInfoTaxeObj.cmdModePaiement = cmbModePaiement.val();

            complementInfoTaxeList.push(complementInfoTaxeObj);

            modalComplementInfoTaxe.modal('hide');

        });

    });
    
    
    btnValidateSubvention.on('click', function (e) {
        e.preventDefault();
        validateBaseCalculSubvention();
    });

    btnValidateInfoBase.on('click', function (e) {
        e.preventDefault();
        validateBaseCalcul();
    });

    btnSelectedTarif.on('click', function (e) {
        e.preventDefault();
        displayInfosPalierAndBudgetArticle();
        codeBdgetArticle2 = codeBdgetArticle;

        switch (codeBdgetArticle) {
            case '00000000000002102016':
                btnAssociateBien.attr('style', 'display:inline');
                break;
            default :
                btnAssociateBien.attr('style', 'display:none');
                break;

        }
    });

    btnSearchBudgetArticle.on('click', function (e) {
        e.preventDefault();
        callModalResearchBudgetArticles();
    });

    btnSelectedBudgetArticle.on('click', function (e) {
        e.preventDefault();
        getCurrentBudgetArticle();
    });

    btnValidateTTransactionnel.on('click', function (e) {
        e.preventDefault();
        showTransactionnelTaux();
    });

    btnCallModalSearchResponsable.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        assujettiModal.modal('show');
    });

    btnResearchBudgetArticle.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadBudgetArticles();
    });

    btnAddBudgetArticle.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (codeBdgetArticle === empty) {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un article budgétaire avant d\'ajouter dans le panier !');
            return;
        }

        if (inputBaseCalcul.val() === empty) {
            alertify.alert('Veuillez d\'abord fournir la valeur de la base de calcul.');
            return;
        }

        if (inputQuantity.val() === empty) {
            alertify.alert('Veuillez d\'abord fournir la valeur de nombre d\'acte');
            return;
        }

        if (inputAmount.val() === empty || amount == 0) {
            alertify.alert('Le montant dû est invalide.');
            return;
        }


        if (codeBdgetArticle == '00000000000002102016') {

            if (dataBienTaxationList.length == 0) {

                alertify.alert('Veuillez d\'abord associer au moins un bien pour cette taxation');
                return;
            }
        }


        if (listTaxationArticle.length > 0) {
            if (accept_N_ArticleToPanier == '0') {
                alertify.confirm('Vous avez déjà ajouter un autre article budgétaire dans le panier. Voulez-vous le remplacer ?', function () {
                    listTaxationArticle = new Array();
                    erase = '1';
                    insertBudgetArticleInTaxationList();
                    return;
                });
            } else {
                erase = '0';
                insertBudgetArticleInTaxationList();
                return;
            }
        } else {
            erase = '0';
            insertBudgetArticleInTaxationList();
            return;
        }

    });

    btnSaveTaxation.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (codeBdgetArticle2 == '00000000000002102016') {

            if (associatedExist == false) {
                var valueTarif = '<span style="font-weight:bold">' + tarifName + '</span>';
                alertify.alert('Veuillez d\'abord associer un panneau publicitaire de catégorie : ' + valueTarif + ' à cette taxation');
                return;

            }

            if (dataBienTaxationList.length == 0) {
                var valueTarif = '<span style="font-weight:bold">' + tarifName + '</span>';
                alertify.alert('Veuillez d\'abord associer un panneau publicitaire de catégorie : ' + valueTarif + ' à cette taxation');
                return;
            }
        } else if (codeBdgetArticle2 == '00000000000001072016' || codeBdgetArticle2 == '00000000000001212016') {

            if (complementInfoTaxeList.length == 0) {
                alertify.alert('Veuillez d\'abord ajouter les compléments d\'informations lier à la taxe');
                return;
            }
        }

        callDialogConfirmSaveNoteCalcul();
    });

    inputValueResearchBudgetArticle.keypress(function (e) {
        if (e.keyCode === 13) {
            btnResearchBudgetArticle.trigger('click');
        }
    });

    inputBaseCalculOnCasePourcent.keypress(function (e) {
        if (e.keyCode === 13) {
            btnValidateInfoBase.trigger('click');
        }
    });


    btnAssociateBien.click(function (e) {

        e.preventDefault();

        if (codeResponsible == empty) {

            alertify.alert('Veuillez d\'abord rechercher et sélectionner un assujetti.');
            return;
        }

        loadBiensPersonne(codeResponsible);
    });

    printResultBudgetArticle('');
    printTaxationBudgetArticle('');

});

function selectPersonneBien() {

    lblZoneBien.attr('style', 'display: inline');
    //codeBien = selectAssujettiBien.id;
    lblSelectedBien.html(dataBienTaxationList.length);
    printTableBienTaxation(dataBienTaxationList);
}

function printTableBienTaxation(dataBienTaxationList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> BIEN </th>';
    header += '<th scope="col">ADRESSE</th>';
    header += '<th scope="col"></th>';

    var body = '<tbody>';

    for (var i = 0; i < dataBienTaxationList.length; i++) {


        var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + dataBienTaxationList[i].libelleTypeBien + '</span>';
        var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + dataBienTaxationList[i].tarifName + '</span>';
        var nameInfo = 'Dénomination : ' + '<span style="font-weight:bold">' + dataBienTaxationList[i].intituleBien + '</span>';

        var descriptionBien = nameInfo + '<br/>' + genreInfo + '<br/>' + categorieInfo + '<hr/>' + bienList[i].complement;

        //var btnDeleteBienTaxe = '<button type="button" class="btn btn-danger" title="Cliquez ici pour retirer ce bien" onclick="retirerBien(\'' + dataBienTaxationList[i].id + '\',\'' + dataBienTaxationList[i].intituleBien + '\')"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Retirer ce bien</button>';

        var btnDeleteBienTaxe = '<button type="button" class="btn btn-danger" title="Cliquez ici pour retirer ce bien" onclick="retirerBien(\'' + dataBienTaxationList[i].bien + '\')"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Retirer ce bien</button>';

        body += '<tr>';
        body += '<td style="vertical-align:middle;width:50px">' + descriptionBien + '</td>';
        body += '<td style="vertical-align:middle;width:25px">' + dataBienTaxationList[i].chaineAdresse.toUpperCase() + '</td>';
        body += '<td style="text-align:center;width:10%;vertical-align:middle">' + btnDeleteBienTaxe + '</td>';
        body += '</tr>';

    }

    body += '</tbody>';

    var tableContent = header + body;

    tableBienTaxation.html(tableContent);

    tableBienTaxation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher le bien ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    if (dataBienTaxationList.length > 1) {

        divTableBienTaxation.attr('style', 'display: inline');

        inputQuantity.val(dataBienTaxationList.length);
        calculateBaseCalcul('Q');
        inputQuantity.attr('readonly', true);

        inputBaseCalcul.val(dataBienTaxationList.length);
        calculateBaseCalcul('B');
        inputBaseCalcul.attr('readonly', true);
    }


}

function calculateBaseCalcul(type) {

    switch (type) {
        case 'Q':
            if (inputQuantity === '') {
                alertify.alert('Veuillez d\'abord fournir le nombre d\'actes');
                return;
                break;
            }
        case 'B':
            if (inputBaseCalcul === '') {
                alertify.alert('Veuillez d\'abord fournir la base de calcul');
                return;
                break;
            }
    }

    if (isTransactionnel == 1) {
        amount = tauxPalier * inputQuantity.val();
        inputAmount.val(formatNumber(amount, devise));
        return;
    }

    switch (typeTaux) {
        case 'F':
            amount = tauxPalier * inputQuantity.val() * inputBaseCalcul.val();
            inputAmount.val(formatNumber(amount, devise));
            if (multiplier == '0') {
                inputBaseCalcul.attr('disabled', true);
            } else {
                inputBaseCalcul.attr('disabled', false);
            }
            break;
        case '%':
            amount = inputBaseCalcul.val() * (tauxPalier / 100) * inputQuantity.val();
            inputAmount.val(formatNumber(amount, devise));
            inputBaseCalcul.attr('disabled', false);
            break;
    }
}

function validateBaseCalcul() {

    amount = 0;

    if (inputBaseCalculOnCasePourcent.val() === '') {
        alertify.alert('Veuillez d\'abord fournir la base de calcul');
        return;
    }

    infosBaseCalculModal.modal('hide');

    baseCalculOnCaseIsPalier = inputBaseCalculOnCasePourcent.val();

    switch (isPalier) {
        case 0:
            if (typeTaux == 'F') {
                inputRate.val(tauxPalier);
                inputBaseCalcul.val(baseCalculOnCaseIsPalier);
                amount = (baseCalculOnCaseIsPalier * inputQuantity.val() * tauxPalier);
                inputAmount.val(formatNumber(amount, devise));
            } else if (typeTaux == '%') {
                inputRate.val(tauxPalier + ' (' + typeTaux + ')');
                inputBaseCalcul.val(baseCalculOnCaseIsPalier);
                amount = (baseCalculOnCaseIsPalier * (tauxPalier / 100) * inputQuantity.val());
                inputAmount.val(formatNumber(amount, devise));
            }
            inputBaseCalcul.attr('disabled', false);
            break;
        case 1:
            inputBaseCalcul.val(baseCalculOnCaseIsPalier);
            loadTarifByBudgetArticleAndBase(codeBdgetArticle, baseCalculOnCaseIsPalier);
            inputBaseCalcul.val('1');
            break;
    }

}

function validateBaseCalculSubvention() {

    amount = 0;

    if (inputMontanSubvention.val() === '') {
        alertify.alert('Veuillez d\'abord fournir le montant de la subvention');
        return;
    }

    infosBaseCalculModal.modal('hide');

    baseCalculOnCaseIsPalier = inputMontanSubvention.val();

    switch (isPalier) {
        case 0:
            if (typeTaux == 'F') {
                inputRate.val(tauxPalier);
                inputBaseCalcul.val(baseCalculOnCaseIsPalier);
                amount = (baseCalculOnCaseIsPalier * inputQuantity.val() * tauxPalier);
                inputAmount.val(formatNumber(amount, devise));
            } else if (typeTaux == '%') {
                inputRate.val(tauxPalier + ' (' + typeTaux + ')');
                inputBaseCalcul.val(baseCalculOnCaseIsPalier);
                amount = (baseCalculOnCaseIsPalier * (tauxPalier / 100) * inputQuantity.val());
                inputAmount.val(formatNumber(amount, devise));
            }
            inputBaseCalcul.attr('disabled', false);
            
            
            break;
        case 1:
            inputBaseCalcul.val(baseCalculOnCaseIsPalier);
            loadTarifByBudgetArticleAndBase(codeBdgetArticle, baseCalculOnCaseIsPalier);
            inputBaseCalcul.val('1');
            break;
    }

}

function displayInfosPalierAndBudgetArticle() {

    if (codeTarif === '0') {
        alertify.alert('Veuillez d\'abord sélectionner un tarif.');
        return;
    }

    tarifBudgetArticleModal.modal('hide');

    if (dataTarifs.length > 1) {

        for (var i = 0; i < dataTarifs.length; i++) {
            if (dataTarifs[i].codeTarif === codeTarif) {
                showSelectedTaux(dataTarifs[i]);
                return;
            }

        }
    }

}

function insertBudgetArticleInTaxationList() {

    if (erase == '0') {
        alertify.confirm('Etes-vous sûre de vouloir ajouter cette taxe dans le panier ?', function () {
            addBudgetArticleInTaxationList();
        });
        return;
    } else if (erase == '1') {
        addBudgetArticleInTaxationList();
    }
}

function addBudgetArticleInTaxationList() {

    objetDetailNoteCalcul = new Object();

    objetDetailNoteCalcul.budgetArticleCode = codeBdgetArticle;
    objetDetailNoteCalcul.budgetArticleText = libelleArticleBudgetaire;
    objetDetailNoteCalcul.baseCalcul = inputBaseCalcul.val();
    objetDetailNoteCalcul.unity = uniteBudgetArticle;
    objetDetailNoteCalcul.unityCode = uniteBudgetArticleCode;
    objetDetailNoteCalcul.quantity = inputQuantity.val();
    objetDetailNoteCalcul.devise = devise;
    objetDetailNoteCalcul.typeTaux = typeTaux;
    objetDetailNoteCalcul.codeTarif = codeTarif;

    switch (typeTaux) {
        case 'F':
            objetDetailNoteCalcul.amount = (tauxPalier * inputQuantity.val() * inputBaseCalcul.val());
            objetDetailNoteCalcul.taux = tauxPalier;
            objetDetailNoteCalcul.tauxDisplay = tauxPalier;
            break;
        case '%':
            objetDetailNoteCalcul.amount = (inputBaseCalcul.val() * (tauxPalier / 100) * inputQuantity.val());
            objetDetailNoteCalcul.tauxDisplay = tauxPalier + ' (' + typeTaux + ')';
            objetDetailNoteCalcul.taux = tauxPalier;
            break;
    }

    if (listTaxationArticle.length > 0) {

        for (i = 0; i < listTaxationArticle.length; i++) {

            if (codeBdgetArticle === listTaxationArticle[i].budgetArticleCode
                    && codeTarif === listTaxationArticle[i].codeTarif) {
                alertify.alert('Cette taxe et ce tarif se trouvent déjà dans le panier.');
                return;
            }
        }
    }

    listTaxationArticle.push(objetDetailNoteCalcul);

    var totalAmout = 0;
    for (i = 0; i < listTaxationArticle.length; i++) {
        totalAmout += listTaxationArticle[i].amount;
    }

    printTaxationBudgetArticle(listTaxationArticle);
    labelTotalAmount.html(formatNumber(totalAmout, devise));

    amount = 0;
    isTransactionnel = 0;
    transacMinimum = 0;
    transacMaximum = 0;
    lblUniteBase.html(empty);
    lblUnityBaseCalcul.html(empty);
    lblUniteBase2.html(empty);
    lblBudgetArticle.text(empty);
    inputRate.val(empty);
    inputBaseCalcul.val('1');
    inputQuantity.val('1');
    inputAmount.val(empty);
    codeBdgetArticle = empty;
    inputBaseCalculOnCasePourcent.val(empty);
    libelleArticleBudgetaire = empty;
    uniteBudgetArticle = empty;
    dataArrayBudgetArticles = empty;
    infoBudgetArticle = {};
    inputValueResearchBudgetArticle.val(empty);
    printResultBudgetArticle(empty);
    btnSelectedBudgetArticle.hide();
    objetDetailNoteCalcul = new Object();
}

function callModalResearchBudgetArticles() {

    if (codeResponsible === empty) {
        alertify.alert('Veuillez d\'abord rechercher et sélectionner un assujetti.');
        return;
    }

    budgetArticleModal.modal('show');
}

function loadBudgetArticles() {
    if (inputValueResearchBudgetArticle.val() === empty) {
        alertify.alert('Veuillez fournir le critère de recherche de l\'article budgétaire.');
        return;
    }

    var viewAllService = controlAccess('SEARCH_FOR_ALL_SERVICES');

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchBudgetArticle.val(),
            'seeEverything': viewAllService,
            'codeService': userData.serviceCode,
            'operation': 'researchBudgetArticle'
        },
        beforeSend: function () {
            budgetArticleModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {
                budgetArticleModal.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnSelectedBudgetArticle.hide();
                    printResultBudgetArticle('');
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fournis.');
                } else {
                    var dataBudgetArticle = JSON.parse(JSON.stringify(response));

                    if (dataBudgetArticle.length > 0) {
                        btnSelectedBudgetArticle.show();
                        printResultBudgetArticle(dataBudgetArticle);
                    } else {
                        btnSelectedBudgetArticle.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            budgetArticleModal.unblock();
            showResponseError();
        }
    });
}

function loadResponsibles() {

    if (inputValueResearchResponsible.val() === '') {
        alertify.alert('Veuillez fournir le critère de recherche de l\'assujetti.');
        return;
    }
    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchResponsible.val(),
            'operation': 'researchResponsible'
        },
        beforeSend: function () {
            responsibleModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {
                responsibleModal.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnSelectedResponsible.hide();
                    printResultResponsible('');
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fournis.');
                    return;
                } else {
                    var dataResponsible = JSON.parse(JSON.stringify(response));
                    if (dataResponsible.length > 0) {
                        btnSelectedResponsible.show();
                        printResultResponsible(dataResponsible);
                    } else {
                        btnSelectedResponsible.hide();
                    }
                }

            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            responsibleModal.unblock();
            showResponseError();
        }

    });
}

function printTaxationBudgetArticle(taxationBudgetArticle) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:44%"scope="col">TAXE</th>';
    tableContent += '<th style="text-align:right;width:11%" scope="col">BASE</th>';
    tableContent += '<th style="text-align:center;width:8%" scope="col">UNITE</th>';
    tableContent += '<th style="text-align:center;width:10%" scope="col">TAUX</th>';
    tableContent += '<th style="text-align:center;width:11%" scope="col">NBRE. ACTES</th>';
    tableContent += '<th style="text-align:right;width:13%" scope="col">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:center;width:4%" scope="col"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < taxationBudgetArticle.length; i++) {

        var firstLineAB = '';

        if (taxationBudgetArticle[i].budgetArticleText.length > 350) {
            firstLineAB = taxationBudgetArticle[i].budgetArticleText.substring(0, 350).toUpperCase() + ' ...';
        } else {
            firstLineAB = taxationBudgetArticle[i].budgetArticleText.toUpperCase();
        }

        tableContent += '<tr>';
        tableContent += '<td title="' + taxationBudgetArticle[i].budgetArticleText + '">' + firstLineAB + '</td>';
        //tableContent += '<td>' + taxationBudgetArticle[i].budgetArticleText + '</td>';
        tableContent += '<td style="text-align:right">' + taxationBudgetArticle[i].baseCalcul + '</td>';
        tableContent += '<td style="text-align:center">' + taxationBudgetArticle[i].unity + '</td>';
        tableContent += '<td style="text-align:center">' + taxationBudgetArticle[i].tauxDisplay + '</td>';
        tableContent += '<td style="text-align:center">' + taxationBudgetArticle[i].quantity + '</td>';
        tableContent += '<td style="text-align:right">' + formatNumber(taxationBudgetArticle[i].amount, taxationBudgetArticle[i].devise) + '</td>';
        tableContent += '<td style="text-align:center"><button class="btn btn-danger" onclick="removeBudgetArticle(\'' + taxationBudgetArticle[i].budgetArticleCode + '\',\'' + taxationBudgetArticle[i].codeTarif + '\')"><i class="fa fa-trash-o"></i></button></td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableTaxationBudgetArticles.html(tableContent);

    var myDataTable = tableTaxationBudgetArticles.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des taxes est vide",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}

function printResultBudgetArticle(dataBudgetArticle) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th scope="col">SERVICE</th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';
    var firstLineAB = '';

    for (var i = 0; i < dataBudgetArticle.length; i++) {

        if (dataBudgetArticle[i].libelleArticleBudgetaire.length > 150) {
            firstLineAB = dataBudgetArticle[i].libelleArticleBudgetaire.substring(0, 150).toUpperCase() + ' ...';
        } else {
            firstLineAB = dataBudgetArticle[i].libelleArticleBudgetaire.toUpperCase();
        }

        tableContent += '<tr>';
        tableContent += '<td title="' + dataBudgetArticle[i].libelleArticleBudgetaire + '">' + firstLineAB + '</td>';
        tableContent += '<td>' + dataBudgetArticle[i].libelleService + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].codeArticleBudgetaire + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].palier + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].libelleUnite + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].addNArticleToPanier + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].transactionnel + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].transactionnelMinimum + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].transactionnelMaximum + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].codeService + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableBudgetArticles.html(tableContent);

    var myDataTable = tableBudgetArticles.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableBudgetArticles tbody').on('click', 'tr', function () {
        infoBudgetArticle = myDataTable.row(this).data();
        accepted = true;
    });
}

function getCurrentBudgetArticle() {

    if (accepted == false) {

        alertify.alert('Veuillez d\'abord sélectionner un article budgétaire');
        return;

    }

    if (infoBudgetArticle.code === '') {
        alertify.alert('Veuillez d\'abord sélectionner un article budgétaire');
        return;
    }

    dataArrayBudgetArticles = infoBudgetArticle;

    codeBdgetArticle = dataArrayBudgetArticles[2];
    libelleArticleBudgetaire = dataArrayBudgetArticles[0];
    uniteBudgetArticle = dataArrayBudgetArticles[4];
    accept_N_ArticleToPanier = dataArrayBudgetArticles[5];
    isTransactionnel = dataArrayBudgetArticles[6];

    transacMinimum = dataArrayBudgetArticles[7];
    transacMaximum = dataArrayBudgetArticles[8];
    codeServiceAB = dataArrayBudgetArticles[9];

    inputAmount.val('');
    inputRate.val('');
    inputBaseCalculOnCasePourcent.val('');
    inputQuantity.val('1');
    inputBaseCalcul.val('1');

    lblUniteBase2.html('(' + uniteBudgetArticle + ')');

    budgetArticleModal.modal('hide');

    if (isTransactionnel == 1) {

        lblBudgetArticle.html('TAXE : ' + libelleArticleBudgetaire);
        lblTransactionnelValue.html('Veuillez saisir un montant compris entre ' + formatNumber(transacMinimum, 'CDF') + ' et ' + formatNumber(transacMaximum, 'CDF'));
        inputMontanTransactionnel.val('1');
        lblUnityBaseCalcul.html('(' + uniteBudgetArticle + ')');
        codeTarif = '0000000420';
        typeTaux = 'F';
        devise = 'CDF';
        modalTransactionnel.modal('show');
        return;
    }

    if (dataArrayBudgetArticles[3] === '1') {
        isPalier = 1;
        inputBaseCalculOnCasePourcent.val('1');
        infosBaseCalculModal.modal('show');

    } else {
        isPalier = 0;
        loadTarifByBudgetArticleAndBase(codeBdgetArticle, 0);
    }
}

function getSelectedAssujetiData() {

    lblNameResponsible.html(selectAssujettiData.nomComplet);
    lblLegalForm.html(selectAssujettiData.categorie);
    lblAddress.html(selectAssujettiData.adresse);
    codeResponsible = selectAssujettiData.code;
    lblNifResponsible.html(selectAssujettiData.nif);
    //lblNifResponsible.html(selectAssujettiData.nif);
    codeFormeJuridique = selectAssujettiData.codeForme;
    adresseId = selectAssujettiData.codeAdresse;

    lbl1.show();
    lbl2.show();
    lbl3.show();
    lbl4.show();

    responsibleObject = new Object();

    responsibleObject.codeResponsible = codeResponsible;
    responsibleObject.codeFormeJuridique = codeFormeJuridique;
    responsibleObject.adresseId = adresseId;
}

function loadTarifByBudgetArticleAndBase(budgetArticleCode, baseCalcul) {

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'codeArticleBudgetaire': budgetArticleCode,
            'codeFormeJuridique': codeFormeJuridique,
            'baseCalcul': baseCalcul,
            'palier': isPalier,
            'operation': 'researchRateArticle'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();

                if (response == '-1') {
                    alertify.alert('Une erreur inattendue est survenue. Veuillez réesayer ou contacter l\'administrateur.');
                    return;
                }

                dataTarifs = JSON.parse(JSON.stringify(response));

                if (dataTarifs.length < 1) {
                    alertify.alert('Aucun tarif trouvé. Veuillez configurer correctement l\'article budgétaire');
                    return;
                }

                if (dataTarifs.length > 1) {

                    consituateTarifList();

                } else {

                    showSelectedTaux(dataTarifs[0]);
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function consituateTarifList() {

    var dataTarifDisplay = '<option value ="0">-- Sélectionner un tarif --</option>';

    for (var i = 0; i < dataTarifs.length; i++) {
        dataTarifDisplay += '<option value =' + dataTarifs[i].codeTarif + '>' + dataTarifs[i].libelleTarif.toUpperCase() + '</option>';
    }

    cmbTarif.html(dataTarifDisplay);
    budgetArticleModal.modal('hide');
    tarifBudgetArticleModal.modal('show');
}

function callDialogConfirmSaveNoteCalcul() {

    if (listTaxationArticle.length > 0) {

        alertify.confirm('Etes-vous sûre de vouloir enregistrer cette taxation ?', function () {

            var checkControl = document.getElementById("checkBoxJoinDocument");

            if (checkControl.checked) {
                initUpload(codeDoc, codeTypeDocs);
            } else {
                saveNoteCalcul();
            }

        });
    } else {
        alertify.alert('Veuillez d\'abord ajouter un article budgétaire dans le panier.');
    }
}

function emptyDataTaxation() {
    listTaxationArticle = [];
    responsibleObject = {};
    objetDetailNoteCalcul = {};
}

function saveNoteCalcul() {

    var taxations = JSON.stringify(listTaxationArticle);

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'codeResponsible': responsibleObject.codeResponsible,
            'codeAdresse': responsibleObject.adresseId,
            'codeSite': userData.SiteCode,
            'userId': userData.idUser,
            'depotDeclaration': null,
            'typeTaxation': typeTaxation,
            'codeService': userData.serviceCode,
            'detailTaxation': taxations,
            'dataBienTaxation': JSON.stringify(dataBienTaxationList),
            'archives': archivages,
            'complementInfoTaxe': JSON.stringify(complementInfoTaxeList),
            'operation': 'saveNoteCalcul'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Création de la note de taxation en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                }
                if (response == '0') {
                    alertify.alert('Echec opération. Une erreur s\'est produite lors de la création de la note de taxation !');
                    return;
                } else {
                    alertify.alert('Votre dossier est à la clôture avec comme référence : ' + response);
                    listTaxationArticle = new Array();
                    objetDetailNoteCalcul = new Object();
                    dataBienTaxationList = [];

                    complementInfoTaxeObj = {};
                    complementInfoTaxeList = [];
                    divTableBienTaxation.attr('style', 'display: none');

                    setTimeout(function () {
                        window.location = 'taxation-ordinaire';
                    }, 3000);
                    return;
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function removeBudgetArticle(codeBudgetArticle, codeTarif) {

    alertify.confirm('Etes-vous sûre de vouloir retirer cet article budgétaire dans le panier ?', function () {

        for (i = 0; i < listTaxationArticle.length; i++) {

            if (codeBudgetArticle === listTaxationArticle[i].budgetArticleCode
                    && codeTarif === listTaxationArticle[i].codeTarif) {

                listTaxationArticle.splice(i, 1);

                var totalAmout = 0;

                for (i = 0; i < listTaxationArticle.length; i++) {
                    totalAmout += listTaxationArticle[i].amount;
                }

                printTaxationBudgetArticle(listTaxationArticle);
                labelTotalAmount.html(formatNumber(totalAmout, devise));

                return;
            }
        }
    });

}

function showTransactionnelTaux() {

    var value = parseFloat(inputMontanTransactionnel.val());

    if (value < transacMinimum) {
        alertify.alert('Le montant dû doit être supérieur à ' + formatNumber(transacMinimum, devise));
        return;
    }

    if (value > transacMaximum) {
        alertify.alert('Le montant dû doit être inférieur à ' + formatNumber(transacMaximum, devise));
        return;
    }

    modalTransactionnel.modal('hide');

    inputRate.val(value);
    amount = value;
    inputAmount.val(formatNumber(value, devise));
    tauxPalier = amount;
    inputBaseCalcul.attr('disabled', true);

}

function showSelectedTaux(tarifObj) {

    codeTarif = tarifObj.codeTarif;

    var tarifName = '';

    if (tarifObj.libelleTarif == 'All' || tarifObj.libelleTarif == 'ALL' || tarifObj.libelleTarif == 'all') {
        tarifName = '';
    } else {
        tarifName = ' : ' + tarifObj.libelleTarif.toUpperCase();
    }

    lblBudgetArticle.html('TAXE : ' + libelleArticleBudgetaire + tarifName);
    libelleArticleBudgetaire = libelleArticleBudgetaire + tarifName;
    devise = tarifObj.devisePalier;
    typeTaux = tarifObj.typeTaux;
    multiplier = tarifObj.multiplierParValeurBase;
    tauxPalier = tarifObj.tauxPalier;

    inputRate.val(tauxPalier);
    inputQuantity.val('1');
    //lblUnityBaseCalcul.html('(' + uniteBudgetArticle + ')');
    uniteBudgetArticle = tarifObj.uniteLibellePalier;
    uniteBudgetArticleCode = tarifObj.unitePalier;
    lblUnityBaseCalcul.html('(' + tarifObj.uniteLibellePalier + ')');
    lblUniteBase2.html('(' + tarifObj.uniteLibellePalier + ')');

    switch (typeTaux) {
        case 'F':
            if (multiplier == '0') {
                inputBaseCalcul.val('1');
                inputBaseCalcul.attr('disabled', true);
                amount = (tarifObj.tauxPalier * inputQuantity.val() * inputBaseCalcul.val());
                inputAmount.val(formatNumber(amount, devise));
            } else {
                inputBaseCalcul.val('1');
                inputBaseCalcul.attr('disabled', false);
                amount = (tauxPalier * inputQuantity.val() * inputBaseCalcul.val());
                inputAmount.val(formatNumber(amount, devise));

                if (isPalier == 0) {

                    if (codeBdgetArticle == '00000000000002372021') {
                        modalSubventionProvinciale.modal('show');
                    } else {
                        infosBaseCalculModal.modal('show');
                    }

                }

            }
            break;
        case '%':
            inputRate.val(tauxPalier + ' (' + typeTaux + ')');
            amount = inputBaseCalcul.val() * (tauxPalier / 100) * inputQuantity.val();
            inputAmount.val(formatNumber(amount, devise));
            if (isPalier == 0) {
                infosBaseCalculModal.modal('show');
            }
            break;
    }

    switch (codeBdgetArticle) {

        case '00000000000001072016': // CONCENTRE
        case '00000000000001212016': // VOIRIE
            btnCallModalInfoSuppTaxeVoirieConcentre.attr('style', 'display:inline');

            codeBdgetArticle2 = codeBdgetArticle;

            break;
        default :
            btnCallModalInfoSuppTaxeVoirieConcentre.attr('style', 'display:none');
            codeBdgetArticle2 = '';
            break;
    }

}

function retirerBien(id) {

    //var value = '<span style="font-weight:bold">' + nameBien + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir retirer ce bien dans la taxation ?', function () {

        for (var i = 0; i < dataBienTaxationList.length; i++) {

            if (dataBienTaxationList[i].bien == id) {

                dataBienTaxationList.splice(i, 1);
                printTableBienTaxation(dataBienTaxationList);

                lblSelectedBien.html(dataBienTaxationList.length);
                break;
            }
        }
    });

}

function getDocumentsToUpload() {

    archivages = getUploadedData();
    saveNoteCalcul();
}
