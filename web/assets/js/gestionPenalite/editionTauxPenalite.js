/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var selectPenalite, selectNature, btnSaveTauxPenalite, btnInitFields, selectTypePersonne,
        inputTaux, inputNbreJourMin, inputNbreJourMax, checkBoxTypeTaux,
        checkBoxActivateTauxPenalite, inputResearchPenalite, btnResearchPenalite, tableTauxPenalite, checkBoxRecidive;

var penaliteList;
var penaliteSelected;
var natureABList;
var tauxPenaliteList;
var idSelected;

var spnLblTypeTaux;

var typeTauxSelected;

$(function () {

    mainNavigationLabel.text('GESTION DE PENALITES');
    secondNavigationLabel.text('Edition des taux de pénalité');

    idSelected = empty;
    typeTauxSelected = empty;

    selectPenalite = $('#selectPenalite');
    selectNature = $('#selectNature');
    btnSaveTauxPenalite = $('#btnSaveTauxPenalite');
    btnInitFields = $('#btnInitFields');
    selectTypePersonne = $('#selectTypePersonne');
    inputTaux = $('#inputTaux');

    inputNbreJourMin = $('#inputNbreJourMin');
    inputNbreJourMax = $('#inputNbreJourMax');
    checkBoxTypeTaux = $('#checkBoxTypeTaux');
    checkBoxActivateTauxPenalite = $('#checkBoxActivateTauxPenalite');
    inputResearchPenalite = $('#inputResearchPenalite');
    btnResearchPenalite = $('#btnResearchPenalite');
    tableTauxPenalite = $('#tableTauxPenalite');
    checkBoxRecidive = $('#checkBoxRecidive');
    spnLblTypeTaux = $('#spnLblTypeTaux');

    inputResearchPenalite.val(empty);

    btnResearchPenalite.click(function (e) {

        e.preventDefault();

        if (inputResearchPenalite.val() === empty) {

            alertify.alert('Veuillez d\'abord saisir la pénalité à rechercher');
            return;

        } else {
            loadPenalites();
        }
    });

    selectPenalite.on('change', function (e) {

        var codePenalite = ($('#selectPenalite').val());

        if (codePenalite === empty) {

            alertify.alert('Veuillez d\'abord sélectionner une pénalité');
            return;

        } else {
            loadTauxPenalites();
        }
    });

    btnInitFields.click(function (e) {
        e.preventDefault();
        resetFields();
    });

    btnSaveTauxPenalite.click(function (e) {
        e.preventDefault();
        checkFields();
    });

    loadNatureAB();
    printTauxPenalite(empty);
});

function initData() {

    var penaliteList = JSON.parse(userData.penaliteList);

    var dataPenalite = '';

    dataPenalite += '<option value="">--</option>';

    for (var i = 0; i < penaliteList.length; i++) {
        dataPenalite += '<option value="' + penaliteList[i].penaliteCode + '">' + penaliteList[i].penaliteName + '</option>';
    }

    selectPenalite.html(dataPenalite);


    var typePersonneList = JSON.parse(userData.formeJuridiqueList);

    var dataTypePersonne = '';

    dataTypePersonne += '<option value="">--</option>';
    dataTypePersonne += '<option value="*">Tous type de personne</option>';

    for (var i = 0; i < typePersonneList.length; i++) {
        dataTypePersonne += '<option value="' + typePersonneList[i].formeJuridiqueCode + '">' + typePersonneList[i].formeJuridiqueName + '</option>';
    }

    selectTypePersonne.html(dataTypePersonne);

    var dataNatureAB = '';

    dataNatureAB += '<option value="">--</option>';

    for (var i = 0; i < natureABList.length; i++) {
        dataNatureAB += '<option value="' + natureABList[i].natureABCode + '">' + natureABList[i].natureABName + '</option>';
    }

    selectNature.html(dataNatureAB);
}


function loadNatureAB() {

    $.ajax({
        type: 'POST',
        url: 'gestionPenaliteBackendServlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': 'loadNatureAb',
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                resetFields();
                printTauxPenalite(empty);
                showResponseError();
                return;

            } else if (response == '0') {

                printTauxPenalite(empty);
                resetFields();

                return;

            } else {
                natureABList = JSON.parse(JSON.stringify(response));
                initData();

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            printTauxPenalite(empty);
            resetFields();
            showResponseError();
        }
    });

}


function loadTauxPenalites() {

    $.ajax({
        type: 'POST',
        url: 'gestionPenaliteBackendServlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': 'loadTauxPenalites',
            'valueSearch': inputResearchPenalite.val(),
            'penaliteCode': selectPenalite.val()
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                printTauxPenalite(empty);
                showResponseError();
                return;

            } else if (response == '0') {

                inputTaux.html(0);
                inputNbreJourMin.html(0);
                inputNbreJourMax.html(0);

                document.getElementById('checkBoxTypeTaux').checked = false;
                document.getElementById('checkBoxActivateTauxPenalite').checked = true;
                document.getElementById('checkBoxRecidive').checked = false;

                printTauxPenalite(empty);
                
                alertify.alert('Aucun taux de  pénalite ne configurer !');
                return;

            } else {

                tauxPenaliteList = JSON.parse(JSON.stringify(response));
                printTauxPenalite(tauxPenaliteList);

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            printTauxPenalite(empty);
            showResponseError();
        }
    });

}

function printTauxPenalite(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">PENALITE</th>';
    tableContent += '<th style="text-align:left">BORNE INFERIEURE</th>';
    tableContent += '<th style="text-align:left">BORNE SUPERIEURE</th>';
    tableContent += '<th style="text-align:right">TAUX</th>';
    tableContent += '<th style="text-align:left">NATURE</th>';
    tableContent += '<th style="text-align:left">TYPE PERSONNE</th>';
    tableContent += '<th style="text-align:center">EST EN (%)</th>';
    tableContent += '<th style="text-align:center">RECIDIVE</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {


        var valueCheckPercent = 'checked';
        var valueCheckRecidive = 'checked';


        if (result[i].isPercent === false) {
            valueCheckPercent = empty;
        }

        if (result[i].isRecidive === false) {
            valueCheckRecidive = empty;
        }


        var btnDisplayTauxPenalite = '<button class="btn btn-success" onclick="displayTauxPenalite(\'' + result[i].id + '\')"><i class="fa fa-check-circle"></i></button>';
        var btnDeleteTauxPenalite = '&nbsp;<button class="btn btn-danger" onclick="deleteTauxPenalite(\'' + result[i].id + '\',\'' + result[i].penaliteName + '\')"><i class="fa fa-close"></i></button>';

        var btnAction = btnDisplayTauxPenalite + btnDeleteTauxPenalite;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + result[i].penaliteName + '</td>';
        tableContent += '<td style="text-align:left;width:12%;vertical-align:middle">' + result[i].bornInf + '</td>';
        tableContent += '<td style="text-align:left;width:12%;vertical-align:middle">' + result[i].bornSup + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle">' + result[i].taux + ' ' + result[i].typeTaux + '</td>';
        tableContent += '<td style="text-align:left;width:11%;vertical-align:middle">' + result[i].natureName + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + result[i].typePersonneName + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" disabled ' + valueCheckPercent + '></td>';
        tableContent += '<td style="text-align:center;width:8%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" disabled ' + valueCheckRecidive + '></td>';
        tableContent += '<td style="text-align:center;width:12%;vertical-align:middle">' + btnAction + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableTauxPenalite.html(tableContent);
    tableTauxPenalite.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 4
    });

}

function displayTauxPenalite(id) {

    for (var i = 0; i < tauxPenaliteList.length; i++) {

        if (tauxPenaliteList[i].id == id) {

            idSelected = id;
            typeTauxSelected = tauxPenaliteList[i].typeTaux;

            selectPenalite.val(tauxPenaliteList[i].penaliteCode);
            selectNature.val(tauxPenaliteList[i].natureCode);
            selectTypePersonne.val(tauxPenaliteList[i].typePersonneCode);

            inputTaux.val(tauxPenaliteList[i].taux);
            inputNbreJourMin.val(tauxPenaliteList[i].bornInf);
            inputNbreJourMax.val(tauxPenaliteList[i].bornSup);

            spnLblTypeTaux.html('(' + tauxPenaliteList[i].typeTaux + ')');

            if (tauxPenaliteList[i].isPercent === true) {
                document.getElementById('checkBoxTypeTaux').checked = true;
            } else {
                document.getElementById('checkBoxTypeTaux').checked = false;
            }

            if (tauxPenaliteList[i].isRecidive === true) {
                document.getElementById('checkBoxRecidive').checked = true;
            } else {
                document.getElementById('checkBoxRecidive').checked = false;
            }

            if (tauxPenaliteList[i].state === true) {
                document.getElementById('checkBoxActivateTauxPenalite').checked = true;
            } else {
                document.getElementById('checkBoxActivateTauxPenalite').checked = false;
            }


        } else {

            idSelected = empty;

            selectPenalite.val(empty);
            selectNature.val(empty);
            selectTypePersonne.val(empty);

            inputTaux.val(empty);
            inputNbreJourMin.val(empty);
            inputNbreJourMax.val(empty);

            spnLblTypeTaux.html(empty);
            document.getElementById('checkBoxTypeTaux').checked = false;
            document.getElementById('checkBoxRecidive').checked = false;
            document.getElementById('checkBoxActivateTauxPenalite').checked = false;
        }
    }
}

function resetFields() {

    idSelected = empty;
    typeTauxSelected = empty;

    initData();

    inputTaux.html(0);
    inputNbreJourMin.html(0);
    inputNbreJourMax.html(0);

    document.getElementById('checkBoxTypeTaux').checked = false;
    document.getElementById('checkBoxActivateTauxPenalite').checked = true;
    document.getElementById('checkBoxRecidive').checked = false;
}

function checkFields() {

    if (selectPenalite.val() == empty) {
        alertify.alert('Veuillez d\'abord sélectionner la pénalité');
        return;
    }

    if (selectNature.val() == empty) {

        alertify.alert('Veuillez d\'abord sélectionner la nature de la pénalité');
        return;
    }

    if (selectTypePersonne.val() == empty) {

        alertify.alert('Veuillez d\'abord sélectionner le type de personne concerncer');
        return;
    }

    if (inputTaux.val() == empty) {

        alertify.alert('Veuillez d\'abord sisir le taux de la pénalité');
        return;
    }

    if (idSelected === empty) {

        alertify.confirm('Etes-vous sûre de vouloir enregistrer ce taux de pénalité ?', function () {
            saveTauxPenalite();
        });

    } else {

        alertify.confirm('Etes-vous sûre de vouloir modifier ce taux de pénalité ?', function () {
            saveTauxPenalite();
        });
    }

}

function saveTauxPenalite() {

    var valueCheckBoxTypeTaux = false;
    var valueCheckBoxActivateTauxPenalite = false;
    var valueCheckBoxRecidive = false;

    if (checkBoxTypeTaux.is(":checked")) {
        valueCheckBoxTypeTaux = true;
    }

    if (checkBoxActivateTauxPenalite.is(":checked")) {
        valueCheckBoxActivateTauxPenalite = true;
    }

    if (checkBoxRecidive.is(":checked")) {
        valueCheckBoxRecidive = true;
    }

    $.ajax({
        type: 'POST',
        url: 'gestionPenaliteBackendServlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': 'saveTauxPenalite',
            'id': idSelected,
            'selectPenalite': selectPenalite.val(),
            'selectNature': selectNature.val(),
            //'typeTaux': typeTauxSelected,
            'selectTypePersonne': selectTypePersonne.val(),
            'inputTaux': inputTaux.val(),
            'inputNbreJourMin': inputNbreJourMin.val(),
            'inputNbreJourMax': inputNbreJourMax.val(),
            'valueCheckBoxTypeTaux': valueCheckBoxTypeTaux,
            'valueCheckBoxActivateTauxPenalite': valueCheckBoxActivateTauxPenalite,
            'valueCheckBoxRecidive': valueCheckBoxRecidive
        },
        beforeSend: function () {

            if (idSelected === empty) {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
            } else {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            }
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' || response == '0') {

                showResponseError();
                return;

            } else {

                if (idSelected === empty) {

                    alertify.alert('L\'enregistrement du taux pénalité s\'est effectué avec succès');

                } else {
                    alertify.alert('La modification du taux pénalité s\'est effectuée avec succès');
                }

                loadTauxPenalites();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function deleteTauxPenalite(id, penaliteName) {

    alertify.confirm('Etes-vous sûre de vouloir supprimer ce taux de pénalité ?', function () {

        $.ajax({
            type: 'POST',
            url: 'gestionPenaliteBackendServlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'operation': 'deleteTauxPenalite',
                'id': id
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else {

                    alertify.alert('La suppression du taux de la pénalité : ' + penaliteName + ', s\'est effectuée avec succès');
                    loadTauxPenalites();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.unblockUI();
                showResponseError();
            }
        });
    });
}

