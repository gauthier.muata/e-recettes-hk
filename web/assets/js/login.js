/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var inputUsername, inputPassword;
var btnLogin;
var modalChangePassword;
var idConnectedUserID;

var newPassword, newPasswordConfirm, btnUpdatePassword;

$(function () {

    inputUsername = $('#inputUsername');
    inputPassword = $('#InputPassword');

    newPassword = $('#newPassword');
    newPasswordConfirm = $('#newPasswordConfirm');

    btnUpdatePassword = $('#btnUpdatePassword');

    btnLogin = $('#btnLogin');

    modalChangePassword = $('#modalUpdatePassword');

    btnLogin.click(function (e) {

        e.preventDefault();

        loginUser();

    });

    btnUpdatePassword.click(function (e) {

        e.preventDefault();

        updateUserPassword();

    });
});

function loginUser() {

    if (inputUsername.val() === '') {

        alertify.alert('Veuillez fournir le nom d\'utilisateur.');
        inputUsername.focus();
        return;
    }


    if (inputPassword.val() === '') {

        alertify.alert('Veuillez fournir le mot de passe.');
        inputPassword.focus();
        return;

    }

    $.ajax({
        type: 'POST',
        url: 'connexion_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'login': inputUsername.val(),
            'password': inputPassword.val(),
            'operation': 'login'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Connexion en cours...</h5>'});

        },
        success: function (response)
        {
            if (response == '-1') {

                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = JSON.parse(JSON.stringify(response));



                if (result.session === true) {
                    if (result.changePassword === '1') {
                        idConnectedUserID = result.idUser;
                        modalChangePassword.modal('show');
                        return;
                    } else if (result.expire === true) {
                        alertify.alert('Votre compte utilisateur a expiré. Veuillez contacter l\'administrateur.');
                        return;
                    }
                    else {
                        sessionStorage.setItem('userData',CryptoJS.AES.encrypt(JSON.stringify(response), CYPHER_KEY).toString());
//                        window.location = 'dashboard';
                        window.location.replace('dashboard');
                    }

                } else {
                    alertify.alert('Vos identifiants sont incorrects. Veuillez réesayer ou contacter l\'administrateur.');
                }

            }
            , 1);

        },
        complete: function () {

        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function updateUserPassword() {

    newPassword = $('#newPassword');
    newPasswordConfirm = $('#newPasswordConfirm');

    if (newPassword.val() === '' || newPasswordConfirm.val() === '') {
        alertify.alert('Veuillez remplir tous les champs.');
        return;
    }

    if (newPassword.val() !== newPasswordConfirm.val()) {
        alertify.alert('Le mot de passe de confirmation doit être identique au nouveau mot de passe');
        return;
    }
    
    if (inputPassword.val() === newPassword.val()) {
        alertify.alert('L\'actuel et le nouveau mot de passe sont identiques.');
        return;
    }

    if (newPassword.val().length < 6) {
        alertify.alert('Le nouveau mot de passe doit avoir 6 caractères minimum');
        return;
    }

    alertify.confirm('Voulez-vous confirmer cette modification ?', function ()
    {
        $.ajax({
            type: 'POST',
            url: 'connexion_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'idUser': idConnectedUserID,
                'newPassword': newPasswordConfirm.val(),
                'currentPassword': empty,
                'operation': 'updatePassword'
            },
            beforeSend: function () {
                modalChangePassword.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Modification du mot de passe...</h5>'});
            },
            success: function (response)
            {

                setTimeout(function () {

                    modalChangePassword.unblock();

                    switch (response) {

                        case '1' :
                            alertify.alert('Modification effectuée avec succès.<br/> Connectez-vous avec votre nouveau mot de passe.');
                            modalChangePassword.modal('hide');
                            break;
                        case '0' :
                            alertify.alert('Echec de la modification du mot de passe. Veuillez ressayer.');
                            break;
                        case '-1':
                            showResponseError();
                            break;
                        default :
                            showResponseError();
                    }
                    ;
                }
                , 1000);

            },
            complete: function () {

            },
            error: function () {
                modalChangePassword.unblock();
                showResponseError();
            }

        });

    }, function () {
    });

}