/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var typeResearchRegistreRetraitDeclaration;

var responsibleObject = new Object();

var inputResearchRetraitDeclar;

var btnResearchRetraitDeclaration, btnAdvencedSearch;

var btnCallModalSearchAvandedRetraitDeclaration;

var btnSelectArticleBudgetaire;

var articleBudgetaireModal;

var tableRegistreRetraitDeclaration;

var listRetraitDeclaration;

var chkbFilterTeleDeclaration, loadOnlyDeclarationOnline;

var assujettiModal;

var avisList;

var isAdvance, lblSite, lblService, labelType, lblDateDebut, lblDateFin;

var modalReviewAmountPenalite, spnAmountPenaliteInit, cmbTauxRemise, spnAmountPenaliteRemise, btnConfirmeRemisePenalite, spnMonthLate, inputObservationRemiseTaux;

var modalPrintVignette;

var lblNoteTaxation, lblNameAssujetti, lblAdressAssujetti,
        lblRequerantControle, codeAvis, lblMereMontantPercu,
        numeroDepot;

var inputAreaObservationControlleur;

var btnValiderControle, tableDetailTaxation, cmbAvisControleur;

var tableNCChildControle, tableDFPControle;

var checkSearch = false;
var tempNCList = [];
var modalRechercheAvancee, isMensuel = false, abCode;
var sumCDF = 0, sumUSD = 0, tscr = 0, penalite = 0, impot = 0;
total = 0;
//var cmbAgent;
var totalX_USD, totalX_CDF = 0;
var isAdancedSearch;

var codeBureauSelected, codeABSelected;

var modalDisplayDetailTaxation, spnNoteTaxation, spnAmountGlobal, spnTotalBien, tableDetailNoteTaxation;
var modalPrintVignette,
        btnPrintVignette,
        inputNotePerceptionBanque, inputNumeroSerieVignette;

var codeBanqueNoteTaxation, nameBanqueNoteTaxation, codeNoteTaxation;
var tableVignette;

var bienTaxationVignetteList = [];
var listDetailTaxarion = [];
var modalTaxationDupplicaVignette;
var objTaxationDupplica = {};

var codeTaxationSelected;
var modifyAmountBienModal, inputNewAmountBien, lblDeviseNewAmount, btnValidateNewAmount;

var noteObj = new Object();

$(function () {

    loadOnlyDeclarationOnline = '0';

    mainNavigationLabel.text('DECLARATION');
    secondNavigationLabel.text('Registre des notes de taxation vignette');

    removeActiveMenu();
    linkSubMenuRegistreRetraitDeclaration.addClass('active');

    tableRegistreRetraitDeclaration = $('#tableRegistreRetraitDeclaration');

    tableDetailTaxation = $('#tableDetailTaxation');
    tableNCChildControle = $('#tableNCChildControle');
    tableDFPControle = $('#tableDFPControle');

    btnCallModalSearchAvandedRetraitDeclaration = $('#btnCallModalSearchAvandedRetraitDeclaration');

    modalPrintVignette = $('#modalPrintVignette');
    btnPrintVignette = $('#btnPrintVignette');
    inputNotePerceptionBanque = $('#inputNotePerceptionBanque');

    cmbAvisControleur = $('#cmbAvisControleur');

    btnSelectArticleBudgetaire = $('#btnSelectArticleBudgetaire');
    articleBudgetaireModal = $('#articleBudgetaireModal');

    btnResearchRetraitDeclaration = $('#btnResearchRetraitDeclaration');
    btnAdvencedSearch = $('.btnAdvencedSearch');

    btnValiderControle = $('#btnValiderControle');

    inputResearchRetraitDeclar = $('#inputResearchRetraitDeclar');

    inputAreaObservationControlleur = $('#inputAreaObservationControlleur');

    typeResearchRegistreRetraitDeclaration = $('#typeResearchRegistreRetraitDeclaration');
    chkbFilterTeleDeclaration = $('#chkbFilterTeleDeclaration');
    assujettiModal = $('#assujettiModal');

    isAdvance = $('#isAdvance');
    lblSite = $('#lblSite');
    lblService = $('#lblService');
    labelType = $('#labelType');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    lblNoteTaxation = $('#lblNoteTaxation');
    lblNameAssujetti = $('#lblNameAssujetti');
    lblAdressAssujetti = $('#lblAdressAssujetti');
    lblRequerantControle = $('#lblRequerantControle');
    lblMereMontantPercu = $('#lblMereMontantPercu');

    modalReviewAmountPenalite = $('#modalReviewAmountPenalite');
    spnAmountPenaliteInit = $('#spnAmountPenaliteInit');
    cmbTauxRemise = $('#cmbTauxRemise');
    spnAmountPenaliteRemise = $('#spnAmountPenaliteRemise');
    btnConfirmeRemisePenalite = $('#btnConfirmeRemisePenalite');
    spnMonthLate = $('#spnMonthLate');
    inputObservationRemiseTaux = $('#inputObservationRemiseTaux');
    modalRechercheAvancee = $('.modalRechercheAvancee');

    modalDisplayDetailTaxation = $('#modalDisplayDetailTaxation');
    spnNoteTaxation = $('#spnNoteTaxation');
    spnAmountGlobal = $('#spnAmountGlobal');
    spnTotalBien = $('#spnTotalBien');
    tableDetailNoteTaxation = $('#tableDetailNoteTaxation');
    inputNumeroSerieVignette = $('#inputNumeroSerieVignette');

    tableVignette = $('#tableVignette');
    modalTaxationDupplicaVignette = $('#modalTaxationDupplicaVignette');

    modifyAmountBienModal = $('#modifyAmountBienModal');
    inputNewAmountBien = $('#inputNewAmountBien');
    lblDeviseNewAmount = $('#lblDeviseNewAmount');
    btnValidateNewAmount = $('#btnValidateNewAmount');


    //noteObj = CryptoJS.AES.decrypt(sessionStorage.getItem('objDataNote'), CYPHER_KEY).toString(CryptoJS.enc.Utf8);
    noteObj = sessionStorage.getItem('objDataNote');

    if (noteObj !== null && noteObj !== 'null') {

        noteObj = JSON.parse(noteObj);

        typeResearchRegistreRetraitDeclaration.val('2');

        inputResearchRetraitDeclar.attr('readonly', false);
        inputResearchRetraitDeclar.attr('placeholder', 'Veuillez saisir le numéro de la note');
        inputResearchRetraitDeclar.attr('style', 'font-weight:normal;width: 380px');

        inputResearchRetraitDeclar.val(noteObj.code);

        loadRetraitDeclaration();

        noteObj = null;
        sessionStorage.setItem('objDataNote', null);

    }

    btnCallModalSearchAvandedRetraitDeclaration.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvancee.modal('show');
    });

    btnValidateNewAmount.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputNewAmountBien.val() == '') {

            alertify.alert('Veuillez d\'abord fournir le nouverau montant');
            return;

        } else {
            modifyAmountBienAutomobileTaxation();
        }
    });

    btnPrintVignette.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputNotePerceptionBanque.val() == '') {

            alertify.alert('Veuillez fournir la note de perception avant de lancer l\'impression');
            return;

        } else {

            var value = '<span style="font-weight:bold">' + inputNotePerceptionBanque.val().toUpperCase().trim() + '</span>';
            var valueBanque = '<span style="font-weight:bold">' + nameBanqueNoteTaxation + '</span>';
            var valueTaxation = '<span style="font-weight:bold">' + codeNoteTaxation + '</span>';

            printBienTaxationVignette('');

            $.ajax({
                type: 'GET',
                url: 'declaration_servlet',
                dataType: 'text',
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                crossDomain: true,
                data: {
                    'notePerception': inputNotePerceptionBanque.val().toUpperCase().trim(),
                    'codeBanque': codeBanqueNoteTaxation,
                    'noteTaxation': codeNoteTaxation,
                    'operation': 'checkValidateNotePerception'
                },
                beforeSend: function () {

                    modalPrintVignette.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification en cours ...</h5>'});

                },
                success: function (response)
                {

                    modalPrintVignette.unblock();

                    if (response == '-1') {
                        showResponseError();
                        return;

                    } else if (response == '1' || response == '5') {

                        alertify.alert('Cette note de perception : ' + value + ' est déjà utilisée dans le système');
                        return;

                    } else if (response == '2') {

                        alertify.alert('Cette note de perception : ' + value + ' n\'est pas affectée à la banque dénommée : ' + valueBanque);
                        return;

                    } else if (response == '3') {

                        alertify.alert('La note de taxation : ' + valueTaxation + ' n\'a pas encore été payer à la banque dénommée : ' + valueBanque);
                        return;

                    } else if (response == '4') {

                        alertify.alert('Cette note de perception : ' + value + ' n\'existe pas dans le système');
                        return;

                        /*} else if (response == '5') {
                         
                         alertify.alert('Cette note de perception : ' + value + ' n\'existe pas liée à cette taxation');
                         return;*/

                    } else if (response == '0') {
                        callPreparatePrintVignette(codeNoteTaxation);

                    }
                },
                complete: function () {
                },
                error: function (xhr, status, error) {
                    modalPrintVignette.unblock();
                    showResponseError();
                }

            });
        }
    });

    btnResearchRetraitDeclaration.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAdvance.attr('style', 'display: none');
        checkSearch = false;
        isAdancedSearch = false;

        switch (typeResearchRegistreRetraitDeclaration.val()) {
            case '1':
                assujettiModal.modal('show');
                break;
            case '2':

                if (inputResearchRetraitDeclar.val() === empty) {
                    alertify.alert('Veuillez fournir le numéro de la note de taxation avant de lancer la recherche');
                    return;
                } else {
                    loadRetraitDeclaration();
                }

                break;
        }
    });

    typeResearchRegistreRetraitDeclaration.on('change', function (e) {

        inputResearchRetraitDeclar.val(empty);

        switch (typeResearchRegistreRetraitDeclaration.val()) {
            case '1':
                inputResearchRetraitDeclar.attr('placeholder', 'Veuillez saisir le nom du contribuable');
                inputResearchRetraitDeclar.attr('readonly', true);
                inputResearchRetraitDeclar.attr('style', 'font-weight:normal;width: 380px');
                break;
            case '2':
                inputResearchRetraitDeclar.attr('readonly', false);
                inputResearchRetraitDeclar.attr('placeholder', 'Veuillez saisir le numéro de la note');
                inputResearchRetraitDeclar.attr('style', 'font-weight:normal;width: 380px');
                break;
        }
    });

    cmbAvisControleur.on('change', function (e) {
        codeAvis = cmbAvisControleur.val();
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAdvance.attr('style', 'display: block');
        checkSearch = true;
        isAdancedSearch = true;
        $('.impression').html('Imprimer le rapport (' + $('.cmbAB option:selected').text() + ')');

        declarationByAdvancedSearch();
    });

    btnValiderControle.click(function (e) {
        e.preventDefault();
        validateControleurAvis();

    });

    loadRetraitDeclarationTable('');

    chkbFilterTeleDeclaration.attr('style', 'display: inline');

    getAvis();

    $('.impression').click(function (e) {

        e.preventDefault();
        alertify.confirm('Etes-vous sûre de vouloir imprimer le registre des retraits d&eacute;claration ?', function () {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            if (tempNCList.length != 0) {
                printDataNoteCalcul();
            } else {
                alertify.alert('Aucune donnée n\'est disponnible pour imprimer ce rapport.');
            }
        });
    });

});

function handleClick(cb) {

    switch (cb.checked) {
        case true:
            loadOnlyDeclarationOnline = '1';
            break;
        case false:
            loadOnlyDeclarationOnline = '0';
            break;
    }
}

function loadRetraitDeclaration() {

    codeBureauSelected = userData.SiteCode;
    codeABSelected = '';

    var viewAllService = controlAccess('VIEW_ALL_SERVICES_NC');
    registerType = getRegisterType();

    var codeSite = userData.SiteCode;

    if (controlAccess('REGISTRE_TAXATION_USER_ALL')) {
        codeSite = '*';
    }

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': typeResearchRegistreRetraitDeclaration.val() === '1' ? responsibleObject.codeResponsible : inputResearchRetraitDeclar.val(),
            'typeSearch': typeResearchRegistreRetraitDeclaration.val(),
            'typeRegister': registerType,
            'allService': viewAllService,
            'codeSite': codeSite,
            'codeService': userData.serviceCode,
            'userId': userData.idUser,
            'dateDebut': '', //inputDateDebut.val(),
            'dateFin': '', //inputdateLast.val(),
            'operation': 'searchRetraitDeclarationVignette'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'})
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    loadRetraitDeclarationTable('');
                    alertify.alert('Aucune donnée ne corresponde au critère de recherche fournie.');
                    return;
                } else {
                    listRetraitDeclaration = null;
                    listRetraitDeclaration = JSON.parse(JSON.stringify(response));
                    loadRetraitDeclarationTable(listRetraitDeclaration);
                }
            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getSelectedAssujetiData() {

    inputResearchRetraitDeclar.val(selectAssujettiData.nomComplet);
    inputResearchRetraitDeclar.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    loadRetraitDeclaration();

}

var tempTitreList = [];
var dateLimiteEcheanceRevocationByIntercalaire, selectedDemandeEchelonnementCode;

function loadRetraitDeclarationTable(result) {

    //console.log(result)

    tempTitreList = result;

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left"></th>';
    tableContent += '<th style="text-align:left">NOTE TAXATION</th>';
    tableContent += '<th style="text-align:left"scope="col">BIEN AUTOMOBILE TAXE</th>';
    tableContent += '<th style="text-align:left"scope="col">CONTRIBUABLE</th>';
    tableContent += '<th style="text-align:left"scope="col">IMPÔT</th>';
    tableContent += '<th style="text-align:left"scope="col">DATE DECLARATION</th>';
    tableContent += '<th style="text-align:left"scope="col">ECHEANCE PAIE</th>';
    tableContent += '<th style="text-align:right"scope="col">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:left"scope="col">BANQUE</th>';
    tableContent += '<th style="text-align:left;width:8%"scope="col"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    sumCDF = 0, sumUSD = 0, tscr = 0, penalite = 0, impot = 0, total = 0;
    tempNCList = [];

    totalX_USD = 0;
    totalX_CDF = 0;

    for (var i = 0; i < result.length; i++) {

        var firstLineAB = '';
        if (result[i].libelleArticleBudgetaire.length > 250) {
            firstLineAB = result[i].libelleArticleBudgetaire.substring(0, 250).toUpperCase() + ' ...';
        } else {
            firstLineAB = result[i].libelleArticleBudgetaire.toUpperCase();
        }

        var buttonDisplayDocument = '';
        var buttonsavePayment = '';
        var buttonOperation = '';
        var callNext = false;

        if (result[i].bordereauExist === '0') {

            //buttonsavePayment = '<center><button class="btn btn-warning" onclick="callModalSavePayment(\'' + result[i].idRetraitDeclaration + '\',\'' + callNext + '\')"><i class="fa fa-list"></i></button></center>';

            if (result[i].preuvePaiementExist === '1') {
                callNext = true;
                buttonDisplayDocument = '&nbsp;<center><button class="btn btn-primary" onclick="callModalDisplayDocument(\'' + result[i].idRetraitDeclaration + '\',\'' + callNext + '\')"><i class="fa fa-print"></i></button></center>';
                buttonOperation = buttonDisplayDocument + ' ' + buttonsavePayment;
            }

        } else {
            callNext = true;
            buttonDisplayDocument = '&nbsp;<button class="btn btn-primary" onclick="callModalDisplayDocument(\'' + result[i].idRetraitDeclaration + '\',\'' + callNext + '\')"><i class="fa fa-print"></i></button>';
            buttonOperation = buttonDisplayDocument;
        }

        var descriptionBien = empty, descriptionBien2 = '';
        var btnPrintNoteTaxation = '&nbsp;<button class="btn btn-warning" title="Cliquez ici pour réimprimer la note de taxation" onclick="printNoteTaxation(\'' + result[i].numeroDepotDeclaration + '\',\'' + result[i].idRetraitDeclaration + '\')"><i class="fa fa-print"></i>&nbsp;&nbsp;Imprimer la note</button>';
        var btnCallModalDsiplayRemise = '';

        if (result[i].resmiseExist == '1') {
            btnCallModalDsiplayRemise = '<br/><br/><button class="btn btn-primary" title="Cliquez ici pour afficher les informations sur la remise de la pénalité" onclick="callModalDsiplayRemise(\'' + result[i].amountRemise + '\',\'' + result[i].tauxRemise + '\',\'' + result[i].observationRemise + '\',\'' + result[i].devise + '\',\'' + result[i].montantPercu + '\',\'' + result[i].moisRetard + '\')"><i class="fa fa-list"></i>&nbsp;Voir plus</button>';
        }

        var txtX = 'Taxateur : ';
        var agentTaxateurInfo = txtX + '<span style="font-weight:bold">' + result[i].agentTaxateur + '</span>';

        var assujettiInfo = '<span style="font-weight:bold">' + result[i].nom + '</span>';
        //var numeroDeclarationInfo = '<span style="font-weight:bold">' + result[i].numeroDepotDeclaration + '</span>';
        var color = 'green';

        var btnCallModalDisplayDetailTaxation = '';

        if (result[i].detailExist == '1') {
            btnCallModalDisplayDetailTaxation = '<br/> <br/><button class="btn btn-dafault" title="Cliquez ici pour afficher les détails de la taxation" onclick="callModalDisplayDetailTaxation(\'' + result[i].idRetraitDeclaration + '\',\'' + formatNumber(result[i].montantPercu, result[i].devise) + '\')"><i class="fa fa-list"></i>&nbsp;Voir les d&eacute;tails</button>';
        }

        var btnCallModalPrintVignette = '';

        if (controlAccess('PRINT_VIGNETTE')) {

            btnCallModalPrintVignette = '<br/> <br/><button class="btn btn-success" title="Cliquez ici pour imprimer la vignette" onclick="callModalPrintVignette(\'' + result[i].idRetraitDeclaration + '\',\'' + result[i].numeroDepotDeclaration + '\',\'' + result[i].notePerception + '\')"><i class="fa fa-print"></i>&nbsp;Imprimer la vignette</button>';
        }


        if (result[i].echeanceDepasse == '1') {
            color = 'red';
        }

        buttonOperation = btnPrintNoteTaxation + btnCallModalDsiplayRemise + btnCallModalPrintVignette + btnCallModalDisplayDetailTaxation;

        var statePayment = '';
        if (result[i].bordereauExist == '1') {
            statePayment = '<hr/>' + '<span style="color:green">' + 'payé'.toUpperCase() + '</span>';
        } else {
            statePayment = '<hr/>' + '<span style="color:red">' + 'non payé'.toUpperCase() + '</span>';
        }

        var typeTaxation = '';

        if (result[i].etat == 6) {

            if (result[i].montantPercu > 0) {
                typeTaxation = '<hr/>' + '<span style="color:green">' + 'EXO. PARTIELLE' + '</span>';
            } else {
                typeTaxation = '<hr/>' + '<span style="color:green">' + 'EXO. TOTALE' + '</span>';
            }

        }

        var bank = 'Banque : ' + '<span style="font-weight:bold">' + result[i].banqueName + '</span>';
        var accountBank = 'Compte bancaire : ' + '<br/><br/><span style="font-weight:bold">' + result[i].compteBancaireName + '</span>';
        var bankInfo = bank + '<br/><br/>' + accountBank;

        if (checkSearch == false) {
            abCode = result[i].codeArticleBudgetaire;
        }

        if (result[i].devise == 'USD') {
            totalX_USD = totalX_USD += result[i].montantPercu;
        } else {
            totalX_CDF = totalX_CDF += result[i].montantPercu;
        }

        var listDependaces = JSON.parse(result[i].listIntercalaireDetail);

        for (var j = 0; j < listDependaces.length; j++) {

            if (listDependaces[j].ETAT_ECHELONNEMENT == 4) {

                if (result[i].devise == 'USD') {
                    totalX_USD = totalX_USD += listDependaces[j].MONTANTDU;
                } else {
                    totalX_CDF = totalX_CDF += listDependaces[j].MONTANTDU;
                }
            }
        }


        var siteNameInfo = '';

        if (result[i].siteName !== '') {
            siteNameInfo = '<br/><br/>' + 'Bureau : ' + '<span style="font-weight:bold">' + result[i].siteName + '</span>';
        }

        if (result[i].devise === 'CDF') {
            sumCDF += result[i].montantPercu;
        } else if (result[i].devise === 'USD') {
            sumUSD += result[i].montantPercu;
        }
        impot += result[i].montantImpot;
        tscr += result[i].montantTscr;
        penalite = 0;
        total += result[i].montantTotal;

        tableContent += '<tr>';
        tableContent += '<td class="details-control" style="width:3%;text-align:center;vertical-align:middle"></td>';
        //tableContent += '<td style="vertical-align:middle;width:8%;">' + numeroDeclarationInfo + '<hr/>' + textTypeTaxation + '<hr/>' + othersInfoTaxation + '</td>';
        tableContent += '<td style="vertical-align:middle;width:8%;font-weight:bold;text-align:center">' + result[i].numeroDepotDeclaration + '</td>';
        tableContent += '<td style="vertical-align:middle;width:20%;font-weight:bold">' + result[i].nameBienAutomobile + '</td>';
        tableContent += '<td style="vertical-align:middle;width:16%">' + assujettiInfo + '</td>';
        tableContent += '<td style="vertical-align:middle;width:13%"><span style="font-weight:bold;"></span>' + firstLineAB + ' <hr/>' + 'EXERCICE : ' + '<span style="font-weight:bold">' + result[i].exerciceFiscal + siteNameInfo + '<br/><br/>' + agentTaxateurInfo + '</span>' + '</td>'
        tableContent += '<td style="vertical-align:middle;width:8%">' + result[i].dateCreate + '</td>';
        tableContent += '<td style="vertical-align:middle;width:8%;font-weight:bold;color:' + color + '">' + result[i].dateEcheance + '</td>';
        tableContent += '<td style="vertical-align:middle;width:8%;text-align:right;font-weight:bold">' + formatNumber(result[i].montantPercu, result[i].devise) + statePayment + typeTaxation + '</td>';
        tableContent += '<td style="vertical-align:middle;width:12%;text-align:left">' + bankInfo + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%;text-align:center">' + buttonOperation + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="7" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableRegistreRetraitDeclaration.html(tableContent);
    var tableDetail = tableRegistreRetraitDeclaration.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste des d&eacute;claration ici  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: true,
        filter: false,
        pageLength: 25,
        columnDefs: [
            {"visible": false, "targets": 3}
        ],
        order: [[3, 'asc']],
        //,lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 25,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(7).footer()).html(
                    formatNumber(totalX_CDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(totalX_USD, 'USD'));
        },
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(3, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6" class="group"><td colspan="11">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableRegistreRetraitDeclaration tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = tableDetail.row(tr);

        var dataDetail = tableDetail.row(tr).data();
        var typeDocument = 'NT';
        var numeroTitre = dataDetail[1];

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');
        }
        else {

            var detailTable = '<center><h4>LES NOTES DE TAXATIONS FILLES </h4><br/></center><table class="table table-bordered">';

            detailTable += '<thead><tr style="background-color:#e6ceac;color:black"><td style="text-align:center">NUMERO NOTE DE TAXATION</td><td style="text-align:center">ECHEANCE</td><td style="text-align:center">TYPE NOTE DE TAXATION</td><td style="text-align:right">MONTANT DU</td><td style="text-align:left">ETAT PAIEMENT</td><td style="text-align:center"></td></tr></thead>';

            for (var i = 0; i < tempTitreList.length; i++) {

                if (tempTitreList[i].idRetraitDeclaration == numeroTitre) {

                    var listIntercalaireDetail = JSON.parse(tempTitreList[i].listIntercalaireDetail);

                    if (listIntercalaireDetail.lenght == 0) {

                        row.child('<center><h5>Aucun titre échelonnements.</h5></center>').show();
                        tr.addClass('shown');

                        return;
                    }

                    var etat = tempTitreList[i].etat;
                    var allPayed;
                    var canProrog = false;
                    var revokFract;

                    var showProrogationAction;

                    detailTable += '<tbody>';

                    for (var j = 0; j < listIntercalaireDetail.length; j++) {

                        if (listIntercalaireDetail[j].IS_INTERET)
                            continue;

                        allPayed = false;

                        var showEditProrogationAction = canProrog ? 'inline' : 'none';

                        var colorEcheance = 'black';
                        var etatPaiement = 'non payé';

                        if (listIntercalaireDetail[j].isEchanceEchus && !listIntercalaireDetail[j].isPaid && etat != '3') {

                            canRevok = true;
                            colorEcheance = 'red';

                            if (dateLimiteEcheanceRevocationByIntercalaire == '' && !listIntercalaireDetail[j].isPaid) {
                                dateLimiteEcheanceRevocationByIntercalaire = listIntercalaireDetail[j].DATE_ECHEANCE_PAIEMENT;
                            }
                        }
                        if (listIntercalaireDetail[j].isEchanceEchus) {

                            revokFract = true;
                        }

                        if (listIntercalaireDetail[j].isPaid) {

                            allPayed = true;
                            showEditProrogationAction = 'none';
                            etatPaiement = 'payé';

                        }

                        if (listIntercalaireDetail[j].NUMERO_MANUEL == empty) {
                            showEditProrogationAction = 'none';
                            showProrogationAction = 'none';
                        }

                        var typeNT = 'NOTE DE TAXATION DE PENALITE';

                        var btnCanceledNTPenalite = '';

                        if (listIntercalaireDetail[j].ETAT_ECHELONNEMENT == 3) {
                            typeNT = 'NOTE DE TAXATION ECHELONNEE';
                        } else {

                            if (allPayed == false) {
                                if (controlAccess('SET_CANCEL_PENALITE')) {
                                    btnCanceledNTPenalite = '<br/><br/><button class="btn btn-danger" onclick="canceledPenalite(\'' + listIntercalaireDetail[j].NUMERO + '\')"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Annuler la p&eacute;nalit&eacute;</button>';
                                }
                            }
                        }

                        var btnPrintNTDepandance = '&nbsp;<button class="btn btn-primary" onclick="printNoteTaxation(\'' + listIntercalaireDetail[j].NUMERO + '\',\'' + listIntercalaireDetail[j].NUMERO + '\')"><i class="fa fa-print"></i>&nbsp;&nbsp;Imprimer la note</button>';

                        detailTable += '<tr>';
                        detailTable += '<td style="text-align:center;width:20%;font-weight:bold;vertical-align:middle">' + listIntercalaireDetail[j].NUMERO + '</td>';
                        detailTable += '<td style="text-align:center;width:20%;color:' + colorEcheance + ';vertical-align:middle;font-weight:bold"> ' + listIntercalaireDetail[j].DATE_ECHEANCE_PAIEMENT + ' </td>';
                        detailTable += '<td style="text-align:center;width:20%;vertical-align:middle"> ' + typeNT + ' </td>';
                        detailTable += '<td style="text-align:right;width:20%;font-weight:bold;vertical-align:middle">' + formatNumber(listIntercalaireDetail[j].MONTANTDU, listIntercalaireDetail[j].DEVISE) + '</td>';
                        detailTable += '<td style="text-align:center:width:20%;vertical-align:middle;font-weight:bold">' + etatPaiement.toUpperCase() + '</td>';
                        detailTable += '<td style="text-align:center:width:10%;vertical-align:middle">' + btnPrintNTDepandance + btnCanceledNTPenalite + '</td>';
                        detailTable += '</tr>';

                    }

                    /*if (!revokFract) {
                     detailTable += '</tbody><tfoot>';
                     detailTable += '<tr><th colspan="3" style="vertical-align:middle;text-align:right"><i class="fa fa-check-circle"></i><th colspan="2" style="vertical-align:middle;text-align:right"><button class="btn btn-danger" onclick="validateRevocation(\'' + numeroTitre + '\',\'' + typeDocument + '\',\'' + selectedDemandeEchelonnementCode + '\')" type="submit" id="btnRevocation"><i class="fa fa-check-circle"></i> Révoquer fractionnement</button></th></tr>';
                     detailTable += '</tfoot></table>';
                     }*/

                    detailTable += '</tbody></table>';
                    row.child(detailTable).show();

                    tr.addClass('shown');

                    break;
                }

            }
        }

    });
}

function callModalSavePayment(idRetrait, callNextStap) {
    alert('callModalSavePayment : ' + idRetrait);
}

function callModalDisplayDocument(idRetrait, callNextStap) {

    //alert('callModalDisplayDocument : ' + idRetrait);

    for (var i = 0; i < listRetraitDeclaration.length; i++) {

        if (listRetraitDeclaration[i].idRetraitDeclaration == idRetrait) {
            reprintRecepisse(listRetraitDeclaration[i].numeroDepotDeclaration);
//            initPrintData(idRetrait);
        }
    }


}

function declarationByAdvancedSearch() {

    registerType = getRegisterType();

    codeBureauSelected = $('.cmbBureau').val();
    codeABSelected = $('.cmbAB').val();

    switch ($('.cmbBureau').val()) {

        case '*':
            lblSite.text('TOUS');
            lblSite.attr('title', 'TOUS');
            break;
        default:
            lblSite.text($('cmbBureau option:selected').text().toUpperCase());
            lblSite.attr('title', $('cmbBureau option:selected').text().toUpperCase());
            break;
    }

    switch ($('.cmbBureau').val()) {

        case '*':
            lblService.text('TOUS');
            lblService.attr('title', 'TOUS');
            break;
        default:
            lblService.text($('.cmbAB option:selected').text().toUpperCase());
            lblService.attr('title', $('.cmbAB option:selected').text().toUpperCase());

            lblSite.text($('.cmbBureau option:selected').text().toUpperCase());
            lblSite.attr('title', $('.cmbBureau option:selected').text().toUpperCase());
            break;
    }

    if (loadOnlyDeclarationOnline === '0') {
        labelType.text('NORMALE');
        labelType.attr('title', 'NORMALE');
    } else {
        labelType.text('EN LIGNE');
        labelType.attr('title', 'EN LIGNE');
    }

    lblDateDebut.text(dateDebut.val());
    lblDateDebut.attr('title', dateDebut.val());

    lblDateFin.text(dateFin.val());
    lblDateFin.attr('title', dateFin.val());

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'SiteCode': $('.cmbBureau').val(),
            'codeAb': $('.cmbAB').val(),
            'mois': $('.cmbMois').val(),
            'annee': $('.cmbAnnee').val(),
            'declarationOnlineOnly': loadOnlyDeclarationOnline,
            'isMensuel': isMensuel,
            'codeAgentTaxateur': $('.cmbAgent').val(), //userData.idUser,
            'dateDebut': dateDebut.val(),
            'dateFin': dateFin.val(),
            'operation': 'SearchAvancedRetraitDeclarationVignette'
        },
        beforeSend: function () {
            modalRechercheAvancee.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                modalRechercheAvancee.unblock();


                if (response == '-1') {
                    loadRetraitDeclarationTable('');
                    showResponseError();
                    return;
                } else if (response == '0') {
                    modalRechercheAvancee.modal('hide');
                    listRetraitDeclaration = '';
                    tempNCList = [];
                    loadRetraitDeclarationTable('');
                    alertify.alert('Aucune donnée ne corresponde au critère de recherche fournie.');
                    return;
                } else {
                    modalRechercheAvancee.modal('hide');
                    listRetraitDeclaration = null;
                    tempNCList = [];
                    listRetraitDeclaration = JSON.parse(JSON.stringify(response));
                    loadRetraitDeclarationTable(listRetraitDeclaration);
                }
            });
        },
        complete: function () {
        },
        error: function () {
            loadRetraitDeclarationTable('');
            modalRechercheAvancee.unblock();
            showResponseError();
        }

    });
}

function reprintRecepisse(declaration) {

    if (declaration == '') {
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codeDepotDeclaration': declaration,
            'operation': 'reprintRecepisse'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            if (response == '0') {
                $.unblockUI();
                alertify.alert('Document non trouvé.');
                return;
            }

            setTimeout(function () {
                $.unblockUI();
                setDocumentContent(response);
                window.open('visualisation-document', '_blank');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printNoteTaxation(numero, codePeriodeDeclaration) {

    var value = '<span style="font-weight:bold">' + numero + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir imprimer la note de taxation n° ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'numero': codePeriodeDeclaration,
                'typeDoc': "NT",
                'operation': 'printNoteTaxation'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    $.unblockUI();
                    alertify.alert('Aucune note de taxation trouvée correspondant à ce numéro : ' + numero);
                    return;
                }

                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });

    });

}

function callModalPrintVignette(id, numero, notePerception) {

    var value = '<span style="font-weight:bold">' + numero + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir imprimer la vignette pour la note de taxation n° ' + value + ' ?', function () {

        for (var i = 0; i < listRetraitDeclaration.length; i++) {

            if (listRetraitDeclaration[i].numeroDepotDeclaration === numero) {

                printBienTaxationVignette('');

                codeBanqueNoteTaxation = listRetraitDeclaration[i].banqueCode;
                nameBanqueNoteTaxation = listRetraitDeclaration[i].banqueName;
                codeNoteTaxation = numero;

                if (listRetraitDeclaration[i].etat == 6) {

                    if (listRetraitDeclaration[i].montantPercu > 0) {

                        if (listRetraitDeclaration[i].notePerception == '') {

                            inputNotePerceptionBanque.val('');
                            inputNumeroSerieVignette.val('');
                            inputNotePerceptionBanque.attr('disabled', false);
                            inputNotePerceptionBanque.attr('style', 'font-weight:normal');
                            modalPrintVignette.modal('show');

                        } else {

                            inputNotePerceptionBanque.val(listRetraitDeclaration[i].notePerception);
                            inputNotePerceptionBanque.attr('disabled', true);
                            inputNotePerceptionBanque.attr('style', 'font-weight:bold');
                            modalPrintVignette.modal('show');

                            btnPrintVignette.trigger('click');
                        }

                    } else {

                        inputNotePerceptionBanque.val(numero);

                        inputNotePerceptionBanque.attr('disabled', true);
                        inputNotePerceptionBanque.attr('style', 'font-weight:bold');
                        modalPrintVignette.modal('show');

                        btnPrintVignette.trigger('click');
                    }


                } else {

                    if (listRetraitDeclaration[i].notePerception == '') {

                        inputNotePerceptionBanque.val('');
                        inputNumeroSerieVignette.val('');
                        inputNotePerceptionBanque.attr('disabled', false);
                        inputNotePerceptionBanque.attr('style', 'font-weight:normal');
                        modalPrintVignette.modal('show');

                    } else {

                        inputNotePerceptionBanque.val(listRetraitDeclaration[i].notePerception);
                        inputNotePerceptionBanque.attr('disabled', true);
                        inputNotePerceptionBanque.attr('style', 'font-weight:bold');
                        modalPrintVignette.modal('show');

                        btnPrintVignette.trigger('click');
                    }
                }

                break;

            }
        }
    });

}

function callModalDsiplayRemise(amountRemise, tauxRemise, observationRemise, devise, amount, moisRetard) {

    spnAmountPenaliteInit.html(formatNumber(amount, devise));
    cmbTauxRemise.val(tauxRemise);
    cmbTauxRemise.attr('disabled', true);
    spnAmountPenaliteRemise.html(formatNumber(amountRemise, devise));

    btnConfirmeRemisePenalite.attr('style', 'display:none');
    spnMonthLate.html(moisRetard);
    inputObservationRemiseTaux.val(observationRemise);

    modalReviewAmountPenalite.modal('show');
}

function validateControleurAvis() {

    codeAvis = cmbAvisControleur.val();

    if (codeAvis === '' || codeAvis === '0') {
        alertify.alert('Veuillez d\'abord sélectionner un avis.');
        return;
    }

    if (inputAreaObservationControlleur.val() === '' && codeAvis === '0') {
        alertify.alert('Veuillez motiver votre décision dans l\'observation.');
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir valider cette opération ?', function () {
        validateControle();
    });
}

function validateControle() {


    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'numeroDepot': numeroDepot,
            'idAvis': codeAvis,
            'observationOrdonnancement': inputAreaObservationControlleur.val(),
            'operation': 'controleTaxation'
        },
        beforeSend: function () {
            modalPrintVignette.unblock({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du contrôle du taxation en cours ...</h5>'});
        },
        success: function (response)
        {
            modalPrintVignette.unblock();
            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {
                if (response == '0') {
                    alertify.alert('Echec opération. Une erreur s\'est produite lors l\'enregistrement de la décison du contrôle');
                    return;
                } else if (response == '1') {
                    alertify.alert('La taxation est validée');

                    if (checkSearch) {
                        btnAdvencedSearch.trigger('click');
                    } else {
                        loadRetraitDeclaration();
                    }
                    modalPrintVignette.modal('hide');

                    codeAvis = '';
                    inputAreaObservationControlleur.val('');
                    cmbAvisControleur.val('0');
                }
            }, 100);
        },
        complete: function () {
        },
        error: function (xhr) {
            modalPrintVignette.unblock();
            showResponseError();
        }
    });
}

function loadDetailTaxationTable(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:right">PERIODICITE</th>';
    tableContent += '<th style="text-align:left">MOIS RETARD</th>';
    tableContent += '<th style="text-align:center">PENALITE</th>';
    tableContent += '<th style="text-align:right">TAUX</th>';
    tableContent += '<th style="text-align:right">MONTANT </th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var firstLineAB = '';

    //alert(JSON.stringify(result));

    for (var i = 0; i < result.length; i++) {

        if (result[i].libelleArticleBudgetaire.length > 40) {
            //firstLineAB = result[i].codeBudgetaire + ' / ' + result[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
            firstLineAB = result[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
        } else {
            //firstLineAB = result[i].codeBudgetaire + ' / ' + result[i].libelleArticleBudgetaire;
            firstLineAB = result[i].libelleArticleBudgetaire;
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:19%;vertical-align:middle" title="' + result[i].libelleArticleBudgetaire + '">' + firstLineAB + '</td>';
        tableContent += '<td style="text-align:right;width:6%;vertical-align:middle">' + result[i].IntitulePeriodicite + '</td>';
        tableContent += '<td style="text-align:left;width:6%;vertical-align:middle">' + result[i].moisRetard + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + result[i].amountRemise + '</td>';
        tableContent += '<td style="text-align:right;width:6%;vertical-align:middle">' + result[i].tauxRemise + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle">' + formatNumber(result[i].montantPercu, result[i].devise) + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableDetailTaxation.html(tableContent);
    tableDetailTaxation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le detail des actes générateurs est vide",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        datalength: 7
    });
}

function getAvis() {

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getAvis'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                var dataAvais = '<option value ="0">-- Sélectionner un avis --</option>';
                for (var i = 0; i < result.length; i++) {
                    dataAvais += '<option value =' + result[i].idAvis + '>' + result[i].libelleAvis + '</option>';
                }
                cmbAvisControleur.html(dataAvais);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printDataNoteCalcul() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    var columns = [], columnStyles;
    var carEspace = '';

    if (tempNCList.length > 0) {

        if (checkSearch) {
            position = 270;
        } else {
            var critere = inputResearchRetraitDeclar.val().toUpperCase();
            docTitle = carEspace.substring(0, critere.length * 2) + docTitle + ' : ' + critere;
            position = 250;
        }
        hauteur = 160;

    } else {
        position = 160;
    }

    if (abCode === AB_IF) {
        carEspace = '                                            ';
        docTitle = carEspace + "DIRECTION\nImpôt sur la superficie des propriétés foncières bâties et non bâties";
        columns = [
            {title: "DATE TAXATION", dataKey: "dateRetrait"},
            {title: "CONTRIBUABLE", dataKey: "nomComplet"},
            {title: "RANG", dataKey: "tarifName"},
            {title: "NATURE", dataKey: "nature"},
            {title: "SUPERFICIE", dataKey: "superficie"},
            {title: "NBRE. D'ETAGE", dataKey: "nbreEtage"},
            {title: "N° TAXATION", dataKey: "idRetraitDeclaration"},
            {title: "MONTANT DÛ", dataKey: "montantDu"},
            {title: "PENALITE", dataKey: "penalites"},
            //{title: "ADRESSES", dataKey: "adressePersonne"}
            {title: "ADRESSE", dataKey: "adresseProprietaire"}
        ];

        columnStyles = {
            dateRetrait: {columnWidth: 65, fontSize: 8, overflow: 'linebreak'},
            nomComplet: {columnWidth: 90, overflow: 'linebreak', fontSize: 8},
            tarifName: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            nature: {columnWidth: 55, overflow: 'linebreak', fontSize: 8},
            superficie: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            nbreEtage: {columnWidth: 85, overflow: 'linebreak', fontSize: 8},
            idRetraitDeclaration: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            montantDu: {columnWidth: 55, overflow: 'linebreak', fontSize: 8},
            penalites: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            adresseProprietaire: {columnWidth: 140, overflow: 'linebreak', fontSize: 8}
        };

    } else if (abCode == AB_IRL) {
        carEspace = '                                       ';
        if (checkSearch) {

            if ($('.cmbAnnee').val() == 'TOUTES') {
                docTitle = carEspace + '                                       DIRECTION\n                   Impot sur les revenus locatifs';
            } else {
                docTitle = carEspace + "                                       DIRECTION\n      Impot sur les revenus locatifs (" + $('.cmbAnnee').val() + ')';
            }

        } else {
            docTitle = carEspace + '                                       DIRECTION\n                   Impot sur les revenus locatifs';
        }
        columns = [
            {title: "DATE TAXATION", dataKey: "dateRetrait"},
            {title: "CONTRIBUABLE", dataKey: "nomComplet"},
            {title: "PERIODE", dataKey: "anneePeriode"},
            {title: "N° TAX.", dataKey: "idRetraitDeclaration"},
            {title: "MONTANT DÛ", dataKey: "montantDu"},
            {title: "PENALITES", dataKey: "penalites"},
            //{title: "RANG", dataKey: "tarifName"},
            {title: "ADRESSE DU BIEN", dataKey: "adresseBien"},
            {title: "BAILLEUR", dataKey: "proprietaire"},
            {title: "ADRESSE BAILLEUR", dataKey: "adresseProprietaire"}
        ];

        columnStyles = {
            dateRetrait: {columnWidth: 65, fontSize: 8, overflow: 'linebreak'},
            nomComplet: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            anneePeriode: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            idRetraitDeclaration: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            montantDu: {columnWidth: 60, overflow: 'linebreak', fontSize: 8},
            penalites: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            tarifName: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            adresseBien: {columnWidth: 120, overflow: 'linebreak', fontSize: 8},
            proprietaire: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            adresseProprietaire: {columnWidth: 120, overflow: 'linebreak', fontSize: 8}
        };

    } else if (abCode === AB_RL) {
        carEspace = '                                       ';
        if (checkSearch) {
            //docTitle = carEspace + "DIRECTION\n        Impot sur les revenus locatifs (" + $('.cmbMois').val() + '/' + $('.cmbAnnee').val() + ')';
            docTitle = carEspace + 'DIRECTION\n                   Impot sur les revenus locatifs (RETENUE LOCATIVE)';
        } else {
            docTitle = carEspace + 'DIRECTION\n                   Impot sur les revenus locatifs (RETENUE LOCATIVE)';
        }

        columns = [
            {title: "DATE TAXATION", dataKey: "dateRetrait"},
            {title: "REDEVABLE", dataKey: "nomComplet"},
            {title: "PERIODE", dataKey: "intitulePeriodicite"},
            {title: "N° TAX.", dataKey: "idRetraitDeclaration"},
            {title: "MONTANT DÛ", dataKey: "montantDu"},
            {title: "PENALITE", dataKey: "penalites"},
            //{title: "RANG", dataKey: "tarifName"},
            {title: "ADRESSE DU BIEN", dataKey: "adresseBien"},
            {title: "BAILLEUR", dataKey: "proprietaire"},
            {title: "ADRESSE BAILLEUR", dataKey: "adresseProprietaire"}
        ];

        columnStyles = {
            dateRetrait: {columnWidth: 65, fontSize: 8, overflow: 'linebreak'},
            nomComplet: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            intitulePeriodicite: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            idRetraitDeclaration: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            montantDu: {columnWidth: 60, overflow: 'linebreak', fontSize: 8},
            penalites: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            tarifName: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            adresseBien: {columnWidth: 120, overflow: 'linebreak', fontSize: 8},
            proprietaire: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            adresseProprietaire: {columnWidth: 120, overflow: 'linebreak', fontSize: 8}
        };
    } else if (abCode == AB_VIGNETTE) {

        carEspace = '                                                  ';
        docTitle = carEspace + "DIVISION DES IMPOTS\nRECETTES ORDONNANCEES DE L\'IMPOT SUR LES VEHICULES ET DE LA TAXE\n\
                                   SPECIALE DE CIRCULATION ROUTIERE\n\
                                                                   " + $('.cmbAnnee').val();

        hauteur = 185;

        columns = [
            {title: "DATE TAXATION", dataKey: "dateRetrait"},
            {title: "CONTRIBUABLE", dataKey: "nomComplet"},
            {title: "NOTE", dataKey: "note"},
            {title: "MARQUE", dataKey: "marque"},
            {title: "MODELE", dataKey: "modele"},
            {title: "GENRE", dataKey: "libelleTypeBien"},
            {title: "PLAQUE", dataKey: "plaque"},
            {title: "IMPOT", dataKey: "montantImpot"},
            {title: "TSCR", dataKey: "montantTscr"},
            {title: "PENALITE", dataKey: "penalites"},
            {title: "MONT. TOT", dataKey: "montantTotal"},
            {title: "ADRESSE", dataKey: "adresseProprietaire"}
        ];

        columnStyles = {
            dateRetrait: {columnWidth: 65, fontSize: 8, overflow: 'linebreak'},
            nomComplet: {columnWidth: 90, overflow: 'linebreak', fontSize: 8},
            note: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            marque: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            modele: {columnWidth: 60, overflow: 'linebreak', fontSize: 8},
            libelleTypeBien: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            plaque: {columnWidth: 60, overflow: 'linebreak', fontSize: 8},
            montantImpot: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            montantTscr: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            penalites: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            montantTotal: {columnWidth: 60, overflow: 'linebreak', fontSize: 8},
            adresseProprietaire: {columnWidth: 120, overflow: 'linebreak', fontSize: 7}
        };
    } else {
        carEspace = '                                       ';
        docTitle = carEspace + 'DIRECTION\n                   Rapport des notes de taxation';
        columns = [
            {title: "NOTE TAXATION", dataKey: "noteTaxation"},
            {title: "BIEN", dataKey: "bien"},
            {title: "IMPÔT", dataKey: "articleBudgetaire"},
            {title: "DATE TAXATION", dataKey: "dateRetrait"},
            {title: "ECHEANCE PAIE", dataKey: "echeancePaie"},
            {title: "MONTANT DÛ", dataKey: "montantDu"},
            {title: "BANQUE", dataKey: "banque"}];

        columnStyles = {
            noteTaxation: {columnWidth: 85, fontSize: 8, overflow: 'linebreak'},
            bien: {columnWidth: 160, overflow: 'linebreak', fontSize: 8},
            articleBudgetaire: {columnWidth: 120, overflow: 'linebreak', fontSize: 9},
            dateRetrait: {columnWidth: 75, overflow: 'linebreak', fontSize: 8},
            echeancePaie: {columnWidth: 85, overflow: 'linebreak', fontSize: 8},
            montantDu: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            banque: {columnWidth: 180, overflow: 'linebreak', fontSize: 9}
        };
    }

    var rows = tempNCList;
    var pageContent = function (data) {

        var bureau = $('.cmbBureau option:selected').text();
        setDocParams(doc, docTitle, position, day, month, year, hh, mm, ss, data, bureau);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: columnStyles,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 5 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');

    if (abCode == AB_VIGNETTE) {
        doc.text("TSCR :  " + formatNumber(tscr, 'USD'), 540, finalY + 40);
        doc.text("IMPOT :  " + formatNumber(impot, 'USD'), 540, finalY + 55);
        doc.text("MONTANT TOTAL :  " + formatNumber(total, 'USD'), 540, finalY + 70);
        doc.text("PENALITE :  " + formatNumber(penalite, 'USD'), 540, finalY + 85);
    } else {
        doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(totalX_CDF), 540, finalY + 25);
        doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(totalX_USD), 540, finalY + 40);
    }

    doc.text(getNameChefBureau(codeBureauSelected), 540, finalY + 65);
    doc.text(getNameChefTaxateur(codeBureauSelected, codeABSelected), 40, finalY + 65);

    window.open(doc.output('bloburl'), '_blank');
}

function canceledPenalite(id) {

    alertify.confirm('Etes-vous sûre de vouloir annuler cette pénalité ?', function () {

        $.ajax({
            type: 'POST',
            url: 'assujetissement_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'codeTaxation': id,
                'operation': 'canceledPenalite'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Annulation de la pénalité en cours ...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else {

                    setTimeout(function () {

                        alertify.alert('L\'annulation de la pénalité s\'est effectuée avec succès');

                        if (isAdancedSearch == true) {
                            btnAdvencedSearch.trigger('click');
                        } else {
                            btnResearchRetraitDeclaration.trigger('click');
                        }

                    }, 1);
                }
            },
            complete: function () {
            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });

    });
}

function printDetailNoteTaxation(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">BIEN</th>';
    tableContent += '<th style="text-align:left">BIEN(S) AUTOMOBILE TAXE(S)</th>';
    tableContent += '<th style="text-align:right">PRINCIPAL DÛ</th>';
    tableContent += '<th style="text-align:right">PENALITE DÛ</th>';
    tableContent += '<th style="text-align:right"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var amountTotal = 0;
    var devise;
    var totalBienTaxe = 0;

    for (var i = 0; i < result.length; i++) {

        totalBienTaxe++;

        amountTotal = amountTotal += (result[i].montant + result[i].penalite);

        devise = result[i].devise;

        var nameInfo = 'Bien automobile : ' + '<span style="font-weight:bold">' + result[i].bienName + '</span>';
        var genreBienInfo = 'Genre : ' + '<span style="font-weight:bold">' + result[i].natureBien + '</span>';
        var categorieBienInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].nameTarifBien + '</span>';
        var adresseInfo = 'Adresse : ' + '<span style="font-weight:bold">' + result[i].adresseBien.toUpperCase() + '</span>';
        var plaqueInfo = '№ Plaque : ' + '<span style="font-weight:bold;color:red">' + result[i].plaque + '</span>';

        var bienInfo = nameInfo + '<br/>' + plaqueInfo + '<br/>' + genreBienInfo + '<br/>' + categorieBienInfo + '<br/>' + adresseInfo;

        var btnCallModalDupplicatVignette = '';

        if (result[i].vignetteExist == '1' || result[i].vignetteExist == '2') {

            if (controlAccess('SET_TAXATION_DUPLICAT_VIGNETTE')) {
                if (result[i].state !== 5 | result[i].state !== 6) {
                    btnCallModalDupplicatVignette = '<button class="btn btn-warning" title="" onclick="callModalDupplicatVignette(\'' + result[i].bienCode + '\',\'' + result[i].code + '\')"><i class="fa fa-calculator fa-1x"></i>&nbsp;Taxer Duplicata</button>';
                }

            }

        }


        var btnDeleteBienAutomobileTaxation = '';
        var btnRemplacerBienAutomobileTaxation = '';
        var btnModifyAmountBienAutomobileTaxation = '';

        if (result[i].codeAB == AB_VIGNETTE) {
            if (controlAccess('DELETE_BIEN_AUTOMOBILE_TAXATION')) {
                if (result[i].vignetteExist == '0') {
                    btnDeleteBienAutomobileTaxation = '&nbsp;&nbsp;<button class="btn btn-danger" title="" onclick="deleteBienAutomobileTaxation(\'' + result[i].bienCode + '\',\'' + result[i].bienName + '\',\'' + result[i].periodeCode + '\')"><i class="fa fa-trash-o"></i>&nbsp;Retirer</button>';
                    btnRemplacerBienAutomobileTaxation = '&nbsp;&nbsp;<button class="btn btn-primary" title="" onclick="remplacerBienAutomobileTaxation(\'' + result[i].bienCode + '\',\'' + result[i].bienName + '\',\'' + result[i].periodeCode + '\',\'' + result[i].codePersonne + '\')"><i class="fa fa-check-circle"></i>&nbsp;Remplacer</button>';
                    btnModifyAmountBienAutomobileTaxation = '<br/><br/><button class="btn btn-success" title="" onclick="callModifyAmountBienAutomobileTaxation(\'' + result[i].bienCode + '\',\'' + result[i].bienName + '\',\'' + result[i].periodeCode + '\',\'' + result[i].devise + '\')"><i class="fa fa-check-circle"></i>&nbsp;Modifier montant</button>';
                }

            }
        }

        if (result[i].codeAB == AB_VIGNETTE) {
            if (controlAccess('DELETE_BIEN_AUTOMOBILE_TAXATION')) {
                if (result[i].vignetteExist == '2') {
                    btnDeleteBienAutomobileTaxation = '';
                    btnRemplacerBienAutomobileTaxation = '&nbsp;&nbsp;<button class="btn btn-primary" title="" onclick="remplacerBienAutomobileTaxation(\'' + result[i].bienCode + '\',\'' + result[i].bienName + '\',\'' + result[i].periodeCode + '\',\'' + result[i].codePersonne + '\')"><i class="fa fa-check-circle"></i>&nbsp;Remplacer</button>';
                    btnModifyAmountBienAutomobileTaxation = '<br/><br/><button class="btn btn-success" title="" onclick="callModifyAmountBienAutomobileTaxation(\'' + result[i].bienCode + '\',\'' + result[i].bienName + '\',\'' + result[i].periodeCode + '\',\'' + result[i].devise + '\')"><i class="fa fa-check-circle"></i>&nbsp;Modifier montant</button>';
                }

            }
        }


        tableContent += '<tr>';

        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + bienInfo + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold">' + result[i].periodeName + '</td>';
        tableContent += '<td style="text-align:right;width:8%;vertical-align:middle;font-weight:bold;font-size:16px">' + formatNumber(result[i].montant, result[i].devise) + btnModifyAmountBienAutomobileTaxation + '</td>';
        tableContent += '<td style="text-align:right;width:8%;vertical-align:middle;font-weight:bold;font-size:16px">' + formatNumber(result[i].penalite, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:center;width:25%;vertical-align:middle">' + btnCallModalDupplicatVignette + btnDeleteBienAutomobileTaxation + btnRemplacerBienAutomobileTaxation + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableDetailNoteTaxation.html(tableContent);
    tableDetailNoteTaxation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le detail des actes générateurs est vide",
            search: "Filtre la liste des biens ici ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"}
        },
        info: false,
        destroy: true,
        searching: true,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 5,
        datalength: 7, columnDefs: [
            {"visible": false, "targets": 0}
        ],
        order: [[0, 'asc']],
        //,lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 25, drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(0, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6" class="group"><td colspan="4">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    spnAmountGlobal.html(formatNumber(amountTotal, devise));
    spnTotalBien.html(totalBienTaxe);
    modalDisplayDetailTaxation.modal('show');
}

function callModalDisplayDetailTaxation(id, montant) {

    var value = '<span style="font-weight:bold">' + id + '</span>';
    codeTaxationSelected = id;

    alertify.confirm('Etes-vous sûre de vouloir afficher les détails de la taxation n° ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'assujetissement_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'codeTaxation': id,
                'operation': 'displayDetailTaxation'},
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Affichage des détails en cours ...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1' || response == '0') {
                    printDetailNoteTaxation('');
                    showResponseError();
                    return;

                } else {
                    listDetailTaxarion = JSON.parse(JSON.stringify(response));

                    spnNoteTaxation.html(id);
                    //spnAmountGlobal.html(montant);

                    printDetailNoteTaxation(listDetailTaxarion);
                }
            },
            complete: function () {
            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });

    });
}

function callPreparatePrintVignette(noteTaxation) {

    $.ajax({
        type: 'GET',
        url: 'declaration_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'noteTaxation': noteTaxation,
            'userId': userData.idUser,
            'operation': 'getInfoTaxationVignette'
        },
        beforeSend: function () {

            modalPrintVignette.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification en cours ...</h5>'});

        },
        success: function (response)
        {

            modalPrintVignette.unblock();

            if (response == '-1' || response == '0') {

                printBienTaxationVignette('');
                showResponseError();
                return;

            } else {
                bienTaxationVignetteList = JSON.parse(JSON.stringify(response));
                printBienTaxationVignette(bienTaxationVignetteList);

            }
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            modalPrintVignette.unblock();
            showResponseError();
        }

    });
}

function printBienTaxationVignette(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center">N°</th>';
    tableContent += '<th style="text-align:left">BIEN AUTOMOBILE TAXE</th>';
    tableContent += '<th style="text-align:left">DESCRIPTION DU BIEN</th>';
    tableContent += '<th style="text-align:left">TAUX VIGNETTE</th>';
    tableContent += '<th style="text-align:center">№ SERIE</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var count = 0;
    for (var i = 0; i < result.length; i++) {

        count++;

        var categorieInfo = 'CATEGORIE : ' + '<span style="font-weight:bold">' + result[i].categorie + '</span>';
        var typeInfo = 'MODELE : ' + '<span style="font-weight:bold">' + result[i].modele + '</span>';
        var marqueInfo = 'MARQUE : ' + '<span style="font-weight:bold">' + result[i].marque + '</span>';
        var plaqueInfo = '№ PLAQUE : ' + '<span style="font-weight:bold">' + result[i].plaque + '</span>';
        var chassisInfo = '№ CHASSIS : ' + '<span style="font-weight:bold">' + result[i].chassis + '</span>';
        var puissanceInfo = 'PUISSANCE FISCALE : ' + '<span style="font-weight:bold">' + result[i].puissanceFiscal + ' ' + result[i].unitePuissanceFiscal + '</span>';

        var plaqueBienInfo = plaqueInfo + '&nbsp;&nbsp;&nbsp;&nbsp; | ' + chassisInfo;

        var descriptionInfo = categorieInfo + '<br/>' + marqueInfo + '<br/>' + typeInfo + '<br/>' + '<br/>' + puissanceInfo;

        var principalInfo = 'PRINCIPAL DÛ : ' + '<span style="font-weight:bold;color:green;font-size:16px">' + formatNumber(result[i].principal, result[i].devise) + '</span>';

        var totalDuInfo = '';
        var penaliteInfo = '';

        if (result[i].penalite > 0) {

            penaliteInfo = 'PENALITE DÛ : ' + '<span style="font-weight:bold;color:red">' + formatNumber(result[i].penalite, result[i].devise) + '</span>';
            totalDuInfo = 'TOTAL DÛ : ' + '<span style="font-weight:bold;color:black;font-size:16px">' + formatNumber((result[i].principal + result[i].penalite), result[i].devise) + '</span>';
        }

        var tauxVignetteInfo = principalInfo + '<br/><br/>' + penaliteInfo + '<br/><br/>' + totalDuInfo;

        var denomination = 'DENOMINATION : ' + '<span style="font-weight:bold">' + result[i].bienName + '</span>';
        var nombreImpressionInfo = 'NBRE. IMPRESSION : ' + '<span style="font-weight:bold;font-size:17px;color:red">' + result[i].numberPrint + '</span>';

        var denominationInfo = denomination + '<br/><br/>' + plaqueBienInfo + '<br/><br/>' + nombreImpressionInfo;

        var btnPrintVignette = '';

        if (controlAccess('PRINT_VIGNETTE')) {
            if (result[i].printable == 1) {

                btnPrintVignette = '<button type="button" class="btn btn-warning" title="Cliquez ici pour imprimer la vignette" onclick="printVignetteX(\'' + result[i].bienCode + '\',\'' + result[i].bienName + '\',\'' + result[i].numeroSerieVignette + '\')"><i class="fa fa-print"></i>&nbsp;Imprimer</button>';
            }
        }


        var inputSerieVignette = '';

        if (result[i].numeroSerieVignette == '') {

            inputSerieVignette = '<input style="font-weight:bold;font-size:16px;color:green" height="40px" type="text" id="inputSerieVignette_' + result[i].bienCode + '" onchange="checkValidateSerieVignette(\'' + result[i].bienCode + '\')">';

        } else {
            inputSerieVignette = '<span style="font-weight:bold;font-size:16px;color:green">' + result[i].numeroSerieVignette + '</span>';
        }

        tableContent += '<tr>';

        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + count + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + denominationInfo + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + descriptionInfo + '</td>';
        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + tauxVignetteInfo + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + inputSerieVignette + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + btnPrintVignette + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableVignette.html(tableContent);
    tableVignette.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "La liste des biens automobile est vide",
            search: "Filtre la liste des biens ici ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"}
        },
        info: false,
        destroy: true,
        searching: true,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 20,
        datalength: 7, columnDefs: [
            {"visible": false, "targets": 0}
        ],
        order: [[0, 'asc']],
        //,lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        }
    });
}

function printVignetteX(codeBien, nameBien, vignette) {

    var valueNameBien = '';
    if (vignette == '') {

        var inputSerieVignette = "#inputSerieVignette_" + codeBien;
        var serieVignette = $('#tableVignette').find(inputSerieVignette).val().toUpperCase();

        valueNameBien = '<span style="font-weight:bold">' + nameBien + '</span>';

        if (serieVignette == null || serieVignette == '') {

            //inputSerieVignette.attr('style', 'background-color:#daf2c9');

            serieVignette = $('#tableVignette').find(inputSerieVignette).attr('style', 'background-color:#FFF380;font-weight:bold;font-size:16px;color:green');
            //serieVignette = $('#tableVignette').find(inputSerieVignette).attr('style', 'font-weight:bold;font-size:16px;color:green');

            alertify.alert('Veuillez d\'abord fournir la quittance vignete pour le bien automobile : ' + valueNameBien);
            return;

        }
    } else {
        valueNameBien = '<span style="font-weight:bold">' + nameBien + '</span>';
    }


    alertify.confirm('Etes-vous sûre de vouloir imprimer la vignette pour le bien automobile : ' + valueNameBien + ' ?', function () {

        for (var i = 0; i < bienTaxationVignetteList.length; i++) {


            if (bienTaxationVignetteList[i].bienCode == codeBien) {

                if (bienTaxationVignetteList[i].autoriserPrint == false) {

                    valueNameBien = '<span style="font-weight:bold">' + bienTaxationVignetteList[i].bienName + '</span>';
                    alertify.alert('Veuillez d\'abord fournir la quittance vignete valide pour le bien automobile : ' + valueNameBien);
                    return;

                } else {

                    /*$.ajax({
                     type: 'POST',
                     url: 'declaration_servlet',
                     dataType: 'Text',
                     crossDomain: false,
                     data: {
                     'serieVignette': vignette,
                     'operation': 'generateCodeQr'
                     },
                     beforeSend: function () {
                     modalPrintVignette.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'})
                     },
                     success: function (response)
                     {
                     setTimeout(function () {
                     
                     modalPrintVignette.unblock();
                     
                     bienTaxationVignetteList[i].codeQr = response;
                     
                     sessionStorage.setItem('objDataVignette', CryptoJS.AES.encrypt(JSON.stringify(bienTaxationVignetteList[i]), CYPHER_KEY).toString());
                     window.open('visualisation-vignette', '_blank');
                     
                     });
                     
                     },
                     error: function (jqXHR, textStatus, errorThrown) {
                     modalPrintVignette.unblock();
                     showResponseError();
                     }
                     
                     });*/

                    sessionStorage.setItem('objDataVignette', CryptoJS.AES.encrypt(JSON.stringify(bienTaxationVignetteList[i]), CYPHER_KEY).toString());
                    window.open('visualisation-vignette', '_blank');
                }

                break;
            }
        }
    });
}

function checkValidateSerieVignette(codeBien) {

    var inputSerieVignette = "#inputSerieVignette_" + codeBien;
    var serieVignette = $('#tableVignette').find(inputSerieVignette).val().toUpperCase();

    var value = '<span style="font-weight:bold">' + serieVignette + '</span>';

    $.ajax({
        type: 'GET',
        url: 'declaration_servlet',
        dataType: 'Text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'serieVignette': serieVignette,
            'operation': 'checkValidateSerieVignette'
        },
        beforeSend: function () {

            modalPrintVignette.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification en cours ...</h5>'});

        },
        success: function (response)
        {

            modalPrintVignette.unblock();

            if (response == '-1') {
                showResponseError();
                return;

            } else if (response == '1') {
                alertify.alert('DESOLE ! La Quittance n° ' + value + ' est déjà utilisée dans le système');
                return;
            } else {

                for (var i = 0; i < bienTaxationVignetteList.length; i++) {


                    if (bienTaxationVignetteList[i].bienCode == codeBien) {

                        bienTaxationVignetteList[i].numeroSerieVignette = serieVignette;
                        bienTaxationVignetteList[i].autoriserPrint = true;
                        //bienTaxationVignetteList[i].codeQr = response;

                        if (bienTaxationVignetteList[i].notePerception == '') {

                            bienTaxationVignetteList[i].notePerception = inputNotePerceptionBanque.val().toUpperCase().trim();
                        }

                        break;
                    }
                }
            }
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            modalPrintVignette.unblock();
            showResponseError();
        }

    });

}

function callModalDupplicatVignette(bienCode, noteTaxation) {

    for (var i = 0; i < listDetailTaxarion.length; i++) {

        if (listDetailTaxarion[i].bienCode == bienCode) {

            var listTauxVignettes = JSON.parse(userData.listTauxVignettes);

            for (var j = 0; j < listTauxVignettes.length; j++) {

                var tauxVignette = 0;
                var tauxTscr = 0;
                var tauxTotal = 0;

                if (listTauxVignettes[j].codeTarif == listDetailTaxarion[i].codeTarifBien) {


                    if (listTauxVignettes[j].codeForme !== '*') {

                        if (listTauxVignettes[j].codeForme == listDetailTaxarion[i].codFj) {

                            if (listTauxVignettes[j].vignette > 0) {
                                tauxVignette = ((listTauxVignettes[j].vignette * listTauxVignettes[j].percentVignette) / 100);
                            }

                            if (listTauxVignettes[j].tscr > 0) {
                                tauxTscr = ((listTauxVignettes[j].tscr * listTauxVignettes[j].percentTscr) / 100);
                            }
                            tauxTotal = (tauxVignette + tauxTscr);
                            var amountTaxation = '<span style="font-weight:bold;color:green">' + formatNumber((tauxVignette + tauxTscr), listDetailTaxarion[i].devise) + '</span>';

                            objTaxationDupplica = {};

                            objTaxationDupplica.amount = tauxTotal;
                            objTaxationDupplica.devise = listDetailTaxarion[i].devise;
                            objTaxationDupplica.codeAB = listDetailTaxarion[i].codeAB;
                            objTaxationDupplica.requerant = listDetailTaxarion[i].namePersonne;
                            objTaxationDupplica.codePersonne = listDetailTaxarion[i].codePersonne;
                            objTaxationDupplica.codeBanque = listDetailTaxarion[i].codeBanque;
                            objTaxationDupplica.codeCompte = listDetailTaxarion[i].codeCompte;
                            objTaxationDupplica.periodeCode = listDetailTaxarion[i].periodeCode;
                            objTaxationDupplica.codeAgent = userData.idUser;
                            objTaxationDupplica.codeSite = userData.SiteCode;
                            objTaxationDupplica.newId = listDetailTaxarion[i].newID;
                            objTaxationDupplica.noteTaxationMere = listDetailTaxarion[i].code;
                            objTaxationDupplica.codeBien = listDetailTaxarion[i].bienCode;
                            objTaxationDupplica.codeQr = listDetailTaxarion[i].codeQr;

                            valueDenomination = '<span style="font-weight:bold">' + listDetailTaxarion[i].bienName + '</span>';

                            alertify.confirm('Etes-vous sûre de vouloir taxer la demande de Duplicata Vignette pour ce bien : ' + valueDenomination + ' <br/><br/>' + ' Avec ce Montant Dû : ' + amountTaxation + ' ?', function () {

                                saveTaxationDupplicatVignette();

                            });

                            break;

                        }
                    } else {

                        if (listTauxVignettes[j].vignette > 0) {
                            tauxVignette = ((listTauxVignettes[j].vignette * listTauxVignettes[j].percentVignette) / 100);
                        }

                        if (listTauxVignettes[j].tscr > 0) {
                            tauxTscr = ((listTauxVignettes[j].tscr * listTauxVignettes[j].percentTscr) / 100);
                        }

                        tauxTotal = (tauxVignette + tauxTscr);
                        var amountTaxation = '<span style="font-weight:bold;color:green">' + formatNumber((tauxVignette + tauxTscr), listDetailTaxarion[i].devise) + '</span>';

                        if (listDetailTaxarion[i].devise == 'CDF') {
                            tauxTotal = ((tauxVignette + tauxTscr) * userData.lastTauxChange);
                            amountTaxation = '<span style="font-weight:bold;color:green">' + formatNumber(((tauxVignette + tauxTscr) * userData.lastTauxChange), listDetailTaxarion[i].devise) + '</span>';
                        }
                        objTaxationDupplica = {};

                        objTaxationDupplica.amount = tauxTotal;
                        objTaxationDupplica.devise = listDetailTaxarion[i].devise;
                        objTaxationDupplica.codeAB = listDetailTaxarion[i].codeAB;
                        objTaxationDupplica.requerant = listDetailTaxarion[i].namePersonne;
                        objTaxationDupplica.codePersonne = listDetailTaxarion[i].codePersonne;
                        objTaxationDupplica.codeBanque = listDetailTaxarion[i].codeBanque;
                        objTaxationDupplica.codeCompte = listDetailTaxarion[i].codeCompte;
                        objTaxationDupplica.periodeCode = listDetailTaxarion[i].periodeCode;
                        objTaxationDupplica.codeAgent = userData.idUser;
                        objTaxationDupplica.codeSite = userData.SiteCode;
                        objTaxationDupplica.newId = listDetailTaxarion[i].newID;
                        objTaxationDupplica.noteTaxationMere = listDetailTaxarion[i].code;
                        objTaxationDupplica.codeBien = listDetailTaxarion[i].bienCode;
                        objTaxationDupplica.codeQr = listDetailTaxarion[i].codeQr;

                        valueDenomination = '<span style="font-weight:bold">' + listDetailTaxarion[i].bienName + '</span>';

                        alertify.confirm('Etes-vous sûre de vouloir taxer la demande de Duplicata Vignette pour ce bien : ' + valueDenomination + ' <br/><br/>' + ' Avec ce Montant Dû : ' + amountTaxation + ' ?', function () {

                            saveTaxationDupplicatVignette();

                        });

                        break;
                    }

                    //alert(JSON.stringify(listTauxVignettes[j]));

                }

            }
            break;
        }

    }
}

function saveTaxationDupplicatVignette() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'amount': objTaxationDupplica.amount,
            'devise': objTaxationDupplica.devise,
            'codeAB': objTaxationDupplica.codeAB,
            'requerant': objTaxationDupplica.requerant,
            'codePersonne': objTaxationDupplica.codePersonne,
            'codeBanque': objTaxationDupplica.codeBanque,
            'codeCompte': objTaxationDupplica.codeCompte,
            'periodeCode': objTaxationDupplica.periodeCode,
            'codeAgent': objTaxationDupplica.codeAgent,
            'codeSite': objTaxationDupplica.codeSite,
            'newId': objTaxationDupplica.newId,
            'noteTaxationMere': objTaxationDupplica.noteTaxationMere,
            'codeBien': objTaxationDupplica.codeBien,
            'operation': 'saveTaxationDupplicatVignette'
        },
        beforeSend: function () {
            modalDisplayDetailTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Taxation duplicata en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalDisplayDetailTaxation.unblock();
                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    alertify.alert('La taxation du duplicata Vignette du bien dénommé : ' + valueDenomination + ' s\'est effectuée avec succès');
                    objTaxationDupplica = {};

                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalDisplayDetailTaxation.unblock();
            showResponseError();
        }
    });
}

function remplacerBienAutomobileTaxation(codeBien, bienName, periodeCode, codePersonne) {

    var value = '<span style="font-weight:bold">' + bienName + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir remplacer ce bien : ' + value + ' <br/><br/>' + ' dans cette taxation ?', function () {
        window.location = 'remplacer-bien-automobile-taxation?id=' + btoa(codeBien) + '&contribuable=' + btoa(codePersonne) + '&exercice=' + btoa(periodeCode) + '&taxation=' + btoa(noteTaxationCurrent);
    });

}

function deleteBienAutomobileTaxation(codeBien, bienName, periodeCode) {

    var value = '<span style="font-weight:bold">' + bienName + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir supprimer ce bien : ' + value + ' <br/><br/>' + ' dans cette taxation ?', function () {

        $.ajax({
            type: 'POST',
            url: 'assujetissement_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'noteTaxation': codeTaxationSelected,
                'periodeCode': periodeCode,
                'codeBien': codeBien,
                'userId': userData.idUser,
                'operation': 'deleteBienAutomobileTaxation'},
            beforeSend: function () {
                modalDisplayDetailTaxation.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
            },
            success: function (response)
            {
                setTimeout(function () {

                    modalDisplayDetailTaxation.unblock();

                    if (response == '-1' | response == '0') {

                        showResponseError();
                        return;

                    } else {

                        alertify.alert('La suppression du bien dénommé : ' + value + ' s\'est effectuée avec succès');
                        callModalDisplayDetailTaxationV2(codeTaxationSelected);
                    }
                }
                , 1);
            },
            complete: function () {
            },
            error: function () {
                modalDisplayDetailTaxation.unblock();
                showResponseError();
            }
        });

    });
}

var noteTaxationCurrent;

function callModalDisplayDetailTaxationV2(id) {

    noteTaxationCurrent = id;

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codeTaxation': id,
            'operation': 'displayDetailTaxation'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Affichage des détails en cours ...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' || response == '0') {
                printDetailNoteTaxation('');
                showResponseError();
                return;

            } else {

                listDetailTaxarion = JSON.parse(JSON.stringify(response));

                spnNoteTaxation.html(id);
                printDetailNoteTaxation(listDetailTaxarion);
            }
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });

}

var codeBienModify, bienNameModify, periodeCodeModify, deviseAmountModify;

function modifyAmountBienAutomobileTaxation() {

    var value = '<span style="font-weight:bold">' + bienNameModify + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir modifier le montant de ce bien : ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'assujetissement_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'noteTaxation': codeTaxationSelected,
                'periodeCode': periodeCodeModify,
                'codeBien': codeBienModify,
                'amountBien': inputNewAmountBien.val(),
                'userId': userData.idUser,
                'operation': 'modifyAmountBienAutomobileTaxation'},
            beforeSend: function () {
                modifyAmountBienModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                setTimeout(function () {

                    modifyAmountBienModal.unblock();

                    if (response == '-1' | response == '0') {

                        showResponseError();
                        return;

                    } else {

                        alertify.alert('La modification du montant du bien dénommé : ' + value + ' s\'est effectuée avec succès');

                        modalDisplayDetailTaxation.modal('hide');
                        modifyAmountBienModal.modal('hide');

                        btnResearchRetraitDeclaration.trigger('click');

                        //callModalDisplayDetailTaxationV2(codeTaxationSelected);
                    }
                }
                , 1);
            },
            complete: function () {
            },
            error: function () {
                modifyAmountBienModal.unblock();
                showResponseError();
            }
        });

    });
}
function callModifyAmountBienAutomobileTaxation(codeBien, bienName, periodeCode, devise) {

    var value = '<span style="font-weight:bold">' + bienName + '</span>';

    codeBienModify = codeBien;
    bienNameModify = bienName;
    periodeCodeModify = periodeCode;
    deviseAmountModify = devise;

    alertify.confirm('Etes-vous sûre de vouloir modifier le montant de ce bien : ' + value + ' ?', function () {

        inputNewAmountBien.val('');
        lblDeviseNewAmount.html(devise);

        modalDisplayDetailTaxation.modal('hide');
        modifyAmountBienModal.modal('show');
    });

}