/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var articleBudgetaireModal;

var divArticleBudgetaire, codeArtBudgetaire;

var inputValueResearchArticleBudgetaire;

var btnLauchSearchArticleBudgetaire, btnSelectArticleBudgetaire;

var tableResultSearchArticleBudgetaire;

var selectArticleBudgetaireData;

$(function () {

    articleBudgetaireModal = $('#articleBudgetaireModal');

    divArticleBudgetaire = $('#divArticleBudgetaire');

    tableResultSearchArticleBudgetaire = $('#tableResultSearchArticleBudgetaire');

    inputValueResearchArticleBudgetaire = $('#inputValueResearchArticleBudgetaire');

    btnLauchSearchArticleBudgetaire = $('#btnLauchSearchArticleBudgetaire');
    btnSelectArticleBudgetaire = $('#btnSelectArticleBudgetaire');


    btnLauchSearchArticleBudgetaire.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (inputValueResearchArticleBudgetaire.val() === empty || inputValueResearchArticleBudgetaire.val().length < SEARCH_MIN_TEXT) {
            alertify.alert('Veuillez fournir un critère de recherche');
            return;
        }
        searchArticleBudgetaire();

    });

    inputValueResearchArticleBudgetaire.on('keypress', function (e) {
        if (e.keyCode == 13) {
            searchArticleBudgetaire();
        }

    });

    btnSelectArticleBudgetaire.click(function (e) {

        if (selectArticleBudgetaireData.code === empty) {
            alertify.alert('Veuillez d\'abord sélectionner un article budgétaire');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir sélectionner <b>' + selectArticleBudgetaireData.nomComplet + '</b> ?', function () {
            getSelectedArticleBudgetaireData();
            articleBudgetaireModal.modal('hide');
        });
    });

    printArticleBudgetaireTable('');

});

function searchArticleBudgetaire() {

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchArticleBudgetaire.val(),
            'operation': 'researchArticleBudgetaire'
        },
        beforeSend: function () {
            articleBudgetaireModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                articleBudgetaireModal.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnSelectArticleBudgetaire.hide();
                    printArticleBudgetaireTable('');
                    return;
                } else {
                    var dataArticleBudgetaire = JSON.parse(JSON.stringify(response));
                    if (dataArticleBudgetaire.length > 0) {
                        btnSelectArticleBudgetaire.show();
                        printArticleBudgetaireTable(dataArticleBudgetaire);
                    } else {
                        btnSelectArticleBudgetaire.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            articleBudgetaireModal.unblock();
            showResponseError();
        }

    });
}

function printArticleBudgetaireTable(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">Code</th>';
    tableContent += '<th scope="col">Article budgétaire</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="display: none">' + data[i].code + '</td>';
        tableContent += '<td>' + data[i].libelleArticleBudgetaire + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultSearchArticleBudgetaire.html(tableContent);

    var myDt = tableResultSearchArticleBudgetaire.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableResultSearchArticleBudgetaire tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
//        selectAssujettiData = new Object();
        codeArtBudgetaire = myDt.row(this).data()[1];
    });
}



