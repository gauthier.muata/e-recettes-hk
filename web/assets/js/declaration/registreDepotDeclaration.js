/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tableRegistreDepotDeclaration, tableDetailDepotDeclaration, linkSubMenuRegistreDepotDeclaration;

var btnCallModalSearchAvandedDepotdeclaration, btnResearchDepotDeclaration,
        btnRechercheAvancee;

var inputResearchDepotDeclar;

var typeResearchRegistredepotDeclaration;

var modalDetailDepotDeclaration, detailDepotDeclaration;

var listDepotDeclaration;

var modalRechercheAvanceeModelTwo, modalRechercheAvanceeByArticle, articleBudgetaireModal;

var lblNameAssujetti, lblAdressAssujetti;

var selectSite, selectServicex, inputDateDebut, inputdateLast, btnAdvencedSearch;
var depotDeclarationSelected;
var divArticleBudgetaire;

var btnAdvencedSearchByArticle, btnPrintRecepisse;

var numeroBordereau;

var modalRechercheAvancee;

var loadOnlyDeclarationOnline = 0, isMensuel = false, abCode;
var tempDepotDecCList = [], isAdvancedSearch = false;
var sumCDF = 0, sumUSD = 0;

$(function () {

    mainNavigationLabel.text('DECLARATION');
    secondNavigationLabel.text('Registre des dépôts de déclaration');

    removeActiveMenu();
    linkSubMenuRegistreDepotDeclaration.addClass('active');

    tableRegistreDepotDeclaration = $('#tableRegistreDepotDeclaration');
    tableDetailDepotDeclaration = $('#tableDetailDepotDeclaration');

    linkSubMenuRegistreDepotDeclaration = $('#linkSubMenuRegistreDepotDeclaration');


    btnCallModalSearchAvandedDepotdeclaration = $('#btnCallModalSearchAvandedDepotdeclaration');
    btnResearchDepotDeclaration = $('#btnResearchDepotDeclaration');

    btnShowModalAdvencedRechearch = $('#btnRechercheAvancee');

    btnPrintRecepisse = $('#btnPrintRecepisse');

    inputResearchDepotDeclar = $('#inputResearchDepotDeclar');

    divArticleBudgetaire = $('#divArticleBudgetaire');
    btnAdvencedSearchByArticle = $('#btnAdvencedSearchByArticle');

    lblNameAssujetti = $('#lblNameAssujetti');
    lblAdressAssujetti = $('#lblAdressAssujetti');

    typeResearchRegistredepotDeclaration = $('#typeResearchRegistredepotDeclaration');

    modalDetailDepotDeclaration = $('#modalDetailDepotDeclaration');
    modalRechercheAvanceeNC = $('#modalRechercheAvanceeModelTwo');
    articleBudgetaireModal = $('#articleBudgetaireModal');
    modalRechercheAvanceeByArticle = $('#modalRechercheAvanceeByArticle');

    btnAdvencedSearch = $('.btnAdvencedSearch');
    inputdateLast = $('#inputdateLast');
    inputDateDebut = $('#inputDateDebut');
    //selectService = $('#selectService');
    selectSite = $('#selectSite');
    modalRechercheAvancee = $('.modalRechercheAvancee');

    btnCallModalSearchAvandedDepotdeclaration.on('click', function (e) {
        e.preventDefault();
//        modalRechercheAvanceeByArticle.modal('show');
        modalRechercheAvancee.modal('show');

    });

    btnResearchDepotDeclaration.on('click', function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (inputResearchDepotDeclar.val() === "" || inputResearchDepotDeclar.val().length < SEARCH_MIN_TEXT) {
            alertify.alert('Veuillez fournir un critère de recherche avec au moins ' + SEARCH_MIN_TEXT + ' caractères');
            return;
        }
        loadDepotDeclaration();

    });

    btnAdvencedSearchByArticle.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadDepotDeclarationByArticle();
    });


    typeResearchRegistredepotDeclaration.on('change', function (e) {
        inputResearchDepotDeclar.val('');
        if (typeResearchRegistredepotDeclaration.val() === '1') {
            inputResearchDepotDeclar.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
        } else if (typeResearchRegistredepotDeclaration.val() === '2') {
            inputResearchDepotDeclar.attr('placeholder', 'Veuillez saisir la note de taxation');
        }

    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        isAdvancedSearch = true;
        declarationByAdvancedSearch();
    });

    btnPrintRecepisse.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var value = '<span style="font-weight:bold">' + depotDeclarationSelected + '</span>';

        alertify.confirm('Etes-vous sûre de vouloir ré-imprimer ce récépissé n° ' + value + ' ?', function () {

            $.ajax({
                type: 'POST',
                url: 'declaration_servlet',
                dataType: 'text',
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                crossDomain: true,
                data: {
                    'numero': depotDeclarationSelected,
                    'typeDoc': 'RECEPISSE',
                    'operation': 'printNoteTaxation'
                },
                beforeSend: function () {

                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

                },
                success: function (response)
                {

                    $.unblockUI();

                    if (response == '-1') {
                        $.unblockUI();
                        showResponseError();
                        return;
                    }

                    if (response == '0') {
                        $.unblockUI();
                        alertify.alert('Aucun récepissé trouvée correspondant à ce numéro : ' + value);
                        return;
                    }

                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');
                },
                complete: function () {

                },
                error: function (xhr, status, error) {
                    $.unblockUI();
                    showResponseError();
                }

            });
        });
    });

//    modalRechercheAvanceeByArticle.on('shown.bs.modal', function () {
//        jQuery(".chosen").chosen();
//    });

    loadDepotDeclarationTable('');

    $('.impression').click(function (e) {

        e.preventDefault();
        alertify.confirm('Etes-vous sûre de vouloir imprimer le registre des dépots d&eacute;claration ?', function () {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            if (tempDepotDecCList.length != 0) {
                printDataDepotDeclaration();
            } else {
                alertify.alert('Aucune donnée n\'est disponnible pour imprimer ce rapport.');
            }
        });
    });

});

function loadDepotDeclaration() {

    var viewAllService = controlAccess('VIEW_ALL_SERVICES_NC');
    registerType = getRegisterType();

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputResearchDepotDeclar.val(),
            'typeSearch': typeResearchRegistredepotDeclaration.val(),
            'typeRegister': registerType,
//            'allSite': viewAllSite,
            'allService': viewAllService,
            'codeSite': userData.SiteCode,
            'codeService': userData.serviceCode,
            'userId': userData.idUser,
            'operation': 'searchDepotDeclaration'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'})
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    alertify.alert('Aucune donnée ne corresponde au critère de recherche fournie.');
                    return;
                } else {
                    listDepotDeclaration = null;
                    listDepotDeclaration = JSON.parse(JSON.stringify(response));
                    loadDepotDeclarationTable(listDepotDeclaration);
                }
            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function loadDepotDeclarationTable(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">N° RECEPISSE</th>';
    tableContent += '<th style="text-align:left;width:30%"scope="col">CONTRIBUABLE</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">NOTE TAXATION</th>';
    tableContent += '<th style="text-align:left;width:30%"scope="col">INFOS PAIEMENT</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">DATE DEPÔT</th>';
    tableContent += '<th style="text-align:right;width:12%"scope="col">MONTANT DEPOSE</th>';
    tableContent += '<th style="text-align:center;width:6%"scope="col"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    sumCDF = 0, sumUSD = 0;
    tempDepotDecCList = [];

    for (var i = 0; i < result.length; i++) {

        if (result[i].devise === 'CDF') {
            sumCDF += result[i].montantPercu;
        } else if (result[i].devise === 'USD') {
            sumUSD += result[i].montantPercu;
        }

        var nc = new Object();
        nc.userName = result[i].userName;
        nc.numeroDepotDeclaration = result[i].numeroDepotDeclaration;
        nc.nomComplet = result[i].nomComplet;
        nc.numeroBordereau = result[i].numeroBordereau;
        nc.penalites = '';
        nc.articleBudgetaire = $('.cmbAB option:selected').text();
        nc.dateDepotDeclaration = result[i].dateCreate;
        nc.adressePersonne = result[i].adressePersonne;
        nc.montantDu = result[i].montantPercu + ' ' + result[i].devise;
        nc.banque = 'Banque: ' + result[i].banqueName + '\nCompte bancaire:' + result[i].compteBancaireName;
        tempDepotDecCList.push(nc);


        var id = 'Identifiant : ' + ' <span style="font-weight:bold">' + result[i].userName + '</span>';
        var typePersonne = 'Type : ' + ' <span style="font-weight:bold">' + result[i].libelleFormeJuridique + '</span>';
        var adressePersonne = 'Adresse : ' + ' <span style="font-weight:bold">' + result[i].adressePersonne + '</span>';
        var nomPersonne = 'Noms : ' + ' <span style="font-weight:bold">' + result[i].nom + '</span>';

        var contribuableInfo = id + '<br/><br/>' + typePersonne + '<br/><br/>' + nomPersonne + '<hr/>' + adressePersonne;

        var bordereauInfo = 'N° Bordereau : ' + ' <span style="font-weight:bold">' + result[i].numeroBordereau + '</span>';
        var banqueInfo = 'Banque : ' + ' <span style="font-weight:bold">' + result[i].banqueName2 + '</span>';
        var compteBancaireInfo = 'Compte bancaire : ' + ' <span style="font-weight:bold">' + result[i].compteBancaireName2 + '</span>';

        var paiementInfo = bordereauInfo + '<br/><br/>' + compteBancaireInfo + '<br/><br/>' + banqueInfo;

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;width:10%">' + result[i].codeDepotDeclaration + '</td>';
        tableContent += '<td style="vertical-align:middle;width:30">' + contribuableInfo + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10">' + result[i].noteTaxation + '</td>';
        tableContent += '<td style="vertical-align:middle;width:30">' + paiementInfo + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10">' + result[i].dateCreate + '</td>';
        tableContent += '<td style="vertical-align:middle;width:20;text-align:right">' + formatNumber(result[i].montantPercu, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:center;vertical-align:middle;width:6"><button class="btn btn-primary" onclick="displayDetailDepotDeclaration(\'' + result[i].codeDepotDeclaration + '\')"><i class="fa fa-list"></i></button></td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';
    tableRegistreDepotDeclaration.html(tableContent);
    tableRegistreDepotDeclaration.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 5,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

}

function displayDetailDepotDeclaration(depotDeclaration) {

    depotDeclarationSelected = depotDeclaration;

    for (var i = 0; i < listDepotDeclaration.length; i++) {

        if (depotDeclaration === listDepotDeclaration[i].codeDepotDeclaration) {

            lblNameAssujetti.text(listDepotDeclaration[i].nom + ' (' + listDepotDeclaration[i].libelleFormeJuridique + ')')
            lblAdressAssujetti.html(listDepotDeclaration[i].adressePersonne);
            numeroBordereau = listDepotDeclaration[i].numeroBordereau;

            var detailDepotDecl = JSON.parse(listDepotDeclaration[i].listDetailDepotDeclaration);
            if (detailDepotDecl.length > 0) {
                loaderDetailDepot(detailDepotDecl);
            }
            modalDetailDepotDeclaration.modal('show');
            return;
        }
    }
}

function loaderDetailDepot(detailDepotDecl) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:20%"scope="col">IMPÔT</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">PERIDOE</th>';
    tableContent += '<th style="text-align:left;width:40%"scope="col">BIEN</th>';
    tableContent += '<th style="text-align:right;width:15%"scope="col">MONTANT PAYE</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < detailDepotDecl.length; i++) {

        var firstLineAB = '';

        if (detailDepotDecl[i].libelleArticleBudgetaire.length > 305) {
            firstLineAB = detailDepotDecl[i].libelleArticleBudgetaire.substring(0, 305).toUpperCase() + ' ...';
        } else {
            firstLineAB = detailDepotDecl[i].libelleArticleBudgetaire.toUpperCase();
        }


        if (detailDepotDecl[i].isImmobilier === '1') {

            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + detailDepotDecl[i].libelleTypeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + detailDepotDecl[i].usageName + '</span>';
            var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + detailDepotDecl[i].tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + detailDepotDecl[i].communeName + '</span>';
            var quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + detailDepotDecl[i].quartierName + '</span>';

            var bienInfo = '<span style="font-weight:bold">' + detailDepotDecl[i].intituleBien + '</span>';

            var adresseInfo = detailDepotDecl[i].adresseBien.toUpperCase();

            descriptionBien = bienInfo + '<br/><br/>' + natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<hr/>' + adresseInfo;

        } else {

            var bienInfo2, natureInfo2, categorieInfo2, adresseInfo2 = '';

            if (detailDepotDecl[i].type == 1) {

                bienInfo2 = '<span style="font-weight:bold">' + detailDepotDecl[i].intituleBien + '</span>';
                natureInfo2 = 'Nature : ' + '<span style="font-weight:bold">' + detailDepotDecl[i].libelleTypeBien + '</span>';
                categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + detailDepotDecl[i].tarifName + '</span>';
                adresseInfo2 = detailDepotDecl[i].adresseBien.toUpperCase();

                descriptionBien = bienInfo2 + '<br/><br/>' + natureInfo2 + '<br/>' + categorieInfo2 + '<hr/>' + adresseInfo2;

            } else if (detailDepotDecl[i].type == 3) {

                bienInfo2 = '<span style="font-weight:bold">' + detailDepotDecl[i].intituleBien + '</span>';
                natureInfo2 = 'Nature : ' + '<span style="font-weight:bold">' + detailDepotDecl[i].libelleTypeBien + '</span>';
                categorieInfo2 = 'Destination : ' + '<span style="font-weight:bold">' + detailDepotDecl[i].tarifName + '</span>';
                adresseInfo2 = detailDepotDecl[i].adresseBien.toUpperCase();

                descriptionBien = bienInfo2 + '<br/><br/>' + natureInfo2 + '<br/>' + categorieInfo2 + '<hr/>' + adresseInfo2;
            }

        }

        tableContent += '<tr>';

        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle" title="' + detailDepotDecl[i].libelleArticleBudgetaire + '"><span style="font-weight:bold;vertical-align:middle"></span>' + firstLineAB + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + detailDepotDecl[i].IntitulePeriodicite + '</td>';
        tableContent += '<td style="text-align:left;width:40%;vertical-align:middle">' + descriptionBien + '</td>';
        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + formatNumber(detailDepotDecl[i].montantPercu, detailDepotDecl[i].devise) + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableDetailDepotDeclaration.html(tableContent);
    tableDetailDepotDeclaration.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le detail des articles budgétaires est vide",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });

}

function declarationByAdvancedSearch() {

    registerType = getRegisterType();

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'SiteCode': $('.cmbBureau').val(),
            'codeAb': $('.cmbAB').val(),
            'mois': $('.cmbMois').val(),
            'annee': $('.cmbAnnee').val(),
            'declarationOnlineOnly': loadOnlyDeclarationOnline,
            'isMensuel': isMensuel,
            'codeAgent': userData.idUser,
            'operation': 'SearchAvancedDepotDeclaration'
        },
        beforeSend: function () {
            modalRechercheAvanceeNC.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                modalRechercheAvanceeNC.unblock();

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    modalRechercheAvancee.modal('hide');
                    listDepotDeclaration = '';
                    loadDepotDeclarationTable('');
                    alertify.alert('Aucune donnée ne corresponde au critère de recherche fournie.');
                    return;
                } else {
                    modalRechercheAvancee.modal('hide');
                    listDepotDeclaration = null;
                    listDepotDeclaration = JSON.parse(JSON.stringify(response));
                    loadDepotDeclarationTable(listDepotDeclaration);
                }
            });
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getDepotDeclarationOfArticleBudgetaire(codeArtBudg) {

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'codeArticleBudgetaire': codeArtBudg,
            'operation': 'searchDepotOfArticleBudgetaire'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'})
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    alertify.alert('Aucune donnée ne corresponde au critère de recherche fournie.');
                    return;
                } else {
                    listDepotDeclaration = null;
                    listDepotDeclaration = JSON.parse(JSON.stringify(response));
                    loadDepotDeclarationTable(listDepotDeclaration);
                }
            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function loadDepotDeclarationByArticle() {


    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'searchDepotOfArticleBudgetaire',
            'codeArticleBudgetaire': codeABSelected,
            'annee': selectAnnee_1.val(),
            'mois': selectMois_1.val(),
            'codePeriodiciteAB': codePeriodicite
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des données ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            setTimeout(function () {

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    listDepotDeclaration = '';
                    loadDepotDeclarationTable('');
                    alertify.alert('Aucune donnée ne corresponde au critère de recherche fournie.');
                    return;
                } else {
                    modalRechercheAvanceeByArticle.modal('hide');

                    listDepotDeclaration = null;
                    listDepotDeclaration = JSON.parse(JSON.stringify(response));
                    loadDepotDeclarationTable(listDepotDeclaration);
                }
            });
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printDataDepotDeclaration() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;
    abCode = $('.cmbAB').val();

    var columns = [], columnStyles;

    docTitle = "                        DIRECTION\n       Rapport des dépôts déclaration"
    columns = [
        {title: "NTD", dataKey: "userName"},
        {title: "DATE DEPOT", dataKey: "dateDepotDeclaration"},
        {title: "CONTRIBUABLE", dataKey: "nomComplet"},
        {title: "NUMERO BORDEREAU", dataKey: "numeroBordereau"},
        {title: "MONTANT", dataKey: "montantDu"},
        {title: "PENALITES", dataKey: "penalites"},
        {title: "ADRESSES", dataKey: "adressePersonne"}
    ];

    columnStyles = {
        userName: {columnWidth: 85, fontSize: 8, overflow: 'linebreak'},
        dateDepotDeclaration: {columnWidth: 75, fontSize: 8, overflow: 'linebreak'},
        nomComplet: {columnWidth: 150, overflow: 'linebreak', fontSize: 8},
        numeroBordereau: {columnWidth: 115, overflow: 'linebreak', fontSize: 8},
        montantDu: {columnWidth: 55, overflow: 'linebreak', fontSize: 8},
        penalites: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
        adressePersonne: {columnWidth: 220, overflow: 'linebreak', fontSize: 8}
    };

    if (tempDepotDecCList.length > 0) {

        if (isAdvancedSearch) {
            position = 350;
            hauteur = 170;
        } else {
            var critere = inputResearchDepotDeclar.val().toUpperCase();
            docTitle = docTitle + ' : ' + critere;
            var len = critere.length % 2;
            if (len == 0) {
                position = 350 - (critere.length / 2);
            } else {
                position = 350 - ((critere.length + 1) / 2);
            }

            hauteur = 170;
        }

    } else {
        position = 350;
    }

    var rows = tempDepotDecCList;
    var pageContent = function (data) {

        var bureau = $('.cmbBureau option:selected').text();
        setDocParams(doc, docTitle, position, day, month, year, hh, mm, ss, data, bureau);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: columnStyles,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 10 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');
    doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(sumCDF), 540, finalY + 40);
    doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD), 540, finalY + 55);
    window.open(doc.output('bloburl'), '_blank');
}

function handleClick(cb) {

    switch (cb.checked) {
        case true:
            loadOnlyDeclarationOnline = '1';
            break;
        case false:
            loadOnlyDeclarationOnline = '0';
            break;
    }
}