/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Intialisation des paramètres
var table               =   $('#tableGroupe'),
    form                =   $('.formulaire-edition-groupe'),
    formDroit           =   $('.formulaire-edition-droit'),
    modalAddGroup       =   $('#ajout-group'),
    modalAddDroit       =   $('#ajout-droit'),
    intitule            =   $('#intitule'),
    description         =   $('#description'),
    codeDroit           =   $('#code-droit'),
    intituleDroit       =   $('#intitule-droit'),
    codeInput           =   $('#idgroupe'),
    btnAdd              =   $('.btn-add-group'),
    btnAddDroit         =   $('.btn-add-droit'),
    no_list_droits      =   $('.no-droits-list'),
    no_list_membres     =   $('.no-membres-list'),
    alertMembres        =   $('.no-membres-list .alert'),
    tableMembres        =   $('#tableMembres'),
    alertDroits         =   $('.no-droits-list .alert'),
    tableDroits         =   $('#tableDroits'),
    tableAutresDroits   =   $('#tableAutresDroits'),
    tableGroupes        =   $('#tableGroupe'),
    divDroits           =   $('.droits-list'),
    divMembres          =   $('.membres-list'),
    btnAddMoreDroit     =   $('.btn-add-more-droit'),
    btnRemoveDroit      =   $('.btn-remove-droit'),
    droitsToAdd         =   [],
    dataTableDroits,
    dataTableOtherDroits,
    dataTableMembres,
    dataGroup,
    currentGroupe,
    droits,
    groupes; 

(function($){
    $(document).ready(function(){
        // charger la liste des groupes
        recupererGroups(); 
        
        btnAdd.on('click', function(event){
            event.preventDefault(); 
            modalAddGroup.modal('show'); 
        }); 
        
        form.on('submit', function (event){
            event.preventDefault(); 
            if (intitule.val() == '') {
                alertify.alert("Veuillez remplir le champ"); 
                return; 
            }
            ajouterGroupe(intitule.val(), description.val(), codeInput.val()); 
            
            // reset form
            intitule.val(''); 
            description.val('');
            
            modalAddGroup.modal('hide'); 
        });
        
        formDroit.on('submit', function (event){
            event.preventDefault(); 
            if (codeDroit.val() == '' || intituleDroit.val() == '') {
                alertify.alert("Veuillez remplir tous les champs"); 
                return; 
            }
            ajouterDroit(codeDroit.val(), intituleDroit.val()); 
            
            // reset form
            codeDroit.val(''); 
            intituleDroit.val('');
            
            modalAddGroup.modal('hide'); 
        });
        
        modalAddGroup.on('hidden.bs.modal', function (e) {
            intitule.val(''); 
            description.val('');
        }); 
        
        btnAddDroit.on('click', function (event){
            modalAddDroit.modal('show'); 
            loadDroit(currentGroupe); 
        }); 
        
        // Ajouter des droits au groupe
        btnAddMoreDroit.on('click', function (event){
            if (droitsToAdd.length > 0) {
                alertify.confirm(
                    "Ajout des droits au groupe", 
                    "Etes vous sûre de vouloir ajouter ces droits dans ce groupe", 
                    function(){ 
                        ajouterDroitAuGroupe(currentGroupe, droitsToAdd);  
                    }
                    , function(){ 
                        //alertify.error('Cancel')
                    }
                );
                
            } else {
               alertify.alert("Veuillez selectionner au moins un droit à ajouter");  
            }
        }); 
        
        
        
    }); 
    
})(jQuery)

// Recuperer la liste des groupes
function recupererGroups() {
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'listeGroupe'
        },
        beforeSend: function () {
            modalUpdateUserPassword.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Modification du mot de passe...</h5>'});
        },
        success: function (response)
        {
            var data = JSON.parse(JSON.stringify(response));
            groupes = data; 
            loadDataTableGroupes(data); 
        },
        complete: function () {

        },
        error: function () {
            showResponseError();
        }

    })
}

// Modifier le groupe
function modifierGroupe( id, intitule_groupe, description_groupe ) {
    modalAddGroup.find('.modal-title').html('Modifier un groupe'); 
    intitule.val(intitule_groupe);
    description.val(description_groupe); 
    codeInput.val(id); 
    
    modalAddGroup.modal(); 
}

function loadDataTableGroupes(result){
    var tr  =   '';
    $.each(result, function (index, item, array){ 
        var classRow = (item.etat) ? 'activate': 'desactivate'; 
        var intitule = (item.intitule.length > 20 ) ? item.intitule.substring(0, 20)+" ..." : item.intitule; 
        tr += '<tr class="' +classRow+ '">';
        tr+=  '<td class="groupe_' +item.code_groupe+ '"><div class="radio" onclick="listerlesDroits(\'' +item.code_groupe+ '\')"><label><input type="radio" id="groupe_' +item.code_groupe+ '"></label></div></td>';
        tr += '<td>'+ intitule +'</td>'; 
        if (item.etat) {
            tr+=  '<td class="text-center pr-20"><button class="btn btn-success btn-edit" onclick="modifierGroupe(\''+item.code_groupe+'\',\''+item.intitule+'\',\''+item.description+'\')"><i class="fa fa-edit"></i> </button> <button class="btn btn-danger btn-remove" onclick="desactiverGroupe(\''+item.code_groupe+'\')"><i class="fa fa-close"></i></button></td>';
        } else {
            tr+=  '<td class="text-center pr-20"><button class="btn btn-success btn-edit" onclick="modifierGroupe(\''+item.code_groupe+'\',\''+item.intitule+'\')"><i class="fa fa-edit"></i> </button> <button class="btn btn-primary btn-activate" onclick="activerGroupe(\''+item.code_groupe+'\')"><i class="fa fa-check"></i></button></td>';
        }
        tr += '</tr>';
    }); 
    tableGroupes.find('tbody').html(tr); 
    
    dataGroup = buildDataTable(dataGroup, tableGroupes); 
}

// Ajouter un groupe
function ajouterGroupe(nom, description, id){
    var operation = 'ajoutGroupe'; 
    if ( id ) {
        operation   = 'modifierGroupe'; 
    }
    if ( operation == 'ajoutGroupe' ) {
        var allGroups = groupes.map(function(item){
            return (item.intitule) ? item.intitule.toLowerCase() : ""; 
        }); 
        if (allGroups.indexOf(nom.toLowerCase()) != -1) {
            alertify.alert('Ce groupe existe déjà'); 
            return; 
        }
    }
    
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'intitule': nom, 
            'code_groupe': id, 
            'description': description,
            'agent_creat': userData.idUser,
            'operation': operation
        },
        beforeSend: function () { },
        success: function (response)
        {
            if (response == 1) {
                window.location.reload(); 
            } else {
                alertify.alert("L'opération a échoué"); 
            } 
        },
        complete: function () { },
        error: function () {
            showResponseError(); 
        }

    });
}

// Lister les droits
function listerlesDroits (id_groupe) {
    currentGroupe = id_groupe; 
    $('#tableGroupe').find('tr').removeClass('selected'); 
    var id_checkbox = "#groupe_"+id_groupe; 
    $('#tableGroupe .radio input[type="radio"]').removeAttr('checked'); 
    $(id_checkbox).attr('checked', 'checked'); 
    
    var classtd =   ".groupe_"+id_groupe;
    var row = $(classtd).parent('tr'); 
    if (row.hasClass('selected')) {
        row.removeClass('selected')
    } else {
        row.addClass('selected');
    }
    $.blockUI({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Chargement des droits et des membres</h5>'});
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'code_groupe': id_groupe,
            'operation': 'listeDroits'
        },
        beforeSend: function () {
            modalUpdateUserPassword.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Modification du mot de passe...</h5>'});
        },
        success: function (response)
        {
           var data = JSON.parse(JSON.stringify(response));
           droits   =   ( data ) ? data.droits : []; 
           if (data.droits.length == 0) {
               no_list_droits.removeClass('hidden'); 
               divDroits.addClass('hidden');
               var destroyDroits = $.fn.dataTable.isDataTable(tableDroits) && dataTableDroits.destroy(); 
           } else {
               no_list_droits.addClass('hidden');
               divDroits.removeClass('hidden');
               loadDataTabledroits(data.droits);
           } 
           if (data.membres.length == 0) {
               no_list_membres.removeClass('hidden');
               divMembres.addClass('hidden');
               var destroyMembres = $.fn.dataTable.isDataTable(tableMembres) && dataTableMembres.destroy();
           } else {
               no_list_membres.addClass('hidden');
               divMembres.removeClass('hidden');
               loadDataTableMembres(data.membres); 
           } 
        },
        complete: function () {
           $.unblockUI(); 
        },
        error: function () {
            showResponseError();
        }

    }) 
}

function loadDroit( currentGroupe ) {
    var operation = "listerAutresDroits"; 
    $.blockUI({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Chargement des autres droits</h5>'});
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'code_groupe': currentGroupe, 
            'operation': operation
        },
        beforeSend: function () {
            
        },
        success: function (response)
        {
            console.log(response); 
            loadDataTableOtherDroits(response.droits); 
        },
        complete: function () {
            $.unblockUI(); 
        },
        error: function () {
            showResponseError(); 
        }

    });
}

// Ajouter un droit
function ajouterDroitAuGroupe ( id_groupe, droits ) { 
    
   //console.log(droits);
    $.blockUI({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Insertion en cours...</h5>'});
    var operation   =   'ajouterDroitAuGroupe'; 
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'code_groupe': id_groupe, 
            'droits': droits.toString(),
            'agent_creat': userData.idUser,
            'operation': operation
        },
        beforeSend: function () {},
        success: function (response)
        {
            var data = JSON.parse(JSON.stringify(response));
           if (data.droits.length == 0) {
               no_list_droits.removeClass('hidden'); 
               divDroits.addClass('hidden');
               var destroyDroits = $.fn.dataTable.isDataTable(tableDroits) && dataTableDroits.destroy(); 
           } else {
               no_list_droits.addClass('hidden');
               divDroits.removeClass('hidden');
               loadDataTabledroits(data.droits);
           } 
           if (data.membres.length == 0) {
               no_list_membres.removeClass('hidden');
               divMembres.addClass('hidden');
               var destroyMembres = $.fn.dataTable.isDataTable(tableMembres) && dataTableMembres.destroy();
           } else {
               no_list_membres.addClass('hidden');
               divMembres.removeClass('hidden');
               loadDataTableMembres(data.membres); 
           } 
            modalAddDroit.modal('hide'); 
        },
        complete: function () {
            $.unblockUI(); 
        },
        error: function () {
            showResponseError(); 
        }

    });
}
// Lister tous les droits d'un autre groupe
function loadDataTableOtherDroits( result ) {
    if ( $.fn.dataTable.isDataTable( '#tableAutresDroits' ) && dataTableOtherDroits != null ) {
        dataTableOtherDroits.destroy(); 
    }
    tableAutresDroits.find('tbody').html("");
    var tr  =   '';
    $.each(result, function (index, item, array){ 
        var classRow = (item.etat) ? '': 'desactivate'; 
        var intitule_droit = item.intitule_droit; 
        tr += '<tr>';
        tr+=  '<td class="groupe_' +item.code_droit+ '"><div class="checkbox" onclick="selectionnerDroit(\'' +item.code_droit+ '\')"><label><input type="checkbox" id="droit_' +item.code_droit+ '"></label></div></td>';
        tr += '<td>'+ intitule_droit +' </td>';
        tr += '</tr>';
    }); 
    tableAutresDroits.find('tbody').html(tr); 
    
    dataTableOtherDroits = buildDataTable(dataTableOtherDroits, tableAutresDroits); 
}

// Lister tous les droits d'un groupe
function loadDataTabledroits( result ) {
    if ( $.fn.dataTable.isDataTable( '#tableDroits' ) && dataTableDroits != null ) {
        dataTableDroits.destroy(); 
    }
    tableDroits.find('tbody').html("");
    var tr  =   '';
    $.each(result, function (index, item, array){ 
        var classRow = (item.etat) ? '': 'desactivate'; 
        var intitule_droit = item.intitule_droit; 
        tr += '<tr>';
        tr+=  '<td class="droit_' +item.code_droit+ '"><div class="checkbox checkbox-droit" onclick="selectionnerDroit(\'' +item.code_droit+ '\')"><label><input type="checkbox" id="droit_' +item.code_droit+ '"></label></div></td>';
        tr += '<td>'+ intitule_droit +' </td>';
        tr += '</tr>';
    }); 
    tableDroits.find('tbody').html(tr); 
    
    dataTableDroits = buildDataTable(dataTableDroits, tableDroits); 
}

//Lister tous les membres d'un groupe
function loadDataTableMembres(data) {
    if ( $.fn.dataTable.isDataTable(tableMembres) && dataTableMembres != null ) {
        dataTableMembres.destroy(); 
    }
    tableMembres.find('tbody').html("");
    var tr  =   '';
    $.each(data, function (index, item, array){ 
        var classRow = (item.etat_droit) ? '': 'desactivate'; 
        tr += '<tr>';
        tr+=  '<td class="groupe_' +item.code_groupe+ '"><div class="checkbox" onclick="listerlesDroits(\'' +item.code_groupe+ '\')"><label><input type="checkbox" id="groupe_' +item.code_groupe+ '"></label></div></td>';
        tr += '<td>'+ item.nom_agent +' '+item.prenom_agent+'</td>'; 
        /*
        if (item.etat) {
            tr+=  '<td class="text-center pr-20"><button class="btn btn-success btn-edit" onclick="modifierDroit(\''+item.code_droit+'\',\''+item.intitule_droit+'\')"><i class="fa fa-edit"></i> </button> <button class="btn btn-danger btn-remove" onclick="supprimerDroit(\''+item.code_droit+'\')"><i class="fa fa-close"></i></button></td>';
        } else {
            tr+=  '<td class="text-center pr-20"><button class="btn btn-success btn-edit" onclick="modifierDroit(\''+item.code_droit+'\',\''+item.intitule_droit+'\')"><i class="fa fa-edit"></i> </button> <button class="btn btn-primary btn-activate" onclick="activerDroit(\''+item.code_droit+'\')"><i class="fa fa-check"></i></button></td>';
        } */
        tr += '</tr>';
    }); 
    tableMembres.find('tbody').html(tr); 
    
    dataTableMembres = buildDataTable(dataTableMembres, tableMembres); 
}

// Activer un groupe 
function activerGroupe ( id ) {
    alertify.confirm(
            "Activation du groupe", 
            "Etes vous sûre de vouloir activer ce groupe", 
            function(){ 
                var operation = "activerGroupe"; 
                enableDisableGroupe(id, operation);  
            }
            , function(){ 
                alertify.error('Cancel')}
        );
}

// D&cactiver un groupe
function desactiverGroupe ( id ) {
    alertify.confirm(
            "Désactivation du groupe", 
            "Etes vous sûre de vouloir désactiver ce groupe", 
            function(){ 
                var operation = "desactiverGroupe";
                enableDisableGroupe (id, operation ); 
            }
            , function(){ 
                alertify.error('Cancel')}
        );
}

function selectionnerDroit ( code ) {
    var index = droitsToAdd.indexOf(code); 
    if (index != -1 ) {
        droitsToAdd = droitsToAdd.filter(function (item){ return item!= code }); 
        
    } else {
        droitsToAdd.push(code);
    }
    console.log(droitsToAdd); 
    var classtd =   ".groupe_"+code;
    var row = $(classtd).parent('tr'); 
    if (row.hasClass('selected')) {
        row.removeClass('selected')
    } else {
        row.addClass('selected');
    }
    if (droitsToAdd.length > 0) {
        btnRemoveDroit.removeClass('hidden');
    } else {
        btnRemoveDroit.addClass('hidden');
    }
}

// Cette fonction permet d'activer/desactiver un groupe selon son opération
function enableDisableGroupe (id, operation ) {
    
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'code_groupe': id, 
            'operation': operation
        },
        beforeSend: function () {
            
        },
        success: function (response)
        {
            if (response == 1) {
                window.location.reload(); 
            } else if (response = 2) {
                alertify.alert("Impossible de supprimer ce groupe car il contient des membres"); 
            } else {
                alertify.alert("L'opération a échoué"); 
            }
        },
        complete: function () {

        },
        error: function () {
            showResponseError(); 
        }

    });
}

function ajouterauGroupe (code) {
    console.log(code); 
    var operation = "listerAutresDroits"; 
    $.blockUI({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Chargement des autres droits</h5>'});
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'code_groupe': code, 
            'operation': operation
        },
        beforeSend: function () {
            
        },
        success: function (response)
        {
            console.log(response); 
        },
        complete: function () {
            $.unblockUI(); 
        },
        error: function () {
            showResponseError(); 
        }

    });
}


// Initialise DataTable avec options
function buildDataTable( dataTableToBuild, table ) {
    dataTableToBuild = table.DataTable({
        "order": [[ 1, "asc" ]],
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    }); 
    
    return dataTableToBuild; 
}