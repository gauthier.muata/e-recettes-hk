/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var btnSaveTauxVignette,
        btnDeleteTauxVignette,
        tableTauxVignette,
        selectFormeJuridique,
        selectTarif,
        inputMontantVignette,
        inputMontantTscr,
        inputTauxVignettePercent,
        inputTotal,
        btnEffacer,
        inputTauxTscrPercent;

var tauxVignetteList = [];
var idRowSelected;

$(function () {

    idRowSelected = '';

    mainNavigationLabel.text('PARAMETRAGE');
    secondNavigationLabel.text('Gestion des taux de vignettes');
    removeActiveMenu();

    btnSaveTauxVignette = $('#btnSaveTauxVignette');
    btnDeleteTauxVignette = $('#btnDeleteTauxVignette');
    tableTauxVignette = $('#tableTauxVignette');
    selectFormeJuridique = $('#selectFormeJuridique');
    selectTarif = $('#selectTarif');
    inputMontantVignette = $('#inputMontantVignette');
    inputMontantTscr = $('#inputMontantTscr');
    inputTauxVignettePercent = $('#inputTauxVignettePercent');
    inputTauxTscrPercent = $('#inputTauxTscrPercent');
    inputTotal = $('#inputTotal');
    btnEffacer = $('#btnEffacer');


    btnEffacer.on('click', function (e) {

        e.preventDefault();

        alertify.confirm('Etes-vous sûre de vouloir effacer les champs ce taux de vignette ?', function () {

            selectFormeJuridique.val('0');
            selectTarif.val('0');
            inputMontantVignette.val('');
            inputMontantTscr.val('');
            inputTauxVignettePercent.val('');
            inputTauxTscrPercent.val('');
            inputTotal.val('');
            idRowSelected = '';

            btnDeleteTauxVignette.attr('disabled', true);
        });

    });

    btnDeleteTauxVignette.on('click', function (e) {

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir supprimer ce taux de vignette ?', function () {

            deleteTauxVignette();
        });
    });

    btnSaveTauxVignette.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectFormeJuridique.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un type de personne valide');
            return;

        }

        if (selectTarif.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un tarif valide');
            return;

        }

        if (inputMontantVignette.val() == '') {

            alertify.alert('Veuillez d\'abord saisir le montant de la vignette');
            return;

        }

        if (inputMontantTscr.val() == '') {

            alertify.alert('Veuillez d\'abord saisir le montant de la TSCR');
            return;

        }

        if (inputTauxVignettePercent.val() == '') {

            alertify.alert('Veuillez d\'abord saisir le taux en % de la vignette');
            return;

        }

        if (inputTauxTscrPercent.val() == '') {

            alertify.alert('Veuillez d\'abord saisir le taux en % de la TSCR');
            return;

        }

        alertify.confirm('Etes-vous sûre de vouloir enregistrer ce taux de vignette ?', function () {

            saveTauxVignette();
        });


    });

    if (!controlAccess('SAVE_TAUX_VIGNETTE')) {
        btnSaveTauxVignette.attr('disabled', true);
    } else {
        btnSaveTauxVignette.attr('disabled', false);
    }

    loadDataTarifVignette();

});

function deleteTauxVignette() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'id': idRowSelected,
            'operation': 'deleteTauxVignette'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    alertify.alert('La suppression du taux de vignette s\'est effectuée avec succès.');

                    selectFormeJuridique.val('0');
                    selectTarif.val('0');
                    inputMontantVignette.val('');
                    inputMontantTscr.val('');
                    inputTauxVignettePercent.val('');
                    inputTauxTscrPercent.val('');
                    inputTotal.val('');
                    idRowSelected = '';

                    btnDeleteTauxVignette.attr('disabled', true);

                    loadTauxVignette();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function printTableTauxVignette(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center;width:5%">№</th>';
    tableContent += '<th style="text-align:left;width:20%">TYPE</th>';
    tableContent += '<th style="text-align:left;width:15%">TARIF</th>';
    tableContent += '<th style="text-align:right;width:15%">VIGNETTE</th>';
    tableContent += '<th style="text-align:right;width:15%">TSCR</th>';
    tableContent += '<th style="text-align:right;width:15%">TOTAL (VIGNETTE + TSCR)</th>';
    tableContent += '<th style="text-align:center;width:10%">TAUX VIGNETTE</th>';
    tableContent += '<th style="text-align:center;width:10%">TAUX TSCR</th>';
    tableContent += '<th style="text-align:center;width:10%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var count = 0;
    var totalBien = result.length;

    for (var i = 0; i < result.length; i++) {

        count++;

        var btnEditTauxVignette = '<button class="btn btn-success" onclick="editTauxVignette(\'' + result[i].id + '\')"><i class="fa fa-edit"></i> Modifier</button>';

        var nameType = '';

        switch (result[i].formeJuridique) {
            case '*':
                nameType = 'TOUS';
                break;
            case '03':
                nameType = 'PERSONNE MORALE';
                break;
            case '04':
                nameType = 'PERSONNE PHYSIQUE';
                break;
        }

        var total = (result[i].amountVignette + result[i].amountTscr);

        var amountQuotaVignette = '';
        var amountQuotaTscr = '';

        if (result[i].amountVignette > 0) {
            amountQuotaVignette = formatNumber(((result[i].amountVignette * result[i].tauxPercentVignette) / 100), 'USD');
        } else {
            amountQuotaVignette = '0,00 USD';
        }

        var amountQuotaVignetteTxt = '<hr/>' + '<span style="font-weight:bold;color:green">' + amountQuotaVignette + '</span>';


        if (result[i].amountTscr > 0) {
            amountQuotaTscr = formatNumber(((result[i].amountTscr * result[i].tauxPercentTscr) / 100), 'USD');
        } else {
            amountQuotaTscr = '0,00 USD';
        }

        var amountQuotaTscrTxt = '<hr/>' + '<span style="font-weight:bold;color:green">' + amountQuotaTscr + '</span>';

        tableContent += '<tr id="row_' + result[i].id + '">';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + count + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + nameType + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle;font-weight:bold">' + result[i].nameTarif + '</td>';
        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + formatNumber(result[i].amountVignette, 'USD') + '</td>';
        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + formatNumber(result[i].amountTscr, 'USD') + '</td>';
        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle;font-weight:bold;font-size:16px">' + formatNumber(total, 'USD') + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle;font-weight:bold">' + formatNumberOnly(result[i].tauxPercentVignette) + '%' + amountQuotaVignetteTxt + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle;font-weight:bold">' + formatNumberOnly(result[i].tauxPercentTscr) + '%' + amountQuotaTscrTxt + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + btnEditTauxVignette + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';
    tableTauxVignette.html(tableContent);

    tableTauxVignette.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des biens automobile est vide",
            search: "Filtrer la liste des taux vignettes ici",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"}
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false, ordering: false,
        pageLength: 50,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(2).footer()).html(totalBien);
        },
        columnDefs: [
            {"visible": false, "targets": 1}
        ],
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(1, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });
}

function saveTauxVignette() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveTauxVignette',
            'codeType': selectFormeJuridique.val(),
            'codeTarif': selectTarif.val(),
            'amountVignette': inputMontantVignette.val(),
            'amountTscr': inputMontantTscr.val(),
            'tauxVignette': inputTauxVignettePercent.val(),
            'tauxTscr': inputTauxTscrPercent.val(),
            'id': idRowSelected
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' || response == '0') {

                showResponseError();
                return;

            } else {

                alertify.alert('L\'Enregistrement du taux de vignette s\'est effectué avec succès.');

                selectFormeJuridique.val('0');
                selectTarif.val('0');
                inputMontantVignette.val('');
                inputMontantTscr.val('');
                inputTauxVignettePercent.val('');
                inputTauxTscrPercent.val('');
                inputTotal.val('');
                idRowSelected = '';
                btnDeleteTauxVignette.attr('disabled', true);

                loadTauxVignette();

            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadTauxVignette() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTauxVignette'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des taux en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                showResponseError();
                printTableTauxVignette('');
                return;

            } else if (response == '0') {

                printTableTauxVignette('');
                alertify.alert('Il n\' y a pas de taux de vignettes disponible');
                return;

            } else {

                tauxVignetteList = $.parseJSON(JSON.stringify(response));
                printTableTauxVignette(tauxVignetteList);
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadDataTarifVignette() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadDataTarifVignette'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else {

                    var dataTarif = empty;
                    var defautlValue = '--';

                    dataTarif += '<option value="0">' + defautlValue + '</option>';

                    var tarifList = JSON.parse(JSON.stringify(response));

                    for (var i = 0; i < tarifList.length; i++) {

                        dataTarif += '<option value="' + tarifList[i].code + '" >' + tarifList[i].intitule + '</option>';

                    }

                    selectTarif.html(dataTarif);

                    loadTauxVignette();

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function editTauxVignette(id) {

    alertify.confirm('Etes-vous sûre de vouloir aficher les informations de ce taux vignettes ?', function () {

        for (var j = 0; j < tauxVignetteList.length; j++) {

            if (tauxVignetteList[j].id == id) {

                idRowSelected = id;

                selectFormeJuridique.val(tauxVignetteList[j].formeJuridique);
                selectTarif.val(tauxVignetteList[j].codeTarif);
                inputMontantVignette.val(tauxVignetteList[j].amountVignette);
                inputMontantTscr.val(tauxVignetteList[j].amountTscr);
                inputTauxVignettePercent.val(tauxVignetteList[j].tauxPercentVignette);
                inputTauxTscrPercent.val(tauxVignetteList[j].tauxPercentTscr);
                inputTotal.val(formatNumber((tauxVignetteList[j].amountVignette + tauxVignetteList[j].amountTscr), 'USD'));

                if (controlAccess('DELETE_TAUX_VIGNETTE')) {
                    btnDeleteTauxVignette.attr('disabled', false);
                }

                break;

            }
        }
    });
}




