/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Intialisation des paramètres
var table           =   $('#tableGroupe'),
    form            =   $('.formulaire-edition-groupe'),
    modalAddGroup   =   $('#ajout-group'),
    intitule        =   $('#intitule'),
    description     =   $('#description'),
    codeInput       =   $('#idgroupe'),
    btnAdd          =   $('.btn-add-group'),
    groupes; 

(function($){
    $(document).ready(function(){
        // charger la liste des groupes
        recupererGroups(); 
        
        btnAdd.on('click', function(event){
            event.preventDefault(); 
            modalAddGroup.modal('show'); 
        }); 
        
        form.on('submit', function (event){
            event.preventDefault(); 
            if (intitule.val() == '') {
                alertify.alert("Veuillez remplir le champ"); 
                return; 
            }
            ajouterGroupe(intitule.val(), description.val(), codeInput.val()); 
            
            // reset form
            intitule.val(''); 
            description.val('');
            
            modal.modal('hide'); 
        })
        
    }); 
    
})(jQuery)

// Recuperer la liste des groupes
function recupererGroups() {
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'listeGroupe'
        },
        beforeSend: function () {
            modalUpdateUserPassword.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Modification du mot de passe...</h5>'});
        },
        success: function (response)
        {
            var data = JSON.parse(JSON.stringify(response));
            groupes = data; 
            loadDataTableDroits(data); 

        },
        complete: function () {

        },
        error: function () {
            showResponseError();
        }

    })
}

function loadDataTableDroits(result){
    var tr  =   '';
    $.each(result, function (index, item, array){ 
        var classRow = (item.etat) ? 'activate': 'desactivate'; 
        tr += '<tr class="' +classRow+ '" onclick="listerlesDroits(\'' +item.id+ '\')">';
        tr += '<td>'+ item.intitule +'</td>'; 
        if (item.etat) {
            tr+=  '<td class="text-center pr-20"><button class="btn btn-success btn-edit" onclick="modifierFonction(\''+item.id+'\',\''+item.intitule+'\')"><i class="fa fa-edit"></i> </button> <button class="btn btn-danger btn-remove" onclick="supprimerFonction(\''+item.codeFonction+'\')"><i class="fa fa-close"></i></button></td>';
        } else {
            tr+=  '<td class="text-center pr-20"><button class="btn btn-success btn-edit" onclick="modifierFonction(\''+item.id+'\',\''+item.intitule+'\')"><i class="fa fa-edit"></i> </button> <button class="btn btn-primary btn-activate" onclick="activerFonction(\''+item.codeFonction+'\')"><i class="fa fa-check"></i></button></td>';
        }
        tr += '</tr>';
    }); 
    $('#tableGroupe').find('tbody').html(tr); 
    
    $('#tableGroupe').DataTable({
        "order": [[ 1, "asc" ]],
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    }); 
}

// Lister les droits
function listerlesDroits (id_droit) {
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'listeDroits'
        },
        beforeSend: function () {
            modalUpdateUserPassword.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Modification du mot de passe...</h5>'});
        },
        success: function (response)
        {
            var data = JSON.parse(JSON.stringify(response));
            //fonctions = data; 
            loadDataTableDroits(data); 

        },
        complete: function () {

        },
        error: function () {
            showResponseError();
        }

    })
}

// Ajouter un groupe
function ajouterGroupe(nom, description, id){
    var operation = 'ajoutGroupe'; 
    if (id) {
        operation   = 'modifierGroupe'; 
    }
    if (operation == 'ajoutGroupe') {
        var allGroups = groupes.map(function(item){
            return (item.intitule) ? item.intitule.toLowerCase() : ""; 
        }); 
        if (allGroups.indexOf(nom.toLowerCase()) != -1) {
            alertify.alert('Ce groupe existe déjà'); 
            return; 
        }
    }
    
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'intitule': nom, 
            'code': id, 
            'description': description,
            'operation': operation
        },
        beforeSend: function () {
            modalUpdateUserPassword.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Modification du mot de passe...</h5>'});
        },
        success: function (response)
        {
            if (response == 1) {
                window.location.reload(); 
            } else {
                alert.alertify("L'opération a échoué"); 
            } 
        },
        complete: function () {

        },
        error: function () {
            showResponseError(); 
        }

    });
}