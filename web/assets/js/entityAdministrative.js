/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var selectProvince, selectVille, selectDistrict, selectCommune, selectQuartier, selectAvenue;
var btnAddTypeProvince, btnAddTypeVille, btnAddTypeDistrict, btnAddTypeCommune, btnAddTypeQuartier, btnAddTypeAvenue;
var groupProvince, groupVille, groupDistrict, groupCommune, groupQuartier, groupAvenue;
var current_entite = 1;

var entityData;

var modalAddEntiteAdministrative, modalAddEntity;

var inputEntityMere, inputEntityIntitule, inputTypeEntity;

var btnAddEntity;

var TYPE_ENTITY_PROVINCE = 6;
var TYPE_ENTITY_VILLE = 4;
var TYPE_ENTITY_DISTRICT = 5;
var TYPE_ENTITY_COMMUNE = 3;
var TYPE_ENTITY_QUARTIER = 2;
var TYPE_ENTITY_AVENUE = 1;

var selectMereCode = empty;
var selectType = empty;

var codeMere = empty;

var codeKatanga = '00000000000427472016';
var codeDistrict = '00000000000118192016';
var codeVille, codeCommune, codeQuartier, codeDistrict;

$(function () {

    //Entite
    selectProvince = $('#selectProvince');
    selectVille = $('#selectVille');
    selectDistrict = $('#selectDistrict');
    selectCommune = $('#selectCommune');
    selectQuartier = $('#selectQuartier');
    selectAvenue = $('#selectAvenue');

    //Bouton Ajouter
    btnAddTypeProvince = $('#btnAddTypeProvince');
    btnAddTypeVille = $('#btnAddTypeVille');
    btnAddTypeDistrict = $('#btnAddTypeDistrict');
    btnAddTypeCommune = $('#btnAddTypeCommune');
    btnAddTypeQuartier = $('#btnAddTypeQuartier');
    btnAddTypeAvenue = $('#btnAddTypeAvenue');

    //Entity Groupe
    groupProvince = $('#groupProvince');
    groupVille = $('#groupVille');
    groupDistrict = $('#groupDistrict');
    groupCommune = $('#groupCommune');
    groupQuartier = $('#groupQuartier');
    groupAvenue = $('#groupAvenue');

    // modal
    modalAddEntiteAdministrative = $('#modalAddEntiteAdministrative');
    modalAddEntity = $('#modalAddEntity');

    //Autres
    inputEntityMere = $('#inputEntityMere');
    inputEntityIntitule = $('#inputEntityIntitule');
    inputTypeEntity = $('#inputTypeEntity');
    btnAddEntity = $('#btnAddEntity');
    btnShowAddresseForm = $('#btnAddEntite');

    selectProvince.on('change', function (e) {
        e.preventDefault();

        if (selectProvince.val() == '0' || selectProvince.val() == '') {

            alertify.alert('Veuillez sélectionner une province');

            return;

        } else {
            current_entite = 1;
            loadEntityByMere(codeKatanga, TYPE_ENTITY_VILLE, false);
        }

    });

    selectVille.change(function (e) {
        e.preventDefault();

        if (selectVille.val() == '0' || selectVille.val() == '') {

            alertify.alert('Veuillez sélectionner une ville');

            return;

        } else {
            codeVille = selectVille.val();
            current_entite = 2;
            loadEntityByMere(codeVille, TYPE_ENTITY_DISTRICT, false);
        }

    });

    selectDistrict.change(function (e) {
        e.preventDefault();

        if (selectDistrict.val() == '0' || selectDistrict.val() == '') {

            alertify.alert('Veuillez sélectionner un district');

            return;

        } else {
            codeDistrict = selectDistrict.val();
            current_entite = 3;
            loadEntityByMereV2(codeDistrict, TYPE_ENTITY_COMMUNE, false);
        }

    });

    selectCommune.change(function (e) {
        e.preventDefault();

        if (selectCommune.val() == '0' || selectCommune.val() == '') {

            alertify.alert('Veuillez sélectionner une commune');

            return;

        } else {
            codeCommune = selectCommune.val();
            current_entite = 4;
            loadEntityByMere(codeCommune, TYPE_ENTITY_QUARTIER, false);
        }


    });

    selectQuartier.change(function (e) {
        e.preventDefault();

        if (selectQuartier.val() == '0' || selectQuartier.val() == '') {

            alertify.alert('Veuillez sélectionner un quartier');

            return;

        } else {

            codeQuartier = selectQuartier.val();
            current_entite = 5;
            loadEntityByMere(codeQuartier, TYPE_ENTITY_AVENUE, false);
        }

    });

//    btnAddTypeProvince.click(function (e) {
//        e.preventDefault();
//        refreshEntitysDatas(codeKatanga, TYPE_ENTITY_VILLE, true);
//    });
//    btnAddTypeProvince.click(function (e) {
//        e.preventDefault();
//        showAddEntityForm(TYPE_ENTITY_PROVINCE, empty, empty);
//    });

    btnAddTypeVille.click(function (e) {
        e.preventDefault();
        var selectText = $('#selectProvince option:selected').text();
//        showAddEntityForm(TYPE_ENTITY_VILLE, selectText, selectProvince.val());
        showAddEntitysForms(TYPE_ENTITY_VILLE, selectText, codeKatanga);
    });

    btnAddTypeDistrict.click(function (e) {
        e.preventDefault();
        var selectText = $('#selectVille option:selected').text();
        showAddEntitysForms(TYPE_ENTITY_DISTRICT, selectText, selectVille.val());
    });

    btnAddTypeCommune.click(function (e) {
        e.preventDefault();
        var selectText = $('#selectDistrict option:selected').text();
        showAddEntitysForms(TYPE_ENTITY_COMMUNE, selectText, codeVille);
    });

    btnAddTypeQuartier.click(function (e) {
        e.preventDefault();
        var selectText = $('#selectCommune option:selected').text();
        showAddEntitysForms(TYPE_ENTITY_QUARTIER, selectText, codeCommune);
    });

    btnAddTypeAvenue.click(function (e) {
        e.preventDefault();
        var selectText = $('#selectQuartier option:selected').text();
        showAddEntitysForms(TYPE_ENTITY_AVENUE, selectText, codeQuartier);
    });

    btnShowAddresseForm.click(function (e) {
        e.preventDefault();
//        loadEntityAdministrative(true);
        loadEntityByCode(codeKatanga, true);
    });

    btnAddEntity.click(function (e) {
        e.preventDefault();
//        addEntity();
        addEntiteAdmin();
    });


});

function getEntityData(mere, type, isfirst) {

    if (mere === empty && !isfirst) {
        resetByType(type);
        return;
    }

    var data = JSON.parse(JSON.stringify(entityData));

    var optionData = '<option value="">--- S&eacute;lectionner ---</option>';

    for (var i = 0; i < data.length; i++) {

        switch (type) {
            case 6:
                if (data[i].mere === mere && data[i].code == '00000000000427472016') {
                    optionData += '<option value="' + data[i].code + '">' + data[i].intitule + '</option>';
                }
                break;
            default:
                if (data[i].mere === mere) {
                    optionData += '<option value="' + data[i].code + '">' + data[i].intitule + '</option>';
                }
                break;
        }

    }
    return optionData;
}

function loadEntityAdministrative(isFirst) {

    $.ajax({
        type: 'POST',
        url: 'general_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': '00025'
        },
        beforeSend: function () {
            if (isFirst) {
                $.blockUI({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Chargement des entit&eacute;s administratives...</h5>'});
            } else {
                modalAddEntiteAdministrative.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Chargement des entit&eacute;s administratives...</h5>'});
            }

        },
        success: function (response)
        {
            if (isFirst) {
                $.unblockUI();
            } else {
                modalAddEntiteAdministrative.unblock();
            }


            if (response == '-1') {
                showResponseError();
                return;
            }
            entityData = response;
            if (isFirst) {
                resetEntityField();
                modalAddEntiteAdministrative.modal('show');
                var province = getEntityData(empty, TYPE_ENTITY_PROVINCE, true);
                selectProvince.html(province);
            } else {
                refreshEntityData(selectMereCode, selectType);
            }
        },
        complete: function () {

        },
        error: function () {
            if (isFirst) {
                $.unblockUI();
            } else {
                modalAddEntiteAdministrative.unblock();
            }
            showResponseError();
        }

    });

}

function showAddEntityForm(type, mereText, mereCode) {

    switch (type) {

        case TYPE_ENTITY_PROVINCE:
            inputTypeEntity.val('PROVINCE');
            inputEntityMere.val('République Démocratique du Congo');
            break;
        case TYPE_ENTITY_VILLE:
            if (mereCode == null || mereCode === empty) {
                alertify.alert('Veuillez sélectionner une province');
                return;
            }
            inputTypeEntity.val('VILLE');
            inputEntityMere.val('PROVINCE : ' + mereText.toUpperCase());
            break;
        case TYPE_ENTITY_DISTRICT:
            if (mereCode == null || mereCode === empty) {
                alertify.alert('Veuillez sélectionner une ville');
                return;
            }
            inputTypeEntity.val('DISTRICT');
            inputEntityMere.val('VILLE : ' + mereText.toUpperCase());
            break;
        case TYPE_ENTITY_QUARTIER:
            if (mereCode == null || mereCode === empty) {
                alertify.alert('Veuillez sélectionner un commune');
                return;
            }
            inputTypeEntity.val('QUARTIER');
            inputEntityMere.val('COMMUNE : ' + mereText.toUpperCase());
            break;
        case TYPE_ENTITY_COMMUNE:
            if (mereCode == null || mereCode === empty) {
                alertify.alert('Veuillez sélectionner un district');
                return;
            }
            inputTypeEntity.val('COMMUNE');
            inputEntityMere.val('DISTRICT : ' + mereText.toUpperCase());
            break;
        case TYPE_ENTITY_AVENUE:
            if (mereCode == null || mereCode === empty) {
                alertify.alert('Veuillez sélectionner une quartier');
                return;
            }
            inputTypeEntity.val('AVENUE');
            inputEntityMere.val('QUARTIER : ' + mereText.toUpperCase());
            break;
    }

    selectMereCode = mereCode;
    selectType = type;
    modalAddEntity.modal('show');
}

function addEntity() {

    if (inputEntityIntitule.val() === empty) {
        alertify.alert('Veuillez saisir le libellé de l\'entité');
        return;
    }

    alertify.confirm('Voulez-vous confirmer la cr&eacute;ation de <b>' + inputEntityIntitule.val() + '</b> ?', function ()
    {

        $.ajax({
            type: 'POST',
            url: 'general_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'intitule': inputEntityIntitule.val(),
                'type': selectType,
                'mere': selectMereCode,
                'idUser': userData.idUser,
                'operation': '1101'
            }, beforeSend: function () {
                modalAddEntity.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Création d\'une entité...</h5>'});
            },
            success: function (response)
            {
                modalAddEntity.unblock();

                switch (response) {

                    case '1' :
                        alertify.alert('<b>' + inputEntityIntitule.val() + '</b> a été ajouté avec succès');
                        modalAddEntity.modal('hide');
                        inputEntityIntitule.val(empty);
                        loadEntityAdministrative(false);
                        break;
                    case '0' :
                        alertify.alert('Echec de l\'ajout de l\'entité. Veuillez ressayer.');
                        break;
                    case '-1':
                        showResponseError();
                        break;
                    default :
                        showResponseError();
                }
            },
            complete: function () {

            },
            error: function () {
                modalAddEntity.unblock();
                showResponseError();
            }

        });
    },
            function () {

            });

}

function refreshEntityData(mere, type) {

    var data = getEntityData(mere, type, false);

    resetByType(type);

    switch (type) {

        case TYPE_ENTITY_PROVINCE:
            selectProvince.html(data);
            break;
        case TYPE_ENTITY_VILLE:
            selectVille.html(data);
            break;
        case TYPE_ENTITY_DISTRICT:
            selectDistrict.html(data);
            break;
        case TYPE_ENTITY_QUARTIER:
            selectQuartier.html(data);
            break;
        case TYPE_ENTITY_COMMUNE:
            selectCommune.html(data);
            break;
        case TYPE_ENTITY_AVENUE:
            selectAvenue.html(data);
            break;
    }


    return;
}

function resetEntityField() {
    selectProvince.html(empty);
    selectVille.html(empty);
    selectDistrict.html(empty);
    selectCommune.html(empty);
    selectQuartier.html(empty);
    selectAvenue.html(empty);
}

function resetByType(type) {

    switch (type) {
        case TYPE_ENTITY_PROVINCE:
            selectVille.html(empty);
            selectDistrict.html(empty);
            selectCommune.html(empty);
            selectQuartier.html(empty);
            selectAvenue.html(empty);
            break;
        case TYPE_ENTITY_VILLE:
            selectDistrict.html(empty);
            selectCommune.html(empty);
            selectQuartier.html(empty);
            selectAvenue.html(empty);
            break;
        case TYPE_ENTITY_DISTRICT:
            selectCommune.html(empty);
            selectQuartier.html(empty);
            selectAvenue.html(empty);
            break;
        case TYPE_ENTITY_QUARTIER:
            selectAvenue.html(empty);
            break;
        case TYPE_ENTITY_COMMUNE:
            selectCommune.html(empty);
            selectQuartier.html(empty);
            selectAvenue.html(empty);
            break;
        case TYPE_ENTITY_AVENUE:
            break;
    }
}

function loadEntityByCode(codeEntite, isFirst) {

    $.ajax({
        type: 'POST',
        url: 'general_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'getEntiteByCode',
            'code': codeEntite
        },
        beforeSend: function () {
            if (isFirst) {
                $.blockUI({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Chargement des entit&eacute;s administratives...</h5>'});
            } else {
                modalAddEntiteAdministrative.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Chargement des entit&eacute;s administratives...</h5>'});
            }

        },
        success: function (response)
        {
            if (isFirst) {
                $.unblockUI();
            } else {
                modalAddEntiteAdministrative.unblock();
            }


            if (response == '-1') {
                showResponseError();
                return;
            }
            entityData = response;
            if (isFirst) {
                modalAddEntiteAdministrative.modal('show');

                var data = JSON.parse(JSON.stringify(entityData));

                var provinceData = '<option value ="0">--- S&eacute;lectionner ---</option>';

                for (var i = 0; i < data.length; i++) {
                    provinceData += '<option value =' + data[i].code + '>' + data[i].intitule.toUpperCase() + '</option>';
                }
                selectProvince.html(provinceData);
            }
        },
        complete: function () {

        },
        error: function () {
            if (isFirst) {
                $.unblockUI();
            } else {
                modalAddEntiteAdministrative.unblock();
            }
            showResponseError();
        }

    });

}

function loadEntityByMere(codeMere, typeEntity, isFirst) {

    var spanLoader = $('#loader_' + current_entite);

    $.ajax({
        type: 'POST',
        url: 'general_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'getEntiteByMere',
            'mere': codeMere,
            'from': 0
        },
        beforeSend: function () {

            if (isFirst) {
                $.blockUI({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Chargement des entit&eacute;s administratives...</h5>'});
            } else {
                spanLoader.html('<img style="width:25px;float:right" src="assets/images/loading.gif" />Patientez...');
                //modalAddEntiteAdministrative.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Chargement des entit&eacute;s administratives...</h5>'});
            }

        },
        success: function (response)
        {

            setTimeout(function () {
                if (isFirst) {
                    $.unblockUI();
                } else {
                    spanLoader.html('');
                    //modalAddEntiteAdministrative.unblock();
                }
            }, 1000);





            if (response == '-1') {
                showResponseError();
                return;
            }
            var dataEntite = response;

            var data = JSON.parse(JSON.stringify(dataEntite));

            var optionData = '<option value ="0">-- Sélectionner --</option>';

            for (var i = 0; i < data.length; i++) {
                optionData += '<option value =' + data[i].code + '>' + data[i].intitule.toUpperCase() + '</option>';
            }

            refreshEntitysDatas(optionData, typeEntity);
        },
        complete: function () {

        },
        error: function () {
            if (isFirst) {
                $.unblockUI();
            } else {
                modalAddEntiteAdministrative.unblock();
            }
            showResponseError();
        }

    });

}

function loadEntityByMereV2(codeMere, typeEntity, isFirst) {

    var spanLoader = $('#loader_' + current_entite);

    $.ajax({
        type: 'POST',
        url: 'general_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'getEntiteByMere',
            'mere': codeMere,
            'from': 1
        },
        beforeSend: function () {

            if (isFirst) {
                $.blockUI({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Chargement des entit&eacute;s administratives...</h5>'});
            } else {
                spanLoader.html('<img style="width:25px;float:right" src="assets/images/loading.gif" />Patientez...');

            }

        },
        success: function (response)
        {

            setTimeout(function () {
                if (isFirst) {
                    $.unblockUI();
                } else {
                    spanLoader.html('');
                    //modalAddEntiteAdministrative.unblock();
                }
            }, 1000);





            if (response == '-1') {
                showResponseError();
                return;
            }
            var dataEntite = response;

            var data = JSON.parse(JSON.stringify(dataEntite));

            var optionData = '<option value ="0">-- Sélectionner --</option>';

            for (var i = 0; i < data.length; i++) {
                optionData += '<option value =' + data[i].code + '>' + data[i].intitule.toUpperCase() + '</option>';
            }

            refreshEntitysDatas(optionData, typeEntity);
        },
        complete: function () {

        },
        error: function () {
            if (isFirst) {
                $.unblockUI();
            } else {
                modalAddEntiteAdministrative.unblock();
            }
            showResponseError();
        }

    });

}

function refreshEntitysDatas(data, type) {

    switch (type) {

        case TYPE_ENTITY_VILLE:
            selectCommune.html(empty);
            selectQuartier.html(empty);
            selectAvenue.html(empty);
            selectDistrict.html(empty);
            selectVille.html(data);
            break;
        case TYPE_ENTITY_DISTRICT:
            selectCommune.html(empty);
            selectQuartier.html(empty);
            selectAvenue.html(empty);
            selectDistrict.html(data);
            break;
        case TYPE_ENTITY_QUARTIER:
            selectAvenue.html(empty);
            selectQuartier.html(data);
            break;
        case TYPE_ENTITY_COMMUNE:
            selectQuartier.html(empty);
            selectAvenue.html(empty);
            selectCommune.html(data);
            break;
        case TYPE_ENTITY_AVENUE:
            selectAvenue.html(data);
            break;
    }
    return;
}

function showAddEntitysForms(type, mereText, mereCode) {

    switch (type) {
        case TYPE_ENTITY_VILLE:
            if (mereCode == null || mereCode === empty) {
                alertify.alert('Veuillez sélectionner une province');
                return;
            }
            inputTypeEntity.val('VILLE');
            inputEntityMere.val('PROVINCE : ' + mereText.toUpperCase());
            break;
        case TYPE_ENTITY_QUARTIER:
            if (mereCode == null || mereCode === empty) {
                alertify.alert('Veuillez sélectionner un commune');
                return;
            }
            inputTypeEntity.val('QUARTIER');
            inputEntityMere.val('COMMUNE : ' + mereText.toUpperCase());
            break;
        case TYPE_ENTITY_DISTRICT:
            if (mereCode == null || mereCode === empty) {
                alertify.alert('Veuillez sélectionner une ville');
                return;
            }
            inputTypeEntity.val('DISTRICT');
            inputEntityMere.val('VILLE : ' + mereText.toUpperCase());
            break;
        case TYPE_ENTITY_COMMUNE:
            if (mereCode == null || mereCode === empty) {
                alertify.alert('Veuillez sélectionner un district');
                return;
            }
            inputTypeEntity.val('COMMUNE');
            inputEntityMere.val('VILLE : ' + mereText.toUpperCase());
            
            current_entite = 3;
            
            break;
        case TYPE_ENTITY_AVENUE:
            if (mereCode == null || mereCode === empty) {
                alertify.alert('Veuillez sélectionner une quartier');
                return;
            }
            inputTypeEntity.val('AVENUE');
            inputEntityMere.val('QUARTIER : ' + mereText.toUpperCase());
            break;
    }

    selectMereCode = mereCode;
    selectType = type;
    modalAddEntity.modal('show');
}

function addEntiteAdmin() {

    if (inputEntityIntitule.val() === empty) {
        alertify.alert('Veuillez saisir le libellé de l\'entité');
        return;
    }

    alertify.confirm('Voulez-vous confirmer la cr&eacute;ation de <b>' + inputEntityIntitule.val() + '</b> ?', function ()
    {

        $.ajax({
            type: 'POST',
            url: 'general_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'intitule': inputEntityIntitule.val(),
                'type': selectType,
                'mere': selectMereCode,
                'idUser': userData.idUser,
                'operation': '1101'
            }, beforeSend: function () {
                modalAddEntity.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Création d\'une entité...</h5>'});
            },
            success: function (response)
            {
                modalAddEntity.unblock();

                switch (response) {

                    case '1' :
                        alertify.alert('<b>' + inputEntityIntitule.val() + '</b> a été ajouté avec succès');
                        modalAddEntity.modal('hide');
                        inputEntityIntitule.val(empty);
//                        loadEntityAdministrative(false);

                        switch (current_entite) {
                            case 3:
                                loadEntityByMereV2(selectMereCode, selectType, false)
                                break;
                            default:
                                loadEntityByMere(selectMereCode, selectType, false)
                                break;
                        }

                        break;
                    case '0' :
                        alertify.alert('Echec de l\'ajout de l\'entité. Veuillez ressayer.');
                        break;
                    case '2' :
                        alertify.alert('Cet entité existe dans la système.');
                        inputEntityIntitule.val(empty);
                        break;
                    case '-1':
                        showResponseError();
                        break;
                    default :
                        showResponseError();
                }
            },
            complete: function () {

            },
            error: function () {
                modalAddEntity.unblock();
                showResponseError();
            }

        });
    },
            function () {

            });

}