/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var
        codeResponsible = '';

var
        lblLegalForm,
        lblNameResponsible,
        lblAddress;

var cmbArticleBudgetaire;
var cmbDevise;
var tableBiensAB;

var dataAB = [], dataAssujettissement = [];

var dataRetrait = [];

var defaultDevise = 'CDF';
var totalSumCdf = 0;
var totalSumUsd = 0;
var valuePeriode;
var abSelected;

var btnEnregistrer, inputNumeroDeclaration, inputRequerant;

var cmdPeriodeDeclarationID, valuecmdPeriodeDeclaration;
var amountPenalite;
var dataPeriode;
var nbreMois, estPenalise, periodeName;
var checkBox;
var checkExist;
var assujettissementSelected, deviseSelected;
var penalitieList = [];
var modalFichePriseEnCharge;
var btnCancelPenalite = empty;
var lblTotalGeneral;

var cmbBanque, cmbCompteBancaire;
var modalReviewAmountPenalite, spnAmountPenaliteInit, cmbTauxRemise, spnAmountPenaliteRemise, btnConfirmeRemisePenalite, spnMonthLate, inputObservationRemiseTaux;
var amountRemise;

var retrait = {};
var amountX;
var dateManuelleDeclaration;
var datePickerDateManuelleDeclaration;
var checkApplyDateManuel;
var modalPeriodeDeclaration;
var cmdPeriodeDeclarationIDx;
var btnValidatePeriodes;
var dataJson;
var advancedSearchParam = {};
var idSelected;
var idControlPD;

var choiseExist;
var periodeDeclarationAssujList;

var amountPD_X;
var txtPeriodeName = '';

var rowAssujCurrent;
var amountCalculateX;
var listAmountPenalites = [];
var accountBankList;

var divCategoriePenalite,
        cmdTauxPenalite;

var displayActivate;
var codeFormeJ;
var AbIDCurrent;

$(function () {

    displayActivate = false;
    choiseExist = false;
    amountCalculateX = 0;
    amountPenalite = 0;
    amountRemise = 0;
    valuecmdPeriodeDeclaration = '0';

    checkExist = false;

    mainNavigationLabel.text('DECLARATION');
    secondNavigationLabel.text('Taxation fiscale');

    removeActiveMenu();
    linkMenuDeclaration.addClass('active');

    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');

    cmbArticleBudgetaire = $("#cmbArticleBudgetaire");
    tableBiensAB = $('#tableBiensAB');
    cmbDevise = $('#cmbDevise');
    modalFichePriseEnCharge = $('#modalFichePriseEnCharge');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');

    btnEnregistrer = $('#btnEnregistrer');
    inputNumeroDeclaration = $('#inputNumeroDeclaration');
    inputRequerant = $('#inputRequerant');
    lblTotalGeneral = $('#lblTotalGeneral');

    cmbBanque = $('#cmbBanque');
    cmbCompteBancaire = $('#cmbCompteBancaire');
    modalPeriodeDeclaration = $('#modalPeriodeDeclaration');
    cmdPeriodeDeclarationIDx = $('#cmdPeriodeDeclarationIDx');
    btnValidatePeriodes = $('#btnValidatePeriodes');

    modalPeriodeDeclaration.on('shown.bs.modal', function () {
        jQuery(".chosen").chosen();
    });

    modalReviewAmountPenalite = $('#modalReviewAmountPenalite');
    spnAmountPenaliteInit = $('#spnAmountPenaliteInit');
    cmbTauxRemise = $('#cmbTauxRemise');
    spnAmountPenaliteRemise = $('#spnAmountPenaliteRemise');
    btnConfirmeRemisePenalite = $('#btnConfirmeRemisePenalite');
    spnMonthLate = $('#spnMonthLate');
    inputObservationRemiseTaux = $('#inputObservationRemiseTaux');

    dateManuelleDeclaration = $('#dateManuelleDeclaration');
    datePickerDateManuelleDeclaration = $('#datePicker1');
    checkApplyDateManuel = $('#checkApplyDateManuel');

    divCategoriePenalite = $('#divCategoriePenalite');
    cmdTauxPenalite = $('#cmdTauxPenalite');

    /*cmdPeriodeDeclarationID.on('shown.bs.modal', function () {
     jQuery(".chosen").chosen();
     });*/


    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerDateManuelleDeclaration.datepicker("setDate", new Date());

    btnCallModalSearchResponsable.click(function (e) {
        e.preventDefault();
        assujettiModal.modal('show');
    });

    btnValidatePeriodes.click(function (e) {
        e.preventDefault();

        if (checkExist == false) {

            alertify.alert('Veuillez d\'abord sélectionner un bien avant de poursuivre');
            return

        } else {

            if (cmbArticleBudgetaire.val() == '00000000000002272020') {

                if (displayActivate == true && cmdTauxPenalite.val() == '0') {
                    alertify.alert('Veuillez d\'abord sélectionner un taux valide de pénalité');
                    return;
                }
            }

            getCurrentSearchParam();
        }
    });

    printBienList('');

    cmbArticleBudgetaire.change(function (e) {

        var AbID = cmbArticleBudgetaire.val();
        AbIDCurrent = AbID;

        //printBienList(AbID);

        var lblTotalGeneral = $("#lblTotalGeneral");

        if (defaultDevise == '') {
            defaultDevise = cmbDevise.val();
        }

        lblTotalGeneral.html(formatNumber(0, defaultDevise));

    });

    cmbTauxRemise.change(function (e) {

        if (cmbTauxRemise.val() == '0') {
            spnAmountPenaliteRemise.html(formatNumber(0, deviseSelected));
            alertify.alert('Veuillez d\'abord sélectionner un taux de remsie valide');
            return;
        } else {
            reviewRatePenalite(cmbTauxRemise.val());
        }
    });

    cmdPeriodeDeclarationIDx.change(function (e) {

        e.preventDefault();
        var tauxX;

        if ($('#cmdPeriodeDeclarationIDx option:selected').text() !== '') {

            if (cmbArticleBudgetaire.val() == '00000000000002272020') {

                if (cmdPeriodeDeclarationIDx.val() !== '') {

                    var valueChoice = $('#cmdPeriodeDeclarationIDx option:selected').text();

                    if (valueChoice.includes('2017')) {

                        var choicePerioe = '[{"code":"' + cmdPeriodeDeclarationIDx.val() + '"}]';
                        var choicePerioeList = JSON.parse(choicePerioe);
                        var result = choicePerioeList[0].code.split(',');

                        if (result.length > 1) {

                            btnValidatePeriodes.attr('style', 'display:none');
                            alertify.alert('Vous ne pouvez pas taxer 2017 ensemble avec les autres années car ses taux sont différents');
                            return;

                        }

                        btnValidatePeriodes.attr('style', 'display:block');

                        var anneeSelected = 2017;

                        var listTauxIfAnterieur = JSON.parse(userData.listTauxIfAnterieur);

                        for (var i = 0; i < listTauxIfAnterieur.length; i++) {

                            if (listTauxIfAnterieur[i].annee == anneeSelected && listTauxIfAnterieur[i].codeTarif == codeTarifX
                                    && listTauxIfAnterieur[i].codeTypeBien == codeTypeBienX
                                    && listTauxIfAnterieur[i].codeFormeJuridique == codeFormeJ) {

                                tauxX = listTauxIfAnterieur[i].taux;
                                amount = parseFloat(listTauxIfAnterieur[i].taux * numberX);
                                amountPD_X = amount;

                                defaultDevise = listTauxIfAnterieur[i].unite;

                                var idColMontantDu = $("#montant_" + idAssujettissementX);
                                idColMontantDu.html(formatNumber(amountPD_X, listTauxIfAnterieur[i].unite));
                                idColMontantDu.attr('style', 'font-weight:bold;color:green;font-size:18px;text-align:right;width:11%;vertical-align:middle');


                                break;
                            }
                        }

                        for (var j = 0; j < dataAssujettissement.length; j++) {

                            if (dataAssujettissement[j].idAssujettissement == idAssujettissementX) {

                                var row = dataAssujettissement[j];

                                var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + row.libelleTypeBien + '</span>';
                                var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + row.usageName + '</span>';
                                var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + row.tarifName + '</span>';
                                var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + row.communeName + '</span>';
                                var quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + row.quartierName + '</span>';

                                var complementInfo = row.complement;

                                var tauxInfo = '';
                                var valeurBaseInfo = '';

                                if (row.typeTaux == '%') {

                                    tauxInfo = 'TAUX : ' + '<span style="font-weight:bold;color:green;font-size:18px">' + tauxX + '%' + '</span>';

                                    if (row.valeurBase > 0) {

                                        valeurBaseInfo = '<br/>' + 'BASE DE CALCUL : ' + '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(tauxX, defaultDevise) + '</span>';

                                    }

                                } else {
                                    tauxInfo = 'TAUX : ' + '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(tauxX, defaultDevise) + '</span>';
                                }


                                descriptionBien = natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<hr/>' + complementInfo + '<br/>' + tauxInfo + valeurBaseInfo;

                                tauxCumulInfo = '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(amountPD_X, defaultDevise) + '</span>';

                                var idColTarif = $("#tarif_" + idAssujettissementX);
                                idColTarif.html(descriptionBien);
                            }
                        }

                        divCategoriePenalite.attr('style', 'display:block');
                        displayActivate = true;

                    } else {

                        btnValidatePeriodes.attr('style', 'display:block');

                        for (var i = 0; i < periodeDeclarationAssujList.length; i++) {

                            if (periodeDeclarationAssujList[i].periodeID == cmdPeriodeDeclarationIDx.val()) {

                                if (periodeDeclarationAssujList[i].estPenalise == '1') {

                                    if (displayActivate == false) {
                                        divCategoriePenalite.attr('style', 'display:block');
                                        displayActivate = true;
                                    }

                                }
                            }
                        }
                    }


                }
            }

        } else {
            modalPeriodeDeclaration.modal('hide');
            printBienList(AbIDCurrent);
        }


    });

    cmbBanque.change(function (e) {

        e.preventDefault();

        /*if (cmbArticleBudgetaire.val() !== '0' && cmbBanque.val() !== '0') {
         //loadingAccountBankData(cmbBanque.val());
         loadingAccountBankDataV2(cmbArticleBudgetaire.val(), cmbBanque.val());
         } else {
         
         cmbCompteBancaire.val('0');
         cmbCompteBancaire.attr('disabled', true);
         alertify.alert('Veuillez d\'abord sélectionner une banque valide');
         return;
         }*/

    });

    cmbCompteBancaire.change(function (e) {
        e.preventDefault();

        if (cmbCompteBancaire.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un compte bancaire valide');
            return;
        }

    });

    btnConfirmeRemisePenalite.click(function (e) {

        e.preventDefault();

        if (cmbTauxRemise.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un taux de remsie valide');
            return;
        }

        if (inputObservationRemiseTaux.val() == empty) {
            alertify.alert('Veuillez d\'abord motiver la raison de cette remise');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir appliquer cette remise de pénalité ?', function () {
            applyReviewPenalite();
        });
    });

    btnEnregistrer.click(function (e) {

        e.preventDefault();

        if (cmbArticleBudgetaire.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner l\'impot de la déclaration');
            return;
        }

        if (cmbDevise.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner la devise de la déclaration');
            return;
        }

        if (cmbBanque.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner une banque avant de poursuivre');
            return;
        }

        if (cmbCompteBancaire.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un compte bancaire de la déclaration');
            return;
        }

        if (dataRetrait.length == 0) {

            alertify.alert('Veuillez d\'abord sélectionner un bien pour cette déclaration');
            return;
        } else {

            if (dataRetrait.length >= 1) {

                for (var j = 0; j < dataRetrait.length; j++) {

                    if (dataRetrait[j].periode == '') {
                        alertify.alert('Veuillez d\'abord indiquer au moins une période de déclaration pour les biens sélectionnés');
                        return;
                    }
                }
            }
        }

        for (var i = 0; i < accountBankList.length; i++) {

            if (accountBankList[i].accountCode == cmbCompteBancaire.val() && accountBankList[i].accountDevise !== cmbDevise.val()) {
                alertify.alert('La devise de la déclaration est différente de la devise du compte bancaire sélectionner');
                return;
            }
        }

        alertify.confirm('Etes-vous sûre de vouloir confirmer cette déclaration ?', function () {

            for (var k = 0; k < dataRetrait.length; k++) {
                dataRetrait[k].devise = defaultDevise;
            }

            saveDeclaration();

        });


    });


    cmbDevise.on('change', function (e) {

        e.preventDefault();

        var lblTotalGeneral = $("#lblTotalGeneral");

        defaultDevise = cmbDevise.val();

        if (cmbArticleBudgetaire.val() !== '0' && cmbBanque.val() !== '0') {

            loadingAccountBankDataV2(cmbArticleBudgetaire.val(), cmbBanque.val(), cmbDevise.val());

            printBienList(AbIDCurrent, cmbDevise.val());

        } else {

            cmbCompteBancaire.val('0');
            cmbCompteBancaire.attr('disabled', true);
            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;
        }

        //printBienList(AbIDCurrent, cmbDevise.val());

        lblTotalGeneral.html(formatNumber(totalSum, defaultDevise));
    });

});

function getSelectedAssujetiData() {


    codeResponsible = selectAssujettiData.code;
    lblNameResponsible.html(selectAssujettiData.nomComplet);
    lblLegalForm.html(selectAssujettiData.categorie);
    lblAddress.html(selectAssujettiData.adresse);

    codeFormeJ = selectAssujettiData.codeForme;

    loadABAssujetti();
    loadingBankData();

    printBienList('');
    var lblTotalGeneral = $("#lblTotalGeneral");

    if (defaultDevise == '') {
        defaultDevise = cmbDevise.val();
    }

    lblTotalGeneral.html(formatNumber(0, defaultDevise));
}

function saveDeclaration() {

    if (listAmountPenalites.length == 0) {
        estPenalise = '0';
    } else {
        estPenalise = '1';
    }

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveRetraitDeclaration',
            'codePersonne': codeResponsible,
            'listRetrait': JSON.stringify(dataRetrait),
            'listPenalite': JSON.stringify(listAmountPenalites),
            'idUser': userData.idUser,
            'codeAB': cmbArticleBudgetaire.val(),
            'codeBanque': cmbBanque.val(),
            'codeCompteBancaire': cmbCompteBancaire.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            if (response == '2') {

                alertify.alert('Le numéro de la déclaration est déjà utilisé');
                return;
            }

            alertify.alert('Le retrait de la déclaration a été enregistré avec succès');

            var documentList = JSON.parse(JSON.stringify(response));

            setDocumentContent(documentList.noteTaxationPrincipal);
            window.open('visualisation-document', '_blank');

            if (cmbArticleBudgetaire.val() !== '00000000000002302020') {

                if (estPenalise == '1') {
                    setDocumentContent(documentList.noteTaxationPenalite);
                    window.open('visualisation-document', '_blank');
                }
            }


            initUI();

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function initUI() {

    dataAB = [];
    dataAssujettissement = [];
    dataRetrait = [];
    inputRequerant.val('');
    inputNumeroDeclaration.val('');
    printBienList('');
    checkExist = false;
    valuePeriode = empty;

    nbreMois = 0;
    periodeName = empty;

    //cmbBanque.va('0');
    //cmbCompteBancaire.va('0');

    penalitieList = [];
    estPenalise == '0';

    var idColPenalite = $("#penalite_" + assujettissementSelected);
    var idColCalcelPenalite = $("#btnCalcelPenalite_" + assujettissementSelected);

    amountPenalite = 0;

    idColPenalite.html(formatNumber(amountPenalite, deviseSelected));
    idColPenalite.attr('style', 'font-weight:bold;color:red;font-size:18px;text-align:right;width:11%;vertical-align:middle');

    idColCalcelPenalite.attr('style', 'text-align:center;width:11%;vertical-align:middle');

    totalSum = amountPenalite;

    var lblTotalGeneral = $("#lblTotalGeneral");
    lblTotalGeneral.html(formatNumber(totalSum, deviseSelected));

    setTimeout(function () {
        loadABAssujetti();
    }, 1500);
}

function loadABAssujetti() {

    var vignetteOnly = '0';

    if (controlAccess('TAXATION_VIGNETTE_ONLY')) {
        vignetteOnly = '1';
    }

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getABAssujetti',
            'codePersonne': codeResponsible,
            'taxationVignetteOnly': vignetteOnly
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des données en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            constituateABData(response);
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function callModalePeriodeDeclaration(id) {

    if (checkExist == false) {

        alertify.alert('Veuillez d\'abord sélectionner un bien avant de poursuivre');
        return

    } else {

        divCategoriePenalite.attr('style', 'display:none');
        cmdTauxPenalite.val('0');
        displayActivate = false;
        //listAmountPenalites = [];


        if (cmbArticleBudgetaire.val() == '00000000000002312021') {

            cmdPeriodeDeclarationIDx.html(empty);

            periodeDeclarationAssujList = rowAssujCurrent.listPeriodesDeclarations
            listPeriodesDeclarations = rowAssujCurrent.listPeriodesDeclarations

            var declarationData;
            var defautlValue = '--';

            declarationData += '<option value="0">' + defautlValue + '</option>';

            for (var i = 0; i < periodeDeclarationAssujList.length; i++) {
                declarationData += '<option value="' + periodeDeclarationAssujList[i].periodeID + '">' + periodeDeclarationAssujList[i].periode + '</option>';
            }

            cmdPeriodeDeclarationIDx.html(declarationData);

            $('#cmdPeriodeDeclarationIDx option').prop('selected', false).trigger('chosen:updated');

            modalPeriodeDeclaration.modal('show');

        } else {

            loadPeriodeDeclarationByAssyj(id);
        }



    }
}

function getCurrentSearchParam() {

    txtPeriodeName = '';

    var nbre = 0;
    var moisRetard = 0;
    var amountPD = 0;

    var isPenalite = false;

    if (listAmountPenalites.length == 0) {
        listAmountPenalites = [];
    } else {
        for (var i = 0; i < listAmountPenalites.length; i++) {

            if (listAmountPenalites[i].assujCode == idSelected) {
                listAmountPenalites.splice(i, 1);
            }
        }
    }

    switch (cmbArticleBudgetaire.val()) {
        //case '00000000000002282020': // RL
        //break;

        case '00000000000002272020':
        case '00000000000002112015':
        case '00000000000002292020':
        case '00000000000002282020': // RL
        case '00000000000002352021': // RL
        case '00000000000002362021': // RL

        case '00000000000002302020': // VIGNETTE

            amountPD = amountPD_X;

            for (var j = 0; j < periodeDeclarationAssujList.length; j++) {

                var choicePerioe = '[{"code":"' + cmdPeriodeDeclarationIDx.val() + '"}]';
                var choicePerioeList = JSON.parse(choicePerioe);

                var result = choicePerioeList[0].code.split(',');

                for (var k = 0; k < result.length; k++) {

                    if (periodeDeclarationAssujList[j].periodeID == result[k]) {

                        if (periodeDeclarationAssujList[j].estPenalise == '1') {
                            isPenalite = true;
                            nbre++;

                            if (txtPeriodeName == '') {
                                txtPeriodeName = periodeDeclarationAssujList[j].periode + ' ';
                            } else {
                                txtPeriodeName = txtPeriodeName += periodeDeclarationAssujList[j].periode + ' ';
                            }

                            moisRetard = moisRetard += periodeDeclarationAssujList[j].moisRetard;

                        } else {
                            isPenalite = false;
                            nbre++;

                            if (txtPeriodeName == '') {
                                txtPeriodeName = periodeDeclarationAssujList[j].periode + ' ';
                            } else {
                                txtPeriodeName = txtPeriodeName += periodeDeclarationAssujList[j].periode + ' ';
                            }

                            moisRetard = moisRetard += periodeDeclarationAssujList[j].moisRetard;
                        }
                        //break;
                    }

                }

            }
            break;

        case '00000000000002312021': // ICM

            for (var j = 0; j < periodeDeclarationAssujList.length; j++) {

                var choicePerioe = '[{"code":"' + cmdPeriodeDeclarationIDx.val() + '"}]';
                var choicePerioeList = JSON.parse(choicePerioe);

                var result = choicePerioeList[0].code.split(',');

                for (var k = 0; k < result.length; k++) {

                    if (periodeDeclarationAssujList[j].periodeID == result[k]) {

                        if (periodeDeclarationAssujList[j].estPenalise == '1') {
                            isPenalite = true;
                            nbre++;

                            if (txtPeriodeName == '') {
                                txtPeriodeName = periodeDeclarationAssujList[j].periode + ' ';
                            } else {
                                txtPeriodeName = txtPeriodeName += periodeDeclarationAssujList[j].periode + ' ';
                            }

                            moisRetard = moisRetard += periodeDeclarationAssujList[j].moisRetard;

                        } else {
                            isPenalite = false;
                            nbre++;

                            if (txtPeriodeName == '') {
                                txtPeriodeName = periodeDeclarationAssujList[j].periode + ' ';
                            } else {
                                txtPeriodeName = txtPeriodeName += periodeDeclarationAssujList[j].periode + ' ';
                            }

                            moisRetard = moisRetard += periodeDeclarationAssujList[j].moisRetard;
                        }
                        //break;
                    }
                }


            }
            break;

        default :

            for (var j = 0; j < periodeDeclarationAssujList.length; j++) {

                var choicePerioe = '[{"code":"' + cmdPeriodeDeclarationIDx.val() + '"}]';
                var choicePerioeList = JSON.parse(choicePerioe);

                var result = choicePerioeList[0].code.split(',');

                for (var k = 0; k < result.length; k++) {

                    if (periodeDeclarationAssujList[j].periodeID == result[k]) {

                        if (periodeDeclarationAssujList[j].estPenalise == '1') {
                            isPenalite = true;
                            nbre++;

                            if (txtPeriodeName == '') {
                                txtPeriodeName = periodeDeclarationAssujList[j].periode + ' ';
                            } else {
                                txtPeriodeName = txtPeriodeName += periodeDeclarationAssujList[j].periode + ' ';
                            }

                            moisRetard = moisRetard += periodeDeclarationAssujList[j].moisRetard;

                        } else {
                            isPenalite = false;
                            nbre++;

                            if (txtPeriodeName == '') {
                                txtPeriodeName = periodeDeclarationAssujList[j].periode + ' ';
                            } else {
                                txtPeriodeName = txtPeriodeName += periodeDeclarationAssujList[j].periode + ' ';
                            }

                            moisRetard = moisRetard += periodeDeclarationAssujList[j].moisRetard;
                        }
                        //break;
                    }
                }


            }

            break;
    }

    advancedSearchParam.amountPenalite = 0;

    var objAmountPen = new Object();
    var resteMois = 0;
    var penaliteDuFirstMonth = 0;
    var penaliteDuOtherMonth = 0;

    if (txtPeriodeName !== '' && isPenalite == true) {

        var valueX = '<span style="font-weight:bold">' + txtPeriodeName + '</span>';
        var amountPen;

        if (nbre == 1) {

            alertify.confirm('Cette période de déclaration : ' + valueX + ' sera pénalisée. <br/> Etes-vous sûr de vouloir appliquer la pénalité ?', function () {

                switch (cmbArticleBudgetaire.val()) {

                    case '00000000000002312021':

                        var choicePerioe = '[{"code":"' + cmdPeriodeDeclarationIDx.val() + '"}]';
                        var choicePerioeList = JSON.parse(choicePerioe);

                        var result = choicePerioeList[0].code.split(',');

                        for (var i = 0; i < listPeriodesDeclarations.length; i++) {

                            for (var k = 0; k < result.length; k++) {

                                if (listPeriodesDeclarations[i].periodeID == result[k]) {

                                    amountPD_X = amountPD_X += listPeriodesDeclarations[i].tauxCumule;
                                    amountPD = amountPD_X;
                                }
                            }

                        }

                        //resteMois = parseInt(moisRetard - 1);
                        resteMois = parseInt(moisRetard);
                        penaliteDuFirstMonth = (((amountPD * 25) / 100) * 1);
                        penaliteDuOtherMonth = (((amountPD * 2) / 100) * resteMois);
                        amountPen = (penaliteDuFirstMonth + penaliteDuOtherMonth);

                        //amountPen = ((((amountPD * nbre) * 25) / 100) * moisRetard);
                        break;

                    case '00000000000002272020': // IF

                        amountPen = 0;
                        penaliteDuFirstMonth = 0;
                        penaliteDuOtherMonth = 0;

                        if (cmdTauxPenalite.val() == '27') {

                            penaliteDuFirstMonth = (((amountPD * 25) / 100) * 1);
                            penaliteDuOtherMonth = (((amountPD * 2) / 100) * moisRetard);

                            amountPen = (penaliteDuFirstMonth + penaliteDuOtherMonth);

                        } else if (cmdTauxPenalite.val() == '100') {
                            amountPen = amountPD;
                        } else {
                            amountPen = ((amountPD * parseInt(cmdTauxPenalite.val())) / 100);
                        }
                        break;

                    default:

                        //amountPen = ((((amountPD * nbre) * 25) / 100) * moisRetard);

                        /*resteMois = parseInt(moisRetard - 1);
                         penaliteDuFirstMonth = ((((amountPD * nbre) * 25) / 100) * 1);
                         penaliteDuOtherMonth = ((((amountPD * nbre) * 2) / 100) * resteMois);
                         amountPen = (penaliteDuFirstMonth + penaliteDuOtherMonth);*/

                        //resteMois = parseInt(moisRetard - 1);
                        resteMois = parseInt(moisRetard);
                        penaliteDuFirstMonth = (((amountPD * 25) / 100) * 1);
                        penaliteDuOtherMonth = (((amountPD * 2) / 100) * resteMois);
                        amountPen = (penaliteDuFirstMonth + penaliteDuOtherMonth);

                        break;
                }

                //advancedSearchParam.amountPenalite = advancedSearchParam.amountPenalite += amountPen;

                objAmountPen.assujCode = idSelected;
                objAmountPen.amountPenalite = 0;
                objAmountPen.amountPenalite = objAmountPen.amountPenalite += amountPen;

                objAmountPen.moisRetard = 0;
                objAmountPen.moisRetard = moisRetard;

                objAmountPen.tauxRemise = 0;
                objAmountPen.observationRemise = '';

                listAmountPenalites.push(objAmountPen);

                xPoursuiteTraitement(txtPeriodeName);

            }, function () {
                //advancedSearchParam.amountPenalite = 0;

                for (var i = 0; i < listAmountPenalites.length; i++) {

                    if (listAmountPenalites[i].assujCode == idSelected) {
                        listAmountPenalites.splice(i, 1);
                    }
                }

                xPoursuiteTraitement(txtPeriodeName);
            });

        } else if (nbre > 1) {

            alertify.confirm('Ces périodes de déclaration : ' + valueX + ' seront pénalisées. <br/>Etes-vous sûr de vouloir appliquer la pénalité ?', function () {

                switch (cmbArticleBudgetaire.val()) {

                    case '00000000000002312021':

                        var choicePerioe = '[{"code":"' + cmdPeriodeDeclarationIDx.val() + '"}]';
                        var choicePerioeList = JSON.parse(choicePerioe);

                        var result = choicePerioeList[0].code.split(',');

                        for (var i = 0; i < listPeriodesDeclarations.length; i++) {

                            for (var k = 0; k < result.length; k++) {

                                if (listPeriodesDeclarations[i].periodeID == result[k]) {

                                    amountPD_X = amountPD_X += listPeriodesDeclarations[i].tauxCumule;
                                    amountPD = amountPD_X;
                                }
                            }

                        }

                        //amountPen = ((((amountPD * nbre) * 25) / 100) * moisRetard);

                        //resteMois = parseInt(moisRetard - 1);
                        resteMois = parseInt(moisRetard);
                        penaliteDuFirstMonth = (((amountPD * 25) / 100) * 1);
                        penaliteDuOtherMonth = (((amountPD * 2) / 100) * resteMois);
                        amountPen = (penaliteDuFirstMonth + penaliteDuOtherMonth);
                        break;

                    case '00000000000002272020': // IF

                        /*amountPen = 0;
                         
                         if (cmdTauxPenalite.val() == '27') {
                         
                         penaliteDuFirstMonth = (((amountPD * 25) / 100) * 1);
                         penaliteDuOtherMonth = (((amountPD * 2) / 100) * moisRetard);
                         
                         amountPen = ((penaliteDuFirstMonth + penaliteDuOtherMonth) * parseInt(nbre));
                         
                         } else {
                         amountPen = (((amountPD * parseInt(cmdTauxPenalite.val())) / 100) * parseInt(nbre));
                         }*/

                        var choicePerioe = '[{"code":"' + cmdPeriodeDeclarationIDx.val() + '"}]';
                        var choicePerioeList = JSON.parse(choicePerioe);

                        var result = choicePerioeList[0].code.split(',');

                        for (var i = 0; i < periodeDeclarationAssujList.length; i++) {

                            var penaliteCurrentYear1 = 0;
                            var penaliteCurrentYear2 = 0;
                            var penaliteCurrentYear3 = 0;

                            for (var k = 0; k < result.length; k++) {

                                if (periodeDeclarationAssujList[i].periodeID == result[k]) {

                                    if (periodeDeclarationAssujList[i].isCurrentYear == '1') {

                                        penaliteCurrentYear1 = 0;
                                        penaliteCurrentYear2 = 0;
                                        penaliteCurrentYear3 = 0;

                                        moisRetard = periodeDeclarationAssujList[i].moisRetard;

                                        penaliteCurrentYear1 = (((amountPD * 25) / 100) * 1);

                                        resteMois = parseInt(periodeDeclarationAssujList[i].moisRetard);

                                        if (resteMois > 0) {
                                            penaliteCurrentYear2 = (((amountPD * 2) / 100) * resteMois);
                                        }

                                        penaliteCurrentYear3 = parseFloat(penaliteCurrentYear1 + penaliteCurrentYear2);


                                    } else {
                                        penaliteDuFirstMonth = penaliteDuFirstMonth += ((amountPD * parseInt(cmdTauxPenalite.val())) / 100);
                                    }

                                }
                            }

                            penaliteDuFirstMonth = penaliteDuFirstMonth += penaliteCurrentYear3;


                        }

                        amountPen = penaliteDuFirstMonth;


                        break;

                    default:

                        //resteMois = parseInt(moisRetard - 1);
                        resteMois = parseInt(moisRetard);
                        penaliteDuFirstMonth = (((amountPD * 25) / 100) * 1);
                        penaliteDuOtherMonth = (((amountPD * 2) / 100) * resteMois);
                        amountPen = (penaliteDuFirstMonth + penaliteDuOtherMonth);

                        break;
                }

                objAmountPen.assujCode = idSelected;
                objAmountPen.amountPenalite = 0;
                objAmountPen.amountPenalite = objAmountPen.amountPenalite += amountPen;

                objAmountPen.moisRetard = 0;
                objAmountPen.moisRetard = moisRetard;

                objAmountPen.tauxRemise = 0;
                objAmountPen.observationRemise = '';

                listAmountPenalites.push(objAmountPen);

                xPoursuiteTraitement(txtPeriodeName);

            }, function () {

                for (var i = 0; i < listAmountPenalites.length; i++) {

                    if (listAmountPenalites[i].assujCode == idSelected) {
                        listAmountPenalites.splice(i, 1);
                    }
                }
                xPoursuiteTraitement(txtPeriodeName);
            });
        }
    } else {

        if (listAmountPenalites.length == 0) {
            listAmountPenalites = [];
        } else {
            for (var i = 0; i < listAmountPenalites.length; i++) {

                if (listAmountPenalites[i].assujCode == idSelected) {
                    listAmountPenalites.splice(i, 1);
                }
            }
        }

        xPoursuiteTraitement(txtPeriodeName);
    }
}


function xPoursuiteTraitement(choicePeriodes) {

    advancedSearchParam.periodeID = cmdPeriodeDeclarationIDx.val();
    advancedSearchParam.periodeName = choicePeriodes.toUpperCase();

    if (txtPeriodeName !== '') {

        for (var i = 0; i < dataAssujettissement.length; i++) {

            if (dataAssujettissement[i].idAssujettissement == idSelected) {


                for (var j = 0; j < dataRetrait.length; j++) {

                    if (dataRetrait[j].assuj == idSelected) {

                        if (advancedSearchParam !== null) {

                            var list = JSON.parse(JSON.stringify(advancedSearchParam.periodeID));
                            var nbre = list.length;

                            /*if (cmbArticleBudgetaire.val() == '00000000000002312021') {
                             dataRetrait[j].montant = amountPD_X;
                             }*/

                            dataRetrait[j].montant = amountPD_X;

                            var amountInit = dataRetrait[j].montant;
                            var amountCalculate = (amountInit * nbre);

                            amount = amountCalculate;
                            amountCalculateX = amountCalculate;

                            totalSum += amount;

                            dataRetrait[j].periode = JSON.stringify(advancedSearchParam.periodeID);
                            dataRetrait[j].periodeName = JSON.stringify(advancedSearchParam.periodeName);
                            dataRetrait[j].montant = 0;
                            dataRetrait[j].montant = parseFloat(amountInit * nbre);

                            dataAssujettissement[i].tauxCumule = amountCalculate;
                            choiseExist = true;
                            getPeriodeDeclaration(dataAssujettissement[i].idAssujettissement);
                        }

                    }

                }

                idControlPD = $("#selectPeriodes_" + idSelected);
                idControlPD.val(advancedSearchParam.periodeName);
                idControlPD.attr('style', 'font-weight:bold;font-size:16px');

                modalPeriodeDeclaration.modal('hide');
            }
        }
    } else {

        alertify.confirm('Etes-vous sûre de vouloir valider la sélection de ces périodes de déclarations ?', function () {

            for (var i = 0; i < dataAssujettissement.length; i++) {

                if (dataAssujettissement[i].idAssujettissement == idSelected) {


                    for (var j = 0; j < dataRetrait.length; j++) {

                        if (dataRetrait[j].assuj == idSelected) {

                            if (advancedSearchParam !== null) {

                                var list = JSON.parse(JSON.stringify(advancedSearchParam.periodeID));
                                var nbre = list.length;

                                var amountInit = dataRetrait[j].montant;
                                var amountCalculate = (amountInit * nbre);

                                amount = amountCalculate;
                                amountCalculateX = amountCalculate;

                                /*if (dataRetrait[j].devsie = 'USD') {
                                 totalSumUsd += amount;
                                 } else {
                                 totalSumCdf += amount;
                                 }*/

                                totalSum += amount;

                                dataRetrait[j].periode = JSON.stringify(advancedSearchParam.periodeID);
                                dataRetrait[j].periodeName = JSON.stringify(advancedSearchParam.periodeName);
                                dataRetrait[j].montant = 0;
                                dataRetrait[j].montant = parseFloat(amountInit * nbre);

                                dataAssujettissement[i].tauxCumule = amountCalculate;
                                choiseExist = true;
                                getPeriodeDeclaration(dataAssujettissement[i].idAssujettissement);
                            }

                        }

                    }

                    idControlPD = $("#selectPeriodes_" + idSelected);
                    idControlPD.val(advancedSearchParam.periodeName);
                    idControlPD.attr('style', 'font-weight:bold;font-size:16px');

                    modalPeriodeDeclaration.modal('hide');
                }
            }


        });
    }

}

function generatePeriode(id, data) {

    dataJson = data;
    idSelected = id;

    var btnCallModal = '<hr/><a  id="btnRow_' + id + '" onclick="callModalePeriodeDeclaration(\'' + id + '\')" class="btn btn-warning" title="Sélectionner pétiode de déclaration"><i class="fa fa-search"></i>&nbsp;Période de déclaration</a>';

    var periodeData = '<textarea rows="2" id="selectPeriodes_' + id + '" type="text" class="form-control" style="width:100%" readonly="true" onchange="getPeriodeDeclaration(\'' + id + '\')"></textarea>' + btnCallModal;
    return periodeData;

}

function constituateABData(data) {

    var cbData = '<option value="0">--</option>';

    dataAB = JSON.parse(data.dataAB);
    dataAssujettissement = JSON.parse(data.dataAssujettisement);

    for (var i = 0; i < dataAB.length; i++) {

        var ab = dataAB[i];
        //cbData += '<option value="' + ab.codeAB + '">' + ab.intituleAB.toUpperCase() + '</option>';
        cbData += '<option value="' + ab.codeAB + '">' + ab.sigleAB.toUpperCase() + '</option>';

    }

    cmbArticleBudgetaire.html(cbData);
}

function printBienList(idAb, idDevise) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center;width:6%"></th>';
    tableContent += '<th style="text-align:left;width:20%">BIEN</th>';
    tableContent += '<th style="text-align:left;width:20%">DESCRIPTION</th>';
    tableContent += '<th style="text-align:left;width:12%">PERIODE DECLARATION</th>';
    tableContent += '<th style="text-align:right;width:12%">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:right;width:12%">PENALITE DÛ</th>';
    tableContent += '<th style="text-align:right;width:10%"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    totalSum = 0;

    //alert(JSON.stringify(dataAssujettissement));

    for (var i = 0; i < dataAssujettissement.length; i++) {

        var row = dataAssujettissement[i];

        row.devise = row.uniteAssuj;

        if (row.codeAB != idAb || row.devise != idDevise)
            continue;


        rowAssujCurrent = row;

        var descriptionBien = empty;
        var tauxCumulInfo = '';

        if (row.isImmobilier === '1') {

            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + row.libelleTypeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + row.usageName + '</span>';
            var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + row.tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + row.communeName + '</span>';
            var quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + row.quartierName + '</span>';

            var complementInfo = row.complement;

            var tauxInfo = '';
            var valeurBaseInfo = '';

            if (row.typeTaux == '%') {

                tauxInfo = 'TAUX : ' + '<span style="font-weight:bold;color:green;font-size:18px">' + row.taux + '%' + '</span>';

                if (row.valeurBase > 0) {

                    valeurBaseInfo = '<br/>' + 'BASE DE CALCUL : ' + '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(row.valeurBase, row.uniteAssuj) + '</span>';

                }

            } else {
                tauxInfo = 'TAUX : ' + '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(row.taux, row.devise) + '</span>';
            }


            descriptionBien = natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<hr/>' + complementInfo + '<br/>' + tauxInfo + valeurBaseInfo;

            tauxCumulInfo = '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(row.tauxCumule, row.uniteAssuj) + '</span>';
            defaultDevise = row.devise;

        } else {

            tauxCumulInfo = '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(row.tauxCumule, row.uniteAssuj) + '</span>';
            defaultDevise = row.devise;

            if (row.type == 1) {

                var genreInfo = 'Genre : ' + '<span style="font-weight:bold">' + row.libelleTypeBien + '</span>';
                var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + row.tarifName + '</span>';

                var marqueInfo = 'Marque : ' + '<span style="font-weight:bold">' + row.marque + '</span>';
                var modeleInfo = 'Modèle : ' + '<span style="font-weight:bold">' + row.modele + '</span>';
                var plaqueInfo = '№ Plaque : ' + '<span style="font-weight:bold">' + row.plaque + '</span>';
                var puissanceFiscalInfo = 'Puissance Fisc. : ' + '<span style="font-weight:bold">' + row.puissanceFiscal + ' ' + row.nameUnitePuissance + '</span>';

                tauxInfo = 'TAUX : ' + '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(row.taux, row.devise) + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<br/><br/>' + marqueInfo + '<br/>' + modeleInfo + '<br/>' + plaqueInfo + '<br/>' + puissanceFiscalInfo + '<hr/>' + tauxInfo;

            } else if (row.type == 3) {

                var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + row.libelleTypeBien + '</span>';
                var categorieInfo2 = 'Destination : ' + '<span style="font-weight:bold">' + row.tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + row.complement;

            } else {

                descriptionBien = bienList[i].descriptionBien + '<hr/>' + row.complement;
            }

        }

        dataPeriode = JSON.parse(JSON.stringify(row.listPeriodesDeclarations));

        var periode = dataPeriode.length === 0 ? 'Aucune période disponible' : '<center>' + generatePeriode(row.idAssujettissement, dataPeriode) + '</center>';

        var readOnly = dataPeriode.length === 0 ? 'hidden' : 'checkbox';

        var bienInfo = '<span style="font-weight:bold">' + row.intituleBien + '</span>' + '<hr/>' + row.adresseBien;

        var amountPenaliteFormat = '<span id="spnPenalite_' + row.idAssujettissement + '" style="font-weight:bold;color:red;font-size:18px">' + formatNumber(amountPenalite, row.uniteAssuj) + '</span>';

        btnCancelPenalite = empty;

        if (controlAccess('REVIEW_AMOUNT_PENALITE')) {
            btnCancelPenalite = '<button type="button" class="btn btn-danger " title="Cliquez ici pour revoir le taux de la pénalité" onclick="callModalDisplayTauxPenalite(\'' + row.idAssujettissement + '\')"><i class="fa fa-check-circle"></i>&nbsp;&nbsp;Revoir la pénalité</button>';
        }

        tableContent += '<tr id="row_' + row.idAssujettissement + '">';

        tableContent += '<td style="text-align:center;width:6%;vertical-align:middle"><input  type="' + readOnly + '" id="checkbox_' + row.idAssujettissement + '" onclick="Declarer(\'' + row.idAssujettissement + '\',\'' + row.devise + '\',\'' + amountPenalite + '\')" /></th>';
        tableContent += '<td style="text-align:left;width:18%;vertical-align:middle">' + bienInfo + '</td>';
        tableContent += '<td style="text-align:left;width:18%;vertical-align:middle" id="tarif_' + row.idAssujettissement + '">' + descriptionBien + '</td>';
        tableContent += '<td style="text-align:left;width:13%;vertical-align:middle" id="periode_' + row.idAssujettissement + '">' + periode + '</td>';
        tableContent += '<td style="text-align:right;width:11%;vertical-align:middle" id="montant_' + row.idAssujettissement + '">' + tauxCumulInfo + '</td>';
        tableContent += '<td style="text-align:right;width:11%;vertical-align:middle" id="penalite_' + row.idAssujettissement + '">' + amountPenaliteFormat + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle" id="btnCalcelPenalite_' + row.idAssujettissement + '">' + btnCancelPenalite + '</td>';
        tableContent += '</tr>';
    }

    //tableContent += '<tfoot>';

    //tableContent += '<tr><th colspan="4" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red" id="lblTotalGeneral"></th></tr>';

    //tableContent += '</tfoot>';

    tableContent += '</tbody>';
    tableBiensAB.html(tableContent);

    var myDt = tableBiensAB.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée à afficher",
            search: "Rechercher ici  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3

                /*footerCallback: function (row, data, start, end, display) {
                 
                 var api = this.api(), data;
                 
                 $(api.column(5).footer()).html(
                 formatNumber(totalSum, defaultDevise));
                 }*/
    });
}

var codeTarifX, codeTypeBienX, idAssujettissementX, numberX;

function Declarer(idAssujettisement, devise, amountPenalite) {

    var checkBox = document.getElementById("checkbox_" + idAssujettisement);

    var row = $("#row_" + idAssujettisement);
    var lblTotalGeneral = $("#lblTotalGeneral");

    defaultDevise = devise;

    var amount = 0;
    var assuj;
    amountPD_X = 0;

    for (var i = 0; i < dataAssujettissement.length; i++) {

        assuj = dataAssujettissement[i];

        if (assuj.idAssujettissement == idAssujettisement) {

            //alert(JSON.stringify(dataAssujettissement[i]));

            codeTarifX = assuj.tarifCode;
            codeTypeBienX = assuj.codeTypeBien;
            idAssujettissementX = idAssujettisement;

            numberX = assuj.valeurBase;

            amount = assuj.tauxCumule;
            abSelected = assuj.codeAB;

            amountPD_X = amount;

            break;
        }

    }

    idSelected = idAssujettisement;
    cmdPeriodeDeclarationIDx.val('');

    advancedSearchParam = {};

    if (checkBox.checked) {

        row.attr('style', 'background-color:#daf2c9');
        totalSum += amount;
        checkExist = true;

        retrait = {};

        retrait.assuj = idAssujettisement;
        retrait.montant = assuj.tauxCumule;
        retrait.devise = defaultDevise;
        retrait.periode = '';// $("#selectPeriode_" + idAssujettisement).val();
        retrait.base = 0;
        retrait.icmParamId = 0;
        retrait.penalite = 0;
        retrait.estPenalise = 0;
        retrait.tauxRemise = 0;
        retrait.observationRemise = '';
        retrait.remise = 0;

        dataRetrait.push(retrait);

    } else {

        if (dataRetrait.length > 0) {
            checkExist = true;
        } else {
            checkExist = false;
            choiseExist = false;
        }

        totalSum -= amount;
        amountPenalite -= amountPenalite;

        row.removeAttr('style');

        for (var k = 0; k < dataRetrait.length; k++) {

            if (dataRetrait[k].assuj == idAssujettisement) {

                dataRetrait.splice(k, 1);
                break;
            }
        }

    }

    //lblTotalGeneral.html(formatNumber((totalSum + parseFloat(amountPenalite)), defaultDevise));
}

function getPeriodeDeclaration(id) {

    for (var j = 0; j < dataAssujettissement.length; j++) {

        if (dataAssujettissement[j].idAssujettissement == id) {

            var amount = dataAssujettissement[j].tauxCumule;
            var devise = dataAssujettissement[j].devise;
            deviseSelected = devise;

            assujettissementSelected = id;

            if (choiseExist == true) {

                if (checkExist) {

                    listPeriodeSelected = JSON.parse(JSON.stringify(advancedSearchParam.periodeID));

                    for (var l = 0; l < listPeriodeSelected.length; l++) {

                        for (var i = 0; i < dataRetrait.length; i++) {

                            if (dataRetrait[i].assuj == assujettissementSelected) {

                                //dataRetrait[i].periode = valuecmdPeriodeDeclaration;
                                dataRetrait[i].estPenalise = '0';
                                dataRetrait[i].penalite = 0;
                                dataRetrait[i].remise = 0;
                                dataRetrait[i].observationRemise = empty;
                                dataRetrait[i].tauxRemise = 0;

                                if (abSelected == '00000000000002312021') {

                                    var assujList_ = dataAssujettissement[j].listPeriodesDeclarations;

                                    for (var j = 0; j < assujList_.length; j++) {

                                        var idColMontantDu = $("#montant_" + assujettissementSelected);
                                        idColMontantDu.html(formatNumber(amountCalculateX, assujList_[j].devise));
                                        idColMontantDu.attr('style', 'font-weight:bold;color:green;font-size:18px;text-align:right;width:11%;vertical-align:middle');

                                        deviseSelected = assujList_[j].devise;

                                        retrait.icmParamId = assujList_[j].icmParamId;

                                        var idColPenalite = $("#penalite_" + assujettissementSelected);

                                        var sumTotalPenalite = 0;

                                        for (var k = 0; k < listAmountPenalites.length; k++) {
                                            if (listAmountPenalites[k].assujCode == assujettissementSelected) {
                                                sumTotalPenalite = sumTotalPenalite += listAmountPenalites[k].amountPenalite;
                                            }
                                        }

                                        idColPenalite.html(formatNumber(sumTotalPenalite, deviseSelected));
                                        idColPenalite.attr('style', 'font-weight:bold;color:red;font-size:18px;text-align:right;width:11%;vertical-align:middle');


                                        //loadPeriodeDeclarationByAssyjV2(assujettissementSelected);

                                        break;

                                    }

                                } else {

                                    var idColMontantDu = $("#montant_" + assujettissementSelected);
                                    idColMontantDu.html(formatNumber(dataRetrait[i].montant, dataRetrait[i].devise));
                                    idColMontantDu.attr('style', 'font-weight:bold;color:green;font-size:18px;text-align:right;width:11%;vertical-align:middle');

                                    var idColPenalite = $("#penalite_" + assujettissementSelected);
                                    //var idColCalcelPenalite = $("#btnCalcelPenalite_" + assujettissementSelected);

                                    var sumTotalPenalite = 0;

                                    for (var k = 0; k < listAmountPenalites.length; k++) {
                                        if (listAmountPenalites[k].assujCode == assujettissementSelected) {
                                            sumTotalPenalite = sumTotalPenalite += listAmountPenalites[k].amountPenalite;
                                        }
                                    }

                                    idColPenalite.html(formatNumber(sumTotalPenalite, deviseSelected));
                                    idColPenalite.attr('style', 'font-weight:bold;color:red;font-size:18px;text-align:right;width:11%;vertical-align:middle');

                                    loadPeriodeDeclarationByAssyjV2(assujettissementSelected);

                                    break;

                                }

                            }
                        }

                        //}
                    }



                } else {

                    alertify.alert('Veuillez d\'abord sélectionner un bien avant de poursuivre');
                    valuecmdPeriodeDeclaration = '0'
                    cmdPeriodeDeclarationID.val('0');
                }


            } else {

                var idColMontantDu = $("#montant_" + assujettissementSelected);
                idColMontantDu.html(formatNumber(0, defaultDevise));
                idColMontantDu.attr('style', 'font-weight:bold;color:green;font-size:18px;text-align:right;width:11%;vertical-align:middle');

                //var lblTotalGeneral = $("#lblTotalGeneral");
                //lblTotalGeneral.html(formatNumber(0, defaultDevise));

                var idColPenalite = $("#penalite_" + assujettissementSelected);
                idColPenalite.html(formatNumber(0, defaultDevise));
                idColMontantDu.attr('style', 'font-weight:bold;color:red;font-size:18px;text-align:right;width:11%;vertical-align:middle');

                alertify.alert('Veuillez d\'abord sélectionner une période de déclaration valide avant de poursuivre');

            }

            break;
        }
    }
}

function checkPenalite(periodeSelected) {

    for (var i = 0; i < dataPeriode.length; i++) {

        if (dataPeriode[i].periodeID == periodeSelected) {
            nbreMois = dataPeriode[i].moisRetard;
            estPenalise = dataPeriode[i].estPenalise;
            periodeName = dataPeriode[i].periode;
            return estPenalise;
        }
    }
}

function callModalDisplayTauxPenalite(id) {


    if (checkExist) {

        if (choiseExist) {

            if (listAmountPenalites.length > 0) {

                var sumTotalPenalite = 0;

                for (var k = 0; k < listAmountPenalites.length; k++) {
                    if (listAmountPenalites[k].assujCode == assujettissementSelected) {
                        sumTotalPenalite = sumTotalPenalite += listAmountPenalites[k].amountPenalite;
                        nbreMois = listAmountPenalites[k].moisRetard;
                    }
                }

                amountPenalite = sumTotalPenalite;

                for (var j = 0; j < dataAssujettissement.length; j++) {

                    if (dataAssujettissement[j].idAssujettissement == id) {
                        assujettissementSelected = id;

                        //cmdPeriodeDeclarationID = "#selectPeriode_" + id;
                        //valuecmdPeriodeDeclaration = $('#tableBiensAB').find(cmdPeriodeDeclarationID).val();

                        spnAmountPenaliteInit.html(formatNumber(amountPenalite, deviseSelected));
                        cmbTauxRemise.val('0');
                        spnMonthLate.html(nbreMois);
                        inputObservationRemiseTaux.val(empty);
                        inputObservationRemiseTaux.attr('placeholder', 'Veuillez motiver la raison de la remise ici');
                        spnAmountPenaliteRemise.html(formatNumber(0, deviseSelected));

                        modalReviewAmountPenalite.modal('show');

                    }
                }
            } else {
                //btnCancelPenalite = empty;
                alertify.alert('Impossible de revoir la pénalité, car cette période de déclaration : ' + valuePeriode + ' n\'est pas pénalisée');
            }

        } else {
            alertify.alert('Veuillez d\'abord sélectionner une période de déclaration valide avant de poursuivre');
        }

    } else {

        alertify.alert('Veuillez d\'abord sélectionner un bien avant de poursuivre');
        valuecmdPeriodeDeclaration = '0'
        cmdPeriodeDeclarationID.val('0');

    }

}

function callModalApplyPenalite(montant, periodeID, periodeName, devise, isRecidiviste, monthLate) {

    periodeIDSelected = periodeID
    deviseSelected = devise;

    var obj = {};

    obj.amount = parseFloat(montant);
    obj.reference = periodeID;
    obj.libelleReference = periodeName;
    obj.currency = devise;
    obj.isRecidiviste = isRecidiviste == 1 ? true : false;
    obj.monthLate = parseInt(monthLate);
    obj.case = 1;

    initUIPenality(obj);
}

function validateFPC() {

    alertify.confirm('Etes-vous sûre de vouloir valider prendre en compte ces pénalités ?', function () {

        penalitieList = getPenalitiesReference(periodeIDSelected);

        var idColPenalite = $("#penalite_" + assujettissementSelected);
        var idColCalcelPenalite = $("#btnCalcelPenalite_" + assujettissementSelected);

        amountPenalite = getSumByReference(valuecmdPeriodeDeclaration);

        idColPenalite.html(formatNumber(amountPenalite, deviseSelected));
        idColPenalite.attr('style', 'font-weight:bold;color:red;font-size:18px;text-align:right;width:11%;vertical-align:middle');

        idColCalcelPenalite.attr('style', 'text-align:center;width:11%;vertical-align:middle');

        if (abSelected == '00000000000002312021') {

            if (totalSum == 0) {
                totalSum = amountX;
            }
        }

        totalSum += amountPenalite;

        var lblTotalGeneral = $("#lblTotalGeneral");
        lblTotalGeneral.html(formatNumber(totalSum, deviseSelected));

        for (var i = 0; i < dataRetrait.length; i++) {

            if (dataRetrait[i].assuj == assujettissementSelected) {

                dataRetrait[i].periode = valuecmdPeriodeDeclaration;
                dataRetrait[i].estPenalise = '1';
                dataRetrait[i].penalite = amountPenalite;
                dataRetrait[i].remise = 0;
                dataRetrait[i].observationRemise = empty;
                dataRetrait[i].tauxRemise = 0;
            }
        }

        modalFichePriseEnCharge.modal('hide');
    });


    return penalitieList;
}

function loadingBankData() {


    var dataBank = empty;
    var defautlValue = '--';
    dataBank += '<option value="0">' + defautlValue + '</option>';

    var banqueUserList = JSON.parse(userData.banqueUserList);

    for (var i = 0; i < banqueUserList.length; i++) {

        dataBank += '<option value="' + banqueUserList[i].banqueCode + '" >' + banqueUserList[i].banqueName + '</option>';

    }

    cmbBanque.html(dataBank);
}

function loadingAccountBankDataV2(abCode, bankCode, deviseCode) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadingAccountBankDataV2',
            'codeAb': abCode,
            'codeDevise': deviseCode,
            'codeBanque': bankCode
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();

                if (response == '-1') {

                    showResponseError();
                    return;

                } else {

                    var dataAccountBank = empty;
                    //dataAccountBank = [];
                    var defautlValue = '--';

                    dataAccountBank += '<option value="0">' + defautlValue + '</option>';

                    accountBankList = JSON.parse(JSON.stringify(response));

                    for (var i = 0; i < accountBankList.length; i++) {

                        dataAccountBank += '<option value="' + accountBankList[i].accountCode + '" >' + accountBankList[i].accountName + '</option>';

                    }

                    cmbCompteBancaire.html(dataAccountBank);
                    cmbCompteBancaire.attr('disabled', false);

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadingAccountBankData(bankCode) {

    var dataAccountBank = empty;
    var defautlValue = '--';

    dataAccountBank += '<option value="0">' + defautlValue + '</option>';

    accountBankList = JSON.parse(userData.accountList);

    for (var i = 0; i < accountBankList.length; i++) {
        if (bankCode == accountBankList[i].acountBanqueCode && (accountBankList[i].typeCompteCode == 2 | accountBankList[i].typeCompteCode == 3)) {
            dataAccountBank += '<option value="' + accountBankList[i].accountCode + '" >' + accountBankList[i].accountName + '</option>';
        }
    }

    cmbCompteBancaire.html(dataAccountBank);
    cmbCompteBancaire.attr('disabled', false);
}

function reviewRatePenalite(taux) {

    var valueRemise = (amountPenalite * parseInt(taux) / 100);
    amountRemise = amountPenalite - valueRemise;
    spnAmountPenaliteRemise.html(formatNumber(amountRemise, deviseSelected));

}

function applyReviewPenalite() {

    for (var i = 0; i < listAmountPenalites.length; i++) {

        if (listAmountPenalites[i].assujCode == assujettissementSelected) {

            listAmountPenalites[i].amountPenalite = listAmountPenalites[i].amountPenalite -= amountRemise;
            listAmountPenalites[i].tauxRemise = cmbTauxRemise.val();
            listAmountPenalites[i].observationRemise = inputObservationRemiseTaux.val();


            var idColPenalite = $("#penalite_" + assujettissementSelected);
            var idColCalcelPenalite = $("#btnCalcelPenalite_" + assujettissementSelected);

            idColPenalite.html(formatNumber(amountRemise, deviseSelected));
            idColPenalite.attr('style', 'font-weight:bold;color:red;font-size:18px;text-align:right;width:11%;vertical-align:middle');

            idColCalcelPenalite.attr('style', 'text-align:center;width:11%;vertical-align:middle');

//            totalSum -= amountPenalite;
//            totalSum += amountRemise;
//
//            var lblTotalGeneral = $("#lblTotalGeneral");
//            lblTotalGeneral.html(formatNumber(totalSum, deviseSelected));

            modalReviewAmountPenalite.modal('hide');
        }
    }
}

function loadPeriodeDeclarationByAssyj(codeX) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadPeriodeDeclarationByAssyj',
            'code': codeX
        },
        beforeSend: function () {
            //alert('OK');
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else {

                    cmdPeriodeDeclarationIDx.html(empty);
                    cmdTauxPenalite.val('0');

                    periodeDeclarationAssujList = JSON.parse(JSON.stringify(response));


                    var declarationData;
                    var defautlValue = '--';

                    declarationData += '<option value="0">' + defautlValue + '</option>';

                    for (var i = 0; i < periodeDeclarationAssujList.length; i++) {
                        declarationData += '<option value="' + periodeDeclarationAssujList[i].periodeID + '">' + periodeDeclarationAssujList[i].periode + '</option>';
                    }

                    cmdPeriodeDeclarationIDx.html(declarationData);

                    $('#cmdPeriodeDeclarationIDx option').prop('selected', false).trigger('chosen:updated');

                    modalPeriodeDeclaration.modal('show');
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadPeriodeDeclarationByAssyjV2(codeX) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadPeriodeDeclarationByAssyj',
            'code': codeX
        },
        beforeSend: function () {
            //alert('OK');
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else {

                    cmdPeriodeDeclarationIDx.html(empty);


                    var result = JSON.parse(JSON.stringify(response));

                    //alert(result.length);

                    var declarationData;
                    var defautlValue = '--';

                    declarationData += '<option value="0">' + defautlValue + '</option>';

                    for (var i = 0; i < result.length; i++) {
                        declarationData += '<option value="' + result[i].periodeID + '">' + result[i].periode + '</option>';
                    }

                    cmdPeriodeDeclarationIDx.html(declarationData);

                    $('#cmdPeriodeDeclarationIDx option').prop('selected', false).trigger('chosen:updated');
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }
    });
}   