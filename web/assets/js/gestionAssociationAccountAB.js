

var selectBanque, selectCompteBancaire, btnSave, tableData, selectImpot;


$(function () {

    mainNavigationLabel.text('GESTION DES BANQUES');
    secondNavigationLabel.text('Associer le compte bancaire à l\'impot');

    removeActiveMenu();
    linkMenuOrdonnancement.addClass('active');

    selectBanque = $('#selectBanque');
    selectCompteBancaire = $('#selectCompteBancaire');
    tableData = $('#tableData');
    selectImpot = $('#selectImpot');
    btnSave = $('#btnSave');


    btnSave.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectBanque.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;
        }

        if (selectCompteBancaire.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un compte bancaire valide');
            return;
        }

        if (selectImpot.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un impot valide');
            return;
        }

        alertify.confirm('Etes-vous sûr de vouloir enregistrer ces informations ?', function () {
            save();
        });

    });

    selectBanque.on('change', function (e) {

        if (selectBanque.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner une banque valdie');
            return;
        } else {
            loadingAccountBankDataV3(selectBanque.val());
        }
    });

    loadArticleBudgetaireAssujetissable();
    getInfoAccountBankAB();

});

function loadArticleBudgetaireAssujetissable() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadArticleBudgetaireAssujetissable'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1' | response == '0') {

                showResponseError();
                return;

            } else {

                var cbData = '<option value="0">--</option>';

                var dataAB = JSON.parse(JSON.stringify(response));

                for (var i = 0; i < dataAB.length; i++) {

                    var ab = dataAB[i];
                    cbData += '<option value="' + ab.codeAB + '">' + ab.intituleAB.toUpperCase() + '</option>';

                }

                selectImpot.html(cbData);
                loadingBankData();

            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function getInfoAccountBankAB() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'getInfoAccountBankAB'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                showResponseError();
                return;

            } else if (response == '0') {


            } else {
                var result = JSON.parse(JSON.stringify(response));
                printTableData(result);
            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function save() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'codeBanque': selectBanque.val(),
            'codeCompteBancaire': selectCompteBancaire.val(),
            'codeArticleBudgetaire': selectImpot.val(),
            'userId': userData.idUser,
            'operation': 'saveAssociation'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;

            } else if (response == '2') {

                alertify.alert('Désolé, car cette affectation existe déjà dans le système !');
                return;

            } else {

                alertify.alert('L\'enregistrement s\'est effectué avec succès');

                //selectBanque.val('0');
                selectCompteBancaire.val('0');
                //selectImpot.val('0');

                getInfoAccountBankAB();
            }

        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function loadingBankData() {

    var dataBank = empty;
    var defautlValue = '--';
    dataBank += '<option value="0">' + defautlValue + '</option>';

    var banqueUserList = JSON.parse(userData.listAllBanques);

    for (var i = 0; i < banqueUserList.length; i++) {

        dataBank += '<option value="' + banqueUserList[i].codeBanque + '" >' + banqueUserList[i].libelleBanque + '</option>';

    }

    selectBanque.html(dataBank);
}

function loadingAccountBankDataV3(bankCode) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadingAccountBankDataV3',
            'codeBanque': bankCode
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();

                if (response == '-1') {

                    showResponseError();
                    return;

                } else {

                    var dataAccountBank = empty;
                    var defautlValue = '--';

                    dataAccountBank += '<option value="0">' + defautlValue + '</option>';

                    accountBankList = JSON.parse(JSON.stringify(response));

                    for (var i = 0; i < accountBankList.length; i++) {

                        dataAccountBank += '<option value="' + accountBankList[i].accountCode + '" >' + accountBankList[i].accountName + '</option>';

                    }

                    selectCompteBancaire.html(dataAccountBank);
                    selectCompteBancaire.attr('disabled', false);

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printTableData(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:20%">BANQUE</th>';
    tableContent += '<th style="text-align:left;width:20%">COMPTE BANCAIRE</th>';
    tableContent += '<th style="text-align:left;width:45%">IMPOT</th>';
    tableContent += '<th style="text-align:center;width:5%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var btnDeleteData = '<button class="btn btn-danger" onclick="deleteData(\'' + result[i].id + '\')"><i class="fa fa-trash-o"></i></button>';

        tableContent += '<tr>';

        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle;font-weight:bold">' + result[i].libelleBanque + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].libelleAccountBank + '</td>';
        tableContent += '<td style="text-align:left;width:45%;vertical-align:middle">' + result[i].libelleAB + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + btnDeleteData + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableData.html(tableContent);

    tableData.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des remises est vide",
            search: "Filtrer la liste ici _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 50,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        select: {
            style: 'os',
            blurable: true
        }
    });
}

function deleteData(id) {

    alertify.confirm('Etes-vous sûr de vouloir désactiver cette affectation ?', function () {

        $.ajax({
            type: 'POST',
            url: 'assujetissement_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'id': id,
                'operation': 'deleteData'
            },
            beforeSend: function () {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Désactivation en cours ...</h5>'});
            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1' || response == '0') {
                    showResponseError();
                    return;
                } else {
                    alertify.alert('La désactivation s\'est effectuée avec succès');
                    getInfoAccountBankAB();
                }

            },
            complete: function () {
            },
            error: function () {
                $.unblockUI();
                showResponseError();
            }
        });
    });

}



