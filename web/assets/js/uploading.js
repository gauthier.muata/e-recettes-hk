/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var uploadModal, visualisationDocModal;
var imagePosition;
var lblNavigate, lblNameImage;
var divBtnsNatigate;
var btnNext, btnPreview, btnValiderDocument;
var textNumDoc, textObservDoc, cmbDocumentType;
var listArchivesDocument;
var cmbDocumentType;
var imageList = [];

var typeDocumentList = [];
var codeTypeDocumentSelected = '';
var codeTypeDoc = '';

$(function () {

    codeTypeDocumentSelected = '';
    btnNext = $('#btnNext');
    btnPreview = $('#btnPreview');
    btnValiderDocument = $('#btnValiderDocument');
    lblNavigate = $('#lblNavigate');
    lblNameImage = $('#lblNameImage');
    divBtnsNatigate = $('#divBtnsNatigate');
    uploadModal = $('#uploadModal');
    visualisationDocModal = $('#visualisationDocModal');
    textNumDoc = $('#textNumDoc');
    textObservDoc = $('#textObservDoc');
    cmbDocumentType = $('#cmbDocumentType');

    cmbDocumentType.on('change', function (e) {
        codeTypeDocumentSelected = cmbDocumentType.val();
    });

    btnValiderDocument.click(function (e) {

        e.preventDefault();

        if (imageList.length == 0) {
            alertify.alert('Veuillez d\'abord sélectionner la pièce à joindre');
            return;
        }

        if (imageList.length > 0 && codeTypeDoc == empty) {
            alertify.alert('Veuillez d\'abord sélectionner le type du document');
            return;
        }

        setDocComplementaryData();

        listArchivesDocument = JSON.stringify(imageList);
        uploadModal.modal('hide');

        getDocumentsToUpload();

    });

    btnNext.click(function (e) {
        e.preventDefault();
        navigate(1);
    });

    btnPreview.click(function (e) {
        e.preventDefault();
        navigate(-1);
    });

    uplodingTypeDocument(codeTypeDoc);
});

function uplodingTypeDocument(codeTypeDoc) {

    $.ajax({
        type: 'POST',
        url: 'general_servlet',
        dataType: 'Json',
        crossDomain: false,
        data: {
            'operation': 'uploadingTypeDoc'
        },
        beforeSend: function () {
            uploadModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des données en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                uploadModal.unblock();
                if (response == '-1' || response == '0') {
                    cmbDocumentType.html(empty);
                    showResponseError();
                    return;
                } else {

                    var resultData = JSON.parse(JSON.stringify(response));

                    var dataTypeDocument = '';

                    if (resultData.length > 0) {

                        if (codeTypeDoc == '') {
                            dataTypeDocument = '<option value ="0">-- Sélectionner un type document --</option>';
                            for (var i = 0; i < resultData.length; i++) {
                                dataTypeDocument += '<option value =' + resultData[i].codeTypeDocument + '>' + resultData[i].libelleTypeDocument + '</option>';
                            }
                        } else {
                            for (var i = 0; i < resultData.length; i++) {
                                if (resultData[i].codeTypeDocument == codeTypeDoc) {
                                    dataTypeDocument += '<option selected value =' + resultData[i].codeTypeDocument + '>' + resultData[i].libelleTypeDocument + '</option>';
                                }

                            }
                        }

                        cmbDocumentType.html(dataTypeDocument);
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            uploadModal.unblock();
            showResponseError();
        }
    });
}

function initUpload(list, typeDoc) {

    codeTypeDoc = typeDoc;
    var img = document.getElementById('imgDocument');
    imageList.length = 0;

    if (list === undefined) {

        imagePosition = 0;
        $("#imgDocument").hide();
        img.src = '';

        divBtnsNatigate.attr('style', 'display:none');
        btnNext.attr('disabled', 'true');
        lblNavigate.html('');
        lblNameImage.html('');

        textNumDoc.val('');
        textNumDoc.attr('disabled', 'true');
        textObservDoc.val('');
        $("#textPieceJointe").val('');

    } else {
        list = JSON.parse(list);
        imagePosition = list.length - 1;

        for (var i = 0; i < list.length; i++) {

            if (i === imagePosition) {
                nextSteep(
                        list[imagePosition].name,
                        list[imagePosition].pvDocument
                        );

            } else {

                var image = {};
                image.name = list[i].name;
                image.pvDocument = list[i].pvDocument;
                image.observation = '';
                imageList.push(image);
            }

        }
    }

    cmbDocumentType.val(codeTypeDoc);
    cmbDocumentType.attr('disabled', true);


    uploadModal.modal('show');

    var modal = document.getElementById('myModal');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("lblPaginator");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = (imagePosition) + '/' + imageList.length + ' - ' + img.alt;
    };

    var btnClose = document.getElementById("btnClose");

    btnClose.onclick = function () {
        modal.style.display = "none";
    };

    $('#btnLeft').click(function (e) {
        e.preventDefault();
        navigate(-1);
    });

    $('#btnRight').click(function (e) {
        e.preventDefault();
        navigate(1);
    });
}

function initPrintData(declarationDocumentList) {

    imageList = [];
    imagePosition = 1;

    declarationDocumentList = JSON.parse(declarationDocumentList);

    for (var i = 0; i < declarationDocumentList.length; i++) {

        var data = declarationDocumentList[i].declarationDocument;
        var observation = declarationDocumentList[i].observation;
        var titleDocument = declarationDocumentList[i].libelleDocument;


        var image = {};
        image.name = 'Aperçu du document';
        image.pvDocument = data;
        image.observation = observation;
        image.libelleDocument = titleDocument;
        imageList.push(image);
    }

    sendDocument(0);

    $('#btnLeft2').click(function (e) {
        e.preventDefault();
        navigatePrint(-1);
    });

    $('#btnRight2').click(function (e) {
        e.preventDefault();
        navigatePrint(1);
    });

}

function readUrl(input) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {

            var name = $("#textPieceJointe")[0].files[0].name;
            var base64 = e.target.result;

            if (!ifExistImage(name)) {
                nextSteep(name, base64);
            }
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function ifExistImage(name) {

    var result = false;

    for (var i = 0; i < imageList.length; i++) {
        if (imageList[i].name === name) {
            result = true;
            break;
        }
    }
    return result;
}

function navigate(sens) {

    var length = imageList.length;

    switch (sens) {
        case -1 :
            if ((imagePosition - 1) > 0) {
                imagePosition = (imagePosition - 1);
                btnNext.removeAttr('disabled', 'true');
            } else {
                btnPreview.attr('disabled', 'true');
                btnNext.removeAttr('disabled', 'true');
            }
            break;
        case 1:
            if (imagePosition < length) {
                imagePosition++;
                btnPreview.removeAttr('disabled', 'true');
            } else {
                btnNext.attr('disabled', 'true');
                btnPreview.removeAttr('disabled', 'true');
            }
    }

    var img = document.getElementById('imgDocument');
    img.src = imageList[imagePosition - 1].pvDocument;

    lblNameImage.html(imageList[imagePosition - 1].name + ' <i class="fa fa-close" onclick="retirer()" style="color:red;font-size:20px" title="Supprimer"></i>');
    lblNavigate.html((imagePosition) + ' sur ' + length);
    $('#imgDocument').prop('alt', imageList[imagePosition - 1].name);

    //----------------------------------------------//
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("lblPaginator");

    modalImg.src = img.src;
    captionText.innerHTML = (imagePosition) + '/' + imageList.length + ' - ' + img.alt;

}

function retirer() {

    alertify.confirm('Etes-vous sûre de vouloir supprimer cette image ?', function () {

        for (var i = 0; i < imageList.length; i++) {

            if (i === (imagePosition - 1)) {

                imageList.splice(i, 1);

                var length = imageList.length;

                if (imagePosition > 1) {
                    if ((imagePosition - 1) > 0) {
                        imagePosition--;
                    } else if (imagePosition < length) {
                        imagePosition++;
                    }
                }

                if (length === 0) {
                    document.getElementById('imgDocument').src = '';
                    $("#imgDocument").hide();
                    lblNameImage.html('');
                    lblNavigate.html('');
                    divBtnsNatigate.attr('style', 'display:none');
                    imagePosition = 0;
                } else {
                    if (length === 1) {
                        divBtnsNatigate.attr('style', 'display:none');
                    } else {
                        divBtnsNatigate.attr('style', 'display:inline');
                    }
                    document.getElementById('imgDocument').src = imageList[imagePosition - 1].pvDocument;
                    lblNameImage.html(imageList[imagePosition - 1].name + ' <i class="fa fa-close" onclick="retirer()" style="color:red;font-size:20px" title="Supprimer"></i>');
                    lblNavigate.html((imagePosition) + ' sur ' + length);
                }
                break;
            }
        }
    });

}

function getUploadedData() {

    return listArchivesDocument;
}

function sendDocument(position) {

    visualisationDocModal.modal('show');

    var modal = document.getElementById('myModal2');
    var modalImg = document.getElementById("img02");
    var captionText = document.getElementById("lblPaginator2");

    modal.style.display = "block";
    modalImg.src = imageList[position].pvDocument;

    captionText.innerHTML = (position + 1) + '/' + imageList.length + ' - ' + imageList[position].libelleDocument;

    // AFFICHAGE DE L'OBSERVATION
    var captionText2 = document.getElementById("caption2");
    captionText2.innerHTML = imageList[position].observation;

    //ZOOMAGE
//    img02.onclick = function () {
//        img02.zoom;
//    }
}

function navigatePrint(sens) {

    var length = imageList.length;

    switch (sens) {
        case -1 :
            if ((imagePosition - 1) > 0) {
                imagePosition = (imagePosition - 1);
                $('#btnRight2').removeAttr('disabled', 'true');
            } else {
                $('#btnLeft2').attr('disabled', 'true');
                $('#btnRight2').removeAttr('disabled', 'true');
            }
            break;
        case 1:
            if (imagePosition < length) {
                imagePosition++;
                $('#btnLeft2').removeAttr('disabled', 'true');
            } else {
                $('#btnRight2').attr('disabled', 'true');
                $('#btnLeft2').removeAttr('disabled', 'true');
            }
    }

    sendDocument(imagePosition - 1);

}

function nextSteep(name, base64) {

    $("#imgDocument").show();

    var image = {};
    image.name = name;
    image.pvDocument = base64;
    image.observation = '';
    imageList.push(image);

    var length = imageList.length;
    imagePosition = length;

    document.getElementById('imgDocument').src = imageList[length - 1].pvDocument;

    $('#imgDocument').prop('alt', name);

    if (length < 1) {
        divBtnsNatigate.attr('style', 'display:none');
        btnNext.attr('disabled', 'true');
        lblNavigate.html('');
    } else if (length === 1) {
        divBtnsNatigate.attr('style', 'display:none');
        btnNext.attr('disabled', 'true');
        lblNavigate.html((imagePosition) + ' sur ' + length);
    } else {
        divBtnsNatigate.attr('style', 'display:inline');
        btnNext.removeAttr('disabled', 'true');
        lblNavigate.html((imagePosition) + ' sur ' + length);
    }
    lblNameImage.html(name + ' <i class="fa fa-close" onclick="retirer()" style="color:red;font-size:20px" title="Supprimer"></i>');
    $("#textPieceJointe").val('');

}

function setDocComplementaryData() {

    if (textObservDoc.val().trim() != '') {

        for (var i = 0; i < imageList.length; i++) {
            imageList[i].observation = textObservDoc.val();
            imageList[i].typeDoc = codeTypeDocumentSelected;
        }
    }

}