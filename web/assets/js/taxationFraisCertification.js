/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var numeroNoteVersement, agenceName, societeName, produit, nombreLot, cotePartDrhkat, dateTaxation,
        selectBanque, selectCompteBancaire, checkBoxJoinDocument, btnSaveTaxation;

var codeDoc = undefined;
var codeTypeDocs = 'NOTE_DEBIT';
var checkBoxJoinDocument;
var archivages;
var complementInfoTaxeList = [];
var btnSearchPerson;

var lblAdresse;
var btnCloseModalSearchPerson, btnCloseModalUpload;

$(function () {

    mainNavigationLabel.text('TAXATION');
    secondNavigationLabel.text('Taxation frais de certification');

    removeActiveMenu();
    linkSubMenuEditerPaiement.addClass('active');


    modaTaxationFraisCertification = $('#modaTaxationFraisCertification');
    numeroNoteVersement = $('#numeroNoteVersement');
    agenceName = $('#agenceName');
    societeName = $('#societeName');

    produit = $('#produit');
    nombreLot = $('#nombreLot');
    cotePartDrhkat = $('#cotePartDrhkat');
    dateTaxation = $('#dateTaxation');
    selectBanque = $('#selectBanque');
    selectCompteBancaire = $('#selectCompteBancaire');
    checkBoxJoinDocument = $('#checkBoxJoinDocument');
    btnSaveTaxation = $('#btnSaveTaxation');
    btnSearchPerson = $('#btnSearchPerson');
    lblAdresse = $('#lblAdresse');
    btnCloseModalSearchPerson = $('#btnCloseModalSearchPerson');
    btnCloseModalUpload = $('#btnCloseModalUpload');


    nombreLot.on('change', function (e) {

        if (nombreLot.val() <= 0) {
            alertify.alert('Le nombre du lot ne doit pas être inférireur à zéro');
            return;
        } else {
            calculateCotePart();
        }

    });

    numeroNoteVersement.on('change', function (e) {

        if (numeroNoteVersement.val() == empty) {
            alertify.alert('Veuillez d\'abord saisir le numéro de la note de débit');
            return;
        } else {

            if (numeroNoteVersement.val().length < 6) {
                alertify.alert('Le numéro de la numéro de la note de débit ne soit pas inférieur à six caractères');
                return;
            } else {
                checkNumberNoteDebit();
            }

        }

    });

    btnCloseModalUpload.on('click', function (e) {

        e.preventDefault();
        uploadModal.modal('hide');
        modaTaxationFraisCertification.modal('show');
    });

    btnCloseModalSearchPerson.on('click', function (e) {

        e.preventDefault();
        assujettiModal.modal('hide');
        modaTaxationFraisCertification.modal('show');
    });

    btnSearchPerson.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        assujettiModal.modal('show');
        modaTaxationFraisCertification.modal('hide');
    });

    btnSaveTaxation.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();
    });

    selectBanque.change(function (e) {

        e.preventDefault();

        if (selectBanque.val() !== '0') {
            loadingAccountBankData(selectBanque.val());
        } else {

            selectCompteBancaire.val('0');
            selectCompteBancaire.attr('disabled', true);
            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;
        }

    });

    loadingBankData();
    modaTaxationFraisCertification.modal('show');

})

function checkFields() {

    if (numeroNoteVersement.val() == '') {
        alertify.alert('Veuillez d\'abord saisir le numéro de la note de débit');
        return;
    }

    if (societeName.val() == '') {
        alertify.alert('Veuillez d\'abord rechercher et sélectionner la société');
        return;
    }

    /*if (agenceName.val() == '') {
     alertify.alert('Veuillez d\'abord saisir le nom de l\'agence');
     return;
     }*/

    if (produit.val() == '') {
        alertify.alert('Veuillez d\'abord saisir la nature du produit');
        return;
    }

    if (nombreLot.val() == '') {

        alertify.alert('Veuillez d\'abord saisir le nombre du lot');
        return;

    } else if (nombreLot.val() < 0) {

        alertify.alert('Le nombre du lot ne doit pas inférireur à zéro');
        return;
    }

    if (cotePartDrhkat.val() == '') {
        alertify.alert('Veuillez d\'abord saisir la cote part de la DRHKAT');
        return;
    } else if (cotePartDrhkat.val() < 0) {

        alertify.alert('La cote part de la DRHKAT ne doit pas inférireur à zéro');
        return;
    }

    if (dateTaxation.val() == '' || dateTaxation.val() == null) {
        alertify.alert('Veuillez d\'abord fournir la date de taxation');
        return;
    }

    if (selectBanque.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner la banque');
        return;
    }

    if (selectCompteBancaire.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner le compte bancaire');
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir enregistrer cette taxation ?', function () {

        complementInfoTaxeObj = {};
        complementInfoTaxeList = [];

        complementInfoTaxeObj.agence = agenceName.val();
        complementInfoTaxeObj.produit = produit.val();
        complementInfoTaxeObj.numeroNoteVersement = numeroNoteVersement.val();
        complementInfoTaxeObj.cotePart = parseFloat(cotePartDrhkat.val());
        complementInfoTaxeObj.nombreLot = parseInt(nombreLot.val());
        complementInfoTaxeObj.banque = selectBanque.val();
        complementInfoTaxeObj.compteBancaire = selectCompteBancaire.val();
        complementInfoTaxeObj.fkAb = "00000000000002392021";
        complementInfoTaxeObj.dateTaxation = dateTaxation.val();

        complementInfoTaxeList.push(complementInfoTaxeObj);

        var checkControl = document.getElementById("checkBoxJoinDocument");

        if (checkControl.checked) {
            modaTaxationFraisCertification.modal('hide');
            initUpload(codeDoc, codeTypeDocs);
        } else {
            saveNoteCalcul();
        }
    });
}

function loadingBankData() {


    var dataBank = empty;
    var defautlValue = '--';
    dataBank += '<option value="0">' + defautlValue + '</option>';

    var banqueUserList = JSON.parse(userData.banqueUserList);

    for (var i = 0; i < banqueUserList.length; i++) {

        dataBank += '<option value="' + banqueUserList[i].banqueCode + '" >' + banqueUserList[i].banqueName + '</option>';

    }

    selectBanque.html(dataBank);
}

function loadingAccountBankData(bankCode) {

    var dataAccountBank = empty;
    var defautlValue = '--';

    dataAccountBank += '<option value="0">' + defautlValue + '</option>';

    var accountBankList = JSON.parse(userData.accountList);

    for (var i = 0; i < accountBankList.length; i++) {
        if (bankCode == accountBankList[i].acountBanqueCode && accountBankList[i].typeCompteCode == 1) {
            dataAccountBank += '<option value="' + accountBankList[i].accountCode + '" >' + accountBankList[i].accountName + '</option>';
        }
    }

    selectCompteBancaire.html(dataAccountBank);
    selectCompteBancaire.attr('disabled', false);
}

function saveNoteCalcul() {

    addBudgetArticleInTaxationList();

    var taxations = JSON.stringify(listTaxationArticle);

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'codeResponsible': responsibleObject.codeResponsible,
            'codeAdresse': responsibleObject.adresseId,
            'codeSite': userData.SiteCode,
            'userId': userData.idUser,
            'depotDeclaration': null,
            'codeService': userData.serviceCode,
            'detailTaxation': taxations,
            'archives': archivages,
            'complementInfoTaxe': JSON.stringify(complementInfoTaxeList),
            'operation': 'saveNoteCalculFraisCertification'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Création de la note de taxation en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                }
                if (response == '0') {
                    alertify.alert('Echec opération. Une erreur s\'est produite lors de la création de la note de taxation !');
                    return;
                } else {
                    alertify.alert('L\'enregistrement de la taxation s\'est efectuée avec succès');
                    listTaxationArticle = new Array();
                    objetDetailNoteCalcul = new Object();

                    complementInfoTaxeObj = {};
                    complementInfoTaxeList = [];

                    setTimeout(function () {
                        window.location = 'taxation-frais-certification';
                    }, 1000);
                    return;
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function getSelectedAssujetiData() {
    //var valueName = '<span style="font-weight:bold">' + selectAssujettiData.nomComplet + '</span>';

    societeName.val(selectAssujettiData.nomComplet);
    lblAdresse.html('(Adresse : ' + selectAssujettiData.adresse + ')');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    assujettiModal.modal('hide');
    modaTaxationFraisCertification.modal('show');
}

function getDocumentsToUpload() {

    archivages = getUploadedData();

    if (archivages == null) {

        alertify.alert('Veuillez d\'abord joindre la note de débit pour cette taxation');
        return;
    } else {
        saveNoteCalcul();
    }

}

function addBudgetArticleInTaxationList() {

    listTaxationArticle = [];
    objetDetailNoteCalcul = new Object();

    objetDetailNoteCalcul.budgetArticleCode = '00000000000002392021';
    objetDetailNoteCalcul.budgetArticleText = 'Frais de certification';
    objetDetailNoteCalcul.baseCalcul = nombreLot.val();
    objetDetailNoteCalcul.unity = 'LOT';
    objetDetailNoteCalcul.unityCode = 'U00802015';
    objetDetailNoteCalcul.quantity = 1;
    objetDetailNoteCalcul.devise = 'USD';
    objetDetailNoteCalcul.typeTaux = '%';
    objetDetailNoteCalcul.codeTarif = '0000001820';

    //var typeTaux = '%';
    var tauxPalier = parseFloat(37.5);

    objetDetailNoteCalcul.amount = parseFloat(tauxPalier * 1 * nombreLot.val());
    objetDetailNoteCalcul.taux = tauxPalier;

    /*switch (typeTaux) {
     case 'F':
     objetDetailNoteCalcul.amount = (tauxPalier * 1 * nombreLot.val());
     objetDetailNoteCalcul.taux = tauxPalier;
     objetDetailNoteCalcul.tauxDisplay = tauxPalier;
     break;
     case '%':
     objetDetailNoteCalcul.amount = (nombreLot.val() * (tauxPalier / 100) * 1);
     objetDetailNoteCalcul.tauxDisplay = tauxPalier + ' (' + typeTaux + ')';
     objetDetailNoteCalcul.taux = tauxPalier;
     break;
     }*/

    listTaxationArticle.push(objetDetailNoteCalcul);
}

function goToClotureNote() {
    setRegisterType('RNCO');
}

function calculateCotePart() {
    cotePartDrhkat.val(parseFloat(nombreLot.val() * 37.5));
}

function checkNumberNoteDebit() {

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'value': numeroNoteVersement.val().substr(0, 6),
            'operation': 'checkNumberNoteDebit'
        },
        beforeSend: function () {
            modaTaxationFraisCertification.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification en cours ...</h5>'});
        },
        success: function (response)
        {

            modaTaxationFraisCertification.unblock();

            if (response == '-1') {

                showResponseError();
                return;

            } else if (response == '1') {

                var valueNB = '<span style="font-weight:bold">' + numeroNoteVersement.val() + '</span>';
                alertify.alert('Cette note de débit : ' + valueNB + ' est déjà utilisée dans une autre taxation');
                return;

            }

        },
        complete: function () {
        },
        error: function (xhr) {
            modaTaxationFraisCertification.unblock();
            showResponseError();
        }
    });
}