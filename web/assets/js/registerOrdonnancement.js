/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var tableTaxation, inputResearchTaxation, inputNotePerceptionManuel, typeResearch;
var btnResearchTaxation, btnCallModalSearchAvanded, btnResearchTaxationClosed,
        btnResearchTaxationRejected, btnValidateNpManuel;
var dataTaxations, tableDetailTaxation;

var lblNoteCalcul, lblLegalForm, lblNameResponsible, lblAdress, lblExercice, lblAmount;
var btnClosedTaxation;
var inputObservation, cmbAvis, inputDateOrdonnancement;
var modalDetailNoteCalcul, notePerceptionManulModal;
var codeAvis;
var dataAvis;
var divDateOrdonnancement;

var numeroNcSelected;

var registerType;

var notePerception = new Object();
var nc = new Object();

var npManuel = '';
var npManuelIsValid = '';
var code = '';

var totalAmount;

var modalRechercheAvanceeNC;

var useNpGenerate;
var checkDate;

var isAdvance, lblSite, lblService, lblDateDebut, lblDateFin;
var lblInfoValidation, lblInfoOrdonnateur, lblInfoPaiement, lblInfoTaxateur;
var btnVoirDeclaration;
var tableComplementInfoTaxation, divTableComplementInfoTaxation;
var deviseSelected;

$(function () {

    mainNavigationLabel.text('ORDONNANCEMENT');
    secondNavigationLabel.text('Ordonnancer les notes de taxation');

    removeActiveMenu();
    linkMenuOrdonnancement.addClass('active');

    codeAvis = '';
    registerType = '';

    tableTaxation = $('#tableTaxation');
    tableDetailTaxation = $('#tableDetailTaxation');

    inputResearchTaxation = $('#inputResearchTaxation');
    inputResearchTaxation.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
    inputObservation = $('#inputObservation');
    inputObservation.val('');
    inputNotePerceptionManuel = $('#inputNotePerceptionManuel');
    inputDateOrdonnancement = $('#inputDateOrdonnancement');
    inputNotePerceptionManuel.val('');
    inputDateOrdonnancement.val('');

    typeResearch = $('#typeResearch');
    btnResearchTaxation = $('#btnResearchTaxation');
    btnClosedTaxation = $('#btnClosedTaxation');
    btnCallModalSearchAvanded = $('#btnCallModalSearchAvanded');
    btnResearchTaxationClosed = $('#btnResearchTaxationClosed');
    btnResearchTaxationRejected = $('#btnResearchTaxationRejected');
    btnValidateNpManuel = $('#btnValidateNpManuel');

    divDateOrdonnancement = $('#divDateOrdonnancement');

    modalDetailNoteCalcul = $('#modalDetailNoteCalcul');
    notePerceptionManulModal = $('#notePerceptionManulModal');

    modalRechercheAvanceeNC = $('#modalRechercheAvanceeModelTwo');

    lblExercice = $('#lblExercice');
    lblNoteCalcul = $('#lblNoteCalcul');
    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAdress = $('#lblAdress');
    lblAmount = $('#lblAmount');

    cmbAvis = $('#cmbAvis');

    isAdvance = $('#isAdvance');
    lblSite = $('#lblSite');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    lblInfoValidation = $('#lblInfoValidation');
    lblInfoOrdonnateur = $('#lblInfoOrdonnateur');
    lblInfoPaiement = $('#lblInfoPaiement');
    lblInfoTaxateur = $('#lblInfoTaxateur');

    btnVoirDeclaration = $('#btnVoirDeclaration');

    tableComplementInfoTaxation = $('#tableComplementInfoTaxation');
    divTableComplementInfoTaxation = $('#divTableComplementInfoTaxation');

    checkDate = true;

    cmbAvis.on('change', function (e) {
        codeAvis = cmbAvis.val();
    });

    loadAllAvis();

    btnResearchTaxation.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (inputResearchTaxation.val() === empty || inputResearchTaxation.val().length < SEARCH_MIN_TEXT) {
            showEmptySearchMessage();
            return;
        }
        loadTaxationNotClosed();

    });

    btnValidateNpManuel.on('click', function (e) {
        e.preventDefault();
        saveNotePerceptionManuel();
    });


    typeResearch.on('change', function (e) {
        inputResearchTaxation.val('');
        if (typeResearch.val() === '1') {
            inputResearchTaxation.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
        } else if (typeResearch.val() === '2') {
            inputResearchTaxation.attr('placeholder', 'Veuillez saisir le numéro de la note de taxation');
        }
    });

    inputResearchTaxation.keypress(function (e) {
        if (e.keyCode === 13) {
            btnResearchTaxation.trigger('click');
        }
    });

    btnCallModalSearchAvanded.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('show');
    });

    btnClosedTaxation.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        callDialogConfirmOrdonnancerTaxation();
    });


    btnSimpleSearch = $('#btnSimpleSearch');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    btnShowModalAdvencedRechearch = $('#btnRechercheAvancee');

    btnShowModalAdvencedRechearch.click(function (e) {
        e.preventDefault();
        $('#modalRechercheAvanceeModelTwo').modal('show');
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        taxationByAdvancedSearch();
    });

    btnAdvencedSearch.trigger('click');

    loadTaxationTable('');
    loadDetailTaxationTable('');
});

function loadTaxationTable(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center;width:6%"scope="col">EXERCICE</th>';
    tableContent += '<th style="text-align:left;width:17%"scope="col">SERVICE S\'ASSIETTE</th>';
    tableContent += '<th style="text-align:left;width:12%"scope="col">NOTE DE TAXATION</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">DATE CREATION</th>';
    tableContent += '<th style="text-align:left;width:16%"scope="col">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left;width:24%"scope="col">TAXE</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:center;width:6%"scope="col"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var sumCDF = 0;
    var sumUSD = 0;

    for (var i = 0; i < result.length; i++) {

        var firstLineAB = '';

        if (result[i].libelleArticleBudgetaire.length > 250) {
            firstLineAB = result[i].libelleArticleBudgetaire.substring(0, 250).toUpperCase() + ' ...';
        } else {
            firstLineAB = result[i].libelleArticleBudgetaire.toUpperCase();
        }

        if (result[i].devisePalier === 'CDF') {
            sumCDF += result[i].totalAmount2;
        } else if (result[i].devisePalier === 'USD') {
            sumUSD += result[i].totalAmount2;
        }

        var btnDisplayBienTaxation = '';

        if (result[i].bienTaxationExist == 1) {
            btnDisplayBienTaxation = '<br/><br/><button class="btn btn-warning" onclick="loadBiensTaxation(\'' + result[i].noteCalcul + '\')"><i class="fa fa-home"></i>&nbsp;&nbsp;Afficher le(s) bien(s)</button>';
        }

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;text-align:center">' + result[i].exerciceFiscal + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + result[i].libelleService + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + result[i].noteCalcul + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + result[i].dateCreate + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + result[i].nom + ' (' + result[i].libelleFormeJuridique + ')' + '</td>';
        tableContent += '<td style="text-align:left;vertical-align:middle; title="' + result[i].libelleArticleBudgetaire + '"><span style="font-weight:bold;"></span>' + firstLineAB + '</td>';
        tableContent += '<td style="text-align:right;vertical-align:middle">' + formatNumber(result[i].totalAmount2, result[i].devisePalier) + '</td>';
        tableContent += '<td style="text-align:center;vertical-align:middle"><button class="btn btn-primary" onclick="displayDetailTaxation(\'' + result[i].noteCalcul + '\',\'' + result[i].declarationExist + '\',\'' + result[i].complementExist + '\')"><i class="fa fa-list"></i>&nbsp;&nbsp;Afficher les détails</button>' + btnDisplayBienTaxation + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="6" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th></th></tr>';

    tableContent += '</tfoot>';

    tableTaxation.html(tableContent);

    var myDataTable = tableTaxation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 25,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        columnDefs: [
            {"visible": false, "targets": 5}
        ],
        order: [[5, 'asc']],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(6).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD'));
        },
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(5, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

}

function loadTaxationNotClosed() {

    var viewAllSite = controlAccess('VIEW_ALL_SITES_NC');
    var viewAllService = controlAccess('VIEW_ALL_SERVICES_NC');

    registerType = getRegisterType();

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputResearchTaxation.val(),
            'typeSearch': typeResearch.val(),
            'typeRegister': registerType,
            'allSite': viewAllSite,
            'allService': viewAllService,
            'codeSite': userData.SiteCode,
            'codeService': userData.serviceCode,
            'userId': userData.idUser,
            'operation': 'researchTaxation'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'})
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {

                    if (dataTaxations.length > 0) {
                        dataTaxations = '';
                        loadTaxationTable('');
                    }
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fournis.');
                } else {
                    dataTaxations = JSON.parse(JSON.stringify(response));
                    if (dataTaxations.length > 0) {

                        btnClosedTaxation.show();
                        if (controlAccess('MANUEL_DATE_ORDONNANCEMENT')) {
                            divDateOrdonnancement.show();
                        }
                        loadTaxationTable(dataTaxations);
                    } else {
                        btnClosedTaxation.hide();
                        divDateOrdonnancement.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });

}

function displayDetailTaxation(noteCalcul, declarationExist, complementInfo) {

    if (declarationExist == '1') {
        btnVoirDeclaration.attr('style', 'display:inline');
    } else {
        btnVoirDeclaration.attr('style', 'display:none');
    }

    for (i = 0; dataTaxations.length; i++) {
        if (dataTaxations[i].noteCalcul === noteCalcul) {

            numeroNcSelected = noteCalcul;
            lblNoteCalcul.text('N° taxation : ' + dataTaxations[i].noteCalcul);
            lblNameResponsible.text('Assujetti : ' + dataTaxations[i].nom);
            lblAdress.html('Adresse : ' + dataTaxations[i].adresseAssujetti);
            lblAmount.html(formatNumber(dataTaxations[i].totalAmount2, dataTaxations[i].devisePalier));

            lblInfoTaxateur.text('Taxer par : ' + dataTaxations[i].agentTaxateur);
            lblInfoValidation.text('Clôturer par : ' + dataTaxations[i].agentCloture);
            lblInfoOrdonnateur.text('Ordonnancer par : ' + dataTaxations[i].agentOrdonnateur);
            lblInfoPaiement.text('Infos paiement : ' + dataTaxations[i].noteCalcul);

            loadDetailTaxationTable($.parseJSON(dataTaxations[i].listDetailNcs), complementInfo);
            btnClosedTaxation.show();
            if (controlAccess('MANUEL_DATE_ORDONNANCEMENT')) {
                divDateOrdonnancement.show();
            }

            totalAmount = dataTaxations[i].totalAmount2;

            notePerception = new Object();
            nc = new Object();

            nc.dateCreateNc = dataTaxations[i].dateCreate;
            nc.dateValidation = new Date();
            nc.observation = inputObservation.val();
            nc.avis = codeAvis;
            nc.userValidate = userData.idUser;
            nc.numeroNc = dataTaxations[i].noteCalcul;
            nc.codeResponsible = dataTaxations[i].codeResponsible;
            nc.exerciceFiscal = dataTaxations[i].exerciceFiscal;

            notePerception.codeSite = userData.SiteCode;
            notePerception.codeService = userData.serviceCode;
            notePerception.exerciceFiscal = dataTaxations[i].exerciceFiscal;
            notePerception.npManuel = '';
            notePerception.codeSite = userData.SiteCode;
            notePerception.numeroNc = dataTaxations[i].noteCalcul;
            notePerception.userCreate = userData.idUser;
            notePerception.etat = '2';
            notePerception.netAPayer = totalAmount;
            notePerception.solde = totalAmount;
            notePerception.payer = 0;
            notePerception.dateCreate = new Date();

            btnVoirDeclaration.click(function (e) {
                e.preventDefault();
                initPrintData(dataTaxations[i].declarationDocumentList);
            });

            modalDetailNoteCalcul.modal('show');
            return;
        }
    }
}

function callOrdonnancementTaxation() {

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'noteCalcul': nc.numeroNc,
            'idAvis': codeAvis,
            'observationOrdonnancement': inputObservation.val(),
            'userId': userData.idUser,
            'netAPaye': notePerception.netAPayer,
            'useNpGenerate': useNpGenerate,
            'npManuel': notePerception.npManuel,
            'codeResponsible': nc.codeResponsible,
            'exerciceFiscal': nc.exerciceFiscal,
            'codeSite': notePerception.codeSite,
            'codeService': notePerception.codeService,
            'remise': 0,
            'npMere': '',
            'dateValidation': inputDateOrdonnancement.val(),
            'operation': 'ordonnancerTaxation'
        },
        beforeSend: function () {
            modalDetailNoteCalcul.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ordonnancement taxation en cours ...</h5>'})
        },
        success: function (response)
        {
            setTimeout(function () {

                modalDetailNoteCalcul.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                }
                if (response == '5') {
                    alertify.alert('Votre site n\'a plus de note de perception disponible. Veuillez contactez le comptable de matière de votre site');
                    return;
                }
                if (response == '0') {
                    alertify.alert('Echec opération. Une erreur s\'est produite lors de l\'ordonnancement de la note de taxation');
                    return;
                } else {
                    modalDetailNoteCalcul.modal('hide');
                    refreshTaxationList();

                    switch (response) {
                        case 'KO':
                            alertify.alert('Cette note de taxation est avisée et rejétée.');
                            break;
                        default:
                            alertify.alert('La note de perception a été crée avec comme référence : ' + response);
                            break;
                    }

                    inputObservation.val('');
                    codeAvis = '';
                    cmbAvis.val('');
                    inputDateOrdonnancement.val('');
                    return;
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            modalDetailNoteCalcul.unblock();
            showResponseError();
        }
    });
}

function loadAllAvis() {

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'getListAvis'
        },
        beforeSend: function () {
        },
        success: function (response)
        {
            setTimeout(function () {

                if (response == '-1') {
                    showResponseError();
                    return;
                }
                dataAvis = JSON.parse(JSON.stringify(response));
                var dataAvisDisplay = '<option value ="0">-- Sélectionner un avis --</option>';
                for (var i = 0; i < dataAvis.length; i++) {

                    dataAvisDisplay += '<option value =' + dataAvis[i].idAvis + '>' + dataAvis[i].libelleAvis + '</option>';
                }
                cmbAvis.html(dataAvisDisplay);

            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            alertify.alert('Une erreur inattendue est survenue. Veuillez réesayer ou contacter l\'administrateur.');
        }
    });
}

function loadDetailTaxationTable(result, exist) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:30%"scope="col">TAXE</th>';
    tableContent += '<th style="text-align:right;width:6%"scope="col">BASE</th>';
    tableContent += '<th style="text-align:center;width:5%"scope="col">NBR.ACTE</th>';
    tableContent += '<th style="text-align:center;width:6%"scope="col">UNITE</th>';
    tableContent += '<th style="text-align:right;width:6%"scope="col">TAUX</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">MONTANT DÛ</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        deviseSelected = result[i].devisePalier;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:30%;vertical-align:middle">' + result[i].libelleArticleBudgetaire + '</td>';
        tableContent += '<td style="text-align:right;width:6%;vertical-align:middle">' + result[i].baseCalcul + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + result[i].quantity + '</td>';
        tableContent += '<td style="text-align:center;width:6%;vertical-align:middle">' + result[i].libelleUnite + '</td>';
        tableContent += '<td style="text-align:right;width:6%;vertical-align:middle">' + result[i].tauxPalier + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle">' + formatNumber(result[i].amount, result[i].devisePalier) + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableDetailTaxation.html(tableContent);

    var myDataTable = tableDetailTaxation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le detail des articles budgétaires est vide",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });

    if (exist == '1') {
        displayInfosComplement(numeroNcSelected);
    } else {
        divTableComplementInfoTaxation.attr('style', 'display: none');
    }
}

function callDialogConfirmOrdonnancerTaxation() {

    if (codeAvis === '' || codeAvis === '0') {
        alertify.alert('Veuillez d\'abord sélectionner votre avis sur la taxation en cours d\'ordonnancement.');
        return;
    }

    if (cmbAvis.val() === 'AV0022015' && inputObservation.val() == empty) {

        alertify.alert('Veuillez d\'abord motiver la raison de votre rejet');
        return;

    }

    var dateOrdonnance = getDateSelect();
    var dateJour = getDateJour();

    if (controlAccess('MANUEL_DATE_ORDONNANCEMENT')) {
        if (inputDateOrdonnancement.val() === '') {
            alertify.alert('Veuillez d\'abord indiquer la date de l\'ordonnancemet.');
            return;
        } else if (dateOrdonnance > dateJour) {
            alertify.alert('La date de l\'ordonnancemet ne peut pas être supérieure à celle du jour.');
            return;
        }
    }

    alertify.confirm('Etes-vous sûre de vouloir ordonnancer cette taxation ?', function () {
        ordonnancerTaxation(nc, notePerception);
    });
}

function ordonnancerTaxation(nc, np) {

    code = '';
    var listAvis = JSON.parse(userData.avisList);

    for (i = 0; i < listAvis.length; i++) {
        if (listAvis[i].idAvis == codeAvis) {
            code = listAvis[i].codeAvis;
        }
    }

    useNpGenerate = controlAccess('USE_NP_GENERATE');
    npManuel = controlAccess('MANUEL_NUMERO_NP');

    switch (code) {
        case 'OK':
            if (!useNpGenerate && npManuel) {
                notePerceptionManulModal.modal('show');
            } else {
                callOrdonnancementTaxation();
            }
            break;
        case 'KO':
            callOrdonnancementTaxation();
            break;
    }
}

function refreshTaxationList() {
    if (dataTaxations.length > 0) {

        for (i = 0; dataTaxations.length; i++) {
            if (dataTaxations[i].noteCalcul === nc.numeroNc) {
                dataTaxations.splice(i, 1);
                if (dataTaxations.length > 0) {
                    loadTaxationTable(dataTaxations);
                } else {
                    loadTaxationTable('');
                }
                return;
            }
        }
    }
}

function taxationByAdvancedSearch() {

    registerType = getRegisterType();

    lblSite.html($('#selectSite option:selected').text().toUpperCase());
    lblService.html($('#selectService option:selected').text().toUpperCase());
    lblDateDebut.html(inputDateDebut.val());
    lblDateFin.html(inputdateLast.val());
    isAdvance.attr('style', 'display: block');

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'SiteCode': selectSite.val(),
            'serviceCode': selectService.val(),
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'typeRegister': registerType,
            'operation': 'researchAdvancedTaxation'
        },
        beforeSend: function () {
            modalRechercheAvanceeNC.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'})
        },
        success: function (response)
        {
            setTimeout(function () {
                modalRechercheAvanceeNC.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    modalRechercheAvanceeNC.modal('hide');
                    dataTaxations = '';
                    loadTaxationTable('');
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fournis.');
                } else {
                    dataTaxations = JSON.parse(JSON.stringify(response));
                    if (dataTaxations.length > 0) {
                        btnClosedTaxation.show();
                        if (controlAccess('MANUEL_DATE_ORDONNANCEMENT')) {
                            divDateOrdonnancement.show();
                        }
                        modalRechercheAvanceeNC.modal('hide');
                        loadTaxationTable(dataTaxations);
                    } else {
                        btnClosedTaxation.hide();
                        divDateOrdonnancement.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalRechercheAvanceeNC.unblock();
            showResponseError();
        }

    });
}

function saveNotePerceptionManuel() {

    if (inputNotePerceptionManuel.val() === '') {

        alertify.alert('Veuillez d\'abord fournir le numéro de la note de perception.');
        return;

    } else {

        alertify.confirm('Etes-vous sûre de vouloir confirmer ce numéro de la note de perception ?', function () {

            $.ajax({
                type: 'POST',
                url: 'taxation_servlet',
                dataType: 'text',
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                crossDomain: true,
                data: {
                    'numeroNp': inputNotePerceptionManuel.val().trim(),
                    'operation': 'checkValidateNpManuel'
                },
                beforeSend: function () {
                    notePerceptionManulModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification de la note en cours ...</h5>'});
                },
                success: function (response)
                {

                    notePerceptionManulModal.unblock();

                    if (response == '-1') {
                        showResponseError();
                        return;

                    } else if (response == '1') {

                        var value = '<span style="font-weight:bold">' + inputNotePerceptionManuel.val().toUpperCase().trim() + '</span>';
                        alertify.alert('Cette note de perception : ' + value + ' est déja utilisée dans le système.');
                        return;

                    } else if (response == '0') {
                        
                        npManuel = inputNotePerceptionManuel.val();
                        notePerception.npManuel = npManuel;
                        notePerceptionManulModal.modal('hide');
                        callOrdonnancementTaxation();

                    }
                },
                complete: function () {

                },
                error: function (xhr) {
                    notePerceptionManulModal.unblock();
                    showResponseError();
                }

            });

        });

    }

}

function getDateSelect() {

    var dateOrdonnancement

    if (inputDateOrdonnancement !== '') {
        dateOrdonnancement = inputDateOrdonnancement.val();
        dateOrdonnancement = dateOrdonnancement.replace(/-/g, '');
        dateOrdonnancement = parseInt(dateOrdonnancement);

        return dateOrdonnancement;
    } else {
        dateOrdonnancement = '';
    }

    return dateOrdonnancement;
}

function getDateJour() {
    var date = new Date();

    var year = date.getFullYear() + '';

    var month = date.getMonth() + 1 + '';
    if (month.length == 1) {
        month = '0' + month;
    }

    var day = date.getDate() + '';
    if (day.length == 1) {
        day = '0' + day;
    }

    var dateJour = parseInt(year + month + day);

    return dateJour;
}

function displayInfosComplement(numero) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:30%"scope="col">TRANSITEUR</th>';
    tableContent += '<th style="text-align:left;width:20%"scope="col">NATURE DE PRODUIT</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">N° PLAQUE CAMION</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">MODE DE PAIEMENT</th>';

    tableContent += '<th style="text-align:center;width:10%"scope="col">TONNAGE SORTIE</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">MONTANT TONNE</th>';
    tableContent += '<th style="text-align:left;width:20%"scope="col">AGENT VALIDATION</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < dataTaxations.length; i++) {

        if (dataTaxations[i].noteCalcul == numero) {

            var dataComplementInfoList = JSON.parse(dataTaxations[i].complementList);

            for (var j = 0; j < dataComplementInfoList.length; j++) {

                var agentInfo = 'Par : ' + '<span style="font-weight:bold">' + dataComplementInfoList[j].agentValidate + '</span>';
                var dateInfo = 'Le : ' + '<span style="font-weight:bold">' + dataComplementInfoList[j].dateValidation + '</span>';

                var infoUser = agentInfo + '<br/>' + dateInfo;

                tableContent += '<tr>';

                tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + dataComplementInfoList[j].transporteur + '</td>';
                tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + dataComplementInfoList[j].produit + '</td>';
                tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + dataComplementInfoList[j].plaque + '</td>';
                tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + dataComplementInfoList[j].modePaiement + '</td>';


                tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + formatNumberOnly(dataComplementInfoList[j].tonnageSortie) + '</td>';
                tableContent += '<td style="text-align:right;width:10%;vertical-align:middle">' + formatNumber(dataComplementInfoList[j].montantTonnageSortie, deviseSelected) + '</td>';
                tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + infoUser + '</td>';

                tableContent += '</tr>';

                tableContent += '</tbody>';
                tableComplementInfoTaxation.html(tableContent);

                var myDataTable = tableComplementInfoTaxation.DataTable({
                    language: {
                        processing: "Traitement en cours...",
                        track: "Rechercher&nbsp;:",
                        lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                        info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                        infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        infoPostFix: "",
                        loadingRecords: "Chargement en cours...",
                        zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        emptyTable: "Le detail des articles budgétaires est vide",
                        search: "Rechercher par N° document _INPUT_  ",
                        paginate: {
                            first: "Premier",
                            previous: "Pr&eacute;c&eacute;dent",
                            next: "Suivant",
                            last: "Dernier"
                        },
                        aria: {
                            sortAscending: ": activer pour trier la colonne par ordre croissant",
                            sortDescending: ": activer pour trier la colonne par ordre décroissant"
                        }
                    },
                    info: false,
                    destroy: true,
                    searching: false,
                    paging: false,
                    lengthChange: false,
                    tracking: false,
                    ordering: false,
                    pageLength: 7,
                    lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
                    datalength: 3
                });
            }

            divTableComplementInfoTaxation.attr('style', 'display: inline');

        }
    }
}

