/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var modalCorrectionBienAutomobileVignette,
        selectTypeCorrection,
        divItem1,
        numeroNoteDeleted,
        divItem2,
        selectBankAccountBank,
        selectNewAccountBank,
        divItem3,
        amountNote,
        numeroNoteUpdate1,
        numeroNoteUpdate2,
        btnValidateAction,
        divItem4;

var divItem4,
        numeroNoteUpdate3,
        selectDevise;

var numeroPlaque, numeroVignette, numeroVignetteActuel, numeroVignetteNouveau;
var numeroNoteTaxationVignette;
var divItem5, notePerceptionBanque;

$(function () {

    mainNavigationLabel.text('REPERTOIRE');
    secondNavigationLabel.text('Correction Bien automobile vignette');

    removeActiveMenu();
    linkSubMenuEditerPaiement.addClass('active');

    modalCorrectionBienAutomobileVignette = $('#modalCorrectionBienAutomobileVignette');
    selectTypeCorrection = $('#selectTypeCorrection');
    divItem1 = $('#divItem1');
    numeroNoteDeleted = $('#numeroNoteDeleted');
    divItem2 = $('#divItem2');
    selectBankAccountBank = $('#selectBankAccountBank');
    selectNewAccountBank = $('#selectNewAccountBank');
    divItem3 = $('#divItem3');
    divItem4 = $('#divItem4');
    btnValidateAction = $('#btnValidateAction');
    amountNote = $('#amountNote');
    numeroNoteUpdate1 = $('#numeroNoteUpdate1');
    numeroNoteUpdate2 = $('#numeroNoteUpdate2');

    divItem4 = $('#divItem4');
    numeroNoteUpdate3 = $('#numeroNoteUpdate3');

    selectDevise = $('#selectDevise');

    numeroPlaque = $('#numeroPlaque');
    numeroVignette = $('#numeroVignette');
    numeroVignetteActuel = $('#numeroVignetteActuel');
    numeroVignetteNouveau = $('#numeroVignetteNouveau');
    numeroNoteTaxationVignette = $('#numeroNoteTaxationVignette');

    
    divItem5 = $('#divItem5');
    notePerceptionBanque = $('#notePerceptionBanque');
    
    selectTypeCorrection.on('change', function (e) {

        if (selectTypeCorrection.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un type de correction valide');
            return;

        } else {

            switch (selectTypeCorrection.val()) {

                case '1' : //Suppression de la plaque

                    divItem1.attr('style', 'display:inline');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem5.attr('style', 'display:none');

                    numeroPlaque.val('');

                    break;

                case '2' : //Réinitialiser le nombre d'impression d'une vignette

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:inline');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem5.attr('style', 'display:none');
                    
                    numeroVignette.val('');
                    numeroNoteTaxationVignette.val('');
                    break;

                case '3' : //Modifier la série de la vignette

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:inline');
                    divItem4.attr('style', 'display:none');
                    divItem5.attr('style', 'display:none');

                    numeroVignetteActuel.val('');
                    numeroVignetteNouveau.val('');
                    numeroNoteTaxationVignette.val('');

                    break;
                case '4' : //Réactiver la note de taxation de la vignette

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem5.attr('style', 'display:none');
                    divItem4.attr('style', 'display:inline');

                  
                    numeroNoteTaxationVignette.val('');

                    break;
                    
                case '5' : //Libérer la note de perception banque

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem5.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem5.attr('style', 'display:inline');

                    notePerceptionBanque.val('');
                    

                    break;

            }
        }
    });

    btnValidateAction.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectTypeCorrection.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un type de correction valide');
            return;

        } else {

            switch (selectTypeCorrection.val()) {

                case '1' : //Libérer une plaque d'immatriculation 

                    if (numeroPlaque.val() == empty) {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la plaque');
                        return;
                    } else {
                        changePlaqueVehicule();
                    }

                    break;

                case '2' : //Réinitialiser le nombre d'impression d'une vignette

                    if (numeroVignette.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la vignette');
                        return;
                    }

                    resetNumberImpressionVignette();

                    break;

                case '3' : //Modifier la série de la vignette

                    if (numeroVignetteActuel.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la vignette actuelle');
                        return;
                    }

                    if (numeroVignetteNouveau.val() == '') {

                        alertify.alert('Veuillez d\'abord fournir le nouveau numéro de la vignette actuelle');
                        return;

                    }

                    modifyNumberVignette();
                    break;

                case '4':

                    if (numeroNoteTaxationVignette.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir le numéro de la note de taxation vignette');
                        return;
                    }
                    
                    activateNoteTaxationVignette();

                    break;
                    
                case '5':

                    if (notePerceptionBanque.val() == '') {
                        alertify.alert('Veuillez d\'abord fournir la note de perception banque');
                        return;
                    }
                    
                    resetNotePerceptionBanque();

                    break;

            }
        }
    });

    modalCorrectionBienAutomobileVignette.modal('show');

})

function changePlaqueVehicule() {

    var value = '<span style="font-weight:bold">' + numeroPlaque.val().trim().toUpperCase() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir libérer ce numéro de plaque n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'plaque': numeroPlaque.val().trim().toUpperCase(),
                'userId': userData.idUser,
                'operation': 'changePlaqueVehicule'

            },
            beforeSend: function () {
                modalCorrectionBienAutomobileVignette.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionBienAutomobileVignette.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette plaque d\'immatriculation n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else if (response == '3') {

                    alertify.alert('SUPPRESSION IMPOSSIBLE. <br/><br/> Cette plaque d\'immatriculation n° ' + value + ' est déjà associée à une taxation !');
                    return;

                } else {

                    alertify.alert('La libération de la plaque d\'immatriculation n° ' + value + ' s\'est effectuée avec succès');

                    numeroPlaque.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');

                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionBienAutomobileVignette.unblock();
                showResponseError();
            }

        });
    });

}

function resetNumberImpressionVignette() {

    var value = '<span style="font-weight:bold">' + numeroVignette.val().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir Réinitialiser le nombre d\'impression de la vignette n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'numeroVignette': numeroVignette.val().trim().toUpperCase(),
                'userId': userData.idUser,
                'operation': 'resetNumberImpressionVignette'

            },
            beforeSend: function () {
                modalCorrectionBienAutomobileVignette.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionBienAutomobileVignette.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette vignette n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else {

                    alertify.alert('La Réinitialisation du nombre d\'impression de la vignette n° ' + value + ' s\'est effectuée avec succès');

                    numeroVignette.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');

                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionBienAutomobileVignette.unblock();
                showResponseError();
            }

        });
    });

}

function modifyNumberVignette() {

    var value = '<span style="font-weight:bold">' + numeroVignetteActuel.val().trim().toUpperCase() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir modifier le numéro de la vignette n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'vignetteOld': numeroVignetteActuel.val().trim().toUpperCase(),
                'userId': userData.idUser,
                'vignetteNew': numeroVignetteNouveau.val().trim().toUpperCase(),
                'operation': 'modifyNumberVignette'

            },
            beforeSend: function () {
                modalCorrectionBienAutomobileVignette.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionBienAutomobileVignette.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette vignette n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else {

                    alertify.alert('La modification de la vignette n° ' + value + ' s\'est effectuée avec succès');

                    numeroVignetteActuel.val('');
                    numeroVignetteNouveau.val('');

                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');

                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionBienAutomobileVignette.unblock();
                showResponseError();
            }

        });
    });

}

function activateNoteTaxationVignette() {

    var value = '<span style="font-weight:bold">' + numeroNoteTaxationVignette.val().trim().toUpperCase() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir réactiver la note de taxation vignette n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'noteTaxation': numeroNoteTaxationVignette.val().trim().toUpperCase(),
                'userId': userData.idUser,
                'operation': 'activateNoteTaxationVignette'

            },
            beforeSend: function () {
                modalCorrectionBienAutomobileVignette.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionBienAutomobileVignette.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '3') {
                    
                    alertify.alert('Cette note de taxation n° ' + value + ' n\'est pas liée à la vignette');
                    return;
                    
                } else if (response == '2') {

                    alertify.alert('Cette note de taxation vignette n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else {

                    alertify.alert('La réactivation de la note de taxation vignette n° ' + value + ' s\'est effectuée avec succès');

                    numeroNoteTaxationVignette.val('');
          
                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');

                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionBienAutomobileVignette.unblock();
                showResponseError();
            }

        });
    });

}

function resetNotePerceptionBanque() {

    var value = '<span style="font-weight:bold">' + notePerceptionBanque.val().trim() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir libérer cette note de perception banque n° ' + value + ' ? ', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'notePerception': notePerceptionBanque.val().trim(),
                'userId': userData.idUser,
                'operation': 'resetNotePerceptionBanque'

            },
            beforeSend: function () {
                modalCorrectionBienAutomobileVignette.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {
                modalCorrectionBienAutomobileVignette.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else if (response == '2') {

                    alertify.alert('Cette note de perception banque n° ' + value + ' n\'existe pas dans le système');
                    return;

                } else {

                    alertify.alert('La libération de la note de perception banque  n° ' + value + ' s\'est effectuée avec succès');

                    notePerceptionBanque.val('');
          
                    divItem1.attr('style', 'display:none');
                    divItem2.attr('style', 'display:none');
                    divItem3.attr('style', 'display:none');
                    divItem4.attr('style', 'display:none');
                    divItem5.attr('style', 'display:none');

                    selectTypeCorrection.val('0');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalCorrectionBienAutomobileVignette.unblock();
                showResponseError();
            }

        });
    });

}


