/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var modalValidationApurement;

var btnValiderApurement;
var inputNumeroReleveBancaire;
var inputDateRelevebancaire;

var advancedSearch;
var numberRows;

$(function () {

    mainNavigationLabel.text('APUREMENT');
    secondNavigationLabel.text('Registre des apurements provisoire');

    modalValidationApurement = $('#modalValiderApurement');

    btnValiderApurement = $('#btnValiderApurement');
    inputNumeroReleveBancaire = $('#inputNumeroReleveBancaire');
    inputDateRelevebancaire = $('#inputDateRelevebancaire');
    inputDateRelevebancaire.datepicker("setDate", new Date());

    removeActiveMenu();
    linkMenuApurement.addClass('active');

    btnSimpleSearch.click(function (e) {
        e.preventDefault();
        if (inputResearchValue.val() === empty || inputResearchValue.val().length < SEARCH_MIN_TEXT) {
            showEmptySearchMessage();
        } else {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            //advancedSearch = '0';
            jourrnalSearching(1, 1);
        }
    });
    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        //advancedSearch = '1';
        jourrnalSearching(1, 2);
    });

    loadJournalTable('', 1);
    
    btnAdvencedSearch.trigger('click');
});

function administratifApurementValidation(codeJournal, reference, agentApurement, montantApure) {

    modalValidationApurement.modal('show');

    btnValiderApurement.click(function (e) {
        e.preventDefault();
        saveApurementAdministratif(codeJournal, reference, agentApurement, montantApure);
    });

}

function saveApurementAdministratif(codeJournal, reference, agentApurement, montantApure) {

    if (inputNumeroReleveBancaire.val() === empty) {

        alertify.alert('Veuillez saisir le numéro du réléve bancaire');
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir apurer provisoirement ce paiement ?', function () {

        modalValidationApurement.modal('hide');

        $.ajax({
            type: 'POST',
            url: 'paiement_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'codeJournal': codeJournal,
                'documentApure': reference,
                'agentApurement': agentApurement,
                'montantApure': montantApure,
                'numeroReleveBancaire': inputNumeroReleveBancaire.val(),
                'dateReleveBancaire': inputDateRelevebancaire.val(),
                'operation': 'validationApurementAdmin'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Apurement provisoire en cours...</h5>'});
            },
            success: function (response)
            {
                $.unblockUI();
                if (response === '-1') {
                    showResponseError();
                    return;
                } else if (response === '0') {
                    showResponseError();
                    return;
                } else if (response === '1') {
                    setTimeout(function () {
                        $.unblockUI();
                        alertify.alert('L\'apurement provisoire a été effectué avec succès.');
                        jourrnalSearching(1, savedOperation);
                    }
                    , 1);
                } else {
                    showResponseError();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.unblockUI();
                showResponseError();
            }

        });
    });

}