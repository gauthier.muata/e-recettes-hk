/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CODE_INTERET_MORATOIRE = 'P00012015';
var CODE_PENALITE_ASSIETTE_CAS1 = 'P00082015';
var CODE_PENALITE_ASSIETTE_CAS2 = 'P000202015';

var tablePenalite,
        lblNameAssujetti,
        lblNUmerotitre,
        lblDateCreation,
        lblDateEacheace,
        lblTotalPenalite,
        lblpenaliteAssiete,
        lblMontantDu, lblCategorieAssujetti, lblMoisRetard, alertMessage;

var selectPenalite;

var inputPenaliteDu,
        inputTaux;

var btnValiderDoc,
        btnFermer,
        btnAjouter;

var totPenalite, totPrincipal;

var modalpenalite;

var DataTitre,
        ListPN;

var listePenalite = [], tempPenaliteList = [];

var checkTauxPenalite;

var codePersonne, documentReference, devise, typeTauxPenalite;

var objPenaliteTable = {};

var isOrdonnancement = 0, isTaxation = 0, isRecouvrement = 0;

var penaliteAssietteObj;
var interetMoratoireObj;

$(function () {

    totPenalite = 0;
    checkTauxPenalite = 0;

    modalpenalite = $('#modalpenalite');
    tablePenalite = $('#tablePenalite');
    selectPenalite = $('#selectPenalite');

    btnFermer = $('#btnFermer');
    btnValiderDoc = $('#btnValiderDoc');
    btnAjouter = $('#btnAjouter');

    lblNameAssujetti = $('#lblNameAssujetti');
    lblCategorieAssujetti = $('#lblCategorieAssujetti');
    lblNUmerotitre = $('#lblNUmerotitre');
    lblDateCreation = $('#lblDateCreation');
    lblDateEacheace = $('#lblDateEacheace');
    lblpenaliteAssiete = $('#lblpenaliteAssiete');
    lblMontantDu = $('#lblMontantDu');
    lblTotalPenalite = $('#lblTotalPenalite');
    lblMoisRetard = $('#lblMoisRetard');
    alertMessage = $('#alertMessage');

    inputPenaliteDu = $('#inputPenaliteDu');
    inputTaux = $('#inputTaux');

    selectPenalite.change(function (e) {
        e.preventDefault();
        if (selectPenalite.val() == '0') {
            inputPenaliteDu.val(0);
            inputTaux.val(0);
            return;
        }
        calculatePenalie(selectPenalite.val());
    });

    btnValiderDoc.click(function (e) {
        e.preventDefault();
        modalpenalite.modal('hide');
        validatePenalite();
    });

    btnAjouter.click(function (e) {

        e.preventDefault();

        if (selectPenalite.val() == '0') {
            alertify.alert('Veuillez sélectionner une pénalité avant d\'ajouter dans le panier');
            return;
        }

        if (inputPenaliteDu.val() == '0') {
            alertify.alert('Vous ne pouvez pas ajouter cette pénalité. Pénalité dû invalide.');
            return;
        }

        if (checkTauxPenalite == 1) {
            alertify.alert('Cette pénalité n\'a pas de taux. Veuillez la configurer correctement');

        } else {

            for (var i = 0; i < listePenalite.length; i++) {

                if (listePenalite[i].penaliteCode == objPenaliteTable.penaliteCode && listePenalite[i].numeroDoc == documentReference) {
                    alertify.alert('Cette pénalité est déjà dans le panier');
                    return;
                }
            }

            listePenalite.push(objPenaliteTable);
            tempPenaliteList.push(objPenaliteTable);

            selectPenalite.val('0');
            inputTaux.val(0);
            interetMoratoire = 0;
            checkTauxPenalite = 0;
            libellePenalite = empty;
            inputPenaliteDu.val(0);

            loadTablePenaliteByDoc(listePenalite, documentReference);

        }

    });

    btnFermer.click(function (e) {
        e.preventDefault();
        inputPenaliteDu.val('');
        inputTaux.val('');
        removeNoValidate();
    });

    loadPenaliteData();

});

/*
 * Appel du modal de gestion des pénalités
 */
function initUI(data) {

    if (ListPN.length > 0) {
        selectPenalite.val('0');
    }

    tempPenaliteList.length = 0;

    inputTaux.val(0);
    loadTablePenaliteByDoc('', '');
    calculateTotalAmount('');
    lblTotalPenalite.html('0');

    DataTitre = data;

    lblNameAssujetti.html(DataTitre.nomAssujetti);
    lblCategorieAssujetti.html(DataTitre.catAssujetti);
    lblNUmerotitre.html(DataTitre.numeroLabelDocument);
    lblDateCreation.html(DataTitre.dateCreate);
    lblDateEacheace.html(DataTitre.dateEcheance);
    lblMontantDu.html(formatNumber(DataTitre.montantDu, DataTitre.devise));
    codePersonne = DataTitre.codeAssujetti;
    documentReference = DataTitre.numeroDocument;
    devise = DataTitre.devise;
    lblpenaliteAssiete.html(getRecidivisteLabel(DataTitre.isRecidiviste));
    lblMoisRetard.html(DataTitre.moisRetard);

    if (parseInt(DataTitre.moisRetard) > 0) {

        alertMessage.html('Cette taxation est &eacute;ligible aux Int&eacute;r&ecirc;ts moratoires pour paiement tardif');
        constitutePenaliteList(false);

    } else {

        constitutePenaliteList(true);
        alertMessage.html('');
    }

    loadTablePenaliteByDoc(listePenalite, documentReference);

}

/*
 * Calcul et affiche la somme des pénalités par document référence
 */
function calculateTotalAmountByDoc(result) {
    totPenalite = 0;
    for (var i = 0; i < result.length; i++) {
        if (result[i].numeroDoc == documentReference) {
            totPenalite += result[i].interetMoratoire;
        }
        lblTotalPenalite.html(formatNumber(totPenalite, result[i].devisePenalite));
    }
}

/*
 * Calcul et affiche la somme des pénalités globales
 */
function calculateTotalAmount(result) {
    totPenalite = 0;
    for (var i = 0; i < result.length; i++) {
        totPenalite += result[i].interetMoratoire;
        lblTotalPenalite.html(formatNumber(totPenalite, result[i].devisePenalite));
    }
}

/*
 * Consititue et affiche la table des pénalités par document référence
 */
function loadTablePenaliteByDoc(result, numDoc) {

    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>\n\\n\
                <th style="text-align:left" hidden="true">Code pénalité</th>\n\\n\\n\
                <th style="text-align:left">Pénalité</th>\n\\n\
                <th style="text-aLign:left">Principel dû</th>\n\\n\
                <th style="text-aLign:left">Taux</th>\n\\n\\n\
                <th style="text-align:center">Mois de retard</th>\n\\n\\n\
                <th style="text-align:right">Pénalité dû</th>\n\\n\\n\
                <th style="text-align:center"> </th>\n\\n\
                </tr></thead>';
    var data = '';
    data += '<tbody id="bodyTable">';

    totPenalite = 0;

    for (var i = 0; i < result.length; i++) {

        if (result[i].numeroDoc == numDoc) {

            totPenalite += result[i].interetMoratoire;

            data += '<tr>';
            data += '<td style="text-align:left;width:5%" hidden="true">' + result[i].penaliteCode + '</td>';
            data += '<td style="text-align:left;width:30%">' + result[i].libellePenalite + '</td>';
            data += '<td style="text-align:left;width:15%">' + formatNumber(result[i].montantDu, result[i].devisePenalite) + '</td>';
            data += '<td style="text-align:left;width:15%">' + result[i].tauxPenalite + ' ' + result[i].typeTaux + '</td>';
            data += '<td style="text-align:center;width:15%">' + result[i].nombreMois + '</td>';
            data += '<td style="text-align:right;width:15%">' + formatNumber(result[i].interetMoratoire, result[i].devisePenalite) + '</td>';
            if (result[i].canDelete) {
                data += '<td style="text-align:center;width:5%"><button style="margin-top:5px" class="btn btn-danger" onclick="deletePenalite(\'' + result[i].penaliteCode + '\',\'' + numDoc + '\')"><i class="fa fa-trash-o"></i></button></td>';
            } else {
                data += '<td style="text-align:center;width:5%"></td>';
            }
            data += '</tr>';
        }
    }
    data += '</tbody>';
    var TableContent = header + data;
    tablePenalite.html(TableContent);
    tablePenalite.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune pénalité dans le panier",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 3,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        datalength: 3
    });

    if (result.length > 0) {
        lblTotalPenalite.html(formatNumber(totPenalite, DataTitre.devise));
    }
}

/*
 * Supprime une pénalité par document référence
 */
function deletePenalite(value, numDoc) {

    alertify.confirm('Etes-vous sûre de vouloir retirer cette pénalité dans le panier ?', function () {

        for (var i = 0; i < listePenalite.length; i++) {
            if (listePenalite[i].penaliteCode == value && listePenalite[i].numeroDoc == numDoc) {
                listePenalite.splice(i, 1);
            }
        }

        for (var i = 0; i < tempPenaliteList.length; i++) {
            if (tempPenaliteList[i].penaliteCode == value && tempPenaliteList[i].numeroDoc == numDoc) {
                tempPenaliteList.splice(i, 1);
            }
        }

        if (listePenalite.length > 0) {
            loadTablePenaliteByDoc(listePenalite, documentReference);
            calculateTotalAmountByDoc(listePenalite);
        } else {
            loadTablePenalite('');
            lblTotalPenalite.html(0);
        }
    });
}

/*
 * Charge la liste de toutes les pénalités se trouvant dans la base des données
 */
function loadPenaliteData() {

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        async: false,
        crossDomain: false,
        data: {
            'operation': 'getPenalite'
        },
        beforeSend: function () {

        },
        success: function (response)
        {
            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {

              ListPN = JSON.parse(JSON.stringify(response));

            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            showResponseError();
        }
    });
}

/*
 * Constitue et affiche les pénalités dans le combobox
 */
function constitutePenaliteList(hideInteretMoratoire) {

    var dataPenalite = '<option value="0">--- S&eacute;lectionner une p&eacute;nalit&eacute --</option>';
    for (var i = 0; i < ListPN.length; i++) {

        if (ListPN[i].IS_VISIBLE_UTILISATEUR) {

            if (ListPN[i].APPLIQUER_MOIS_RETARD && hideInteretMoratoire) {
                continue;
            }

            dataPenalite += '<option value="' + ListPN[i].CODE + '">' + ListPN[i].INTITULE + '</option>';

        }

    }

    selectPenalite.html(dataPenalite);
}

/*
 * Calcul et prépare l'objet pénalité
 */
function calculatePenalie(codePenalite) {

    var objPenalite;
    var objTaux;
    var interetMoratoire;
    var taux, valTaux, nbrMois, libellePenalite;

    var appliquerMoisPenalite = 0;

    for (var i = 0; i < ListPN.length; i++) {

        if (codePenalite === ListPN[i].CODE) {

            libellePenalite = ListPN[i].INTITULE;
            appliquerMoisPenalite = ListPN[i].APPLIQUER_MOIS_RETARD;
            objPenalite = ListPN[i];
            objTaux = JSON.parse(objPenalite.LIST_TAUX_PANALIE);

            if (objTaux.length > 0) {

                for (var h = 0; h < objTaux.length; h++) {

                    taux = objTaux[h].VALEUR;
                    if (objTaux[h].VALEUR === 0) {
                        valTaux = 1;
                    } else {
                        valTaux = objTaux[h].VALEUR
                    }

                    if (appliquerMoisPenalite == false) {
                        nbrMois = 1;
                    } else {
                        nbrMois = DataTitre.moisRetard;
                    }

                    switch (objTaux[h].EST_POURCENTAGE) {
                        case true:
                            interetMoratoire = (((DataTitre.montantDu) * valTaux) / 100) * nbrMois;
                            typeTauxPenalite = '%';
                            inputTaux.val(objTaux[h].VALEUR + '' + typeTauxPenalite);
                            break;
                        case false:
                            interetMoratoire = valTaux * nbrMois;
                            typeTauxPenalite = DataTitre.devise;
                            inputTaux.val(objTaux[h].VALEUR);
                            break;
                        default :
                    }
                }
                checkTauxPenalite = 0;
            } else {
                inputTaux.val(0);
                interetMoratoire = 0;
                checkTauxPenalite = 1;
                libellePenalite = '';
            }

            break;
        }
    }

    inputPenaliteDu.val(formatNumber(interetMoratoire, DataTitre.devise));
    objPenaliteTable = {};
    objPenaliteTable.interetMoratoire = interetMoratoire;
    objPenaliteTable.tauxPenalite = taux;
    objPenaliteTable.libellePenalite = libellePenalite;
    objPenaliteTable.montantDu = DataTitre.montantDu;
    objPenaliteTable.nombreMois = !appliquerMoisPenalite ? 0 : DataTitre.moisRetard;
    objPenaliteTable.devisePenalite = DataTitre.devise;
    objPenaliteTable.numeroDoc = documentReference;
    objPenaliteTable.typeTaux = typeTauxPenalite;
    objPenaliteTable.penaliteCode = codePenalite;
    objPenaliteTable.dateCreate = DataTitre.dateCreate;
    objPenaliteTable.dateLimite = DataTitre.dateEcheance;
    objPenaliteTable.canDelete = true;

}

/*
 * calcul la pénalité d'assiete et prépare l'objet pénalité
 */
function calculPenaliteAssiete(idPeriode, montantDu, isRediviste, devise) {

    var tauxPenaliteAssiete = 0;

    if (ListPN.length == 0) {
        alertify.alert('Aucune pénalité configuré');
        return;
    }
    
    if(isUndefined(isRediviste)) {
        isRediviste = 0;
    }

    for (var i = 0; i < ListPN.length; i++) {

        if (CODE_PENALITE_ASSIETTE_CAS1 == ListPN[i].CODE) {
            penaliteAssietteObj = ListPN[i];
            break;
        }
    }


    tauxPenaliteAssiete = getTaux(CODE_PENALITE_ASSIETTE_CAS1, penaliteAssietteObj.LIST_TAUX_PANALIE, isRediviste);

    var penaliteAssieteTemp = (montantDu * tauxPenaliteAssiete) / 100;

    emptyListID(idPeriode);

    var objPenalite = {};

    objPenalite.interetMoratoire = penaliteAssieteTemp;
    objPenalite.tauxPenalite = tauxPenaliteAssiete;
    objPenalite.libellePenalite = 'Pénalité d\'assiète';
    objPenalite.montantDu = montantDu;
    objPenalite.nombreMois = 0;
    objPenalite.devisePenalite = devise;
    objPenalite.numeroDoc = idPeriode;
    objPenalite.typeTaux = '%';
    objPenalite.isRecidiviste = isRediviste;
    objPenalite.penaliteCode = CODE_PENALITE_ASSIETTE_CAS1;
    objPenalite.canDelete = false;

    listePenalite.push(objPenalite);

    return penaliteAssieteTemp;
}

/*
 * calcul et retourne l'interet moratoire et sans préparer l'objet pénalité
 */
function calculteInteretMoratoireV3(montantDu, isRecidiviste, moisRetard) {

    var tauxInteretMoratoire = 0;
    var interetMoratoire = 0;

//    for (var i = 0; i < ListPN.length; i++) {
//
//        if (CODE_INTERET_MORATOIRE == ListPN[i].CODE) {
//            interetMoratoireObj = ListPN[i];
//            break;
//        }
//    }
//
//    tauxInteretMoratoire = getTaux(CODE_INTERET_MORATOIRE, interetMoratoireObj.LIST_TAUX_PANALIE, isRecidiviste);
//
//    interetMoratoire = (montantDu * (tauxInteretMoratoire / 100)) * moisRetard;

    return interetMoratoire;

}

/*
 * 
 */
function calculteInteretMoratoireV2ByDoc(docNum, montantDu, isRecidiviste, moisRetard, devise) {

    var tauxInteretMoratoire = 0;
    var interetMoratoire = 0;

    for (var i = 0; i < ListPN.length; i++) {

        if (CODE_INTERET_MORATOIRE == ListPN[i].CODE) {
            interetMoratoireObj = ListPN[i];
            break;
        }
    }

    tauxInteretMoratoire = getTaux(CODE_INTERET_MORATOIRE, interetMoratoireObj.LIST_TAUX_PANALIE, isRecidiviste);

    interetMoratoire = (montantDu * (tauxInteretMoratoire / 100)) * moisRetard;

    var objPenalite = {};

    objPenalite.interetMoratoire = interetMoratoire;
    objPenalite.tauxPenalite = tauxInteretMoratoire;
    objPenalite.libellePenalite = 'Intérêts moratoires';
    objPenalite.montantDu = montantDu;
    objPenalite.nombreMois = moisRetard;
    objPenalite.devisePenalite = devise;
    objPenalite.numeroDoc = docNum;
    objPenalite.typeTaux = '%';
    objPenalite.isRecidiviste = isRecidiviste;
    objPenalite.penaliteCode = CODE_INTERET_MORATOIRE;
    listePenalite.push(objPenalite);

    return interetMoratoire;

}

/*
 * 
 */
function calculteInteretMoratoireByDoc(docNum, montantDu, isRecidiviste, moisRetard, devise) {

    var tauxInteretMoratoire = 0;
    var tauxPenaliteAssiete = 0;
    var interetMoratoire = 0;

    for (var i = 0; i < ListPN.length; i++) {

        if (CODE_INTERET_MORATOIRE == ListPN[i].CODE) {
            interetMoratoireObj = ListPN[i];
            break;
        }

        if (CODE_PENALITE_ASSIETTE_CAS1 == ListPN[i].CODE) {
            penaliteAssietteObj = ListPN[i];
            break;
        }
    }

    tauxInteretMoratoire = getTaux(CODE_INTERET_MORATOIRE, interetMoratoireObj.LIST_TAUX_PANALIE, isRecidiviste);
    tauxPenaliteAssiete = getTaux(CODE_PENALITE_ASSIETTE_CAS1, penaliteAssietteObj.LIST_TAUX_PANALIE, isRecidiviste);

    var interetMoratoireTemp = (montantDu + (montantDu * (tauxPenaliteAssiete / 100)));

    interetMoratoire = (interetMoratoireTemp * (tauxInteretMoratoire / 100)) * moisRetard;


    var objPenalite = {};

    objPenalite.interetMoratoire = interetMoratoire;
    objPenalite.tauxPenalite = tauxInteretMoratoire;
    objPenalite.libellePenalite = 'Intérêts moratoires';
    objPenalite.montantDu = montantDu;
    objPenalite.nombreMois = moisRetard;
    objPenalite.devisePenalite = devise;
    objPenalite.numeroDoc = docNum;
    objPenalite.typeTaux = '%';
    objPenalite.isRecidiviste = isRecidiviste;
    objPenalite.penaliteCode = CODE_INTERET_MORATOIRE;

    listePenalite.push(objPenalite);

    return interetMoratoire;

}

/*
 * 
 */
function getTaux(codePenalite, listTaux, isRecidiviste) {

    var tauxList = JSON.parse(listTaux);
    var taux = 0;

    if (CODE_INTERET_MORATOIRE == codePenalite) {

        taux = tauxList[0].VALEUR;
    }

    if (CODE_PENALITE_ASSIETTE_CAS1 == codePenalite) {

        for (var i = 0; i < tauxList.length; i++) {
            if (isRecidiviste == 1) {
                if (tauxList[i].RECIDIVE == true) {
                    taux = tauxList[i].VALEUR;
                    return taux;
                }
            } else {
                taux = tauxList[i].VALEUR;
                return taux;
            }
        }
    }

    return taux;
}

/*
 * 
 */
function getSumByDoc(numDoc) {

    var penaliteSUM = 0;

    for (var j = 0; j < listePenalite.length; j++) {

        if (listePenalite[j].numeroDoc == numDoc) {
            penaliteSUM += listePenalite[j].interetMoratoire;
            break;
        }
    }

    return penaliteSUM;
}

/*
 * 
 */
function getRecidivisteLabel(isRecidiviste) {

    if (isRecidiviste == 1) {
        return 'OUI';
    } else {
        return 'NON';
    }
}

/*
 * 
 */
function emptyListID(numDoc) {

    var newPenaliteList = [];

    var size = listePenalite.length;

    for (var i = 0; i < size; i++) {

        if (listePenalite[i].numeroDoc != numDoc) {
            newPenaliteList.push(listePenalite[i]);
            break;
        }
    }

    listePenalite = newPenaliteList;
}

/*
 * 
 */
function removeNoValidate() {

    var size = tempPenaliteList.length;

    for (var i = 0; i < size; i++) {

        for (var j = 0; j < listePenalite.length; j++) {

            if (listePenalite[j].numeroDoc == tempPenaliteList[i].numeroDoc &&
                    listePenalite[j].penaliteCode == tempPenaliteList[i].penaliteCode) {

                listePenalite.splice(j, 1);

            }
        }
    }

}

/*
 * 
 */
function getPenaliteListByDoc(numDoc) {

    var tempList = [];

    for (var j = 0; j < listePenalite.length; j++) {
        if (listePenalite[j].numeroDoc == numDoc) {
            tempList.push(listePenalite[j]);
            break;
        }
    }

    return tempList;
}
