/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var tableSousProvision,
        btnCallModalSearchAvanded,
        selectSocieteMiniere,
        selectPeriode,
        modalResearchContribuableMinier;

var dataSousprovision;
var btnAdvencedSearch;

var modalAddNewOrUpdateManifeste,
        inputTransporteur,
        inputNatureProduit,
        inputNumeroPlaque,
        inputTonnage,
        inputMontantDu,
        btnConfirmationValidation;

var btnAddNewManifeste;
var titreList = [];
var isUpdateManifeste;

var npSelected;
var ncSelected;
var sousProvisionID;
var manifesteID;
var statuSousProvision;
var clickedDetailExist;

var btnPrintRapportVoirie,
        btnPrintRapportConcentre;

var manifesteVoirieList = [];
var manifesteConcentreList = [];

var tauxSelected;

var dateDebut,
        dateFin;


var codePersonneSelected,
        periodeSelected,
        datedebutSelected,
        datefinSelected;

$(function () {

    clickedDetailExist = false;

    mainNavigationLabel.text('ORDONNANCEMENT');
    secondNavigationLabel.text('Registre des sous-provision des miniers');

    removeActiveMenu();
    linkMenuOrdonnancement.addClass('active');

    tableSousProvision = $('#tableSousProvision');
    btnCallModalSearchAvanded = $('#btnCallModalSearchAvanded');
    modalResearchContribuableMinier = $('#modalResearchContribuableMinier');
    selectSocieteMiniere = $('#selectSocieteMiniere');
    btnAdvencedSearch = $('#btnAdvencedSearch');

    dateDebut = $('#dateDebut');
    dateFin = $('#dateFin');

    dateDebut.val(new Date());
    dateFin.val(new Date());

    tauxSelected = 0;

    modalAddNewOrUpdateManifeste = $('#modalAddNewOrUpdateManifeste');
    inputTransporteur = $('#inputTransporteur');
    inputNatureProduit = $('#inputNatureProduit');
    inputNumeroPlaque = $('#inputNumeroPlaque');
    inputTonnage = $('#inputTonnage');
    inputMontantDu = $('#inputMontantDu');
    btnConfirmationValidation = $('#btnConfirmationValidation');
    btnAddNewManifeste = $('#btnAddNewManifeste');

    btnPrintRapportVoirie = $('#btnPrintRapportVoirie');
    btnPrintRapportConcentre = $('#btnPrintRapportConcentre');
    selectPeriode = $('#selectPeriode');

    btnPrintRapportConcentre.click(function (e) {
        e.preventDefault();
        printRapportSituationEntrepriseTaxeConcentre();
    });

    inputTonnage.on('change', function (e) {

        if (inputTonnage.val() <= 0) {
            alertify.alert('Le tonnage à sortir ne doit pas être inférieur ou égale à zéro');
            return;
        } else {
            calculateMontantDu();
        }

    });

    btnPrintRapportVoirie.click(function (e) {
        e.preventDefault();
        printRapportSituationEntrepriseTaxeVoirie();
    });

    btnCallModalSearchAvanded.click(function (e) {
        e.preventDefault();
        modalResearchContribuableMinier.modal('show');
    });

    btnConfirmationValidation.click(function (e) {
        e.preventDefault();
        saveManifeste();
    });

    btnAdvencedSearch.click(function (e) {

        e.preventDefault();

        if (dateDebut.val() == '' || dateDebut.val() == null) {
            alertify.alert('Veuillez d\'abord indiquer la date du début');
            return;
        }

        if (dateFin.val() == '' || dateFin.val() == null) {
            alertify.alert('Veuillez d\'abord indiquer la date fin');
            return;
        }

        codePersonneSelected = selectSocieteMiniere.val();
        periodeSelected = selectPeriode.val();
        datedebutSelected = dateDebut.val();
        datefinSelected = dateFin.val();

        loadSousProvisionMinier();
    });

    loadMiniers();

    printSousprovision('');
});


function loadMiniers() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getMinier'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            setTimeout(function () {

                var dataMinierList = JSON.parse(JSON.stringify(response));
                var dataMinier = '';

                dataMinier += '<option value="*" style="font-weight: bold;size: 20px;font-style: italic">Tous les  miniers confodues</option>';

                for (var i = 0; i < dataMinierList.length; i++) {
                    dataMinier += '<option value="' + dataMinierList[i].codeMinier + '">' + dataMinierList[i].nameMinier.toUpperCase() + '</option>';
                }

                selectSocieteMiniere.html(dataMinier);
            }
            , 1);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
        }
    });
}

function loadSousProvisionMinier() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codePersonne': selectSocieteMiniere.val(),
            'periode': selectPeriode.val(),
            'datedebut': dateDebut.val(),
            'datefin': dateFin.val(),
            'operation': 'getSousProvisionMinier'
        },
        beforeSend: function () {
            modalResearchContribuableMinier.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            modalResearchContribuableMinier.unblock();

            if (response == '0') {

                printSousprovision(empty);
                alertify.alert('Aucune sous provision trouvée !');
                return;

            } else if (response == '-1') {
                printSousprovision(empty);
                showResponseError();
                return;

            } else {

                setTimeout(function () {

                    dataSousprovision = JSON.parse(JSON.stringify(response));
                    modalResearchContribuableMinier.modal('hide');
                    printSousprovision(dataSousprovision);

                }
                , 1);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            modalResearchContribuableMinier.unblock();
            printSousprovision(empty);
            showResponseError();
        }
    });
}

var sumCredit = 0;
var sumDebit = 0;
var sumSolde = 0;
var suumTonnageInit = 0;
var suumTonnageRestant = 0;

function printSousprovision(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center;width:5%"scope="col">ID</th>';
    tableContent += '<th style="text-align:left;width:13%"scope="col">N° NOTE PERCEPTION</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">CREEE-LE</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">MINIER</th>';
    tableContent += '<th style="text-align:right;width:11%"scope="col">CREDIT</th>';
    tableContent += '<th style="text-align:right;width:11%"scope="col">DEBIT</th>';
    tableContent += '<th style="text-align:right;width:11%"scope="col">SOLDE</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">AUTRES INFOS</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">TONNAGE REST.</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">TAXE</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    sumCredit = 0;
    sumDebit = 0;
    sumSolde = 0;
    suumTonnageInit = 0;
    suumTonnageRestant = 0;

    for (var i = 0; i < result.length; i++) {

        sumCredit += result[i].credit;
        sumDebit += result[i].debit;
        sumSolde += result[i].solde;
        suumTonnageInit += result[i].tonnageInit;
        suumTonnageRestant += result[i].tonnageRestant;

        var btnCallModalManifeste = '<button class="btn btn-success" onclick="callModalManifeste(\'' + result[i].notePerception + '\',\'' + result[i].id + '\',\'' + result[i].taux + '\')"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Ajouter</button>';

        var colorTonneRestant = 'green';

        if (result[i].tonnageRestant <= 0) {
            colorTonneRestant = 'red';
        }

        var colorCredit = 'green';

        if (result[i].credit <= 0) {
            colorCredit = 'red';
        }

        var tonnageInitInfo = 'TONNAGE INITIAL : ' + '<span style="font-weight:bold">' + formatNumberOnly(result[i].tonnageInit) + ' T' + '</span>';
        var montantInitInfo = 'MONTANT INITIAL : ' + '<span style="font-weight:bold">' + formatNumber(result[i].montantInit, result[i].devise) + '</span>';

        var otherInitInfo = tonnageInitInfo + '<br/><br/>' + montantInitInfo;

        tableContent += '<tr>';
        tableContent += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        //tableContent += '<td style="vertical-align:middle;width:5%;text-align:center">' + result[i].id + '</td>';
        tableContent += '<td style="vertical-align:middle;width:13%">' + result[i].notePerception + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%">' + result[i].dateCreate + '</td>';
        //tableContent += '<td style="vertical-align:middle;width:10%">' + result[i].dateDerniereSortie + '</td>';
        tableContent += '<td style="vertical-align:middle;width:15%">' + result[i].nameMinier + '</td>';
        tableContent += '<td style="vertical-align:middle;width:11%;text-align:right;font-weight:bold;color:' + colorCredit + '">' + formatNumber(result[i].credit, result[i].devise) + '</td>';
        tableContent += '<td style="vertical-align:middle;width:11%;text-align:right;font-weight:bold">' + formatNumber(result[i].debit, result[i].devise) + '</td>';
        tableContent += '<td style="vertical-align:middle;width:11%;text-align:right;font-weight:bold;color:' + colorCredit + '">' + formatNumber(result[i].solde, result[i].devise) + '</td>';
        tableContent += '<td style="vertical-align:middle;width:12%;text-align:right;font-weight:bold">' + otherInitInfo + '</td>';
        tableContent += '<td style="vertical-align:middle;width:11%;text-align:right;font-weight:bold;color:' + colorTonneRestant + '">' + formatNumberOnly(result[i].tonnageRestant) + ' T' + '</td>';
        tableContent += '<td style="vertical-align:middle;width:15%">' + result[i].taxeName + '</td>';
        tableContent += '<td style="vertical-align:middle;width:5%">' + btnCallModalManifeste + '</td>';
        tableContent += '</tr>';

    }
    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="3" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableSousProvision.html(tableContent);

    var datatable = tableSousProvision.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste ici _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: true,
        pageLength: 25,
        columnDefs: [
            {"visible": false, "targets": 3}
        ],
        order: [[3, 'asc']],
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(4).footer()).html(
                    formatNumber(sumCredit, 'USD'));

            $(api.column(5).footer()).html(
                    formatNumber(sumDebit, 'USD'));

            $(api.column(6).footer()).html(
                    formatNumber(sumSolde, 'USD'));

            $(api.column(7).footer()).html(
                    formatNumberOnly(suumTonnageInit) + ' T');

            $(api.column(8).footer()).html(
                    formatNumberOnly(suumTonnageRestant) + ' T');
        },
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(3, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="12">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableSousProvision tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');

        var row = datatable.row(tr);
        var dataDetail = datatable.row(tr).data();
        var numeroTitre = dataDetail[1];

        var tableTitreDependant = '';

        console.log(row.child.isShown());

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');

        } else {

            var btnPrintManifeste = '<button class="btn btn-warning" onclick="printManifeste(\'' + numeroTitre + '\')"><i class="fa fa-print"></i>&nbsp;&nbsp;Imprimer le manifeste</button>';

            tableTitreDependant = '<center><h4>LES MANIFESTES ASSOCIES A LA NOTE DE PERCEPTION N° : ' + numeroTitre + '</h4><br/>' + btnPrintManifeste + '</center><br/><table class="table table-bordered">';
            tableTitreDependant += '<thead><tr style="background-color:#e6ceac;color:black"><td>TRANSITAIRE</td><td>NATURE PRODUIT</td><td>N° PLAQUE CAMION</td><td style="text-align:right">TONNAGE SORTIE</td><td style="text-align:right">MONTANT DÛ</td><td style="text-center:center">STATUS</td><td style="text-center:center">AUTRES INFORMATIONS</td></tr></thead>';

            for (var j = 0; j < dataSousprovision.length; j++) {

                if (dataSousprovision[j].notePerception == numeroTitre) {

                    titreList = JSON.parse(dataSousprovision[j].complementList);

                    clickedDetailExist = true;

                    for (var i = 0; i < titreList.length; i++) {

                        var agentInfo = 'AGENT VALIDATION : ' + '<span style="font-weight:bold">' + titreList[i].agentValidate + '</span>';
                        var dateInfo = 'DATE VALIDATION : ' + '<span style="font-weight:bold">' + titreList[i].dateValidation + '</span>';

                        var otherInfo = agentInfo + '<br/><br/>' + dateInfo;

                        var colorStatuManifeste = 'green';

                        if (titreList[i].status == 'NEGATIF') {
                            colorStatuManifeste = 'red';
                        }

                        tableTitreDependant += '<tr>';
                        tableTitreDependant += '<td style="text-align:left;width:20%;vertical-align:middle">' + titreList[i].transporteur + '</td>';
                        tableTitreDependant += '<td style="text-align:left;width:20%;vertical-align:middle">' + titreList[i].produit + '</td>';
                        tableTitreDependant += '<td style="text-align:left;width:12%;vertical-align:middle">' + titreList[i].plaque + '</td>';
                        tableTitreDependant += '<td style="text-align:right;width:13%;vertical-align:middle;font-weight:bold;color:green">' + formatNumberOnly(titreList[i].tonnageSortie) + ' T' + '</td>';
                        tableTitreDependant += '<td style="text-align:right;width:12%;vertical-align:middle;font-weight:bold;color:green">' + formatNumber(titreList[i].montantTonnageSortie, titreList[i].devise) + '</td>';
                        tableTitreDependant += '<td style="text-center:center:width:15%;vertical-align:middle;font-weight:bold;color:' + colorStatuManifeste + '">' + titreList[i].status + '</td>';
                        tableTitreDependant += '<td style="text-center:center:width:20%;vertical-align:middle">' + otherInfo + '</td>';
                        tableTitreDependant += '</tr>';
                    }

                }
            }

            tableTitreDependant += '<tbody>';

            tableTitreDependant += '</tbody>';
            row.child(tableTitreDependant).show();
            tr.addClass('shown');

        }

    });
}

var isUpdateManifeste;

function callModalManifeste(np, id, taux) {

    tauxSelected = taux;

    if (clickedDetailExist == false) {

        for (var k = 0; k < dataSousprovision.length; k++) {

            if (dataSousprovision[k].id == id) {
                statuSousProvision = dataSousprovision[k].status;
                titreList = JSON.parse(dataSousprovision[k].complementList);
            }
        }

    } else {

        for (var k = 0; k < dataSousprovision.length; k++) {

            if (dataSousprovision[k].id == id) {
                statuSousProvision = dataSousprovision[k].status;
                titreList = JSON.parse(dataSousprovision[k].complementList);
            }
        }

    }

    for (var i = 0; i < titreList.length; i++) {

        if (titreList[i].numeroNP == np) {

            npSelected = titreList[i].numeroNP;
            sousProvisionID = titreList[i].sousProvisionID;
            manifesteID = titreList[i].manifestId;


            if (titreList.length == 1 && titreList[i].tonnageSortie == 0) {

                isUpdateManifeste = true;

                inputTransporteur.val(titreList[i].transporteur);
                inputNatureProduit.val(titreList[i].produit);
                inputNumeroPlaque.val(titreList[i].plaque);
                inputTonnage.val(empty);
                inputMontantDu.val(empty);

            } else {

                isUpdateManifeste = false;

                inputTransporteur.val(titreList[i].transporteur);
                inputNatureProduit.val(titreList[i].produit);
                inputNumeroPlaque.val(empty);
                inputTonnage.val(empty);
                inputMontantDu.val(empty);
            }



            modalAddNewOrUpdateManifeste.modal('show');

        }

    }
}

function saveManifeste() {

    if (inputTransporteur.val() == '') {
        alertify.alert('Veuillez d\'abord fournir le nom du transitaire');
        return;
    }

    if (inputNatureProduit.val() == '') {
        alertify.alert('Veuillez d\'abord fournir la nature du produit');
        return;
    }

    if (inputNumeroPlaque.val() == '') {
        alertify.alert('Veuillez d\'abord fournir le numéro de plaque');
        return;
    }

    if (inputTonnage.val() == '') {
        alertify.alert('Veuillez d\'abord fournir le tonnage à sortir');
        return;
    }

    var tonne = parseFloat(inputTonnage.val());

    if (tonne <= 0) {
        alertify.alert('La valeur du tonnage à sortir doit être supérieure à zéro');
        return;
    }

    var amount = parseFloat(inputMontantDu.val());

    if (inputMontantDu.val() == '') {
        alertify.alert('Veuillez d\'abord fournir le montant lié au tonnage');
        return;
    }

    if (amount <= 0) {
        alertify.alert('Le montant du tonnage à sortir doit être supérieure à zéro');
        return;
    }

    alertify.confirm('Etes-vous sûr de vouloir valider ce manifeste ?', function () {

        $.ajax({
            type: 'POST',
            url: 'registrenoteperception',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'inputTransporteur': inputTransporteur.val(),
                'inputNatureProduit': inputNatureProduit.val(),
                'inputNumeroPlaque': inputNumeroPlaque.val(),
                'inputTonnage': inputTonnage.val(),
                'inputMontantDu': inputMontantDu.val(),
                'isUpdateManifeste': isUpdateManifeste == true ? "1" : "0",
                'userId': userData.idUser,
                'manifesteID': manifesteID,
                'sousProvisionID': sousProvisionID,
                'statuSousProvision': statuSousProvision,
                'operation': 'saveManifeste'
            },
            beforeSend: function () {
                modalAddNewOrUpdateManifeste.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Validation en cours ...</h5>'});
            },
            success: function (response)
            {

                modalAddNewOrUpdateManifeste.unblock();

                if (response == '0' || response == '-1') {
                    showResponseError();
                    return;

                } else {
                    setTimeout(function () {

                        alertify.alert('La validation du manifeste s\'est effectuée avec succès');
                        modalAddNewOrUpdateManifeste.modal('hide');
                        loadSousProvisionMinier();

                    }
                    , 3000);
                }


            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalAddNewOrUpdateManifeste.unblock();
                showResponseError();
            }
        });

    });


}

function printManifeste(code) {
    alert(code);
}

function printRapportSituationEntrepriseTaxeVoirie() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codePersonne': codePersonneSelected,
            'periode': periodeSelected,
            'datedebut': datedebutSelected,
            'datefin': datefinSelected,
            'operation': 'printRapportSituationEntrepriseTaxeVoirie'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '0') {

                alertify.alert('Désolé, il n\'y a pas des données disponibles pour l\'instant');
                return;

            } else if (response == '-1') {
                showResponseError();
                return;

            } else {

                setTimeout(function () {

                    var result = JSON.parse(JSON.stringify(response));

                    for (var i = 0; i < result.length; i++) {

                        var data = new Object();

                        data.nameMinier = result[i].nameMinier;
                        data.transiteur = result[i].transiteur;
                        data.taxe = result[i].taxe;
                        data.tonnage = formatNumberOnly(result[i].tonnage);
                        data.observation = result[i].observation;
                        data.taux = formatNumberOnly(result[i].taux);
                        data.montantDu = formatNumberOnly(result[i].montantDu * result[i].taux);

                        manifesteVoirieList.push(data);
                    }

                    printDataRapportMineVoirie();

                }
                , 1);
            }



        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printRapportSituationEntrepriseTaxeConcentre() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codePersonne': codePersonneSelected,
            'periode': periodeSelected,
            'datedebut': datedebutSelected,
            'datefin': datefinSelected,
            'operation': 'printRapportSituationEntrepriseTaxeConcentre'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '0') {

                alertify.alert('Désolé, il n\'y a pas des données disponibles pour l\'instant');
                return;

            } else if (response == '-1') {

                showResponseError();
                return;

            } else {

                setTimeout(function () {

                    var result = JSON.parse(JSON.stringify(response));

                    for (var i = 0; i < result.length; i++) {

                        var data = new Object();

                        data.nameMinier = result[i].nameMinier;
                        data.transiteur = result[i].transiteur;
                        data.taxe = result[i].taxe;
                        data.tonnage = formatNumberOnly(result[i].tonnage);
                        data.observation = result[i].observation;
                        data.taux = formatNumberOnly(result[i].taux);
                        data.montantDu = formatNumberOnly(result[i].montantDu * result[i].taux);

                        manifesteConcentreList.push(data);
                    }

                    printDataRapportMineConcentres();

                }
                , 1);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printDataRapportMineConcentres() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    //var toDayFormat = day < 9 ? '0' + day : day + '/' + month < 9 ? '0' + month : month + '/' + year;
    var toDayFormat = day + '/' + month + '/' + year;

    var columns = [], columnStyles;
    var carEspace = '';

    if (manifesteConcentreList.length > 0) {
        position = 270;
        hauteur = 160;

    } else {
        position = 160;
    }


    carEspace = '';
    docTitle = carEspace + "RAPPORT - TAXE CONCENTRE : SITUATION JOURNALIERE DES ENTREPRISES DRHKAT DU  : " + toDayFormat;
    columns = [
        {title: "NOM DE L'ENTREPRISE", dataKey: "nameMinier"},
        {title: "TRANSITAIRE", dataKey: "transiteur"},
        {title: "TONNAGE", dataKey: "tonnage"},
        {title: "OBSERVATION", dataKey: "observation"},
        {title: "TAUX EN USD", dataKey: "taux"},
        {title: "MONTANT EN USD", dataKey: "montantDu"},
    ];

    columnStyles = {
        nameMinier: {columnWidth: 250, fontSize: 8, overflow: 'linebreak'},
        transiteur: {columnWidth: 150, overflow: 'linebreak', fontSize: 8},
        tonnage: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
        //observation : {columnWidth: 100, overflow: 'linebreak', fontSize: 8, textColor: '#BEFF33'},
        observation: {columnWidth: 100, overflow: 'linebreak', fontSize: 8},
        taux: {columnWidth: 100, overflow: 'linebreak', fontSize: 8},
        montantDu: {columnWidth: 100, overflow: 'linebreak', fontSize: 8},
    };

    var rows = manifesteConcentreList;
    var pageContent = function (data) {

        var bureau = 'BUREAU TAXES EXCEPTIONNELLES';
        setDocParams(doc, docTitle, position, day, month, year, hh, mm, ss, data, bureau);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: columnStyles,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 8 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');

    doc.text("Fait à Lubumbashi, le " + toDayFormat, 540, finalY + 25);
    //doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD_MineConcentres), 540, finalY + 40);
    //doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD_MineConcentres), 540, finalY + 40);

    var codeBureauSelected = 'ST00000022';
    //var codeABSelected = '';

    doc.text(getNameChefBureau(codeBureauSelected), 540, finalY + 45);
    //doc.text(getNameChefTaxateur(codeBureauSelected, codeABSelected), 40, finalY + 65);

    window.open(doc.output('bloburl'), '_blank');
}

function printDataRapportMineVoirie() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    //var toDayFormat = day < 9 ? '0' + day : day + '/' + month < 9 ? '0' + month : month + '/' + year;
    var toDayFormat = day + '/' + month + '/' + year;

    var columns = [], columnStyles;
    var carEspace = '';

    if (manifesteVoirieList.length > 0) {
        position = 270;
        hauteur = 160;

    } else {
        position = 160;
    }


    carEspace = '';
    docTitle = carEspace + "RAPPORT - TAXE VOIRIE : SITUATION JOURNALIERE DES ENTREPRISES DRHKAT DU  : " + toDayFormat;
    columns = [
        {title: "NOM DE L'ENTREPRISE", dataKey: "nameMinier"},
        {title: "TRANSITAIRE", dataKey: "transiteur"},
        {title: "TONNAGE", dataKey: "tonnage"},
        {title: "OBSERVATION", dataKey: "observation"},
        {title: "TAUX EN USD", dataKey: "taux"},
        {title: "MONTANT EN USD", dataKey: "montantDu"},
    ];

    columnStyles = {
        nameMinier: {columnWidth: 250, fontSize: 8, overflow: 'linebreak'},
        transiteur: {columnWidth: 150, overflow: 'linebreak', fontSize: 8},
        tonnage: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
        //observation : {columnWidth: 100, overflow: 'linebreak', fontSize: 8, textColor: '#BEFF33'},
        observation: {columnWidth: 100, overflow: 'linebreak', fontSize: 8},
        taux: {columnWidth: 100, overflow: 'linebreak', fontSize: 8},
        montantDu: {columnWidth: 100, overflow: 'linebreak', fontSize: 8},
    };

    var rows = manifesteVoirieList;
    var pageContent = function (data) {

        var bureau = 'BUREAU TAXES EXCEPTIONNELLES';
        setDocParams(doc, docTitle, position, day, month, year, hh, mm, ss, data, bureau);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: columnStyles,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 8 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');

    doc.text("Fait à Lubumbashi, le " + toDayFormat, 540, finalY + 25);
    //doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD_MineConcentres), 540, finalY + 40);
    //doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD_MineConcentres), 540, finalY + 40);

    var codeBureauSelected = 'ST00000022';
    //var codeABSelected = '';

    doc.text(getNameChefBureau(codeBureauSelected), 540, finalY + 45);
    //doc.text(getNameChefTaxateur(codeBureauSelected, codeABSelected), 40, finalY + 65);

    window.open(doc.output('bloburl'), '_blank');
}

function calculateMontantDu() {
    inputMontantDu.val(parseFloat(inputTonnage.val() * tauxSelected));
}