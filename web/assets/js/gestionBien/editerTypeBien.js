/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnEnregistrerTypeBien, btnModifierTypeBien;

var checkBoxEstContratuel;

var valEstContractuel, inputValueIntitule;

var tableResultTypeBien;

var selectTypeBientData;

var typeBienList;

var codeTypeBienSelected;

var btnNettoyerTypeBien;

$(function () {
    
    codeTypeBienSelected = empty;
    
    tableResultTypeBien = $('#tableResultTypeBien');
    btnEnregistrerTypeBien = $('#btnEnregistrerTypeBien');
    btnModifierTypeBien = $('#btnModifierTypeBien');
    inputValueIntitule = $('#inputValueIntitule');
    btnNettoyerTypeBien = $('#btnNettoyerTypeBien');

    btnEnregistrerTypeBien.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputValueIntitule.val() === empty) {
            alertify.alert('Veuillez d\'abord fournir l\'intutilé de type bien');
            return;
        }

        alertify.confirm('Etes-vous de vouloir enregistrer ce type bien ?', function () {

            saveTypeBien();

        });
    });

    btnModifierTypeBien.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputValueIntitule.val() === empty) {
            alertify.alert('Veuillez d\'abord fournir l\'intutilé de type bien');
            return;
        }

        alertify.confirm('Etes-vous de vouloir modifier ce type bien ?', function () {
            updateTypeBien();
        });
    });
    
    
    btnNettoyerTypeBien.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous de vouloir nettoyer le champ ?', function () {
            inputValueIntitule.val(empty);
            codeTypeBienSelected = empty;
        });
    });

    loadTypeBiens();

});

function loadTypeBiens() {


    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTypeBienByService',
            'codeService': userData.serviceCode,
            'type': ID_TYPE
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                typeBienList = JSON.parse(JSON.stringify(response));
                printTypeBien(typeBienList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}


function printTypeBien(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">LIBELLE</th>';
    tableContent += '<th style="display:none" scope="col">Code type bien</th>';
    tableContent += '<th scope="col"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        var btnDeleteTypeBien = '<button type="button" onclick="removeTypeBien(\'' + data[i].codeTypeBien + '\',\'' + data[i].libelleTypeBien + '\')" class="btn btn-danger"><i class="fa fa-trash"></i></button>';
        var btnEditTypeBien = '<button type="button" onclick="editTypeBien(\'' + data[i].codeTypeBien + '\',\'' + data[i].libelleTypeBien + '\')" class="btn btn-success"><i class="fa fa-edit"></i></button>';
        var btnAction = btnDeleteTypeBien + ' ' + btnEditTypeBien;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left">' + data[i].libelleTypeBien + '</td>';
        tableContent += '<td style="display:none">' + data[i].codeTypeBien + '</td>';
        tableContent += '<td style="width:15%">' + btnAction + '</td>';

        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultTypeBien.html(tableContent);

    var myDt = tableResultTypeBien.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par Intitulé droit _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });


}



function removeTypeBien(codeTypeBien, intitule) {
    alertify.confirm('Voulez-vous désactiver ' + intitule + ' ?', function () {
        validateDisableTypeBien(codeTypeBien);
    });
}

function saveTypeBien() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveTypeBien',
            'code': codeTypeBienSelected,
            'libelleTypeBien': inputValueIntitule.val(),
            'estContractuel': valEstContractuel,
            'codeService': userData.serviceCode
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de type bien en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\enregistrement de type bien s\'est effectué avec succès.');
                    loadTypeBien();
                    loadTypeBiens();
               
                    btnEnregistrerTypeBien.attr('style', 'display:block');
                             
                    codeTypeBienSelected = empty;
                    inputValueIntitule.val(empty);
                    
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors d\'enregistrement de type bien.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function updateTypeBien() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'updateTypeBien',
            'libelleTypeBien': inputValueIntitule.val(),
            'estContractuel': valEstContractuel,
            'codeTypeBien': selectTypeBientData.codeTypeBien
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Mise à jour du type bien en cours...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('La modification de type bien s\'est effectué avec succès.');
                    loadTypeBien();
                    loadTypeBiens();
//                    BtnEditerTypeBien.trigger('click');                   
                    inputValueIntitule.val('');
                    btnEnregistrerTypeBien.attr('style', 'display:block');
                    btnModifierTypeBien.attr('style', 'display:none');

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de la modification de type bien.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}
function validateDisableTypeBien(codeTypeBien) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'disableTypeBien',
            'codeTypeBien': codeTypeBien
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Désactivation de type bien ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('La désactivation de type bien s\'est effectué avec succès.');
                    loadTypeBien();
                    loadTypeBiens();
                    inputValueIntitule.val('');
                    btnEnregistrerTypeBien.attr('style', 'display:block');
                    btnModifierTypeBien.attr('style', 'display:none');

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de la désactivation de type bien.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function displayNatureBienInfo() {

    if (cmbTypeBien.val() !== '0') {
        inputValueIntitule.val($('#cmbTypeBien option:selected').text());
        codeTypeBienSelected = cmbTypeBien.val();
    } else {
        inputValueIntitule.val('0');
        codeTypeBienSelected = empty;
    }

}

function editTypeBien(code, libelle) {

    alertify.confirm('Etes-vous de vouloir sélectionner ce type bien : ' + libelle + ' ?', function () {

        inputValueIntitule.val(libelle);
        codeTypeBienSelected = code;

    });
}