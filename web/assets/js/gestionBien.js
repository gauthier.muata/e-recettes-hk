/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var
        codeResponsible = '',
        codeTypeAssjetti = '',
        nameResponsible = '',
        selectedBien = '',
        yes4Assujetti = true; //Pour afficher les biens non loués

var
        tabBiens,
        tabArticles;

var isTypeMobilier;

var
        tableBiens,
        tableArticles,
        tablePeriodeDeclaration;

var
        lblLegalForm,
        lblNameResponsible,
        lblAddress;

var
        btnCallModalSearchResponsable,
        btnNewBien,
        btnPasserAB, btnPasserAB2, btnPasserAB21,
        btnLocateBien, btnNewBienImmobilier, btnNewBienConcession, btnNewBienPanneauPublicitaire,
        btnPermuteBien;

var
        tempBienList = [],
        tempArticleList = [];

var bienList = [];

var modalPeriodeDeclaration;

var lbl1, lbl2, lbl3;

var typePage;

var modalDetailDeclaration, modalPermutationBien;

var lblNameAssujettiModal, lblAdress,
        lblNoteCalcul, lblAgentTaxateur, lblAgentValidateur, lblAgentOrdonnateur, btnVoirDeclaration;

var archiveContent = '';

var redressementObj = null;

var modalMiseEnQuarantaine, textaeraObservation, btnMiseQuarantaine;

var periodeDeclarationId, periodeDeclarationName;
var codeAssujettiSave;

var modalInfoMiseEnQuarantaine, valuePeriode2, valueDate2, valueAgent2, valueObservation;
var valuePeriode1, valueAgent1, dateAcquisitionBien;

var btnIRL;
var btnIF;
var btnRL;
var btnVIGNETTE;
var btnICM;
var btnPUBLICITE;

var btnRL22M, btnRL22A;

var namePropriete, adressePropriete;

var checkOperation = false;

var btnConfirmeRedressement, inputObservationRedressement, newBase, spnBaseActuelle, modalRedressement, cmbUnite;
var assujettissementIDSlected;

var modalAddPeriodeDeclarationAssujettissement, spnPDDepart, spnBienAssujSelected, tableNewPDAssuj;
var tempPeriodeDeclarationList = [];
var btnAjouterPD;
var beginPeriodValue;
var communeCodeBienSelected;
var codeNatureBienSelected;
var clickExist;

var lblBienName, lblAdresseBien;

$(function () {

    clickExist = false;

    tabBiens = $('#tabBiens');
    tabArticles = $('#tabArticles');
    assujettiModal = $('#assujettiModal');
    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    tableBiens = $('#tableBiens');
    tableArticles = $('#tableArticles');
    tablePeriodeDeclaration = $('#tablePeriodeDeclaration');
    modalPeriodeDeclaration = $('#modalPeriodeDeclaration');

    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');
    lbl3 = $('#lbl3');

    modalInfoMiseEnQuarantaine = $('#modalInfoMiseEnQuarantaine');
    modalPermutationBien = $('#modalPermutationBien');
    valuePeriode2 = $('#valuePeriode2');

    valueDate2 = $('#valueDate2');
    valueAgent2 = $('#valueAgent2');
    valueObservation = $('#valueObservation');
    valuePeriode1 = $('#valuePeriode1');
    valueAgent1 = $('#valueAgent1');

    modalMiseEnQuarantaine = $('#modalMiseEnQuarantaine');
    textaeraObservation = $('#textaeraObservation');
    btnMiseQuarantaine = $('#btnMiseQuarantaine');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');

    btnConfirmeRedressement = $('#btnConfirmeRedressement');
    inputObservationRedressement = $('#inputObservationRedressement');
    newBase = $('#newBase');
    spnBaseActuelle = $('#spnBaseActuelle');
    modalRedressement = $('#modalRedressement');
    cmbUnite = $('#cmbUnite');

    inputObservationRedressement.val(empty)

    btnIRL = $("#btnIRL");
    btnIF = $("#btnIF");
    btnRL = $("#btnRL");
    btnVIGNETTE = $("#btnVIGNETTE");
    btnICM = $("#btnICM");
    btnPUBLICITE = $("#btnPUBLICITE");

    btnRL22M = $("#btnRL22M");
    btnRL22A = $("#btnRL22A");

    modalAddPeriodeDeclarationAssujettissement = $("#modalAddPeriodeDeclarationAssujettissement");
    spnPDDepart = $("#spnPDDepart");
    spnBienAssujSelected = $("#spnBienAssujSelected");
    tableNewPDAssuj = $("#tableNewPDAssuj");
    btnAjouterPD = $("#btnAjouterPD");

    lblBienName = $("#lblBienName");
    lblAdresseBien = $("#lblAdresseBien");


    var urlCode = getUrlParameter('id');
    if (jQuery.type(urlCode) !== 'undefined') {
        codeResponsible = atob(urlCode);
        refreshDataAssujetti();
        loadBiens(codeResponsible, '0');
    }

    tabBiens.tab('show');

    btnCallModalSearchResponsable.click(function (e) {
        e.preventDefault();
        yes4Assujetti = true;
        allSearchAssujetti = '1';
        resetSearchAssujetti();
        assujettiModal.modal('show');
    });

    btnLocateBien = $('#btnLocateBien');
    btnPasserAB = $('#btnPasserAB');
    btnPasserAB2 = $('#btnPasserAB2');
    btnPasserAB21 = $('#btnPasserAB21');
    btnPermuteBien = $('#btnPermuteBien');

    /*if (controlAccess('ASSUJETIR_BIEN')) {
     btnLocateBien.attr('style', 'display:block');
     }
     
     if (controlAccess('ASSUJETIR_BIEN')) {
     btnPasserAB.attr('style', 'display:block');
     //btnPasserAB2.attr('style', 'display:block;width:100%');
     //btnPasserAB21.attr('style', 'display:block;width:100%');
     }*/

    btnAjouterPD.attr('style', 'display:none');

    btnLocateBien.click(function (e) {
        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un contribuable');
            return;
        }

        codeLocataireBien = codeResponsible;

        yes4Assujetti = false;
        allSearchAssujetti = '1';

        resetSearchAssujetti();

        var messageLocation = 'Location d\'un bien :<br/>'
                + '<br/>- Rechercher et sélectionner le propiétaire'
                + '<br/>- Sélectionner le bien à louer'
                + '<br/>- Renseigner les informations du bien.'
                + '<br/><br/>Voulez-vous continuer ?';

        alertify.confirm(messageLocation, function () {
            assujettiModal.modal('show');
        });

    });

    btnConfirmeRedressement.click(function (e) {

        e.preventDefault();


        if (newBase.val() == '') {

            alertify.alert('Veuillez d\'abord saisir la nouvelle valeur de base de calcul');
            return;
        }

        if (cmbUnite.val() == '0') {

            alertify.alert('Veuillez d\'abord saisir l\'unité de la nouvelle valeur de base de calcul');
            return;
        }

        saveRedressement();
    });

    btnPermuteBien.click(function (e) {
        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un contribuable');
            return;
        }

        codeLocataireBien = codeResponsible;
        codePermuteBien = codeResponsible;

        checkOperation = true;

        yes4Assujetti = false;
        allSearchAssujetti = '1';

        resetSearchAssujetti();

        var messageLocation = 'Location d\'un bien :<br/>'
                + '<br/>- Rechercher et sélectionner le propiétaire'
                + '<br/>- Sélectionner le bien à permuter'
                + '<br/>- Renseigner les informations du bien.'
                + '<br/><br/>Voulez-vous continuer ?';

        alertify.confirm(messageLocation, function () {
            assujettiModal.modal('show');
        });

    });

    btnNewBien = $('#btnNewBien');
    btnNewBienImmobilier = $('#btnNewBienImmobilier');
    btnNewBienConcession = $('#btnNewBienConcession');
    btnNewBienPanneauPublicitaire = $('#btnNewBienPanneauPublicitaire');

    btnNewBien.click(function (e) {
        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un contribuable.');
            return;
        }

        window.location = 'edition-bien-automobile?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible);

    });

    btnAjouterPD.click(function (e) {

        e.preventDefault();

        alertify.confirm('Etes-vous sûre de vouloir ajouter cette période de déclaration dans l\'assujettissement en cours ?', function () {
            saveAddNewPriodeDeclaration();
        });

    });

    btnNewBienPanneauPublicitaire.click(function (e) {
        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un contribuable.');
            return;
        }

        window.location = 'edition-bien-panneau-publicitaire?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible);

    });

    btnNewBienImmobilier.click(function (e) {
        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un contribuable.');
            return;
        }

        window.location = 'edition-bien-immobilier?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible);

    });

    btnNewBienConcession.click(function (e) {
        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un contribuable.');
            return;
        }

        window.location = 'edition-bien-concession-mine?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible);

    });

    btnMiseQuarantaine.click(function (e) {

        e.preventDefault();

        if (textaeraObservation.val() == empty) {

            alertify.alert('Veuillez d\'abord motiver la mise en quarantaine de cette période');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir mettre en quarantaine cette période de déclaration : ' + periodeDeclarationName + ' ?', function () {
            miseEnQuarantaine();
        });

    });

    //vérifier si le bien est de type immobilier
    isTypeMobilier = false;

    btnPasserAB.click(function (e) {

        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un contribuable.');
            return;
        }

        if (clickExist == false) {
            alertify.alert('Veuillez d\'abord cliquer sur un bien avant de passer à son assujettissement');
            return;
        }

        if (isTypeMobilier) {
            $('#modalChooseAB').modal('show');
            return;
        }

        setBienData(JSON.stringify(tempBienList));

        window.location = 'assujettissement?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&type=' + btoa(codeTypeAssjetti) + '&bien=' + btoa(selectedBien);
        selectedBien = '';

    });

    btnPasserAB2.click(function (e) {

        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        setBienData(JSON.stringify(tempBienList));

        window.location = 'assujettissement?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&type=' + btoa(codeTypeAssjetti) + '&bien=' + btoa(selectedBien);
        selectedBien = '';

    });

    btnPasserAB21.click(function (e) {

        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        if (!controlAccess('2004')) {
            alertify.alert('Vous n\'avez pas le droit de faire un assujettissement.');
            return;
        }

        setBienData(JSON.stringify(tempBienList));

        window.location = 'assujettissement-multiple?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&type=' + btoa(codeTypeAssjetti) + '&bien=' + btoa(selectedBien);
        selectedBien = '';

    });

    printBien('');
    printArticles('');

    var paramGuideValue = JSON.parse(getTourParam());

    if (paramGuideValue === 1) {

        Tour.run([
            {
                element: $('#btnCallModalSearchResponsable'),
                content: '<p style="font-size:17px;color:black">Commencer par rechercher un assujetti.</p>',
                language: 'fr',
                position: 'right',
                close: 'false'}
        ]);
    } else {
        Tour.run();
    }

    modalDetailDeclaration = $('#modalDetailDeclaration');
    btnVoirDeclaration = $('#btnVoirDeclaration');

    btnVoirDeclaration.click(function (e) {
        e.preventDefault();
        initPrintData(archiveContent);
    });

    btnIRL.on('click', function () {
        goToAssujettisement(AB_IRL, 'Impôt sur les revenus locatifs');
    });

    btnIF.on('click', function () {

        if (communeCodeBienSelected == '') {

            switch (codeNatureBienSelected) {

                //case 'TB00000028': //TERRAIN
                case 'TB00000026': //AP
                case 'TB00000027': //AT
                case 'TB00000025': //VILLA
                case 'TB00000029': //ETAGE

                    alertify.alert('Veuillez d\'abord renseigner la commune, le quartier et le rang du bien immobilier sélectionner');
                    return;

                    break;

                default:

                    goToAssujettisement(AB_IF, 'Impôts fonciers (IF)');

                    break;
            }
        } else {
            goToAssujettisement(AB_IF, 'Impôts fonciers (IF)');
        }

    });

    btnRL.on('click', function () {
        goToAssujettisement(AB_RL, 'La retenue locative (RL)');
    });

    btnRL22M.on('click', function () {
        goToAssujettisement(AB_RL22M, 'La retenue locative (RL - 22% MOIS)');
    });

    btnRL22A.on('click', function () {
        goToAssujettisement(AB_RL22A, 'La retenue locative (RL - 22% L\'AN)');
    });

    btnICM.on('click', function () {
        goToAssujettisement(AB_ICM, 'Impôts sur l\'ICM');
    });

    btnVIGNETTE.on('click', function () {
        goToAssujettisement(AB_VIGNETTE, 'Impôts sur la vignette Automobile');
    });

    btnPUBLICITE.on('click', function () {
        goToAssujettisement(AB_PUBLICITE, 'Taxes sur autorisation de dépôts des affiches et des panneaux publicitaires dans les lieux publics');
    });

    switch (userData.isSitePeage) {
        case true:

            btnNewBienImmobilier.attr('style', 'display:none');
            btnNewBienConcession.attr('style', 'display:none');
            btnNewBienPanneauPublicitaire.attr('style', 'display:none');
            btnPasserAB.attr('style', 'display:none');
            tabArticles.attr('style', 'display:none');

            break;
        case false:
            btnNewBienImmobilier.attr('style', 'display:inline');
            btnNewBienConcession.attr('style', 'display:inline');
            btnNewBienPanneauPublicitaire.attr('style', 'display:inline');
            btnPasserAB.attr('style', 'display:inline');
            tabArticles.attr('style', 'display:inline');
            break;

    }

});

function goToAssujettisement(ab, libelle) {

    setBienData(JSON.stringify(tempBienList));
    window.location = 'assujettissement?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&type=' + btoa(codeTypeAssjetti) + '&bien=' + btoa(selectedBien) + '&ab=' + btoa(ab) + '&libAb=' + btoa(libelle);
    selectedBien = '';
    $('#modalChooseAB').modal('hide');
}

function getSelectedAssujetiData() {

    if (yes4Assujetti == true) {

        codeResponsible = selectAssujettiData.code;
        codeTypeAssjetti = selectAssujettiData.codeForme;
        nameResponsible = selectAssujettiData.nomComplet;

        codeLocataireBien = codeResponsible;
        codePermuteBien = codeResponsible;
        codeAssujettiSave = codeResponsible;

        lblNameResponsible.html(nameResponsible);
        lblLegalForm.html(selectAssujettiData.categorie);
        lblAddress.html(selectAssujettiData.adresse);

        lbl1.show();
        lbl2.show();
        lbl3.show();

        setAssujettiData(JSON.stringify(selectAssujettiData));
        loadBiens(codeResponsible, '0');

    } else {

        if (codeResponsible == selectAssujettiData.code) {
            alertify.alert('Vous ne pouvez pas louer vos propres biens.');
            return;
        }

        codeProprietaireBien = selectAssujettiData.code;
        lblNameProprietaire.html(selectAssujettiData.nomComplet + (' (Propriétaire)'));
        lblAddressProprietaire.html(selectAssujettiData.adresse);

        lblNameProprietairePerm.html(selectAssujettiData.nomComplet + (' (Propriétaire)'));
        lblAddressProprietairePerm.html(selectAssujettiData.adresse);

        loadBiens(codeProprietaireBien, '1');
    }

}

function refreshDataAssujetti() {

    var jsonAssujetti = getAssujettiData();
    var assujettiObj = JSON.parse(jsonAssujetti);

    codeResponsible = assujettiObj.code;
    nameResponsible = assujettiObj.nomComplet;

    lblNameResponsible.html(nameResponsible);
    lblLegalForm.html(assujettiObj.categorie);
    lblAddress.html(assujettiObj.adresse);

    lbl1.show();
    lbl2.show();
    lbl3.show();
}
function loadBiens(personneCode, forLocation) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBiensPersonne',
            'codePersonne': personneCode,
            'forLocation': forLocation,
            'codeService': userData.serviceCode,
            'cmbTarif': '*'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response) {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();
                var assujettissementList = $.parseJSON(JSON.stringify(response));
                bienList = $.parseJSON(JSON.stringify(assujettissementList.listBiens));
                var articleList = $.parseJSON(JSON.stringify(assujettissementList.listArticleBudgetaires));

                if (yes4Assujetti == true) {
                    printBien(bienList);
                    printArticles(articleList);
                } else {

                    var dataBiens = printOtherBien(bienList);

                    if (checkOperation) {
                        if (bienList.length === 0) {
                            alertify.alert('L\assujetti ' + selectAssujettiData.nomComplet + ' n\'a pas des biens à permuter.');
                            return;
                        }
                        cmbPermutationBien.html(dataBiens);
                        modalPermutationBien.modal('show');

                    } else {

                        if (bienList.length === 0) {
                            alertify.alert('L\assujetti ' + selectAssujettiData.nomComplet + ' n\'a pas des biens à mettre en location.');
                            return;
                        }
                        cmbLocationBien.html(dataBiens);
                        modalLocationBien.modal('show');
                    }
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printBien(bienList) {

    tempBienList = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" style="width:20%"> BIEN </th>';
    header += '<th scope="col" style="width:30%"> DESCRIPTION </th>';
    header += '<th scope="col" style="width:30%"> ADRESSE </th>';
    header += '<th scope="col" style="width:10%"> PROPRIETAIRE </th>';
    header += '<th ></th>';
    header += '<th hidden="true" scope="col">Id bien</th>';
    header += '<th hidden="true" scope="col">Type</th>';
    header += '<th hidden="true" scope="col">Commune</th>';
    header += '<th hidden="true" scope="col">Nature</th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < bienList.length; i++) {

        //alert(JSON.stringify(bienList));

        var proprietaire = bienList[i].proprietaire;

        var descriptionBien = empty;
        var dateContratInfo = '';

        if (bienList[i].codeAB == '00000000000002282020') {
            dateContratInfo = '<hr/>' + 'Date du contrat : ' + '<span style="font-weight:bold;color:green">' + bienList[i].dateAcquisition2 + '</span>';
        }

        if (bienList[i].isImmobilier === '1') {

            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + bienList[i].usageName + '</span>';
            var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + bienList[i].communeName + '</span>';

            var quartierInfo = 'Quartier :';

            if (bienList[i].communeName !== empty) {
                quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + bienList[i].quartierName + '</span>';
            }

            var complementInfo = bienList[i].complement;

            descriptionBien = natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<hr/>' + complementInfo + dateContratInfo;


        } else {

            if (bienList[i].type == 1) {

                var genreInfo = 'Genre : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
                var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + bienList[i].complement;

            } else if (bienList[i].type == 3) {

                var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
                var categorieInfo2 = 'Destination : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + bienList[i].complement;

            } else if (bienList[i].type == 4) {

                var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
                var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + bienList[i].complement + dateContratInfo;

            } else {

                descriptionBien = bienList[i].descriptionBien + '<hr/>' + bienList[i].complement;
            }
        }


        var btnDisabledBien = '';

        if (controlAccess('DELETE_BIEN')) {
            btnDisabledBien = '<br/><br/><a onclick="disabledBien(\'' + bienList[i].idAcquisition + '\',\'' + proprietaire + '\')" class="btn btn-danger" title="Désactiver le bien"><i class="fa fa-trash-o"></i>&nbsp;D&eacute;sactiver ce bien</a>';
        }

        body += '<tr>';
        body += '<td style="vertical-align:middle;">' + bienList[i].intituleBien + '</td>';
        body += '<td style="vertical-align:middle;">' + descriptionBien + '</td>';
        body += '<td style="vertical-align:middle;">' + bienList[i].chaineAdresse.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;font-weight:bold">' + bienList[i].responsable.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;text-align:center">'
                + '<a onclick="editBien(\'' + bienList[i].idBien + '\',\'' + bienList[i].codeTypeBien + '\',\'' + bienList[i].isImmobilier + '\')" class="btn btn-success" title="Modifier le bien"><i class="fa fa-edit"></i>&nbsp;Modifier ce bien</a>' + btnDisabledBien
                + '<a style="display:none" onclick="storyBien(\'' + 0 + '\')" class="btn btn-warning" title="Voir l\'historique"><i class="fa fa-history"></i></a></td>';
        body += '<td hidden="true">' + bienList[i].idBien + '</td>';
        body += '<td hidden="true">' + bienList[i].type + '</td>';
        body += '<td hidden="true">' + bienList[i].communeCode + '</td>';
        body += '<td hidden="true">' + bienList[i].codeTypeBien + '</td>';
        body += '</tr>';

        var bien = new Object();

        bien.idBien = bienList[i].idBien;
        bien.intituleBien = bienList[i].intituleBien;
        bien.chaineAdresse = bienList[i].chaineAdresse;
        bien.codeAP = bienList[i].codeAP;
        bien.dateAcquisition = bienList[i].dateAcquisition;
        bien.codeTypeBien = bienList[i].codeTypeBien;
        bien.communeCode = bienList[i].communeCode;
        bien.natureCode = bienList[i].codeTypeBien;
        tempBienList.push(bien);
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableBiens.html(tableContent);
    var dtBien = tableBiens.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste ici _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 20
    });
    $('#tableBiens tbody').on('click', 'tr', function () {
        var data = dtBien.row(this).data();
        //alert(JSON.stringify(data));
        selectedBien = data[5];
        codeNatureBienSelected = data[8];

        //alert(codeNatureBienSelected);

        switch (data[6]) {
            case 1:
            case 2:
                isTypeMobilier = false;
                break;

            default:
                isTypeMobilier = true;
                communeCodeBienSelected = data[7];
                break;
        }

        clickExist = true;
    });
}

function printArticles(articleList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" hidden="true"> Id </th>';
    header += '<th scope="col" hidden="true"> Code </th>';
    header += '<th scope="col" style="width:300px"> SECTEUR </th>';
    header += '<th scope="col" style="width:30px"> BIEN </th>';
    header += '<th scope="col" style="width:400px"> IMPÔT </th>';
    header += '<th scope="col" style="width:100px"> PERIODICITE </th>';
    header += '<th scope="col" style="width:100px"> ETAT </th>';
    header += '<th scope="col" style="width:150px"> </th>';
    header += '</tr></thead>';

    tempArticleList = [];

    var body = '<tbody id="tbodyArticles">';
    var firstLineAB = '';

    for (var i = 0; i < articleList.length; i++) {

        var article = {};

        firstLineAB = articleList[i].intituleAB;

        if (articleList[i].intituleTarif.toUpperCase() != 'TOUT' && articleList[i].intituleTarif.toUpperCase() != 'UNIQUE') {

            firstLineAB += ' : ' + articleList[i].intituleTarif;
        }

        if (firstLineAB.length > 340) {
            firstLineAB = firstLineAB.substring(0, 340) + ' ...';
        }

        article.idAssujettissement = articleList[i].idAssujettissement;
        article.codeAB = articleList[i].codeAB;
        article.intituleAB = articleList[i].intituleAB.toUpperCase();
        article.valeurBase = articleList[i].valeurBase;
        article.unite = articleList[i].unite;
        article.codeUnite = articleList[i].codeUnite;
        article.duree = articleList[i].duree;
        article.dateDebut = articleList[i].dateDebut;
        article.dateFin = articleList[i].dateFin;
        article.dateFin2 = articleList[i].dateFin2;
        article.reconduction = articleList[i].reconduction;
        article.nombreJour = articleList[i].nombreJour;
        article.nombreJourLimite = articleList[i].nombreJourLimite;
        article.nombreJourLimitePaiement = articleList[i].nbreJourLegalePaiement;
        article.echeanceLegale = articleList[i].echeanceLegale;
        article.periodeEcheance = articleList[i].periodeEcheance;
        article.codePeriodiciteAB = articleList[i].codePeriodiciteAB;
        article.listPeriodesDeclarations = articleList[i].listPeriodesDeclarations;
        article.montantDu = formatNumber(articleList[i].montant, articleList[i].devise);
        article.intituleTarif = articleList[i].intituleTarif;
        article.libellePeriodiciteAB = articleList[i].libellePeriodiciteAB;
        article.waittingClosing = articleList[i].waittingClosing;

        tempArticleList.push(article);

        var etat = 'VALIDE';
        var etatColor = 'black';

        if (articleList[i].etat == 0) {

            etat = 'ANNULE';
            etatColor = 'red';
        }


        var btnAddPeriodeDeclarationBien = '';
        var btnDeleteAssujettissement = '';

        if (controlAccess('ADD_NEW_PERIODE_DECLARATION_ASSUJ')) {
            btnAddPeriodeDeclarationBien = '<br/><br/><a onclick="addPeriodeDeclarationBien(\'' + articleList[i].intituleBien + '\',\'' + articleList[i].idAssujettissement + '\')" class="btn btn-success" title="Ajouter la période de déclaration"><i class="fa fa-plus-circle"></i>&nbsp;Ajouter p&eacute;riodes</a>';
        }

        if (controlAccess('DELETE_ASSUJETISSEMENT')) {
            btnDeleteAssujettissement = '<br/><br/><a onclick="deleteAssujettissement(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-danger" title="Supprimer assujetissement"><i class="fa fa-trash-o"></i>&nbsp;Supprimer assujetissement</a>';
        }

        body += '<tr>';
        body += '<td hidden="true">' + articleList[i].idAssujettissement + '</td>';
        body += '<td hidden="true">' + articleList[i].codeAB + '</td>';
        body += '<td style="width:20%;vertical-align:middle">' + articleList[i].secteurActivite.toUpperCase() + '</td>';
        body += '<td style="text-align:left;width:25%;vertical-align:middle"><span style="font-weight:bold">' + articleList[i].intituleBien.toUpperCase() + '</span><br/><br/>' + articleList[i].adresseBien.toUpperCase() + '</td>';

        if (articleList[i].isManyAB) {
            body += '<td style="width:30%;vertical-align:middle" title="Plusieurs"><a href="#3" style="font-weight:bold;color:blue;text-decoration:underline">Voir la liste</a></td>';
        } else {
            body += '<td style="width:30%;vertical-align:middle" title="' + articleList[i].intituleAB + '"><span style="font-weight:bold;">' + firstLineAB.toUpperCase() + '</td>';
        }

        var valueX = '';

        switch (articleList[i].unite) {
            case 'USD':
            case 'CDF':

                valueX = formatNumber(articleList[i].valeurBase, articleList[i].unite);
                break;
            default:
                //valueX = articleList[i].valeurBase + ' ' + articleList[i].unite;
                valueX = articleList[i].valeurBase + ' ' + articleList[i].uniteValeurBase;
                break;
        }

        var redressementAssuj = '';

        if (controlAccess('REDRESSER_BASE_CALCUL')) {
            redressementAssuj = '<hr/><a  onclick="callModaleRedressement(\'' + articleList[i].idAssujettissement + '\',\'' + valueX + '\',\'' + articleList[i].codeUnite + '\',\'' + articleList[i].valeurBase + '\',\'' + articleList[i].unite + '\',\'' + articleList[i].codeAB + '\')" class="btn btn-danger"><i class="fa fa-edit"></i>&nbsp;Modifier la base de calcul</a>';
        }


        var valeurBaseInfo = '<hr/>VALEUR DE BASE : <span style="font-weight:bold;color:green">' + valueX + '</span>' + redressementAssuj;


        body += '<td style="width:10%;vertical-align:middle"> ' + articleList[i].libellePeriodiciteAB + valeurBaseInfo + '</td>';
        body += '<td style="width:8%;vertical-align:middle;color:' + etatColor + '"> ' + etat + '</td>';

        var taxButton = '<a  onclick="gotoTaxation(\'' + articleList[i].idAssujettissement + '\',' + articleList[i].isManyAB + ')" class="btn btn-success" title="Passer à la taxation"><i class="fa fa-arrow-right"></i></a>&nbsp;';
        var generateButton = '<a  onclick="generatePeriode(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-success" title="Générer période"><i class="fa fa-plus-circle"></i></a>&nbsp;';
        var deleteButton = '<a onclick="removeAssujettissement(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-danger" title="Annuler cet assujettissement"><i class="fa fa-close"></i></a>&nbsp';

        if (!controlAccess('SET_TAXATION_FISCALE')) {
            taxButton = '';
        }
        if (!controlAccess('3011')) {
            generateButton = '';
        }
        if (!controlAccess('3012')) {
            deleteButton = '';
        }

        if (articleList[i].etat == 1) {

            body += '<td style="text-align:center;width:17%;vertical-align:middle">'
                    + '' + deleteButton + ''
                    + '<a onclick="openPeriodes(\'' + articleList[i].idAssujettissement + '\',\'' + articleList[i].intituleBien.toUpperCase() + '\',\'' + articleList[i].adresseBien.toUpperCase() + '\')" class="btn btn-warning" title="Afficher les périodes"><i class="fa fa-list"></i>&nbsp;Voir les p&eacute;riodes</a>&nbsp;' + btnAddPeriodeDeclarationBien + btnDeleteAssujettissement;
            +'' + generateButton + '' + '' + taxButton + '</td>';
        } else {

            body += '<td style="text-align:center;width:10%;vertical-align:middle">'
                    + '<a onclick="openPeriodes(\'' + articleList[i].idAssujettissement + '\',\'' + articleList[i].intituleBien.toUpperCase() + '\',\'' + articleList[i].adresseBien.toUpperCase() + '\')" class="btn btn-warning" title="Afficher les périodes"><i class="fa fa-list"></i>&nbsp;Voir les p&eacute;riodes</a></td>&nbsp;' + btnAddPeriodeDeclarationBien + btnDeleteAssujettissement;
        }

        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableArticles.html(tableContent);
    var dtArticles = tableArticles.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste ici  _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        columnDefs: [
            {"visible": false, "targets": 4}
        ],
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os', blurable: true
        },
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(4, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });
    $('#tableArticles tbody').on('click', 'tr', function () {
        var data = dtArticles.row(this).data();
    });
}

function printOtherBien(bienList) {

    var dataBien = '<option value ="0">-- Sélectionner --</option>';
    for (var i = 0; i < bienList.length; i++) {
        dataBien += '<option value =' + bienList[i].idBien + '>' + bienList[i].intituleBien + '</option>';
    }

    return dataBien;
}

function generatePeriode(idAssujettissement) {

    alertify.confirm('Etes-vous sûre de vouloir générer une période supplémentaire ?', function () {

        var dateFin = '', nombreJour = '', nombreJourLimite = '', nombreJourLimitePaiement = '', echeanceLegale = '', codePeriodiciteAB = '', periodeEcheance = '';

        for (var i = 0; i < tempArticleList.length; i++) {

            if (tempArticleList[i].idAssujettissement == idAssujettissement) {

                dateFin = tempArticleList[i].dateFin2;
                nombreJour = tempArticleList[i].nombreJour;
                nombreJourLimite = tempArticleList[i].nombreJourLimite;
                echeanceLegale = tempArticleList[i].echeanceLegale;
                periodeEcheance = tempArticleList[i].periodeEcheance;
                codePeriodiciteAB = tempArticleList[i].codePeriodiciteAB;
                nombreJourLimitePaiement = tempArticleList[i].nombreJourLimitePaiement;

                break;
            }
        }

        createPeriodeDeclarationIfNotExist(1, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance);
    });
}

function gotoTaxation(idAssujettissement, isMany) {

    alertify.confirm('Etes-vous sûre de vouloir passer à la déclaration proprement dite ?', function () {

        var dateFin = '', nombreJour = '', nombreJourLimite = '', nombreJourLimitePaiement = '', echeanceLegale = '', codePeriodiciteAB = '', periodeEcheance = '';

        for (var i = 0; i < tempArticleList.length; i++) {

            if (tempArticleList[i].idAssujettissement == idAssujettissement) {

                dateFin = tempArticleList[i].dateFin2;
                nombreJour = tempArticleList[i].nombreJour;
                nombreJourLimite = tempArticleList[i].nombreJourLimite;
                echeanceLegale = tempArticleList[i].echeanceLegale;
                periodeEcheance = tempArticleList[i].periodeEcheance;
                codePeriodiciteAB = tempArticleList[i].codePeriodiciteAB;
                nombreJourLimitePaiement = tempArticleList[i].nombreJourLimitePaiement;

                break;
            }
        }

        createPeriodeDeclarationIfNotExist(0, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance, isMany);
    });
}

var tempPeriodeDeclaration;

function openPeriodes(idAssujettissement, bienName, adresseName) {


    lblBienName.html('BIEN : ' + bienName.toUpperCase());
    lblAdresseBien.html('ADRESSE : ' + adresseName.toUpperCase());

    for (var i = 0; i < tempArticleList.length; i++) {

        if (tempArticleList[i].idAssujettissement == idAssujettissement) {

            var header = empty, body = empty;

            tempPeriodeDeclaration = tempArticleList[i].listPeriodesDeclarations;

            header += '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>';
            header += '<th style="text-align:left">EXERCICE</th>';
            header += '<th style="text-aLign:left">ECHEANCE DECLARATION</th>';
            header += '<th style="text-aLign:left">DATE TAXATION</th>';
            header += '<th style="text-aLign:left">NOTE TAXATION</th>';
            header += '<th style="text-aLign:left">NOTE PERCEPTION</th>';
            header += '<th style="text-aLign:right">MONTANT DU</th>';
            header += '<th style="text-aLign:right">PENALITE DU</th>';
            header += '<th style="text-aLign:right">TOTAL DU</th>';
            header += '<th style="text-aLign:right">TOTAL PAYE</th>';
            header += '<th style="text-aLign:right">RESTE A PAYER</th>';
            header += '</tr></thead>';

            for (var j = 0; j < tempPeriodeDeclaration.length; j++) {

                var periodeDeclaration = tempPeriodeDeclaration[j];


                var montantDu = empty, penaliteDu = empty, totalDu = empty, montantPayer = empty, restePayer = empty;
                var noteTaxation = periodeDeclaration.noteTaxation;

                if (noteTaxation != empty) {

                    montantDu = formatNumber(periodeDeclaration.montantDu, periodeDeclaration.devise);
                    penaliteDu = formatNumber(periodeDeclaration.penaliteDu, periodeDeclaration.devise);
                    totalDu = formatNumber((periodeDeclaration.montantDu + periodeDeclaration.penaliteDu), periodeDeclaration.devise);
                    montantPayer = formatNumber(periodeDeclaration.montantPayer, periodeDeclaration.devise);

                    if (periodeDeclaration.montantPayer > 0) {
                        restePayer = formatNumber(0, periodeDeclaration.devise);
                    } else {
                        restePayer = formatNumber(periodeDeclaration.montantDu, periodeDeclaration.devise);
                    }


                }

                switch (periodeDeclaration.estPenalise) {
                    case "1":
                        colorEcheance = 'color: red';
                        break;
                    case "0":
                        colorEcheance = 'color: black';
                        break;
                }

                var background = '';

                if (noteTaxation !== '') {
                    background = 'style', 'background-color:#daf2c9';
                }

                body += '<tr ' + background + '>';
                body += '<td style="text-align:left;width:9%;vertical-align:middle">' + periodeDeclaration.periode + '</td>';
                body += '<td style="text-align:left;width:7%;vertical-align:middle">' + periodeDeclaration.dateLimite + '</td>';
                body += '<td style="text-align:left;width:11%;vertical-align:middle">' + periodeDeclaration.dateDeclaration + '</td>';
                body += '<td style="text-align:left;width:9%;vertical-align:middle">' + noteTaxation + '</td>';
                body += '<td style="text-align:left;width:10%;vertical-align:middle">' + periodeDeclaration.notePerception + '</td>';
                body += '<td style="text-align:right;width:10%;vertical-align:middle">' + montantDu + '</td>';
                body += '<td style="text-align:right;width:10%;vertical-align:middle">' + penaliteDu + '</td>';
                body += '<td style="text-align:right;width:10%;vertical-align:middle">' + totalDu + '</td>';
                body += '<td style="text-align:right;width:10%;vertical-align:middle">' + montantPayer + '</td>';
                body += '<td style="text-align:right;width:10%;vertical-align:middle">' + restePayer + '</td>';

            }

            body += '</tr></tbody>';

            var tableContent = header + body;
            tablePeriodeDeclaration.html(tableContent);
            tablePeriodeDeclaration.DataTable({
                language: {
                    processing: "Traitement en cours...",
                    track: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible pour le critère sélectionné",
                    search: "Filtrer la liste ici ",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                },
                info: false,
                destroy: true,
                searching: true,
                paging: true,
                order: [[0, 'asc']],
                lengthChange: false,
                tracking: false,
                ordering: false,
                pageLength: 10,
                lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
                /*select: {
                 style: 'os',
                 blurable: true
                 }*/
                datalength: 3
            });

            modalPeriodeDeclaration.modal('show');
            break;
        }
    }
}

function removeAssujettissement(idAssujettissement) {

    alertify.confirm('Etes-vous sûre de vouloir retirer cet acte générateur du bien ?', function () {
        desactivateAssujettissement(idAssujettissement);
    });
}

function createPeriodeDeclarationIfNotExist(isGenerate, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance, isMany) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'createPeriodeDeclaration',
            'isGenerate': isGenerate,
            'idAssujettissement': idAssujettissement,
            'dateFin': dateFin,
            'nombreJour': nombreJour,
            'nombreJourLimite': nombreJourLimite,
            'nbreJourLegalePaiement': nombreJourLimitePaiement,
            'echeanceLegale': echeanceLegale, 'codePeriodiciteAB': codePeriodiciteAB,
            'periodeEcheance': periodeEcheance
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Veuillez patientier...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1' && isGenerate == 0) {

                    if (isMany) {
                        setAssujettiData(JSON.stringify(selectAssujettiData));
                    }

                    window.location = isMany ? 'taxation-repetitive_multiple?assuj=' + btoa(idAssujettissement) : 'taxation-repetitive?id=' + btoa(idAssujettissement);

                } else if (response == '1' && isGenerate == 1) {

                    alertify.alert('Des périodes supplémnetaires sont générées avec succès');
                    loadBiens(codeResponsible, '0');

                } else if (response == '0') {

                    alertify.alert('Echec lors du passage à la déclaration');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function desactivateAssujettissement(idAssujettissement) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'desactivateAssujettissement',
            'idAssujettissement': idAssujettissement
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Annulation de l\'assujettissement en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('l\'assujettissement est annulé avec succès.');

                    setTimeout(function () {

                        loadBiens(codeResponsible, '0');
                    }
                    , 1000);

                } else if (response == '0') {
                    alertify.alert('Echec lors de l\'annulation de l\'assujettissement');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function resetSearchAssujetti() {

    inputValueResearchAssujetti.val('');
    printAssujettiTable('');
}

function disabledBien(idAcquisition, proprietaire) {

    alertify.confirm('Etes-vous sûre de vouloir rétirer ce bien ?', function () {

        $.ajax({
            type: 'POST',
            url: 'assujetissement_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'desactivateBienAcquisitionV2',
                'idAcquisition': idAcquisition
            },
            beforeSend: function () {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Rétrait du bien en cours ...</h5>'});
            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1') {
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    if (response == '1') {

                        alertify.alert('Le rétrait du bien s\'est effectué avec succès.');
                        loadBiens(codeResponsible, '0');

                    } else if (response == '0') {
                        alertify.alert('Echec lors du rétrait du bien');
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });
}

function showDeclarationDetail(nc, state, periode) {

    alertify.confirm('Etes-vous sûre de vouloir visualiser les détails de cette taxation ?', function () {

        //setRegisterType('RNT');

        if (state === '0') {
            setRegisterType('RNCC');
            window.open('registre-notes-taxations-cloturees?nc=' + btoa(nc), '_blank');
        } else if (state === '1') {

            if (controlAccess('CLOTURE_TAXATION')) {
                setRegisterType('RNC')
                window.open('cloturer-taxation?nc=' + btoa(nc), '_blank');
            } else {
                alertify.alert('La taxation n° ' + '<span style="font-weight: bold">' + nc + '</span>' + ' relative à la période de déclaration ' + '<span style="font-weight: bold">' + periode + '</span>' + ' est en attente d\'une clôture.<br/><br/>Vous ne possedez pas le droit d\'accéder au registre des notes de ce type.');
                return;
            }

        }

    });
}

function loadModalMiseEnQuarantaine(periodeId, periodeName) {

    periodeDeclarationId = periodeId;
    periodeDeclarationName = periodeName;
    textaeraObservation.val(empty);
    valuePeriode1.html(periodeName);
    valueAgent1.html(userData.nomComplet);
    modalMiseEnQuarantaine.modal('show');
}

function miseEnQuarantaine() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'miseQuarantaine',
            'periodeId': periodeDeclarationId,
            'observation': textaeraObservation.val(),
            'userId': userData.idUser
        },
        beforeSend: function () {

            modalMiseEnQuarantaine.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>La mise en quarantaine en cours ...</h5>'});
        },
        success: function (response) {

            if (response == '-1' || response == '0') {
                modalMiseEnQuarantaine.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalMiseEnQuarantaine.unblock();
                modalMiseEnQuarantaine.modal('hide');

                alertify.alert('La mise en quarantaine s\'est effectuée avec succès.');


                loadBiens(codeAssujettiSave, '1');

                modalPeriodeDeclaration.modal('hide');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalMiseEnQuarantaine.unblock();
            showResponseError();
        }
    });
}

function loadModalInfoMiseEnQuarantaine(periodeName, obervation, agent, date) {

    valuePeriode2.html(periodeName);
    valueDate2.html(date);
    valueAgent2.html(agent);
    valueObservation.val(obervation);

    modalInfoMiseEnQuarantaine.modal('show');
}

function editBien(idBien, codeTypeBien, isImmobilier) {

    var codeBien = idBien + ';' + codeTypeBien;

    switch (isImmobilier) {
        case '0':
            if (codeTypeBien == 'TB00000041') {
                window.location = 'edition-bien-concession-mine?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&codeBien=' + btoa(codeBien);
            } else if (codeTypeBien == 'TB00000042') {
                window.location = 'edition-bien-panneau-publicitaire?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&codeBien=' + btoa(codeBien);
            } else {
                window.location = 'edition-bien-automobile?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&codeBien=' + btoa(codeBien);
            }

            break;
        case '1':
            window.location = 'edition-bien-immobilier?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&codeBien=' + btoa(codeBien);
            break;
    }


}

function prepareDateAcquitionBien(idBien) {

    for (var i = 0; i < bienList.length; i++) {

        if (idBien == bienList[i].idBien) {
            dateAcquisitionBien = bienList[i].dateAcquisition;
            break;
        }

    }
    return dateAcquisitionBien;
}

function callModaleRedressement(id, base, unite, baseOnly, libelleUnite, codeAB) {


    assujettissementIDSlected = id;

    inputObservationRedressement.val('');
    newBase.val(baseOnly);
    spnBaseActuelle.html(base);

    var unites = JSON.parse(userData.listUniteJson);

    var dataUnite = '';
    dataUnite += '<option value="0">--</option>';

    switch (codeAB) {

        case '00000000000002282020':
        case '00000000000002352021':
        case '00000000000002362021':
        case '00000000000002292020':

            for (var i = 0; i < unites.length; i++) {

                if (unites[i].uniteName == libelleUnite) {
                    unite = unites[i].uniteCode;
                }
                dataUnite += '<option value="' + unites[i].uniteCode + '">' + unites[i].uniteName + '</option>';
            }
            break;
        default:

            for (var i = 0; i < unites.length; i++) {
                dataUnite += '<option value="' + unites[i].uniteCode + '">' + unites[i].uniteName + '</option>';
            }
            break;
    }



    cmbUnite.html(dataUnite);
    cmbUnite.val(unite);

    modalRedressement.modal('show');

}

function saveRedressement() {

    alertify.confirm('Etes-vous sûre de vouloir modifier cette base de calcul ?', function () {

        $.ajax({
            type: 'POST',
            url: 'assujetissement_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'redresement',
                'id': assujettissementIDSlected,
                'observation': inputObservationRedressement.val(),
                'uniteCode': $('#cmbUnite option:selected').text().toUpperCase(), //cmbUnite.val(),
                'userId': userData.idUser,
                'newBase': newBase.val()
            },
            beforeSend: function () {

                modalRedressement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            },
            success: function (response)
            {

                if (response == '-1' || response == '0') {
                    modalRedressement.unblock();
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    modalRedressement.unblock();
                    modalRedressement.modal('hide');

                    alertify.alert('La modification de la base de calcul s\'est effectué avec succès.');


                    loadBiens(codeResponsible, '0');


                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                modalRedressement.unblock();
                showResponseError();
            }
        });

    });
}

var resultNewPeriodeList;

function addPeriodeDeclarationBien(bienName, code) {

    idAssujettissement = code;

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getNewPeriodeDeclarationBien',
            'codeAssuj': code
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));
                beginPeriodValue = result[0].beginPeriod;

                resultNewPeriodeList = result[0].periodeList;

                printTableNewPeriodeDeclaration(resultNewPeriodeList, beginPeriodValue);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });

    spnBienAssujSelected.html(bienName);
}

var checkExist;

function setNewPeriodeDeclarationSelected(id) {

    var checkBox = document.getElementById("checkbox_" + id);
    var row = $("#row_" + id);

    if (checkBox.checked) {

        row.attr('style', 'background-color:#daf2c9');
        checkExist = true;

        btnAjouterPD.attr('style', 'display:block');

        for (var i = 0; i < resultNewPeriodeList.length; i++) {

            if (resultNewPeriodeList[i].ordrePeriode == id) {

                var periode = new Object();

                periode.dateDebut = resultNewPeriodeList[i].dateDebut;
                periode.dateFin = resultNewPeriodeList[i].dateFin;
                periode.periode = resultNewPeriodeList[i].periode;
                periode.dateLimite = resultNewPeriodeList[i].dateLimite;
                periode.ordrePeriode = resultNewPeriodeList[i].ordrePeriode;
                periode.dateLimitePaiement = resultNewPeriodeList[i].dateLimitePaiement;

                tempPeriodeDeclarationList.push(periode);
            }
        }
    } else {


        row.removeAttr('style');

        for (var k = 0; k < tempPeriodeDeclarationList.length; k++) {

            if (tempPeriodeDeclarationList[k].ordrePeriode == id) {

                tempPeriodeDeclarationList.splice(k, 1);
                break;
            }
        }

        if (tempPeriodeDeclarationList.length > 0) {
            checkExist = true;
            btnAjouterPD.attr('style', 'display:block');
        } else {
            checkExist = false;
            btnAjouterPD.attr('style', 'display:none');

        }
    }


}

function printTableNewPeriodeDeclaration(periodeList, beginValue) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="text-align:center"> PERIODE </th>';
    header += '<th style="text-align:left"> ECHEANCE DECLARATION </th>';
    header += '<th style="text-align:center"> COCHEZ </th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    tempPeriodeDeclarationList = [];

    for (var i = 0; i < periodeList.length; i++) {

        var readOnly = periodeList.length === 0 ? 'hidden' : 'checkbox';

        body += '<tr id="row_' + periodeList[i].ordrePeriode + '">';
        body += '<td style="text-align:center;width:10%;vertical-align:middle">' + periodeList[i].periode + '</td>';
        body += '<td style="text-align:left;width:20%;vertical-align:middle">' + periodeList[i].dateLimite + '</td>';
        body += '<td style="text-align:center;width:6%;vertical-align:middle"><input  type="' + readOnly + '" id="checkbox_' + periodeList[i].ordrePeriode + '" onclick="setNewPeriodeDeclarationSelected(\'' + periodeList[i].ordrePeriode + '\')" /></th>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableNewPDAssuj.html(tableContent);

    tableNewPDAssuj.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 12,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });

    spnPDDepart.html(beginValue);
    modalAddPeriodeDeclarationAssujettissement.modal('show');
}

function saveAddNewPriodeDeclaration() {

    var debut = '', fin = '', value = '';

    if (tempPeriodeDeclarationList.length == 1) {

        value = '';
        debut = tempPeriodeDeclarationList[0].dateDebut;
        fin = tempPeriodeDeclarationList[0].dateFin;

    } else if (tempPeriodeDeclarationList.length > 1) {

        for (var i = 0; i < tempPeriodeDeclarationList.length; i++) {

            if (value == '') {

                value = tempPeriodeDeclarationList[i].ordrePeriode;

                debut = tempPeriodeDeclarationList[i].dateDebut;
                fin = tempPeriodeDeclarationList[i].dateFin;

            } else {

                if (tempPeriodeDeclarationList[i].ordrePeriode < value) {

                    value = tempPeriodeDeclarationList[i].ordrePeriode;
                    debut = tempPeriodeDeclarationList[i].dateDebut;
                    fin = tempPeriodeDeclarationList[i].dateFin;
                }
            }
        }
    }

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveAddNewPriodeDeclaration',
            'codeAssuj': idAssujettissement,
            'dateDebut': debut,
            'dateFin': fin,
            'periodeList': JSON.stringify(tempPeriodeDeclarationList),
            'userId': userData.idUser
        },
        beforeSend: function () {

            modalAddPeriodeDeclarationAssujettissement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ajout des périodes en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                modalAddPeriodeDeclarationAssujettissement.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalAddPeriodeDeclarationAssujettissement.unblock();

                alertify.alert('L\'ajout de période de déclaration s\'est effectué avec succès.');
                modalAddPeriodeDeclarationAssujettissement.modal('hide');

                loadBiens(codeResponsible, '0');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalAddPeriodeDeclarationAssujettissement.unblock();
            showResponseError();
        }
    });
}

function getDatePeriode(ordre, debut) {

    var result = '';
    var x = '';

    for (var i = 0; i < tempPeriodeDeclarationList.length; i++) {

        if (ordre == tempPeriodeDeclarationList[i].ordrePeriode) {
            if (debut == true) {
                result = tempPeriodeDeclarationList[i].dateDebut;
            } else {
                result = tempPeriodeDeclarationList[i].dateFin;
            }
            break;
        }
    }
    return result;
}

function deleteAssujettissement(idAssuj) {

    alertify.confirm('Etes-vous sûre de vouloir supprimer cet assujettissement ?', function () {

        $.ajax({
            type: 'POST',
            url: 'assujetissement_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'deleteAssujettissement',
                'id': idAssuj,
                'userId': userData.idUser
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression en cours ...</h5>'});
            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1' || response == '0') {
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    alertify.alert('La suppression de l\'assujettissement s\'est effectuée avec succès.');
                    loadBiens(codeResponsible, '0');

                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });
}
