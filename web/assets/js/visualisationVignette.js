/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var nombre;

var documentContainer = $('#document_container'),
        puissanceFiscalKg = $('#puissanceFiscalKg'),
        puissanceFiscalCv = $('#puissanceFiscalCv'),
        dateImpression = $('#dateImpression'),
        chassis = $('#chassis'),
        adresse = $('#adresse'),
        phonePersonne = $('#phonePersonne'),
        penalite = $('#penalite'),
        total = $('#total'),
        plaque = $('#plaque'),
        plaque2 = $('#plaque2'),
        marque = $('#marque'),
        marque2 = $('#marque2'),
        modele = $('#modele'),
        modele2 = $('#modele2'),
        adresse = $('#adresse'),
        codePersonne = $('#codePersonne'),
        namePersonne = $('#namePersonne'),
        nameUserPrint = $('#nameUserPrint'),
        numeroSerieVignette = $('#numeroSerieVignette'),
        lieuPrintVignette = $('#lieuPrintVignette'),
        btnPrint1 = $('#btnPrint1'),
        btnPrint = $('#btnPrint'),
        btnPrint2 = $('#btnPrint2');

var modalPrintVignette, btnPrintVignette;

var obj = new Object();
var objDataVignette = new Object();
$(function () {

    modalPrintVignette = $('#modalPrintVignette');
    btnPrintVignette = $('#btnPrintVignette');

    document_container[$('table').attr('border', '0')];

    for (var i = 0; i <= 22; i++) {
        $("#field_" + i).html('');
    }

    objDataVignette = CryptoJS.AES.decrypt(sessionStorage.getItem('objDataVignette'), CYPHER_KEY).toString(CryptoJS.enc.Utf8);
    objDataVignette = JSON.parse(objDataVignette);
    printVignette(objDataVignette);

    btnPrint1.click(function (e) {

        e.preventDefault();
        document_container[$('table').attr('style', 'margin-top: 257px; margin-left: 30%')];

    });
    
    $('#plaque2').attr('style', 'font-size: 7px; margin-bottom: 20px; text-align: center;margin-left: 10px; font-weight: bold');
    $('#adresse').attr('style', 'font-size: 8px;text-align: center; font-weight: bold');

    btnPrint.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        sendDataPrintVignette();
    });

    btnPrint2.click(function (e) {

        e.preventDefault();
        document_container[$('table').attr('style', 'margin-top: 252px; margin-left: -15px')];
        $('#marque').attr('style', 'font-size: 10px; margin-top: -2px; font-weight: bold;text-align:left');
        $('#modele').attr('style', 'font-size: 10px ; margin-top: -2px; font-weight: bold;text-align:left');
	$('#plaque').attr('style', 'font-size: 10px ; margin-top: 9%; font-weight: bold;text-align:left');
        $('#codeQR').attr('style', 'margin-left: 44%; margin-bottom: 30%;');
        //sendDataPrintVignette();

    });

});

function sendDataPrintVignette() {

    var valueSerieVignette = '<span style="font-weight:bold">' + objDataVignette.numeroSerieVignette + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir imprimer la vignette n° ' + valueSerieVignette + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'Text',
            crossDomain: false,
            data: {
                'serieVignette': objDataVignette.numeroSerieVignette,
                'plaque': objDataVignette.plaque,
                'notePerception': objDataVignette.notePerception,
                'noteTaxation': objDataVignette.noteTaxation,
                'codeBien': objDataVignette.bienCode,
                'userId': userData.idUser,
                'operation': 'savePrintVignette'
            },
            beforeSend: function () {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'})
            },
            success: function (response)
            {
                setTimeout(function () {

                    $.unblockUI();

                    if (response == '-1' || response == '0') {
                        showResponseError();
                        return;

                    } else if (response == '2') {

                        alertify.alert('DESOLE ! Le nombre d\'impression prévu (3 fois) pour une vignette est épuiser');
                        return;

                    } else {

                        documentContainer.printThis({
                        });
                    }
                });

            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.unblockUI();
                showResponseError();
            }

        });
    });

}

function printVignette(data) {

    //console.log(data);

    data.unitePuissanceFiscal === 'CV' ? puissanceFiscalCv.html(data.puissanceFiscal) : puissanceFiscalKg.html(data.puissanceFiscal);
    dateImpression.html(data.dateImpression);
    chassis.html(data.chassis);
    adresse.html(data.adresse);
    phonePersonne.html(data.phonePersonne);
    penalite.html(formatNumberOnly(data.penalite) + ' $');
    total.html(formatNumberOnly(data.total) + ' $');
    plaque.html(data.plaque);
//    plaque2.html(data.plaque);
    plaque2.html(data.plaque.substr(0, 4) + '<br/>' + data.plaque.substr(4, data.plaque.legth));
    marque2.html(data.marque);
    marque.html(data.marque);
    modele.html(data.modele);
    modele2.html(data.modele);
    adresse.html(data.adresse);
    codePersonne.html(data.codePersonne !== '' ? '' : '');
    namePersonne.html(data.namePersonne);
    nameUserPrint.html(data.nameUserPrint);
    numeroSerieVignette.html(data.numeroSerieVignette !== '' ? '' : '');
    lieuPrintVignette.html(data.lieuPrintVignette);

    jQuery('#codeQR').attr('src', data.codeQr);

}