/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var ModalAssujettiPrevisionCredit;

var divAssujettiPC, cmbSearchTypePC;

var inputValueResearchAssujettiPC;

var btnLauchSearchAssujettiPC, btnSelectAssujetiPC;

var tableResultSearchAssujetiPC;

var codeResearch, messageEmptyValue, messageAvancee;


$(function () {

    ModalAssujettiPrevisionCredit = $('#ModalAssujettiPrevisionCredit');

    divAssujettiPC = $('#divAssujettiPC');
    cmbSearchTypePC = $('#cmbSearchTypePC');

    inputValueResearchAssujettiPC = $('#inputValueResearchAssujettiPC');

    btnLauchSearchAssujettiPC = $('#btnLauchSearchAssujettiPC');
    btnSelectAssujetiPC = $('#btnSelectAssujetiPC');

    tableResultSearchAssujetiPC = $('#tableResultSearchAssujetiPC');

    codeResearch = cmbSearchTypePC.val();
    messageEmptyValue = 'Veuillez d\'abord saisir le nom de l\'assujetti';
    messageAvancee = 'Veuillez d\'abord saisir le critere de recherche.';

    btnLauchSearchAssujettiPC.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        searchAssujettiPC();

    });

    inputValueResearchAssujettiPC.on('keypress', function (e) {
        if (e.keyCode == 13) {
            searchAssujettiPC();
        }

    });

    cmbSearchTypePC.on('change', function (e) {
        codeResearch = cmbSearchTypePC.val();
        inputValueResearchAssujettiPC.val('');

        if (codeResearch === '0') {
            messageEmptyValue = 'Veuillez saisir le nom de l\'assujetti.';
            inputValueResearchAssujettiPC.attr('placeholder', 'Nom de l\'assujetti');
        } else {
            messageEmptyValue = 'Veuillez saisir le numéro d\'identification nationale de l\'assujetti.';
            inputValueResearchAssujettiPC.attr('placeholder', 'Numéro d\'identification nationale');
        }

        printAssujettiTable('');
    });
    
    btnSelectAssujetiPC.click(function (e) {

        if (selectAssujettiData.code === empty) {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir sélectionner <b>' + selectAssujettiData.nomComplet + '</b> ?', function () {
            getSelectedAssujetiData();
            ModalAssujettiPrevisionCredit.modal('hide');
        });
    });

    printAssujettiTable('');


});


function searchAssujettiPC() {

    if (inputValueResearchAssujettiPC.val() === empty || inputValueResearchAssujettiPC.val().length < SEARCH_MIN_TEXT) {
        showEmptySearchMessage();
        return;
    }
    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchAssujettiPC.val(),
             'typeSearch': codeResearch,
            'operation': 'loadAssujetti'
        },
        beforeSend: function () {
            ModalAssujettiPrevisionCredit.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                ModalAssujettiPrevisionCredit.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    ModalAssujettiPrevisionCredit.hide();
                    printAssujettiTable('');
                    return;
                } else {
                    var dataAssujetti = JSON.parse(JSON.stringify(response));
                    if (dataAssujetti.length > 0) {
                        btnSelectAssujetiPC.show();
                        printAssujettiTable(dataAssujetti);
                    } else {
                        btnSelectAssujetiPC.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            ModalAssujettiPrevisionCredit.unblock();
            showResponseError();
        }

    });
}

function printAssujettiTable(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">NIF</th>';
    tableContent += '<th scope="col">ASSUJETTI</th>';
    tableContent += '<th scope="col">TYPE</th>';
    tableContent += '<th scope="col">ADRESSE</th>';
    tableContent += '<th hidden="true" scope="col">Code assujetti</th>';
    tableContent += '<th hidden="true" scope="col">Code forme</th>';
    tableContent += '<th hidden="true" scope="col">Code adresse</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td>' + data[i].nif + '</td>';
        tableContent += '<td>' + data[i].nomComplet + '</td>';
        tableContent += '<td>' + data[i].libelleFormeJuridique + '</td>';
        tableContent += '<td>' + data[i].chaine + '</td>';
        tableContent += '<td hidden="true">' + data[i].codePersonne + '</td>';
        tableContent += '<td hidden="true">' + data[i].codeFormeJuridique + '</td>';
        tableContent += '<td hidden="true">' + data[i].adresseId + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultSearchAssujetiPC.html(tableContent);

    var myDt = tableResultSearchAssujetiPC.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableResultSearchAssujetiPC tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        selectAssujettiData = new Object();
        
        selectAssujettiData.nif = myDt.row(this).data()[0];
        selectAssujettiData.nomComplet = myDt.row(this).data()[1].toUpperCase();
        selectAssujettiData.categorie = myDt.row(this).data()[2].toUpperCase();
        selectAssujettiData.adresse = myDt.row(this).data()[3].toUpperCase();
        selectAssujettiData.code = myDt.row(this).data()[4];
        selectAssujettiData.codeForme = myDt.row(this).data()[5];
        selectAssujettiData.codeAdresse = myDt.row(this).data()[6];

    });
}

