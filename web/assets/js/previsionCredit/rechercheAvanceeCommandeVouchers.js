/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var rechercheAvanceeCommandeVoucher;

var inputDateDebut;
var inputdateLast;

var datePickerDebut;
var datePickerFin;
var selectSitePeage, selectEtatCommande,selectTypeCommande;


$(function () {

    rechercheAvanceeCommandeVoucher = $('#rechercheAvanceeCommandeVoucher');

    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');

    datePickerDebut = $("#datePicker1");
    datePickerFin = $('#datePicker2');

    selectSitePeage = $('#selectSitePeage');
    selectEtatCommande = $('#selectEtatCommande');
    selectTypeCommande = $('#selectTypeCommande');

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerDebut.datepicker("setDate", new Date());
    datePickerFin.datepicker("setDate", new Date());


    loadAxePeage();

});

function loadAxePeage() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAxePeage'

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Il n\'y a pas des postes de péage disponible');
                return;
            } else {

                listSitePeage = $.parseJSON(JSON.stringify(response));
                
                var dataSitePeage = empty;

                dataSitePeage += '<option value="*" style="font-style: italic;font-weight: bold">(Tous les axes)</option>';

                for (var i = 0; i < listSitePeage.length; i++) {

                    dataSitePeage += '<option value="' + listSitePeage[i].axeCode + '">' + listSitePeage[i].axeName + '</option>';
                }

                selectSitePeage.html(dataSitePeage);
                selectSitePeage.val('*');

            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

