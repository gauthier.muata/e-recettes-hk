/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var data = null;
var _ticketList;
var sumCDF, sumUSD;
var tableTicketPeage;
var inputResearchTaxation, typeResearch;
var btnResearchTaxation, btnCallModalSearchAvanded;
var modalRechercheAvanceePeage;
var modalPrintRapportProductionPeage;

var isAdvanced;

var tempTicketList = [];

var btnGenereDoc, btnGenereDoc2;

var txtObservation, inputPercepteur, selectControleur, btnPrintRapportProduction, inputPostePeage, inputTypeRapport, inputAxePeage;
var modalUploadTicket;

var codeTypeDoc = 'TICKET_PEAGE';
var codeDoc = undefined;

var btnValiderDocumentTicket, textObservDocTicket;

var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;
var divBtnsNatigate, btnNext, lblNavigate;
var uploadModalTicket;

var codeTicketSelected;
var isAdvanced;
var selectShiftPeage;

$(function () {

    mainNavigationLabel.text('PEAGE');
    secondNavigationLabel.text('Registre de production des tickets');

    removeActiveMenu();
    linkSubMenuGestionExemptes.addClass('active');

    inputResearchTaxation = $('#inputResearchTaxation');
    inputResearchTaxation.attr('placeholder', 'Numéro de la plaque ou chassis');

    typeResearch = $('#typeResearch');
    btnCallModalSearchAvanded = $('#btnCallModalSearchAvanded');
    tableTicketPeage = $('#tableTicketPeage');
    modalRechercheAvanceePeage = $('#modalRechercheAvanceePeage');
    modalPrintRapportProductionPeage = $('#modalPrintRapportProductionPeage');

    btnGenereDoc = $('#btnGenereDoc');
    btnGenereDoc2 = $('#btnGenereDoc2');

    txtObservation = $('#txtObservation');
    inputPercepteur = $('#inputPercepteur');
    selectControleur = $('#selectControleur');
    btnPrintRapportProduction = $('#btnPrintRapportProduction');
    inputPostePeage = $('#inputPostePeage');
    inputTypeRapport = $('#inputTypeRapport');
    inputAxePeage = $('#inputAxePeage');
    modalUploadTicket = $('#modalUploadTicket');

    btnValiderDocumentTicket = $('#btnValiderDocumentTicket');
    textObservDocTicket = $('#textObservDocTicket');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');
    divBtnsNatigate = $('#divBtnsNatigate');
    btnNext = $('#btnNext');
    lblNavigate = $('#lblNavigate');
    selectShiftPeage = $('#selectShiftPeage');

    txtObservation.val('');

    inputResearchTaxation.keypress(function (e) {
        if (e.keyCode === 13) {
            btnResearchTaxation.trigger('click');
        }
    });

    btnResearchTaxation = $('#btnResearchTaxation');

    btnResearchTaxation.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputResearchTaxation.val() == '') {
            alertify.alert('Veuillez d\'abord saisir le numéro du ticket');
            return;
        }

        data = {};
        data.operation = 'loadPeages';
        data.isAdvanced = '0';
        isAdvanced = '0';
        //data.typeSearch = typeResearch.val();
        data.valueSearch = inputResearchTaxation.val();

        if (controlAccess('VIEW_ALL_SITE_PEAGE')) {
            data.codeSite = '*';
        } else {
            data.codeSite = userData.SiteCode;
        }

        data.userId = userData.idUser;

        send(data);


    });

    btnGenereDoc2.on('click', function (e) {
        e.preventDefault();

        txtObservation.val(empty);
        selectControleur.val(empty);
        inputPercepteur.val($('#selectPercepteur option:selected').text().toUpperCase());
        inputPostePeage.val(empty);
        percepeurCode = selectPercepteur.val();
        inputAxePeage.val(empty);
        inputTypeRapport.val($('#selectTypePaiement option:selected').text().toUpperCase());

        modalPrintRapportProductionPeage.modal('show');
    });

    btnPrintRapportProduction.on('click', function (e) {
        e.preventDefault();

        if (txtObservation.val() == empty) {
            alertify.alert('Veuillez d\'abord saisir les observations');
            return;
        }

        if (selectControleur.val() == empty) {
            alertify.alert('Veuillez d\'abord sélectionner le contrôleur');
            return;
        }

        if (inputAxePeage.val() == empty) {
            alertify.alert('Veuillez d\'abord saisir l\'axe du péage');
            return;
        }

        if (inputPostePeage.val() == empty) {
            alertify.alert('Veuillez d\'abord saisir le poste de péage');
            return;
        }

        alertify.confirm('Etes-vous de vouloir imprimer ce rapport de production ?', function () {
            printRapportProduction();
        });


    });

    btnValiderDocumentTicket.click(function (e) {
        e.preventDefault();

        if (imageList.length == 0) {
            alertify.alert('Veuillez d\'abord joindre le ticket à problème');
            return;
        }

        if (textObservDocTicket.val() == empty) {

            alertify.alert('Veuillez d\'abord motiver la raison de l\'annulation de ce ticket');
            return;
        }

        setDocComplementaryData();

        listArchivesDocument = JSON.stringify(imageList);
        //modalUploadTicket.modal('hide');

        getDocumentsToUpload();

    });

    selectControleur.on('change', function (e) {

        if (selectControleur.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner le contrôleur valide');
            return;
        }
    });

    btnCallModalSearchAvanded.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceePeage.modal('show');
    });

    btnAdvencedSearch = $('#btnAdvencedSearch');
    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        //loadSitePeage();

        data = {};
        data.operation = 'loadPeages';
        data.isAdvanced = '1';
        isAdvanced = '1';
        data.siteCode = selectSitePeage.val();
        data.type = selectTypePaiement.val();
        data.tarif = selectTerif.val();
        data.percepteur = selectPercepteur.val();
        data.dateDebut = inputDateDebut.val();
        data.dateFin = inputdateLast.val();
        data.shift = selectShiftPeage.val();
        send(data);

        modalRechercheAvanceePeage.modal('hide');

    });

    //btnAdvencedSearch.trigger('click');

    document.getElementById("btnGenereDoc").onclick = function () {
        printData();
    };

    printTicketsPeage(empty);

    loadControleurs();

    uplodingTypeDocument(codeTypeDoc);

});


function send(data) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: data,
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            callback(JSON.parse(JSON.stringify(response)), data.operation);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            //alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function callback(response, operation) {

    switch (operation) {
        case 'loadPeages':
            printTicketsPeage(response);
            break;
    }
}

function printTicketsPeage(ticketList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" style="width:10%"> TICKET </th>';
    header += '<th scope="col" style=""> VEHICULE </th>';
    header += '<th scope="col" style=""> TAXE </th>';
    header += '<th scope="col" style="width:13%;text-align:right"> MONTANT </th>';
    header += '<th scope="col" style=""> PERCEPEUR </th>';
    header += '<th scope="col" style=""></th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    sumCDF = 0;
    sumUSD = 0;
    frequence = 0;
    _ticketList = [];

    for (var i = 0; i < ticketList.length; i++) {
        frequence += 1;

        var tarif = '--';
        if (ticketList[i].tarif !== undefined) {
            tarif = ticketList[i].tarif;
        }

        var btnCancelled = '';

        if (controlAccess('CANCELED_TICKET_PEAGE')) {
            btnCancelled = '<button type="button" onclick="callModalCancelledTicket(\'' + ticketList[i].code + '\')" class="btn btn-danger"><i class="fa fa-check-circle"></i></button>';
        }

        var shiftInfo = '<span style="font-weight:bold;color:red">' + ticketList[i].shift + '</span>';

        body += '<tr>';
        body += '<td style="text-align:left;width:15%;vertical-align:middle"><span style="font-weight:bold">' + ticketList[i].code + '</span><br/>Type : <span style="font-style: italic">' + ticketList[i].typePaiement + '</span></td>';
        body += '<td style="text-align:left;width:20%;vertical-align:middle"><span style="font-weight:bold">' + ticketList[i].bien + '</span><br/>\n\
                N° plaque ou chassis : <span style="font-style: italic;font-weight:bold">' + ticketList[i].numPlaque + '</span><br/>\n\
                Propriétaire : <span style="font-style: italic;font-weight:bold">' + ticketList[i].personne + '</span><br/></td>';
        body += '<td style="text-align:left;width:15%;vertical-align:middle"><span style="font-weight:bold">' + ticketList[i].taxe + '</span><br/>Tarif : <span style="font-style: italic">' + tarif.toUpperCase() + '</span></td>';
        body += '<td style="text-align:right;width:15%;vertical-align:middle">' + formatNumber(ticketList[i].montant, ticketList[i].devise) + '</td>';
        body += '<td style="text-align:left;width:30%;vertical-align:middle"><span style="font-weight:bold">' + ticketList[i].agentCreate + ' ' + shiftInfo + '</span><br/>Site : <span style="font-style: italic;font-weight:bold">' + ticketList[i].site + '</span><br/>\n\
                Produit le : <span style="font-style: italic;font-weight:bold">' + ticketList[i].dateProd + '</span><br/>\n\Synchronisé le : <span style="font-style: italic;font-weight:bold">' + ticketList[i].dateSync + '</span></td>';
        body += '<td style="text-align:center;width:5%;vertical-align:middle">' + btnCancelled + '</td>';
        body += '</tr>';

        if (ticketList[i].devise == 'CDF') {
            sumCDF += +ticketList[i].montant;
        } else {
            sumUSD += +ticketList[i].montant;
        }

        var montant = formatNumberOnly(ticketList[i].montant);

        var data = {};
        data.code = ticketList[i].code + '\nType : ' + ticketList[i].typePaiement;
        data.bien = ticketList[i].bien + '\nN° plaque : ' + ticketList[i].numPlaque + '\nPropriétaire : ' + ticketList[i].personne;
        data.taxe = ticketList[i].taxe + '\nTarif : ' + ticketList[i].tarif.toUpperCase();
        data.montant = montant + ' ' + ticketList[i].devise;
        data.agentCreate = ticketList[i].agentCreate + '\nSite : ' + ticketList[i].site + '\nProduit le ' + ticketList[i].dateProd;
        _ticketList.push(data);

        var obj = {};

        obj.ticketId = ticketList[i].code;
        tempTicketList.push(obj);
    }



    if (isAdvanced == '1') {

        if (frequence > 0) {

            btnGenereDoc.attr('style', 'display:inline');

            if (isAdvanced === '1') {
                btnGenereDoc2.attr('style', 'display:inline');
            } else {
                btnGenereDoc2.attr('style', 'display:none');
            }

        } else {
            btnGenereDoc.attr('style', 'display:none');
            btnGenereDoc2.attr('style', 'display:none');
        }

    } else {
        btnGenereDoc.attr('style', 'display:none');
        btnGenereDoc2.attr('style', 'display:none');
    }


    btnGenereDoc2.attr('style', 'display:none');

    body += '</tbody>';

    body += '<tfoot>';
    body += '<tr><th colspan="3" style="text-align:right;font-size:14px;vertical-align:middle">RESUME REALISATION :</th><th style="text-align:right;font-size:15px;color:red"></th></tr>';
    body += '</tfoot>';

    var tableContent = header + body;
    tableTicketPeage.html(tableContent);
    tableTicketPeage.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste ici ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 100,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;
            $(api.column(3).footer()).html(
                    'TOTAL EN CDF : ' + formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    'TOTAL USD : ' + formatNumber(sumUSD, 'USD') +
                    '<hr/>' +
                    'TOTAL FREQUENCE : ' + frequence
                    );

        }
    });
}

function printData() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    if (data == null) {
        docTitle = 'REGISTRE DE PRODUCTION DES TICKETS';
        position = 450;
    } else {
        if (data.isAdvanced == '0') {
            var critere = data.critere.toUpperCase();
            if (data.typeSearch == '1') {
                docTitle = 'REGISTRE DES TICKETS DE LA PLAQUE N° ' + critere;
                position = 300;
            } else {
                docTitle = 'REGISTRE DU TICKET N° ' + critere;
                position = 315;
            }
        } else {
            docTitle = 'REGISTRE DE PRODUCTION DES TICKETS ';
            if (data.siteCode == '*' && data.type == '*') {
                docTitle += '(' + data.dateDebut + ' au ' + data.dateFin + ')';
                position = 300;
            } else if (data.siteCode != '*' && data.type == '*') {
                docTitle += '(' + data.dateDebut + ' au ' + data.dateFin + ')';
                docTitle += '\nSITE : ' + $('#selectSitePeage option:selected').text();
                position = 300;
                hauteur = 160;
            } else if (data.type != '*' && data.siteCode == '*') {
                docTitle += '(' + data.dateDebut + ' au ' + data.dateFin + ')';
                docTitle += '\nTYPE : ' + $('#selectTypePaiement option:selected').text();
                position = 300;
                hauteur = 160;
            } else {
                docTitle += '(' + data.dateDebut + ' au ' + data.dateFin + ')';
                docTitle += '\nSite : ' + $('#selectSitePeage option:selected').text();
                docTitle += '\tType : ' + $('#selectTypePaiement option:selected').text();
                position = 300;
                hauteur = 160;
            }
        }

    }

    var columns = [
        {title: "TICKET", dataKey: "code"},
        {title: "VEHICULE", dataKey: "bien"},
        {title: "TAXE", dataKey: "taxe"},
        {title: "MONTANT", dataKey: "montant"},
        {title: "AGENT", dataKey: "agentCreate"}];

    var rows = _ticketList;

    var pageContent = function (data) {

        doc.setFontSize(10);
        doc.text("REPUBLIQUE DEMOCRATIQUE DU CONGO", 40, 25);
        doc.setFontSize(9);
        doc.text("PROVINCE DU HAUT-KATANGA", 40, 40);
        doc.setFontSize(10);
        doc.text(docTitle, position, 140);

        doc.setFontSize(8);
        doc.text("Imprimé le " + day + '/' + month + '/' + year + ' à ' + hh + ':' + mm + ':' + ss + ' avec E-RECETTES 1.0', 590, 25);
        doc.text("Par : " + userData.nomComplet, 590, 35);
        doc.text("Page : " + data.pageCount, 590, 45);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: {
            siteIntitule: {columnWidth: 80, fontSize: 8, overflow: 'linebreak'},
            idDemande: {columnWidth: 80, overflow: 'linebreak', fontSize: 7},
            numeroTransaction: {columnWidth: 60, overflow: 'linebreak', fontSize: 7},
            typeDocument: {columnWidth: 180, overflow: 'linebreak', fontSize: 9},
            intituleService: {columnWidth: 100, overflow: 'linebreak', fontSize: 9},
            nomCompletBeneficiaire: {columnWidth: 150, overflow: 'linebreak', fontSize: 8},
            dateCreationDemande: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            montantDevisePaye: {columnWidth: 60, overflow: 'linebreak', fontSize: 8}
        },
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 10 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');
    doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(sumCDF), 550, finalY + 40);
    doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD), 550, finalY + 60);
    doc.text("TOTAL FREQUENCE :  " + frequence, 550, finalY + 80);
    window.open(doc.output('bloburl'), '_blank');
}

function printRapportProduction() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'printRapportProduction',
            'userID': userData.idUser,
            'percepeurCode': percepeurCode,
            'percepeurName': inputPercepteur.val(),
            'controleurCode': selectControleur.val(),
            'controleurName': $('#selectControleur option:selected').text().toUpperCase(),
            'poste': inputPostePeage.val(),
            'axe': inputAxePeage.val(),
            'typeRapport': selectTypePaiement.val(),
            'observation': txtObservation.val(),
            'data': JSON.stringify(tempTicketList)

        },
        beforeSend: function () {

            modalPrintRapportProductionPeage.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression du rapport en cours...</h5>'});

        },
        success: function (response)
        {

            modalPrintRapportProductionPeage.unblock();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;

            } else {
                setDocumentContent(response);
                window.open('document-paysage', '_blank');
                txtObservation.val(empty);
                selectControleur.val(empty);
                inputPostePeage.val(empty);
                inputAxePeage.val(empty);
                tempTicketList = [];

                modalPrintRapportProductionPeage.modal('hide');
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalPrintRapportProductionPeage.unblock();
            showResponseError();
        }
    });
}

function loadControleurs() {

    var codeSite = '';

    if (selectSitePeage.val()) {

        codeSite = selectSitePeage.val();
    } else {
        codeSite = userData.SiteCode;
    }

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadControleurs',
            'site': codeSite
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                showResponseError();
                return;

            } else {

                var controleurs = JSON.parse(JSON.stringify(response));
                var dataControleur = '';

                dataControleur += '<option value="0">--</option>';

                for (var i = 0; i < controleurs.length; i++) {
                    dataControleur += '<option value="' + controleurs[i].contoleurId + '">' + controleurs[i].contoleurName + '</option>';
                }
                selectControleur.html(dataControleur);
                selectControleur.val("0");
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function callModalCancelledTicket(code) {

    alertify.confirm('Etes-vous de vouloir annuler le ticket n° ' + '<span style="font-weight:bold">' + code + '</span>' + ' ?', function () {
        codeTicketSelected = code;
        modalUploadTicket.modal('show');
    });

}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }

    canceledTicket();
}

function canceledTicket() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'canceledTicket',
            'userID': userData.idUser,
            'code': codeTicketSelected,
            'observation': textObservDocTicket.val(),
            'archive': listArchivesDocument

        },
        beforeSend: function () {

            modalUploadTicket.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Annulation ticket en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                modalUploadTicket.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalUploadTicket.unblock();

                if (response == '1') {

                    alertify.alert('L\'annulation du ticket s\'est effectuée avec succès.');

                    textObservDocTicket.val(empty);
                    document.getElementById('imgDocument').src = empty;
                    lblNbDocumentEchelonnement.text('');
                    archivesMep = empty;
                    codeTicketSelected = empty;
                    listArchivesDocument = empty;

                    modalUploadTicket.modal('hide');

                    if (isAdvanced === '0') {

                        btnResearchTaxation.trigger('click');
                    } else {
                        btnAdvencedSearch.trigger('click');
                    }
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalUploadTicket.unblock();
            showResponseError();
        }
    });
}