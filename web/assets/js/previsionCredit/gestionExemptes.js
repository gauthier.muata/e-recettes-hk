/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var lblTypeAssujet, lblTypeAss, lblLegalFormPrevisionCredit;

var lblNomAssujet, lblNameAssujet;

var lblArticleBudg, cmbArticleBudgetaire;

var lblAddresseAssujet, lblAdress;

var codePrePaiementettis, codeTypeAssujetti, codeAdresse;

var btnSearchAssujettis, btnValiderPrevisionCredit;

var tableBiens, ModalAssujettiPrevisionCredit;
var bienList;

var btnAllValidateExempt, btnAllCanceledExempt;
var tempList = [];


$(function () {

    mainNavigationLabel.text('PEAGE');
    secondNavigationLabel.text('Registre des exemptés');

    removeActiveMenu();
    linkSubMenuRegistreSousProvision.addClass('active');

    btnSearchAssujettis = $('#btnSearchAssujettis');
    btnValiderPrevisionCredit = $('#btnValiderPrevisionCredit');

    tableBiens = $('#tableBiens');
    ModalAssujettiPrevisionCredit = $('#ModalAssujettiPrevisionCredit');

    lblLegalFormPrevisionCredit = $('#lblLegalFormPrevisionCredit');

    lblTypeAssujet = $('#lblTypeAssujet');
    lblTypeAss = $('#lblTypeAss');

    lblAddresseAssujet = $('#lblAddresseAssujet');
    lblAdress = $('#lblAdress');

    lblNameAssujet = $('#lblNameAssujet');
    lblNomAssujet = $('#lblNomAssujet');
    btnAllValidateExempt = $('#btnAllValidateExempt');
    btnAllCanceledExempt = $('#btnAllCanceledExempt');

    btnSearchAssujettis.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        ModalAssujettiPrevisionCredit.modal('show');
    });

    btnAllCanceledExempt.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        desactivateAllExemption();
    });

    btnAllValidateExempt.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        activateAllExemption();
    });

});


function getSelectedAssujetiData() {
    lblNameAssujet.html(selectAssujettiData.nomComplet);
    lblTypeAssujet.html(selectAssujettiData.categorie);
    lblAddresseAssujet.html(selectAssujettiData.adresse);
    codeAssujettis = selectAssujettiData.code;
    codeTypeAssujetti = selectAssujettiData.codeForme;

    lblTypeAss.show();
    lblNomAssujet.show();
    lblAdress.show();

    loadBiensAssujettiExempte(codeAssujettis);

}

function loadBiensAssujettiExempte(codePersonne) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBiensAssujettiExempte',
            'codePersonne': codePersonne
        },
        beforeSend: function () {
        },
        success: function (response)
        {
            if (response == '-1') {
                showResponseError();
                return;
            }

            bienList = $.parseJSON(JSON.stringify(response));
            printBienAssujetti(bienList);


        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            showResponseError();
        }
    });
}

function printBienAssujetti(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:20%">BIEN  (AUTOMOBILE)</th>';
    header += '<th style="width:30%">DESCRIPTION DU BIEN </th>';
    header += '<th style="width:20%">TARIF</th>';
    header += '<th style="width:10%;text-align:left"> ETAT </th>';
    header += '<th style="width:10%;text-align:center"></th>';
    header += '<th hidden="true" scope="col">ID</th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    btnAllCanceledExempt.attr('style', 'display:none');
    btnAllCanceledExempt.attr('style', 'display:none');

    for (var i = 0; i < result.length; i++) {

        var obj = new Object();

        obj.id = result[i].id;
        //obj.bien = result[i].bienId;

        tempList.push(obj);

        var bienName = result[i].bienName + ' (' + result[i].bienType + ')';
        var plaqueInfo = 'Numéro plaque ou chassis: ' + '<span style="font-weight:bold">' + result[i].bienPlaque + '</span>'
        var bienNameInfo = bienName + '<hr/>' + plaqueInfo;

        var tarifInfo = result[i].tarifName + '<br/>' + '<span style="font-weight:bold">' + formatNumber(result[i].taux, result[i].devise) + '</span>';

        var btn = empty;

        if (result[i].bienStateId == 1) {
            btnAllValidateExempt.attr('style', 'display:inline');
            btn = '<button type="button" onclick="activateExemption(\'' + result[i].id + '\')" class="btn btn-success"><i class="fa fa-check-circle"></i>&nbsp;Exempter</button>';
        } else if (result[i].bienStateId == 2) {
            btnAllCanceledExempt.attr('style', 'display:inline');
            btn = '<button type="button" onclick="desactivateExemption(\'' + result[i].id + '\')" class="btn btn-danger"><i class="fa fa-close"></i>&nbsp;Annuler</button>';
        }

        if (result.length === 1) {
            btnAllCanceledExempt.attr('style', 'display:none');
            btnAllValidateExempt.attr('style', 'display:none');
        }

        var color = empty;

        if (result[i].bienStateId == 1) {
            color = 'black';
        } else {
            color = 'green';
        }

        body += '<tr>';
        body += '<td style="text-align:left;width:20%;vertical-align:middle">' + bienNameInfo + '</td>';
        body += '<td style="text-align:left;width:30%;vertical-align:middle">' + result[i].descriptionBien + '</td>';
        body += '<td style="text-align:left;width:20%;vertical-align:middle">' + tarifInfo + '</td>';
        body += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:bold;color:' + color + '">' + result[i].bienStateName + '</td>';
        body += '<td style="text-align:center;width:10%;vertical-align:middle">' + btn + '</td>';
        body += '<td hidden="true">' + result[i].id + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableBiens.html(tableContent);
    tableBiens.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 20,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}


function activateExemption(id) {

    alertify.confirm('Etes-vous de vouloir exempter ce bien ?', function () {

        $.ajax({
            type: 'POST',
            url: 'peage_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'activateExemption',
                'userID': userData.idUser,
                'id': id

            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>L\'opération est en cours...</h5>'});

            },
            success: function (response)
            {

                if (response == '-1' || response == '0') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    $.unblockUI();

                    if (response == '1') {
                        alertify.alert('L\'opération s\'est effectuée avec succès.');
                        loadBiensAssujettiExempte(codeAssujettis);
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });
}

function desactivateExemption(id) {

    alertify.confirm('Etes-vous de vouloir annuler l\'exemption de ce bien ?', function () {

        $.ajax({
            type: 'POST',
            url: 'peage_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'desactivateExemption',
                'userID': userData.idUser,
                'id': id

            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>L\'opération est en cours...</h5>'});

            },
            success: function (response)
            {

                if (response == '-1' || response == '0') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    $.unblockUI();

                    if (response == '1') {
                        alertify.alert('L\'opération s\'est effectuée avec succès.');
                        loadBiensAssujettiExempte(codeAssujettis);
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });

}

function activateAllExemption() {

    alertify.confirm('Etes-vous de vouloir exempter tout ce(s) bien(s) ?', function () {

        $.ajax({
            type: 'POST',
            url: 'peage_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'activateAllExemption',
                'userID': userData.idUser,
                'list': JSON.stringify(tempList)

            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>L\'opération est en cours...</h5>'});

            },
            success: function (response)
            {

                if (response == '-1' || response == '0') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    $.unblockUI();

                    if (response == '1') {
                        alertify.alert('L\'opération s\'est effectuée avec succès.');
                        loadBiensAssujettiExempte(codeAssujettis);
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });
}

function desactivateAllExemption() {

    alertify.confirm('Etes-vous de vouloir annuler l\'exemption de tout ce(s) bien(s) ?', function () {

        $.ajax({
            type: 'POST',
            url: 'peage_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'desactivateAllExemption',
                'userID': userData.idUser,
                'list': JSON.stringify(tempList)

            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>L\'opération est en cours...</h5>'});

            },
            success: function (response)
            {

                if (response == '-1' || response == '0') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    $.unblockUI();

                    if (response == '1') {
                        alertify.alert('L\'opération s\'est effectuée avec succès.');
                        loadBiensAssujettiExempte(codeAssujettis);
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });

}


