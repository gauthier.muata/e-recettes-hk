/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var lblTypeAssujet, lblTypeAss, lblLegalFormPrevisionCredit;

var lblNomAssujet, lblNameAssujet;

var lblArticleBudg, cmbArticleBudgetaire;

var lblAddresseAssujet, lblAdress;

var codeAssujettis, codeTypeAssujetti, codeAdresse;

var codeArticleBudg, codeTypePrevision, codeDevise;

var nbrToure, inputMontantPercu;

var cmbTypePrevisionCredit, cmbDevise;

var btnSearchAssujettis, btnValiderPrevisionCredit;

var tablePrevisionCredit, ModalAssujettiPrevisionCredit;

var dataDetailsPrevisionCredit, tempBienList;

var assujettissementList;

var detailAssjListTampo = [], detailAssujList = [];

var modalDetailProvisionCredit, spnBienSelected, spnTarifBienSelected, selectTypePaiement, inputNbreTour, inputTotalAPayer, selectDevise, btnSaveDetailProvisionCredit;
var codeSelected,tauxSelected;

$(function () {

    mainNavigationLabel.text('PEAGE');
    secondNavigationLabel.text('Gestion des prévision et des crédits');

    removeActiveMenu();
    linkSubMenuGestionPrevisionCredit.addClass('active');

    btnSearchAssujettis = $('#btnSearchAssujettis');
    btnValiderPrevisionCredit = $('#btnValiderPrevisionCredit');

    tablePrevisionCredit = $('#tablePrevisionCredit');
    ModalAssujettiPrevisionCredit = $('#ModalAssujettiPrevisionCredit');

    cmbTypePrevisionCredit = $('#cmbTypePrevisionCredit');
    cmbDevise = $('#cmbDevise');

    modalDetailProvisionCredit = $('#modalDetailProvisionCredit');
    spnBienSelected = $('#spnBienSelected');
    spnTarifBienSelected = $('#spnTarifBienSelected');
    selectTypePaiement = $('#selectTypePaiement');
    inputNbreTour = $('#inputNbreTour');
    inputTotalAPayer = $('#inputTotalAPayer');
    selectDevise = $('#selectDevise');
    btnSaveDetailProvisionCredit = $('#btnSaveDetailProvisionCredit');

    lblLegalFormPrevisionCredit = $('#lblLegalFormPrevisionCredit');

    lblTypeAssujet = $('#lblTypeAssujet');
    lblTypeAss = $('#lblTypeAss');

    cmbArticleBudgetaire = $('#cmbArticleBudgetaire');

    inputMontantPercu = $('#inputMontantPercu');
    nbrToure = $('#nbrToure');

    lblAddresseAssujet = $('#lblAddresseAssujet');
    lblAdress = $('#lblAdress');

    lblNameAssujet = $('#lblNameAssujet');
    lblNomAssujet = $('#lblNomAssujet');

    //codeTypePrevision = cmbTypePrevisionCredit.val();
    //codeDevise = cmbDevise.val();

    btnSearchAssujettis.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        ModalAssujettiPrevisionCredit.modal('show');
    });

    btnSaveDetailProvisionCredit.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        checkFields();
    });

    btnValiderPrevisionCredit.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var typePaidExist = false;

        for (var i = 0; i < detailAssjListTampo.length; i++) {

            if (detailAssjListTampo[i].typePaidExist == "1") {
                typePaidExist = true;
            }
        }

        if (typePaidExist === false) {

            alertify.alert('Veuillez d\'abord définir le type de paiement pour au moins un bien avant de valder cette opération');
            return;
        }

        alertify.confirm('Etes-vous de vouloir valider cette opération ?', function () {
            savePrevisionCredit();
        });

    });

    printAssujettissement(empty);

});


function printDetailsPrevisionCredit(bienList) {

    tempBienList = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" style="width:20%"> BIEN  (AUTOMOBILE)</th>';
    header += '<th scope="col" style="width:30%"> DESCRIPTION DU BIEN </th>';
    header += '<th scope="col" style="width:30%"> ADRESSE </th>';
    header += '<th scope="col" style="width:10%;text-align:right"> TAUX </th>';
    header += '<th ></th>';
    header += '<th hidden="true" scope="col">Id bien</th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < bienList.length; i++) {

        body += '<tr>';
        body += '<td>' + bienList[i].intituleBien + '</td>';
        body += '<td>' + bienList[i].descriptionBien + '</td>';
        body += '<td>' + bienList[i].chaineAdresse.toUpperCase() + '</td>';
        body += '<td style="text-align:right;font-weight:bold">' + formatNumber(bienList[i].valeur, bienList[i].devise) + '</td>';
        body += '<td style="text-align:center"><center><input type="checkbox" id="checkBoxSelectBienPrevCred' + bienList[i].codebien + '" name="checkBoxSelectBienPrevCred' + bienList[i].codebien + '"></center></td>';
        body += '<td hidden="true">' + bienList[i].codebien + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tablePrevisionCredit.html(tableContent);
    tablePrevisionCredit.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 3,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function printAssujettissement(result) {

    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>\n\\n\
                <th style="text-align:left"></th>\n\
                <th style="text-align:left">CODE</th>\n\
                <th style="text-aLign:left">TAXE</th>\n\\n\\n\
                <th style="text-aLign:left">DATE CREATION</th>\n\\n\
                <th style="text-aLign:center">ETAT</th>\n\
                </tr></thead>';
    var data = '';
    data += '<tbody id="bodyTable">';


    for (var i = 0; i < result.length; i++) {

        var state = 'ACTIF';
        if (result[i].assujetissementState === 0) {
            state = 'NON ACTIF';
        }

        data += '<tr>';
        data += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        data += '<td  style="text-align:left;width:10%;vertical-align:middle">' + result[i].assujetissementCode + '</td>';
        data += '<td style="text-align:left;width:30%;vertical-align:middle">' + result[i].intituleAB + '</td>';
        data += '<td style="text-align:left;width:15%;vertical-align:middle">' + result[i].assujetissementDate + '</td>';
        data += '<td style="text-align:center;width:10%;vertical-align:middle">' + state + '</td>';
        data += '</tr>';
    }
    data += '</tbody>';

    var TableContent = header + data;

    tablePrevisionCredit.html(TableContent);

    var datatable = tablePrevisionCredit.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 6,
        lengthMenu: [[7, 25, 50, -1], [7, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        lengthChange: false,
        select: {
            style: 'os',
            blurable: true
        }
    });

    $('#tablePrevisionCredit tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');

        var row = datatable.row(tr);
        var dataDetail = datatable.row(tr).data();
        var codeAssuj = dataDetail[1];

        var tableDetailAssuj = '';

        console.log(row.child.isShown());

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');

        } else {

            tableDetailAssuj = '<center><h4>LES DETAILS DE L\'ASSUJETTISSEMENT N° : ' + codeAssuj + '</h4><br/></center><table class="table table-bordered">';
            tableDetailAssuj += '<thead><tr style="background-color:#e6ceac;color:black"><td hidden="true">BIENID</td><td>BIEN (AUTOMOBILE)</td><td>DESCRIPTION</td><td>TARIF</td><td>INFORMATIONS SOUSCRIPTIONS</td><td style="text-align:center"></td></tr></thead>';

            for (var j = 0; j < assujettissementList.length; j++) {

                if (assujettissementList[j].assujetissementCode == codeAssuj) {

                    detailAssujList = JSON.parse(assujettissementList[j].detailAssujettissementList);

                    for (var i = 0; i < detailAssujList.length; i++) {

                        var obj = {};

                        obj.bienId = detailAssujList[i].bienCode;
                        obj.taux = detailAssujList[i].taux;
                        obj.devise = detailAssujList[i].devise;
                        obj.numberTour = 0;
                        obj.totalPaid = 0;
                        obj.typePaidExist = 0;
                        obj.type = empty;
                        obj.detailAssujettissementId = detailAssujList[i].detailAssujId;

                        detailAssjListTampo.push(obj);

                        var bienName = detailAssujList[i].bienName + ' (' + detailAssujList[i].typeBien + ')';
                        var plaqueInfo = 'Numéro plaque ou chassis: ' + '<span style="font-weight:bold">' + detailAssujList[i].bienPlaque + '</span>'

                        var bienNameInfo = bienName + '<br/>' + plaqueInfo;

                        var amount = formatNumber(detailAssujList[i].taux, detailAssujList[i].devise);

                        var btn = '<button type="button" onclick="displayModalTypePayment(\'' + detailAssujList[i].bienCode + '\',\'' + bienName + '\',\'' + amount + '\',\'' + detailAssujList[i].tarifName + '\',\'' + detailAssujList[i].devise + '\',\'' + detailAssujList[i].taux + '\')" class="btn btn-warning"><i class="fa fa-list"></i></button>';

                        tableDetailAssuj += '<tr>';
                        tableDetailAssuj += '<td hidden="true">' + detailAssujList[i].bienCode + '</td>';
                        tableDetailAssuj += '<td style="text-align:left;width:20%;vertical-align:middle">' + bienNameInfo + '</td>';
                        tableDetailAssuj += '<td style="text-align:left;width:20%;vertical-align:middle">' + detailAssujList[i].bienDescription + '</td>';
                        tableDetailAssuj += '<td style="text-align:left;width:20%;vertical-align:middle">' + detailAssujList[i].tarifName + '<br/>' + '<span style="font-weight:bold">' + amount + '</span>' + '</td>';
                        tableDetailAssuj += '<td style="text-align:left;width:35%;vertical-align:middle"><span id="lblInfoPaiement_' + detailAssujList[i].bienCode + '">' + '</span></td>';
                        tableDetailAssuj += '<td style="text-align:center;width:5%;vertical-align:middle">' + btn + '</td>';
                        tableDetailAssuj += '</tr>';
                    }

                }
            }

            tableDetailAssuj += '<tbody>';

            tableDetailAssuj += '</tbody>';
            row.child(tableDetailAssuj).show();
            tr.addClass('shown');


        }

    });

}

function getSelectedAssujetiData() {
    lblNameAssujet.html(selectAssujettiData.nomComplet);
    lblTypeAssujet.html(selectAssujettiData.categorie);
    lblAddresseAssujet.html(selectAssujettiData.adresse);
    codeAssujettis = selectAssujettiData.code;
    codeTypeAssujetti = selectAssujettiData.codeForme;

    lblTypeAss.show();
    lblNomAssujet.show();
    lblAdress.show();

    loadArticleBudgetaireV2(codeAssujettis);

}

function loadArticleBudgetaire(codeAssujetti) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadArtcleOfAssujetti',
            'codePersonne': codeAssujetti
        },
        beforeSend: function () {
        },
        success: function (response)
        {
            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {

                var result = $.parseJSON(JSON.stringify(response));

                var dataArticleBudgetaire = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < result.length; i++) {

                    dataArticleBudgetaire += '<option value =' + result[i].codeAB + '>' + result[i].intituleAB.toUpperCase() + '</option>';
                }

                cmbArticleBudgetaire.html(dataArticleBudgetaire);

            }
            , 1000);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            showResponseError();
        }
    });
}

function loadArticleBudgetaireV2(codeAssujetti) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadArtcleOfAssujettiV2',
            'codePersonne': codeAssujetti
        },
        beforeSend: function () {
        },
        success: function (response)
        {
            if (response == '-1') {
                showResponseError();
                return;
            }

            assujettissementList = $.parseJSON(JSON.stringify(response));
            printAssujettissement(assujettissementList);


        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            showResponseError();
        }
    });
}

function getDetailsAssujettissement(codeArticle) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codeAB': codeArticle,
            'codePersonne': codeAssujettis,
            'operation': 'getDetailsAssujettissement'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {
                $.unblockUI()();
                alertify.alert('Aucune données trouvées.');
                return;
            } else {
                setTimeout(function () {

                    $.unblockUI();

                    dataDetailsPrevisionCredit = JSON.parse(JSON.stringify(response));

                    if (dataDetailsPrevisionCredit.length > 0) {
                        printDetailsPrevisionCredit(dataDetailsPrevisionCredit);
                    }

                }
                , 1);
            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
//            $.unblockUI();
            showResponseError();
        }

    });
}

function preparerDetailsPrevisionCredit() {

    detailPrevisionCreditList = [];

    for (var i = 0; i < dataDetailsPrevisionCredit.length; i++) {

        var selectDetailPrevision = '#checkBoxSelectBienPrevCred' + dataDetailsPrevisionCredit[i].codebien;


        var select = $(selectDetailPrevision);

        if (select.is(':checked')) {

            var detailsPrevision = {};

            detailsPrevision.codeBien = dataDetailsPrevisionCredit[i].codebien;
            detailsPrevision.valeur = dataDetailsPrevisionCredit[i].valeur;
            detailsPrevision.devise = dataDetailsPrevisionCredit[i].devise;
            detailsPrevision.codeDetailAssujettisement = dataDetailsPrevisionCredit[i].codeDetailAssujettisement;


            detailPrevisionCreditList.push(detailsPrevision);

        }

    }

    detailPrevisionCreditList = JSON.stringify(detailPrevisionCreditList);

    return detailPrevisionCreditList;
}

function savePrevisionCredit() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'savePrevisionCredit',
            'userID': userData.idUser,
            'codePersonne': codeAssujettis,
            'detailPrevisionCreditList': JSON.stringify(detailAssjListTampo)

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de type paiement en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'enregistrement de type paiement s\'est effectué avec succès.');
                    loadArticleBudgetaireV2(codeAssujettis);
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function cleanData() {

    printDetailsPrevisionCredit('');
    cmbArticleBudgetaire.html('');
    inputMontantPercu.val('');
    nbrToure.val('');

}

function displayModalTypePayment(code, name, amount, tarif, devise,amount2) {

    codeSelected = code;
    tauxSelected = parseFloat(amount2);
    
    spnBienSelected.html(name);
    spnTarifBienSelected.html(tarif + ' : ' + amount);
    selectTypePaiement.val('0');
    inputNbreTour.val(1);
    inputTotalAPayer.val(empty);
    selectDevise.val(devise);
    modalDetailProvisionCredit.modal('show');


}

function checkFields() {

    if (selectTypePaiement.val() === '0') {

        alertify.alert('Veuillez d\'abord sélectionner un type paiement');
        return;
    }

    if (inputNbreTour.val() == empty) {

        alertify.alert('Veuillez d\'abord saisir le nombre de tour à souscrire');
        return;
    } else if (inputNbreTour.val() < 0) {
        alertify.alert('Le nombre de tour à souscrire ne doit pas être inférieur à zéro');
        return;
    }

    if (inputTotalAPayer.val() == empty) {

        alertify.alert('Veuillez d\'abord saisir le montant total à payer de nombre de tour souscrit');
        return;
    } else if (inputTotalAPayer.val() < 0) {
        alertify.alert('Le montant total à payer de nombre de tour souscrit ne doit pas être inférieur à zéro');
        return;
    }

    var newTotalPaid = parseFloat(tauxSelected * inputNbreTour.val());

    if (inputTotalAPayer.val() > newTotalPaid) {

        alertify.alert('Le montant total à payer de nombre de tour souscrit ne pas correct');
        return;
    }

    alertify.confirm('Etes-vous de vouloir confirmer ces informations ?', function () {

        for (var i = 0; i < detailAssjListTampo.length; i++) {

            if (detailAssjListTampo[i].bienId === codeSelected) {

                detailAssjListTampo[i].numberTour = inputNbreTour.val();
                detailAssjListTampo[i].totalPaid = inputTotalAPayer.val();
                detailAssjListTampo[i].type = selectTypePaiement.val();
                detailAssjListTampo[i].typePaidExist = 1;

                var idColumnLblInfoPaiement = $("#lblInfoPaiement_" + codeSelected);

                var typePayInfo = 'Type paiement : ' + '<span style="font-weight : bold">' + $('#selectTypePaiement option:selected').text().toUpperCase() + '</span>';
                var numberTourInfo = 'Nombre de tour souscrit : ' + '<span style="font-weight : bold">' + inputNbreTour.val() + '</span>';
                var totalAmountTourInfo = 'Montant total à payer : ' + '<span style="font-weight : bold">' + formatNumber(inputTotalAPayer.val(), selectDevise.val()) + '</span>';

                var paymentInfo = typePayInfo + '<br/>' + numberTourInfo + '<br/>' + totalAmountTourInfo;
                idColumnLblInfoPaiement.html(paymentInfo);

                modalDetailProvisionCredit.modal('hide');

                return;
            }
        }



    });
}


