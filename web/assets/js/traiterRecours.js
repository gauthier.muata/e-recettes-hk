/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var btnCallModalSearchAssujettis, btnValiderTraitement;

var assujettiModal, idReclamation;

var lblNameResponsible, lblAddress, lblNifResponsible, lblLegalForm;

var lbl1, lbl2, lbl3, lbl4;

var divMontantDu, divRestPayer;

var lblMontantDu, lblRestePayer, lblPartieMontantConteste, lblCinqPourcentPartieConteste;

var selectDecision, inputDegreveAmount, stateTraitement, formRadioButton;

var tableRecours, modalTraitementJuridictinnel;

var tempRecoursLists = [];

$(function () {

    mainNavigationLabel.text('CONTENTIEUX');
    secondNavigationLabel.text('Traitement recours juridictionnel');

    btnCallModalSearchAssujettis = $('#btnCallModalSearchAssujettis');

    btnValiderTraitement = $('#btnValiderTraitement');

    assujettiModal = $('#assujettiModal');

    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    lblNifResponsible = $('#lblNifResponsible');
    lblLegalForm = $('#lblLegalForm');

    tableRecours = $('#tableRecours');

    modalTraitementJuridictinnel = $('#modalTraitementJuridictinnel');

    selectDecision = $('#selectDecision');

    formRadioButton = $('#formRadioButton');

    divMontantDu = $('#divMontantDu');
    divRestPayer = $('#divRestPayer');

    inputDegreveAmount = $('#inputDegreveAmount');

    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');
    lbl3 = $('#lbl3');
    lbl4 = $('#lbl4');


    btnCallModalSearchAssujettis.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        assujettiModal.modal('show');

    });

    selectDecision.on('change', function () {

        inputDegreveAmount.val('');

        if (selectDecision.val() == '1') {

            inputDegreveAmount.removeAttr('disabled');
            formRadioButton.attr('style', 'display:none');
            divMontantDu.attr('style', 'display:inline');
            divRestPayer.attr('style', 'display:inline');
        }
        else if (selectDecision.val() == '2') {

            inputDegreveAmount.attr('disabled', true);
            formRadioButton.attr('style', 'display:none');
            divMontantDu.attr('style', 'display:none');
            divRestPayer.attr('style', 'display:none');
        } else if (selectDecision.val() == '3') {

            inputDegreveAmount.attr('disabled', true);
            formRadioButton.attr('style', 'display:none');
            divMontantDu.attr('style', 'display:inline');
            divRestPayer.attr('style', 'display:inline');
        } else {
            formRadioButton.attr('style', 'display:inline');
            inputDegreveAmount.attr('disabled', true);
            divMontantDu.attr('style', 'display:none');
            divRestPayer.attr('style', 'display:none');

            var checkControlRadio = $("#radioDegreveTotal");

            checkControlRadio.change(function (e) {
                e.preventDefault();

                inputDegreveAmount.attr('disabled', true);
                inputDegreveAmount.val('');
                divMontantDu.attr('style', 'display:none');
                divRestPayer.attr('style', 'display:none');
            });

            var checkControlRadioPartiel = $("#radioDegrevePartiel");
            checkControlRadioPartiel.change(function (e) {
                e.preventDefault();

                inputDegreveAmount.removeAttr('disabled');
                inputDegreveAmount.val('');
                divMontantDu.attr('style', 'display:inline');
                divRestPayer.attr('style', 'display:inline');
            });
        }

    });

    $('input[name=radioTraiter]').on('change', function (e) {
        stateTraitement = $('input[name=radioTraiter]:checked').val();
    });


    printRecours('');

});

function getSelectedAssujetiData() {
    lblNameResponsible.html(selectAssujettiData.nomComplet);
    lblLegalForm.html(selectAssujettiData.categorie);
    lblAddress.html(selectAssujettiData.adresse);
    codeResponsible = selectAssujettiData.code;
    lblNifResponsible.html(selectAssujettiData.nif);
    codeFormeJuridique = selectAssujettiData.codeForme;
    adresseId = selectAssujettiData.codeAdresse;

    lbl1.show();
    lbl2.show();
    lbl3.show();
    lbl4.show();

    loadRecours();
}

function printRecours(recoursList) {

    tempRecoursLists = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col">TYPE RECOURS</th>';
    header += '<th scope="col">NUMERO COURRIER</th>';
    header += '<th scope="col">ASSUJETTI</th>';
    header += '<th scope="col">TYPE ASSUJETTI</th>';
    header += '<th scope="col">DATE CREATION</th>';
    header += '<th scope="col">DATE DEPOT RECOURS</th>';
    header += '<th scope="col">DECISION ADMIN</th>';
    header += '<th scope="col">ETAT</th>';
    header += '<th></th>';
    header += '</tr></thead>';

    var body = '<tbody >';

    for (var i = 0; i < recoursList.length; i++) {

        var type, etat;
        var action;
        var printBP;

        type = 'recouvrement';

        action = '<a onclick="showModalTraitementRecours(\'' + recoursList[i].recoursId + '\')" class="btn btn-success" title="traitement"><i class="fa fa-check-circle"></i></a>';
        printBP = '<a onclick="printBonAPayer(\'' + recoursList[i].codeAmr + '\')" class="btn btn-warning" title="impression"><i class="fa fa-print"></i></a>';

        switch (recoursList[i].etat) {
            case 3:
                etat = 'REJETE';
                break;
            case 1:
                etat = 'EN ATTENTE DE TRAITEMENT';
                break;
            case 2:
                etat = 'TRAITE';
                break;
        }

        var recours = {};
        recours.recoursId = recoursList[i].recoursId;
        recours.assujettiCode = recoursList[i].assujettiCode;
        recours.assujettiName = recoursList[i].assujettiName;
        recours.legalFormName = recoursList[i].legalFormName;
        recours.adresseName = recoursList[i].adresseName;
        recours.dateCreate = recoursList[i].dateCreate;
        recours.dateReceptionCourrier = recoursList[i].dateReceptionCourrier;
        recours.etat = recoursList[i].etat;
        recours.numeroEnregistrementGreffe = recoursList[i].numeroEnregistrementGreffe;
        recours.dateAudience = recoursList[i].dateAudience;
        recours.dateDepotRecours = recoursList[i].dateDepotRecours;
        recours.decisionAdministrative = recoursList[i].decisionAdministrative;
        recours.detailsReclamation = recoursList[i].detailsReclamation;
        assujettiCode = recoursList[i].assujettiCode;
        idReclamation = recoursList[i].reclamationId;

        tempRecoursLists.push(recours);

        body += '<tr>';
        body += '<td style="text-align:left;width:12%;vertical-align:middle;">' + type.toUpperCase() + '</td>';
        body += '<td style="text-align:left;width:10%;vertical-align:middle;">' + recoursList[i].referenceCourrierReclamation + '</td>';
        body += '<td style="text-align:left;width:25%;vertical-align:middle" title="' + recoursList[i].assujettiName + '"><span style="font-weight:bold;">' + recoursList[i].assujettiName + '</span><br/><br/>' + recoursList[i].adresseName + '</td>';
        body += '<td style="text-align:left;width:12%;vertical-align:middle;">' + recoursList[i].legalFormName + '</td>';
        body += '<td style="text-align:left;width:10%;vertical-align:middle;">' + recoursList[i].dateCreate + '</td>';
        body += '<td style="text-align:center;width:12%;vertical-align:middle;">' + recoursList[i].dateDepotRecours + '</td>';
        body += '<td style="text-align:center;width:12%;vertical-align:middle;">' + recoursList[i].decisionAdministrative + '</td>';
        body += '<td style="text-align:left;width:10%;vertical-align:middle;">' + etat + '</td>';

        if (recoursList[i].etat == '1') {
            body += '<td style="text-align:center;width:7%;vertical-align:middle;"><center>' + action + '</center></td>';
            body += '</tr>';
        } else {
            if (recoursList[i].CodeDecisionAdministrative == '1') {
                body += '<td style="text-align:center;width:7%;vertical-align:middle;"><center>' + printBP + '</center></td>';
                body += '</tr>';
            } else {
                body += '<td style="text-align:center;width:9%;vertical-align:middle;"><center></td>';
                body += '</tr>';
            }

        }
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableRecours.html(tableContent);
    tableRecours.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}

function loadRecours() {

    var dateDebut;
    var dateLast;


    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadRecoursJuridictionnel',
            'valueSearch': codeResponsible,
            'typeSearch': 1,
            'dateDebut': dateDebut,
            'dateFin': dateLast,
            'isAdvancedSearch': 0,
            'typeReclamation': 1,
            'TypePage': 1,
            'idUser': userData.idUser
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                printRecours('');
                return;
            }

            setTimeout(function () {
                var allRecoursList = JSON.parse(JSON.stringify(response));
                printRecours(allRecoursList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });

}


function showModalTraitementRecours(recoursId) {

    var documentRef, typeDocument,
            detailReclamationId, typeRemiseGracieuse,
            documentArchive, amountDegre, idDecision;
    var amount, devise, fivePercent, restePayer;
    var degrevValue;
    var montantConteste;
    var libelleAB;

    var lblMontantDu = $('#lblMontantDu');
    var lblRestePayer = $('#lblRestePayer');

    var lblPartieMontantConteste = $('#lblPartieMontantConteste');
    var lblCinqPourcentPartieConteste = $('#lblCinqPourcentPartieConteste');

    var dataDecision = '<option value ="0">-- Sélectionner une décision --</option>'
            + '<option value="1">Dégrèvement partiel</option>'
            + '<option value="2">Dégrèvement total</option>'
            + '<option value="3">Rejet recours</option>'

    selectDecision.html(dataDecision);

    for (var i = 0; i < tempRecoursLists.length; i++) {

        if (tempRecoursLists[i].recoursId == recoursId) {

            var detailsRecours = JSON.parse(tempRecoursLists[i].detailsReclamation);

            amount = detailsRecours[i].amount;
            fivePercent = detailsRecours[i].pourcentageMontantNonContester;
            montantConteste = detailsRecours[i].montantContester;
            codeTypeDocumentSelected = detailsRecours[i].codeAmr;
            libelleAB = detailsRecours[i].libelleArticleBudgetaire;
            typeDocument = detailsRecours[i].typeDocument;
            detailReclamationId = detailsRecours[i].detailReclamationId;
            devise = detailsRecours[i].devise;
            documentArchive = '';
            lblMontantDu.html(formatNumber(amount, devise));
            typeTraitement = 2;

            lblPartieMontantConteste.html(formatNumber(montantConteste, devise));
            lblCinqPourcentPartieConteste.html(formatNumber(fivePercent, devise));

            lblRestePayer.html(formatNumber((parseFloat(amount) - parseFloat(fivePercent)), devise));
            break;
        }
    }

    modalTraitementJuridictinnel.modal('show');

    if (selectDecision.val() == '5') {
        formRadioButton.attr('style', 'display:inline');
    } else {
        formRadioButton.attr('style', 'display:none');
    }

    inputDegreveAmount.change(function () {

        degrevValue = parseFloat(inputDegreveAmount.val());
        restePayer = parseFloat(amount) - parseFloat(montantConteste);

        if (degrevValue >= 0) {

            if (degrevValue > restePayer) {
                alertify.alert('Le montant dégrevé est superieur à ' + formatNumber(restePayer, devise) + '. Veuillez saisir un montant valide.');
                inputDegreveAmount.val('');
                return;
            } else {
                amountDegre = parseFloat(amount) - parseFloat(degrevValue) - parseFloat(fivePercent);
                lblRestePayer.html(formatNumber(amountDegre, devise));
            }

        } else {
            amountDegre = parseFloat(amount) - parseFloat(fivePercent);
            lblRestePayer.html(formatNumber(amountDegre, devise));
        }


    });



    btnValiderTraitement.click(function (e) {
        e.preventDefault();

        var selectedDecision = selectDecision.val();
        var selectState = $('input[name=radioValider]:checked').val();

        if (inputDegreveAmount.val() == '') {
            amountDegre = 0;
        } else {
            degrevValue = parseFloat(inputDegreveAmount.val());
            amountDegre = amount - degrevValue;
        }

        if (parseInt(amountDegre) != 0 && parseInt(inputDegreveAmount.val()) > amount) {
            alertify.alert('Le montant dégrevé est superieur au montant dû. Veuillez saisir un montant valide.');
            return;
        }

        if (selectDecision.val() == '0') {
            alertify.alert('Veuillez sélectionner une décision.');
            return;
        }

        switch (selectedDecision) {
            case "1" :
                if (inputDegreveAmount.val() == '') {
                    alertify.alert('Aucun montant dégrevé est fourni. Veuillez saisir un montant.');
                }
                break;
        }

        switch (selectedDecision) {

            case "1" :
                if (amountDegre == 0) {
                    selectedDecision = '2';
                }
                break;
        }


        alertify.confirm('Etes-vous sûr de vouloir valider ce traitement ?', function () {

            traitementRecours(
                    selectedDecision,
                    typeDocument,
                    detailReclamationId,
                    documentArchive,
                    amountDegre,
                    fivePercent,
                    amount,
                    libelleAB
                    );
        });

    });
}


function traitementRecours(
        idDecision,
        typeDocument,
        detailReclamationId,
        documentArchive,
        amountDegre,
        fivePercent,
        amount,
        libelleAB
        ) {
    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'traitementDecision',
            'decisionId': idDecision,
            'userId': userData.idUser,
            'typeDocument': typeDocument,
            'detailReclamationId': detailReclamationId,
            'archives': documentArchive,
            'amountDegre': amountDegre,
            'amount': amount,
            'pourcentageMontantNonContester': fivePercent,
            'typeTraitement': 2,
            'fkDocument': codeTypeDocumentSelected,
            'codeAssujetti': assujettiCode,
            'reclamationId': idReclamation,
            'libelleArticleBudgetaire': libelleAB

        },
        beforeSend: function () {
            modalTraitementJuridictinnel.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Traitement en cours...</h5>'});
        },
        success: function (response)
        {
            modalTraitementJuridictinnel.unblock();

            if (response == '0') {
                showResponseError();
                return;
            }

            if (response == '1') {
                alertify.alert('Le traitement de la décision s\'est effectué avec success!');

                setTimeout(function () {
                    modalTraitementJuridictinnel.modal('hide');

                    idReclamation = '';
                    stateTraitement = '';

                    getSelectedAssujetiData();
                    
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalTraitementJuridictinnel.unblockUI();
            showResponseError();
        }
    });
}

function printBonAPayer(numeroDocument) {


    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'documentReference': numeroDocument,
            'typeDocument': 'BP',
            'operation': 'printDocument'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();
                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
                setTimeout(function () {
                }, 2000);
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }

    });

}
