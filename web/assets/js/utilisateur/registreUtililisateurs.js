/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var cmbSearchTypeUser, isAdvancedSearch;

var inputSearchUser, selectService, selectSite, selectDivision;

var btnSearchUser, btnCreateUser, btnCallModalSearchUser,
        btnAdvancedSearchUser, btnSaveAffecterBureau;

var codeResearchUser, messageEmptyValueUser, messageAvanceeUser;

var datePickerDebut, datePickerFin;

var tableUser, modalUserAdvancedSearchs;

var tempUserList;

var typeRechercheUser = '*';

var selectSiteVal, ValInputSearchUser;
var idAdvanced;
var userList;
var codeUserSelected, nameUserSelected, profilUser;

var displayModalRightUser, lblUser, tableDroitUser;
var btnAccorderDroit, btnRetirerDroit;
var rightUserList;

var objRightList = [];

var divisionList = [];

var bureauList = [];

var bureauAffecterList = [];

var codeDivisionSelect, selectedCodeUser, divisionSelected;

var tempBureauList;

var checkAffectedUser = false;

var codeUserSelected, stateSelected, nameUserSelected;

var modalTraitementUser,
        inputObservation,
        spnBtnOperation,
        spnNameUser,
        spnFonctionUser,
        btnConfirmeOperation, spnTitleModal;

var selectStatuUser;

$(function () {

    diaplayBtnAddAllRight = false;
    diaplayBtnRemovedAllRight = false;

    mainNavigationLabel.text('UTILISATEUR');
    secondNavigationLabel.text('Registre des utilisateurs');

    removeActiveMenu();
    linkSubMenuRegistreUtilisateurs.addClass('active');

    tableUser = $('#tableUser');

    modalUserAdvancedSearchs = $('#modalUserAdvancedSearchs');

    btnSearchUser = $('#btnSearchUser');
    btnCreateUser = $('#btnCreateUser');
    btnCallModalSearchUser = $('#btnCallModalSearchUser');
    btnAdvancedSearchUser = $('#btnAdvancedSearchUser');

    btnSaveAffecterBureau = $('#btnSaveAffecterBureau');

    cmbSearchTypeUser = $('#cmbSearchTypeUser');
    selectService = $('#selectService');

    selectSite = $('#selectSite');
    selectDivision = $('#selectDivision');

    btnAccorderDroit = $('#btnAccorderDroit');
    btnRetirerDroit = $('#btnRetirerDroit');

    inputSearchUser = $('#inputSearchUser');

    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');

    datePickerDebut = $("#datePicker1");
    datePickerFin = $("#datePicker2");

    modalTraitementUser = $("#modalTraitementUser");
    inputObservation = $("#inputObservation");
    spnBtnOperation = $("#spnBtnOperation");
    spnNameUser = $("#spnNameUser");
    spnFonctionUser = $("#spnFonctionUser");
    btnConfirmeOperation = $("#btnConfirmeOperation");
    spnTitleModal = $("#spnTitleModal");
    selectStatuUser = $("#selectStatuUser");

    inputObservation.val('');

    if (userData.codeFonction == 'F0044') {
        btnCallModalSearchUser.attr('style', 'display:none');
    } else {
        btnCallModalSearchUser.attr('style', 'display:block');
    }

    displayModalRightUser = $("#displayModalRightUser");
    lblUser = $("#lblUser");
    tableDroitUser = $("#tableDroitUser");

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerDebut.datepicker("setDate", new Date());
    datePickerFin.datepicker("setDate", new Date());


    $('.date').datepicker({
        multidate: true
    });

    selectService.on('change', function () {

        typeRechercheUser = selectService.val();

        if (typeRechercheUser == '*') {
            selectSite.attr('disabled', true);
        } else {
            selectSite.attr('disabled', false);
            selectSiteVal = $('#selectSite option:selected').text();
        }

    });

    selectDivision.on('change', function () {

        divisionSelected = selectDivision.val();
        if (divisionSelected != '*') {
            getBureauByDivision(divisionSelected, '');
        }
    });

    btnAccorderDroit.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var value = '<span style="font-weight:bold">' + nameUserSelected + '</span>';

        alertify.confirm('Etes-vous sûre de vouloir accorder tous ces droits à l\'utilisateur : ' + value + ' ?', function () {
            affecterDroit();
        });
    });

    btnRetirerDroit.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var value = '<span style="font-weight:bold">' + nameUserSelected + '</span>';

        alertify.confirm('Etes-vous sûre de vouloir retirer tous ces droits à l\'utilisateur : ' + value + ' ?', function () {
            retirerDroit();
        });
    });


    btnConfirmeOperation.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputObservation.val() == '') {
            alertify.alert('Veuillez d\'abord fournir les motivations de cette action');
            return;
        }

        deleteOrActivateUser();
    });

    btnCreateUser.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        window.location = 'gestion-utilisateur';
    });

    cmbSearchTypeUser.on('change', function (e) {

        inputSearchUser.val('');

        if (cmbSearchTypeUser.val() === '1') {
            inputSearchUser.attr('placeholder', 'Veuillez saisir le login utilisateur');
        } else {
            inputSearchUser.attr('placeholder', 'Veuillez saisir le nom utilisateur');
        }
    });

    btnSearchUser.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputSearchUser.val() == empty) {

            switch (cmbSearchTypeUser.val()) {
                case '1':
                    alertify.alert('Veuillez saisir le login utilisateur');
                    return;
                    break;
                case '2':
                    alertify.alert('Veuillez saisir le nom utilisateur');
                    return;
                    break;
            }
        } else {
            idAdvanced = '0';
            loadUser();
        }
    });

    btnCallModalSearchUser.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalUserAdvancedSearchs.modal('show');
    });

    btnAdvancedSearchUser.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        idAdvanced = '1';
        loadUser();
    });

    btnSaveAffecterBureau.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (checkAffectedUser) {
            alertify.confirm('Etes-vous sûre de vouloir effectué cette opération ?', function () {
                affecterUserDivision();
            });
        }
    });

    initData();
    printUser('');
});

function loadUser() {

    var dateDebut;
    var dateLast;

    if (idAdvanced == '1') {

        dateDebut = inputDateDebut.val();
        dateLast = inputdateLast.val();
    }

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadUser',
            'typeSearch': cmbSearchTypeUser.val(),
            'dateDebut': dateDebut,
            'dateFin': dateLast,
            'isAdvance': idAdvanced,
            'service': selectService.val(),
            'status': selectStatuUser.val(),
            'site': selectSite.val(),
            'valueSearch': inputSearchUser.val().trim(),
            'userId': userData.idUser,
            'division': selectDivision.val()
        },
        beforeSend: function () {

            switch (idAdvanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
                case '1':
                    modalUserAdvancedSearchs.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
            }
        },
        success: function (response)
        {

            switch (idAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalUserAdvancedSearchs.unblock();

                    break;
            }

            if (response == '-1') {

                showResponseError();
                return;

            } else if (response == '0') {

                if (idAdvanced === '0') {

                    switch (cmbSearchTypeUser.val()) {
                        case '1':
                            printUser('');
                            alertify.alert('Aucun utilisateur ne correspond au login saisie');
                            return;
                            break;
                        case '2':
                            printUser('');
                            alertify.alert('Aucun utilisateur ne correspond au nom saisie');
                            return;
                            break;
                    }

                } else {
                    printUser('');
                    alertify.alert('Aucun agent ne correspond au critère fournis');
                }


            } else {

                userList = JSON.parse(JSON.stringify(response));
                printUser(userList);
                modalUserAdvancedSearchs.modal('hide');
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {

            switch (idAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalUserAdvancedSearchs.unblock();

                    break;
            }
            printUser('');
            showResponseError();
        }

    });
}


function printUser(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:10%">MATRICULE</th>';
    header += '<th style="width:20%">NOMS COMPLET</th>';
    header += '<th style="width:20%">INFOS CONNEXION</th>';
    header += '<th style="width:30%">INFOS SERVICES</th>';
    header += '<th style="width:15%">CONTACT</th>';
    header += '<th style="width:5%;text-align:center"> </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyPersonnes">';

    for (var i = 0; i < result.length; i++) {

        var nameUser = result[i].nom_agent.toUpperCase() + ' ' + result[i].prenom_agent.toUpperCase();

        var button = '<button class="btn btn-warning" onclick="callModalRightUser(\'' + result[i].code_agent + '\',\'' + nameUser + '\',\'' + result[i].libelleFonction.toUpperCase() + '\')"><i class="fa fa-list"></i>&nbsp&nbspGérer les droits</button>';

        var buttonUpdateUser = '<br/><br/><button class="btn btn-primary" onclick="updateUser(\'' + result[i].code_agent + '\',\'' + nameUser + '\')"><i class="fa fa-edit"></i>&nbsp&nbspModifier infos</button>';

        var buttonDivision = '';

        if (result[i].isPeage == 0) {
            buttonDivision = '<br/><br/><button class="btn btn-success" onclick="showDivisionBureau(\'' + result[i].code_agent + '\',\'' + nameUser + '\')"><i class="fa fa-exchange"></i>&nbsp&nbspAccorder division</button>';
        }

        if (userData.codeFonction == 'F0044') {
            buttonDivision = '';
        }

        var textBtnDeleteOrActivateUser = '';
        var iconBtnDeleteOrActivateUser = '';
        var typeBtnDeleteOrActivateUser = '';

        if (result[i].stateUser == 1) {

            textBtnDeleteOrActivateUser = 'Désactiver';
            iconBtnDeleteOrActivateUser = 'fa fa-trash-o';
            typeBtnDeleteOrActivateUser = 'btn btn-danger';

        } else {

            textBtnDeleteOrActivateUser = 'Activer';
            iconBtnDeleteOrActivateUser = 'fa fa-check-circle';
            typeBtnDeleteOrActivateUser = 'btn btn-primary';
        }

        var buttonDeleteOrActivateUser = '<br/><br/><button class="' + typeBtnDeleteOrActivateUser + '" onclick="callModalDeleteOrActivateUser(\'' + result[i].code_agent + '\',\'' + nameUser + '\',\'' + result[i].stateUser + '\',\'' + result[i].libelleFonction + '\')"><i class="' + iconBtnDeleteOrActivateUser + '"></i>&nbsp&nbsp' + textBtnDeleteOrActivateUser + '</button>';

        var valueConnexion = result[i].isConnected == 1 ? "est connecté".toUpperCase() : "déconnecté".toUpperCase();
        var colorConnexion = result[i].isConnected == 1 ? "green" : "red";

        var valueState = result[i].stateUser == 1 ? "est actif".toUpperCase() : "est désactivé".toUpperCase();
        var colorState = result[i].stateUser == 1 ? "green" : "red";

        var dateSuppressionInfo = '';

        if (result[i].stateUser == 0) {
            dateSuppressionInfo = '<br/><br/>DATE SUPPRESSION : ' + '<span style="font-weight:bold;color:red">' + result[i].dateSuppression + '</span>';
        }

        var isConnectedInfo = 'STATUS : ' + '<span style="font-weight:bold;color:' + colorConnexion + '">' + valueConnexion + '</span>';
        var dateLastConnexionInfo = 'DATE CONNEXION : ' + '<span style="font-weight:bold">' + result[i].dateLastConnexion + '</span>';
        var dateLastDeconnexionInfo = 'DATE dernière déconnexion : ' + '<span style="font-weight:bold">' + result[i].dateLastDeconnexion + '</span>';
        var stateInfo = 'ETAT : ' + '<span style="font-weight:bold;color:' + colorState + '">' + valueState + '</span>';

        var userConnexionInfo = isConnectedInfo + '<br/><br/>' + dateLastConnexionInfo + '<br/><br/>' + dateLastDeconnexionInfo.toUpperCase() + '<br/><br/>' + stateInfo + dateSuppressionInfo;

        var fonctionInfo = 'FONCTION : ' + '<span style="font-weight:bold">' + result[i].libelleFonction.toUpperCase() + '</span>';
        var gradeInfo = 'GRADE : ' + '<span style="font-weight:bold">' + result[i].grade.toUpperCase() + '</span>';
        var serviceInfo = 'SERVICE : ' + '<span style="font-weight:bold">' + result[i].libelleService.toUpperCase() + '</span>';
        var siteInfo = 'SITE/BUREAU : ' + '<span style="font-weight:bold">' + result[i].libelleSite.toUpperCase() + '</span>';
        var divisionInfo = 'DIVISION : ' + '<span style="font-weight:bold">' + result[i].divisionName.toUpperCase() + '</span>';

        var agentInfo1 = '';

        if (userData.codeFonction == 'F0044') {

            agentInfo1 = fonctionInfo + '<br/><br/>' + gradeInfo + '<hr/>' + siteInfo;

        } else {
            agentInfo1 = fonctionInfo + '<br/><br/>' + gradeInfo + '<br/><br/>' + serviceInfo + '<hr/>' + siteInfo + '<br/><br/>' + divisionInfo;
        }



        var phoneInfo = 'TELEPHONE : ' + '<span style="font-weight:bold">' + result[i].phone + '</span>';
        var mailInfo = 'EMAIL : ' + '<span style="font-weight:bold">' + result[i].mail + '</span>';

        var agentInfo2 = phoneInfo + '<br/><br/>' + mailInfo;

        var nomAgent = result[i].prenom_agent.toUpperCase() + ' ' + result[i].nom_agent.toUpperCase();

        body += '<tr>';
        body += '<td style="text-align:left;vertical-align:middle;width:5%">' + result[i].matricule + '</td>';
        body += '<td style="text-align:left;vertical-align:middle;width:20%">' + nomAgent + '</td>';
        body += '<td style="text-align:left;vertical-align:middle;width:20%">' + 'LOGIN : ' + result[i].login + '<hr/>' + userConnexionInfo + '</td>';
        body += '<td style="text-align:left;vertical-align:middle;width:30%">' + agentInfo1 + '</td>';
        body += '<td style="text-align:left;vertical-align:middle;width:15%">' + agentInfo2 + '</td>';
        body += '<td style="text-align:center;vertical-align:middle;width:10%">' + button + buttonUpdateUser + buttonDivision + buttonDeleteOrActivateUser + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableUser.html(tableContent);

    tableUser.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Filtrer la liste des utilisateurs ici ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 50,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function initData() {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'userId': userData.idUser,
            'operation': 'initDataUser'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));
                divisionList = JSON.parse(result.division);
                var serviceList = JSON.parse(result.service);
                var dataService;
                dataService += '<option value ="*">Tous</option>';

                for (var i = 0; i < serviceList.length; i++) {
                    dataService += '<option value =' + serviceList[i].codeService + '>' + serviceList[i].libelleService + '</option>';
                }

                selectService.html(dataService);

                var siteList = JSON.parse(result.site);
                var dataSite;
                dataSite += '<option value ="*">Tous</option>';
                for (var i = 0; i < siteList.length; i++) {
                    dataSite += '<option value =' + siteList[i].codeSite + '>' + siteList[i].libelleSite + '</option>';
                }
                selectSite.html(dataSite);

                var dataDivision;
                dataDivision += '<option value ="*">Tous</option>';
                for (var i = 0; i < divisionList.length; i++) {
                    dataDivision += '<option value =' + divisionList[i].codeDivision + '>' + divisionList[i].libelleDivision + '</option>';
                }
                selectDivision.html(dataDivision);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function callModalRightUser(code, name, profil) {

    codeUserSelected = code;
    nameUserSelected = name;
    profilUser = profil;

    btnAccorderDroit.attr('style', 'display:none');
    btnRetirerDroit.attr('style', 'display:none');

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadDroitUser',
            'userId': code
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des droits en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' | response == '0') {

                showResponseError();
                return;

            } else {
                rightUserList = JSON.parse(JSON.stringify(response));
                printDroitUser(rightUserList);

            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });

    function printDroitUser(result) {

        var header = '<thead style="background-color:#0085c7;color:white"><tr>';
        header += '<th style="width:15%">CODE</th>';
        header += '<th style="width:50%">INTITULE</th>';
        header += '<th style="width:10%">MODULE</th>';
        header += '<th style="width:10%;text-align:center">ETAT</th>';
        header += '<th style="width:15%;text-align:center"></th>';
        header += '</tr></thead>';

        var body = '<tbody id="tbodyPersonnes">';

        var objRight;

        for (var i = 0; i < result.length; i++) {

            var btnEditRight;
            var operation;
            var icon;
            var iconButton;
            var buttonName;

            objRight = new Object();

            objRight.code_droit = result[i].droitId;
            var state;

            switch (result[i].access) {
                case 1:
                    operation = 'remove';
                    icon = 'fa fa-remove';
                    buttonName = '&nbsp;Retirer';
                    iconButton = 'btn btn-danger';
                    btnRetirerDroit.attr('style', 'display:inline');
                    objRight.access = 1;
                    state = 'activé';
                    break;
                case 0:
                    operation = 'add';
                    icon = 'fa fa-plus-circle';
                    buttonName = '&nbsp;Ajouter';
                    iconButton = 'btn btn-success';
                    btnAccorderDroit.attr('style', 'display:inline');
                    objRight.access = 0;
                    state = 'Non activé';
                    break;

            }

            objRightList.push(objRight);

            btnEditRight = '<button type="button" class="' + iconButton + '" onclick="editAccessUser(\'' + codeUserSelected + '\',\'' + result[i].droitId + '\',\'' + operation + '\')"><i class="' + icon + '"></i>' + buttonName + '</button>';

            body += '<tr>';
            body += '<td style="text-align:left;vertical-align:middle;width:15%">' + result[i].droitId.toUpperCase() + '</td>';
            body += '<td style="text-align:left;vertical-align:middle;width:50%">' + result[i].droitName.toUpperCase() + '</td>';
            body += '<td style="text-align:left;vertical-align:middle;width:10%">' + result[i].module.toUpperCase() + '</td>';
            body += '<td style="text-align:center;vertical-align:middle;width:10%">' + state.toUpperCase() + '</td>';
            body += '<td style="text-align:center;vertical-align:middle;width:15%">' + btnEditRight + '</td>';
            body += '</tr>';
        }

        body += '</tbody>';

        var tableContent = header + body;

        tableDroitUser.html(tableContent);

        tableDroitUser.DataTable({
            language: {
                processing: "Traitement en cours...",
                track: "Rechercher&nbsp;:",
                lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix: "",
                loadingRecords: "Chargement en cours...",
                zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable: "Aucune donnée disponible pour le critère sélectionné",
                search: "Rechercher par N° document _INPUT_  ",
                paginate: {
                    first: "Premier",
                    previous: "Pr&eacute;c&eacute;dent",
                    next: "Suivant",
                    last: "Dernier"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            },
            info: false,
            destroy: true,
            searching: false,
            paging: true,
            lengthChange: false,
            tracking: false,
            ordering: false,
            pageLength: 25,
            lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
            select: {
                style: 'os',
                blurable: true
            },
            datalength: 3,
            dom: 'Bfrtip', columnDefs: [
                {"visible": false, "targets": 2}
            ], order: [[2, 'asc']],
            drawCallback: function (settings) {
                var api = this.api();
                var rows = api.rows({page: 'current'}).nodes();
                var last = null;
                api.column(2, {page: 'current'}).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="4">' + group + '</td></tr>'
                                );

                        last = group;
                    }
                });
            }
        });

        lblUser.html(nameUserSelected + ' : (' + profilUser + ")");
        displayModalRightUser.modal('show');
    }
}

function editAccessUser(userId, droitId, action) {

    var message = '';

    switch (action) {
        case 'add':
            message = 'Etes-vous sûre de vouloir accorder le droit : ' + droitId + ' à cet utilisateur ?';
            break;
        case 'remove':
            message = 'Etes-vous sûre de vouloir retirer le droit : ' + droitId + ' à cet utilisateur ?';
            break;
    }

    alertify.confirm(message, function () {

        $.ajax({
            type: 'POST',
            url: 'utilisateur_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'editAccessUser',
                'userId': userId,
                'droitId': droitId,
                'action': action
            },
            beforeSend: function () {

                switch (action) {
                    case 'add':
                        displayModalRightUser.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>L\'affectation du droit en cours ...</h5>'});
                        break;
                    case 'remove':
                        displayModalRightUser.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Le retrait du droit en cours ...</h5>'});
                        break;
                }

            },
            success: function (response)
            {

                displayModalRightUser.unblock();

                if (response == '0') {

                    showResponseError();
                    return;

                } else {

                    switch (action) {
                        case 'add':
                            alertify.alert('L\'affectation du droit utilisateur s\'est effectuée avec succès');
                            break;
                        case 'remove':
                            alertify.alert('Le retrait du droit utilisateur s\'est effectué avec succès');
                            break;
                    }

                    displayModalRightUser.modal('hide');
                    loadUser();
                }
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                displayModalRightUser.unblock();
                showResponseError();
            }
        });
    });


}

function retirerDroit() {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'retirerDroit',
            'code_agent': codeUserSelected,
            'droitList': JSON.stringify(objRightList)
        },
        beforeSend: function () {
            displayModalRightUser.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Le retrait de tous les droits en cours ...</h5>'});
        },
        success: function (response)
        {

            displayModalRightUser.unblock();

            if (response == '0') {

                showResponseError();
                return;

            } else {

                alertify.alert('Le retrait de tous les droits utilisateur s\'est effectué avec succès');
                btnAccorderDroit.attr('style', 'display:none');
                btnRetirerDroit.attr('style', 'display:none');
                displayModalRightUser.modal('hide');
                loadUser();
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            displayModalRightUser.unblock();
            showResponseError();
        }

    });
}

function affecterDroit() {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'affecterDroit',
            'code_agent': codeUserSelected,
            'idUser': userData.idUser,
            'droitList': JSON.stringify(objRightList)
        },
        beforeSend: function () {
            displayModalRightUser.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>L\'affectationt de tous les droits en cours ...</h5>'});
        },
        success: function (response)
        {

            displayModalRightUser.unblock();

            if (response == '0') {

                showResponseError();
                return;

            } else {

                alertify.alert('L\'affectation de tous les droits utilisateur s\'est effectuée avec succès');
                btnAccorderDroit.attr('style', 'display:none');
                btnRetirerDroit.attr('style', 'display:none');
                displayModalRightUser.modal('hide');
                loadUser();
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            displayModalRightUser.unblock();
            showResponseError();
        }

    });


}

function updateUser(codeUser, nameUser) {

    var value = '<span style="font-weight:bold">' + nameUser.toUpperCase() + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir modifier les informations de l\'utilisateur : ' + value + ' ?', function () {
        window.open('gestion-utilisateur?id=' + btoa(codeUser));
    });
}

function showDivisionBureau(codeUser, nameUser) {
    selectedCodeUser = codeUser;
    $('#divisionBureauModal').modal('show');

    printDivision(divisionList);
    printBureau('');
}

function printDivision(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:10%;display:none">CODE</th>';
    header += '<th style="width:80%">INTITULE</th>';
    header += '<th style="width:10%;text-align:center"></th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyDivision">';

    for (var i = 0; i < result.length; i++) {

        body += '<tr>';
        body += '<td style="text-align:left;vertical-align:middle;width:10%;display:none">' + result[i].codeDivision + '</td>';
        body += '<td style="text-align:left;vertical-align:middle;width:80%">' + result[i].libelleDivision + '</td>';
        body += '<td style="text-align:center;vertical-align:middle;width:10%"><center><input onclick="checkDivision(\'' + result[i].codeDivision + '\')" type="checkbox" id="checkBoxSelectDivision' + result[i].codeDivision + '" name="checkBoxSelectDivision' + result[i].codeDivision + '"></center></td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    $('#tableDivision').html(tableContent);
    $('#tableDivision').DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function printBureau(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:10%;display:none">CODE</th>';
    header += '<th style="width:80%">INTITULE</th>';
    header += '<th style="width:10%;text-align:center"></th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyBureau">';

    for (var i = 0; i < result.length; i++) {

        body += '<tr>';
        body += '<td style="text-align:left;vertical-align:middle;width:10%;display:none">' + result[i].codeSite + '</td>';
        body += '<td style="text-align:left;vertical-align:middle;width:80%">' + result[i].libelleSite + '</td>';
        if (result[i].checkAffectation) {
            body += '<td style="text-align:center;vertical-align:middle;width:10%"><center><input onclick="checkBureau(\'' + result[i].codeSite + '\')" type="checkbox" checked id="checkBoxSelectBureau' + result[i].codeSite + '" name="checkBoxSelectBureau' + result[i].codeSite + '"></center></td>';
        } else {
            body += '<td style="text-align:center;vertical-align:middle;width:10%"><center><input onclick="checkBureau(\'' + result[i].codeSite + '\')" type="checkbox" id="checkBoxSelectBureau' + result[i].codeSite + '" name="checkBoxSelectBureau' + result[i].codeSite + '"></center></td>';
        }

        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    $('#tableBureau').html(tableContent);
    $('#tableBureau').DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function checkDivision(codeDivision) {

    codeDivisionSelect = '';

    for (var i = 0; i < divisionList.length; i++) {
        if (codeDivision != divisionList[i].codeDivision) {
            var control = document.getElementById("checkBoxSelectDivision" + divisionList[i].codeDivision);
            control.checked = false;
        } else {
            codeDivisionSelect = divisionList[i].codeDivision;
            getBureauByDivision(codeDivisionSelect, selectedCodeUser);
        }
    }
}

function getBureauByDivision(codeDivision, selectedCodeUser) {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getBureauByDivision',
            'codeDivision': codeDivision,
            'codeUser': selectedCodeUser
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '0') {
                alertify.alert('Aucune données retrouvée.');
                printBureau('');
                return;
            } else {
                bureauList = $.parseJSON(JSON.stringify(response));

                if (selectedCodeUser == '') {
                    var dataSite;
                    dataSite += '<option value ="*">Tous</option>';
                    for (var i = 0; i < bureauList.length; i++) {
                        dataSite += '<option value =' + bureauList[i].codeSite + '>' + bureauList[i].libelleSite + '</option>';
                    }
                    selectSite.html(dataSite);
                } else {
                    tempBureauList = [];

                    for (var i = 0; i < bureauList.length; i++) {
                        var bureau = new Object();
                        bureau.checkAffectation = bureauList[i].checkAffectation;
                        bureau.affectationExist = bureauList[i].affectationExist;
                        bureau.libelleSite = bureauList[i].libelleSite;
                        bureau.sigle = bureauList[i].sigle;
                        bureau.typeEntite = bureauList[i].typeEntite;
                        bureau.centre = bureauList[i].centre;
                        bureau.codeDivision = bureauList[i].codeDivision;
                        bureau.intituleDivision = bureauList[i].intituleDivision;
                        bureau.codeSite = bureauList[i].codeSite;
                        bureau.codeUser = selectedCodeUser;
                        bureau.codeUserDivision = bureauList[i].codeUserDivision;
                        tempBureauList.push(bureau);
                    }

                    printBureau(bureauList);
                }
            }
            codeDivision = '';
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function checkBureau(codeBureau) {

    var selectBureau;
    var affectationCheckVal;

    for (var i = 0; i < tempBureauList.length; i++) {

        if (tempBureauList[i].codeSite == codeBureau) {

            selectBureau = '#checkBoxSelectBureau' + tempBureauList[i].codeSite;

            var select = $(selectBureau);

            if (select.is(':checked')) {
                affectationCheckVal = true;
            } else {
                affectationCheckVal = false;
            }

            tempBureauList[i].checkAffectation = affectationCheckVal;

            checkAffectedUser = true;

            break;
        }
    }
}

function affecterUserDivision() {

    tempBureauList = JSON.stringify(tempBureauList);

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'affecterUserDivision',
            'userDivisionList': tempBureauList
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Affectation de l\'utilisateur en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'opération effectuée avec succès.');
                    getBureauByDivision(codeDivisionSelect, selectedCodeUser);
                    checkAffectedUser = false;
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'affectation de l\'utilisateur.');
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });

}

function callModalDeleteOrActivateUser(codeUser, nameUser, state, fonction) {

    codeUserSelected = codeUser;
    stateSelected = state;

    spnTitleModal.html('D&eacute;sactivation utilisateur');

    if (state == 0) {
        spnTitleModal.html('Activation utilisateur');
    }

    nameUserSelected = '<span style="font-weight:bold">' + nameUser.toUpperCase() + '</span>';
    var fonctionInfo = '<span style="font-weight:bold">' + fonction.toUpperCase() + '</span>';

    spnNameUser.html(nameUserSelected);
    spnFonctionUser.html(fonctionInfo);

    modalTraitementUser.modal('show');
}

function deleteOrActivateUser() {

    var nameOperation = stateSelected == 1 ? 'désactiver' : 'activer';

    alertify.confirm('Etes-vous sûre de vouloir ' + nameOperation + ' l\'utilisateur : ' + nameUserSelected + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'utilisateur_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'deleteOrActivateUser',
                'userId': codeUserSelected,
                'stateUser': stateSelected,
                'observation': inputObservation.val(),
                'agentOperation': userData.idUser
            },
            beforeSend: function () {

                if (stateSelected == 1) {
                    modalTraitementUser.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>La désactivation utilisateur en cours...</h5>'});
                } else {
                    modalTraitementUser.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>L\'activation utilisateur en cours...</h5>'});
                }
            },
            success: function (response)
            {

                if (response == '-1') {
                    modalTraitementUser.unblock();
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    modalTraitementUser.unblock();

                    if (response == '1') {

                        if (stateSelected == 1) {
                            alertify.alert('La désactivation de l\'utilisateur : ' + nameUserSelected + ', s\'est effectuée avec succès.');
                        } else {
                            alertify.alert('L\'activation de l\'utilisateur : ' + nameUserSelected + ', s\'est effectuée avec succès.');
                        }

                        inputObservation.val('');
                        spnNameUser.html('');
                        spnFonctionUser.html('');
                        modalTraitementUser.modal('hide');

                        loadUser();

                    } else {
                        showResponseError();
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                modalTraitementUser.unblock();
                showResponseError();
            }
        });
    });

}