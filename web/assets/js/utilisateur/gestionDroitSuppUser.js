/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tableDroitAccess;

var cmbModule, codeResearchModule;

var btnEnregistrerDroit, btnSearchDroit;

var inputSearchDroit, inputCodeDroit, inputIntituleDroit;

var codeDroit = '';

var selectDroitData;
var codeModule;
var moduleList;
var btnInitDroit;
var cmbBureau;
var cmbUsers;
var cmbDroit;
var spnNameUserSelected;
var droitsNonAccordesList = [];
var droitsNonAccorde = {};
var droitsNonAccordeTable = [];
var checkExist;
var btnAddNewDroitUser, spnNbreDroit;
var codeUserSelected;

$(function () {

    mainNavigationLabel.text('UTILISATEUR');
    secondNavigationLabel.text('Gestion des droits supplémentaires des utilisateurs');

    tableDroitAccess = $('#tableDroitAccess');

    cmbModule = $('#cmbModule');

    checkExist = false;

    inputSearchDroit = $('#inputSearchDroit');
    inputCodeDroit = $('#inputCodeDroit');
    inputIntituleDroit = $('#inputIntituleDroit');

    btnEnregistrerDroit = $('#btnEnregistrerDroit');
    btnSearchDroit = $('#btnSearchDroit');
    btnInitDroit = $('#btnInitDroit');
    cmbBureau = $('#cmbBureau');
    cmbUsers = $('#cmbUsers');
    cmbDroit = $('#cmbDroit');
    spnNameUserSelected = $('#spnNameUserSelected');

    btnAddNewDroitUser = $('#btnAddNewDroitUser');
    spnNbreDroit = $('#spnNbreDroit');

    cmbModule.on('change', function (e) {
        codeResearchModule = cmbModule.val();
    });

    cmbBureau.on('change', function (e) {

        if (cmbBureau.val() == '0') {
            alertify.alert('Veuillez selectionner d\'abord sélectionner un bureau');
            return;
        }

        loadUsersBySite(cmbBureau.val());

    });

    cmbUsers.on('change', function (e) {

        if (cmbUsers.val() == '0') {
            spnNameUserSelected.html('');
            alertify.alert('Veuillez selectionner d\'abord sélectionner un utilisateur');
            return;
        }

        loadDroitsAccessByUser(cmbUsers.val());
        spnNameUserSelected.html($('#cmbUsers option:selected').text().toUpperCase());

        codeUserSelected = cmbUsers.val();

        loadDroitsNonAccordeesByUser(cmbUsers.val());

    });

    btnInitDroit.click(function (e) {

        e.preventDefault();

        cmbModule.val('0');
        inputCodeDroit.val('');
        inputIntituleDroit.val('');
        inputCodeDroit.attr('readonly', false);
        codeDroit = '';
        codeModule = '';

    });

    btnAddNewDroitUser.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir accorder ce(s) ' + droitsNonAccordeTable.length + ' droit(s) à cet utilisateur ?', function () {
            saveNewDroit();
        });
    });

    loadModule();
    loadSite();


});

function loadSite() {

    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadSite',
            'userId': userData.idUser

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var siteList = $.parseJSON(JSON.stringify(response));

                var data = '<option value="0">--</option>';

                for (var i = 0; i < siteList.length; i++) {
                    data += '<option value="' + siteList[i].code + '">' + siteList[i].intitule.toUpperCase() + '</option>';
                }

                cmbBureau.html(data);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadUsersBySite(site) {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadInfosUserBySite',
            'codeSite': site

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des utilisateurs en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var usersList = $.parseJSON(JSON.stringify(response));

                var data = '<option value="0">--</option>';

                for (var i = 0; i < usersList.length; i++) {

                    var nameUser = '<span style="font-weight:bold">' + usersList[i].matricule.toUpperCase() + '</span>' + ' : ' + usersList[i].prenom_agent.toUpperCase() + ' ' + usersList[i].nom_agent.toUpperCase() + ' (Fonction : ' + usersList[i].fonctionName.toUpperCase() + ')';
                    data += '<option value="' + usersList[i].code_agent + '">' + nameUser + '</option>';
                }

                cmbUsers.html(data);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadDroitsAccessByUser(user) {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadDroitUserV2',
            'userId': user

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des droits utilisateurs en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var droitsList = $.parseJSON(JSON.stringify(response));

                var data = '<option value="0">--</option>';

                for (var i = 0; i < droitsList.length; i++) {

                    data += '<option value="' + droitsList[i].code + '">' + droitsList[i].code + ' (Module : ' + droitsList[i].module + ')' + '</option>';
                }

                cmbDroit.html(data);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadDroitsNonAccordeesByUser(user) {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadDroitUserV3',
            'userId': user

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des droits utilisateurs en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                droitsNonAccordesList = $.parseJSON(JSON.stringify(response));
                printDroit(droitsNonAccordesList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printDroit(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">CODE</th>';
    tableContent += '<th scope="col">DROIT</th>';
    tableContent += '<th scope="col">MODULE</th>';
    tableContent += '<th scope="col">COCHEZ</th>';
    tableContent += '<th hidden="true" scope="col">Code module</th>';
    tableContent += '<th hidden="true" scope="col">Code droit</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr id="row_' + data[i].code_droit + '">';
        tableContent += '<td style="width:15%;vertical-align:middle">' + data[i].code_droit.toUpperCase() + '</td>';
        tableContent += '<td style="width:80%;vertical-align:middle">' + data[i].intitule_droit.toUpperCase() + '</td>';
        tableContent += '<td style="width:40%;vertical-align:middle">' + data[i].intituleModule.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle"><input  type="checkbox" id="checkbox_' + data[i].code_droit + '" onclick="getDroit(\'' + data[i].code_droit + '\')" /></th>';
        tableContent += '<td hidden="true">' + data[i].fkModule + '</td>';
        tableContent += '<td hidden="true">' + data[i].code_droit + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';
    tableDroitAccess.html(tableContent);

    var myDt = tableDroitAccess.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par Intitulé droit _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 50,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        columnDefs: [
            {"visible": false, "targets": 2}
        ],
        order: [[2, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="6">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableDroitAccess tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        selectDroitData = new Object();
        selectDroitData.code_droit = myDt.row(this).data()[0].toUpperCase();

    });
}

function preparerModification() {

    codeDroit = selectDroitData.code_droit;
    inputCodeDroit.val(selectDroitData.code_droit);
    inputIntituleDroit.val(selectDroitData.intitule_droit);

    inputCodeDroit.attr('readonly', true);
}

function loadModule() {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadModule'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            moduleList = $.parseJSON(JSON.stringify(response));
            
            var data = '<option value="0">--</option>';

            for (var i = 0; i < moduleList.length; i++) {
                data += '<option value="' + moduleList[i].codeModule + '">' + moduleList[i].intituleModule.toUpperCase() + '</option>';
            }
            
            cmbModule.html(data);
            
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function saveNewDroit() {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveNewDroit',
            'codeUser': codeUserSelected,
            'idUser': userData.idUser,
            'droitAccordeList': JSON.stringify(droitsNonAccordeTable)
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement des droits en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('L\'ajout de droit(s) supplémentaires à l\'utilisateur en cours s\'est effectué avec succès.');

                    droitsNonAccordeTable = [];
                    btnAddNewDroitUser.attr('disabled', true);
                    spnNbreDroit.html('');
                    
                    loadDroitsAccessByUser(cmbUsers.val());
                    loadDroitsNonAccordeesByUser(cmbUsers.val());

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'ajout de droits supplémentaires');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function getDroit(id) {

    var row = $("#row_" + id);
    var checkBox = document.getElementById("checkbox_" + id);

    if (checkBox.checked) {

        row.attr('style', 'background-color:#daf2c9;color:black');

        checkExist = true;

        for (var j = 0; j < droitsNonAccordesList.length; j++) {

            if (droitsNonAccordesList[j].code_droit == id) {

                droitsNonAccorde = {};
                droitsNonAccorde.code_droit = id;
                droitsNonAccordeTable.push(droitsNonAccorde);
            }
        }

        spnNbreDroit.html(droitsNonAccordeTable.length);
        btnAddNewDroitUser.attr('disabled', false);

    } else {

        row.removeAttr('style');

        for (var i = 0; i < droitsNonAccordeTable.length; i++) {

            if (droitsNonAccordeTable[i].code_droit == id) {

                droitsNonAccordeTable.splice(i, 1);
                break;
            }
        }

        if (droitsNonAccordeTable.length == 0) {
            checkExist = false;
            btnAddNewDroitUser.attr('disabled', true);

        } else {
            checkExist = true;
            btnAddNewDroitUser.attr('disabled', false);
        }

        spnNbreDroit.html(droitsNonAccordeTable.length);
    }
}

