/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnEnregistrer, btnAjouterAdresse, btnRéinitialiserMotPasse,
        btnRéinitialiser, btnRechercherAdresses, btnAddEntite, btnValiderAdresse, btnFermerAdresse;
var inputMatricule, inputNom, inputPrenom, inputGrade, inputEmail,
        inputTelephone, inputLogin, inputLibelle, inputNumber;
var cmbFonction, cmbDistrinct, cmbSite, cmbService, cmbUnitAdm, cmbDivisions;
var tableAdressesPersonne, tableAdresses;
var labelInfo, labelSelectedAdresse;
var checkBoxExpirationCompte, checkBoxActiverCompte, checkBoxExpirationCompteDate;
var modalAdresseAssujetti, modalAdresses, exampleModalLongTitle;
var divAdresses;
var adresse, codeFonction, loginUser;
var
        tempAdresseList = [],
        divisionList = [],
        siteList = [],
        suppressAdresseList = [];
var codeAgentEdition = '';
var compteExiper;
var dataSites;
var dataSitePeages;
var cmbSitePeage;
var idDivSitePeage;
var isPeageSite;
var codeDivisions;

var idDivision,
        idDivService;

var isBankAdminUser;

$(function () {

    isPeageSite = '0';

    var urlCode = getUrlParameter('id');
    if (jQuery.type(urlCode) !== 'undefined') {
        codeAgentEdition = atob(urlCode);
    }

    mainNavigationLabel.text('UTILISATEUR');
    secondNavigationLabel.text(codeAgentEdition == ''
            ? 'Edition d\'un utilisateur'
            : 'Modification d\'un utilisateur');
    removeActiveMenu();
    linkMenuGestionUtilisateur.addClass('active');
    tableAdressesPersonne = $('#tableAdressesPersonne');
    tableAdresses = $('#tableAdresses');
    divAdresses = $('#divAdresses');
    modalAdresseAssujetti = $('#modalAdresseAssujetti');
    modalAdresses = $('#modalAdresses');
    exampleModalLongTitle = $('#exampleModalLongTitle');
    checkBoxExpirationCompte = $('#checkBoxExpirationCompte');
    checkBoxActiverCompte = $('#checkBoxActiverCompte');
    checkBoxExpirationCompteDate = $('#checkBoxExpirationCompteDate');
    labelInfo = $('#labelInfo');
    labelSelectedAdresse = $('#labelSelectedAdresse');
    btnEnregistrer = $('#btnEnregistrer');
    btnAjouterAdresse = $('#btnAjouterAdresse');
    btnRéinitialiserMotPasse = $('#btnRéinitialiserMotPasse');
    btnRéinitialiser = $('#btnRéinitialiser');
    btnRechercherAdresses = $('#btnRechercherAdresses');
    btnAddEntite = $('#btnAddEntite');
    btnValiderAdresse = $('#btnValiderAdresse');
    btnFermerAdresse = $('#btnFermerAdresse');
    cmbSitePeage = $('#cmbSitePeage');
    inputMatricule = $('#inputMatricule');
    inputNom = $('#inputNom');
    inputPrenom = $('#inputPrenom');
    inputGrade = $('#inputGrade');
    inputEmail = $('#inputEmail');
    inputTelephone = $('#inputTelephone');
    inputLogin = $('#inputLogin');
    inputLibelle = $('#inputLibelle');
    inputNumber = $('#inputNumber');
    cmbFonction = $('#cmbFonction');
    cmbDistrinct = $('#cmbDistrinct');
    cmbSite = $('#cmbSite');
    cmbService = $('#cmbService');
    cmbUnitAdm = $('#cmbUnitAdm');
    cmbDivisions = $('#cmbDivisions');
    idDivSitePeage = $('#idDivSitePeage');

    idDivision = $('#idDivision');
    idDivService = $('#idDivService');

    if (userData.codeFonction == 'F0044') {

        isBankAdminUser = true;

        idDivision.attr('style', 'display:none');
        idDivService.attr('style', 'display:none');

    } else {

        isBankAdminUser = false;

        idDivision.attr('style', 'display:block');
        idDivService.attr('style', 'display:block');
    }

    checkBoxExpirationCompteDate.attr('disabled', true);

    btnEnregistrer.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        checkFields();
    });
    btnRéinitialiserMotPasse.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        resetPasswordUser();
    });
    btnAjouterAdresse.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }


        $('#inputLibelle').val('');
        inputNumber.attr('style', 'visibility: hidden');
        btnValiderAdresse.attr('style', 'visibility: hidden');
        printAdresse('');
        $('#labelSelectedAdresse').text('');
        $('#inputNumber').val('');
        $('#modalAdresses').modal('show');
    });
    btnRechercherAdresses.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadAdresses();
    });
    btnValiderAdresse.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        validateAdresse();
    });
    cmbFonction.on('change', function () {
        codeFonction = cmbFonction.val();
    });

    cmbSite.on('change', function () {

        if (cmbSite.val() == '0') {
            alertify.alert('Veuillez d\'abord saisir un site valide');
            return;
        } else {

            for (var i = 0; i < dataSites.length; i++) {

                if (dataSites[i].codeSite == cmbSite.val() && dataSites[i].typeSite == '1') {

                    isPeageSite = '1';

                    var data = '<option value ="0">-- Sélectionner --</option>';
                    for (var i = 0; i < dataSitePeages.length; i++) {
                        data += '<option value =' + dataSitePeages[i].codeSite + '>' + dataSitePeages[i].libelleSite.toUpperCase() + '</option>';
                    }

                    cmbSitePeage.html(data);
                    idDivSitePeage.attr('style', 'display:inline');
                    return;
                } else {
                    idDivSitePeage.attr('style', 'display:none');
                    isPeageSite = '0';
                }
            }
        }
    });

    cmbDivisions.on('change', function () {
        codeDivisions = cmbDivisions.val();
        loadSiteByDivision(codeDivisions);
    });

    checkBoxExpirationCompte.on('change', function () {
        var checkActiverCompte = document.getElementById("checkBoxExpirationCompte");
        if (checkActiverCompte.checked) {
            checkBoxExpirationCompteDate.attr('disabled', false);
        } else {
            checkBoxExpirationCompteDate.attr('disabled', true);
        }
    });
    btnEnregistrer.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        checkFields();
    });
    initDataUser();
    loadDivision();
});

function initDataUser() {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'userId': userData.idUser,
            'operation': 'initDataUser'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));
                var dataFonctions = JSON.parse(result.fonction);
                var dataFonction = '<option value ="0">-- Sélectionner --</option>';

                if (userData.codeFonction == 'F0043') {
                    dataFonction = '';
                    var fonctionBank = 'BANQUIER';
                    dataFonction += '<option value ="F0019">' + fonctionBank + '</option>';
                    cmbFonction.html(dataFonction);

                } else {
                    for (var i = 0; i < dataFonctions.length; i++) {
                        dataFonction += '<option value =' + dataFonctions[i].codeFonction + '>' + dataFonctions[i].libelleFonction.toUpperCase() + '</option>';
                    }
                    cmbFonction.html(dataFonction);
                    dataSitePeages = JSON.parse(result.sitePeage);
                }


                var dataServices = JSON.parse(result.service);
                var dataservice = '<option value ="0">-- Sélectionner --</option>';

                if (userData.codeFonction == 'F0043') {

                    dataservice = '';
                    var value = 'BANQUE';
                    dataservice += '<option value ="00003">' + value + '</option>';
                    cmbService.html(dataservice);

                } else {

                    for (var i = 0; i < dataServices.length; i++) {
                        dataservice += '<option value =' + dataServices[i].codeService + '>' + dataServices[i].libelleService.toUpperCase() + '</option>';
                    }
                    cmbService.html(dataservice);
                }

                if (userData.codeFonction == 'F0044') {

                    var siteList = JSON.parse(result.site);

                    var datasite = '<option value ="0">-- Sélectionner --</option>';
                    for (var i = 0; i < siteList.length; i++) {
                        datasite += '<option value =' + siteList[i].codeSite + '>' + siteList[i].libelleSite.toUpperCase() + '</option>';

                    }

                    cmbSite.html(datasite);
                    cmbSite.val(userData.codeSite);
                }

                if (codeAgentEdition != '') {
                    loadInfosUser(codeAgentEdition);
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadAdresses() {

    if (inputLibelle.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir une adresse.');
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAdresses',
            'libelle': inputLibelle.val(),
        },
        beforeSend: function () {

            modalAdresses.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                modalAdresses.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalAdresses.unblock();
                var adresseList = $.parseJSON(JSON.stringify(response));
                if (adresseList.length > 0) {
                    inputNumber.attr('style', 'visibility: visible');
                    btnValiderAdresse.attr('style', 'visibility: visible');
                    printAdresse(adresseList);
                } else {
                    inputNumber.attr('style', 'visibility: hidden');
                    btnValiderAdresse.attr('style', 'visibility: hidden');
                    printAdresse('');
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            modalAdresses.unblock();
            showResponseError();
        }

    });
}

function validateAdresse() {

    if (labelSelectedAdresse.text().trim() === '') {
        alertify.alert('Veuillez d\'abord sélectionner une adresse.');
        return;
    }

    if (inputNumber.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir un numéro de l\'adresse en cours de sélectionner.');
        return;
    }

    var numero = inputNumber.val();
    var exist = ifAdressExists(adresse.code, numero);
    if (exist === true) {
        alertify.alert('Cette adresse a déjà été ajoutée dans la liste des adresses.');
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir confirmer cette adresse ?', function () {
        labelInfo.html('L\'adresse par défaut est mise en surbrillance');
        adresse.numero = numero;
        if (tempAdresseList.length === 0) {
            adresse.defaut = 'Oui';
        } else {
            adresse.defaut = '';
        }
        tempAdresseList.push(adresse);
        printAdressePersonne(tempAdresseList);
        $('#labelSelectedAdresse').text('');
        $('#inputNumber').val('');
    });
}

function printAdresse(adresseList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> PROVINCE </th>';
    header += '<th scope="col"> VILLE </th>';
    header += '<th hidden="true" scope="col"> DISTRICT </th>';
    header += '<th scope="col"> COMMUNE </th>';
    header += '<th scope="col"> QUARTIER </th>';
    header += '<th scope="col"> AVENUE </th>';
    header += '<th hidden="true" scope="col"> Code avenue </th>';
    header += '</tr></thead>';
    var body = '<tbody id="tbodyAdresses">';
    for (var i = 0; i < adresseList.length; i++) {
        body += '<tr>';
        body += '<td>' + adresseList[i].province.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].ville.toUpperCase() + '</td>';
        body += '<td hidden="true">' + i + '</td>';
        body += '<td>' + adresseList[i].commune.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].quartier.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].avenue.toUpperCase() + '</td>';
        body += '<td hidden="true">' + adresseList[i].codeAvenue + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableAdresses.html(tableContent);
    var dtAdresse = tableAdresses.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    $('#tableAdresses tbody').on('click', 'tr', function () {
        var data = dtAdresse.row(this).data();
        adresse = new Object();
        adresse.code = data[6];
        adresse.chaine = data[0].toUpperCase() + '--> C/' + data[3].toUpperCase() + ' - Q/' + data[4].toUpperCase() + ' - Av. ' + data[5].toUpperCase();
        adresse.numero = '';
        adresse.defaut = '';
        adresse.codeAP = '';
        adresse.etat = '1';
        labelSelectedAdresse.html(adresse.chaine);
    });
}

function printAdressePersonne(tempAdresseList) {

    var header = '<thead><tr>';
    header += '<th scope="col"> ADRESSES UTILISATEUR </th>';
    header += '<th hidden="true" scope="col"> Principale </th>';
    header += '<th width="82" scope="col"></th>';
    header += '<th hidden="true" scope="col"> Code </th>';
    header += '<th hidden="true" scope="col"> Numero </th>';
    header += '<th hidden="true" scope="col"> ID Adresse </th>';
    header += '</tr></thead>';
    var body = '<tbody id="tbodyAdressesPersonne">';
    for (var i = 0; i < tempAdresseList.length; i++) {

        var numero = ' n° ' + tempAdresseList[i].numero;
        if (tempAdresseList[i].codeAP !== '') {
            numero = '';
        }

        var style = '', hidden = '';
        if (tempAdresseList[i].defaut === 'Oui') {
            style = 'style="background-color: #c1e2b3;color:black"';
            hidden = 'style="visibility: hidden"';
        }

        body += '<tr ' + style + '>';
        body += '<td>' + tempAdresseList[i].chaine.toUpperCase() + numero + '</td>';
        body += '<td hidden="true">' + tempAdresseList[i].defaut + '</td>';
        body += '<td><a ' + hidden + 'style="margin-right:3px" onclick="setDefaultAdresse(\'' + tempAdresseList[i].code + '\',\'' + tempAdresseList[i].numero + '\')" class="btn btn-success"><i class="fa fa-check"></i></a><a onclick="removeAdresse(\'' + tempAdresseList[i].code + '\',\'' + tempAdresseList[i].numero + '\')" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>';
        body += '<td hidden="true">' + tempAdresseList[i].code + '</td>';
        body += '<td hidden="true">' + tempAdresseList[i].numero + '</td>';
        body += '<td hidden="true">' + tempAdresseList[i].idAdresse + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableAdressesPersonne.html(tableContent);
    tableAdressesPersonne.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}

function resetFields() {

    inputNom.val('');
    inputPrenom.val('');
    inputTelephone.val('');
    inputEmail.val('');
    cmbFonction.val(0);
    cmbSite.val(0);
    cmbService.val(0);
    cmbUnitAdm.val(0);
    cmbDistrinct.val(0);
    cmbDivisions.val(0);
    inputLogin.val('');
    inputMatricule.val('');
    inputGrade.val('');
    tableAdressesPersonne.remove();
    tempAdresseList = [];
    codeDivisions = '0';

    if (isPeageSite === '1') {
        cmbSitePeage.val('0');
        idDivSitePeage.attr('style', 'display:none');
    }

    if (codeAgentEdition !== '') {
        codeAgentEdition = '';
        window.location = 'gestion-utilisateur';
    }

}

function ifAdressExists(code, numero) {

    for (var i = 0; i < tempAdresseList.length; i++) {
        if (tempAdresseList[i].code === code && tempAdresseList[i].numero === numero) {
            return true;
        }
    }
    return false;
}

function ifDefaultAdressExists() {

    for (var i = 0; i < tempAdresseList.length; i++) {
        if (tempAdresseList[i].defaut === 'Oui') {
            return true;
        }
    }
    return false;
}

function setDefaultAdresse(code, numero) {

    var adresseFormatted = getAdresseFormatted(code, numero);
    alertify.confirm('Etes-vous sûre de vouloir définir cette adresse : <br/>' + adresseFormatted + ' comme adresse principale ?', function () {
        for (var i = 0; i < tempAdresseList.length; i++) {
            if (tempAdresseList[i].code === code && tempAdresseList[i].numero === numero) {
                tempAdresseList[i].defaut = 'Oui';
            } else {
                tempAdresseList[i].defaut = '';
            }
        }
        printAdressePersonne(tempAdresseList);
    });
}

function removeAdresse(code, numero) {

    for (var i = 0; i < tempAdresseList.length; i++) {
        if (tempAdresseList[i].code === code && tempAdresseList[i].numero === numero) {

            alertify.confirm('Etes-vous sûre de vouloir rétirer cette adresse : <br/>' + tempAdresseList[i].chaine + ' n° ' + tempAdresseList[i].numero + ' ?', function () {

                if (tempAdresseList[i].codeAP !== '') {
                    adresse = new Object();
                    adresse.code = tempAdresseList[i].code;
                    adresse.chaine = tempAdresseList[i].chaine;
                    adresse.numero = tempAdresseList[i].numero;
                    adresse.defaut = tempAdresseList[i].defaut;
                    adresse.codeAP = tempAdresseList[i].codeAP;
                    adresse.etat = '';
                    suppressAdresseList.push(adresse);
                }

                tempAdresseList.splice(i, 1);
                printAdressePersonne(tempAdresseList);
                if (tempAdresseList.length == 0) {
                    tableAdressesPersonne.remove();
                }
            });
            break;
        }
    }

}

function getAdresseFormatted(code, numero) {

    var adresseFormatted = '';
    for (var i = 0; i < tempAdresseList.length; i++) {
        if (tempAdresseList[i].code === code && tempAdresseList[i].numero === numero) {

            if (tempAdresseList[i].codeAP === '') {
                adresseFormatted = tempAdresseList[i].chaine + ' n° ' + tempAdresseList[i].numero;
            } else {
                adresseFormatted = tempAdresseList[i].chaine;
            }

        }
    }
    return adresseFormatted;
}

function checkFields() {

    if (inputMatricule.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir le matricule de l\'utilisateur.');
        return;
    }

    if (inputNom.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir le nom de l\'utilisateur.');
        return;
    }

    if (inputPrenom.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir le prénom de l\'utilisateur.');
        return;
    }

    if (cmbFonction.val() === '0') {
        alertify.alert('Veuillez d\'abord sélectionner la fonction de l\'utilisateur.');
        return;
    }

    if (isBankAdminUser == false) {

        if (cmbService.val() === '0') {
            alertify.alert('Veuillez d\'abord sélectionner le service de l\'utilisateur.');
            return;
        }

        if (cmbDivisions.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner une division valide');
            return;
        }
    }


    if (cmbSite.val() === '0') {
        alertify.alert('Veuillez d\'abord sélectionner le site de l\'utilisateur.');
        return;
    }

    if (isPeageSite == '1') {

        if (cmbSitePeage.val().trim() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un site péage pour l\'utilisateur en cour');
            return;
        }

    }

    if (inputLogin.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir le login de l\'utilisateur.');
        return;
    }

    if (tempAdresseList.length === 0) {
        alertify.alert('Veuillez sélectionner au moins une adresse.');
        return;
    } else {
        var exist = ifDefaultAdressExists();
        if (exist === false) {
            alertify.alert('Veuillez spécifier l\'adresse par principale de l\'utilisateur.');
            return;
        }
    }

    if (codeAgentEdition == '') {

        loginUser = inputLogin.val().trim();
        if (checkLogin(loginUser)) {
            alertify.alert('Ce login existe déjà dans le système.');
            return;
        }
    }

    alertify.confirm('Etes-vous sûre de vouloir enregistrer cet utilisateur ?', function () {
        saveUser();
    });
}

function saveUser() {

    completeListAdresse();
    var adresses = JSON.stringify(tempAdresseList);
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveUser',
            'codeFonction': cmbFonction.val(),
            'codeSite': cmbSite.val(),
            'codeService': cmbService.val(),
            'nom': inputNom.val(),
            'prenom': inputPrenom.val(),
            'telephone': inputTelephone.val(),
            'email': inputEmail.val().trim(),
            'login': inputLogin.val(),
            'idUser': userData.idUser,
            'matricule': inputMatricule.val(),
            'grade': inputGrade.val(),
            'compteExiper': compteExiper,
            'dateExipiration': checkBoxExpirationCompteDate.val(),
            'codeuser': codeAgentEdition,
            'codeSitePeage': isPeageSite === '1' ? cmbSitePeage.val() : empty,
            'adresses': adresses
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de l\'utilisateur en cours...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                if (response == '1') {
                    alertify.alert('L\'enregistrement de l\'utilisateur s\'est effectué avec succès.');
                    resetFields();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement de l\'assujetti.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function completeListAdresse() {

    if (suppressAdresseList.length > 0) {
        for (var i = 0; i < suppressAdresseList.length; i++) {
            adresse = new Object();
            adresse.code = suppressAdresseList[i].code;
            adresse.chaine = suppressAdresseList[i].chaine;
            adresse.numero = suppressAdresseList[i].numero;
            adresse.defaut = suppressAdresseList[i].defaut;
            adresse.codeAP = suppressAdresseList[i].codeAP;
            adresse.etat = suppressAdresseList[i].etat;
            tempAdresseList.push(adresse);
        }
    }

}

function checkLogin(login) {

    var isExist = true;
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        async: false,
        data: {
            'operation': 'checkLogin',
            'login': login
        },
        success: function (response)
        {
            if (response == '0' || response == '-1') {
                isExist = false;
            }
        },
        complete: function () {
        },
        error: function () {
            isExist = false;
        }
    });
    return isExist;
}

function loadInfosUser(codeUser) {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadInfosUser',
            'codeAgentEdition': codeUser
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Récupération des informations en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                var result = JSON.parse(JSON.stringify(response));

                cmbDivisions.html('');
                cmbDivisions.val('');


                var data = '<option value="0">--- Sélectionner une division ---</option>';

                if (userData.codeFonction == 'F0043') {
                    //data = '';
                    var value = 'BANQUIER';
                    data += '<option value="DIV00000011">' + value + '</option>';

                    cmbDivisions.html(data);
                    //cmbDivisions.val('DIV00000011');

                } else {

                    for (var i = 0; i < divisionList.length; i++) {

                        data += '<option value="' + divisionList[i].codeDivision + '">' + divisionList[i].intituleDivision + '</option>';
                    }

                    cmbDivisions.html(data);
                    cmbDivisions.val(result.divisionCode);
                }



                siteList = JSON.parse(userData.allSiteList);

                var datasite = '<option value ="0">-- Sélectionner --</option>';
                for (var i = 0; i < siteList.length; i++) {
                    if (siteList[i].SiteCode == result.codeSite) {
                        datasite += '<option value =' + siteList[i].SiteCode + '>' + siteList[i].siteName.toUpperCase() + '</option>';
                    }
                }

                cmbSite.html(datasite);
                cmbSite.val(userData.codeSite);

                cmbFonction.val(result.codeFonction);
                cmbSite.val(result.codeSite);
                cmbService.val(result.codeService);
                cmbUnitAdm.val(result.codeUa);
                cmbDistrinct.val(result.codeDistrict);
                cmbDivisions.val(result.codeDivision);
                cmbSite.val(result.codeSite);
                inputNom.val(result.nom_agent);
                inputPrenom.val(result.prenom_agent);
                inputTelephone.val(result.telephone);
                inputEmail.val(result.email);
                inputLogin.val(result.login);
                inputMatricule.val(result.matricule);
                inputGrade.val(result.grade);
                if (result.etat == '1') {
                    checkBoxActiverCompte.attr('checked', "true");
                } else {
                    checkBoxActiverCompte.removeAttr('checked');
                }

                if (result.typeSite == '1') {

                    isPeageSite = '1';

                    var data = '<option value ="0">-- Sélectionner --</option>';

                    for (var i = 0; i < dataSitePeages.length; i++) {
                        data += '<option value =' + dataSitePeages[i].codeSite + '>' + dataSitePeages[i].libelleSite.toUpperCase() + '</option>';
                    }

                    cmbSitePeage.html(data);
                    idDivSitePeage.attr('style', 'display:inline');
                    idDivSitePeage.attr('class', 'form-group');

                    if (result.codeSitePeage !== '') {
                        cmbSitePeage.val(result.codeSitePeage);
                    } else {
                        cmbSitePeage.val('0');
                    }

                } else {
                    isPeageSite = '0';
                    cmbSitePeage.val(empty);
                    idDivSitePeage.attr('style', 'display:none');
                }

                if (result.adresses !== '') {
                    var adressesPersonneList = $.parseJSON(result.adresses);
                    for (var i = 0; i < adressesPersonneList.length; i++) {
                        adresse = new Object();
                        adresse.code = adressesPersonneList[i].code;
                        adresse.chaine = adressesPersonneList[i].chaine;
                        adresse.numero = adressesPersonneList[i].numero;
                        adresse.defaut = adressesPersonneList[i].defaut;
                        adresse.codeAP = adressesPersonneList[i].codeAP;
                        adresse.etat = '1';
                        tempAdresseList.push(adresse);
                    }
                    printAdressePersonne(tempAdresseList);
                    btnRéinitialiserMotPasse.attr('style', 'display:inline');
                }

                $.unblockUI();
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function resetPasswordUser() {
    alertify.confirm('Etes-vous sûre de vouloir réunitialiser le mot de passe de cet utilisateur ?', function () {

        $.ajax({
            type: 'POST',
            url: 'utilisateur_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'resetPasswordUser',
                'userID': codeAgentEdition,
                'agentMaj': userData.idUser

            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Réunitialisation en cours...</h5>'});
            },
            success: function (response)
            {

                $.unblockUI();
                if (response == '-1' || response == '0') {
                    showResponseError();
                    return;
                } else {

                    alertify.alert('La réuntialisation du mot de passe s\'est effectuée avec succès.');
                    //resetFields();
                }

            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });
}

function loadDivision() {
    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'userId': userData.idUser,
            'operation': 'getDivision'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucune données retrouvée.');
                return;
            }

            divisionList = $.parseJSON(JSON.stringify(response));

            var data = '<option value="0">--- Sélectionner une division ---</option>';

            if (userData.codeFonction == 'F0043') {
                //data = '';
                var value = 'BANQUIER';
                data += '<option value="DIV00000011">' + value + '</option>';

                cmbDivisions.html(data);
                //cmbDivisions.val('DIV00000011');

            } else {
                for (var i = 0; i < divisionList.length; i++) {
                    data += '<option value="' + divisionList[i].codeDivision + '">' + divisionList[i].intituleDivision + '</option>';
                }
                cmbDivisions.html(data);
            }


        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}
function loadSiteByDivision(codeDivisions) {
    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getsiteByDivision',
            'codeDivision': codeDivisions
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                cmbSite.html(empty);
                cmbSite.val(empty);
                var value = '<span style="font-weight:bold">' + $('#cmbDivisions option:selected').text().toUpperCase() + '</span>';

                alertify.alert('Cette division : ' + value + ', n\'a pas de bureaux. Pour continuer, veuillez y attacher les bureaux.');
                return;
            }

            siteList = $.parseJSON(JSON.stringify(response));

            var datasite = '<option value ="0">-- Sélectionner --</option>';
            for (var i = 0; i < siteList.length; i++) {
                datasite += '<option value =' + siteList[i].codeSite + '>' + siteList[i].libelleSite.toUpperCase() + '</option>';
            }
            cmbSite.html(datasite);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

