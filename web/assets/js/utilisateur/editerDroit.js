/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tableEditerDroit;

var cmbModule, codeResearchModule;

var btnEnregistrerDroit, btnSearchDroit;

var inputSearchDroit, inputCodeDroit, inputIntituleDroit;

var codeDroit = '';

var selectDroitData;
var codeModule;
var moduleList;
var btnInitDroit;

$(function () {

    mainNavigationLabel.text('UTILISATEUR');
    secondNavigationLabel.text('Gestion des droits d\'acces');

    tableEditerDroit = $('#tableEditerDroit');

    cmbModule = $('#cmbModule');

    inputSearchDroit = $('#inputSearchDroit');
    inputCodeDroit = $('#inputCodeDroit');
    inputIntituleDroit = $('#inputIntituleDroit');

    btnEnregistrerDroit = $('#btnEnregistrerDroit');
    btnSearchDroit = $('#btnSearchDroit');
    btnInitDroit = $('#btnInitDroit');

    cmbModule.on('change', function (e) {
        codeResearchModule = cmbModule.val();
    });

    btnInitDroit.click(function (e) {

        e.preventDefault();

        cmbModule.val('0');
        inputCodeDroit.val('');
        inputIntituleDroit.val('');
        inputCodeDroit.attr('readonly', false);
        codeDroit = '';
        codeModule = '';

    });

    btnEnregistrerDroit.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbModule.val() == '' || cmbModule.val() == 'null' || cmbModule.val() == '0') {
            alertify.alert('Veuillez selectionner un module d\'appartenance du droit.');
            return;
        }

        if (inputCodeDroit.val() == '' || inputCodeDroit.val() == 'null') {
            alertify.alert('Veuillez saisir le code du droit');
            return;
        }

        if (inputIntituleDroit.val() == '') {
            alertify.alert('Veuillez saisir le libellé du droit');
            return;
        }

        var action;
        if (codeDroit == '') {
            action = ' enregistrer ce droit ?';
        } else {
            action = ' modifier ce droit ?';
        }

        var message = 'Etes-vous sûr de vouloir ' + action;

        alertify.confirm(message, function () {
            saveDroit(1);
        });
    });

    loadModule();
    loadDroits();


});

function loadDroits() {


    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getDroits'

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var droitList = JSON.parse(JSON.stringify(response));

                printDroit(droitList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}


function printDroit(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">CODE</th>';
    tableContent += '<th scope="col">DROIT</th>';
    tableContent += '<th scope="col">MODULE</th>';
    tableContent += '<th hidden="true" scope="col">Code module</th>';
    tableContent += '<th hidden="true" scope="col">Code droit</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="width:15%;vertical-align:middle">' + data[i].code_droit.toUpperCase() + '</td>';
        tableContent += '<td style="width:85%;vertical-align:middle">' + data[i].intitule_droit.toUpperCase() + '</td>';
        tableContent += '<td style="width:40%;vertical-align:middle">' + data[i].intituleModule.toUpperCase() + '</td>';
        tableContent += '<td hidden="true">' + data[i].fkModule + '</td>';
        tableContent += '<td hidden="true">' + data[i].code_droit + '</td>';
        tableContent += '</tr>';
    }
    
    tableContent += '</tbody>';
    tableEditerDroit.html(tableContent);

    var myDt = tableEditerDroit.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par Intitulé droit _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 50,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        columnDefs: [
            {"visible": false, "targets": 2}
        ],
        order: [[2, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="6">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableEditerDroit tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        codeModule = myDt.row(this).data()[3];

        for (var i = 0; i < moduleList.length; i++) {

            if (moduleList[i].codeModule == codeModule) {
                cmbModule.val(codeModule);
            }
        }

        selectDroitData = new Object();
        selectDroitData.code_droit = myDt.row(this).data()[0].toUpperCase();
        selectDroitData.intitule_droit = myDt.row(this).data()[1].toUpperCase();
        selectDroitData.intitule_module = myDt.row(this).data()[2].toUpperCase();
        selectDroitData.fkModule = codeModule;

        preparerModification();
    });
}

function preparerModification() {

    codeDroit = selectDroitData.code_droit;
    inputCodeDroit.val(selectDroitData.code_droit);
    inputIntituleDroit.val(selectDroitData.intitule_droit);

    inputCodeDroit.attr('readonly', true);
}

function loadModule() {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadModule'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            moduleList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--</option>';

            for (var i = 0; i < moduleList.length; i++) {
                data += '<option value="' + moduleList[i].codeModule + '">' + moduleList[i].intituleModule.toUpperCase() + '</option>';
            }
            cmbModule.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function saveDroit(etat) {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveDroit',
            'codeModule': cmbModule.val(),
            'idUser': userData.idUser,
            'code_droit': codeDroit == '' ? inputCodeDroit.val().trim() : codeDroit,
            'etat': etat,
            'intitule_droit': inputIntituleDroit.val().trim()


        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement des droits en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('L\'enregistrement du droit s\'est effectué avec succès.');

                    cmbModule.val('0');
                    inputCodeDroit.val('');
                    inputIntituleDroit.val('');
                    inputCodeDroit.attr('readonly', false);
                    codeDroit = '';
                    codeModule = '';

                    loadDroits();

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement des droits.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function removeDroit(code, intitule, fkModule) {

    alertify.confirm('Etes-vous sûrs de vouloir désactiver ce droit ?', function () {
        codeDroit = code;
        inputCodeDroit.val(intitule);
        if (fkModule != '') {
            cmbModule.val(fkModule);
        }
        saveDroit(0);
    });
}