/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function () {

    codeAvis = '';
    registerType = '';

    tableTaxation = $('#tableTaxation');
    tableDetailTaxation = $('#tableDetailTaxation');

    inputResearchTaxation = $('#inputResearchTaxation');
    inputResearchTaxation.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
    inputObservation = $('#inputObservation');
    inputObservation.val('');

    typeResearch = $('#typeResearch');
    btnResearchTaxation = $('#btnResearchTaxation');
    btnClosedTaxation = $('#btnClosedTaxation');
    btnCallModalSearchAvanded = $('#btnCallModalSearchAvanded');
    btnResearchTaxationClosed = $('#btnResearchTaxationClosed');
    btnResearchTaxationRejected = $('#btnResearchTaxationRejected');

    modalDetailNoteCalcul = $('#modalDetailNoteCalcul');
    modalRechercheAvanceeNC = $('#modalRechercheAvanceeModelTwo');

    lblExercice = $('#lblExercice');
    lblNoteCalcul = $('#lblNoteCalcul');
    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAdress = $('#lblAdress');
    lblAmount = $('#lblAmount');

    cmbAvis = $('#cmbAvis');

    isAdvance = $('#isAdvance');
    lblSite = $('#lblSite');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    lblInfoValidation = $('#lblInfoValidation');
    lblInfoOrdonnateur = $('#lblInfoOrdonnateur');
    lblInfoPaiement = $('#lblInfoPaiement');
    lblInfoTaxateur = $('#lblInfoTaxateur');
    tableBienTaxation2 = $('#tableBienTaxation2');

    btnVoirDeclaration = $('#btnVoirDeclaration');

    tableComplementInfoTaxation = $('#tableComplementInfoTaxation');
    divTableComplementInfoTaxation = $('#divTableComplementInfoTaxation');

    cmbAvis.on('change', function (e) {
        codeAvis = cmbAvis.val();
    });

    loadAllAvis();

    btnResearchTaxation.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (inputResearchTaxation.val() === empty || inputResearchTaxation.val().length < SEARCH_MIN_TEXT) {
            showEmptySearchMessage();
            return;
        }
        loadTaxationNotClosed();
    });

    btnResearchTaxationClosed.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (inputResearchTaxation.val() === "" || inputResearchTaxation.val().length < SEARCH_MIN_TEXT) {
            alertify.alert('Veuillez fournir un critère de recherche avec au moins ' + SEARCH_MIN_TEXT + ' caractères');
            return;
        }
        loadTaxationClosed();

    });

    btnResearchTaxationRejected.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (inputResearchTaxation.val() === "" || inputResearchTaxation.val().length < SEARCH_MIN_TEXT) {
            alertify.alert('Veuillez fournir un critère de recherche avec au moins ' + SEARCH_MIN_TEXT + ' caractères');
            return;
        }
        loadTaxationRejected();
    });

    typeResearch.on('change', function (e) {
        inputResearchTaxation.val('');

        if (typeResearch.val() === '1') {
            inputResearchTaxation.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
        } else if (typeResearch.val() === '2') {
            inputResearchTaxation.attr('placeholder', 'Veuillez saisir le numéro de la note de taxation');
        }
    });

    inputResearchTaxation.keypress(function (e) {
        if (e.keyCode === 13) {
            btnResearchTaxation.trigger('click');
        }
    });

    btnCallModalSearchAvanded.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('show');
    });

    btnClosedTaxation.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        callDialogConfirmClosedTaxation();

    });

    btnSimpleSearch = $('#btnSimpleSearch');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    btnShowModalAdvencedRechearch = $('#btnRechercheAvancee');

    btnShowModalAdvencedRechearch.click(function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('show');
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        taxationByAdvancedSearch();
    });

    var urlNcParam = getUrlParameter('nc');

    if (jQuery.type(urlNcParam) !== 'undefined') {

        ncParam = atob(urlNcParam);
        typeResearch.val('2');
        inputResearchTaxation.val(ncParam);

        if (getRegisterType() === 'RNC') {
            btnResearchTaxation.trigger('click');
        } else if (getRegisterType() === 'RNCC') {
            btnResearchTaxationClosed.trigger('click');
        }

        typeResearch.attr('disabled', true);
        inputResearchTaxation.attr('readonly', true);
    }

    btnAdvencedSearch.trigger('click');

    loadTaxationTable('');
    loadDetailTaxationTable('');
});

function loadTaxationTable(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center;width:6%"scope="col">EXERCICE</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">SECTEUR</th>';
    tableContent += '<th style="text-align:left;width:11%"scope="col">NOTE DE TAXATION</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">DATE DE CREATION</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">NTD</th>';
    tableContent += '<th style="text-align:left;width:14%"scope="col">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left;width:18%"scope="col">TAXE</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:center;width:6%"scope="col"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var sumCDF = 0;
    var sumUSD = 0;

    for (var i = 0; i < result.length; i++) {

        var firstLineAB = '';

        if (result[i].libelleArticleBudgetaire.length > 250) {
            firstLineAB = result[i].libelleArticleBudgetaire.substring(0, 250).toUpperCase() + ' ...';
        } else {
            firstLineAB = result[i].libelleArticleBudgetaire.toUpperCase();
        }

        if (result[i].devisePalier === 'CDF') {
            sumCDF += result[i].totalAmount2;
        } else if (result[i].devisePalier === 'USD') {
            sumUSD += result[i].totalAmount2;
        }


        var btnDisplayBienTaxation = '';

        if (result[i].bienTaxationExist == 1) {
            btnDisplayBienTaxation = '<br/><br/><button class="btn btn-warning" onclick="loadBiensTaxation(\'' + result[i].noteCalcul + '\')"><i class="fa fa-home"></i>&nbsp;&nbsp;Afficher le(s) bien(s)</button>';
        }


        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;width:6%;text-align:center">' + result[i].exerciceFiscal + '</td>';
        tableContent += '<td style="vertical-align:middle;width:15%">' + result[i].libelleService + '</td>';
        tableContent += '<td style="vertical-align:middle;width:11%">' + result[i].noteCalcul + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%">' + result[i].dateCreate + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%">' + result[i].userName + '</td>';
        tableContent += '<td style="vertical-align:middle;width:14%">' + result[i].nom + ' (' + result[i].libelleFormeJuridique + ')' + '</td>';
        tableContent += '<td style="text-align:left ;vertical-align:middle;title="' + result[i].libelleArticleBudgetaire + '"><span style="font-weight:bold;"></span>' + firstLineAB + '</td>';
        tableContent += '<td style="text-align:right;vertical-align:middle;width:10%">' + formatNumber(result[i].totalAmount2, result[i].devisePalier) + '</td>';
        tableContent += '<td style="text-align:center;vertical-align:middle"><button class="btn btn-primary" onclick="displayDetailTaxation(\'' + result[i].noteCalcul + '\',\'' + result[i].declarationExist + '\',\'' + result[i].complementExist + '\')"><i class="fa fa-list"></i>&nbsp;&nbsp;Afficher les détails</button>' + btnDisplayBienTaxation + '</td>';
        tableContent += '</tr>';

    }
    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="7" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th></th></tr>';

    tableContent += '</tfoot>';

    tableTaxation.html(tableContent);

    var myDataTable = tableTaxation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 25,
        columnDefs: [
            {"visible": false, "targets": 5}
        ],
        order: [[5, 'asc']],
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(7).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD'));
        },
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(5, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

}

function loadTaxationNotClosed() {

    var viewAllSite = controlAccess('VIEW_ALL_SITES_NC');
    var viewAllService = controlAccess('VIEW_ALL_SERVICES_NC');
    registerType = getRegisterType();

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputResearchTaxation.val(),
            'typeSearch': typeResearch.val(),
            'typeRegister': registerType,
            'allSite': viewAllSite,
            'allService': viewAllService,
            'codeSite': userData.SiteCode,
            'codeService': userData.serviceCode,
            'userId': userData.idUser,
            'operation': 'researchTaxation'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'})
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {

                    if (dataTaxations.length > 0) {
                        dataTaxations = '';
                        loadTaxationTable('');
                    }
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fournis.');
                } else {
                    dataTaxations = JSON.parse(JSON.stringify(response));
                    if (dataTaxations.length > 0) {

                        switch (registerType) {
                            case 'RNC':
                                btnClosedTaxation.show();
                                break;
                            case 'RNCC':
                            case 'RNCR':
                                btnClosedTaxation.hide();
                                break;
                        }
                        loadTaxationTable(dataTaxations);
                    } else {
                        btnClosedTaxation.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });

}

function loadTaxationClosed() {

    var viewAllSite = controlAccess('VIEW_ALL_SITES_NC');
    var viewAllService = controlAccess('VIEW_ALL_SERVICES_NC');
    registerType = getRegisterType();

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputResearchTaxation.val(),
            'typeSearch': typeResearch.val(),
            'typeRegister': registerType,
            'allSite': viewAllSite,
            'allService': viewAllService,
            'codeSite': userData.SiteCode,
            'codeService': userData.serviceCode,
            'userId': userData.idUser,
            'operation': 'researchTaxation'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'})
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {

                    if (dataTaxations.length > 0) {
                        dataTaxations = '';
                        loadTaxationTable('');
                    }
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fournis.');
                } else {
                    dataTaxations = JSON.parse(JSON.stringify(response));
                    if (dataTaxations.length > 0) {
                        btnClosedTaxation.hide();
                        loadTaxationTable(dataTaxations);
                    } else {
                        btnClosedTaxation.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });

}

function loadTaxationRejected() {

    var viewAllSite = controlAccess('VIEW_ALL_SITES_NC');
    var viewAllService = controlAccess('VIEW_ALL_SERVICES_NC');
    registerType = getRegisterType();

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputResearchTaxation.val(),
            'typeSearch': typeResearch.val(),
            'typeRegister': registerType,
            'allSite': viewAllSite,
            'allService': viewAllService,
            'codeSite': userData.SiteCode,
            'codeService': userData.serviceCode,
            'userId': userData.idUser,
            'operation': 'researchTaxation'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'})
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {

                    if (dataTaxations.length > 0) {
                        dataTaxations = '';
                        loadTaxationTable('');
                    }
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fournis.');
                } else {
                    dataTaxations = JSON.parse(JSON.stringify(response));
                    if (dataTaxations.length > 0) {
                        btnClosedTaxation.hide();
                        loadTaxationTable(dataTaxations);
                    } else {
                        btnClosedTaxation.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });

}

function displayDetailTaxation(noteCalcul, declarationExist, complementInfo) {

    if (declarationExist == '1') {
        btnVoirDeclaration.attr('style', 'display:inline');
    } else {
        btnVoirDeclaration.attr('style', 'display:none');
    }

    for (i = 0; i < dataTaxations.length; i++) {
        if (dataTaxations[i].noteCalcul === noteCalcul) {

            numeroNcSelected = noteCalcul;
            lblNoteCalcul.text('N° taxation : ' + dataTaxations[i].noteCalcul);
            lblNameResponsible.text('Assujetti : ' + dataTaxations[i].nom);
            lblAdress.html('Adresse : ' + dataTaxations[i].adresseAssujetti);
            lblAmount.html(formatNumber(dataTaxations[i].totalAmount2, dataTaxations[i].devisePalier));

            lblInfoTaxateur.text('Taxer par : ' + dataTaxations[i].agentTaxateur);
            lblInfoValidation.text('Clôturer par : ' + dataTaxations[i].agentCloture);
            lblInfoOrdonnateur.text('Ordonnancer par : ' + dataTaxations[i].agentOrdonnateur);
            lblInfoPaiement.text('Infos paiement : ' + dataTaxations[i].noteCalcul);

            loadDetailTaxationTable($.parseJSON(dataTaxations[i].listDetailNcs), complementInfo);

            switch (registerType) {

                case 'RNC':

                    inputObservation.val('');
                    cmbAvis.val('');
                    cmbAvis.attr('disabled', false);
                    inputObservation.attr('disabled', false);
                    btnClosedTaxation.show();

                    break;

                case 'RNCC':
                case 'RNCR':

                    inputObservation.val(dataTaxations[i].observationTaxation);
                    cmbAvis.val(dataTaxations[i].idAvis);
                    cmbAvis.attr('disabled', true);
                    inputObservation.attr('disabled', true);
                    btnClosedTaxation.hide();

                    break;

                case 'RNCRO':

                    inputObservation.val(dataTaxations[i].observationOrdonnancement);
                    cmbAvis.val(dataTaxations[i].idAvis);
                    cmbAvis.attr('disabled', true);
                    inputObservation.attr('disabled', true);
                    btnClosedTaxation.hide();

                    break;

            }

            btnVoirDeclaration.click(function (e) {
                e.preventDefault();
                initPrintData(dataTaxations[i].declarationDocumentList);
            });

            modalDetailNoteCalcul.modal('show');

            return;
        }
    }
}

function loadAllAvis() {

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'getListAvis'
        },
        beforeSend: function () {
        },
        success: function (response)
        {
            setTimeout(function () {

                if (response == '-1') {
                    showResponseError();
                    return;
                }
                dataAvis = JSON.parse(JSON.stringify(response));
                var dataAvisDisplay = '<option value ="0">-- Sélectionner un avis --</option>';
                for (var i = 0; i < dataAvis.length; i++) {

                    dataAvisDisplay += '<option value =' + dataAvis[i].idAvis + '>' + dataAvis[i].libelleAvis + '</option>';
                }
                cmbAvis.html(dataAvisDisplay);

            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            showResponseError();
        }
    });
}

function loadDetailTaxationTable(result, exist) {


    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:30%"scope="col">TAXE</th>';
    tableContent += '<th style="text-align:right;width:6%"scope="col">BASE</th>';
    tableContent += '<th style="text-align:center;width:5%"scope="col">NBR.ACTE</th>';
    tableContent += '<th style="text-align:center;width:6%"scope="col">UNITE</th>';
    tableContent += '<th style="text-align:right;width:6%"scope="col">TAUX</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">MONTANT DÛ</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var firstLineAB = '';
        deviseSelected = result[i].devisePalier;

        if (result[i].libelleArticleBudgetaire.length > 250) {
            firstLineAB = result[i].libelleArticleBudgetaire.substring(0, 250).toUpperCase() + ' ...';
        } else {
            firstLineAB = result[i].libelleArticleBudgetaire.toUpperCase();
        }

        tableContent += '<tr>';

        tableContent += '<td style="text-align:left;width:28%;vertical-align:middle title="' + result[i].libelleArticleBudgetaire + '"><span style="font-weight:bold;"></span>' + firstLineAB + '</td>';
        tableContent += '<td style="text-align:right;width:6%;vertical-align:middle">' + formatNumberOnly(result[i].baseCalcul) + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + result[i].quantity + '</td>';
        tableContent += '<td style="text-align:center;width:6%;vertical-align:middle">' + result[i].libelleUnite + '</td>';
        tableContent += '<td style="text-align:right;width:6%;vertical-align:middle">' + result[i].tauxPalier + '</td>';
        tableContent += '<td style="text-align:right;width:12%;vertical-align:middle">' + formatNumber(result[i].amount, result[i].devisePalier) + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableDetailTaxation.html(tableContent);

    var myDataTable = tableDetailTaxation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le detail des articles budgétaires est vide",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });

    if (exist == '1') {
        displayInfosComplement(numeroNcSelected);
    } else {
        divTableComplementInfoTaxation.attr('style', 'display: none');
    }

}

function callDialogConfirmClosedTaxation() {

    if (codeAvis === '' || codeAvis === '0') {
        alertify.alert('Veuillez d\'abord sélectionner votre avis sur la taxation en cours de clôture.');
        return;
    }

    if (cmbAvis.val() === 'AV0022015' && inputObservation.val() == empty) {

        alertify.alert('Veuillez d\'abord motiver la raison de votre rejet');
        return;

    }

    alertify.confirm('Etes-vous sûre de vouloir clôturer cette taxation ?', function () {
        closedTaxation();
    });
}

function closedTaxation() {
    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'noteCalcul': numeroNcSelected,
            'idAvis': codeAvis,
            'observationTaxation': inputObservation.val(),
            'userId': userData.idUser,
            'operation': 'closedTaxation'
        },
        beforeSend: function () {
            modalDetailNoteCalcul.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Clôture de la taxation en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                modalDetailNoteCalcul.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                }
                if (response == '0') {
                    alertify.alert('Echec opération. Une erreur s\'est produite lors de la clôture de la note de taxation');
                    return;
                } else {
                    modalDetailNoteCalcul.modal('hide');
                    refreshTaxationList();
                    alertify.alert('Cette note de taxation est clôturée avec succès.');
                    inputObservation.val('');
                    codeAvis = '';
                    cmbAvis.val('');
                    return;
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            modalDetailNoteCalcul.unblock();
            showResponseError();
        }
    });
}

function refreshTaxationList() {
    if (dataTaxations.length > 0) {

        for (i = 0; dataTaxations.length; i++) {
            if (dataTaxations[i].noteCalcul === numeroNcSelected) {
                dataTaxations.splice(i, 1);
                if (dataTaxations.length > 0) {
                    loadTaxationTable(dataTaxations);
                } else {
                    loadTaxationTable('');
                }
                return;
            }
        }
    }
}

function taxationByAdvancedSearch() {

    if (inputDateDebut.val() === '') {
        alertify.alert('Veuillez d\'abord indiquer la date de début de la recherche.');
        return;
    }

    if (inputdateLast.val() === '') {
        alertify.alert('Veuillez d\'abord indiquer la date fin de la recherche.');
        return;
    }
    registerType = getRegisterType();

    lblSite.html($('#selectSite option:selected').text().toUpperCase());
    lblService.html($('#selectService option:selected').text().toUpperCase());
    lblDateDebut.html(inputDateDebut.val());
    lblDateFin.html(inputdateLast.val());

    isAdvance.attr('style', 'display: block');

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'SiteCode': selectSite.val(),
            'serviceCode': selectService.val(),
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'typeRegister': registerType,
            'operation': 'researchAdvancedTaxation'
        },
        beforeSend: function () {
            modalRechercheAvanceeNC.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            modalRechercheAvanceeNC.unblock();

            setTimeout(function () {

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    modalRechercheAvanceeNC.modal('hide');
                    dataTaxations = '';
                    loadTaxationTable('');
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fournis.');

                } else {

                    dataTaxations = JSON.parse(JSON.stringify(response));

                    if (dataTaxations.length > 0) {
                        btnClosedTaxation.show();
                        modalRechercheAvanceeNC.modal('hide');
                        loadTaxationTable(dataTaxations);
                    } else {
                        btnClosedTaxation.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalRechercheAvanceeNC.unblock();
            showResponseError();
        }

    });


}


function displayInfosComplement(numero) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:30%"scope="col">TRANSITEUR</th>';
    tableContent += '<th style="text-align:left;width:20%"scope="col">NATURE DE PRODUIT</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">N° PLAQUE CAMION</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">MODE DE PAIEMENT</th>';

    tableContent += '<th style="text-align:center;width:10%"scope="col">TONNAGE SORTIE</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">MONTANT TONNE</th>';
    tableContent += '<th style="text-align:left;width:20%"scope="col">AGENT VALIDATION</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < dataTaxations.length; i++) {

        if (dataTaxations[i].noteCalcul == numero) {

            var dataComplementInfoList = JSON.parse(dataTaxations[i].complementList);

            for (var j = 0; j < dataComplementInfoList.length; j++) {

                var agentInfo = 'Par : ' + '<span style="font-weight:bold">' + dataComplementInfoList[j].agentValidate + '</span>';
                var dateInfo = 'Le : ' + '<span style="font-weight:bold">' + dataComplementInfoList[j].dateValidation + '</span>';

                var infoUser = agentInfo + '<br/>' + dateInfo;

                tableContent += '<tr>';

                tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + dataComplementInfoList[j].transporteur + '</td>';
                tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + dataComplementInfoList[j].produit + '</td>';
                tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + dataComplementInfoList[j].plaque + '</td>';
                tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + dataComplementInfoList[j].modePaiement + '</td>';


                tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + formatNumberOnly(dataComplementInfoList[j].tonnageSortie) + '</td>';
                tableContent += '<td style="text-align:right;width:10%;vertical-align:middle">' + formatNumber(dataComplementInfoList[j].montantTonnageSortie, deviseSelected) + '</td>';
                tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + infoUser + '</td>';

                tableContent += '</tr>';

                tableContent += '</tbody>';
                tableComplementInfoTaxation.html(tableContent);

                var myDataTable = tableComplementInfoTaxation.DataTable({
                    language: {
                        processing: "Traitement en cours...",
                        track: "Rechercher&nbsp;:",
                        lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                        info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                        infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        infoPostFix: "",
                        loadingRecords: "Chargement en cours...",
                        zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        emptyTable: "Le detail des articles budgétaires est vide",
                        search: "Rechercher par N° document _INPUT_  ",
                        paginate: {
                            first: "Premier",
                            previous: "Pr&eacute;c&eacute;dent",
                            next: "Suivant",
                            last: "Dernier"
                        },
                        aria: {
                            sortAscending: ": activer pour trier la colonne par ordre croissant",
                            sortDescending: ": activer pour trier la colonne par ordre décroissant"
                        }
                    },
                    info: false,
                    destroy: true,
                    searching: false,
                    paging: false,
                    lengthChange: false,
                    tracking: false,
                    ordering: false,
                    pageLength: 7,
                    lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
                    datalength: 3
                });
            }

            divTableComplementInfoTaxation.attr('style', 'display: inline');

        }
    }
}