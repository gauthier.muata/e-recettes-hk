<%-- 
    Document   : registreDemandeEchelonnement
    Created on : 30 juil. 2018, 15:48:02
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des demandes d'échelonnements </title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>
        <style>
            .details-control {
                background: url('assets/images/details_open.png') no-repeat center center;
                cursor: pointer;
            }
            tr.shown td.details-control {
                background: url('assets/images/details_close.png') no-repeat center center;
            }
        </style>
    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="ResearchType">
                                <option value="1" >Assujetti</option>
                                <option value="2">Titre de perception</option>
                                <option value="3">Référence de la lettre</option>
                            </select>
                            <div class="input-group" >
                                <input type="text" class="form-control" style="width: 300px" id="ResearchValue" placeholder="Nom de l'assujetti" readonly="true">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit" id="btnSimpleSearch"><i class="fa fa-search"></i> Rechercher</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnShowAdvancedSerachModal" style="margin-right: 20px" class="btn btn-warning pull-right">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée</button>
                </div>

                <hr/>

                <div class="row" id="isAdvance" style="display: none">
                    <div  class="col-lg-12">
                        <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour :</label>
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <div class="row" style="margin-left: 5px;">
                                    <div class="journal" style="float: right">

                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Site : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="lblSite" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Service : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblService" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black" >
                                            &nbsp;&nbsp;&nbsp;&nbsp;Du : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="lblDateDebut" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black" >
                                            &nbsp;&nbsp;&nbsp;&nbsp;Au : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="lblDateFin" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                    </div>
                                </div>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <div >
                    <table id="tableDemandeEchelonnement" class="table table-bordered">
                    </table>
                </div>

            </div>
        </div>

        <div class="modal fade" id="modalTraitementEchelonnement"
             data-backdrop="static" data-keyboard="false"
             tabindex="-1" 
             role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document" style="width: 50%;margin-top: 70px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">TRAITEMENT ECHELONNEMENT</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> LES D&Eacute;TAILS DU TITRE DE PERCEPTION</h4>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <br/>
                                            <div class="form-group" style="margin-top: -10px">
                                                <span style="font-size: 16px">N° TITRE DE PERCEPTION :</span>
                                                <b id="lblNumTitreModalTraitementEchelonnement" style="font-size: 16px">&nbsp;</b>
                                            </div><br/>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group" style="margin-top: -10px;font-size: 20px;">
                                                        <span>MONTANT DU :</span>
                                                        <b style="color:red" id="lblMontantDuModalTraitementEchelonnement">&nbsp;</b>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group" style="margin-top: -10px">
                                                        <span style="font-size: 16px">ETAT :</span>
                                                        <b id="lblEtatModalTraitementEchelonnement">&nbsp;</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="panel-footer">
                                </div>

                            </div>

                        </div>

                        <ul class="nav nav-tabs">
                            <li id="tab1" class="active"><a data-toggle="tab" href="#contentTab1" >D&eacute;cison</a></li>
                            <li id="tab2"><a data-toggle="tab" href="#contentTab2" >Echelonnement</a></li>
                        </ul>

                        <div class="tab-content">

                            <div id="contentTab1" class="tab-pane fade in active">
                                <div class="panel-body">
                                    <div id="divRadio">
                                        <br/>
                                        <input class="form-check-input " type="radio" name="radioValider" id="radioValider" value="1" >
                                        <label style="margin-right: 40px">APPROUVER</label>
                                        <input class="form-check-input " type="radio" name="radioValider" id="radioRejeter" value="0" >
                                        <label>REJETER</label>
                                        <hr/>
                                    </div>      
                                    <label style="font-weight: normal">OBSERVATION (Mentionnez la référence de la décision) :</label>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="txtObservalitionValidation"></textarea>
                                    </div> 
                                </div>
                            </div>

                            <div id="contentTab2" class="tab-pane fade">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label style="font-weight: normal">
                                                Nombre de tranches <span style="color:red"> (Valeur comprise entre 2 et 6 par défaut) *</span>
                                            </label>
                                            <input type="number" class="form-control" id="inputNombreIntercalaire" min="2"/>
                                        </div>
                                    </div>
                                    <table id="tableEchelonnement" class="table table-bordered" >
                                    </table>
                                </div>

                                <br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;<span id="lblAmountInteretEchelonnement" style="display:none;color:red;font-size: 15px"></span>
                                
                            </div>

                        </div>

                        <div class="panel-footer" style="text-align: right"> 
                            <button                    
                                type="button" 
                                class="btn btn-warning pull-left" 
                                id="btnJoindreArchiveDecison">
                                <i class="fa fa-download"></i>  
                                Joindre les pièces jointes (Décision)
                            </button>
                            <a 
                                id="lblNbDocumentDecision"
                                class="pull-left"
                                style="font-style: italic;margin-top: 8px;margin-left: 8px;text-decoration: underline;color: blue">
                            </a>
                            <button type="button" class="btn btn-success" id="btnValiderTraitement"><i class="fa fa-check-circle"></i>&nbsp; Valider</button>
                            <button type="button" class="btn btn-secondary" 
                                    data-dismiss="modal" >Fermer</button>
                        </div>



                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalDetailEchelonnement"
             data-backdrop="static" data-keyboard="false"
             tabindex="-1" 
             role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document" style="width: 70%;margin-top: 60px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">DETAILS ECHELONNEMENT</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <br/>
                        <div class="panel panel-default"> &nbsp;
                            <div class="panel-heading">
                                <h4 class="panel-title"> LES D&Eacute;TAILS DES TITRES DE PERCEPTIONS</h4>
                            </div>
                            <div class="panel-wrapper collapse in">

                                <div class="panel-body">
                                    <label for="lblNameAssujettiModalDetailEchelonnement" style="font-weight: normal" >
                                        ASSUJETTI &nbsp;&nbsp;: 
                                    </label>
                                    <b id="lblNameAssujettiModalDetailEchelonnement">&nbsp;</b><br/><br/>
                                    <label style="font-weight: normal;color:red;font-style: italic" id="lblTxtApplyTenPercentPenalites" >
                                        <input type="checkbox" id="checkApplyTenPercent" name="checkApplyTenPercent" readonly="true"/>
                                    </label>

                                </div>
                                <div class="panel-footer" style="height: 60px">

                                    <button 
                                        type="button" 
                                        class="btn btn-primary pull-right" 
                                        id="btnShowArchiveEchelonnement">
                                        <i class="fa fa-archive"></i> &nbsp;
                                        Voir les pi&egrave;ces jointes
                                    </button>
                                </div>

                            </div>

                        </div>

                        <table id="tableDetailEchelonnement" class="table table-bordered" >

                        </table>

                        <div class="panel-footer" style="text-align: right"> 

                            <button type="button" 
                                    class="btn btn-secondary" 
                                    data-dismiss="modal" >Fermer</button>
                        </div>



                    </div>
                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>

        <%@include file="assets/include/modal/rechercheAvanceeNC.html" %>
        <%@include file="assets/include/modal/modalProrogationEcheance.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>
        <%@include file="assets/include/modal/observationModal.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>


        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script src="assets/lib/choosen/chosen.jquery.js" type="text/javascript"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script src="assets/lib/choosen/chosen.jquery.js" type="text/javascript"></script>
        <script src="assets/js/rechercheAvancee_1.js" type="text/javascript"></script>
        <script src="assets/js/uploading.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script src="assets/js/registreDemandeFractionnement.js" type="text/javascript"></script>

    </body>
</html>
