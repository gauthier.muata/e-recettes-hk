<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edition des pénalités</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>
                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-inline" role="form">
                            Intitulé : 
                            <input 
                                type="text" 
                                class="form-control" 
                                style="width: 500px" 
                                id="inputPenalite">
                            Type pénalité  : 

                            <select  class="form-control" id="selectTypePenalite" style="width: 350px">
                            </select>

                            <button 
                                class="btn btn-success" 
                                type="submit"id="btnSavePenalite">
                                <i class="fa fa-save"></i>  
                                Enregistrer
                            </button>

                            <button 
                                class="btn btn-default" 
                                type="submit"id="btnInitFields">
                                <i class="fa fa-close"></i> 
                                Initialiser
                            </button>

                        </form>
                    </div>
                </div><hr/>
                <div class="row">
                    <div class="col-lg-3">

                        <div style="margin-top: 10px">

                            <input  type="checkbox" id="checkBoxVisibleUser" style="margin-top: -90px"/>
                            <label style="font-weight: bold;font-size: 13.5px">Visible à l'utilisateur</label><br/>
                            <label style="font-size: 13px;font-weight: normal;font-style: italic;color: blue">Cette p&eacute;nalit&eacute; sera applicable par l'utilisateur. Au cas contrainre, le système prendra en charge l'applicabilit&eacute; de cette p&eacute;nalit&eacute;</label>
                        </div>
                    </div>

                    <div class="col-lg-3">

                        <div style="margin-top: 10px">

                            <input  type="checkbox" id="checkBoxActivatePenalite" style="margin-top: -90px"/>
                            <label style="font-weight: bold;font-size: 13.5px">Activer la p&eacute;nalit&eacute;</label><br/>
                            <label style="font-size: 13px;font-weight: normal;font-style: italic;color: blue">Au cas contrainte la  p&eacute;nalit&eacute; sera non applicable dans le système</label>
                        </div>
                    </div>

                    <div class="col-lg-3">

                        <div style="margin-top: 10px">

                            <input  type="checkbox" id="checkBoxVerifyTaux" style="margin-top: -90px"/>
                            <label style="font-weight: bold;font-size: 13.5px">Vérifier le taux par rapport au borne</label><br/>
                            <label style="font-size: 13px;font-weight: normal;font-style: italic;color: blue">Seulement pour les pénalités faisant appel à une échéance</label>
                        </div>
                    </div>

                    <div class="col-lg-3">

                        <div style="margin-top: 10px">

                            <input  type="checkbox" id="checkBoxApplyNumberMonth" style="margin-top: -90px"/>
                            <label style="font-weight: bold;font-size: 13.5px">Tenir compte de nombre des mois de retard</label><br/>
                            <label style="font-size: 13px;font-weight: normal;font-style: italic;color: blue">En cas d'application de la pénalité le système tiendra compte de nombre des mois de retard</label>
                        </div>
                    </div>
                </div><hr/>

                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-inline" role="form">
                            <input 
                                type="text" 
                                class="form-control"
                                placeholder="Veuillez rechercher une pénalité ici..."
                                style="width: 1200px" 
                                id="inputResearchPenalite">


                            <button 
                                class="btn btn-primary" 
                                type="submit"id="btnResearchPenalite">
                                <i class="fa fa-search"></i> 
                                Rechercher
                            </button>

                        </form>
                    </div>
                </div><hr/>

                <div class="journal" >
                    <table id="tablePenalite" class="table table-bordered">
                    </table>
                </div>
            </div>

        </div>
        <%@include file="assets/include/modal/header.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/gestionPenalite/editionPenalite.js"></script>
    </body>
</html>