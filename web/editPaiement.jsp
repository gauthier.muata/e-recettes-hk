<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <title>Edition d'un paiement</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div style="visibility: hidden; width: 100%" id="panelInformationNote">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="panel panel-primary">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DE L'ASSUJETTI
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body"> 
                                        <span>
                                            <span style="font-size: 15px">Type : &nbsp;</span>&nbsp
                                            <p id="lblLegalForm"
                                               style="font-weight: bold;color: black"></p>
                                        </span><br/>

                                        <span>
                                            <span style="font-size: 15px">Assujetti : &nbsp;</span>&nbsp   
                                            <p id="lblNameResponsible" style="font-weight: bold;color: black" ></p>
                                        </span><br/><br/>
                                        <span>
                                            <span style="font-size: 15px">Adresse : &nbsp;</span>&nbsp
                                            <p id="lblAddress"
                                               style="font-size: 14px;
                                               font-weight: bold;color: black"></p>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-primary">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DU TITRE DE PERCEPTION
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body"> 
                                        <span>
                                            <span style="font-size: 15px">Titre de perception : &nbsp;</span>&nbsp
                                            <p id="lblNotePerception"
                                               style="font-weight: bold;color: black"></p>
                                        </span><br/>

                                        <span>
                                            <span style="font-size: 15px">Type titre de perception : &nbsp;</span>&nbsp   
                                            <p id="lblTypeTitrePerception" style="font-weight: bold;color: black"></p>
                                        </span><br/><br/>
                                        <span>
                                            <span style="font-size: 15px">Taxe : &nbsp;</span>&nbsp
                                            <p id="lblArticleBudgetaire"
                                               style="font-size: 15px;
                                               font-weight: bold;color: black"></p>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DU PAIEMENT
                                    </h4>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <p/>
                                    <div class="panel-body"> 
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group" >
                                                    <label  for="inputSolde" style="font-weight: normal" >Net à payer : </label>
                                                    <b id="lblNetAPayer" style="">&nbsp; </b>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group" >
                                                    <label  for="inputSolde" style="font-weight: normal" >Montant payé : </label>
                                                    <b id="lblMontantPayer" style="">&nbsp; </b>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group" >
                                                    <label  for="inputSolde" style="font-weight: normal" >Solde : </label>
                                                    <b id="lblSolde" style="">&nbsp; </b>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-2">
                                                <label  for="lblInputBanaue" > Banque <span style="color: red">*</span> </label>
                                                <!--input type="text" class="form-control" id="inputBanque" readonly="true"/-->
                                                <select style="width: 100%" class="form-control" id="selectBankPaid">
                                                </select>
                                            </div>
                                            <div class="col-lg-3">
                                                <label  for="lblInputCompteBancaire">Compte bancaire <span style="color: red">*</span> </label>
                                                <!--input type="text" class="form-control" id="inputCompteBancaire"readonly="true"/-->
                                                <select style="width: 100%"  class="form-control" id="selectAccountBankPaid">
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label  for="lblInputBordereau">Référence paiement <span style="color: red">*</span></label>
                                                <input type="text" class="form-control" id="inputBordereau" required="true"/>                  
                                            </div>
                                            <div class="col-lg-2"  style="display: block">
                                                <div class="form-group">
                                                    <label  for="lblInputDateEmissionBordereau">Date du bordereau <span style="color: red">*</span></label>
                                                    <div class="input-group date " id="datePicker1" style="width: 100%;">
                                                        <input type="text" class="form-control" id="inputDateEmission" >
                                                        <div class="input-group-addon">
                                                            <span class="fa fa-th"></span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-lg-2">
                                                <label  for="lblInputMontantPercu" >Montant perçu <span style="color: red">*</span> </label>
                                                <input class="form-control" id="inputMontantPercu" type="number"/>
                                            </div>

                                            <!--div class="col-lg-1">
                                                <label  for="lblInputDevise" > Devise <span style="color: red">*</span> </label>
                                                <!--input type="text" class="form-control" id="inputBanque" readonly="true"/-->
                                                <!--select style="width: 100%" disabled="true" class="form-control" id="selectDevise" >
                                                    <option value="CDF">CDF</option>
                                                    <option value="USD">USD</option>
                                                </select-->
                                            </div-->
                                            
                                            <div class="col-lg-1">
                                                <label style="display: none" for="lblInputDevise" > Devise <span style="color: red;display: none">*</span> </label>
                                                <input class="form-control" id="selectDevise" readonly="true" style="display: none"/>
                                            </div>

                                        </div>

                                        <div class="row" style="display: none">
                                            <div class="col-lg-2">
                                                <label  for="lblInputMontantPercu1" > Montant perçu <span style="color: red">*</span> </label>
                                                <input class="form-control" readonly="true" id="inputMontantPercu"/>
                                            </div>
                                            <div class="col-lg-3" style="display: none">
                                                <label  for="lblInputNumeroAttestation">Numéro d'attestation du paiement </label>
                                                <input type="text" class="form-control" id="inputNumeroAttestation" required="true"/>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                <div class="panel-footer" style="margin-top: 15px;visibility: hidden;" id="footerPanelPaiementInfo">
                                    <div class="row">

                                        <div class="col-lg-6 " style="margin-top: 1%">
                                            <button  
                                                id="btnValiderPaiement1"
                                                class="btn btn-success pull-left">
                                                <i class="fa fa-check-circle"></i>  
                                                Valider le paiement
                                            </button>
                                            &nbsp;&nbsp;<label  id="lblDocumentPaid1" style="display: none;color: red;margin-top: -50px" > &nbsp;&nbsp;<span id="idValueMessagePaiement1"style="color: red;font-size: 14px;font-weight: normal"></span> </label>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-success" style="margin-top: 15px;visibility: hidden" id="panelDeclarationInfo">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> Informations sur la d&eacute;claration
                                    </h4>
                                </div>
                                <div class="panel-wrapper collapse in">

                                    <div class="panel-body"> 

                                        <div class="row">

                                            <table id="tablePeriodeDeclaration" style="width: 98%;margin-left: 1%"  
                                                   class="table table-bordered">
                                            </table>

                                        </div>
                                    </div>

                                </div>
                                <div class="panel-footer" style="margin-top: 15px">
                                    <div class="row">
                                        <div class="col-lg-3 pull-left" style="margin-top: 2%">
                                            <b  
                                                style="display: inline"
                                                for="labelMontantPayer">Montant à payer : &nbsp; <span 
                                                    id="labelTotalAmount"
                                                    style="color: red;font-size: 20px" ></span>
                                            </b>
                                        </div>
                                        <div class="col-lg-3 pull-left" style="margin-top: 2%">
                                            <b  
                                                style="display: inline; visibility: hidden"
                                                for="labelPenaliteRecouvrement">Pénalité de recouvrement dû : &nbsp; <span 
                                                    id="labelPenaliteRecouvrement"
                                                    style="color: red;font-size: 20px" ></span>
                                            </b>
                                        </div>

                                        <div class="col-lg-3 pull-left" style="margin-top: 2%">
                                            <b  
                                                style="display: inline"
                                                for="labelEcheance">&nbsp; <span 
                                                    id="labelEcheance"
                                                    style="color: red;font-size: 18px" ></span>
                                            </b>
                                        </div>

                                        <div class="col-lg-3 " style="margin-top: 1%">
                                            <button  
                                                id="btnValiderPaiement2"
                                                class="btn btn-success pull-right">
                                                <i class="fa fa-check-circle"></i>  
                                                Valider le paiement
                                            </button>
                                            &nbsp;&nbsp;<label  id="lblDocumentPaid2" style="display: none;color: red" > <span id="idValueMessagePaiement2"style="color: red;margin-top: -25%;font-size: 16px"></span> </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/modalSearchDocumentPaiement.xhtml" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>

        <script src="assets/js/paiement.js" type="text/javascript"></script>
        <script src="assets/js/paiement/editPaiement.js" type="text/javascript"></script>


    </body>
</html>

