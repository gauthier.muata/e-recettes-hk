<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des comptes bancaires</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">Registre des comptes bancaires</h4> 
                            <button type="button" class="btn btn-success btn-add-fonction" id="btnAddAccountBank">
                                <i class="fa fa-plus-circle"></i> Ajouter un compte
                            </button>
                        </div>
                        <div class="panel-body">

                            <div class="fonctions">
                                <table id="tableAccountBanks" class="table">

                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="modal fade" id="modalUpdateAccountBank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form class="form formulaire-edition-fonction">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Ajouter un compte bancaire </h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label class="control-label">Code</label>
                                <input type="text" class="form-control" id="codeAccountBank" placeholder="Saisir le code">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Libellé</label>
                                <input type="text" class="form-control" id="libelleAccountBank" placeholder="Saisir le libellé">
                            </div>

                            <div class="form-group">
                                <label for="swift" class="control-label">Devise</label>
                                <select class="form-control" id="selectDevise" >
                                    <option value="0">--</option>
                                    <option value="CDF">FRANC CONGOLAIS</option>
                                    <option value="USD">DOLLARS AM&Eacute;RICAINS</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="swift" class="control-label">Type de compte</label>
                                <select class="form-control" id="selectTypeCompte" >
                                    <option value="0">--</option>
                                    <option value="1">COMPTE BANCAIRE POUR TAXE</option>
                                    <option value="2">COMPTE BANCAIRE POUR IMP&Ocirc;T</option>
                                    <option value="3">COMPTE BANCAIRE POUR TAXE & IMP&Ocirc;T</option>
                                    <option value="4">COMPTE BANCAIRE POUR LA DPGP</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="swift" class="control-label">Banque</label>
                                <select class="form-control" id="selectBanque" >
                                    <option value="0">--</option>
                                </select>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-dismiss" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-danger" id="btnDeleteAccountBank" style="display: none">Supprimer</button>
                            <button type="submit" class="btn btn-success" id="btnSaveAccountBank">Enregistrer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/gestionBanque/compteBancaire.js"></script>

    </body>
</html>
