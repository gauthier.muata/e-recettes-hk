<%-- 
    Document   : document
    Created on : 22 juil. 2021, 10:06:34
    Author     : Juslin TSHIAMUA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="asset/css/font.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <title>Impression vignette</title>
        <style type="text/css">
            @page {
                //size: A4;
                size: landscape;
                margin: 0;
            }
            body{
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0;
                background-color: #FAFAFA;
                font-family: "Tahoma";
            }
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .page {
                width: 290mm;
                min-height: 350mm;
                padding: 20mm;
                margin: 10mm auto;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);

                -webkit-transform: rotate(-90deg); 
                -moz-transform:rotate(-90deg);
                filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

            }
            .subpage {
                padding: 1cm;
                border: 4px #CCC solid;
                height: 310mm;
            }
            .button {
                background-color: #4CAF50; /* Green */
                border: none;
                color: white;
                padding: 16px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                -webkit-transition-duration: 0.4s; /* Safari */
                transition-duration: 0.4s;
                cursor: pointer;
            }
            .button2 {
                background-color: white; 
                color: black; 
                border: 2px solid #008CBA;
            }
            .button2:hover {
                background-color: #008CBA;
                color: white;
            }
        </style>
    </head>

    <body class="A4" style="background-color: #FAFAFA;">
        <br/><br/>
        <div class="row">
            <button id="btnPrint1" style="margin-left: 350px" class="button button2"><i class=""></i> &nbsp; Orientation vignette 1</button>
            <button id="btnPrint2" style="margin-left: 50px" class="button button2"><i class=""></i> &nbsp;Orientation vignette 2</button>
            <button id="btnPrint" style="margin-left: 250px" class="button button2"><i class="fa fa-print"></i> &nbsp;&nbsp; Lancer impression Vignette</button>
        </div>
        <hr/>
        <div class="page">
            <div class="subpage">
                <div id="document_container">
                    <table width="70%" border="1" cellpadding="1" cellspacing="0" bordercolor="#000000" style="margin-top: 255px; margin-left: 30%">
                        <tr>
                            <td width="53%" style="height: 30px" colspan="9">
                                <div align="left" class="Style1">
                                    <p style="font-size: 10px; font-family: sans-serif; margin-bottom: 5px; margin-top: 0" id="field_0">REPUBLIQUE DEMOCRATIQUE DU CONGO</p>
                                    <p style="font-size: 10px; font-weight: bolder; font-family: sans-serif; margin-bottom: 5px; margin-top: 5px" id="field_1">PROVINCE DU HAUT-KATANGA</p>
                                    <p style="font-size: 10px; font-family: sans-serif; margin-bottom: 8px; margin-top: 7px"><span id="field_2">QUITTANCE N&deg;&nbsp;</span><span style="margin-left: 10%" id="numeroSerieVignette" ></span><span id="field_3">DELIVRE A:&nbsp;</span><span id="lieuPrintVignette" style="margin-left: 52%"></span></p>
                                    <p style="font-size: 7px; margin-bottom: 2px; margin-top: 6px" id="field_4">L'ADMINISTRATION SE RESERVE DE DROIT D'EXIGER A TOUT MOMENT LA PRESENTATION DE CETTE QUITTANCE.</p>
                                </div>
                            </td>
                            <!-- partie 2 -->
                            <td width="47%" colspan="5" rowspan="3">
                                <div align="center" style="font-size: 7px;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr style="height: 22px">
                            <td width="7%">
                                <div align="center" style="font-size: 5px;" id="field_5">NUMERO PROPRIETAIRE</div>
                            </td>
                            <td width="29%" colspan="5">
                                <div align="center" style="font-size: 7px;" id="field_6">NOM, POST-NOM, PRENOM/RAISON SOCIALE</div>
                            </td>
                            <td width="17%" colspan="3">
                                <div align="center" style="font-size: 7px;" id="field_7">DATE DE DELIVRANCE</div>
                            </td>
                        </tr>
                        <tr style="height: 15px">
                            <td width="7%" rowspan="2">
                                <div style="font-size: 6px;text-align: left; margin-left: -5px" id="codePersonne"></div>
                            </td>
                            <td width="31%" colspan="5" rowspan="2">
                                <div align="center" style="font-size: 11px;" id="namePersonne"></div>
                            </td>
                            <td width="15%" colspan="3" rowspan="2">
                                <div align="center" style="font-size: 10px;" id="dateImpression"></div>
                            </td>
                        </tr>
                        <!--plus-->
                        <tr style="height: 15px">
                            <!-- partie 2 -->
                            <td width="11%" rowspan="2">
                                <div align="center" style="font-size: 7px;">&nbsp;</div>
                            </td>
                            <td width="13.5%" rowspan="2">
                                <div align="center" style="font-size: 7px;"></div>
                            </td>
                            <td width="14%" rowspan="2" colspan="2">
                                <div align="center" style="font-size: 10px ; margin-top: 10%; font-weight: bold" id="plaque"></div>
                            </td>
                            <td width="7%" rowspan="2">
                                <div align="center" style="font-size: 7px;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr style="height: 11px">
                            <td width="37%" colspan="5" rowspan="2">
                                <div align="center" style="font-size: 5px;" id="field_8">ADRESSE GEOGRAPHIQUE</div>
                            </td>
                            <td width="1%" rowspan="2">
                                <div align="center" style="font-size: 7px;">&nbsp;</div>
                            </td>
                            <td width="15%" colspan="3" rowspan="2">
                                <div align="center" style="font-size: 6px;" id="field_9">ADRESSE POSTALE/TELEPHONE</div>
                            </td>
                        </tr>
                        <!--plus-->
                        <tr style="height: 12px">
                            <!-- partie 2 -->
                            <td width="14%" colspan="5" rowspan="2">
                                <div align="center" style="font-size: 7px;">&nbsp;</div>
                            </td>
                        </tr>
                        <tr style="height: 5px">
                            <td width="37%" colspan="6" rowspan="3">
                                <div align="center" style="font-size: 7px; text-align: center; font-weight: bold" id="adresse"></div>
                            </td>
                            <td width="15%" colspan="3" rowspan="3">
                                <div align="center" style="font-size: 8px;" id="phonePersonne"></div>
                            </td>
                        </tr>
                        <tr style="height: 21px">
                            <!-- partie 2 -->
                            <td width="11%">
                                <div align="center" style="font-size: 7px;"></div>
                            </td>
                            <td width="15%" colspan="2">
                                <div align="center" style="font-size: 10px; margin-top: 10%; font-weight: bold" id="marque"></div>
                            </td>
                            <td width="14%">
                                <div align="center" style="font-size: 10px ; margin-top: 10%; font-weight: bold" id="modele"></div>
                            </td>
                            <td width="7%">
                                <div align="center" style="font-size: 7px;">&nbsp;</div>
                            </td>
                        </tr>
                        <!--Plus-->
                        <tr style="height: 5px">
                            <!-- partie 2 -->
                            <td width="14%" colspan="5" rowspan="7">
                                <img id="codeQR" style="margin-left: 44%; margin-bottom: 24%" src = "" alt="" width="48px" height="48px" />
                            </td>
                        </tr>
                        <tr style="height: 17px">
                            <td width="8%" colspan="2">
                                <div align="center" style="font-size: 4px;" id="field_10">MONTANT&nbsp;DE L'IMPOT</div>
                            </td>
                            <td width="9%">
                                <div align="center" style="font-size: 7px;" id="field_11">TOTAL&nbsp;(IMPOT+TSCR)</div>
                            </td>
                            <td width="10%">
                                <div align="center" style="font-size: 7px;" id="field_12">ACCROISSEMENTS</div>
                            </td>
                            <td width="10%">
                                <div align="center" style="font-size: 7px;" id="field_13">DROITS&nbsp;RECOUVRES</div>
                            </td>
                            <td width="18%" colspan="4">
                                <div align="center" style="font-size: 7px;" id="field_14">NOMS&nbsp;SIGNATURE</div>
                            </td>
                        </tr>
                        <tr style="height: 29px">
                            <td width="8%" colspan="2">
                                <div align="center" style="font-size: 12px;"></div>
                            </td>
                            <td width="9%" rowspan="3">
                                <div align="center" style="font-size: 12px;"></div>
                            </td>
                            <td width="8.5%" rowspan="3">
                                <div align="center" style="font-size: 12px;" id="penalite"></div>
                            </td>
                            <td width="8.5%" rowspan="3">
                                <div align="center" style="font-size: 12px;" id="total"></div>
                            </td>
                            <td width="18%" rowspan="3" colspan="4">
                                <div align="center" style="font-size: 9px;" id="nameUserPrint"></div>
                            </td>
                        </tr>
                        <tr style="height: 15px">
                            <td width="8%" colspan="2">
                                <div align="center" style="font-size: 4px;" id="field_15">MONTANT&nbsp;DE LA TSCR</div>
                            </td>
                        </tr>
                        <tr style="height: 29px">
                            <td width="7%" colspan="2">
                                <div align="center" style="font-size: 7px;"></div>
                            </td>
                        </tr>
                        <tr style="height: 22px">
                            <td width="7%">
                                <div align="center" style="font-size: 7px;" id="field_16">NUMERO DE PLAQUE</div>
                            </td>
                            <td width="9%" colspan="2">
                                <div align="center" style="font-size: 7px;" id="field_17">GENRE</div>
                            </td>
                            <td width="11%">
                                <div align="center" style="font-size: 7px;" id="field_18">MARQUE</div>
                            </td>
                            <td width="8.5%">
                                <div align="center" style="font-size: 7px;" id="field_19">TYPE</div>
                            </td>
                            <td width="8.5%" colspan="2">
                                <div align="center" style="font-size: 7px;" id="field_20">NUMERO DU CHASSIS</div>
                            </td>
                            <td width="6%">
                                <div align="center" style="font-size: 7px;" id="field_21">POIDS EN KG.</div>
                            </td>
                            <td width="3%">
                                <div align="center" style="font-size: 7px;" id="field_22">NBRE CV</div>
                            </td>
                        </tr>
                        <tr style="height: 56px">
                            <td width="7%">
                                <div style="font-size: 5px;margin-bottom: 20px; text-align: center;margin-left: 10px; font-weight: bold" id="plaque2"></div>
                            </td>
                            <td width="9%" colspan="2">
                                <div align="center" style="font-size: 7px;">&nbsp;</div>
                            </td>
                            <td width="11%">
                                <div align="center" style="font-size: 10px;margin-bottom: 20px;" id="marque2"></div>
                            </td>
                            <td width="8.5%">
                                <div align="center" style="font-size: 10px;margin-bottom: 20px;" id="modele2"></div>
                            </td>
                            <td width="8.5" colspan="2">
                                <div align="center" style="font-size: 10px;margin-bottom: 20px;" id="chassis"></div>
                            </td>
                            <td width="6%">
                                <div align="center" style="font-size: 10px;margin-bottom: 20px;" id="puissanceFiscalKg"></div>
                            </td>
                            <td width="3%">
                                <div align="center" style="font-size: 10px;margin-bottom: 20px;" id="puissanceFiscalCv"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>    
        </div>

        <!--%@include file="assets/include/modal/modalPrintVignette.html" %-->

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script> 
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/lib/js/printThis.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.qrcode.js"></script>
        <script type="text/javascript" src="assets/lib/js/qrcode.js"></script>

        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>


        <script src="assets/js/visualisationVignette.js" type="text/javascript"></script>
        <script type="text/javascript">

            $(function () {

                $('#create_pdf').click(function (e) {
                    e.preventDefault();

                    var element = document.getElementById('document_container');

                    var opt = {
                        margin: 1,
                        filename: 'myfile.pdf',
                        image: {type: 'jpeg', quality: 0.98},
                        html2canvas: {scale: 2},
                        //jsPDF: {unit: 'in', format: 'letter', orientation: 'portrait'}
                    };

                    var worker = html2pdf().from(element).set(opt).save();

                });

            });

        </script>
    </body> 
</html>
