<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion de prise en compte de bons de pré-paiement péage</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading flex-align-center"> 
                            <h4 class="panel-title">Informations des bons de pré-paiement en circulation</h4> 

                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-10">
                                    <label class="control-label" style="font-weight: normal">Soci&eacute;t&eacute; s&eacute;lectionn&eacute;e :</label>
                                    <input type="text" class="form-control" id="societeNameEdit" readonly="true"
                                           placeholder="Soci&eacute;t&eacute; s&eacute;lectionn&eacute;e">
                                </div>
                                <div class="col-lg-2" style="margin-top: 23px">
                                    <button type="submit" class="btn btn-success" id="btnSearchSociete">Rechercher une Soci&eacute;t&eacute;</button>
                                </div>
                            </div><br/>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Axe p&eacute;age </label>
                                <select class="form-control" id="selectSiteEdit" disabled="true">
                                    <option value="0">--</option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Mode de perception</label>
                                <select class="form-control" id="selectModePaiement" >
                                    <option value="0">--</option>
                                    <option value="1">PRE-PAIEMENT</option>
                                    <option value="2">CREDIT</option>
                                    <option value="3">EXON&Eacute;RATION</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Cat&eacute;gorie</label>
                                <select class="form-control" id="selectCategorieEdit" disabled="true">
                                    <option value="0">--</option>
                                </select>

                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: normal">Tarif <span id="spnDevise" style="color:red"></span></label>
                                            <input class="form-control" min="1" readonly="true"
                                                   id="tarifEdit" placeholder="Saisir le tarif">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <label style="font-weight: normal">Date du paiement</label>
                                        <input type="date" class="form-control" id="datePaiement">
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Nombre de bons remis</label>
                                <input type="number" class="form-control" min="1"
                                       id="nombreBonRemis" placeholder="Saisir le nombre de bon remis">
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="font-weight: normal">Montant total <span id="spnDevise2" style="color:red"></span></label>
                                <input type="text" class="form-control" min="1" readonly="true" style="font-weight: bold;font-size: 16px"
                                       id="montantTotal">
                            </div>


                            <div class="row">
                                <div class="col-lg-6">
                                    <label class="control-label" style="font-weight: normal">Banque</label>
                                    <select class="form-control" id="selectBanque" >
                                        <option value="B0011">TMB</option>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label class="control-label" style="font-weight: normal">Compte bancaire</label>
                                    <select class="form-control" id="selectCompteBancaire" >
                                        <option value="0017-25000-00372070201-60">(0017-25000-00372070201-60) PROVINCE DUHAUT-KATANGA AXE LUBUMBASHI-PEAGE KASENGA</option>
                                    </select>
                                </div>
                            </div><br/>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success" id="btnSave" disabled="true">
                                    <i class="fa fa-save"></i> Enregistrer
                                </button>
                            </div><hr/>

                            <div class="fonctions">
                                <table id="tableData" class="table">

                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>

        <%@include file="assets/include/modal/rechercheAssujetti.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script type="text/javascript" src="assets/js/tarifSite/gestionExistantBonPeage.js"></script>

    </body>
</html>
