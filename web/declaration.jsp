<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des paiements</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Information sur la déclaration</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="panel-body">

                                <div class="row"> 
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Noms assujetti <span style="color: red">*</span> :</label>
                                            <input type="text" class="form-control" id="inputAssujetti" >
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label  for="lblInputCompteBancaire">Compte bancaire <span style="color: red">*</span> </label>
                                            <!--input type="text" class="form-control" id="inputCompteBancaire" /-->
                                            <select class="form-control" id="inputCompteBancaire">

                                            </select><br/>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label>Intitulé impôt :</label>
                                            <textarea type="text" style="height: 100px" class="form-control"  id="inputImpot" > </textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <br/><br/>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary" id="btnSearchImpot"><i class="fa fa-search"></i> &nbsp;Rechercher</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group" style="font-size: 15px">
                                            <label for="lblPeriodicité"  >
                                                Périodicité &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                            </label>
                                            <a id="lblPeriodicite">&nbsp;</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group" style="font-size: 15px">
                                            <label for="lblFaitGenerateur" >
                                                Fait génerateur &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                            </label>
                                            <a id="lblFaitGenerateur">&nbsp;</a>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>Année :</label>
                                            <select class="form-control" id="selectAnnee">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>Mois :</label>
                                            <select class="form-control" id="selectMois">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-1" id="divJour" hidden="false">
                                        <label>Jour :</label>
                                        <input type="number" class="form-control" id="inputJour"/>
                                    </div>
                                </div>

                                <hr/>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <label  for="lblMontantDu">Montant dû <span style="color: red">*</span> </label>
                                        <input type="number" class="form-control" id="inputMontantDu" />
                                    </div>
                                    <div class="col-lg-4">
                                        <label  for="lblMontantDuFormat"> </label>
                                        <input type="text" class="form-control" id="inputMontantDuFormat" readonly="true" required="true"/>                  
                                    </div>
                                    <div class="col-lg-4">
                                        <label  for="lblInputBordereau">Numéro bordereau <span style="color: red">*</span></label>
                                        <input type="text" class="form-control" id="inputBordereau" required="true"/>                  
                                    </div>
                                </div> <br/>

                                <div class="row">

                                    <div class="col-lg-3">
                                        <label  for="lblInputNumAttestation">Numéro attestation <span style="color: red">*</span> </label>
                                        <input type="text" class="form-control" id="inputNumAttestation"/>
                                    </div>
                                    <div class="col-lg-3">
                                        <label  for="lblInputNumDeclaration">Numéro déclaration <span style="color: red">*</span></label>
                                        <input type="text" class="form-control" id="inputDeclaration" />                  
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label  for="lblInputDateEmissionBordereau">Date d'émission du bordereau <span style="color: red">*</span></label>
                                            <div class="input-group date " id="datePicker1" style="width: 100%;">
                                                <input type="text" class="form-control" id="inputDateEmissionBordereau" >
                                                <div class="input-group-addon">
                                                    <span class="fa fa-th"></span>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-lg-3">
                                        <label  for="lblInputSite">Centre <span style="color: red">*</span></label>
                                        <select type="text" class="form-control" id="inputCentre" >   <option value="Centre1">Centre1</option>  </select>             
                                    </div>

                                </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="btnSaveBordereau"><i class="fa fa-check"></i> &nbsp;Enregistrer</button>
                        <!--button type="button" class="btn btn-secondary" data-dismiss="modal" >Annuler</button-->
                    </div>
                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>

        <%@include file="assets/include/modal/rechercheAvanceePaiement.html" %>
        <%@include file="assets/include/modal/rechecheImpot.html" %>


        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script src="assets/js/getImpotModal.js" type="text/javascript"></script>
        <script src="assets/js/declaration.js" type="text/javascript"></script>

    </body>
</html>
